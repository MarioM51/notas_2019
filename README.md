# NOTAS

- [NOTAS](#notas)
  - [Como tomar notas](#como-tomar-notas)
    - [Crear fichas anki en texto plano](#crear-fichas-anki-en-texto-plano)
    - [Markdown y pdf](#markdown-y-pdf)
      - [Plugin para generarlo](#plugin-para-generarlo)
      - [Nueva pagina](#nueva-pagina)
    - [Cuestionarios con Quizfaber](#cuestionarios-con-quizfaber)

## Como tomar notas

Se van asiendo al mismo tiempo los siguientes pasos, estos 3 archivos en
una misma carpeta, donde cada carpeta significa un tema.

1. Al estudiar teoria
   - Escribir lo que entendi
   - Escribir analogias
   - Guardar lo necesario y escribirlo para repetir la practica
      en el futuro

2. Al hacer una practica
   - Escribir resultados

3. Crear recursos visuales en mi caso .drawio
   - Mapas mentales solo con palabras o fraces cortas clave.
   - Dibujos, diagramas que expliquen el tema

4. .anki: Crear preguntas solo por pregunta y respuesta.

**Cada vez que termines una seccion** o capitulo crear checar hacer lo siguiente.

1. Agregar contenido a resultados finales con evidencia, yo lo hago
   con imagenes gif y .zip para mostrar que hize y poder descargarlo.
2. Agregar contenido a un mapa mental mas general.
3. Checar que si importan bien a .PDF o .PNG lo que estas haciendo.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Crear fichas anki en texto plano

- Fuente: [ankiweb.net](https://apps.ankiweb.net/docs/manual20.html#importing-text-files)

Crear un archivo en texto plano (podemos ponerle terminacion .txt u otra)
y crear las targetas con el siguiente formato.

```r
field one;"field two with ""escaped quotes"" inside it"
field two;"Holle there"
field two;"Imagenes al mismo nivel del .anki <img src="myimage.jpg"> "
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Markdown y pdf

#### Plugin para generarlo

Plugin en VS Code `Markdown PDF de vyzane`, en mi caso no abre el pdf,
solo lo guarda en la carpeta de descargas.

#### Nueva pagina

Usamos el siguiente div donde queremos que termine y empieze otra pag.

```html
<div style="break-after:page"></div>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Cuestionarios con Quizfaber

Para crear un examen usamos el programa `quizfaber`
