# Estadística descriptiva con RStudio y Python

- [Estadística descriptiva con RStudio y Python](#estad%c3%adstica-descriptiva-con-rstudio-y-python)
  - [Introduccion](#introduccion)
    - [7. Que es R y RStudio](#7-que-es-r-y-rstudio)
    - [Instalacion de R y RStudio](#instalacion-de-r-y-rstudio)
    - [Descargar proyecto](#descargar-proyecto)
    - [La ayuda de R](#la-ayuda-de-r)
    - [18. Cómo instalar y cargar librerías en R](#18-c%c3%b3mo-instalar-y-cargar-librer%c3%adas-en-r)
    - [19. R como calculadora](#19-r-como-calculadora)
    - [Variables y funciones](#variables-y-funciones)
    - [Numeros complejos](#numeros-complejos)

## Introduccion

### 7. Que es R y RStudio

R es un entorno de programacion de codigo libre orientado especificamente
para el analisis estadistico, y se a estado abriendo al mundo de los
graficos y otras librerias, y es muy usado en sector privado y publico,
y con una comunidad muy activa la principal es CRAN (Comprehensive R
Archive Network)

**RStudio** es el IDE por defecto de R, solo es gratuito para fines no
comerciales

![01-IDE-RStudio](./img/01-IDE-RStudio.PNG)

### Instalacion de R y RStudio

A. **Instalar R** (siempre tendremos que instalarlo primero) Vamos a www.r-project.org -> cran, seleccionamos un mirrror en mi caso mirrors.nics.utk.edu/cran y descargamos la vercion de nuestro sistema, y sig, sig.

B. **Instalar RStudio** Lo descargamos de [rstudio.com](https://rstudio.com/products/rstudio/download/#download) y le damos sig, sig.

C. **Comprobar** con RStudio abierto file -> new -> RMarkdown nos pedira instalar algunos plugins y construimos en HTML

C.1. **Generar HTML** Con el rmarkdown abierto le damos file -> preview y devemos de ver nuestro documento en formato html en una ventana

C-2. **Generar PDF**, para esto le cambiamos el output por `pdf_document` y demos preview, nos da un **error** que nos dice que instalemos Latex y nos da la opcion de ejecutar `tinytex::install_tinytex()` para instalar solo lo que necesitamos

C-3, y luego en PDF el cual nos marcara un error, y para solucionarlo leemos el error el cual indica que intalemos lLatex

> **Error** "pandoc document conversion failed with error 43
Execution halted".  
> **Descripcion** Al darle en preview en PDF no muestra el PDF y muestra este error.  
> **Solucion** Ejecutamos los siguientes comandos

```r
install.packages("devtools")
library(usethis)
library(devtools)
install_version("rmarkdown",version=1.8)
```

### Descargar proyecto

A. Lo clonamos desde [aqui](https://github.com/joanby/r-basic) y abrimos el proyecto con RStudio seleccionando el `.Rproj`

B. Asignamos el directorio de trabajo donde esta nuestro proyecto con Session->Set Working directory->To files pane Location.

![02-entorno-listo](./img/02-entorno-listo.PNG)

**Resultado:** Tenemos abierto el pryecto de R que nos descargamos de Git y podemos compilar el Rmd a html y pdf.

### La ayuda de R

podemos conseguir ayuda de los comandos de R por medio del siguiente comando.

```r
help(comando)

# Ejemplo help(ln)
```

En donde podremos ver los ejemplos en la parte de abajo

![03-ayuda-rstudio](./img/03-ayuda-rstudio.PNG)

### 18. Cómo instalar y cargar librerías en R

A lo largo del curso usaremos diferentes paquetes que nos ayudaran

- Las librerias cargadas desaparecen al reiniciar session 

```r
# dep=TRUE, instala los paquetes que dependa el paquete a instalar
install.packages("nombre_paquete", dep=TRUE); #instala o actualiza

library("nombre_paquete", dep=TRUE); #Carga paquete ya instalado

installes.packages() # mustra todos los paquetes instalados
```

### 19. R como calculadora

```r
2*pi # Uso de constantes como PI

Inf # Uso de infinuto
-Inf

NA # equivalente a null
NaN # No es un numero

2^50# = 1.1259e+15 = 1125900000000000 # uso de exponentes
2 ^(-15) # 3.051758e-05 = 0.00003051758
```

Funciones Matematicas

![04-funciones-matematicas](./img/04-funciones-matematicas.PNG)

```r
sqrt(25) # Raiz cuadrada
exp(1)
log(pi)
abs(-pi)

factorial(7) # !7 = 5040

choose(5, 3)   # = 10, Cantidad de combinaciones, ej, de cuantas formas me puedo comer 5 platos en 3 combinaciones
```

Funciones trigonometricas, Los argumentos los debemos de dar en radianes, y nos daran los resultado en grados.

```r
# para sacar el seno 60 grados lo debemos de multiplicar por pi/180
sin(60*pi/180)  
asin(0.8660254) # = 1.047198, arc sin en radianes
asin(0.8660254)*180/pi # = 60, en grados
asin(5) # = NAN, el arcocoseno esta entre 1 y -1 asi que da NAN
```

Numeros con coma flotante y redondeos

```r
print(sqrt(2),10) # = 1.414213562 (10=queremos 10 dijitos)
round(sqrt(2), 3) # = 1.414, rodondear a 3 decimales
floor(sqrt(2)) # = 1, rodondear hacia abajo
ceiling(sqrt(2)) # = 1, rodondear hacia arriba

print(2^50, 15) # pedir mas decimales
```

### Variables y funciones

![05-variables-creadas](./img/05-variables-creadas.PNG)

```r
x = (pi^2)/2
x # = 4.934802

sin(pi/4)+cos(pi/4) -> z
z # = 1.414214

edad <- 30
edad # = 30

# Funcion
suma1 <- function(t){
  t + 1
}
suma1(6) # = 7
suma1(-5) # = -4

# funcion de varias lineas
suma5 <- function(numero){
  numero = suma1(numero);
  numero = suma1(numero);
  numero = suma1(numero);
  numero = suma1(numero);
  suma1(numero)
}
suma5(3) # = 8

# listar todas las variables creadas, listar todas las funciones creadas
ls()

# eliminar variable, eliminar funcion
rm(product)

rm(list = ls()) # elimintar todo
```

### Numeros complejos

```r
(3+2i)*5 # = 15+10i
(3+2i)*(-1+3i) # = -9+7i
(3+2i)/(-1+3i) # = 0.3-1.1i
##2+7*i  Esto es un error: 

sqrt(-5) # da error NaN
sqrt(as.complex(-5)) # = 0+2.236068i
```
