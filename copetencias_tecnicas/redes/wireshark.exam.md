
# WireShark

1 ¿Cuales son las partes de una direccion MAC?

- [ ] OUI (Organizationally Unique Identifier) <!-- Correcta -->
- [ ] UUID (Unic Unix IDentifier)
- [ ] UAA (Universally Administred Address) <!-- Correcta -->

____

2 Para que sirve el OUI de una direccion MAC?

- [ ] Podemos identificar el fabricante del dispositivo <!-- Correcta -->

____

3 QUe es lo primero que deveriamos de hacer en Wireshark al ver en la
capa de aplicacion de tipo TCP?

- [ ] ver las banderas  <!-- Correcta -->

____

6 Como esta conformado un handshake

- [ ] Por las banderas en TCP en el orden primer trama (cliente->servidor)
      tiene un [SYN] el segundo (servidor->cliente) un [SYN, ACK] y el
      tercero (cliente->servidor) [ACK],  <!-- Correcta -->

____

4 ¿Que es el trafico tipico y de fondo?

- [ ] Tipico, es el trafico que nos interesa analizar  <!-- Correcta -->
- [ ] De Fondo, es trafico que ocurre en automatico y lo concideramos ruido para nuestro analisis  <!-- Correcta -->

____

5 Como podemos analisar las IPs publicas, para saber por ejemplo, su ubicacion aprox, organizacion etc?

- [ ] Con herramientas como `whois.domaintools.com`  <!-- Correcta -->

____

6 Que es un buffer en anillo para la creacion de archivos de registros?

- [ ] Cuando la captura de registros se iran guardado en una X cantidad de archivos, por ejemplo, se guardaran los registros en 3 archivos cuando se llene el ultimo, se suistituira el primero y asi susesivamente.    <!-- Correcta -->
