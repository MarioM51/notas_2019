# WireShark

- [WireShark](#wireshark)
  - [Introduccion](#introduccion)
    - [Que es WireShark](#que-es-wireshark)
    - [TCP/IP](#tcpip)
    - [TCP/IP 2 - conceptos de red](#tcpip-2---conceptos-de-red)
  - [Exploracion](#exploracion)
    - [Laboratorio 1](#laboratorio-1)
    - [11. Tráfico de red típico y de fondo](#11-tr%c3%a1fico-de-red-t%c3%adpico-y-de-fondo)
    - [12. Laboratorio 2 - identificar trafico de fondo](#12-laboratorio-2---identificar-trafico-de-fondo)
    - [13. Archivos de captura de tráfico](#13-archivos-de-captura-de-tr%c3%a1fico)
  - [Personalizacion](#personalizacion)
    - [16. Columnas](#16-columnas)
    - [17. Lab. 4 Resaltar URLs](#17-lab-4-resaltar-urls)
    - [18. Disectores](#18-disectores)
    - [19. Puertos no-estándar](#19-puertos-no-est%c3%a1ndar)
    - [20. Ajustes de Visualizaicón](#20-ajustes-de-visualizaic%c3%b3n)
    - [21. Lab. 5, Detectar Offloading](#21-lab-5-detectar-offloading)
    - [22. Perfiles](#22-perfiles)
    - [23. Lab. 6, Crear perfil para HTTP](#23-lab-6-crear-perfil-para-http)
    - [24. Archivos de configuración](#24-archivos-de-configuraci%c3%b3n)
    - [25. Lab. 7 importar perfil](#25-lab-7-importar-perfil)
    - [26. Detección de latencia y sus tipos](#26-detecci%c3%b3n-de-latencia-y-sus-tipos)
    - [27. Lab. 8. Detectar latencia y su causa](#27-lab-8-detectar-latencia-y-su-causa)
  - [Sección 4: Captura](#secci%c3%b3n-4-captura)
    - [29. Demasiados Paquetes](#29-demasiados-paquetes)
    - [30. Técnicas de Captura](#30-t%c3%a9cnicas-de-captura)
    - [32. Reducir el Tráfico Capturado](#32-reducir-el-tr%c3%a1fico-capturado)
    - [33. Capturar Tráfico de Algunos Nodos](#33-capturar-tr%c3%a1fico-de-algunos-nodos)
    - [34. Lab. 10, Capturar trafico de los demas nodos](#34-lab-10-capturar-trafico-de-los-demas-nodos)
    - [35. Capturar Tráfico de una Aplicación](#35-capturar-tr%c3%a1fico-de-una-aplicaci%c3%b3n)
    - [Lab. Usar filtro de captura DNS, Guardar filtro](#lab-usar-filtro-de-captura-dns-guardar-filtro)
    - [Reto](#reto)
  - [Seccion 5: Visualizacion](#seccion-5-visualizacion)
    - [38. Filtros de Visualización](#38-filtros-de-visualizaci%c3%b3n)
    - [39. Filtrado en la Capa de Aplicación](#39-filtrado-en-la-capa-de-aplicaci%c3%b3n)
    - [40. Filtrar Tráfico para un Nodo](#40-filtrar-tr%c3%a1fico-para-un-nodo)
    - [40. Expandir Filtros](#40-expandir-filtros)
    - [42. Aplicar Filtros de Manera Rápida](#42-aplicar-filtros-de-manera-r%c3%a1pida)
    - [43. Filtrar una Conversación TCP/UDP](#43-filtrar-una-conversaci%c3%b3n-tcpudp)
    - [44. Revisión de Filtro Aplicado](#44-revisi%c3%b3n-de-filtro-aplicado)
    - [45. Filtrar una Palabra](#45-filtrar-una-palabra)
  - [Seccion 6: Analisis](#seccion-6-analisis)
    - [47. Reglas de Coloreado](#47-reglas-de-coloreado)
    - [48. Crear Reglas de Coloreado](#48-crear-reglas-de-coloreado)
    - [50. Exportar Paquetes de Interés](#50-exportar-paquetes-de-inter%c3%a9s)
    - [51. Quién está Hablando con Quién](#51-qui%c3%a9n-est%c3%a1-hablando-con-qui%c3%a9n)
    - [52. Identificar a Top-Talkers](#52-identificar-a-top-talkers)
    - [53. GeoIP](#53-geoip)
    - [54. Listar Aplicaciones de Red](#54-listar-aplicaciones-de-red)
    - [55. Graficar el Ancho de Banda Ocupado](#55-graficar-el-ancho-de-banda-ocupado)
  - [Seccion 4: CapturaSección 7: Reensamblado](#seccion-4-capturasecci%c3%b3n-7-reensamblado)
    - [57. Reensamblado HTTP](#57-reensamblado-http)
    - [58. Reensamblado de un Archivo Transferido vía FTP](#58-reensamblado-de-un-archivo-transferido-v%c3%ada-ftp)
    - [59. Añadir Comentarios al Archivo de Captura](#59-a%c3%b1adir-comentarios-al-archivo-de-captura)
    - [extraer imagenes/archivos de captura de carga de un sitio](#extraer-imagenesarchivos-de-captura-de-carga-de-un-sitio)
  - [Critica que di al curso](#critica-que-di-al-curso)

## Introduccion

### Que es WireShark

Es un analizador de protocolos de internet, siendo una herramienta que
trabaja con una interface de web el cual es nuestro medio de comunicacion,
el cual tiene 3 funciones

1. Capturar trafico de red
2. Visializar trafico de red
3. Analizar trafico de red

Con los 3 anteriores wireshark puede interpretar del lenguaje de pc
a lenguaje humano, tenemos los siguientes ejemplos.

- Ancho de banda: Identiicar que nodos consumen el mayor ancho de banda, lo
cual se haria lo siguientes

  - Tomar captura de trafico de la interfas de red
  - Hacer un analisis estadistico que generara una lista de computadoras
    ordenadas segun su consumo

- Errores de Conexion, 1) tomar captura de trafico, 2) replicar el problema
  3) analisar el trafico para ver los mensajes de error

- Cuellos de botella: tomar el trafico y ver que es lo que esta causando
  los problemas de desempeño
  
- Malware/Virus: Podemos hacer una captura de trafico de esta computadoras
  por un tiempo, analisar el por ejemplo que se conecte con IPs publicas de
  mala reputacion.

**¿Quienes usan WireShark?**: Es muy popular para seguridad informatica pero
tiene diferentes usos como los ejemplos, los cuales puede cubrir igual
programadores.

----

### TCP/IP

Es un conjunto de protocolos para una red, estos protocolos son
parecidos a "no hablar todos al mismo tiempo", "no interrumpir" etc, y
si todos cumplen con estos, todas las computadoras podran comunicarse.

**Las cuatro capas TCP/IP** funcionan de forma modular cada capa hace
una tarea concreta las cuales son

![01-capas-tcp-ip](./img/01-capas-tcp-ip.PNG)

1. **Aplicacion**: Es un puente entre nuestra aplicacion y la red.
2. **Transporte**: Provee comunicacion de extremo a extremo en una red
   de forma cofiable y no confiable, ejemplo, es el encargado de crear
   multiples conexiones para multiples servicios que podemos tener al
   mismo tiempo.
3. **Internet**: Provee direcciones logicas para dispositivos(se encaga
   de a cada computadora asignarle una IP unica), empaquetado (cortar
   grandes archivos en pequeños y rearmarlo) y enrutamiento (busqueda
   de la ruta mas optima)
4. **Red**: Provee acceso a la interfaz fisica de red y al medio de
   transmicion, se preocupa por los caminos y los nodos por donde va a
   viajar el paquete, por ejemplo, pc -> router -> DNS -> switch -> servidor.
   tambien se preocupa por si es inalambrico, cable, etc.

**Cabecera** Existe una cabecera por cada capa la cual contiene cosas
como ip-origen, ip-destino, direccion-mac etc. y cada una tiene un
nombre como vemos abajo. Es una convecion para por ejemplo, decirle a
un compañero que revice las tramas.

![02-tcp-ip-nombre-cabeceras](./img/02-tcp-ip-nombre-cabeceras.PNG)

Podremos encontrar en cada cabecera la siguiente informacion

- En red, podremos encontrar las direcciones mac

- En internet, veremos las direcciones IP origen destino

- En Trasporte, TCP (Confiable) y UDP (No confiable) veremos numero de puerto.

- En Aplicacion, Son multiples datos ya que pueden ver protocolos como
  HTTP, DNS, y comandos propios por ejemplo como GET, POST para http

Podemos ver una relacion directa en Wireframe con las capas por ejemplo

- el centro azul son los paquetes que puede que varios de estos
  signifique un mensaje puede que por el tamaño la capa de trasporte
  lo halla partido

- Las cajitas de colores de abajo a la izq son las capas, estas aparecen
 cuando seleccionamos un paquete

![02-tcp-ip-nombre-cabeceras](./img/03-relacion-capas-wireframe.PNG)

----

### TCP/IP 2 - conceptos de red

**Router:** Este dividen dominios de broadcast (lan), las computadoras
se pueden comunicar directamente en una lan, pero para comunicarse con
una que este fuera de su lan, lo tendra que hacer por medio de su puerta
de enlace (192.168.1.1)

![04-broadcast](./img/04-broadcast.PNG)

**Swiches** Dividen dominios de colision el cuas es un segmento fisico
de una red, donde es posible que las tramas puedan colicionar (puerto),
podemos ver como cada maquina no podra colicionar su informacion ya que
cada una tiene su propio puerto, hace tiempo no hera hace lo que podia
haber colicion lo cual obligaba a tener cominicacion uni-direccional o
half-duplex

![05-dominio-de-colision](./img/05-dominio-de-colision.PNG)

**Router y Swiches** Lo que hacen estos es utilizar y modificar las
cabeceras de TCP/IP, Por ejemplo, en la tabla podemos ver informacion
de las capas (es un dispositivo de red por lo que solo le intereza esta
capa), y lo que hara el Switch es leer la direccion MAC destino y
**usara el puerto 1** para enviar la informacion por el puerto 1 que
le pertenece a esa maquina.

![06-ejemplo-comunacion-ssh](./img/06-ejemplo-comunacion-ssh.PNG)

----
----
----

## Exploracion

### Laboratorio 1

Vamos a aprender de una red que tiene un en una red asiendo span con
algun puerto troncal de la red, en la cual veremos paquetes de otras
computadoras. El escenario es no conocemos la topologia de la red, es
decir vamos a ver que otras maquinas estan conectadas a nuestra red y
a donde dirijen su trafico.

**TCP 3-way handshake** este es un concepto de redes en el que cada vez
que vamos a iniciar comunicacion con un servidor, ocurre este proceso
que es un intercambio de paquetes previo entre cliente-servidor, para
asegurar una comunicacion segura, el cual ocurre por medio de TCP.

![handshake](./img/07-handshake.PNG)

1. Se envia un paquete SYN de sincronizacion al servidor
2. El serviror responde con un ACK que dice basicamente que si le llego
   el paquete que le envio, asemas le envia un SYN ya que igual quiere
   sincronizar datos con el cliente.
3. El cliente le envia un ACK para indicar que acepta la sincronizacion
   con el servidor

A. Abrimos la [captura](./img/08-laboratorio-01.pcapng) de nuestro
laboratorio.

A-1. En la **primer trama** en la **capa de ethernet** podemos ver la
MAC de origen (terminacion c1) y de destino (terminacion 01)

```r
Ethernet II...
  Source=Origen: Cadant_31:bb:c1 (00:01:5c:31:bb:c1) # router
  Destination: IPv6mcast_01 (33:33:00:00:00:01) # NO NOS SIRVE
```

si nos fijamos tiene IPv6mcast_01 lo que significa que es una multi cast,
lo que significa no le envia el paquete a nadie en espesifico mas bien
se los envia a un grupo de nodos.

A-2. Ahora vamos a **Protocolo de internet** y vemos que el origen del
paquete es terminacion bbc1.

```r
# Ethernet II...
#   Source=Origen: Cadant_31:bb:c1 (00:01:5c:31:bb:c1)
Internet Protocol Version 6...
  Source: fe80::201:5cff:fe31:bbc1
  # Destination: ff02::1 NO NOS SIRVE IGUAL ES MULTICAST POR EL ANTERIOR LO SABEMOS

```

A-3. Vamos a la **capa de aplicacion** en este caso un ICMP (Internet
Control Message Protocol v6) que sirve para cosas como Pings, trace-routes,
advertansing (anuncio de ruta).

```r
# Ethernet II...
#   Source=Origen: Cadant_31:bb:c1 (00:01:5c:31:bb:c1)
# Internet Protocol Version 6...
#   Source: fe80::201:5cff:fe31:bbc1
Internet Control Message Protocol v6
  Type: Router Advertisement (134)
```

Y vemos que el paquete de tipo **Router Advertisement** lo cual ya nos
indica/confirmar que el dipositivo origen es un router

**Resumen** Sabemos que tenemos un router en la sig MAC e IP

![08-laboratorio-1-paso-1](./img/08-laboratorio-1-paso-1.PNG)

B. En la **segunda trama**,

B-1. En la capa de Internet vemos que el destino es la misma MAC de
nuestra primera trama en la misma capa, lo que sabemos es el

B-2.Vemos nuestra **capa de aplicacion** en este caso es un protocolo
**ARP** que tenemos el origen=source con la ip 24.6.168.1 que es nuestro
Router, y el destino es 24.6.175.56

```r
Ethernet
  Source: Cadant_31:bb:c1 (00:01:5c:31:bb:c1) #router

Address Resolution Protocol (request)
  Sender IP address: 24.6.168.1  # = ORIGEN
  Target IP address: 24.6.175.56
```

La IP 24.6.175.56 no sabemos que es podria ser un PC, Router, Switch, etc

![08-laboratorio-1-paso-1](./img/08-laboratorio-1-paso-2.PNG)

C. Vamos a **Trama 3**

C-1. **Capa de Red (Ethernet)** vemos el destino (d4:85:64:a7:bf:a3) lo
cual es un equipo que no hemos conocemos, pero si nos fijamos tiene
`HewlettP_a7` el cual Wireshark lo sabe por el OUI de la direccion MAC
y si buscamos en Google sabremos que este es un fabricante targetas de red
= interface de red para Laptops.

![08-laboratorio-1-paso-1](./img/08-laboratorio-1-paso-3-partes-de-una-MAC.jpg)

```r
Ethernet II...
  Destination: Cadant_31:bb:c1 (00:01:5c:31:bb:c1) # rauter
  Source: HewlettP_a7:bf:a3 (d4:85:64:a7:bf:a3) # LAPTOP
```

**Resumen** podemos ver que el paquete sale (Source) de la Laptop y
va al router

C-2. **En la capa de internet**, podemos ver que el origen del paquete
es 24.6.173.220 y como el origen de la capa de Ethernet es el de la Lap,
sabemos que **24.6.173.220 es la IP de la Laptop**, y vemos que el destino
es 216.168.252.157 pero con esta trasa no podemos saber que tipo de equipo
es este.

```r
Internet Protocol Version 4...
  Source: 24.6.173.220
  Destination: 216.168.252.157
```

**Resumen** Por medio de la OUI sabemos que tipo de equipo es el origen
(una Laptop) y que tenemos un nuevo dispositivo (216.168.252.157) desconocido.

**Nota:** en el mundo real una PC no se va a conectar directamente a un
router, lo hacen por medio de un Switch o varios, en el laboratorio no
nos itereza ya que es transparente para nosotros.

![08-laboratorio-1-paso-3](./img/08-laboratorio-1-paso-3.PNG)

C-3. **En la capa de aplicacion TCP** vemos que tiene una bandera de
tipo (SYN), lo cual indica que se inicia una sincronizacion

```r
Transmission Control Protocol ...
  Source Port: 41865 # este es aleatorio puesto por la laptop
  Destination Port: 80 # Puerto estandart para HTTP
  Flags: 0x002 (SYN) # indica que es una Sincronizacion
```

Si nos fijamos cuanso seleccionamos la **trama 3** se creo un corchete
encerrando a 3 paquetes indicando que estos funcionan en conjunto

![08-laboratorio-1-paso-3](./img/08-laboratorio-1-paso-3-handshake.PNG)

Y si vemos en la columna de info vemos que la primer trama tiene un **[SYN]**
el segundo un **[SYN, ACK]** y el tercero **[ACK]**, tambien si vemos
la comunicacion es entre 24.6.173.220 y 216.168.252.157, lo cual nos
indica que se trata de un **handshake**,

```r
24.6.173.220, 216.168.252.157  , 41865 → 80, [SYN]
216.168.252 . 157, 24.6.173.220, 80 → 41865. [SYN,ACK]
24.6.173.220, 216.168.252.157  , 41865 → 80, [ACK]
```

**Resumen** La IP 216.168.252.157 al hacer un handshake y por el puerto
80, inferimos que esta ip es un servidor.

![08-laboratorio-1-paso-3](./img/08-laboratorio-1-paso-3-topologia.PNG)

D. **Vamos a trama 6**

D-1. En nuestra capa de Red en este caso ethernet y de internet nos dice
lo mismo el paquete sale de la laptop y se dirige al broadcast.

D-2. En la **capa de tranporte en este caso UDP** vemos que los puertos
de origen y destino son los mismos **17500** y si investigamos este es
un **puerto propietario** que lo usa Dropbox (almacenamiento en la nube)

D-4. Si vemos la **capa de apliacion en este caso dropbox** Primero
Wireshark sabe que tipo de aplicacion es por el puerto propietario

```r
Ethernet...
  Destination: Broadcast (ff:ff:ff:ff:ff:ff) # no nos interesa por que es Broadcast
  Source: d4:85:64:a7:bf:a3 # LA LAPTOP QUE YA IDENTIFICAMOS

Internet...
  Destination: 255.255.255.255 # Broadcast
  Source: 24.6.173.220 # la laptop

User Datagram Protocol...
  Source Port: 17500
  Destination Port: 17500
```

E. **Vamos a trama 7**

E-1. **Ethernet II** Tenemos (c8:60:00:19:9e:19) que es un nuevo dispositivo que por el
OUI identificamos `AsustekC_19` (fabricante de dispositivos de red finales
osea, computadoras) por lo que inferimos que es una Laptop y que esta
conectada al mismo router (pasando por el switch).

E-2. **Internet Protocol**, Tenemos una IP desconocida y por propiedad
transitiva (la MAC origen del paquete en la capa de red es c8:60:00:19:9e:19
y la IP origen de la capa de internet es 24.6.169.43, entonces la ip
de c8:60:00:19:9e:19 es 24.6.169.43)

E-3. **Transmission Control Protocol**, tenemos que tiene una vandera
de tipo SYN por el puerto destino 80, lo que ya es indicio que este es
un handshake

```r
Ethernet II
  Destination: (00:01:5c:31:bb:c1) # router conocido
  Source: AsustekC_19:9e:19 (c8:60:00:19:9e:19)  # nueva Laptop(Asus)

Internet Protocol...
  Destination: 199.59.150.9 # nueva direccion IP desconocida
  Source: 24.6.169.43 # Ip de la nueva laptop (Asus)

Transmission Control Protocol
  Flags: 0x002 (SYN) # posible handshake y lo confirmamos por la
                     # interconexion de paquetes de la imagen de abajo
  Destination Port: 80      # nos indica que
  
```

Y si nos fijamos en el panel de arriba podemos confiimar que es un handshake

![08-laboratorio-1-paso-4](./img/08-laboratorio-1-paso-4.PNG)

**Resumen** Sabemos que la IP 199.59.150.9 es un servidor ya que resive
el paquete por el puerto 80 y hace un handshake, y que tenemos una nueva
Laptop que se comunica con el Switch ya conocido.

![08-laboratorio-1-paso-4-2-topologia.PNG](./img/08-laboratorio-1-paso-4-2-topologia.PNG)

F. **Vamos a trama 10**

Al seleccionar vemos que se nos marcan los corchetes, y de primera
instancia inferimos que es un handshake

![08-laboratorio-1-paso-4-2-topologia.PNG](./img/08-laboratorio-1-paso-5-tramas.PNG)

```r
10, 42.791267 24.6.169.43 107.21.109.41 TCP 66, 58405 → 443 [SYN]...
  Ethernet II...
    Source: AsustekC_19:9e:19 (c8:60:00:19:9e:19) # Laptop/asus
    Destination: Cadant_31:bb:c1 (00:01:5c:31:bb:c1) # rauter
  Internet Protocol
    Source: 24.6.169.43 # Laptop/asus
    Destination: 107.21.109.41 # nueva IP
  Transmission Control Protocol
    Destination Port: 443 # puerto-estandart http/s
    Flags: 0x002 (SYN) # posible handshake
```

----

### 11. Tráfico de red típico y de fondo

**Ejemplo de trafico Tipico:** Podria ser alguien encargado de analizar
el trafico de una navegacion web, como es el de resolver un caso de
que alguien no puede o le carga lento un dominio.
Una vez sabiendo que tipo de analisis vamos a hacer, ya sabemos cual
es la estrategia a tomar, en nuestro ejemplo, sabemos que vamos a ver
una peticion a DNS para que la maquina cliente conosca la IP del servidor
que tiene el dominio, luego una petiion TCP para hacer un handshake, y
por ultimo empezara el intercambio de contenido.

A. Abrimos la [captura](./img/09-traficoTipico-y-fondo.pcapng) el cual
es la navegacion de alguien que entro a google.com

en nuestro primeros paquetes vemos que se hico una peticion de quien es
google.com y se respondio.

![09-trafico-dns](./img/09-trafico-dns.PNG)

Lo podemos ver mas detallado en

```r
Paquete 1
  Domain Name System (response)
    Queries
      www.google.com: type A, class IN

Paquete 2
  Domain Name System (response)
    Answers
      www.google.com: type A, class IN, addr 74.125.224.80
      www.google.com: type A, class IN, addr 74.125.224.81
      ...
```

Luego vemos el handshake, lo podemos ver por el protocolo TCP y su orden
de banderas

![09-1-trafico-handshake](./img/09-1-trafico-handshake.PNG)

**Ejemplo Trafico de fondo** En las capturas que hemos abierto tienen
muy pocos paquetes, lo cual en la realidad casi nunca va a pasar esto,
ya que nos vamos a encontrar con mucho otro trafico que no, nos interesa
analisar ya sea por procesos automaticos de nuestro pc, por ejemplo,
una actualizacion de windows, puede pasarnos que queremos analisar el
envio de un correo, pero esta tambien vamos a capturar el trafico de
la actualzacion

A. Abrimos la [captura](./img/10-trafico-de-fondo.pcapng)

**para distinguir el trafico de fondo** si vemos el primer paquete
vemos que tenemos una IP de clase A (Source: 24.6.173.220) que lo mas
seguro es una IP de una PC, y una IP publica (67.217.65.244) que esta
fuera de nuestra LAN.

B. Buscamos en **whois.domaintools.com** la IP que desconocemos (67.217.65.244)
y veremos que nos da el siguiente dato.

```r
OrgName:        LogMeIn, Inc.
```

Buscamos "LogMeIn, Inc." en google y descubrimos que esta organizacion
se trata de una empreza de webinars, lo cual inferimos que el paquete
es enviado de forma automatica por un software de este estilo, y como
no estamos analizando eso entonces no es algo que nos intereze y por
lo tanto es trafico de fondo

**Resumen** Es importante saber identifica el trafico de fondo y tipico
para simplificar nuestro analisis

### 12. Laboratorio 2 - identificar trafico de fondo

Vamos a capturar el trafico de nuestra red mientra no hacemos nada con
nuestro equipo por unos 2 min, esto para identificar nuestro trafico de
fondo.

### 13. Archivos de captura de tráfico

Los nativos de wireshark tenemos los `.pcap` y los `.pcapng` este ultimo
es New Generation el cual es una actualizacion con mucho mas metadata,
y de otros fabricantes tenemos muchos

----

## Personalizacion

Veremos como la personalizacion de la UI nos facilitara el trabajo como

- Personalizacion de columnas
- Guardar cambiar perfiles de UI y pasarlo a otras PC
- Disectores
- Resaltar URLs del trafico capturado
- importar perfil para la deteccion de errores HTTP y DNS
- Detectar problemas de latencia

----

### 16. Columnas

**Objetivos:**

- Entender por que nos conviene personalizar columnas
- agregar quitar ordenar columnas

A. Abrimos [11-columnas.pcapng](./img/11-columnas.pcapng)

Podemos ver bastante informacion en las columnas del primer panel,
por ejemplo

![12-columnas](./img/12-columnas.PNG)

En el paquete 1 podemos ver que sale un paquete de 24.6.173.220 dirigido
a 75.75.75.75, con un protocolo DNS y que se trata de un "Standard query
A www.google.com" que dice ¿cual es la ip de ese dominio?

![12-columnas](./img/13-info-de-golpe-solo-con-columnas.PNG)

**Objetivo**> Obtener los sockets de origen y destino de todos los
paquetes TCP del archivo de captura (ip:puerto) de paquetes de origen
y de destino

A. En nuestro caso queremos tener a la mano la IP y el puerto, para eso

1. seleccionamos el paquetee
2. Buscamos el valor en el panel de enmedio que nos interesa
3. Click der y "Apply as column"

**Objetivo**: exportar nuestro primer panel de informacion como .csv

Vamos a File -> Export packet direction -> as .csv

----

### 17. Lab. 4 Resaltar URLs

A. Abrimos [14-resaltar-urls](./img/14-resaltar-urls.pcapng)

B. Seleccionamos el primer paquete http y en el 2ª panel veremos

```r
Hypertext Transfer Protocol
  Host: www.disney.com\r\n
```

Agregamos el host como columna y damos clic sobre la columna para
ordenar por el valor de la columna.

![15-resaltar-urls](./img/15-resaltar-urls.PNG)

----

### 18. Disectores

Los desectores son herramientas encargadas de pasar el binario a texto
entensible por nosotros, por ejemplo, lo hace para mostrarnos la ip,
puerto, protocolo etc, y la forma de saber que disector usar es por medio
del puerto.

![16-concatenacion-capas-TCP](./img/16-concatenacion-capas-TCP.PNG)

Podemos ver representado cada nivel como se va construyendo el paquete
en el cual se van agregando los cuadros de data con la capa superior
hasta llegar al final para marcar la cabezera y la cola al paquete. Y
el proceso pasa a la viceversa, donde como va pasando el paquete por
las capas le va quitando la informacion al paquete, para ahorrarle
trabajo a la capa de arriba.

----

### 19. Puertos no-estándar

Vamos a entender como Wireshark determina cual es el protocolo de la capa
de aplicacion cuando se utilizan puertos no estandart, por ejemplo, para
una peticion http no usamos el puerto 80, usamos uno personalizado, por
lo que Wireshark tendra que determinarlo de otra forma.

**Asignacion de disectores** como mensinavamos arriba el disector se
asigna por medio del puerto que se utilize, osea, estamos confiando que
la comunicacion use un puerto estandart

Por lo dicho entonces al no usar un puerto standart wireshark deberia de
asignar un disector erroneo y fallar en la convercion binario->texto,
incluso **por seguridad** se diseña a grede el no usar un puerto standart
pero wireshark usa 3 metodo para asignar el puerto

1. **Metodo estatico**, el que ya comentamos.
2. **Metodo Heuristico**, al fallar la descodificacion se Compara con todos
   los disectores hasta que coinsida con uno, pero aun puede ocurrir que
   ninguno coincida.
3. **Metodo manual**, Nosotros se lo asignamos, el primer inidicio de
   que el uso de disector fallo es que el protocolo del paquete es TCP.

**Objetivo:** Vamos a asignar un disector que wireshark fallo al asignarlo
de forma manual. Escenario, estamos capturando trafico de la empresa
en que trabajamos y sabemos que se usamos un puerto distinto para FTP
por seguridad, asi que debemos de asignar el disector de forma manual
al que sabemos que realmente es.

A. Abrimos  [17-asiganar-disector-manualmente](./img/17-asiganar-disector-manualmente.pcapng)

B. Click der. sobre el primer paquete y **"Decode As"** y de la ultima
columna seleccionamos FTP

![18-asignar-disector](./img/18-asignar-disector.gif)

**Resultado** vemos como cambia la informacion que se nos muestra por
una mas util.

----

### 20. Ajustes de Visualizaicón

**Objetivo** Entender las ventajas de personalizar la UI de Wireshark y
personalizar la UI.

Cambiamos la interface en `Edit -> Preferenses... -> Appearance`

En `Edit -> Preferenses... -> Capture` tenemos la casilla que dice si
vamos a capturar los paquetes que no vayan dirigido a nosotros.

En `Edit -> Preferenses... -> Name Resolution` tenemos la opcion de
convertir cosas como la OUI de la MAC lo pase al nombre del fabricante,
o que nla IP la pase a nombre del dominio, por ejemplo, de logar de
mostrarnos xxx.xxx.xxx.xxx nos mostrara dominio.com.

En `Edit -> Preferenses... -> Name Resolution -> MaxMind database dir...`
aqui podemos conectar con un mapa para poder ver de que ubicacion geografica
estan llegando las peticiones.

**Objetivo** De un paquete http vamos a ver el contenido del archivo

A. Abrimos [19-evitar-TCP-segment-of-a-reassembled-PDU](./img/19-evitar-TCP-segment-of-a-reassembled-PDU.pcapng)

B. Vemos que a partir de nuestro paquete 10 tenemos un mensaje
"TCP segment of a reassembled PDU"

![20-evitar-TCP-segment-of-a-reassembled-PDU](./img/20-evitar-TCP-segment-of-a-reassembled-PDU.PNG)

esto nos lo muestra ya que en `Edit -> Preferenses... -> Protocols -> TCP`
tenemos marcada la opcion **Allow subdissector to ressemble TCP steams**
que hace que veamos las cabecera de aplicacion como data pero este no
siempre es el caso, ya que en el caso de HTTP ahi cabezeras que son
propias de la capa de aplicacion. pero es cierto que **la mayoria** de
cabezeras de aplicacion solo traen data.

C. Vamos a `Edit -> Preferenses... -> Protocols -> TCP` y apagamos la
opcion de **Allow subdissector to ressemble TCP steams** y ya podremos
ver las cabezeras propias del protocolo HTTP.

![21-evitar-TCP-segment-of-a-reassembled-PDU](./img/21-evitar-TCP-segment-of-a-reassembled-PDU.PNG)

Y ta podremos ver las cabezeras de HTTP en este caso un "Status code 200"
entre otros datos como cookie, cache etc.

----

### 21. Lab. 5, Detectar Offloading

**checksums** Es lo que nos permite que confrmar que un paquete sea
integro

El Offloading ocurre cuando los checksums son calculados por la targeta
de red (NIC), justo antes de ser transmitidos por el cable.

Wireshark captura los paquetes antes de ser enviados por la NIC, por lo
tanto, no vera el checksum correcto por que aun no lo ha calculado. Cada
capa calculara su propio checksum.

A. Abrimos [22-Offloading-y-checksum](./img/22-Offloading-y-checksum.pcapng)

![23-Offloading-y-checksum](./img/23-Offloading-y-checksum.PNG)

Veremos que varios paquetes estan pintados de negro con letras rojas
esto significa que tienen algun error, y mas abajo veremos que dice
**"may be caused by IP checksum offload"**.

Que es lo que comentabamos en este caso wireshark no puede calcular los
checksums lo cual nos dice **los paquetes no estan integros**.

![24-quitar-offloading-y-checksum2](./img/24-quitar-offloading-y-checksum2.gif)

para quitar esto debemos de desactivar el **validate the IPv4 checksum if possible**
en ambos campos ya que los dos dan el mismo error.

Tambien si vemos mas abajo tenemos el error de PDU que vimos antes, que
significa que no podemos ver las cabeceras de la capa de aplicacion, ya
que wireshark por defecto cree que esta solo trae data, pero no siempre
es asi.

![25-evitar-TCP-segment-of-a-reassembled-PDU-facil](./img/25-evitar-TCP-segment-of-a-reassembled-PDU-facil.PNG)

Para desactivalo lo hacemos igual que lo hicimos recien

![25-evitar-TCP-segment-of-a-reassembled-PDU-facil-2](./img/25-evitar-TCP-segment-of-a-reassembled-PDU-facil-2.PNG)

Y ya podrmos ver que se trata de una cabezera http

![25-evitar-TCP-segment-of-a-reassembled-PDU-facil-3](./img/25-evitar-TCP-segment-of-a-reassembled-PDU-facil-3.PNG)

----

### 22. Perfiles

Los perfiles son las diferentes configuraciones que hicimos por ejemplo
en el laboratorio anterior, y guardarlos nos sera util por ejemplo para

- Tener diferentes perfiles para diferentes tipos de analisis, por
  ejemplo, un perfil para analizar HTTP y otro perfil para analizar FTP

Los perfiles los podemos ver en la parte inferior derecha de Wireshark

![26-perfiles](./img/26-perfiles.PNG)

----

### 23. Lab. 6, Crear perfil para HTTP

A. Abrimos [27-perfiles](./img/27-perfiles.pcapng)

B. Vamos a perfiles y creamos un nuevo perfil copiando el perfil default
con `perfiles (abajo-der) -> click Der. -> manage perfiles -> seleccionar default -> botos de abajo -> copy this perfil -> OK`

C. Vamos a `edit -> preferences -> aparences -> Layout` y seleccionamos
el layout penultimo

![28-crear-perfil-1](./img/28-crear-perfil-1.PNG)

D. Resolvemos las IPs como host en `Edit -> Preferenses... -> Name Resolution`

E. Y quitamos el segment-of-a-reassembled-PDU para que se vean las
cabezeras HTTP

![25-evitar-TCP-segment-of-a-reassembled-PDU-facil-2](./img/25-evitar-TCP-segment-of-a-reassembled-PDU-facil-2.PNG)

**Resultado:** Tenemos la siguiente vista que nos facilita el analisis
de HTTP

![28-crear-perfil-2](./img/28-crear-perfil-2.PNG)

----

### 24. Archivos de configuración

Vamos a aprender como acceder al archivo donde se guardan las config.
de nuestro wireshark, los cuales tenemos 2.

1. Configuracion global = Global configuration
2. configuracion personal = Personal configuration

A. Vamos a `help -> about wireshark -> folders` y podremos ver todas
las carpetas que usa y podremos ver los 2 nombres que nos interesa

![29-ver-carpetas-configuracion](./img/29-ver-carpetas-configuracion.PNG)

----

### 25. Lab. 7 importar perfil

**Objetivo** Vamos a descargar un perfil que nos ayudara a detectar
errores HTTP y DNS

A. Abrimos [30-importar-configuracion](./img/30-importar-configuracion.pcapng)

![30-importar-configuracion](./img/30-importar-configuracion.PNG)

B. Vamos a donde vemos las carpetas de configuracion y abrimos Config.
personal en mi caso `C:\Users\subject51\AppData\Roaming\Wireshark`
entramos a `perfiles` y copiamos la carpeta que tenemos en [31-configuracion-detectar-errores-dns-http.zip](./img/31-configuracion-detectar-errores-dns-http.zip)

![31-importar-configuracion-seleccionar.PNG](./img/31-importar-configuracion-seleccionar.PNG)

Y los paquetes deverian de cambiar de la siguiente manera.

![32-importar-configuracion-resultado](./img/32-importar-configuracion-resultado.PNG)

En donde los que estan de color es donde tenemos errores

----

### 26. Detección de latencia y sus tipos

Tenemos diferentes tipos de latencias como es

**LATENCIA DE RECORRIDO**: Aqui no sabemos si la latencia es al llegar
o al mandar el paquete, podemos verlo reprecentado por la linea punteada,
entre mayor sea significa mayor tiempo.

![33-latencia-de-recorrido](./img/33-latencia-de-recorrido.PNG)

**LATENCIA DE CLIENTE** Puede ser causada por usuario, aplicacion o
falta de recursos, debemos de saber diferenciar entre una letancia
natural del usuario mientras realiza la accion y una latencia ajena a
la del usuario, **es la menos comun**.  
Aqui vemos un ACK que va al servidor indicando que resivimos la data y
una linea punteada que termina cuando reacciona el cliente. **Ejemplo**
A) puede que el usuario al llegarle una lista de opciones tarda un rato
natural al seleccionar una de estas, o B) donde el servidor envia la
respuesta y llega al cliente pero esta tarda en llegar o mostrase.

![33-latencia-de-cliente.PNG](./img/33-latencia-de-cliente.PNG)

**LATENCIA DE SERVIDOR**: Puede ser falta de recursos u optimizacion del
lado del servidor, Nos damos cuenta por que la peticion y su respectivo ACK
se hacen rapido, pero la respuesta tarda en llegar.

Ejemplo, "enviame foto 1", "Ok te la mandare", "despues de tiempo
considerable llega la foto"

![34-latencia-de-servidor](./img/34-latencia-de-servidor.PNG)

**Practica**. Aprenderemos a detectar latencia en una captura limpia

A. Abrimos [35-detectar-latencia](./img/35-detectar-latencia.pcapng)

En nuestra columna de tiempo nos indica el tiempo entre el paquete n y
el primer paquete, lo cual no nos sirve para saber el tiempo entre el
paquete n y el paquete m.

B. **Cambiar la prcentacion del tiempo**, para esto vamos a
`view -> Time display format -> time since previus Displayed Packet`

C. Ordenamos la columna de mayor a menoy y veremos que el primer
paquete es un handshake (por el `[SYN, ACK]`) y este coincide con la
imagen que tenemos **latencia de recorrido**.

![35-latencia-en-handshake](./img/35-latencia-en-handshake.PNG)

**PERO** si nos fijamos esta es una captura limpia no tenemos ruido de
otras conexiones, lo cual es poco realista y en un escenario en donde
hay ruido **no nos serviria** este metodo ya que podriamos el paquete
anterior del paquete que nos interesa podria ser uno que no este
relacionado con el trafico, dondonos datos erroneos.

**Practica-2**. Aprenderemos a detectar latencia en una captura con
ruido. Lo cual aprenderemos como ver el tiempo que tardo en llegar el
paquete con respecto al ultimo paquete que llego del mismo stream.

A. Abrimos [36-detectar-latencia-con-ruido.pcapng](./img/36-detectar-latencia-con-ruido.pcapng)

B. Abrimos un paquete TCP y damos click en `Time since previous frame`
derecho y la mandamos como columna.

```r
Transmission Control Protocol...
  [Timestamps]
    [Time since previous frame...
```

Y si ordenamos por mayor a menor tiempo (no la que acabamos de agregar)
veremos que los que se tardaron son paquetes que tienen las vanderas
`[ACK, y FIN, ACK]` lo que nos dice que son peticiones en donde el
usuario aun no a mandado request, lo que significa que es **LATENCIA DE CLIENTE**

![37-latencia-usuario](./img/37-latencia-usuario.PNG)

c. Ahora ordenamos por `Time since previous frame in this TCP stream`
(la columna que agregamos apenas), y veremos lo siguiente.

**Los primeros 6 paquetes** que tardan unos `18seg` son peticiones
`http/GET` lo que mas seguro significa que el usuario las pidio con
destiempo, por ejemplo, son imagenes de Facebook y estas son peticiones
que se hicieron segun hacia Scroll, los paquetes que tardan unos `5seg`
tienen las banderas `[FIN, ACK]`, lo que significa que igual son
peticiones iniciadas por el usuario por lo que es **LATENCIA DE CLIENTE**.

**Nota**: Los paquetes `[FIN]` son usados para terminar una conexion,
por ejemplo, cuando lleva un tiempo sin actividad el navegador envia
este paquete para cerrar la conexion

**Los paqutes 7 y 8** son peticiones `http OK` lo que significa que se
termino de enviar alguna respuesta y son `casi 2seg` lo que es mucho,
osea este es un caso de **LATENCIA DE SERVIDOR**.

----

### 27. Lab. 8. Detectar latencia y su causa

A. Abrimos [38-causa-latencia](./img/38-causa-latencia.pcapng)

![38-detectar-latencia](./img/38-detectar-latencia.PNG)

**Paquete No. 354**, este paquete no nos preocupa ya que es de tipo
`[FIN, ACK]` lo que significa que fue una conexion que se quedo abierta
y algun proceso envio este paquete para terminarla por lo que es un
caso de **LATENCIA DE CLIENTE**.

**Paquetes No. 210, 34, 30, 23**, Si nos preocupa por que es un
`HTTP 200` lo que significa que es **LATENCIA DE SERVIDOR**.

**Moraleja**. Saber cual es el tipo de latencia es facil solo debemos
de recordar la teoria (images) de la clase anterior

----

----

----

## Sección 4: Captura

- Como lidiar con demaciados paquetes
- Como capturar solo el trafico que nos interesa
- Filtrar la captura por nodos
- Filtros de captura enfocado a aplicaciones
- Capturas en anillo, que nos ayudaran a hacer capturas al vuelo
- Capturar trafico de los demas, quitando el nuestro
- Crear, guardar y aplicar filtro de captura para DNS

----

### 29. Demasiados Paquetes

Al capturar trafico por defecto wireshak nos trae todo pero esto es
demaciado trafico, en lo cual nos puede pasar

- Nuestro analisis se nos complica mas
- Ahi tanto trafico que nuestra interface no puede y empieza a capturar
  paquetes incompletos

Tenemos diferentes soluciones pàra evitar que esto pase

**FILTROS DE CAPTURA:** con estos definimos cual es el trafico vamos a
capturar, ej. capturar el trafico de una maquina en especifico en un
protocolo en especifico.

**SET DE ARCHIVOS:** Cuando ahi un problema dificil de replicar, ya sea
porque solo pasa bajo ciertas condiciones que aun no entendemos, entonces
dejamos una maquina capturando por un tiempo (talvez horas) pero con la
opcion de que cree **sets de archivos de 100MB**, que es el tamaño maximo
recomendable, y que cada X archivos (ej. 5) sustituya el 1º archivo y
para una captura ciclica o igual llamado **BUFFER EN ANILLO** sin
ocupar tanto espacio en disco, ya que podriamos crear archivos de uno
o varios GBs y estos no son manejables o requieren muchos recursos.

**DETENCION AUTOMATICA:** Es cuando conocemos las condiciones de
cuando detener nuestro analisis, como seria que, se cumpla X tiempo,
se detecte x paquete, se apague un servidor, etc.

----

### 30. Técnicas de Captura

Vamos a caputrar errores esporadicos osea no sabemos por que y cuando
va a fallar pero sabemos que fallara tarde o temprano. Asi que vemos a
capturar evitando el problema que mensionamos en "set de archivos".

**Objetivo, Captura en anillo**, vamos a configurar wireshark para que
capture en archivos

- Vamos al boton "capture interfaces" seleccionamos la interface que
  uerramos y vamos a la pesnaña **Output** donde vemos a settear las
  configuraciones que mensionavamos.
  - Seleccionamos carpeta y su nombre de archivo
  - marcamos "Create a new file automatically after.." y podremos
    configurar por cantidad de paquetes, tamaño de archivo, tiempo y
    en multiplo de tiempo, por ejemplo cada hora.
  - Marcamos "use a ring buffer with" con 3 archivos, que es lo que
    nos permitira el buffer en anillo

![39-captura-en-anillo](./img/39-captura-en-anillo.PNG)

Le damos en `start` y veremos como al terminar el tiempo que le dimos
en mi caso 10seg, empezara a capturar de nuevo y se creara un nuevo
archivo y si hay mas de 3 borrara el mas antiguo.

![40-capruta-en-anillo](./img/40-capruta-en-anillo.gif)

Para navegar entre ellos en wireshark navegaremos podemos ir a
`File -> File Set -> List Files` y podremos escojer el archivo que
nos interese.

----

### 32. Reducir el Tráfico Capturado

**Objetivo Coceptual:** Vamos a entender como contrarestar la captura
execiva de paquetes, o en otras palabras, reducir el ruido de nuestro
analisis, o incluso que nos lance un **error Dropped** que significa
que la interface no puede capturar la gran cantidad de paquetes de
nuestro equipo, ya que al no poder capturar todos los paquetes de un
switch arrojara **ACKed: Lost segment and previous segment not captured**.
<!-- capturar trafico de todas las computadoras, capturar trafico de todas los equipos-->
Para capturar todo el trafico de la red se necesita configurar un SPAN
en el switch, y ageegar un hardware llamado **Full duplex Tap ò TAP Inteligente**

----

### 33. Capturar Tráfico de Algunos Nodos

Objetivos

- Conceptual: entender como funciona la sintaxis BPF y los filtros de
  captura basados en las direcciones MAC/IP
- Operativo: Aprender a como aplicar filtros de captura basados en
  direcciones

**Sintaxis BPF:** Berkeley Packet Filter

- Una exprecion consiste de una o mas primitivas
- Una primitiva consiste de una ID (nombre [ej. TCP] o numero [ej.
  una IP]), precesido por un calificador
- Existen 3 tipos de calificadores: (Type, Dir, Proto)

**Trafico hacia/desde una direccion IP especifica** veremos unos
ejemplos de este estilo, donde vemos

En el 1, tenemos solo una primitiva donde host es el nombre y la IP es
el numero

|No.| Filtro BPF                           | Descripcion                       |
|---|--------------------------------------|---------------------------------- |
| 1 | host 192.168.1.20                    |  Captura trafico hacia/desde IPv4 |
| 2 | host 1234:5678:910a:cdef:a1a2       |  Captura trafico hacia/desde IPv6 |
| 3 | not host 192.168.1.20                |  Captura todo el trafico menos el de la ip indicada hacia/desde IPv4 |
| 4 | src host 192.168.1.20                |  Captura el trafico desde |
| 4 | dst host 192.168.1.20                |  Captura el trafico hacia |
| 4 | host 192.168.1.20 or host 192.168.1.20  |  Captura hacia/desde de las dos IPv4 indicafas |
| 4 | host www.cnn.com                     |  Captura hacia/desde la url |

**Trafico hacia/desde un rago de direcciones IP**: Veremos como captuar
el trafico de un rango de IPs

- `net 192.168.1.0/24`: Captura trafico hacia/desde cualquier IP de la
  subred
- `net 192.168.1.0 mask 255.255.255.0`: Lo mismo de arriba sintaxis
  diferente
- `ipv6 net 1234:5678:910::/64`: Lo mismo de arriba sintaxis diferente

**Trafico de direcciones IP Multicast/Broadcast**.

- `ip broadcast` capturar el trafico hacia 255.255.255.255
- `ip multicast` capturar el trafico hacia 255.255.255.255

**Trafico de direccion MAC especifica**.

- `ether host 12:34:56:78` Capturar trafico hacia/desde MAC
- `ether src host 12:34:56:78`: Capturar trafico desde MAC
- `ether dst host 12:34:56:78`: Capturar trafico hacia MAC
- `not ether host 12:34:56:78`: Capturar todo el trafico menos el de MAC

**Practica:** Vamos a capturar el trafico filtrando para que solo
veamos el trafico hacia/desde un servidor DNS en este caso 8.8.8.8.

A. Seleccionamos la interfas de donde capturar y ponemos el filtro y
le damos en iniciar

![40-poner-filtro-de-captura](./img/40-poner-filtro-de-captura.PNG)

y hacemos un ping desde una shell y deveriamos de ver el trafico que
va y viene, en este caso vemos como se enviaron 4 pings y se enviaron
y recivieron 8 paquetes como resultado de este.

![41-solo-capturar-trafico-de-IP](./img/41-solo-capturar-trafico-de-IP.PNG)

----

### 34. Lab. 10, Capturar trafico de los demas nodos

Para eso usamos el siguiente filto: `not host 192.168.1.72` en mi caso
esa es mi IP

----

### 35. Capturar Tráfico de una Aplicación

Con aplicación nos referimos al puerto estandart que usan las
aplicaciones, por ejemplo,

- TCP-80=HTTP
- UDP-P53->DNS
- UDP-67=DHCP
- TCP-21 = FTP

Ejemplos

- `port 53`: Capturar trafico UDP/TCP del puerto 53
- `udp port 67` para capturar el trafico DHCP
- `portrange 1-80` Capturar trafico de los puertos 1 a 80
- `tcp portrange 1-80` Capturar trafico de puertos 1 a 80 protocolo
  TCP
- `port 20 or 21` Transferencias FTP donde 21 es para comunicacion y
  20 para latency
- `host 192.168.1.60 and port 80`  capturar transferencia del host x
  en puerto x
- `udp src port 68 and udp dst port 67`  Trafico DHCP desde nodo
- `udp src port 67 and udp dst port 68`  Trafico DHCP desde servidor

----

### Lab. Usar filtro de captura DNS, Guardar filtro

Usamos `udp port 53`, udp es cuando la peticion se hace de cliente a
servidor, y para guardarlo lo hacemos dando click en el icono que esta
al lado de donde metemos el filtro.

![42-guardar-filtro](./img/42-guardar-filtro.PNG)

### Reto

`ether host 68-5D-43-51-B4-27 or tcp port 80`

----

----

----

## Seccion 5: Visualizacion

- Filtros de visualizacion y como se diferencian con los de captura
- Filtrado en capa de aplicacion y transporte
- Filtrar trafico de un nodo
- Expandir filtros
- Aplicar filtros de una manera mas agil
- Filtrar conversacion especifica en la capa de transporte
- Revicion de filtro aplicado, para que no sea ambiguo
- Filtrar una palabra y resaltarla

----

### 38. Filtros de Visualización

**Diferencia entre filtro de captura y visualizacion:** Uno es para que
no lo capture y asi poder evitar saturacion y fallas de captura, y el
de visualizacion

Filtros protocolo: `arp, ip, ipv6, tcp, bootp, dns, tftp, http, icmp`

Filtros campo: `http.host, ftp.request.command, bootp.option.hostname`

Operaciones de comparacion

![56-operadores-comparacion](./img/56-operadores-comparacion.PNG)

----

### 39. Filtrado en la Capa de Aplicación

Por ejemplo si queremos visualizar un trafico de navegacion a una web
podriamos usar `http` pero esto nos ocultaria la comunicacion del
handshake que ocurre en tcp por el puerto 80, osea `tcp.port==80`,
para poder ver ambas, tanto http como tcp en el puerto 80, lo cual es
util en este caso que queremos ver el handshake.

Pero tambien tenemos filtros que hacen lo mismo, por ejemplo `dns` y
`udp.port == 53` ambos mostraran el mismo resultado, lo debemos de
entender bien como funcionan los protocolos en sus diferentes capas
para usar el que nos convenga o el que se nos facilite.

----

### 40. Filtrar Tráfico para un Nodo

Filtrar la IP

- `ip.addr==192.168.0.1` - Efecto Filtrar la IP 192.168.0.1 (Origen o
  Destino)
- `!ip.addr==192.168.0.1` - Filtrar todo menos la IP 192.168.0.1
  (Origen o Destino)
- `ipv6.addr==1234:5678:90ab:cdef` - Filtrar la IPv6 X
- `ip.src==192.168.0.1` - Destino Filtrar la IP de Origen 192.168.0.1
- `ip.dst==192.168.0.1` - Filtrar la IP de Destino 192.168.0.1
- `ip.host==www.algo.com` - Filtrar el hostname wvvw.cnn.com
- `ip.addr==192.168.0.0/24` - Filtrar la subred 192.168.0.0/24 
   (Origen o Destino)
- `!ip.addr==192.168.0.0/24` - Filtrar todo menos la subred 192.168-0.0/24 
  origen o Destino

----

### 40. Expandir Filtros

El ejemplo que tenemos en el `or` es toda la navegacion web tanto http
como https

![56-concatenadores-filtros.PNG](./img/56-concatenadores-filtros.PNG)

----

### 42. Aplicar Filtros de Manera Rápida

Lo podemos hacer click sobre el

![57-aplicar-filtro-repido](./img/57-aplicar-filtro-repido.PNG)

Tambien tenemos la opcion `prepare as filter` este genera el filtro
arriba pero no lo aplica, tendremos que darle enter para que se aplique-

----

### 43. Filtrar una Conversación TCP/UDP

Vamos a entender como identificar una conversacion en medio de una
captura con multiples conversaciones, el cual podemos aplicar 3 metodos

- Conversation filter UDP TCP
- Follow TCP UDP Stream

**Que es un Stream** por ejemplo tenemos una descarga, spotify y
navegando por la red, cada uno de estos ocurre en un steam diferente
para que no ahiga problemas, **es trabajo de la capa de transporte**

**Objetivo:** Capturar solamente el stream que nos interesa

**Conversation filter UDP TCP** lo podemos hacer identificando un
handshake y le damos `trama -> clickDer -> Conversation Filter ->TCP`
tamvien podemos darle sobre IPv4 que filtrara por IP, o ethernet que
filtrara por MAC, pero tendremos el trafico de estas capas, que muchas
veces no nos interesara.

![58-filtrar-stream-1](./img/58-filtrar-stream-1.PNG)

y se nos generara el filtro `(ip.addr eq 24.6.173.220 and ip.addr eq 209.133.32.69) and (tcp.port eq 21213 and tcp.port eq 80)`

**Follow TCP UDP Stream** Lo mismo que el anterior le damos click der
sobre la trama y damos `Follow` el hara un re-ensamblado de la
conversacion y nos data por ejemplo el html de la comunicacion http,
el cual lo veremos con mas profundidad en su seccion.

![59-re-ensamblado-paver-stream](./img/59-re-ensamblado-paver-stream.PNG)

**Statistics** Aqui nos puede dar un resumen de cuantas conversaciones
tenemos en este archivo, qu esta en `Statistics -> conversations`.

![60-conversaciones-tcp](./img/60-conversaciones-tcp.PNG)

Podemos ver que ahi multiples converzaciones (streams) el cual puede
significar varias cargas de pagina en un mismo host o multiples. y
desde aqui podemos aplicar un filtro.

----

### 44. Revisión de Filtro Aplicado

En la barra donde ingresamos los filtros se pueden colorear de 3 colores

- verde: todo ok
- rojo: incorrecto
- amarillo: la sintaxs es correcta pero los resultados pueden ser los
  no esperados o **ambiguo**

**Resultado ambiguo** por ejemplo `ip.addr != 75.75.75.75` el filtro es
correcto pero, pero no va a haber convercaciones en nuestro equipo que
la ip de origen y de destino sea la misma IP, y lo que hace es mostrarnos
aquiellas que solamente una de las dos (origen destino) no sean esa IP.

----

### 45. Filtrar una Palabra

tenemos dos formas usando `contains` o `matches`, por ejemplo, queremos
encontrar la palabra `clientes` ya que sabemos que nuestro analisis
involocra esta palabra, para esto

Buscar exactamente (case sensitive): `frame contains abuscar`

Buscar sin case sensitive: `frame matches FeedbAcK`

----

----

----

## Seccion 6: Analisis

- Reglas de coloreado: veremos que regla de coloreado a sido aplicada a un paquete
- Crear nuestras propias reglas de coloreado
- ScrollBar inteligente
- Exportar los paquetes d interes, para simplificar el proceso de analisis
- identificar quien habla con quien facilmente
- Identificar a Top-talkers
- GeoIP para referenciarlas con su ubicacion geografica
- Listar todas las aplicaciones en el archivo de captura
- Graficar el ancho de banda ocupado

----

### 47. Reglas de Coloreado

Aprenderemos a identificar que regla de coloreado se a aplicado

A. Seleccionar un paquete e ir a

```r
Frame 1
  [Coloring Rule Name: UDP]
```

lo que indica el paquete que este coloreado de `azul`, es que esta
asociado al protocolo de transporte la cual incluye al DNS y otros
protocolos

Para un coloreado `verde` vemos que tiene

```r
Frame 1
  [Coloring Rule Name: HTTP]
```

Para ver todas las reglas que tiene cradas wireshark por defecto vamos
a `view -> Coloring rules` y veremos lo sig.

![42-guardar-filtro](./img/43-reglas-de-coloreado.PNG)

----

### 48. Crear Reglas de Coloreado

En el mismo lugar donde podemos ver las reglas imagen de arriba,
tambien vamos a crearlas

**Objetivo** Vamos a crear un fitro de colores para identificar la
latencia.

A. Abrimos [44-pa-crear-regla-coloreado](./img/44-pa-crear-regla-coloreado.pcapng)

B. Vamos a `view -> Coloring rules` y creamos una nueva regla
`tcp.time_delta > 1` y como color de fondo uno azul, y marcamos la
casilla para que se aplique.

![46-aplicar-reglas-de-coloreado](./img/46-aplicar-reglas-de-coloreado.PNG)

**Resultado:** Vemos como se aplican las reglas de coloreado a aquellas
columnas que se cumple la cindicion que agregamos

----

### 50. Exportar Paquetes de Interés

Aprenderemos a crear un archivo compuesto por los paquetes que si nos
interesa analizar, esto nos sera util para cuando queramos compartir
los paquetes con colegas, o para un reporte.

**Objetivo:** Crear un archivo de los paquetes que sean http/get.

A. Seleccionamos un paquete de este estilo, y le damos click derecho
sobre el valor que queremos

```r
Hypertext Transfer Protocol
  GET /viewvc/trunk-1.6/ HTTP/1.1\r\n
    Request Method: GET
```

B. `Click-Der->Apply As Filter->Selected` y nos quedara solo los
paquetes con esa caracteristica

C. `File -> Export Specified Packets` y guardamos y en la parte
superior se aplicara una regla: `http.request.method == "GET"`

**Resultado** Tenemos un archivo con los paquetes que nos interesa.

**Objetivo** Pero en caso de que queramos seleccionar algunos con
caracteristicas no compartidas, entonces podemos seleccionarlo con el
mause uno por uno

A. Sobre el paquete que nos interesa `ClickDer->Mark Packet` ya
seleccionados vamos a `File -> Export Specified Packets` y en la parte
de abajo marcamos `marked packages`

----

### 51. Quién está Hablando con Quién

Vamos a aprender dentro de una captura saber cuales son los host y
como se relacionan entre ellos.

A. En cualquier aechivo de captura vamos a `Statistics -> Conversations`

![47-quien-habla-con-quien](./img/47-quien-habla-con-quien.PNG)

Tambien podemos ver datos como la duracion, tamaño, etc.

----

### 52. Identificar a Top-Talkers

**Top-Talkers** son aquellos que hacen el mayor intercambio de
paquetes y poder indentificar cosas como, si la red esta lenta quien
es el que esta concimiendo la Red.

Hacemos lo mismo que la anterior practica y ordenamos por Bytes

![48-quien-consume-mas](./img/48-quien-consume-mas.PNG)

----

### 53. GeoIP

Aprenderemos a como utilizar una DB para relacionar a las direcciones
IP publicas con su ubicacion geografica

A. Abrimos [49-geo-ip.pcapng](./img/49-geo-ip.pcapng)

B. Descargamos las ubicaciones de las IPs lo cual podemos hacer desde
www.maxmind.com, el cual deberemos de registrarnos, en mi caso la
descarge desde [aqui](https://www.udemy.com/course/experto-en-wireshark/learn/lecture/18486256#overview)

C. El descargado lo cargamos en `Edit -> Preferences -> Name Resolution -> MaxMind database directories`
y le podemos la carpeta donde descargamos las ubicaciones.

D. Vamos a `Statistics -> Endpoints` y en las ultimas columnas podremos
ver la ubicacion de las IPs

![50-geo-ips](./img/50-geo-ips.PNG)

----

### 54. Listar Aplicaciones de Red

Ver facilmente todas las aplicaciones que hay en el archivo de captura

A. Vamos a `Statistics -> Protocols Hierarchy` y podremos ver dividido
por porsentage y su consumo en bytes cada protocolo de aplicacion.

![51-aplicaciones-y-su-consumo](./img/51-aplicaciones-y-su-consumo.PNG)

----

### 55. Graficar el Ancho de Banda Ocupado

A. Vamos a `Statistics -> I/O Graph` y podremos ver una grafica de
consumo bytes respecoto a tiempo y podemos cambiar el tipo de grafica
con respecto a bites, agregar formulas para ver el consumo de X
servidor

![52-grafica-ancho-de-banda](./img/52-grafica-ancho-de-banda.PNG)

----

----

----

## Seccion 4: CapturaSección 7: Reensamblado

- Reensamblado HTTP
- Reensamblado de un archivo transferido via FTP, reconstruir una
  imagen que fue enviada en paquetes.
- Añadir comentarios al archivo de captura, para dejar notas sobre
  nuestra investigacion

----

### 57. Reensamblado HTTP

Aprender como ver la conversacion entre nodos (cliente-servidor) desde
la perspectiva de la capa de aplicacion, **En el 80% de los casos**
vamos a querer ver que es lo que esta pasando en la capa de aplicacion,
ya que realmente lo que hacen las capas fisica, transporte e internet
es prestar un servicio a la capa de aplicacion, y esta capa es donde
se dan los servicios entre computadores.

A. Abrimos [50-re-ensamblado](./img/53-re-ensamblado.pcapng)

B. Damos click sobre un paquete HTTP y entramos al siguiente

![53-re-ensamblado-paso-1](./img/53-re-ensamblado-paso-1.PNG)

C. Y nos mostrara la siguiente estructuracion

![53-re-ensamblado-paso-2](./img/53-re-ensamblado-paso-2.PNG)

Esta pintado en `rojo`, lo que nos indica que enviamos el contenido.

En otro caso podemos ver en `azul` el contenido que hemos resivido.

![53-re-ensamblado-paso-3](./img/53-re-ensamblado-paso-3.PNG)

**Resultado** Re-ensamblamos el archivo que enviamos por http y como
podemos ver es el codigo html que el cliente esta viendo.

----

### 58. Reensamblado de un Archivo Transferido vía FTP

Nuestra proxima captura fue hecha cuando se hizo una transferencia de
un archivo via FTP

A. Abrimos [54-re-ensamblado-de-archivo-por-ftp](./img/54-re-ensamblado-de-archivo-por-ftp.pcapng)

Lo primero que vemos es un handshake, le damos un `follow TCP` y veremos
la autenticacion y que el usuario esta pidiendo un archivo llamado `pantheon.jpg`

B. Buscamos el paquete cuando se pide `pantheon.jpg` que es el 34 abajo
seleccionamos el primero que sea un protocolo `FTP-DATA`

![54-re-ensamblado-de-archivo-por-ftp-1](./img/54-re-ensamblado-de-archivo-por-ftp-1.PNG)

Veremos la siguiente codificacion el cual tiene al inicio un `JFIF` lo
que nos indica que

C. Cambiamos el `Show and save data as: Raw` y lo guardamos con el
mismo nombre con el que se pidio el archivo, si le ponemos otro nombre
no va a funcionar

![54-re-ensamblado-de-archivo-por-ftp-resultado](./img/54-re-ensamblado-de-archivo-por-ftp-resultado.PNG)

**Resultado:** Podemos ver la imagen que capturamos del trafico.

----

### 59. Añadir Comentarios al Archivo de Captura

Para agregar comentarios lo tenemos en la parte izq abajo

![55-agregar-comentarios](./img/55-agregar-comentarios.PNG)

Y cuando le damos guardar en save se agregara, esto se utiliza para
agregar comentarios a la captura en general.

Para agregar un comentario a un paquete en particular lo hacemos desde
click-der sobre el paquete y `packet commet`, y lo podemos ver en el
segundo paquete. Y para verlo en especifico podemos dar click sobre el
circulo verde igualmente en la parte abajo izq.

----

### extraer imagenes/archivos de captura de carga de un sitio

Tenemos 2 maneras de hacer esto

1. En la captura ir a `File -> Export Objects -> http`

2. Guardar la captura como `.pcap` y cargarlo con un programa externo 
   como [NetworkMiner](https://www.netresec.com/index.ashx?page=NetworkMiner)

![61-captura-de-imagenes-sitio](./img/61-captura-de-imagenes-sitio.PNG)

## Critica que di al curso

Excelente curso para introducirse a Wireshark de 0 a medio-avanzado

me gusta

- Los laboratorios donde se explica por medio de la practica
- Que maneja un apartado teórico-conceptual para a la hora de hacer la
  practica se entienda la imagen general o para que se hace
- Que tiene tareas
- El instructor esta atento a comentarios y preguntas

Me parece que le falta contenido mas avanzado como

- Detección de intrusos
- usar llave privada para des-cifrar un trafico https
- Captura de trafico remoto
- uso de Wireshark por CLI creo TShark
- Un apéndice de lecturas recomendadas y próximos pasos
- También me parece que falta un escenario real mas completo, en mi
  caso como desarrollador, me gustaría ver el proceso para capturar,
  por ejemplo, el trafico de android a php y php a mysql y viceversa
  o algo parecido