# Bases de Datos PostgreSQL

- [Bases de Datos PostgreSQL](#bases-de-datos-postgresql)
  - [Introduccion](#introduccion)
    - [postgresql.conf](#postgresqlconf)
    - [comandos basicos](#comandos-basicos)
  - [Table-Space](#table-space)
  - [Funciones en texto](#funciones-en-texto)
  - [backups](#backups)
    - [Creacion](#creacion)
    - [Restauracion](#restauracion)
    - [Importar backup](#importar-backup)
  - [Algebra relacional](#algebra-relacional)
    - [UNION](#union)
    - [INTERSECCION](#interseccion)
    - [DIFERENCIA](#diferencia)
    - [Producto Cartesiano - JOIN](#producto-cartesiano---join)
      - [NATURAL JOIN](#natural-join)
      - [INNER JOIN](#inner-join)
      - [LEFT/RIGHT JOIN](#leftright-join)
      - [FULL JOIN](#full-join)
  - [Modelo Entidad Relacion](#modelo-entidad-relacion)
    - [Cardinalidades](#cardinalidades)
    - [Ejemplo diagramar E/R](#ejemplo-diagramar-er)
    - [Diseño logico Modelo Relacional - M/R](#diseño-logico-modelo-relacional---mr)
    - [Restricciones y Violaciones](#restricciones-y-violaciones)
  - [SQL](#sql)
    - [DML Lenguaje de manipulacion de Datos (Data manipulation Lenguage)](#dml-lenguaje-de-manipulacion-de-datos-data-manipulation-lenguage)
    - [DCL: Lenguaje de control de acceso de a datos (Data control Lenguage)](#dcl-lenguaje-de-control-de-acceso-de-a-datos-data-control-lenguage)
  - [Clausula SELECT a fondo](#clausula-select-a-fondo)
  - [Lenguaje Procedurales](#lenguaje-procedurales)
    - [Uso de procedimientos almacenados vs aplicacion de servidor (Java, node, phython etc)](#uso-de-procedimientos-almacenados-vs-aplicacion-de-servidor-java-node-phython-etc)
    - [Ejemplo 1](#ejemplo-1)
  - [Trigers](#trigers)
    - [Ejemplo 1\_](#ejemplo-1_)
    - [Ejemplo 2](#ejemplo-2)
    - [Ejemplo 3 1/2](#ejemplo-3-12)
    - [Ejemplo 3 2/2](#ejemplo-3-22)
  - [Normalizacion / Formas Normales](#normalizacion--formas-normales)
  - [Proceco de Normalizacion](#proceco-de-normalizacion)
    - [1 Hacemos preguntas](#1-hacemos-preguntas)
    - [2 Encontramos las Dependncias Funcionales de esta tabla](#2-encontramos-las-dependncias-funcionales-de-esta-tabla)
    - [3. Resumimos](#3-resumimos)
    - [3. Separamos entidades](#3-separamos-entidades)
    - [4. Repetimos preguntas](#4-repetimos-preguntas)
  - [Bases de datos no relacionales](#bases-de-datos-no-relacionales)
  - [Investigacion sobre Versionado en bases de datos](#investigacion-sobre-versionado-en-bases-de-datos)
  - [Cosas que me voy encontrando](#cosas-que-me-voy-encontrando)
    - [feachas](#feachas)
    - [Ejecutar script desde consola](#ejecutar-script-desde-consola)
    - [Cambiar idioma de postgres (espaniol-ingles)](#cambiar-idioma-de-postgres-espaniol-ingles)
    - [Cerrar conexiones](#cerrar-conexiones)
    - [Uso de expreciones regulares](#uso-de-expreciones-regulares)
    - [Marcar tabla como eliminada y recuperarla](#marcar-tabla-como-eliminada-y-recuperarla)
    - [Migrar columna de varios valores a otra tabla en columnas](#migrar-columna-de-varios-valores-a-otra-tabla-en-columnas)
    - [Checar si existe](#checar-si-existe)
      - [una tabla](#una-tabla)
      - [Una secuencia](#una-secuencia)
      - [Una Base de datos](#una-base-de-datos)
    - [Comparar dos columnas de diferentes tablas](#comparar-dos-columnas-de-diferentes-tablas)
    - [Unir varias filas en una columna separadas por coma (mucho-a-muchos/3-tablas)](#unir-varias-filas-en-una-columna-separadas-por-coma-mucho-a-muchos3-tablas)
    - [Copiar columna a otra tabla](#copiar-columna-a-otra-tabla)
    - [Ver el tamaño de una columna](#ver-el-tamaño-de-una-columna)
  - [Errores](#errores)
    - [Error al agregar columna con default](#error-al-agregar-columna-con-default)
    - [Error uso de secuencia](#error-uso-de-secuencia)

## Introduccion

- Características de PostgreSQL.

  1.1. Es codigo abierto con una licencia muy libre

  1.2. Es 100% ACID = Atomicidad, Consistencia, Aislamiento, Durabilidad

  1.3. OS Soportados: Windows, Unix, macOS

  1.4. Notificaciones en tiempo real por medio de: Lisen, Notify, Unlisten

  1.5. Objetos en forma JSON: Podemos guardar nuestra BD en formato JSON

<!-- ubicacion de configuracion, archivo de confiuracion, ruta instalacion, carpeta instalacion-->
  1.6. En linux la carpeta por default es '/var/lib/postgresql/14/data/'

----

### postgresql.conf

aqui tenemos diferencites configuraciones como.

1,1, `listen_addresses = '*'` : Aqui podemos indicar que maquinar que maquinas
se pueden conectar a nuestro servidor por defecto esta que todos, pero por seguridad
deberiamos liminar por ejemplo `listen_addresses = '192.168.0.6, 192.168.0.5'`

<!-- puerto por defecto de postgress, puerto default de postgres, -->
1.2. `port = 5432` El puerto.

1.3. `max_connections = 100` Numero de conexiones

1. **pg_hba.conf**: Este archivo nos permite la auth del cliente
donde podemos indicar

```r
que tipo de conexion - a que base de datos - con que usuario - desde que maquina/s - metodo de auth
# host       DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
# hostssl    DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
```

**Ejemplo:** Si olvidamos la pass de postgres=root podemos cambiar este archivo
para que nuestra maquina se pueda conectar sin pass a root a todas las BD y
con esto poder resetear la pass.

```r
host  all        postgres    127.0.0.1/32      trust

# nos conectamos indicando el -h para que conecte por ipv4
```

Cambiamos la contraseña con: `ALTER ROLE postgres PASSWORD '12345'`

Borramos la linea que pusimos del host ... trust y reiniciamos, y ya podremos
conectarnos con la nueva pass.

```r
# ver estatus del servicio
service postgresql status

# detener servicio
service postgresql stop
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### comandos basicos

*, Conectarse a Postgress: `psql -U <usuario> -h 127.0.0.1 -d <nombre_bd>`

<!-- evitar que psql pida contraseña, pre definir contraseña, evitar pregunta de contraseña, usar uri para conectarse -->
*,* Conectase via uri: `psql postgresql://username:password@dbmaster:5433/mydb?sslmode=require`

*, Ver ayuda de los meta comandos de postgres: `\?`

<!-- enlista bases de datos, ver bases de datos, listar bases dey -->
*, Ver bases de datos creadas: `\l`

<!-- crear bd -->
*, Crear base de datos (Postgres): `CREATE DATABASE <midb> OWNER <usuario> TABLESPACE <tlb_misdatos;>`

*, Eliminar base de datos: `DROP DATABASE "HolaPg";`

<!-- entrar a base de datos, seleccionar base de datos, cambiar de base de datos -->
*, Entrar a una base de datos: `\c nombre_Base`

*,*, Listar usuarios, mostrar usuarios, ver usuarios: `\dg`

*,*, cambiar de usuario en terminal, usar otro usuario, cambiar usuario: `\c nombre_bd nombre_usuario`

*,*, limpiar pantalla: `\! clear`

<!-- crear llave primaria, -->
*,*, Crear Tabla: `CREATE TABLE productos(productoId INT PRIMARY KEY, nombre VARCHAR(40) NOT NULL, precio NUMERIC, id_fabrica INT NOT NULL);`

*,*, Crear Tabla con ID autoincremental:Serial aumentara 1 en 1.
`CREATE TABLE usuarios(id SERIAL PRIMARY KEY, nombre VARCHAR(40) NOT NULL, apellido VARCHAR(40),_activo BOOLEAN NOT NULL);`

*, Mostrar todas las tablas y sequencias en la BD, el simbolo + es opcional el cual nos da mas info.

<!-- mostrar tablas listar tablas show tables en postgres, ver tablas creadas-->
- Mostrar cosas en la BD: `\d`
- Mostrar tablas en la BD: `\dt`

*, Mostrar detalles de una tabla: `\d+ <nombre_table>`

*,*, Uso de secuencias donde por defecto ira 1 en 1 pero le podemos mover.

```sql
CREATE SEQUENCE seq_turno;
INSERT INTO usuarios(nombre, apellido, _activo, turno) VALUES('Sutano', 'Colima', true, nextval('seq_turno'));
```

*, insertar registro: `INSERT INTO productos VALUES(1, 'Trompo', 20.00, 1);`

*,*, NOTA: Al parecer no acepta commillas dobles solo simples.

*,  Actualizar: `update usuarios set turno=1 where nombre='Mario';`

*,  Eliminar: `DELETE FROM products WHERE price = 10;`

*,  Renombramiento: `SELECT identificacion as id, nombre as nom, direccion as dir from clientes;`

<!-- tipo de datos, data types, datatypes, typedata, type data -->
*,  Tipos de datos en postgres: [Fuente](https://www.postgresql.org/docs/9.2/datatype.html)

*,*, Uso de `%TYPE`: Este nos permite copiar un tipo de dato que asinamos
en una tabla, es util para declarar nuestro procedimeintos almacenados
que deben de estar omologado con un datos.

ejemplo

```r
DECLARE
   mi_variable nombre_tabla.nombre_columna%TYPE
```

15.2: Uso de `%ROWTYPE`: Este permite copiar una columna
en una especie de objeto con los diferentes tipos de datos donde
podriamos guardar el registro.

```r
DECLARE
   mi_registro nombre_tabla%ROWTYPE
```

*,*, Mostrar resultados verticalmente

```sql
\x  -- SE ACTIVA MOSTRAR VERTICALMENTE
select * from ...;
-[ RECORD 1 ]---+--------------------------
id_business     | 1
create_at       | 2021-05-14 19:12:41.311
description     | foto de archivo libre de
founding_date   | 1970-05-11 18:00:00
....
\x -- SE DESACTIVA MOSTRAR VERTICALMENTE
```

## Table-Space

Estos son directorios donde se encuantra nuestra informacion y con
estos podemos definir exactamente donde se guardara.

**Error:** no se pudo definir los permisos del directorio «E:/cursos/0activ/sql/a»: Permission denied
**Descripcion:** En windows Al querer correr el comando "CREATE TABLESPACE" me marca este error.
**Solucion;** TEMPORAL le damos permisos a todos en la carpeta queremos asignar con el siguiente
comando `icacls {{carpeta_a_usar}} /grant Subject51:(OI)(CI)F /T`

<!--crear tablespace crear espacio de tabla.-->
PostgreSQL:

```r
\h create tablespace
CREATE TABLESPACE tlb_misdatos LOCATION 'E:\cursos\0activ\sql\a';
```

Mysql: `CREATE TABLESPACE 'tlb_misdatos' ADD DATAFILE '/data/zabbix.ibd' Engine=InnoDB;`

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Funciones en texto

- `SELECT char_length('hola mundo');` Cuenta los caracteres de una cadena el cual la primera pos sera 1

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## backups

[Documentacion](https://www.postgresql.org/docs/9.1/app-pgdump.html)

### Creacion

- Fuente: [Link](https://www.postgresql.org/docs/8.1/app-pgdump.html)

<!-- hacer backup, crear backup, crear respaldo, hacer respaldo, creacion de respaldo-->

**--verbose**: Es para que nos saque el log.

Comunmente hacemor el respaldo con el usuario postgres pero igual se
puede con el que le pertenesca la bd.

```r
pg_dump --host 127.0.0.1 --port 5432 --verbose --username postgres --file backup_algebra_20190911.backup.sql nombre_bd
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Restauracion

1, Para restaurar el archivo desde la shell (sin meternos a psql) usamos
el siguiente comando

```r
psql -d <DB_NAME> -U <USER> -f <BACKUP_FILE>.out
```

**Nota:** Tiene los valores de el host localhost y el puerto a 5432 por defecto.  

```r
pg_dump --host 127.0.0.1 --port 5432 --verbose --inserts --column-inserts --username postgres --file backup_algebra_20190911.backup.sql nombre_bd
```

Si queremos respaldar solamente la estructura sin los datos

```r
pg_dump --host 127.0.0.1 --port 5432 --verbose --schema-only --username postgres --file backup_algebra_20190911.backup.sql nombre_bd
```

solamente los datos, sin schema, respaldar datos

```r
pg_dump --data-only --verbose \
  --port 5432 \
  --username postgres \
  --host 127.0.0.1 \
  --file backup_algebra_20190911.backup.sql \
  nombre_bd \

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Importar backup

<!-- Usar backup, Usar respaldo, importar respaldo, restaurar backup, restaurar base de datos -->

1, En el archivo buscamos a que usario lo esta asignando y si no lo tenemos lo creamos.

```sql
CREATE USER estudiante PASSWORD 'estudiante';
```

2, Creamos la base de datos, el tablespace no es obligatorio pero recomendable

```sql
CREATE DATABASE algebra OWNER = estudiante TABLESPACE = tlb_misdatos;
```

3, Nos conectamos a la bd con el usuario root y exportamos

```sql
\i backup_algebra_20190911.backup.sql
```

5, opcional: Si solo nos podemos conectar con el usuario
creado este debe de tener los permisos necesarios ya que podria
sacar el proximo **Error** debe ser dueño de la extensión plpgsql / must be owner of extension plpgsql
**Solucion:** desde postgres [Link](https://stackoverflow.com/questions/13410631/how-to-solve-privileges-issues-when-restore-postgresql-database)

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Algebra relacional

- Fuente: [Teoria de conujuntos](https://firebird21.wordpress.com/2013/08/06/teoria-de-conjuntos-union-interseccion-diferencia/)

- Se recomienda NO usar el \* en las consultas por temas de mejor
lectura, inmutabilidad y rendimiento.

- Se recomienda renombrar los campos **AS** para una mejor lectura.

![11_teoria_de_conjuntos](./img/11_teoria_de_conjuntos.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### UNION

Dos relaciones son compatibles en union si sus dominios
coinsiden en numero y en orden. Y es la disyujcion de los conjuntos.
Es decir, el elemento esta en uno o en otro conjunto.

**Estructura de un union**.

```sql
SELECT campo1, campo2, ... campoN
FROM tabla1
UNION [all]
SELECT otro, otro2, ... otroN
FROM tabla2
```

**Objetivo:** Crear una consulta union, de los siguientes tabls mostrar las ventas en contado y las ventas en credito.

```sql
-- 1. Creamos usuario
CREATE USER estudiante PASSWORD 'estudiante';

-- 2. creamos base de datos
CREATE DATABASE algebra OWNER = estudiante TABLESPACE = tlb_misdatos;

CREATE TABLE usuarios(
  id SERIAL PRIMARY KEY,
  nombre VARCHAR(40) NOT NULL,
  apellido VARCHAR(40),
  _activo BOOLEAN NOT NULL
);

INSERT INTO usuarios(nombre, apellido, _activo) VALUES('Mario', 'Marquez', true);
INSERT INTO usuarios(nombre, apellido, _activo) VALUES('Sutano', 'Xitlot', true);
INSERT INTO usuarios(nombre, apellido, _activo) VALUES('Mengano', 'Xitlot', false);
INSERT INTO usuarios(nombre, apellido, _activo) VALUES('Tacañin', 'Tacañon', false);
INSERT INTO usuarios(nombre, apellido, _activo) VALUES('temporal', 'a eliminar', false);


CREATE TABLE ventas_contado(
  id_ventas_contado SERIAL PRIMARY KEY,
  fecha date NOT NULL,
  total REAL NOT NULL,
  id_usuario INTEGER,
  CONSTRAINT      fk_ventas_contado_usuarios FOREIGN KEY(id_usuario)
              REFERENCES usuarios(id)
              ON UPDATE SET NULL ON DELETE SET NULL
);

INSERT INTO ventas_contado(fecha, total, id_usuario) VALUES('2019-01-24', 203.50, 1);
INSERT INTO ventas_contado(fecha, total, id_usuario) VALUES('2019-01-24', 303.90, 2);
INSERT INTO ventas_contado(fecha, total, id_usuario) VALUES('2019-01-25', 403,     3);
INSERT INTO ventas_contado(fecha, total, id_usuario) VALUES('2019-01-26', 503.20, 1);
INSERT INTO ventas_contado(fecha, total, id_usuario) VALUES('2019-01-27', 603,     1);
INSERT INTO ventas_contado(fecha, total, id_usuario) VALUES('2019-01-28', 703.60, 2);

--no aparce en la union por que se repite
INSERT INTO ventas_contado(id_ventas_contado, fecha, total, id_usuario) VALUES(12, '2019-01-28', 110, 2);

--ventas a cliente que vamos a eliminar
INSERT INTO ventas_contado(fecha, total, id_usuario) VALUES('2019-01-28', 666, 5);
INSERT INTO ventas_contado(fecha, total, id_usuario) VALUES('2019-01-28', 666, 5);


CREATE TABLE ventas_credito(
  id_ventas_credito SERIAL PRIMARY KEY,
  fecha_inicio DATE NOT NULL,
  fecha_pago DATE NOT NULL,
  total REAL NOT NULL,
  id_usuario INTEGER,
-- crear llave foranea, agregar llave foranea
  CONSTRAINT  fk_ventas_credito_usuarios FOREIGN KEY(id_usuario)
              REFERENCES usuarios(id)
              ON UPDATE SET NULL ON DELETE SET NULL
);

INSERT INTO ventas_credito(fecha_inicio, fecha_pago, total, id_usuario) VALUES('2019-01-24', '2019-02-24', 903.90,   1);
INSERT INTO ventas_credito(fecha_inicio, fecha_pago, total, id_usuario) VALUES('2019-01-24', '2019-02-01', 1003.90,  1);
INSERT INTO ventas_credito(fecha_inicio, fecha_pago, total, id_usuario) VALUES('2019-02-01', '2019-01-10', 1303.90,  1);
INSERT INTO ventas_credito(fecha_inicio, fecha_pago, total, id_usuario) VALUES('2019-01-27', '2019-02-14', 1903,      3);
INSERT INTO ventas_credito(fecha_inicio, fecha_pago, total, id_usuario) VALUES('2019-01-01', '2019-01-21', 21003.10, 3);
INSERT INTO ventas_credito(fecha_inicio, fecha_pago, total, id_usuario) VALUES('2019-01-17', '2019-02-04', 103,      2);

--no aparce en la union por que se repite
INSERT INTO ventas_credito(id_ventas_credito, fecha_inicio, fecha_pago, total, id_usuario) VALUES(12, '2019-01-01', '2019-01-28', 110,    2);

INSERT INTO ventas_credito(fecha_inicio, fecha_pago, total, id_usuario) VALUES('2019-01-01', '2019-01-21', 111, 5);
INSERT INTO ventas_credito(fecha_inicio, fecha_pago, total, id_usuario) VALUES('2019-01-17', '2019-02-04', 111, 5);

--eliminanos para que ahiga ventas sin usuario
DELETE FROM usuarios WHERE id=5;
```

para eliminar

```sql
DROP TABLE ventas_credito;
DROP TABLE ventas_contado;
DROP TABLE usuarios;
```

para ver datos

```sql
SELECT * FROM usuarios;
SELECT * FROM ventas_credito;
SELECT * FROM ventas_contado;
```

**Analisis:** CMo vemos ambas tablas coinsiden en sus columnas
exepto en la fecha_pago que tiene de mas la tabla ventas_credito
por lo que en la consulta debemos omitirla.

```sql
SELECT id_ventas_contado as id_venta, id_usuario, fecha, total FROM ventas_contado
UNION
SELECT id_ventas_credito as id_venta, id_usuario, fecha_pago as fecha, total FROM ventas_credito
ORDER BY id_usuario;
```

**Resultado:** Tenemos la siguiente consulta

```r
 id_venta | id_usuario |   fecha    |  total
----------+------------+------------+---------
        1 |          1 | 2019-01-24 |   203.5
        5 |          1 | 2019-01-27 |     603
        3 |          1 | 2019-01-10 |  1303.9
        4 |          1 | 2019-01-26 |   503.2
        1 |          1 | 2019-02-24 |   903.9
        2 |          1 | 2019-02-01 |  1003.9
        2 |          2 | 2019-01-24 |   303.9
        6 |          2 | 2019-01-28 |   703.6
        6 |          2 | 2019-02-04 |     103
       12 |          2 | 2019-01-28 |     110
        5 |          3 | 2019-01-21 | 21003.1
        4 |          3 | 2019-02-14 |    1903
        3 |          3 | 2019-01-25 |     403
(13 rows)
```

<!--teoria de conjuntos UNION: juntar dos conjuntos distintos -->
**Problema Resultado** Podemos ver que tenemos 13 columnas pero en cada
una tenemos 7 registros por lo que deberian de aparecer 14 registros.  
**Causa:**  si nos fijamos el que falta  es un registro de id 12 de total
110 el cual son iguales, y en las uniones no muestran resultados repetidos
por teoria de conjuntos.  
**Solucion** agregmaos la palabra reservada ```ALL``` delante de union para
que agrege todos los resultados, incluyendo repetidos.

``` sql
SELECT id_ventas_contado as id_venta, id_usuario, fecha, total FROM ventas_contado
UNION ALL
SELECT id_ventas_credito as id_venta, id_usuario, fecha_pago as fecha, total FROM ventas_credito
ORDER BY id_usuario;
```

**Objetivo:** Queremos saber los registros a que tabla pertenecen
en este caso, saber que ventas son al contado y que ventas son a
credito.  
**Sulucion:** Agregamos una columna con un valor estatico.

``` sql
SELECT id_ventas_contado as id_venta, id_usuario, fecha, total, 'Al contado' as tipo_venta FROM ventas_contado
UNION ALL
SELECT id_ventas_credito as id_venta, id_usuario, fecha_pago as fecha, total, 'A Credito' as tipo_venta FROM ventas_credito
ORDER BY tipo_venta;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### INTERSECCION

Dos relaciones son compatibles son compatibles en interseccion
si sus dominios coinciden en numero y orden. Y en conjuncion de
los conjuntos. Es decir, *el elemento tiene que estar en ambos conjuntos*.

**Objetivo:** Queremos saber los registros que son
iguales en dos tablas diferentes,
**por ejemplo** queremos saber que ventas son iguales ya
que para el negocio significa que se vendio un mismo productos
del mismo dia por lanzamiento de una oferta, pero los clientes
lo adquirieron al contado o a credito.

```sql
SELECT fecha as dia_oferta, total as oferta FROM ventas_contado
INTERSECT
SELECT fecha_pago as dia_oferta, total as oferta FROM ventas_credito;
```

Otra forma de aserlo seria usando un where con el condicional IN

```sql
SELECT fecha as dia_oferta, total as oferta FROM ventas_contado
WHERE (fecha, total)
IN (SELECT fecha_pago as dia_oferta, total as oferta FROM ventas_credito);
```

Resultado:

 ```r
dia_oferta | oferta
------------+--------
 2019-01-28 |    110
(1 row)
 ```

 <!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### DIFERENCIA

Dos relaciones son compatibles en diferencia si sus dominios coinciden
en numero y orden. Y es la diferencia de los conjuntos. *Es decir*, el
elemento tiene que estar en el primer conjunto, pero no en el segunto.

```sql
SELECT fecha as dia_oferta, total as oferta FROM ventas_contado
EXCEPT
SELECT fecha_pago as dia_oferta, total as oferta, 'Credito' FROM ventas_credito;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Producto Cartesiano - JOIN

Este es el mas utilizado y nos sirve para hacer combinaciones,
El producto cartesiano no exige la compatibilidad de los dominios
ni en numero ni en orden.

**CROSS JOIN**: Es la union de todos con todos, por si solo no es
util/utilizado pero los motores pueden aserlo de manera natural
por la algebra relacional que contienen.

Estructura

```sql
SELECT a.campo1, a.campo2, ...a.campoN,  b.campo1, b.campo2, ...b.campoN FROM tabla1 as a CROSS JOIN tabla2 as b;
```

----

#### NATURAL JOIN

Combina dos relaciones de manera natural entre la clave primaria de una
y la clave foreanea de otra como **condicion** las columnas por las
cuales queremos unirla deben de llamarse igual.

![12_natural_join.PNG](./img/12_natural_join.PNG)

Estructura

```sql
SELECT a.campo1, a.campo2, ...a.campoN,  b.campo1, b.campo2, ...b.campoN FROM tabla1 as a NATURAL INNER JOIN tabla2 as b;
```

----

#### INNER JOIN

Combina dos relaciones cuando se necesita especificar
el(los) atriburos con los que se conbinaran.

Estructura

```sql
SELECT a.campo1, a.campo2, ...a.campoN,  b.campo1, b.campo2, ...b.campoN
FROM tabla1 as a INNER JOIN tabla2 as b
ON a.campoN = u.campoN AND a.campoN = u.campoN... ;
```

**Objetivo:** Queremos saber las ventas de cada usuario en la
de ventas en contado;

```sql
SELECT vc.id_ventas_contado, vc.fecha, vc.total, vc.id_usuario,
       u.id, u.nombre, u.apellido
FROM ventas_contado as vc JOIN usuarios as u
ON vc.id_usuario = u.id
ORDER BY u.id;
```

**Resultado:** Vemos las ventas que tiene cada usuario en
otras palabras vemos la venta y a que usuario le pertenece
pudiendo ver su nombre y apellido.

```r
id_ventas_contado |   fecha    | total | id_usuario | id | nombre  | apellido
-------------------+------------+-------+------------+----+---------+----------
                 4 | 2019-01-26 | 503.2 |          1 |  1 | Mario   | Marquez
                 1 | 2019-01-24 | 203.5 |          1 |  1 | Mario   | Marquez
                 5 | 2019-01-27 |   603 |          1 |  1 | Mario   | Marquez
                 6 | 2019-01-28 | 703.6 |          2 |  2 | Sutano  | Xitlot
                12 | 2019-01-28 |   110 |          2 |  2 | Sutano  | Xitlot
                 2 | 2019-01-24 | 303.9 |          2 |  2 | Sutano  | Xitlot
                 3 | 2019-01-25 |   403 |          3 |  3 | Mengano | Xitlot
(7 rows)
```

----

#### LEFT/RIGHT JOIN

Combina dos relaciones realizando primero un Join y
luego añade todas aquellas filas de la relacion nombrada
en la parte izquierda de la sentencia que nos satisfacen
la combinacion con la relacion de la parte derecha.
Y para *RIGHT JOIN* aplica lo mismo solo que con los
registros de la derecha

![13_left_right_join](./img/13_left_right_join.PNG)

Estructura

```sql
SELECT a.campo1, a.campo2, ...a.campoN,  b.campo1, b.campo2, ...b.campoN FROM tabla1 as a
FROM tabla1 as a LEFT JOIN  tabla2 as b
ON a.campoN = u.campoN AND a.campoN = u.campoN;
```

**Ejemplo:** Queremos saber todos los usuarios e
idependientemente de que tengan o no ventas queremos
que se muestren.

```sql
SELECT u.id, u.nombre, u.apellido,
       vc.id_ventas_contado, vc.fecha, vc.total, vc.id_usuario
FROM  usuarios as u LEFT JOIN ventas_contado as vc
ON vc.id_usuario = u.id
ORDER BY u.id;
```

**Resultado:** Podemos ver los usuarios y sus ventas relacionadas
en este caso podemos ver que "Tacañin Tacañon" no tiene registrada
ninguna venta.

```r
 id | nombre  | apellido | id_ventas_contado |   fecha    | total | id_usuario
----+---------+----------+-------------------+------------+-------+------------
  1 | Mario   | Marquez  |                 4 | 2019-01-26 | 503.2 |          1
  1 | Mario   | Marquez  |                 1 | 2019-01-24 | 203.5 |          1
  1 | Mario   | Marquez  |                 5 | 2019-01-27 |   603 |          1
  2 | Sutano  | Xitlot   |                 6 | 2019-01-28 | 703.6 |          2
  2 | Sutano  | Xitlot   |                12 | 2019-01-28 |   110 |          2
  2 | Sutano  | Xitlot   |                 2 | 2019-01-24 | 303.9 |          2
  3 | Mengano | Xitlot   |                 3 | 2019-01-25 |   403 |          3
  4 | Tacañin | Tacañon  |                   |            |       |
(8 rows)
```

----

#### FULL JOIN

Combina dos relaciones realizando primero un inner join y
luego realiza un left join y por ultimo un right join, ya sea
que tengan una relacion o no.

![14_full_join.PNG](./img/14_full_join.PNG)

**Ejemplo**: Queremoa ver todas las ventas y usuarios
idependiente mente de que los usuarios no tengan ventas
o de que las ventas no tengan usuarios ya que en este caso
al eliminar un usuario la venta no se borra se setea a null.

```sql
SELECT u.id, u.nombre, u.apellido,
       vc.id_ventas_contado, vc.fecha, vc.total, vc.id_usuario
FROM  usuarios as u FULL JOIN ventas_contado as vc
ON vc.id_usuario = u.id
ORDER BY u.id;

id | nombre  | apellido | id_ventas_contado |   fecha    | total | id_usuario
----+---------+----------+-------------------+------------+-------+------------
  1 | Mario   | Marquez  |                 4 | 2019-01-26 | 503.2 |          1
  1 | Mario   | Marquez  |                 5 | 2019-01-27 |   603 |          1
  1 | Mario   | Marquez  |                 1 | 2019-01-24 | 203.5 |          1
  2 | Sutano  | Xitlot   |                 6 | 2019-01-28 | 703.6 |          2
  2 | Sutano  | Xitlot   |                 2 | 2019-01-24 | 303.9 |          2
  2 | Sutano  | Xitlot   |                12 | 2019-01-28 |   110 |          2
  3 | Mengano | Xitlot   |                 3 | 2019-01-25 |   403 |          3
  4 | Tacañin | Tacañon  |                   |            |       |
    |         |          |                 7 | 2019-01-28 |   666 |
    |         |          |                 8 | 2019-01-28 |   666 |
(10 rows)
```

**Resultado:** Podemos ver a "Tacañin Tacañon" aun que
no tenga ventas y las ventas con toral "666" son que
tengan usuario.

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Modelo Entidad Relacion

Los modemos entidad relacion son diagramas que han existido
desde los años 70.

![2_DiagramaEntidadRelacion](./img/2_DiagramaEntidadRelacion.PNG)

**Entidad**: Reprecenta un objeto/elemennto/entidad del universo de
discurso (del problema a resilver)

**Enrtidad Debil**: Reprecenta un objeto/elemento del universo de discurso
pero que necesita de una relacion con otra entidad para su
identificacion

**Relacion:** Reprecenta el tipo de ralacion entre entidades.

**Relacion Con dependencia de identificacion:** Reprecenta el tipo de
relacion que identifica la dependencia entre entidades.

**Conector o Rol**: Es la reprecentacion de la union entre las relaciones y las
entidades

**Atributos:** Reprecentan las caracteristicas de las entidades.

**Atributo simple y monovaluado:** Se identifica unicamente y tiene un solo valor. Por ejemplo
Persona tiene el atributo edad es simple por su tipo de datos en este caso entero y es monovaluado
por que con uno es mas que suficiente una persona no puede tener varias edades registradas.

**Atributo identificador principal:** Identifica la entidad/elemento como unico por ejemplo
el curp ya que es irepetible para cada persona.

**Atributo Identificador Alternativo:** por ejemplo Una persona puede tener un RFC el
cual igual es un identificador unico pero por fines del sistema no es el principal.

**Atributo Multivaluado:** Es un atributo que puede tener varios, por ejemplo telefonos ya que
una persona puede terner tel de oficina, hogar, personal etc.

**Atributo Opcional:** No es obligatorio en el sistema este puede ser nulo.

**Atributo Compuesto:** Necesitan mas de un atributo para identificarse, por ejemplo
el una direccion se compone de numero, calle, codigo postal.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Cardinalidades

Estas se;alan el numero maximo de ejemplares de una entidad asociados a una combinacion
de ejemplares de las otras entidades en la interrelacion, que puede ser 1 o N.

En este ejemplo estamos diciendo que una persona puede tener varios o ningun vehiculo
y que un vehiculo puede terner ninguno o varios dueños.

![3_DiagramaER_cardinalidades.PNG](./img/3_DiagramaER_cardinalidades.PNG)

----

**Relacion Recursiva:** Esta es una que hace relacion a si misma, por ejemplo
una persona puede tener de compañera a otra persona.

![4_DiagramaER_recursivo.PNG](./img/4_DiagramaER_recursivo.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Ejemplo diagramar E/R

Se requiere un sisitema de informacion que permita gestionar la informacion de los
Arriendos/Alquileres de las viviendas.

Este sistema debe contener la información del propietario de la(s) vivienda(s) y la
información que se debe contemplar para dicho propietario es: identificación, nombre
completo, teléfonos de contacto y opcionalmente el correo electrónico. La(s) vivienda(s)
deben tener la información de: código de la vivienda, dirección, la cual está compuesta
por: calle y número, la vivienda debe tener además una descripción y la cantidad de
habitantes. Cada vivienda se encuentra ubicada en una ciudad y dicha ciudad pertenece
a un estado. Se debe tener la información de los alquileres los
cuales tendrán la información de: fecha de inicio, fecha de fin, valor mensual.

Y también se requiere conocer la información del (los) inquilino(s), la información de
este debe ser: identificación, nombre completo y teléfonos.

![5_DiagramaER_ejemplo.PNG](./img/5_DiagramaER_ejemplo.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Diseño logico Modelo Relacional - M/R

En estos solo definimos la entidad con su atributos en el cual podemos
agregamar cosas como tipo de dato y tamaño. por ejemplo

![6_DiagramaMR_ejemplo](./img/6_DiagramaMR_ejemplo.PNG)

Su MR seria donde el \* (asterisco) significa que es opcional y para
campos compuestos es el nombre del campo compuesto guion bajo y el
sub-campo del valor compuesto ejemplo dir_calle, dir_numero.

```r
PERSONA(Nombre:cadena(50), Edad:entero(0 a 150), Estatura:decimal(0 a 2.90), \*Email_opcional:cadena(90), direccion_calle, direccion_numero)
```

Para **campos multivaluados** tenemos que crearlo como una nueva entidad
y para relacionarlo tenemos dos formas **1)** agregar el identificador en
la entidad que se genero por ser un valor multivaluado y agregar una
linea que los relacione esnte ellos. **2)** en lugar de la linea poner el
nombre el nombre de la entidad ambos ejemplos los tenemos en la imagen.

![6_DiagramaMR_ejemplo](./img/7_DiagramaMR_multivaluados.PNG)

De manera similar funciona para las relaciones, a continuacion se muestra.

![8_DiagramaMR_relaciones_1-n_n-m](./img/8_DiagramaMR_relaciones_1-n_n-m.PNG)

----

De nuestro Diagrama de entidad relacion anterior resultaria un Diagrama MR como
el siguiente.

![9_DiagramaMR_ER_a_MR](./img/9_DiagramaMR_ER_a_MR.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Restricciones y Violaciones

- **Restricción Nulo**: No permite que el valor del atributo sea nulo.

- **Restricción de Identidad:** No permite tener dos valores repetidos
en las claves primarias.

- **Restricción de Integridad de Entidad:** No permite tener valores
nulos en la clave primaria.

- **Restricción de Integridad Referencial:** No permite tener valores
en la clave foránea que no existan en la clave primaria. Sin embargo,
si permite tener valores nulos en la clave foránea.

- **Restricción de Clave Alterna:** No permite valores repetidos en la
clave alterna.

- **Restricción de Dominio:** Los datos deben ser del dominio específico.
Ejemplo: dominio de los números enteros comprendidos entre 0 y 18.

![10_vilacion_de_restricciones](./img/10_vilacion_de_restricciones.PNG)

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## SQL

El lenguaje de SQL se divide en tre grupos principales

1. **DDL**: Lenguaje de definicion de Datos (Data Definition Lenguage).
1.1. Crea, Modifica y borra los datos, podemos crear usuarios, bases de datos y tablas

- Crear usuario:

Postgress: `create user mario with encrypted password 'mario'`
Mysql: `CREATE USER 'sutano'@'localhost' IDENTIFIED BY 'secreto';`

- Crear usuario si no existe, en el caso de postgres debemos de

Postgress: `psql -U mario_admin -d usuarios_devops -tAc "SELECT 1 FROM pg_roles WHERE rolname='mario_admin'" | grep -q 1 || echo "no exite"`

tambien podemos hacerlo en forma de shell script

```bash
NEW_USR=$1
NEW_USR_PASS=$2

# VARIABLES
SUPER_USER="mario_admin"

# COMANDO DE EJECUCION
CMD_EXEC_SQL="psql -U $SUPER_USER -d usuarios_devops -tAc"

QUERY_SEACH_USR="SELECT 1 FROM pg_roles WHERE rolname='$NEW_USR'"

QUERY_CREATE_USR="CREATE USER $NEW_USR WITH ENCRYPTED PASSWORD '$NEW_USR_PASS'"

# EJECUCION

#  buscar si exite
USR_FINDED=$($(echo $CMD_EXEC_SQL) "$QUERY_SEACH_USR")

#echo $USR_FINDED
if [ -z "$USR_FINDED" ]; then
  #echo "Creando usuario"
  $(echo $CMD_EXEC_SQL) "$QUERY_CREATE_USR"
else
  echo "El usuario $NEW_USR ya existe"
fi

```

- Crear tabla extenso, crear tabla detalles, estructura crear tabla estructura, postgress, crear tabla con expreciones regulares, crear tabla con exprecion regular, estructura para crear tabla, estructura crear una tabla

```sql
CREATE TABLE nombre_tabla (
  columna1 tipo_dato,
  columna2 tipo_dato (tamaño),
  columna3 tipo_dato NOT NULL,
  columna4 tipo_dato DEFAULT valor_x_defecto,
  columna5 SERIAL,
  CONSTRAINT nombre_constraint1 CHECK (columnaN > valor and columaN < valor),
  CONSTRAINT nombre_constraint2 CHECK (columnaN ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$')
  CONSTRAINT nombre_constraint3 PRIMARY KEY (columnaN, …, columnaM),
  CONSTRAINT nombre_constraint4 UNIQUE (columnaN, …, columnaM),
  CONSTRAINT nombre_constraint5 FOREIGN KEY (columnaN, … columnaM) REFERENCES  otra_tabla(columnaN, … columnaM) ON DELETE acción ON UPDATE accion
)
```

ejemplo: De nuestro diagrama MR anterior podemos ver lo siguiente.

```sql
CREATE TABLE propietarios(
  identificacion  VARCHAR(12),
  nom_nombre      VARCHAR(100) NOT NULL,
  nom_apellidos   VARCHAR(100) NOT NULL,
  email           VARCHAR(200),
  CONSTRAINT      pk_propietario PRIMARY KEY (identificacion)
);


CREATE TABLE telefonos(
  id_propietario  VARCHAR(12),
  telefono        VARCHAR(20),
  CONSTRAINT      fk_un_propietario_tiene_varios_telefonos FOREIGN KEY(id_propietario)
            REFERENCES propietarios(identificacion)
            ON UPDATE RESTRICT ON DELETE RESTRICT
);


CREATE TABLE estados(
  estado_id    SMALLSERIAL,
  nombre       VARCHAR(50) NOT NULL,
  CONSTRAINT   pk_estados PRIMARY KEY(estado_id)
);


CREATE TABLE municipios (
  minicipio_id   SMALLSERIAL,
  nombre         VARCHAR(100) NOT NULL,
  estado_id      SMALLSERIAL,
  CONSTRAINT     pk_municipio PRIMARY KEY(minicipio_id),
  CONSTRAINT     fk_un_estado_tiene_varios_muni FOREIGN KEY(estado_id)
              REFERENCES estados(estado_id)
              ON UPDATE RESTRICT ON DELETE RESTRICT
);

CREATE TABLE viviendas(
  id_vivienda     SERIAL,
  dir_calle       VARCHAR(200) NOT NULL,
  dir_numero      SMALLINT NOT NULL,
  cant_habitantes SMALLINT NOT NULL,
  descripcion     TEXT NOT NULL,
  id_propietario   VARCHAR(12),
  minicipio_id    SMALLSERIAL,
  CONSTRAINT      pk_viviendas PRIMARY KEY (id_vivienda),
  CONSTRAINT      fk_n_viviendas_estan_en_1_municipio FOREIGN KEY (minicipio_id)
                      REFERENCES municipios(minicipio_id)
                      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT      fk_1_propietarios_tiene_n_viviendas FOREIGN KEY (id_propietario)
                      REFERENCES propietarios(identificacion),
  CONSTRAINT      ck_mex_diez_hab_por_viv CHECK (cant_habitantes < 10 AND cant_habitantes > 1)
);

```

2.1. Cambiar el nombre de la base de datos, cambiar nombre base de datos

<!-- renombrar base de datos-->

```sql
ALTER DATABASE nombre RENAME TO nuevo_nombre;
```

2.3. cambiar dueño de la base de datos, cambiar usuario a base de datos

```sql
ALTER DATABASE nombre OWNER TO nuevo_usuario;
```

2.4. Cambiar el tablespace de base de datos

```sql
ALTER DATABASE nombre SET TABLESPACE nuevo_tablespac
```

2.5. cambios en tablas

```sql
--agregar columana a tabla
ALTER TABLE nombre ADD COLUMN campoN tipo_dato (tamaño)

--eliminar columna a tabla
ALTER TABLE nombre DROP COLUMN campoN

--agregar propiedad not null a tabla, agregar not null a tabla
ALTER TABLE nombre ALTER COLUMN campoN SET NOT NULL

--modificar propiedad a tabla: Nota debe de cumplir con la propiedad para agregar la propiedad,
--ej. si algun campo tiene un null no podremos cambiarlo asta que todos los campos tengan algun valor
ALTER TABLE nombre ALTER COLUMN campoN DROP NOT NULL

--agregar llave foranea a tabla
ALTER TABLE nombre ADD CONSTRAINT nombre_constraint FOREIGN KEY (campoN, … campoM) REFERENCES otra_tabla (campoN, …, campoM) ON DELETE acción ON UPDATE acción

--aliminar constrain a tabla, aliminar restriccion a tabla
ALTER TABLE nombre DROP CONSTRAINT nombre_constraint

--agregar propiedad default a tabla, agregar default a tabla
ALTER TABLE nombre ALTER COLUMN campoN SET DEFAULT valor_x_defecto

--eliminar propiedad default a tabla, eliminar default a tabla
ALTER TABLE nombre ALTER COLUMN campoN DROP DEFAULT

--cambiar el tipo de dato, por ejemplo en un smallserial se desbordo, lo podemos cambiar a un serial u otro mas grande
ALTER TABLE nombre ALTER COLUMN campoN TYPE nuevo_tipo (tamaño)

--renombrar columna a tabla, renombrar columna tabla
ALTER TABLE nombre RENAME COLUMN columnaN TO columnaM

--renombrar tabla, cambiar nombre tabla
ALTER TABLE nombre RENAME TO nuevo_nombre
```

### DML Lenguaje de manipulacion de Datos (Data manipulation Lenguage)

- Son
  - INSERT
  - UPDATE
  - DELETE

uso de where, update detallado, detalles update detalles, update a fondo

```sql
Where:
  WHERE campoM = valorM (=, >, <, <>)
  WHERE campoM BETWEEN valor1 AND valor2
  WHERE campoM IN (valor1, valor2, …, valorN)
  WHERE campoM IN SELECT consulta
  WHERE campoM NOT IN…
```

----

### DCL: Lenguaje de control de acceso de a datos (Data control Lenguage)

- Son
  - GRANT
  - REVOKE

```sql
-- Agregar permisos a usuario sobre tabla
GRANT CONNECT ON DATABASE base_de_datos TO usuario;
GRANT SELECT ON tabla TO usuario;

--quitar permisos a usuario sobre tabla
REVOKE SELECT ON tabla TO plablo;

--eliminar usuario
DROP USER nombre;

--eliminar base de datos
DROP DATABASE nombre;

--eliminar tabla
DROP TABLE nombre;
```

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Clausula SELECT a fondo

[sql para la practica](./img/src/select_a_fondo.sql)

- Obtencion de fechas [doc](https://www.postgresql.org/docs/9.1/functions-datetime.html)

  - Ejemplo obtener solo el mes de x fecha:

```sql
SELECT date_part('MONTH', now());
-- Resultado
 date_part
-----------
    11
```

- Comparar con uso de funciones, en este ambos estamos pasando a
  mayus para no tener problemas con el case sensivite.

```sql
SELECT nombre, valor
FROM colores
WHERE upper(nombre) = upper('Azul');
```

- Uso de LIKE

  - Los que empiezonpos "A"

```sql
SELECT nombre
FROM nombres
WHERE nombre LIKE 'M%';
```

- Comodin de solo unos caracteres

```sql
SELECT nombre
FROM nombres
WHERE nombre LIKE 'Mari_%';
```

- Uso de BETWEEN, NOta este obliga que primero sea el numero mas bajo

```sql
SELECT nombre, edad
FROM nombres
WHERE edad BETWEEN 22 AND 30 ORDER BY edad;
```

- Buscar nulos

```sql
SELECT *
FROM colores
WHERE valor is null;
```

- Uso de EXIST, de pendiendo de que si retorne o no valores la sub-consulta
  retornara o no todos los datos

```sql
SELECT *
FROM colores
WHERE EXISTS (SELECT * FROM colores WHERE nombre='Azul');
```

- Uso de CASE

  - ejemplo 1

```sql
SELECT *,
  CASE WHEN precio > 100
    THEN 'Caro'
    ELSE 'economico'
  END
FROM productos;
```

- ejemplo 2

```sql
SELECT *,
  CASE
    WHEN precio < 9 THEN 'Barato'
    WHEN precio > 10 AND precio < 40 THEN 'Normal'
    ELSE 'Caro'
  END AS rango
FROM productos ORDER BY rango;
```

- Resultado ejemplo 2 uso CASE

```r
id |   nombre    | precio | vencimiento | rango
---+-------------+--------+-------------+-------+
 4 | Bonicula    |      1 | 2021-03-10  | Barato
 6 | Chocolate   |      5 | 2021-06-12  | Barato
 3 | Vols        |  220.5 | 2021-03-22  | Caro
 7 | Vols barato |  111.5 | 2021-03-22  | Caro
 1 | Yogurt      |   12.5 | 2021-01-12  | Normal
 5 | Fire        |     20 | 2021-05-12  | Normal
 2 | Paquetaso   |     20 | 2021-02-12  | Normal
```

- Uso de INTO y Operaciones matematicas en SELECT y Creacion de tablas temporales: En estas podemos
volcar informacion de otras tablas.

```sql
SELECT p.nombre, p.precio, v.descripcion, v.cantidad, v.cantidad * p.precio as total
INTO tmp_reporte_ventas
FROM productos as p INNER JOIN ventas AS v
ON p.id_producto = v.id_producto;
```

```r
  nombre   | precio |  descripcion  | cantidad | total
-----------+--------+---------------+----------+-------
 Yogurt    |   12.5 | Pa noviembre  |        4 |    50
 Yogurt    |   12.5 | Pa septiembre |        3 |  37.5
 Paquetaso |     20 | Pa enero      |        5 |   100
 Vols      |  220.5 | Pa julio      |        6 |  1323
 Bonicula  |      1 | Pa Junio      |        3 |     3
 Bonicula  |      1 | Pa agosto     |        7 |     7
```

- Uso de DISTINCT: Nos filtrara los registros filtrando
los registros repetidos para que no nos muestre estos.

```sql
SELECT DISTINCT *
FROM repetidos;
```

```r
  color1  |  color2           color1  |  color2
----------+----------    ---------+----------
r rojo     | azul          naranja  | azul
r rojo     | azul          vira     | lira
  amarillo | rojo          verde    | amarillo
  banco    | verde        rosa     | pink
  verde    | amarillo  >  amarillo | rojo
  negro    | negro        rojo     | azul
  naranja  | azul          banco    | verde
  azul     | azul          azul     | azul
r rojo     | azul          rojo     | verde
r rojo     | azul          negro    | negro
r rojo     | azul          (10 rows)
  rojo     | verde
  vira     | lira
  rosa     | pink
(14 rows)
```

- Uso de MAX, Uso de MIN, Uso de COUNT, Uso de SUM, Uso de AVG
  - ejemplo queremos saber los datos de cantidad
  
```sql
SELECT MAX(cantidad) AS Maximo, MIN(cantidad) AS minimo, COUNT(cantidad) AS veces, SUM(cantidad) AS total, AVG(cantidad) AS promedio FROM ventas;
```

- ejemplo: queremos saber la cantidad de los productos vendidos
e.o.p. saber cuantas veces se a vendido un producto

```sql
SELECT id_producto, SUM(cantidad) from ventas GROUP BY (id_producto);
```

- ejemplo: lo mismo de arriba pero mas info

```sql
SELECT p.id_producto, p.nombre, SUM(v.cantidad), SUM(v.cantidad)*p.precio AS total
from ventas AS v INNER JOIN productos AS p
ON p.id_producto = v.id_producto
GROUP BY p.id_producto, p.nombre, p.precio;
```

----

- **Uso de HAVING**

Este sirve para aplicar filtros o comparar sobre los grupos,
recordatorio el WHERE solo aplica comparaciones sobre registros asi
que cuidado cn comparar un grupo con where.
**Ejemplo1:** Queremoves ver solo aquellos productos que
se han vendido mas de X veces

```sql
SELECT p.id_producto, p.nombre, SUM(v.cantidad), SUM(v.cantidad)*p.precio AS total
from ventas AS v INNER JOIN productos AS p
ON p.id_producto = v.id_producto
GROUP BY p.id_producto, p.nombre, p.precio
HAVING SUM(v.cantidad) > 6
```

Resultado:

```r
            SIN HAVING                                   USANDO HAVING
 id_producto |  nombre   | sum | total       id_producto |  nombre  | sum | total
-------------+-----------+-----+-------     -------------+----------+-----+-------
           3 | Vols      |   6 |  1323                 4 | Bonicula |  10 |    10
           4 | Bonicula  |  10 |    10   >             1 | Yogurt   |   7 |  87.5
           2 | Paquetaso |   5 |   100      (2 rows)
           1 | Yogurt    |   7 |  87.5
(4 rows)
```

**Ejemplo2:** Encontrar los registros repetidos y mostrar
las veces que han sido repetipos con sus datos

```sql
SELECT color1, color2, COUNT(*) AS total
FROM repetidos
GROUP BY (color1, color2)
HAVING COUNT(*) > 1;
```

Resultado:

```r
 id |  color1  |  color2         color1 | color2 | total
----+----------+----------       -------+--------+-------
  1 | rosa     | violeta    >    rojo   | azul   |     2
  2 | negro    | cafe            (1 row)
  3 | rojo     | azul
  4 | rojo     | azul
  5 | amarillo | rojo
  6 | blanco   | rojo
  7 | verde    | amarillo
  8 | verde    | cafe
  9 | amarillo | amarillo
 10 | cafe     | cafe
(10 rows)
```

- **USO de LIMIT y OFFSET**

De una lista grande podemos indicar que nos muestre menos, por ejemplo
para el uso de paginacion.

<!-- limitar cantidad de registros, definir cantidad de registros a mostrar, indicar cuantos registros mostrar, limitar numero de registros a mostrar, indicar limite de cuantos registros mostrar-->

```sql
--mostramos los primeros 5
select * from repetidos LIMIT 5;

-- mostramos los ultimos 5
select * from repetidos LIMIT 5 OFFSET 5;
```

- **USO de Sub - Consultas**

Ejemplo: Queremos mostrar las ventas de aquellos productos
que tenrminen con "o"

```sql
SELECT * from ventas
WHERE id_producto IN (SELECT id_producto from productos WHERE nombre LIKE '%o');
```

Resultado:

```r
 id_ventas | descripcion | cantidad |   fecha    | id_producto
-----------+-------------+----------+------------+-------------
         3 | Pa enero    |        5 | 2019-03-01 |           2
        14 | Pa enero 2  |        5 | 2019-01-01 |           7
 ```

 <!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Lenguaje Procedurales

Este es homologo a los procedimientos almacenados y
postgres puede utilizar varios lenguajes procedurales
como es pl/pgsql (por defecto), pl/perl, pl/java, pl/phython
pl/javascript en el curso veremos el por defecto/estandart

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Uso de procedimientos almacenados vs aplicacion de servidor (Java, node, phython etc)

No es recomendable tener toda la logica en la base de datos, pero si es
recomendabe delegar ciertas tareas a esta.

En un escenario donde tendriamos toda la logica en la BD podriamos tener los
siguiente pros y contras en la base de datos.

- Pros
  - One place to contain all of the business logic
  - Possibly faster applications because multiple SQL queries and such can be performed in one "round trip" to the database
  - Trivial to make use of the stored procedures from multiple applications

- Cons
  - A DBA will be required for performance tuning
  - All developers will have to be very well versed in your particular SQL dialect(T-SQL, Pl/SQL, etc)
  - SQL code isn't as expressive and thus harder to write when covering higher level concepts that aren't really related to data
  - A lot more unnecessary load on the database

Pero como siempre se debe de encontrar el punto medio optimo, por ejemplo. en un login
donde debemos de saber 1) si existe el usuario 2) registro de auditoria, 3) saber los permisos del usuario
este proceso en una app tradicional costaria 3 peticiones, mientras que una con
procedimientos almacenados costaria una consulta.
Pero demas tareas como validar que los datos esten bien formateados, tener la logica de
seguridad, etc. esto no deberia de manejar la DB

![15_logic_on_stored_pro_vs_app_server](./img/15_logic_on_stored_pro_vs_app_server.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Ejemplo 1

<!-- Hola mundo procedimientos almecenados, hola mundo funciones postgres, Concatenar-->

```sql
--creamos funcion
CREATE OR REPLACE FUNCTION saludar() RETURNS CHARACTER VARYING AS
$$
DECLARE
  nombre CHARACTER VARYING;
BEGIN
  nombre = 'Mario';

  FOR i IN 1..10 LOOP
    RAISE NOTICE 'Iteracion numero: %', i;  --especie de log, tipo log, parecido a log
  END LOOP;

  RETURN 'Hola ' || nombre || ' desde postgres funciones ';
END;
$$
LANGUAGE plpgsql;

--ejecutamos
SELECT saludar() AS saludo;
```

**Cuidado:** Si creamos variables de entrada para la funcion
que se llaman igual que alguna columna de nuestra tabla podriamos
tener problemas por lo que se recomienda cambiarla almenos
con algun caracter.
<!-- funcion con parametros de entrada en funcion parametro de entrada, funcion con parametro de entrada,  -->
Ejemplo: Crear una funcion que aplique un descuento del 10%

```sql
CREATE OR REPLACE FUNCTION descuentoFijo(_total real) RETURNS real AS
$BODY$
DECLARE
  nuevo_valor real
BEGIN
  nuevo_valor = _total * .4;
  RETURN nuevo_valor;
END;
$BODY$
LANGUAGE plpgsql;
```

<!-- Postgress iteraciones, ciclos, ciclo for, iterar, iterar  -->
<!-- Postgress recorrer tabla, iterar tabla, iterar consulta, HAY PARA ORACLE -->
Ejemplo: Recorrer una tabla para aplicar cambios sobre ella
de forma iterativo.

```sql
CREATE OR REPLACE FUNCTION recorre() RETURNS SETOF ventas AS
$BODY$
DECLARE
  fila ventas%ROWTYPE;
BEGIN
  FOR fila IN SELECT * FROM ventas LOOP
    fila.descripcion = fila.descripcion || ' inc. ';
    RETURN NEXT fila;
  END LOOP;
END;
$BODY$
LANGUAGE plpgsql;

SELECT * FROM recorre();

```

Ejemplo: Funcion para calcular los dis de vencimiento que queda de un producto

```sql
CREATE OR REPLACE FUNCTION dias_para_vencimiento(_fecha_vencimiento DATE ) RETURNS SMALLINT AS
$BODY$
DECLARE
  dias_faltantes SMALLINT;
BEGIN
  dias_faltantes = extract('day' from date_trunc('day', _fecha_vencimiento::date) - now());
  RETURN dias_faltantes;
END;
$BODY$
LANGUAGE plpgsql;

--uso
SELECT nombre, vencimiento, dias_para_vencimiento(vencimiento) as dias_faltantes FROM productos;
```

Ejemplo: Asignar una categoria segun un numero, en este caso segun los dias restantes de
vencimiento se asignara 'Sin Problema', 'Normal' 'Cuidado'

<!-- Postgres estructura condicional estructura, estructura condicioneles estructura, estructura if estructura, estructura if estructura, estructura if else estructura, estructura if else estructura -->

```sql
CREATE OR REPLACE FUNCTION set_tipo_alarma(dias_faltantes SMALLINT) RETURNS CHARACTER VARYING AS
$BODY$
DECLARE
  categoria CHARACTER VARYING;
BEGIN

  IF dias_faltantes < 20 THEN
    RETURN 'Cuidado';

  ELSIF ias_faltantes > 21 AND dias_faltantes < 70 THEN
    RETURN 'Normal';

  ELSE
    RETURN 'Sin Problema';
  END IF;
  
END;
$BODY$
LANGUAGE plpgsql;

--uso
SELECT nombre, vencimiento, dias_para_vencimiento(vencimiento) as dias_faltantes,
  set_tipo_alarma(dias_para_vencimiento(vencimiento)) AS cat_alarma
FROM productos;
```

resultado

```r
NOMBRE         VENCIMIENTO     DIAS_FLTE    CAT_ALARMA
"Yogurt"        "2019-09-20"    6            "Cuidado"
"Paquetaso"     "2019-09-28"    14           "Cuidado"
"Vols"          "2019-10-01"    17           "Cuidado"
"Bonicula"      "2019-10-27"    43           "Normal"
"Fire"          "2019-11-01"    48           "Normal"
"Chocolate"     "2019-11-27"    74           "Sin Problema"
"Vols barato"   "2019-12-01"    78           "Sin Problema"
"Canabis"       "2019-12-27"   104           "Sin Problema"
```

Ejemplo: Crearemos una funcion que nos permita la paginacion de
una tabla.

**Algoritmo para paginar**: Para esto necesitaremos sacar el offset

Cantidad de registros a ver = CRV
pagina a mostrar = PM

```r
CRV  x PM - CRV = offset
5    x  1 -  5  =  0
5    x  2 -  5  =  5
5    x  3 -  5  =  15
5    x  4 -  5  =  20
5    x  5 -  5  =  25
5    x  5 -  5  =  30
```

creacion

<!-- Retornar una consulta en una funcion, Retornar una consulta en un procedimiento almacenado -->

```sql
CREATE OR REPLACE FUNCTION nombres_paginados(_registros SMALLINT, _pagina SMALLINT) RETURNS SETOF nombres AS
$BODY$
DECLARE
    _inicio SMALLINT;
BEGIN
    _inicio = _registros * _pagina - _registros;
  RETURN QUERY
      SELECT nombre, edad FROM nombres
      LIMIT _registros OFFSET _inicio;
  
END;
$BODY$
LANGUAGE plpgsql;

--uso
SELECT * FROM nombres_paginados(5::SMALLINT, 1::SMALLINT);
```

Ejemplo: Registrar auditoria

```sql
CREATE OR REPLACE FUNCTION auditoria(nombre character varying) RETURNS SETOF nombres AS
$BODY$
BEGIN
  INSERT INTO auditoria(id, usuario, accion, fecha) VALUES(DEFAULT, nombre, 'consulta ventas', NOW());
  RETURN QUERY  SELECT nombre, edad FROM nombres;
END;
$BODY$
LANGUAGE plpgsql;

--uso
SELECT * FROM nombres_paginados(5::SMALLINT, 1::SMALLINT);
```

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Trigers

<!-- file://E:\cursos\_2019\img\sql\src\trigers.sql -->

Son como escuchadores que cada vez que ocurre un cambio sobre
una tabla estos se ejecutan y dependen de lenguaje procedural
para esto.
Para la creacion del trigger se requiere de una funcionalidad que podemos
declarar directamente pero es constumbre standart siempre asignarle esta
logica a travez de una funcion. [sql para la practica](./img/src/trigers.sql)

1. Pasos para crear triger

  1.1. Creacion de funcion

  1.2. Creacion del triger asignandole la funcion

Se maneja diferentes variables para el manipular la informacion como lo son.

- **NEW**: contiene la informacion del nuevo registro que se esta trabajando.

- **OLD**: Contiene la informacion del registro anterior a la operacion.

- **TG_OP**: Contiene el nombre de la operacion  que estamos realizando.

Tenemos 2 diferentes momentos de ejecucion del trigger

- **BEFORE**: Antes de realizar la accion
- **AFTER**: despues de realizar la accion

Estructura

```r
CUANDO = BEFORE ò AFTER
ACCION = INSERT y/o UPDATE y/o DELETE

CREATE TRIGGER nombre_triger CUANDO ACCION FOR EACH ROW EXECUTE PROCEDURE nombre_funcion;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Ejemplo 1_

Vamos a validar el campo nombre en tanto registremos un producto

<!-- lanzar exepcion, uso de excepciones, crear excepcion-->
creacion funcion

```sql
CREATE OR REPLACE FUNCTION valida_producto() RETURNS TRIGGER AS
$BODY$
BEGIN
--validamos
  IF length(NEW.nombre) = 0 THEN
  RAISE EXCEPTION 'El nombre debe de contener alguna informacion';
  END IF;

  IF NEW.cantidad <= 0 THEN
  RAISE EXCEPTION 'La contidad no puede ser menor a cero';
  END IF;

  IF NEW.precio <= 0 THEN
  RAISE EXCEPTION 'el precio no puede ser menor a cero';
  END IF;

--auditoria
  NEW.ultima_mod = now();
  NEW.usuario = user;

  RETURN NEW;

END;
$BODY$
LANGUAGE plpgsql;
```

creacion triger

```sql
CREATE TRIGGER trigger_inserta_producto
BEFORE INSERT OR UPDATE
ON productos
FOR EACH ROW EXECUTE PROCEDURE valida_producto();
```

prueba

```r
INSERT INTO productos(nombre, precio, cantidad, vencimiento) VALUES ('', 0, 0, '2019-09-28');
ERROR:  El nombre debe de contener alguna informacion

INSERT INTO productos(nombre, precio, cantidad, vencimiento) VALUES ('algo', 0, 0, '2019-09-28');
ERROR:  La contidad no puede ser menor a cero

INSERT INTO productos(nombre, precio, cantidad, vencimiento) VALUES ('algo', 1, 0, '2019-09-28');
ERROR:  La contidad no puede ser menor a cero

UPDATE productos SET cantidad = -1 WHERE id_producto = 1;
ERROR:  La contidad no puede ser menor a cero
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Ejemplo 2

Vamos a crear un triger de auditoria donde se grabaran todos los cambios sobre productos.

Nota: Firjarnos que cuando eliminamos un producto usamos la variable OLD ya que no en este
caso no podremos usar NEW. Y que en **actualizar** estamos usando los 2 para tener la informacion
de que es lo que estaba antes como lo que estaba despues.

**Error**: Al crear el trigger no se elimina registro
**Descripcion**: al eliminar un productos despues de crear el trigger no eliminaba, eliminaba el trigger y se podia de nuevo
**Solucion**: Al perecer se necesita que al eliminar retorne el OLD por que al regresar null pasaba esto.

```sql
DROP FUNCTION auditoria_producto();

CREATE OR REPLACE FUNCTION auditoria_producto() RETURNS TRIGGER AS
$BODY$
BEGIN
  
  IF TG_OP = 'INSERT' THEN
    RAISE NOTICE 'tg insert';

    INSERT INTO auditoria_productos (accion, fecha, nombre, cantidad, precio)
    VALUES ('insertar', NOW(), NEW.nombre, NEW.cantidad, NEW.precio);

    RETURN NEW;

  ELSIF TG_OP = 'DELETE' THEN
    RAISE NOTICE 'tg delete';

    INSERT INTO auditoria_productos (accion, fecha, nombre, cantidad, precio)
    VALUES ('borrar', NOW(), OLD.nombre, OLD.cantidad, OLD.precio);

    RETURN OLD;
  
  ELSIF TG_OP = 'UPDATE' THEN
    RAISE NOTICE 'tg update';

    INSERT INTO auditoria_productos (accion, fecha, nombre, cantidad, precio)
    VALUES ('actualizar_antes', NOW(), OLD.nombre, OLD.cantidad, OLD.precio);

    INSERT INTO auditoria_productos (accion, fecha, nombre, cantidad, precio)
    VALUES ('actualizar_despues', NOW(), NEW.nombre, NEW.cantidad, NEW.precio);

    RETURN NEW;
  
  END IF;
  
END;
$BODY$
LANGUAGE plpgsql;
```

creacion del trigger

```sql
DROP TRIGGER trigger_auditoria_producto ON productos;

CREATE TRIGGER trigger_auditoria_producto
BEFORE INSERT OR UPDATE OR DELETE
ON productos
FOR EACH ROW EXECUTE PROCEDURE auditoria_producto();
```

prueba

```sql
INSERT INTO productos(id_producto, nombre, precio, cantidad, vencimiento) VALUES (14, 'algo', 1, 1, '2019-09-28');

UPDATE productos SET nombre = 'algo upd', precio = 2, cantidad = 2 WHERE nombre = 'algo';

UPDATE productos SET nombre = 'algo upd 3', precio = 3, cantidad = 3 WHERE nombre = 'algo upd';

DELETE FROM productos WHERE id_producto = 14;

SELECT * from auditoria_productos;
       accion       |           fecha            |   nombre   | cantidad | precio
--------------------+----------------------------+------------+----------+--------
 insertar           | 2019-09-14 00:38:49.270138 | algo       |        1 |      1
 actualizar_antes   | 2019-09-14 00:39:31.333937 | algo       |        1 |      1
 actualizar_despues | 2019-09-14 00:39:31.333937 | algo upd   |        2 |      2
 actualizar_antes   | 2019-09-14 00:40:42.246429 | algo upd   |        2 |      2
 actualizar_despues | 2019-09-14 00:40:42.246429 | algo upd 3 |        3 |      3
 borrar             | 2019-09-14 00:42:12.243437 | algo upd 3 |        3 |      3
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Ejemplo 3 1/2

Creacion de un trigger que realize lo siguiente.

1. Compra/fabricacion de producto
1.1. En caso de que no exista el producto agregarlo a la tabla producto
1.2. Agregar la cantidad de producto a la tabla productos

2. Venta de producto
2.1 validar que hay suficiente productos para vender
en caso de que no lanzar una exepcion.
2.2. En tanto se haga la venta reducir en productos la
cantidad que se vendio.

creacion de la funcion del punto 1

<!-- uso de PERFORM, en este caso lo usamos para saber si existe el producto-->

```sql
CREATE OR REPLACE FUNCTION fn_agregar_producto_cantidad_en_compra() RETURNS TRIGGER AS
$BODY$
BEGIN
  RAISE NOTICE 'ID> %', NEW.id_producto;
  PERFORM id_producto FROM productos WHERE id_producto = NEW.id_producto;
  IF FOUND THEN
    RAISE NOTICE 'update cantidad productos pro compra';
    UPDATE productos SET cantidad = cantidad + NEW.cantidad WHERE id_producto = NEW.id_producto;
  ELSE
    RAISE NOTICE 'nuevo prod por compra';
    INSERT INTO productos(nombre, cantidad, precio, vencimiento) VALUES(NEW.nombre, NEW.cantidad, NEW.precio, NEW.vencimiento);
  END IF;
  RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql;

```

creacion del trigger del punto 1

```sql
DROP TRIGGER tg_agregar_producto_cantidad_en_compra ON compras

CREATE TRIGGER tg_agregar_producto_cantidad_en_compra
BEFORE INSERT OR UPDATE
ON compras
FOR EACH ROW EXECUTE PROCEDURE fn_agregar_producto_cantidad_en_compra();
```

prueba del punto 1.

```sql
--       comprar yogurt
SELECT * FROM productos WHERE id_producto = 1;
INSERT INTO compras(descripcion, cantidad, fecha, id_producto) VALUES('Pa enero', 3, '2019-01-01', 1);
SELECT * FROM productos WHERE id_producto = 1;
SELECT * FROM compras;
--       FIN comprar yogurt, debio de actualizar la cantidad en la tabla producto
```

![trigger_compra_producto_actualizar_cantidad](./img/16_trigger_compra_producto_actualizar_cantidad.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Ejemplo 3 2/2

Creacion de la funcion para trigger venta punto 2

<!-- Postgress Guardar registro en variable, Guardar fila en variable, Guardar consulta en variable, Guardar registro en una variable, Guardar fila en una variable, Guardar consulta en una variable, guardar informacion en variable -->
```sql
CREATE OR REPLACE FUNCTION fn_quitar_cantidad_producto_en_venta() RETURNS TRIGGER AS
$BODY$
DECLARE
  _producto_actualizar productos%ROWTYPE;
BEGIN  
  SELECT * INTO _producto_actualizar FROM productos WHERE id_producto = NEW.id_producto;

  IF _producto_actualizar.cantidad >= NEW.cantidad THEN
    RAISE NOTICE 'update cantidad productos % por venta', _producto_actualizar.nombre;
    UPDATE productos SET cantidad = cantidad - NEW.cantidad WHERE id_producto = NEW.id_producto;
  ELSE
    RAISE EXCEPTION 'No hay suficiente producto para realizar la venta';
  END IF;

  RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql;

```

creacion del trigger del punto 2

```sql
DROP TRIGGER IF EXISTS tg_quitar_cantidad_producto_en_venta ON ventas;

CREATE TRIGGER tg_quitar_cantidad_producto_en_venta
BEFORE INSERT OR UPDATE
ON ventas
FOR EACH ROW EXECUTE PROCEDURE fn_quitar_cantidad_producto_en_venta();
```

prueba del trigger punto 2.

```sql
--       venta yogurt
SELECT * FROM productos WHERE id_producto = 1;
INSERT INTO ventas(descripcion, cantidad, fecha, id_producto) VALUES('Pa Enero', 2, '2019-01-01', 1);
SELECT * FROM productos WHERE id_producto = 1;
SELECT * FROM ventas;
--       FIN venta yogurt, debio de actualizar quitando la cantidad vendida en la tabla productos

--       venta no posible de yogurt
SELECT * FROM productos WHERE id_producto = 1;
INSERT INTO ventas(descripcion, cantidad, fecha, id_producto) VALUES('Pa Enero', 200, '2019-01-01', 1);
SELECT * FROM productos WHERE id_producto = 1;
SELECT * FROM ventas;
--       FIN venta yogurt, debio de haber lanzado ERROR: No hay suficiente producto para realizar la venta

```

![trigger_compra_producto_actualizar_cantidad](./img/17_trigger_venta_producto_actualizar_cantidad.PNG)

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Normalizacion / Formas Normales

La normalizacion se utiliza para veridficar si una relacion no tiene
problemas como:

- Rendundancia
- Problemas de
-- insercion
-- Actualizacion
-- Borrado

**Calculo de las dependencias Funcionales** ``` A -> B ```
De una relacion tenemos diferentes atributos y dependencias, donde
podemos decir que una relacion R depende de los atributos A y B donde
B depende de Adonde por cada A unicamente existe un dato B.

Notacion: A -> B (A depende de B)

Ejemplo: De una tabla personas donde tenemos nombre=B solo existe
un CURP=A, es decir, NO puede haber mas de un nombre para cada CURP,
es decir, para el curp 111 solo le corresponde a Juanito, es decir,
que el curp 111 NO puede estar relacionado a Juanito y Pedrito por igual.

```r
CURP -> Nombre
```

----

**Colculo de dependencias Redundantes**
De una relacion que tiene los atriburos A, B y C y tenenmos que
A->B, B->C, A->C pero la relacion A->C es redundante ya que tenemos
que B->C y desde B podemos encontrar a A, entonces ya no necesitamos
que A->C.

Al encontrar estasdependencias redundantes debemos de eliminarlas.

----

**Colculo de Claves candidatas**
Identificar que dato es el que puede identificar a toda una
fila

Tenemos 6 Formas Normales que son 1FN, 2FN, 3FN, FNBC, 4FN, y 5FN donde el
99% de los casos usaremos hasta la FNBC y se usan en orden, las demas son
especializadas y/o poco comunes.

- **1FN**

  - Cada Atributo debe ser simple: Es simple

  - Cada Atributo es monovaluado: Nada de en una celda tener datos
    separados por algun separador o de tener columnas como telefono1, telefono2.

Ejemplo: Podemos ver que tenemos varios valores en la misma
celda, lo que podria probocar repeticiones, falta de control etc.

![18_primera_forma_normal](./img/18_primera_forma_normal.PNG)

- **2FN**
  - Se encuentra en 1FN
  
  - Cada atributo no principal tiene Dependencia Funcional de la clave candidata: Nada
    de tener datos que no dependan de la llave candidata

Ejemplo: Podemos ver en la tabla de arriba los siguiente

1. para saber la calificacion del alumno necesitamos del alumno,
la materia y de la nota, es decir que la materia y la nota dependen
directamente del alumno. Por lo que nos sale sobrando la informacion de
salon.

2. Para saber en que salon se imparte X materia necesitamos saber de
la materia y del salon, es decir, el salon depende de la materia para
identificarse o viceversa.

![19_segundo_forma_normal](./img/19_segundo_forma_normal.PNG)

- **3FN**

  - Se encuentra en 2FN

  - No existe ningun atributo no principal que dependa transitivamente de la clave candidata: nada de
    datos tener datos que no dependan directamente de la clave candidata, ya que este dato debe de
    estar con su clave candidata que mejor le corresponde y acceder a este por propiedad transitiva.

Ejemplo: Podemos ver la materia y el salon dependen directamente para
identificarse y en este caso cada salon tiene un telefono, pero vemos
que salon depende de materia y telefono depende de salon, lo que significa
que podemos saber el telefono de la materia a travez del salon. por lo que
denemos separar esta informacion.

![20_tercera_forma_normal](./img/20_tercera_forma_normal.PNG)

----

- **FNBC**
  - Si y solo si todo determinante es la clave candidata, Es decir, Solo debe
  de haber una clave candidata.

Ejemplo: Podemos ver que con la identificacion podemos saber el nombre
y que con la identificacion y la mataria podemos identificar la nota
por lo que tenemos 2 claves candidatas 1) identificacion y 2) identificacion/materia
por lo que estamos violando esta forma normal.
Por lo que para saber las notas del estudinte nos basta solamente con la identificacion y
atravez de esta sabemos el nombre.

![20_BC_forma_normal](./img/21_BC_forma_normal.PNG)

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Proceco de Normalizacion

![22_ejem_normalizacion](./img/22_ejem_normalizacion.PNG)

### 1 Hacemos preguntas

1.1. ¿Que necesito para ingrear un profesor?: Materia, libro si
no tengo su materia o libro no podre hacer la insercion

1.2. ¿Que necesito para ingresar una maeria?: Libro y Profesor

1.3. El mismo problema para ingresar un libro

1.4. ¿Si quiero eliminar un profesor aue pasa?: Al eliminar por ejemplo
Pablo se borrarian 3 registros, y con ellos informacion de oficina, libro,
y materia

1.5. El mismo problema para eliminar un libro, por ejemplo quiero eliminar
"impuestos" se borrarian 2 registros con ellos toda la info del profesor Juan

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 2 Encontramos las Dependncias Funcionales de esta tabla

2.1. Un profesor tiene una unica oficina y telefono

- `profesor -> Oficina, Telefono`

2.2. Una oficina tiene un unico telefono: `Oficina -> Telefono`

2.2. Una materia tiene solo un numero de creditos asignado: `Materia -> Creditos`

2.2. Un libro tiene autor y año: `Libro -> Autor, Año`

2.2. un profesor puede dictar mas de una materia: `Profesor --/-> Materia`

2.2. Una materia tiene mas de un profesor: `Materia --/-> Profesor`

2.2. Un profesor en una materia solo usa un libro: `Profesor, Materia -> libro`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 3. Resumimos

Todas

```r
Dependencias {
  profesor -> Oficina, Telefono
  Oficina -> Telefono
  Materia -> Creditos
  Libro -> Autor, Año
  Profesor --/-> Materia
  Materia --/-> Profesor
  Profesor, Materia -> libro
}
```

1. Vemos que podemos llegar al telefono de
la oficina a traves de profesor asi que eliminamos
oficina -> telefono

2. Vemos que profesor materia y profesor materia
son los mismo indicando una llave candidata

```r
Dependencias {
  profesor -> Oficina
  Oficina -> Telefono
  Materia -> Creditos
  Libro -> Autor, Año
  Profesor, Materia -> libro
}
```

Para saber cuales son las llaves candidatas devemos de
fijarnos que esten siempre en la parte derecha, por ejemplo
profesor es una candidata

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 3. Separamos entidades

El cuadro de hasta arriba reprecenta la tabla donde
tenemos el mismo los datos.

`P=profesor, O=Oficina, T=telefono M=Materia C=Creditos L=Libro, A=Autor, Ñ=Año`

De los cuales al final tenemos 5 relaciones

![23_ejem_normalizacion.PNG](./img/23_ejem_normalizacion.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 4. Repetimos preguntas

Hacemos el mismo proceso hasta que lo tengamos refinado

<!--

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Bases de datos no relacionales

Tenemos varios tipos de bases de datos entre ella esta mongo que es muy
popular, la cual precenta menos trabajo del lado de la BD ya que ni siquiera
requiere de un diseño previo, pero hay un mayor trabajo del lado de la
aplicacion de servidor para guardar y consultar de forma correcta la info.

Podemos simular una base de datos no relacional con postgres

1. 101

```sql
CREATE DATABASE mi_no_relacional OWNER = estudiante TABLESPACE = tlb_misdatos;

--creacionde tabla, en estos necesitamos solo 2 campos el id y el json
CREATE TABLE productos (
  id     SMALLSERIAL PRIMARY KEY,
  info    json
);

--introducir
INSERT INTO productos(id, info) VALUES(DEFAULT, '{"codigo": 1, "nombre": "aguacate", "cantidad": 20, "precio": 20.5, "vencimiento":"2019-09-25"}');
INSERT INTO productos(id, info) VALUES(DEFAULT, '{"codigo": 222, "nombre": "pimienta", "cantidad": 10, "precio": 5, "vencimiento":"2020-09-25"}');

-- consultar, esto seria lo mismo para actualizar
SELECT info->'nombre' FROM productos; -- regresa las comillas dobles
SELECT info->>'nombre' AS nombre, info->>'cantidad' AS Cantidad FROM productos;
SELECT info->>'nombre' AS nombre, info->>'cantidad' AS Cantidad FROM productos WHERE info->>'codigo' = '222';

UPDATE productos SET info->>'codigo' = '111' WHERE info->'nombre' = 'aguacate';

-- convertir fila a json, convertir registro a json
SELECT row_to_json(colores) FROM colores;
```

Nota: podria ser muy util guardar la auditoria usando la conversion
de registro a json, lo cual nos facilita ya que al sufrir un cambio
la tabla uditada deberia de cambiar la de auditoria, pero con esto no
ya que el json no nos obliga a complir con una estructura.

Ejemplo

```sql
CREATE TABLE auditoria(
  id SERIAL,
  accion VARCHAR,
  fecha DATE,
  usuaio VARCHAR,
  nombre_tabla VARCHAR,
  antes json,
  despues json
);
```

<!--

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Investigacion sobre Versionado en bases de datos

- Fuente [One does not simply update a database](https://dev.to/pesse/one-does-not-simply-update-a-database--migration-based-database-development-527d?ref=hackernoon.com)

En la fuente de arriba nos explican por que las el deploy de una app
(entiendace appcomobackend o frontend) no es lo mismo a una base de datos,
porque tenemos restricciones que en una aplicacion son pocas, por ejemplo
una API externa, dependencia, configuracion externa, que igual no son
mayor problema. Resumidamente esto pasa porque tenemos un estado de la
aplicacion que casi no cambia, hablando de lo construido/compilado.

En cambio una D.B. en tanto mandamos a produccion esta su estado esta
cambiando continuamente, y no podemos o no es recomendable, desintalar
y reinstalar todo, por que a veces no es posible separar la estructura
de los datos.

Se habla de 2 formas del manejo de cambios:

Una parecida al del **unico script** pero asistido por una herramienta
como sql-compare, que cargando la db actual se crean nuevos cambios,
estos cambios son aplicados sin guardarlo en un script, para que la
proxima vez sea cargado de nuevo la db actual para hacerlo mismo.

La otra (Recomendada) es por **scripts incrementales** el cual por
cada estructura y cambio crear diferente scripts los cuales ejecutamos
en orden para llegar a la db actual, y pero por iteraciones, donde
usamos un script base/principal el cual este se carga y luego los demas.

**Ejemplo de scripts incrementales** abajo podemos ver una serie de
scripts los cuales ejecutando en el orden correcto podemos llegar a
una version especifica de nuestra DB

```r
migrations/
  1.0/
    1.0.1_alter_table_powerful_sith.sql
    1.0.2_update_table_powerful_sith_split_name.sql
  1.1/
    1.1.1_add_view_V_POWERFUL_SITH.sql
```

Es **Recomendado si no necesario** que los scripts sean **IDEMPOTENTES**
lo que significa que si los ejecutamos varias veces vamos a tener el mismo
resultado.

Por ejemlo. En el script de abajo podemos ver como hacemos una consulta
por la existencia de la columna `PRENAME` que NO EXISTE se va a crear
la columna, y si YA EXISTE esta no se creara y mandara un mensaje.  
Asi podemos ejecutar multipes veces este script y tendra el mismo
resultado que en este caso sera crear la columna o no.

```sql
declare
  v_count integer;
begin
  select count(*) into v_count from user_columns where table_name ='POWERFUL_SITH' and column_name ='PRENAME';
  if ( v_count <= 0 ) then
    execute immediate 'alter table POWERFUL_SITH add PRENAME varchar2(100)';
  else
    dbms_output.put_line('POWERFUL_SITH.PRENAME already exists');
  end if;
end;
/
```

**Los cambios en DB son refactorizaciones:** entendemos refactorizacion
como modificaciones que hacen mas facil de entender y mantener el software
sin cambiar el comportamiento obserbable, ya que cuando hacemos un cambio
en BD estamos haciendo 3 cosas

1. Cambiar la estructura de DB
2. Migrar la informacion a la nueva estructura
3. Cambiar el codigo de acceso a la DB.

**Cambios backwards compatible (no-rompedores) y rompedores:** tenemos
estos dos tipos que como su nombre lo indica los rompedores rompen el
esquema actual o software rio abajo, mientras que los no-rompedores
pueden ser agregados y todo puede seguir funcionando bien rio abajo,
por ejemplo.

- Cambios NO-rompedores
  - agregar algo como una columna, tabla, default
- Cambios Rompedores
  - Eliminar una columna, tabla, renombrar, agregar restricciones etc.

**Lidiar con cambios rompedores:** Estos es forzoso ya sea.

- Detener el servicio por un momento: Para que en este tiempo se apliquen
  los cambio en la DB como en rio-abajo.
- Un periodo de migracion donde funcionan la version nueva y vieja: este
  modo podemos hacer por ejemplo, al renombrar una tabla crear una vista
  con el mismo nombre de la tabla y cuando rio-abajo ya tengan los cambios
  eliminar la vista para que usen la tabla renombrada, para no se quede
  esta compejidad extra.

- Fuentes:
  - [Link1](https://www.javiergarzas.com/2014/11/versionar-bases-de-datos-2.html)

Existen dos formas populares de manejar los cambios de una base de datos
que son

- scripts incrementales
- Unico Script

Cualquiera de estos debemos el(los) script los usamos para implemetar
los cambios que queremos agregar, y debemos de cuidar cosas como

**Version Base** Todo programa o base de datos empieza por una primera
version, la cual de esta se empiezan a implementar y guardar cambios,
y tambien debemos de tener claro cual es nuestra version ya que esta va
evolucionando, igual que las diferentes ramas de `test, calidad o PRODUCCION`
en realidad la version mas importante o base es produccion, de la cual
deberemos de empezar a implementar cualquier cambio, como vemos en la
imagen de abajo.

![VERSIONADO](./img/24_versionado.PNG)

**Que scripts ya se ejecutaron** para esto podemos crear una tabla que
como la de abajo, donde llevamos un control de que hemos aplicado sobre
la base de datos.

| ID  | VersionNumber | ScriptName | dateApplied |
| --- | ------------- | ---------- | ----------- |

En caso de usar alguna herramienta como `Flyway ó Liquidbase` estan nos
ayudan a manejar los cambios. los cuales tenemos este [curso de fundamentos](https://learn.liquibase.com/catalog/info/id:125z)
para ver como implementarlo y no inventar el hilo negro

<!--

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Cosas que me voy encontrando

### feachas

<!--
asignar fecha como default, agregar fecha como default, operaciones con fechas
sumar a fecha, sumar dias a fecha,
-->

poner fecha como default

```sql
CREATE TABLE usuarios (
  ...
  create_at timestamp NOT NULL DEFAULT now()+interval '1 day'
);
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Ejecutar script desde consola

<!--
ejecutar archivo sql desde comando,  ejecutar archivo sql desde shell
ON_ERROR_STOP=1: al haber un error se detiene la ejecucion y como resultado
saca un 1 para poder detectar el error, por ejemplo en jenkins o en on
bash script.
-->

podemos ejecutar un script sql desde un comando en postgres, por ejemplo

- `psql -v ON_ERROR_STOP=1 -h 127.0.0.1 -U mario_admin -d usuarios_devops -a -f path/script.sql`

Igual podemos ejecutar un comando de la siguiente manera

<!-- Ejecutar comando, -->

- `psql -c 'SELECT * FROM test;'`
- `psql postgresql://user:pass@x.x.x.x:1234/dn_name -c "select...;"`

### Cambiar idioma de postgres (espaniol-ingles)

<!-- cambiar lenguaje, cambiar a ingles, idioma ingles -->

Fuente: [Link](https://stackoverflow.com/questions/31605837/change-language-of-the-postgresql-server-error-messages-to-english)

Vamos a `[RUTA_DE_INSTALACION]/data/postgresql.conf` y settear la
siguiete configuracion

```conf
lc_messages = 'English_United States.1252'
```

### Cerrar conexiones

<!--
cerrar todas las conexiones, matar todas las conexiones, terminar todas las conexiones, matar conexiones, eliminar todas las conexiones
ERROR:  database "*" is being accessed by other users, DETAIL:  There are 5 other sessions using the database.
-->

terminar conexiones [fuente](https://stackoverflow.com/questions/5408156/how-to-drop-a-postgresql-database-if-there-are-active-connections-to-itz)

```sql
-- para verciones +9.2 (nuevas)
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'TARGET_DB' -- ← change this to your DB
  AND pid <> pg_backend_pid();

-- para verciones -9.1 (antiguas)
SELECT pg_terminate_backend(pg_stat_activity.procpid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'TARGET_DB' -- ← change this to your DB
  AND procpid <> pg_backend_pid();
```

### Uso de expreciones regulares

<!-- usar expreciones regurales, regex, validacion de datos-->

- Fuente1: [StackOverflow](https://stackoverflow.com/questions/46697011/postgresql-regular-expression-in-type-rather-than-check-constraint)
- Fuente2: [Stackoverflow](https://stackoverflow.com/questions/5689718/how-can-i-create-a-constraint-to-check-if-an-email-is-valid-in-postgres)

A. Podemos crear la tabla con una columna con dicha validacion

```sql
CREATE TABLE emails (
  email varchar
  CONSTRAINT proper_email CHECK (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$')
```

B. Podemos crear una funcion que haga la validacion y agregarlo

```sql
CREATE OR REPLACE FUNCTION check_email(email text) RETURNS bool
LANGUAGE plperlu
AS $$
use Email::Address;

my @addresses = Email::Address->parse($_[0]);
return scalar(@addresses) > 0 ? 1 : 0;
$$;

CREATE TABLE emails (
    email varchar
    CONSTRAINT proper_email CHECK (check_email(email))
);
```

C, Tambien podemos hacer uso de [domain](https://www.postgresql.org/docs/current/static/sql-createdomain.html)
este nos sirve para validar el dato despues de que se ahiga guardado

```sql
create domain email_address as text
    check (value ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$')

-- ejemplo
select 'abc'::email_address;
ERROR:  value for domain email_address violates check constraint "email_address_check"

select 'abc@mail.com'::email_address;
 email_address
---------------
 abc@mail.com
(1 row)
```

### Marcar tabla como eliminada y recuperarla

<!-- eliminar tabla de forma segura, -->

Si no queremos eliminar una tabla pero queremos evitar perder informacion,
podremos marcarla como eliminada, y asi la tenemos siempre disponible para
consultarla o en algun momento recuperarla.

**NOTA:** Cuando creamos una tabla con un tipo serial (ej. para el id),
automaticamente se crea una secuencia como dependencia de la tabla, y
si renombramos la tabla el nombre de la secuencia quedara igual, pudiendo
generar diferencias en los respaldos de esquema que generemos.

**Objetivo:** Vamos a crear una tabla usuarios para luego marcarla como
eliminada para luego recuperarla.

1, Asi es como comunmente creamos una tabla

```sql
CREATE TABLE IF NOT EXISTS usuarios (
  id_usuario        SERIAL        PRIMARY KEY,
  create_at         TIMESTAMP     NOT NULL DEFAULT now()+interval '1 day'
);
```

2, De forma automatica se crea una secuencia

```sql
CREATE SEQUENCE IF NOT EXISTS seq_tlb_usuarios_col_id_usuario AS integer START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
```

3, Marcamos la tabla como eliminada

```sql
ALTER TABLE IF EXISTS usuarios RENAME TO deleted_usuarios;
```

**Problema:** Cuando creamos un respaldo veremos la secuencia con el
mismo nombre, lo cual no estara marcada como eliminada y eso podria
darnos problemas, por ejemplo comparando schemas.  

**Solucion:** Vamos a usar funciones para crear la tabla la cual si ya
marcamos la tabla como eliminada entonces la renombrara para que la
podamos usar rio abajo.

1, renombramos la tabla con todo y secuencia y por temas de dependencia
lo ahcemos en el siguiente orden

```sql
-- 1.
-- Renombrar secuencia que se creo automaticamente
ALTER SEQUENCE IF EXISTS seq_id_usuario RENAME TO deleted_seq_id_usuario;

-- 2
-- Agregamos el cambio en la tabla para que no falle
ALTER TABLE IF EXISTS usuarios
  ALTER COLUMN id_usuario SET DEFAULT nextval('deleted_seq_id_usuario');

-- 3
-- renombramos la tabla
ALTER TABLE IF EXISTS usuarios RENAME TO deleted_usuarios;
```

2, Cremos las funciones/scripts para recuperar la tabla si se elimino
buscando la secuencia y la tabla que renombramos

<!-- ver si existe, checar si existe, uso condicional, uso if, ver si existe, 
como saber si existe, checar existencia, ver existencia, consultar existencia
-->

```sql
-- 1
-- Crear secuencia, o si ya se elimino dicha secuencia recuperarla
DO
$do$
BEGIN
  IF EXISTS (SELECT 1 FROM pg_catalog.pg_sequences WHERE sequencename = 'deleted_seq_id_usuario') THEN
    RAISE NOTICE 'La secuencia <seq_id_usuario> se elimino anteriormente, recuperando...';
    ALTER SEQUENCE IF EXISTS deleted_seq_id_usuario RENAME TO seq_id_usuario;
    RAISE NOTICE 'Secuencia seq_id_usuario recuperada';
  ELSE
    CREATE SEQUENCE IF NOT EXISTS seq_id_usuario AS integer START WITH 1 INCREMENT BY 1
      NO MINVALUE NO MAXVALUE CACHE 1;
    RAISE NOTICE 'Creada secuencia seq_id_usuario';
  END IF;
END
$do$;


-- 2
-- crear tabla usuarios, o si se elimino recuperarla
DO
$do$
BEGIN
  IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables WHERE tablename = 'deleted_usuarios') THEN
    RAISE NOTICE 'La tabla ya se habia eliminado, retornando';
    ALTER TABLE IF EXISTS deleted_usuarios RENAME TO usuarios;
  ELSE
    CREATE TABLE IF NOT EXISTS usuarios (
      id_usuario        INTEGER       PRIMARY KEY DEFAULT nextval('seq_id_usuario'),
      titulo_y_nombre   VARCHAR(40)   NOT NULL,
      roles             VARCHAR(40)   NOT NULL,
      activo            BOOLEAN       NOT NULL,
      create_at         TIMESTAMP     NOT NULL DEFAULT now()+interval '1 day'
    );
    RAISE NOTICE 'Tabla usuarios creada desde cero';
  END IF;
END
$do$;
```

3, ahora gracias a esa marca cuando creemos un respaldo del schema para
compararlo podremos evitar tanto la tabla como su secuencia, de la
siguiente manera

```r
pg_dump -U <user> -d <name_db> --schema-only --exclude-table=deleted_* > backup_schema_without_deleted_things.sql
```

**Resultado:** Pudimos "eliminar" una tabla sin perder datos y vimos la
menera de como evitarla en schema para compararla con versiones anteriores,
como tambien la podemos recuperar.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Migrar columna de varios valores a otra tabla en columnas

**Objetivo:** Separar una columna con multiples datos separados por coma
a una relacion muchos a muchos.

**Escenario** Supongamos que tenemos una tabla usuarios con una columna
roles, y si observamos estos son 7 roles que se comparten entre 9 usuarios,
por lo que lo mas correcto seria separar esta columna a una relacion muchos
a muchos, es decir.

A, Tenemos la siguiente tabla usuarios con una columna roles

```r
"SELECT nombre, roles FROM usuarios;"
  nombre  |            roles
  --------+-----------------------------
  Mario   | root, admin, editor
  Neey    | admin, editor
  Pedro   | editor
  Sancho  | viewer
  Carla   | editor
  Linda   | editor, publicador-ciencias
  Julio   | admin, publicador-medicina
  Carlos  | publicador-ciencias
  Carlos2 | publicador-tecnologia
  (9 rows)
```

B, Queremos separar la columna roles de la siguiente manera, donde podemos
ver que.

1. Tenemos una nueva tabla roles
2. Tenemos una nueva tabla usuarios_roles
3. En la tabla usuarios ya no tenemos la columna roles.
4. La tabla `usuarios_roles` cumple la funcion que tenia la columna
   roles en usuarios.roles, por ejemplo, el usr 1 tiene

```r
# Ref: __vgd82ijnmc33__ arriba podemos ver el estado anterior
"SELECT * FROM usuarios;"            "SELECT * FROM roles;"
id_usuario | nombre_usuario       id_rol |        nombre_rol
-----------+------------         --------+-----------------------
 1         | Mario                     1 | root
 2         | Neey                      2 | admin
 3         | Pedro                     3 | editor
 4         | Sancho                    7 | viewer
 5         | Carla                    10 | publicador-ciencias
 6         | Linda                    12 | publicador-medicina
 7         | Julio                    14 | publicador-tecnologia
 8         | Carlos                       (7 rows)
 9         | Carlos2
            (9 rows)

                "SELECT * FROM usuarios_roles ORDER BY fk_id_usuario;"
                        fk_id_usuario | fk_id_rol
                       ---------------+-----------
                                    1 |         1
                                    1 |         2
                                    1 |         3
                                    2 |         2
                                    2 |         3
                                    3 |         3
                                    4 |         7
                                    5 |         3
                                    6 |         3
                                    6 |        10
                                    7 |         2
                                    7 |        12
                                    8 |        10
                                    9 |        14
                                        (14 rows)
```

**Implementacion:** Vamos a seguir los siguientes pasos para hacer
la migracion

A. Creamos una nueva tabla roles

```sql
CREATE TABLE IF NOT EXISTS roles (
  id_rol            SERIAL        PRIMARY KEY,
  nombre            VARCHAR(40)   NOT NULL UNIQUE,
  activo            BOOLEAN       NOT NULL,
  create_at         TIMESTAMP     NOT NULL DEFAULT now()
);
```

B. Insertamos los roles en la tabla usuarios dividiendolos por comas,
en la tabla roles sin que estos se repitan, lo logramos por medio de.

1. `string_to_array`: dividir cadena a arreglo, en este caso por una
   coma y un espacio
2. `UNNEST(`: nos permite usar el arreglo en el select
3. `ON CONFLICT DO NOTHING`: Como los roles se repiten los que esten
   repetidos no se insertaran.

<!-- split, partir cadena, cadena a arreglo, separar por comas-->

```sql
INSERT INTO roles(nombre, activo)
SELECT UNNEST(string_to_array(roles, ', ')) AS nombre, true
FROM usuarios ON CONFLICT DO NOTHING;
--RESULTADO: tenemos la tabla con los roles que habia en usuarios.roles de forma vertical
"SELECT * FROM rol"
 id_rol |        nombre
--------+-----------------------
      1 | root
      2 | admin
      3 | editor
      7 | viewer
     10 | publicador-ciencias
     12 | publicador-medicina
     14 | publicador-tecnologia
(7 rows)
```

C, Creamos la tabla que funcionara como nuestra relacion muchos-muchos

```sql
CREATE TABLE IF NOT EXISTS usuarios_roles (
  fk_id_usuario            INTEGER       REFERENCES usuarios(id_usuario),
  fk_id_rol                INTEGER       REFERENCES roles(id_rol),
  primary key (fk_id_usuario, fk_id_rol)
);
--RESULTADO: Creamos la tabla donde guardaremos los id de usuario y de rol con llave compuesta
```

D. Llemar la tabla usuarios_roles, por medio de insertar los ids
`usuarios.id_usuarios` y `roles.id_roles` en usuarios_roles, con la
condicion de que usuarios.roles debe contener el nombre del id_rol que
se insertara, es decir.

1. `_roles_id := ARRAY(SELE...`: Conseguimos todos los roles y los guardamos en un arreglo
2. `FOR i IN...`: Recorremos el arreglo de los roles
3. `insert`: Hacemos como si estubieramos copiando los datos de una tabla a la otra,
   pero usamos el id_usuario de la tabla con el id_rol del arreglo.

Nota: Primero llege a esta consulta la cual me permitia insertar los
id_usuario, id_rol pero solo de un rol, por lo que luego se me ocurrio
en iterar los roles.

```sql
-- insert into usuarios_roles(fk_id_usuario, fk_id_rol)
-- select usr.id_usuario, 1
-- from usuarios as usr
-- where roles LIKE '%' || (select nombre from roles WHERE id_rol=1) || '%';
```

Ya con la consulta anterior pude crear esta funcion

```sql
DO
$do$
DECLARE
  _roles_id INTEGER[];
  i integer;
BEGIN
  _roles_id := ARRAY(SELECT id_rol FROM roles);
  FOR i IN 1 .. array_upper(_roles_id, 1)
  LOOP
    insert into usuarios_roles(fk_id_usuario, fk_id_rol)
    select usr.id_usuario, _roles_id[i]
    from usuarios as usr
    where roles LIKE '%' || (select nombre from roles WHERE id_rol=_roles_id[i]) || '%'
    ON CONFLICT DO NOTHING;
  END LOOP;
END
$do$;
```

**Resultado:** Cmo vemos en `__vgd82ijnmc33__` separamos la columna roles
a una relacion muchos-muchos, en este caso una columna usuarios.roles a
roles.nombre y la relacionamos por medio de una tabla usuarios_roles con
los foreing_key de la tabla usuarios y roles.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Checar si existe

<!-- Ver si existe, comprobar si existe, saber si existe-->

#### una tabla

```sql
SELECT 1 FROM information_schema.columns WHERE table_schema='public' AND table_name='usuarios' AND column_name='roles'
```

#### Una secuencia

```sql
DO
$do$
BEGIN
  IF EXISTS (SELECT 1 FROM pg_catalog.pg_sequences WHERE sequencename = 'my_seq') THEN
    RAISE NOTICE 'Si existe la secuencia';
  ELSE
    RAISE NOTICE 'No existe la secuencia';
  END IF;
END
$do$;
```

#### Una Base de datos

```sql
SELECT 1 FROM pg_database WHERE datname = 'my_db'
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Comparar dos columnas de diferentes tablas

Por ejemplo tenemos las dos tablas siguientes una tabla de respaldo y
otra actual, y queremos saber sus diferencias entre estas dos columans
en este caso la de deleted_colums_from_usuarios con roles

A. Vemos que es lo que quermeos comparar

```r
"select * from deleted_colums_from_usuarios;"         "select id_usuario, roles from usuarios_list;"
 fk_id_usuario |        deleted_roles                  id_usuario |            roles
---------------+-----------------------------         ------------+-----------------------------
             1 | root, admin, editor                            1 | root, admin, editor
             2 | admin, editor                                  2 | admin, editor
             3 | editor                                         3 | editor,
             4 | viewer                                         4 | viewer
             5 | editor                                         5 | editor
             6 | editor, publicador-ciencias                    6 | editor, publicador-ciencies
             7 | admin, publicador-medicina                     7 | admin, publicador-medicina
             8 | publicador-ciencias                            8 | publicador-ciencies
             9 | publicador-tecnologia                          9 | publicador-tecnologia
            10 | admin, viewer, editor, root                   10 | root, admin, editor, viewer
(10 rows)
```

B. Comparamos columans de diferentes tablas

```sql
SELECT DISTINCT
  CASE WHEN usr.roles <> del_r.deleted_roles
    THEN 'Cuidado hay difrencias en id usuario: ' || usr.id_usuario || '->  ' || usr.roles || ' != ' || del_r.deleted_roles
    ELSE '0'
  END
  AS diff
from usuarios_list as usr
INNER JOIN deleted_colums_from_usuarios AS del_r
ON usr.id_usuario = del_r.fk_id_usuario
;

-- RESULTADO
                                                  diff
--------------------------------------------------------------------------------------------------------
 0
 Cuidado hay difrencias en id usuario: 10->  root, admin, editor, viewer != admin, viewer, editor, root
 Cuidado hay difrencias en id usuario: 3->  editor != editor,
 Cuidado hay difrencias en id usuario: 6->  editor, publicador-ciencias != editor, publicador-ciencies
 Cuidado hay difrencias en id usuario: 8->  publicador-ciencias != publicador-ciencies
(5 rows)
-- NOTA: EN CASO DE QUE NO UBIERA DIFERENCIAS MOSTRARIA EL SOLO EL PRIMER ROW CON '0'
```

**Resultado:** Pudimos ver cuales son las difetrencias entre dos columans
de dos tablas diferentes

### Unir varias filas en una columna separadas por coma (mucho-a-muchos/3-tablas)

**Objetivo:** Tenemos tres tablas diferentes (usuarios, usuarios_roles, roles)
las cuales reprecentan una relacion muchos-a-muchos, y queremos traer
una de estas relaciones a una sola columana para ver mejor esta relacion.

En este caso queremos ver los nombres de los roles que tiene cada usuario
separados por coma en una columna

A. Informacion disponible

```r
"SELECT id_rol, nombre nombre FROM roles;"     "SELECT id_usuario, nombre FROM usuarios;"
 id_rol |        nombre                           id_usuario | nombre
--------+-----------------------                 ------------+---------
     99 | root                                             1 | Mario
    100 | admin                                            2 | Neey
    101 | editor                                           3 | Pedro
    105 | viewer                                           4 | Sancho
    108 | publicador-ciencias                              5 | Carla
    110 | publicador-medicina                              6 | Linda
    112 | publicador-tecnologia                            7 | Julio
          (7 rows)                                         8 | Carlos
                                                           9 | Carlos
                                                          10 | Delia
                                                          12 | Gustavo
                                                          11 | Homero
                                                                (12 rows)
      "SELECT * FROM usuarios_roles ORDER BY fk_id_usuario;"
                      fk_id_usuario | fk_id_rol
                     ---------------+-----------
                                  1 |        99
                                  1 |       100
                                  1 |       101
                                  2 |       100
                                  2 |       101
                                  3 |       101
                                  4 |       105
                                  5 |       101
                                  6 |       101
                                  6 |       108
                                  7 |       100
                                  7 |       110
                                  8 |       108
                                  9 |       112
                                 10 |        99
                                 10 |       100
                                 10 |       101
                                 10 |       105
                              (18 rows)
```

B. Hacemos la siguiente consulta la cual nos juntara los nombres de los
roles que tiene cada usuario

- `string_agg(`: Es el que juntara los registros pero necesitaremos
  agrupar por nombre de usuario o su id en este caso uso el id
- `FULL JOIN usuarios`: traigo todas las considencias entre usuarios_roles
  y usuarios, le dejamos el `full` para que tambien nos muestre aquellos
  usuarios que no tienen ningun permiso asignado
- `FULL JOIN roles`: Lo mismo pero entre usuarios_roles y roles
- `GROUP  BY`: es necesario ya que tenemos varios usuarios y queremos
  ver solo una columna de roles, osea por un usuario tenemos varios roles

```sql
--Para manejar cvs , string_agg(r.nombre, ', ') as roles,
--Para menajar erreglos, array_agg(r.nombre) as roles,
SELECT usr.id_usuario, usr.nombre, string_agg(r.nombre, ', ') AS roles
FROM usuarios_roles AS ur
FULL JOIN usuarios AS usr ON ur.fk_id_usuario = usr.id_usuario
FULL JOIN roles AS r ON ur.fk_id_rol = r.id_rol
GROUP  BY usr.id_usuario
ORDER BY usr.id_usuario
;
```

**Resultado:** Podemos ver el nombre del rol que tiene cada usuario, y
ademas el nombre del usuario.

```r
 id_usuario | nombre  |            roles
------------+---------+-----------------------------
          1 | Mario   | root, admin, editor
          2 | Neey    | admin, editor
          3 | Pedro   | editor
          4 | Sancho  | viewer
          5 | Carla   | editor
          6 | Linda   | editor, publicador-ciencias
          7 | Julio   | admin, publicador-medicina
          8 | Carlos  | publicador-ciencias
          9 | Carlos  | publicador-tecnologia
         10 | Delia   | root, admin, editor, viewer
         11 | Homero  |
         12 | Gustavo |
(12 rows)
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Copiar columna a otra tabla

```sql
INSERT INTO tabla_donde_se_copiara(col1, col2)
    SELECT col1A, col2B FROM tabla_a_extraer_columna;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Ver el tamaño de una columna

**Ejemplo:** Tenemos una tabla la cual una de sus columnas, es un campo
grande, por ejemplo, una imagen en base64, podemos saber el tamaño que
ocupa esta imagen con la siguiente funcion.

```sql
-- TABLA
                            Table "public.product_images"
      Column      |         Type          | Collation | Nullable | ...
------------------+-----------------------+-----------+----------+----
 product_image_id | integer               |           | not null | ...
 base64           | text                  |           | not null |
 image_name       | character varying(50) |           | not null |

-- COMANDO
select pg_column_size(base64) FROM product_images;

-- OUTPUT
 pg_column_size
----------------
            644
           1152
(2 rows)

```

**Resultado**: Podemos ver el tamaño del campo, en el cual en este caso
la imagen original pesaban 475b y 860b, pero al pasarlo a base64, se incrementa
el tamaño un 30%, por lo que podemos ver que pesan 644b y 1152b.

<!--                              TERMINA "POR EL CAMINO"

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Errores

### Error al agregar columna con default

> **Error:** multiple default values specified for column.  
> **Descripcion:** Al querer ejecutar la sig. consulta para agregar una
> nueva columna con un default con una consulta.

```sql
ALTER TABLE public.usuarios ADD COLUMN fk_rol serial NOT NULL DEFAULT id_by_rolname_in_tlb_rol('visit');
```

> **CAUSA:** El campo serial hace que se agrege un campo incremental,
> y al default le estamos diciendo que ponga el resultado del query por
> lo que no sabe cual asignar y nos da el error de multiples valores.
> **solucion:** Cambiar el tipo de dato a nuestra columna, que no este
> indicando que valor poner por ejemplo.

```sql
-- es lo mismom solo le pasamos de serial a integer
ALTER TABLE public.usuarios ADD COLUMN fk_rol integer NOT ...
```

----

### Error uso de secuencia

**Error:** permission denied for sequence xxx_seq.  
**Descripcion:** Al querer insertar un nuevo registro marca este error
**Causa:** Cuando creamos la BD quitamos todos los permisos, por lo que
eso inclullo el uso/actualizacion de las sequencias
