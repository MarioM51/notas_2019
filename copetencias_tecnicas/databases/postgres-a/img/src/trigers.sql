
CREATE DATABASE trigers OWNER estudiante TABLESPACE tlb_misdatos;

DROP TABLE IF EXISTS productos;

CREATE TABLE productos(
	id_producto SERIAL PRIMARY KEY,
	nombre VARCHAR(40) UNIQUE NOT NULL,
	cantidad REAL NOT NULL,
	precio REAL NOT NULL,
	ultima_mod TIMESTAMP,
	usuario VARCHAR(50), 
	vencimiento DATE 
);



INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Yogurt', 	1.50, 	1,	'2019-09-20', '2019-08-01 12:00:00', 'mario');
INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Paquetaso',	200, 	2,	'2019-09-28', '2019-08-01 11:00:00', 'mario');
INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Vols', 		300,	3,	'2019-10-01', '2019-08-01 10:00:00', 'sutano');
INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Bonicula', 	400, 	4,	'2019-10-27', '2019-08-01 09:00:00', 'sutano');
INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Fire', 		500, 	5,	'2019-11-01', '2019-08-01 08:00:00', 'mario');
INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Chocolate', 	600, 	6,	'2019-11-27', '2019-08-01 07:00:00', 'mario');
INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Vols barato',700, 	7,	'2019-12-01', '2019-08-01 06:00:00', 'sutano');
INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Canabis', 	800, 	8,	'2019-12-27', '2019-08-01 05:00:00', 'neey');

INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Lapizero', 	900, 	9,	'2020-01-01', '2019-08-01 04:00:00', 'mario');
INSERT INTO productos(nombre, precio, cantidad, vencimiento, ultima_mod, usuario) VALUES ('Raton', 		1000, 	10,	'2020-01-27', '2019-08-01 03:00:00', 'mario');


SELECT * FROM productos;



CREATE TABLE auditoria_productos(
 accion varchar(20),
 fecha TIMESTAMP,
 nombre VARCHAR(40),
 cantidad REAL,
 precio REAL
);



SELECT * from auditoria_productos;






-- #################################################################################
DROP TABLE IF EXISTS ventas;
CREATE TABLE ventas (
	id_venta 	SERIAL PRIMARY KEY, 
	descripcion VARCHAR(240) NOT NULL, 
	cantidad 	SMALLINT NOT NULL, 
	fecha 		DATE NOT NULL, 
	id_producto SERIAL, 
	CONSTRAINT fk_productos_ventas FOREIGN KEY (id_producto) 
					REFERENCES productos(id_producto) 
					ON DELETE SET NULL ON UPDATE SET NULL 
);


SELECT * FROM ventas;














-- #################################################################################
DROP TABLE IF EXISTS compras;
CREATE TABLE compras (
	id_compra 	SERIAL PRIMARY KEY, 
	descripcion VARCHAR(240) NULL, 
	id_producto SERIAL, 
	cantidad 	SMALLINT NOT NULL, 
	fecha 		DATE NOT NULL, 
	
	CONSTRAINT fk_productos_compras FOREIGN KEY (id_producto) 
					REFERENCES productos(id_producto) 
					ON DELETE SET NULL ON UPDATE SET NULL 
);
--

INSERT INTO compras(descripcion, cantidad, fecha, id_producto) VALUES('Pa enero', 3, '2019-01-01', 1);


SELECT * FROM compras;



