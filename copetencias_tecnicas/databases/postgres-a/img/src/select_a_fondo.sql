

CREATE DATABASE select_a_fongo OWNER = estudiante TABLESPACE = tlb_misdatos;


CREATE TABLE colores(
	nombre VARCHAR(20) PRIMARY KEY,
	valor VARCHAR(7) UNIQUE
);

INSERT INTO colores VALUES('Negro', '#000000');
INSERT INTO colores VALUES('Azul', '#0000FF');
INSERT INTO colores VALUES('verde', '#00FF00');
INSERT INTO colores VALUES('Rojo', '#FF0000');
INSERT INTO colores VALUES('banco', '#FFFFFF');
INSERT INTO colores VALUES('amarillo', null);

SELECT * FROM colores;






-- ###############################################################################33
DROP TABLE IF EXISTS nombres;
CREATE TABLE nombres(
	nombre VARCHAR(40) UNIQUE,
	edad SMALLSERIAL
);

INSERT INTO nombres VALUES ('Manuel Iglesias Cobo', 	18);
INSERT INTO nombres VALUES ('Inés Martínez Rey',		28);
INSERT INTO nombres VALUES ('Julio del Río Ramos', 		38);
INSERT INTO nombres VALUES ('Clara Rodríguez Pinto', 	12);
INSERT INTO nombres VALUES ('Pedro Rico Martín',		15);
INSERT INTO nombres VALUES ('Oscar Martín Jiménez',		19);
INSERT INTO nombres VALUES ('Raúl Arce Gallardo',		20);
INSERT INTO nombres VALUES ('Sonia Ruiz de André',		22);
INSERT INTO nombres VALUES ('Martín Jiménez',		24);
INSERT INTO nombres VALUES ('Mario Marquez V.', 	18);

INSERT INTO nombres VALUES ('Manuel Iglesias Cobo 2', 	18);
INSERT INTO nombres VALUES ('Inés Martínez Rey 2',		28);
INSERT INTO nombres VALUES ('Julio del Río Ramos 2', 		38);
INSERT INTO nombres VALUES ('Clara Rodríguez Pinto 2', 	12);
INSERT INTO nombres VALUES ('Pedro Rico Martín 2',		15);
INSERT INTO nombres VALUES ('Oscar Martín Jiménez 2',		19);
INSERT INTO nombres VALUES ('Raúl Arce Gallardo 2',		20);
INSERT INTO nombres VALUES ('Sonia Ruiz de André 2',		22);
INSERT INTO nombres VALUES ('Martín Jiménez 2',		24);
INSERT INTO nombres VALUES ('Mario Marquez V. 2', 	18);

INSERT INTO nombres VALUES ('Manuel Iglesias Cobo 3', 	18);
INSERT INTO nombres VALUES ('Inés Martínez Rey 3',		28);
INSERT INTO nombres VALUES ('Julio del Río Ramos 3', 		38);
INSERT INTO nombres VALUES ('Clara Rodríguez Pinto 3', 	12);
INSERT INTO nombres VALUES ('Pedro Rico Martín 3',		15);
INSERT INTO nombres VALUES ('Oscar Martín Jiménez 3',		19);
INSERT INTO nombres VALUES ('Raúl Arce Gallardo 3',		20);
INSERT INTO nombres VALUES ('Sonia Ruiz de André 3',		22);
INSERT INTO nombres VALUES ('Martín Jiménez 3',		24);
INSERT INTO nombres VALUES ('Mario Marquez V. 3', 	18);

SELECT * FROM nombres;


-- ###############################################################################
DROP TABLE IF EXISTS ventas;
DROP TABLE IF EXISTS productos;

CREATE TABLE productos(
	id_producto SERIAL PRIMARY KEY,
	nombre VARCHAR(40) UNIQUE NOT NULL,
	precio REAL NOT NULL,
	vencimiento DATE 
);
INSERT INTO productos(nombre, precio, vencimiento) VALUES ('Yogurt', 12.50, '2019-09-20');
INSERT INTO productos(nombre, precio, vencimiento) VALUES ('Paquetaso', 20, '2019-09-28');
INSERT INTO productos(nombre, precio, vencimiento) VALUES ('Vols', 220.50, '2019-10-01');
INSERT INTO productos(nombre, precio, vencimiento) VALUES ('Bonicula', 1, '2019-10-27');
INSERT INTO productos(nombre, precio, vencimiento) VALUES ('Fire', 20, '2019-11-01');
INSERT INTO productos(nombre, precio, vencimiento) VALUES ('Chocolate', 5, '2019-11-27');
INSERT INTO productos(nombre, precio, vencimiento) VALUES ('Vols barato', 111.50, '2019-12-01');
INSERT INTO productos(nombre, precio, vencimiento) VALUES ('Canabis', 40, '2019-12-27');

-- ####################################################
CREATE TABLE ventas (
	id_ventas 	SERIAL PRIMARY KEY, 
	descripcion VARCHAR(240) UNIQUE NOT NULL, 
	cantidad 	SMALLINT NOT NULL, 
	fecha 		DATE NOT NULL, 
	id_producto SERIAL, 
	CONSTRAINT fk_productos_ventas FOREIGN KEY (id_producto) 
					REFERENCES productos(id_producto) 
					ON DELETE SET NULL ON UPDATE SET NULL 
);
INSERT INTO ventas(descripcion, cantidad, fecha, id_producto) VALUES('Pa septiembre', 3, '2019-01-01', 1);
INSERT INTO ventas(descripcion, cantidad, fecha, id_producto) VALUES('Pa noviembre', 4, '2019-02-01', 1);
INSERT INTO ventas(descripcion, cantidad, fecha, id_producto) VALUES('Pa enero', 5, '2019-03-01', 2);
INSERT INTO ventas(descripcion, cantidad, fecha, id_producto) VALUES('Pa julio', 6, '2019-04-01', 3);
INSERT INTO ventas(descripcion, cantidad, fecha, id_producto) VALUES('Pa agosto', 7, '2019-05-01', 4);
INSERT INTO ventas(descripcion, cantidad, fecha, id_producto) VALUES('Pa Junio', 3, '2019-06-01', 4);
INSERT INTO ventas(descripcion, cantidad, fecha, id_producto) VALUES('Pa enero 2', 5, '2019-01-01', 7);

SELECT * FROM ventas;







-- ###############################################################################33
DROP TABLE IF EXISTS repetidos;

CREATE TABLE repetidos (
	id SERIAL PRIMARY KEY,
	color1 VARCHAR(20),
	color2 VARCHAR(20)
);

--aleatorios
INSERT INTO repetidos(color1, color2) VALUES('rosa', 'violeta');
INSERT INTO repetidos(color1, color2) VALUES('negro', 'cafe');

--reoetidos
INSERT INTO repetidos(color1, color2) VALUES('rojo', 'azul');
INSERT INTO repetidos(color1, color2) VALUES('rojo', 'azul');

--repetidos derecha
INSERT INTO repetidos(color1, color2) VALUES('amarillo','rojo');
INSERT INTO repetidos(color1, color2) VALUES('blanco', 	'rojo');

--repetidos izquierda
INSERT INTO repetidos(color1, color2) VALUES('verde',	'amarillo');
INSERT INTO repetidos(color1, color2) VALUES('verde', 	'cafe');

--repetidos en columnas
INSERT INTO repetidos(color1, color2) VALUES('amarillo','amarillo');
INSERT INTO repetidos(color1, color2) VALUES('cafe', 	'cafe');

SELECT * FROM repetidos;




