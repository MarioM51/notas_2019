# Curso completo de PL/SQL

## 1. Introducción a intalacion y PL/SQL

- Aprenderemos a dominar PL/SQL de cero a avanzado
- Conocimientos y buenas practicas
- Ejemplos reales y ejercicios prácticos
- Herramientas
  - SQL Developer
  - Oracle Expres

### 2. Instalación del ambiente de trabajo

Descargamos e instalamos  las siguientes herramientas

- Base de datos Oracle XE  http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html
- SQL Developer http://www.oracle.com/technetwork/developer-tools/sql-developer/downloads/index.html
- Java JDK http://www.oracle.com/technetwork/es/java/javase/downloads/index.html


### 3. Conexión a la base de datos
<!--  abrir consola de oracle 11g, abrir linea de comandos oracle 11g, cli oracle cli -->
1. Instalamos e iniciamos oracle 11g instalador file://E:\Progamas\Java\OracleXE112_Win64.zip
  1.1. next, next... le ponemos contraseña en mi caso "12345678".
  1.2. Win + R -> services.msc -> iniciamos los servicios: "OracleServiceXE", "OracleXETNSListener", "OracleMTSRecoveryService" -> Start the service.   <!-- iniciar orable 11g, iniciar 11g orable, iniciar oracle sql -->

2. Abrir terminal de oracle.
  2.1. Agregamos a variables de entorno o vamos a la ruta bin/ de nuestra instalacion de oracle en mi caso  E:\Progamas\Java\oracle11g\app\oracle\product\11.2.0\server\bin
  2.2. Vamos a cmd y ejecutamos "sqlplus.exe" o "sqlplus".

3. Abrimos el SQL-Developer y agregamos una nueva conexión, en mi caso file://E:\Progamas\Java\sqldeveloper
  3.1. Ponemos los datos del usuario system (system:12345678), el cual definimos en la instalacion.
  3.2. hacemos test de conexion y "save".

4. Creamos un nuevo usuario ya que system tiene demasiados permisos.
  4.1. Vamos a conexión system (desplegamos) -> Otros Usuarios (clic der) -> *Crear Usuario
  4.2. Asignamos usuario y contraseña, roles y permisos (le otorgamos todos).

**Error**: Permisos insuficientes.
**Causa**: Tengo algo raro con mi usuario system.
**Descripción**: A la hora de querer crear un usuario desde SQL-Developer me saca este error.
**Link**: https://docs.oracle.com/cd/E17781_01/admin.112/e18585/toc.htm#XEGSG110
**Solución**: PROVICIONAL, hacerlo desde la consola.

```sql
create user <USER-NAME> identified by <PASSWORS>;

grant CREATE SESSION, ALTER SESSION, CREATE DATABASE LINK, CREATE MATERIALIZED VIEW, CREATE PROCEDURE, CREATE PUBLIC SYNONYM, CREATE ROLE, CREATE SEQUENCE, CREATE SYNONYM, CREATE TABLE, CREATE TRIGGER, CREATE TYPE, CREATE VIEW, UNLIMITED TABLESPACE to <USER-NAME>;
```

5. Creamos una nueva conexión con el usuario que acabamos de crear

### 4. Hola Mundo en PLSQL

1. Nos conectamos por la nueva conexion que apenas creamos y en el editor de texto

que tenemos a la derecha ponemos lo siguiente.
  1.1. Ejecutamos y deberiamos de poder ver el mensaje de hola.

**CUIDADO**: Puede que no se vea nuestro mensaje.
**CAUSA**: Lo mas seguro es que ahi una incopativilidad de versiones [link.](https://stackoverflow.com/questions/49803808/sqldeveloper-dbms-output-put-line-doesnt-work?rq=1)
**SOLUCION:** Pero igual podemos ver en la barra superior -> View -> **Dbms Output** y agregamos la conexion que estamos usando.

```sql
SET SERVEROUTPUT ON; --para que se muestren mensajes por consola
DECLARE
    --definicion de variables y funciones
BEGIN
    --Definimos cuerpo de la funcion
    DBMS_OUTPUT.PUT_LINE('Hola desde PL/SQL');
END;
```

![23_ejem_normalizacion.PNG](./img/hola_mundo.PNG)

### 5. Manejo de variables basicas
<!-- concatenar dos cadenas, concatenar dos string, concatenar dos variables, concatenar 2 cadenas, concatenar 2 string, concatenar 2 variables, concatenar cadenas, concatenar strings, concatenar variables -->
<!-- crear fecha, iniciar fecha, -->
Declaracion de variables :speak_no_evil:

```sql
SET SERVEROUTPUT ON;
DECLARE
    --definicion de variables y funciones
    salario NUMBER := 1500;
    nombreEmpleado NVARCHAR2(100);
    activo BOOLEAN;
    fecha DATE;
    hoy DATE;
BEGIN
    nombreEmpleado := 'Mario Marquez';
    fecha := TO_DATE('2003/07/09', 'yyyy/mm/dd');
    hoy := SYSDATE;
    DBMS_OUTPUT.PUT_LINE('Nombre: ' || nombreEmpleado);
    DBMS_OUTPUT.PUT_LINE('Salario: ' || salario);
    DBMS_OUTPUT.PUT_LINE('fecha: ' || fecha);
    DBMS_OUTPUT.PUT_LINE('hoy es: ' || hoy);
END;
```

Nota: Al querer imprimir el voleano no prodremos, nos marcara un error de mal uso de tipos, para lo cual deberemos hacer uso de case.

```sql
DBMS_OUTPUT.PUT_LINE('activo: ' || CASE WHEN activo then 'Activo' ELSE 'NO-ACTIVO' END);
```

### 6. Constantes

Variables que no cambian, esto sirve para declarar cosas como PI, porcentajes etc.

**OBJETIVO**: vamos a calcular el area de un círculo.
**OBJETIVO_2**: vamos a redondear un numero, ya que al multiplicar PI nos apareceran muchos decimales lo cual no es necesario.

```sql
SET SERVEROUTPUT ON;
DECLARE
  PI CONSTANT NUMBER := 3.141559;
  area NUMBER;
  radio NUMBER;
BEGIN
  radio := 7;
  area := PI * (radio * radio);
  DBMS_OUTPUT.PUT_LINE('Area: ' || ROUND(area, 2) || ' cm2');
END;
```




### 07 - Valores de un Query a variables - INTO

1. Creamos un nuevo archivo (file -> new -> All Items -> SQL File) y
seleccionamos el usaurio con que ejecutarlo.

<!-- contar registros de una consulta, contar filas de una columna, contar resultados de una consulta -->
<!-- saber usuario actual de la conexion, saber que usuario esta conectado, ver usuario actual de la conexion, ver que usuario esta conectado, mostrar usuario actual, mostrar que usuario esta conectado -->

```sql
SET SERVEROUTPUT ON;
DECLARE
    usuario_actual VARCHAR2(30);
    cantidad NUMBER;
BEGIN
    SELECT USER INTO usuario_actual FROM dual;
    DBMS_OUTPUT.PUT_LINE('Usuario actual:' || usuario_actual);

    SELECT COUNT(*) INTO cantidad FROM dual;
    DBMS_OUTPUT.PUT_LINE('Cantidad:' || cantidad);
END;
```

2. Podemos hacer **lo mismo de una forma mas eficiente** si en la misma
consulta obtenemos ambos datos, a lo cual podremos guardar ambos datos
en dos variables, solo deberemos de respetar el orden
```sql
SET SERVEROUTPUT ON;
DECLARE
  espacios VARCHAR2(30);
BEGIN
  espacios := " espacios al inicio y final ";
  DBMS_OUTPUT.PUT_LINE('espacios sin trim:' || length(espacios));
  DBMS_OUTPUT.PUT_LINE('espacios con trim:' || length(TRIM(espacios)));
END;
```



### 08 - Funciones creadas por Oracle
- Lista de funciones oracle: https://www.techonthenet.com/oracle/functions/index.php

<!-- TRIM() eliminar espacios de cadena, eliminar espacios en blanco de cadena, eliminar espacios vacios de cadena, eliminar espacios de texto, eliminar espacios en blanco de texto, eliminar espacios vacios de texto, eliminar espacios de string, eliminar espacios en blanco de string, eliminar espacios vacios de string -->
<!-- funciones de oracle funciones oracle -->
1. uso de funciones.
```sql
SET SERVEROUTPUT ON;
DECLARE
  espacios NVARCHAR2(100);
  hoy DATE;
  algo NVARCHAR2(10);
BEGIN
  DBMS_OUTPUT.PUT_LINE('####################- CADENAS -####################');
  espacios := ' Espacios Al Inicio Y Final ';
  DBMS_OUTPUT.PUT_LINE('espacios sin trim: ' || length(espacios));
  DBMS_OUTPUT.PUT_LINE('espacios con trim: ' || length(TRIM(espacios)));
  DBMS_OUTPUT.PUT_LINE('a mayusculas: ' || upper(espacios));
  DBMS_OUTPUT.PUT_LINE('a mayusculas: ' || lower(espacios));
  DBMS_OUTPUT.PUT_LINE('cortar: ' || substr(espacios, 10, LENGTH(espacios)));
  DBMS_OUTPUT.PUT_LINE('Remplazar: ' || replace(espacios, 'Inicio', 'Comienzo'));
  /* algo := '';  lo detecta como nulo  algo := ' ';  lo detecta como vacio */
  DBMS_OUTPUT.PUT_LINE('Detectar nulos: ' || NVL(algo, 'Es nulo'));

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('####################- FECHAS -####################');
  hoy := SYSDATE;
  DBMS_OUTPUT.PUT_LINE('hoy:' || hoy);
  DBMS_OUTPUT.PUT_LINE('Agregar 2 dias: ' || (hoy+2));
  DBMS_OUTPUT.PUT_LINE('Agregar 2 meses: ' || add_months(hoy, 2));
  DBMS_OUTPUT.PUT_LINE('Dia: ' || to_char(hoy, 'dd'));
  DBMS_OUTPUT.PUT_LINE('Reordenar fecha: ' || to_char(hoy, 'yyyy-MM-dd'));

END;
```


### 09 - Arreglos
```sql
SET SERVEROUTPUT ON;
DECLARE
  --definimos tipo de arreglo
  estudiantes_array IS VARRAY(5) OF NVARCHAR2(100);
  notas_array IS VARRAY(5) OF NUMBER;

  --definimos el arreglo con el tipo que definimos
  estudiantes estudiantes_array;
  notas notas_array;

BEGIN
  estudiantes := estudiantes_array('Fernando', 'Mario', 'Neey', 'Pancho', 'Mengano');
  notas := notas_array(6, 10, 8, 7, 5);

  DBMS_OUTPUT.PUT_LINE('Estudiante: ' || estudiantes(2) || 'Nota: ' || notas(2) );

  estudiantes(1) := 'Panfilo';
  DBMS_OUTPUT.PUT_LINE('Estudiante: ' || estudiantes(1) || 'Nota: ' || notas(1) );

END;
```


### 12 - Condicion IF - THEN y IF THEN ELSE
**Objetivo:** Crear un condicional que si la nota es igual a 67 o 68 esta sube a 70
y que imprima si aprobo o no.

<!-- Postgres estructura condicional estructura, estructura condicioneles estructura, estructura if estructura, estructura if estructura, estructura if else estructura, estructura if else estructura -->
uso de if, uso de if else.
```sql
SET SERVEROUTPUT ON;
DECLARE
  nota number := 66;
BEGIN
  IF (nota = 69 ) THEN
    nota := 70;
  END IF;
  DBMS_OUTPUT.PUT_LINE('Nota final A: ' || nota);

  IF (nota >= 70) THEN
    DBMS_OUTPUT.PUT_LINE('Aprobado');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Chin chin Reprovado');
  END IF;

END;
```

### 14 - Condicional CASE
**Objetivo** Crear un script que generar un valor aleatorio entre
1 al 5 y lo pasaremos a letra.
<!-- oracle default case default default en case en default -->
```sql
SET SERVEROUTPUT ON;
DECLARE
  aleatorio number := ROUND(DBMS_RANDOM.VALUE(1,5));
  letra NVARCHAR2(10);
BEGIN
  DBMS_OUTPUT.PUT_LINE('aleatorio: ' || aleatorio);
  CASE
    WHEN aleatorio = 1 THEN
      letra := 'uno';
    WHEN aleatorio = 2 THEN
      letra := 'dos';
    WHEN aleatorio = 3 THEN
      letra := 'tres';
    WHEN aleatorio = 4 THEN
      letra := 'cuatro';
    WHEN aleatorio = 5 THEN
      letra := 'cinco';
    ELSE -- EQUIVALENTE AL DEFAULT
      letra := 'default';
  END CASE;
  DBMS_OUTPUT.PUT_LINE(' Letra: ' || letra);
END;
```


### 14- Condicion IF-THEN-ELSIF
```sql
SET SERVEROUTPUT ON;
DECLARE
  nota number := 90;
BEGIN
  IF (nota >= 90) THEN
    DBMS_OUTPUT.PUT_LINE('Exelente');

  ELSIF (nota >= 80) THEN
    DBMS_OUTPUT.PUT_LINE('Muy bien');

  ELSIF (nota >= 70) THEN
    DBMS_OUTPUT.PUT_LINE('Bien');

  ELSE
    DBMS_OUTPUT.PUT_LINE('Mejora');
  END IF;

END;
```

### 15 - Operadores de evaluacion
<!-- operadores if operadores logicos en condicionales, operadores boleanos en condicionales "hay otro para el where" -->

- Diferente: ``` mi_var_a <> mi_var_b ```

- Igual: ``` mi_var_a = mi_var_b```

- Contiene: ``` mi_var_a LIKE '%algo%' ```

- entre: ```  mi_var_a BETWEEN 0 AND 10 ```

- tiene elemento de conjunto ``` mi_var_a IN (1, 5, 7, 10) ```

- es nulo ``` mi_var_a IS NULL ```

### 18 - Ciclo FOR
iteraciones, iterar, estructura for estructura basica, estructura basica for
```sql
SET SERVEROUTPUT ON;
DECLARE

BEGIN
 FOR i IN 1..10 LOOP
    DBMS_OUTPUT.PUT_LINE('Iteracion.. ' || i);
 END LOOP;
END;
```

Rotulando el ciclo, esto solo para agregar semantica al codigo, como comentarios
```sql
SET SERVEROUTPUT ON;
DECLARE

BEGIN
  <<recorrido_uno>>
  FOR i IN 1..10 LOOP
    DBMS_OUTPUT.PUT_LINE('Iteracion.. ' || i);
  END LOOP recorrido_uno;
END;
```

### 19 - Salir o Continuar en el ciclo ( EXIT y CONTINUE)
exit y continue
```sql
SET SERVEROUTPUT ON;
DECLARE

BEGIN
  <<recorrido_uno>>
  FOR i IN 1..10 LOOP
    if(i = 8) then
      DBMS_OUTPUT.PUT_LINE('loop detenido en 8');
      exit;
    end if;
    if(i = 2) then
      DBMS_OUTPUT.PUT_LINE('dos ignorado');
      continue;
    end if;
    DBMS_OUTPUT.PUT_LINE('Iteracion.. ' || i);
  END LOOP recorrido_uno;
END;
```


### 20 - Ciclo Basico - Loop
```sql
SET SERVEROUTPUT ON;
DECLARE
  numero number := 1;
BEGIN
  LOOP
    numero := numero + 1;
    if(numero = 8) then
      DBMS_OUTPUT.PUT_LINE('loop detenido en 8, si no este fuera infinito');
      exit;
    end if;
  END LOOP;
END;
```

## Procedimientos almacenados

### 26 - Preparando el ambiente de trabajo

ejecutamos el script para tener la info necesaria
```sql
CREATE TABLE empleados(id_empleado  NUMBER(2), nombre  NVARCHAR2(100), fecha_contra  DATE, salario  NUMBER(8,2), actualizado  DATE);
SET DEFINE OFF;
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(1, 'Mario Marquez', TO_DATE('2019/10/10 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 1200);
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(2, 'Neey Lima', TO_DATE('2019/10/12 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 1000);
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(3, 'Carlos Lima', TO_DATE('2019/10/14 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 800);
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(4, 'Panfilo Sanchez', TO_DATE('2019/10/16 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 600);
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(5, 'Sutano Sanches', TO_DATE('2019/10/18 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 400);
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(6, 'Carmen Saltillo', TO_DATE('2019/10/20 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 200);
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(7, 'Maria Lizol', TO_DATE('2019/10/22 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 200);
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(8, 'Daniel Lima', TO_DATE('2019/10/24 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 1100);
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(9, 'Fernando Zopilote', TO_DATE('2019/10/28 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 400);
INSERT INTO empleados(id_empleado, nombre, fecha_contra, salario) VALUES(10, 'Vanesa Vazquez', TO_DATE('2019/11/18 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 600);
COMMIT;
```


### 27 - Funcion Basica
1. crear funcion, <!-- ORACLE 11g crear funcion, definir funcion, declarar funcion estructura basica de funcion -->
```sql
create or replace FUNCTION total_empleados RETURN NUMBER
IS
  total NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO total FROM empleados;
  return total;
END;
```

2. usamo la funcion
```sql
SET SERVEROUTPUT ON;
DECLARE
  numero_empleados number := 0;
BEGIN
  numero_empleados := total_empleados();
  DBMS_OUTPUT.PUT_LINE('Total empleados: ' || numero_empleados);
END;
```



### 28 - Funciones con parametros de entrada
<!--UNA IGUAL PARA POSTGRES, ESTE ORACLE "funcion con parametros de entrada en funcion parametro de entrada, funcion con parametro de entrada",  -->
creamos la funcion
```sql
CREATE OR REPLACE FUNCTION mayor_entre(a in number, b in number) return number
IS

BEGIN
  IF(a > B) THEN
    RETURN a;
  ELSE
    return b;
  END IF;
END;
```

usamos la funcion
```sql
SET SERVEROUTPUT ON;
DECLARE
  numero_empleados number := 0;
  mayor number := 0;
BEGIN
  mayor := mayor_entre(8,1);
  DBMS_OUTPUT.PUT_LINE('El mayor es: ' || mayor);
END;
```


### 29 - Funcion Calculo de edad actual, uso de funcion en consulta
<!-- usar funcion en consulta -->
creamos la funcion
```sql
CREATE OR REPLACE FUNCTION get_edad(nacimiento in DATE) return number
IS
  edad number := 0;
BEGIN
  edad := round(trunc(to_number(to_char(sysdate, 'yyyymmdd')) - to_number(to_char(nacimiento, 'yyyymmdd'))) / 10000);
  return edad;

  EXCEPTION WHEN OTHERS THEN
    edad := 0;
  RETURN edad;
END;
```

usamos la funcion en la consulta
```sql
select nombre, FECHA_CONTRA, GET_EDAD(FECHA_CONTRA) AS tiempo FROM EMPLEADOS ORDER BY tiempo DESC;
```


### 30 - Definir funciones disponible solo dentro de PLs
**Objetivo:**  Crear funciones que solo esten disponibles dentro de PL/SQL actual, osea
solo se podra llamar la funcion en el procedimiento actual.
```java
SET SERVEROUTPUT ON;
DECLARE
  sueldo_promedio number := 0;

-- ================= INICIO obtener_sueldo_promedio ======================
FUNCTION obtener_sueldo_promedio return number
IS
  promedio number := 0;
BEGIN
  select avg(salario) INTO promedio FROM empleados;
  return promedio;
END;
-- ================= FIN obtener_sueldo_promedio ======================

BEGIN -- INICIO PROCEDIMIENTO PRINCIPAL
  sueldo_promedio := obtener_sueldo_promedio();
  DBMS_OUTPUT.PUT_LINE('Sueldo promedio: ' || sueldo_promedio);
END;
```


### 31 - Procedimiento Basico
Los procedimientos pueden alojarse en.
  - Dentro de nuestro esquema (tal como se guardan las funciones que podemos llamar en consultas).
  - Dentro de otro procedimiento como vimos arriba.
  - Dentro de un paquete (parecido a los paquetes de java)

<!-- diferencias entre funciones y procedimientos almacenados, diferencia entre funciones y procedimientos almacenados, diferencias entre procedimientos almacenados y funciones, diferencia entre  procedimientos almacenados y funciones, diferencias entre procedimientos y funciones, diferencia entre procedimientos y funciones -->
Diferencias entre procedimientos almecenados y funciones.
  - Las funciones pueden ser llamadas desde sentencias sql convencionales, mientras que los
  procedimientos no pueden.
  - Las funciones son comunmente usadas para computar informacion, mientras que los procedimientos
  para logica de negocio.

Estructura basica de procedimiento almacenado
<!--
procedimiento almacenado basico, estructura de procedimiento basico, declarar procedimiento almacenado, iniciar procedimiento almacenado, crear procedimiento almacenado
-->
```sql
CREATE OR REPLACE PROCEDURE hola_mundo AS
BEGIN
  DBMS_OUTPUT.PUT_LINE('Hola desde procedimiento almacenado');
END;
```

Ejecutar procedimiento almacenado <!-- iniciar procedimiento almacenado, correr procedimiento almacenado, usar procedimiento almacenado, -->
```sql
EXECUTE hola_mundo();
```


### 32 - Procedimiento con variables de Entrada y Salida
Hay 3 tipos de valores **IN** entrada, **OUT** salida y **IN_OUT** lo mismo que entra es lo
mismo que sale con alguna posible modificacion.

```sql
CREATE OR REPLACE PROCEDURE menor_entre(a in number, b in number, menor out number)
IS
BEGIN
  IF(a < b) THEN
    menor := a;
  ELSE
    menor := b;
  END IF;
END;
```

lo usamos dentro de un pl
```sql
SET SERVEROUTPUT ON;
DECLARE
  menor number := 0;
BEGIN
  menor_entre(4,6,menor);
  DBMS_OUTPUT.PUT_LINE('El numero menor es: ' || menor);
END;
```


### 35 - Cursor en tiempo de ejecucion (registros en variable e iterar consulta~tabla)
Un cursor es como una consulta guardada en memoria, la cual podemos acceder a un
registro y movernos hacia arriba o abajo entre los registros dentro del cursos

1. Creamos la siguiente tabla y la poblamos
```sql
CREATE TABLE NOTAS ("NOMBRE" VARCHAR2(100), "NOTA1" NUMBER(3,0), "NOTA2" NUMBER, "NOTA3" NUMBER, "NOTA4" NUMBER);
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Tonya Vazquez','100','80','99','77');
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Mathews Robbins','88','56','100','89');
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Walton Vincent','77','38','50','100');
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Audra Wade','92','93','93','80');
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Susanne Moody','70','67','78','83');
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Stevenson Dickson','45','90','70','89');
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Hinton Cooper','76','80','60','78');
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Hahn Brown','70','88','73','93');
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Bennett Brady','90','95','100','99');
Insert into NOTAS (NOMBRE,NOTA1,NOTA2,NOTA3,NOTA4) values ('Mueller Jimenez','65','70','85','96');
commit;
```

2. crear loop para recorrer consulta <!-- ORACLE recorrer tabla, iterar tabla, iterar consulta, HAY PARA POSTGRES -->
```sql
SET SERVEROUTPUT ON;
DECLARE
  promedio number := 0;
BEGIN
  FOR fila IN (select * from NOTAS ORDER BY promedio DESC) LOOP
    promedio := (fila.nota1 + fila.nota2 + fila.nota3 + fila.nota4) / 4;
    DBMS_OUTPUT.PUT_LINE('Promedio de: ' || fila.nombre || ' es ' || promedio);
  END LOOP;
END;
```

**Resultado:** Pudimos recorrer la tabla en un ciclo en que cada iteracion
dispusimos de toda la info de cada registro para poder hacer con ella lo que requiramos.

### 36 - Cursores Implicitos
Se crean cursores de forma automatica con los cuales podremos saber tres cosas.
 - Afecto y/o encontro registros? si/no.
 - Cuantos registros fueron afectados o encontrados.

**Nota:** Solo existe un cursor implicito que tiene informacion de la
ultima consulta, por lo que si tubiramos varias consultar este cambiaria
con cada consulta.
**Importante:** Estas variables se resetean despues de usar el commit, asi
que si consultamos una de estas variables despues de hacer commit veremos en
cero aun que en la consulta anterior si aigamos modificado.
```sql
SET SERVEROUTPUT ON;
DECLARE
  promedio number := 0;
BEGIN
  update empleados set actualizado = sysdate;
  --    Registros afectados: 10
  --    Hubo cambios?: Sip hubo cambios

  --update empleados set actualizado = sysdate where id_empleado > 500;
  --    Registros afectados: 0
  --    Hubo cambios?: No cambio nada

  --select id_empleado INTO promedio FROM empleados where ID_EMPLEADO=1;
  --    Registros afectados: 1
  --    Hubo cambios?: Sip hubo cambios

  -- cuantos registros fueron afectados
  DBMS_OUTPUT.PUT_LINE('Registros afectados: ' || sql%rowcount);


  -- hubo registros afectados
  IF(sql%found) THEN
    DBMS_OUTPUT.PUT_LINE('Hubo cambios?: Sip hubo cambios');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Hubo cambios?: No cambio nada');
  END IF;

END;
```
**Resultado:** Nos dira "Registros afectados: 10" ya que se modificaron
todos los registros de la tabla empleados que en mi caso tengo 10, y la
consulta de abajo deberia de no afectar ninguno.



### 37 - Cursores Explicitos
Es obligatorio usar la palabra reservada cursor para declarar uno

```sql
SET SERVEROUTPUT ON;
DECLARE
  nombre nvarchar2(100);
  salario number;
  CURSOR c_empleado IS select nombre, salario from empleados;
BEGIN
  OPEN c_empleado;
  LOOP
    FETCH c_empleado INTO nombre, salario;
    EXIT WHEN c_empleado%notfound;
    --Logica con nuestro cursor
    DBMS_OUTPUT.PUT_LINE('Nombre: ' || nombre || ' - Salario: ' || salario);
  END LOOP;
  CLOSE c_empleado;
END;
```



### 38 - Tipo de dato registro - Table base Record
Lo que hemos hecho hasta ahora para guardar los registros de un registro es transcribir
los campos de la tabla a variables, pero puede darnos el caso donde tendremos tablas
con demaciadas columnas lo que hara esto muy dificil.

Para esto podemos definir variables que tengan la estructura/campos de un registro y asi
poder guardar un resultado x dentro de una variable/objeto con la misma eestructura que
la de la tabla sin problemas.

**Nota** en rowtype solo podemos guardar un solo registro
<!-- ORACLE "Guardar registro en variable, Guardar fila en variable, Guardar consulta en variable, Guardar registro en una variable,
Guardar fila en una variable, Guardar consulta en una variable, guardar informacion en variable" HAY UNO IGUAL PARA POSGRESS -->
```sql
SET SERVEROUTPUT ON;
DECLARE
  registro_empleado empleados%ROWTYPE;
BEGIN
  select * INTO registro_empleado from empleados where id_empleado = 3;
  DBMS_OUTPUT.PUT_LINE('Nombre: ' || registro_empleado.nombre);
END;
```



### 39 - Registro definido por usuario - User defined record
Podemos ver un tipo de objeto el cual podremos definir nosotrs mismos.
```sql
SET SERVEROUTPUT ON;
DECLARE
  type carro is record(
    marca nvarchar2(100),
    modelo nvarchar2(100),
    puertas number
  );

  mazda carro;
  toyota carro;

BEGIN
  mazda.marca := 'Mazda';
  mazda.modelo := '98';
  mazda.puertas := 4;

  toyota.marca := 'toyota';
  toyota.modelo := '17';
  toyota.puertas := 3;

  DBMS_OUTPUT.PUT_LINE(mazda.marca);

END;
```


### 40 - Registros como parametros a funciones y procedimientos
Lo mismo que el de arriba pero usaremos un procedimiento para imprimir el carro.
```sql
SET SERVEROUTPUT ON;
DECLARE
  type carro is record(
    marca nvarchar2(100),
    modelo nvarchar2(100),
    puertas number
  );

  mazda carro;
  toyota carro;

-- #################### INICIA  ####################
  PROCEDURE imprimir_carro(mi_carro carro) is
  BEGIN
    DBMS_OUTPUT.PUT_LINE(mi_carro.marca);
    DBMS_OUTPUT.PUT_LINE(mi_carro.modelo);
    DBMS_OUTPUT.PUT_LINE(mi_carro.puertas);
  END;
-- #################### TERMINA ####################

BEGIN
  mazda.marca := 'Mazda';
  mazda.modelo := '98';
  mazda.puertas := 4;

  toyota.marca := 'toyota';
  toyota.modelo := '17';
  toyota.puertas := 3;

  imprimir_carro(mazda);
  imprimir_carro(toyota);

END;
```


### 44 - Manejo de excepciones
En este caso estamos usando una excepcion predefinida de oracle no_data_found y
la de others que manejara cualquier otra exepcion, la cual es buena practica usar
minimo esta.
```sql
SET SERVEROUTPUT ON;
DECLARE
  nombre nvarchar2(100);
BEGIN
  select nombre into nombre from empleados where id_empleado = 20;
  DBMS_OUTPUT.PUT_LINE(nombre);

  EXCEPTION
    WHEN no_data_found THEN
      DBMS_OUTPUT.PUT_LINE('Cliente no encontrado con ese codigo');
    WHEN others THEN
      DBMS_OUTPUT.PUT_LINE('Error no controlado');

END;
```

### 45 - Excepciones personalizadas.
```sql
SET SERVEROUTPUT ON;
DECLARE
  random number := round(dbms_random.value(1,3));
  error_1 exception;
  error_2 exception;
  error_3 exception;
BEGIN

  case
    when random = 1 then
      RAISE error_1;

    when random = 2 then
      RAISE error_2;

    when random = 3 then
      RAISE error_3;

  end case;


  EXCEPTION
    WHEN error_1 THEN
      DBMS_OUTPUT.PUT_LINE('Lanzamos el error 1');
    WHEN error_2 THEN
      DBMS_OUTPUT.PUT_LINE('Lanzamos el error 2');
    WHEN error_3 THEN
      DBMS_OUTPUT.PUT_LINE('Lanzamos el error 3');

END;
```

<br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br />

## Recordatorios

## Iniciar Servidor de mysql en windows

<!-- iniciar servidor mysql en windows, iniciar mysql en windows iniciar mysql -->

1. Desde UI
  1.1. Win + R -> services.msc -> Right-click on MySQL57 -> Start the service.



## Ejercicios

1. Actualizar los x registros que esten entre x valor

2. crear una BD con un arbol de usuarios que no sea el root, por ejemplo
un admin (manipulacion de tablas y permisos sobre la bd), editor (agregar, actualizar, eliminar), lector(solo leer) lector Basico (solo puede leer x tabla)

3. Hacer joins para obtener info como obtener todos los clientes idependientemente si
tienen ventas o no. Y un full join para ver todas los clientes con sus compras
y ver todad las compras independientemente si no tienen info de algunlado.

4. Uso de HAVING para hacer comparativa sobre un grupo, por ejemplo
mostrar aquellos productos que tengan menos de 10 en almacen.