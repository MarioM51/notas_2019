# Usuarios DevOps

- [Usuarios DevOps](#usuarios-devops)
  - [PENDIENTES](#pendientes)
  - [Resultados Finales](#resultados-finales)
  - [1. Objetivo](#1-objetivo)
    - [1.1. Objetivo 1, ambiente de desarrollo](#11-objetivo-1-ambiente-de-desarrollo)
  - [2. Crear ambiente](#2-crear-ambiente)
    - [2.2. Explicacion de carpetas y archivos a usar](#22-explicacion-de-carpetas-y-archivos-a-usar)
    - [2.3. Explicacion del docker-compose.yml](#23-explicacion-del-docker-composeyml)
    - [2.4. Manos a la obra Crear el ambiente](#24-manos-a-la-obra-crear-el-ambiente)
  - [3. base de datos](#3-base-de-datos)
    - [3.1. Crear D.B. en local y su repositorio git](#31-crear-db-en-local-y-su-repositorio-git)
    - [3.3. pipeline para subir cambios a ambiente: postgres-deploy](#33-pipeline-para-subir-cambios-a-ambiente-postgres-deploy)
    - [3.4. pipeline para des-hacer cambio subido: postgres-rollback](#34-pipeline-para-des-hacer-cambio-subido-postgres-rollback)
    - [3.5. pipeline para meter determinados .sql y/o respaldo de info: postgres-out-of-commit](#35-pipeline-para-meter-determinados-sql-yo-respaldo-de-info-postgres-out-of-commit)
  - [backend](#backend)
    - [Aspectos necesarios del backend](#aspectos-necesarios-del-backend)
    - [Script para iniciar la app y detectar cuando termine de hacerlo](#script-para-iniciar-la-app-y-detectar-cuando-termine-de-hacerlo)
    - [Pipeline para deploy ultimo commit](#pipeline-para-deploy-ultimo-commit)
  - [X. EMPEZAR DE NUEVO PERO AUTOMATIZADO](#x-empezar-de-nuevo-pero-automatizado)
  - [Base de datos: Postgres](#base-de-datos-postgres)
    - [Coomandos basicos](#coomandos-basicos)
    - [crear y ejecutar jenkinsfile v1](#crear-y-ejecutar-jenkinsfile-v1)
  - [Backend: Spring Boot](#backend-spring-boot)
    - [Comandos basicos](#comandos-basicos)
    - [Crear proyecto Spring](#crear-proyecto-spring)
    - [Cambiar la pagina de errores](#cambiar-la-pagina-de-errores)
  - [Preparar servicios/servidores para desarrollo](#preparar-serviciosservidores-para-desarrollo)
    - [Levantar servicios](#levantar-servicios)
    - [Levantar servicio de Jenkins con docker](#levantar-servicio-de-jenkins-con-docker)
    - [Cambiar .properties sengun ambiente](#cambiar-properties-sengun-ambiente)
  - [Spring deploy pipeline](#spring-deploy-pipeline)
  - [Spring pipeline backup](#spring-pipeline-backup)
  - [Postgres: Deploy - Deploy](#postgres-deploy---deploy)
    - [escribir pid en archivo](#escribir-pid-en-archivo)

## PENDIENTES

- diferencias con multiples hosts
- entrategia para cuidar que no se borren datos en la D.B. al desplegar cambio
- Crear dockerfile de jenkins con las herramientas necesarias
- instalar `netcat` a los contenedores ya que es util para checar cosillas de red
- Implementar logs en archivos de maximo tamaño y maximo de estos
  - [Link-pregunta](https://stackoverflow.com/questions/52479747/how-to-set-max-number-of-archived-logs-in-spring-boot)
  - [Link-documetacion](https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto-logging)

## Resultados Finales

Codido con DevOps y con base de datos evolutivas, usando jenkins, gitea,
docker, bashScript, Spring.

- ![Codigo](./img/_1_resultado_final_mdlmt.zip)

## 1. Objetivo

### 1.1. Objetivo 1, ambiente de desarrollo

**escenario** Se desarrollara de forma local pero cuando sea testeable
un cambio, se subira a un ambiente pre-preductivo (test, calidad), asi
todos los desarrolladores involucrados subiran a un ambiente, el cual
si suben un cambio que rempe sera mas facil identificarlo, esto para
los de backend y frontend con un sistema de DevOps tradicional, mientras
que para base de datos se usara una estrategia de base de datos evolutiva,
el cual sera parecido pero no igual al DevOps tradicional.

En resumen tenemos

1. Base de datos con postgres
2. Backend con spring
3. Front-end con angular
4. Un servidor con estas 3 distintas partes corriendo

**Problema a atacar:** Los procesos para implemetar los cambios en el ambientes
de test para cada parte de la aplicacion, deben de ir homologados con las ramas que
hay en el repositorio, lo cual hasta el momento esto depende de los
desarroladores por lo NO siempre existe esta homologacion por un margen
de error humano, por lo que surgen diferentes problemas como.

- codigo agregado al repositorio con errores.
- Codigo en produccion que no existe en ambientes previos.
- mayor dificultad para saber cual es el estado de la aplicacion.
- Difucultad para homologar un ambiente con otro, por ejemplo, pre-produccion
  con producccion.
- etc.

**Solucion:** Una estrategia de DevOps, donde cada vez que se realize
un commit en la rama de test este automaticamente de despliege en el
ambiente de test. nos basaremos en este [git flow](https://nvie.com/posts/a-successful-git-branching-model/)
para el manejo del repositorio,

**Requisitos:** Por facilidad y repidez los desarrolladores podran levantar
sus aplicaciones en sus maquinas locales, pero su aplicacion local debe
consumir el ambiente de test, para asegurar que sus nuevos cambios funcionan con
los cambios de los demas.

en el cual modificaremos.

1. En el servidor, instalaremos docker y docker-compose para poder
   tener tener las 3 partes de la aplicacion corriendo de una manera
   mas idependiente y repetible por si queremos pasar esto mismo a
   otro host ej. crear una carga distribuida, o crear un nuevo ambiente
   por ejemplo uno de pre-produccion.

    1.1. Un contenedor postgres 12 para deploy la D.B.  
    1.2. Un contenedor java11 para deploy de una API-Rest en Spring-Boot.  
    1.3. Un contenedor ngnix para deploy un FrontEnd en Angular+8.  

2. En el servidor de desarrollo, crearemos lo siguiente para poder
   automatizar las tareas de los deploy (implemetar DevOps).

   2.2. Un Contenedor Gitea para tener hacer a este los commits y que
   este por medio de githooks gatillar en Jenkins el proceso de implentacion.

   2.1. Un contenedor con Jenkins y Docker con ecceso al docker-host
   para hacer los procesos de automatizacion arrancables desde una url
   que usaran los githooks de gitea.

<!-- 

.

.

=============================================================================================================
=============================================================================================================

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

=============================================================================================================
=============================================================================================================

.

divicion seccion, divicion de seccion 2

.

-->

## 2. Crear ambiente

**Objetivo:** Crearemos las servicios/contenedores necesarios para
nuestra aplicacion de backend, como ya explicamos en `Objetivo 1` que
sera de tal manera que el proceso sea automatico y repetible, Ya que al
poder crear un contenedor a partir de un archivo que contiene las variables
de entorno, podremos modificar estas variables de entorno (IPs, Puertos,
Credenciales, enums, etc.) para que consuma servicios diferentes, y de
esta manera crear los ambientes necesarios por ejemplo test, calidad y
produccion.

### 2.2. Explicacion de carpetas y archivos a usar

**Objetivo** Explicar que estructura de carpetas y archivos implementaremos
para el servidor de desarrollo, el cual podra contener los diferentes
ambientes de desarrollo, tambien podra contener el de produccion, pero
se vera mas adelante como separar el ambiente de produccion del servidor
de desarrollo.

<!-- TODO: actualizar la estructura ya que hubo cambios en el camino-->
El siguiente [.zip](./img/_1_resultado_final_mdlmt.zip) contiene los archivos que va
a utilizar el docker-host.

1, Tenemos la siguiente estructura de carpetas donde guardaremos los
`ejecutables y la informacion` (ej. logs, archivos de D.B. etc.) que
se usaran dentro de nuestros contenedores.

- 1,1, Abajo describimos el uso de cada archivo y carpeta.

```r
docker/
│   "docker-compose.yml"  # detalles de la creacion de contenedores y sus volumentes los cuales hara referencia a estas carpetas y archivos
│
├───gitea/ # todo sobre gitea
│      "*" # archivos generados por el contenedor de gitea en teoria no movemos nada aqui
│
├───jenkins/
│       dockerfile-jenkinswdocker # la imagen oficial de jenkins no tiene doker asi que la modificamos/extendemos para que lo tenga
│
│
| # todo sobre base de datos postgres. Se separan los diferentes ambientes
| # con el sistema de git, y se recomenda fuertemente que los backups
| # se repliquen en otro servidor
| # IMPORTANTE: Entender que estos archivos son los que contendran los
| # contenedores, no el docker host, mas abajo se explica su uso.
├───postgres/ 
│   ├───app/
│   │   ├───backups/ # aqui guardamos los respaldos de la base de datos que se generan automaticamente
│   │   │       "last_backup" # es un archivo de texto con el nombre del ultimo respaldo que estaba funcionando
│   │   │       "01.usuarios_devops_database-2020-11-24--12-30-50.sql" # ejemplo de respaldo de base de datos
│   │   │
│   │   └───sql/ # guardamos los scripts sql que crearan la base de datos ya que para mejor orden los tenemos separados
│   │       └───v1/
│   │       │       "_install_schema_without_delete_data.sh" # llamamos todos aquellos .sql para instalar/modificar la estructura conservando la data actual
│   │       │       "_re_install_database_DELETING_data.sh" # elimina e instala el usr y db para sin ninguna estructura
│   │       │       "_re_poblate_data_DELETING_data.sh" # elimina y agrega registros
│   │       │       "001_create_table_usuarios.sql"
│   │       └───v2/
│   │             "*.sql"
│   │
│   └───data/ # usara esta carpta el contenedor de postgres para guardar/leer la B.D.
│           "*" # archivos creados por postgress en teoria no movemos nada aqui
│
│ # todo sobre backend en java-spring-boot,
│ # IMPORTANTE: entender que esta es una plantilla de archivos que contendran
│ # los contenedores, no son los archivos que se ejecutan, abajo se extiende
│ # la forma de usar los archivos
└───spring/ 
    │
    └───app/
        ├───backups/ # respaldo de los .jar antes en ejecucion
        │       "copy_previus_jar_to_exec_path.sh" # copiara el .jar anteriror a "jar/"
        │
        ├───jar/
        │       "usuarios_devops.jar" # aqui tenemos solo lo que esta en ejecucion en este caso solo un .jar
        │       "start-app.sh" # inicia la app respalda y reinicia los logs y busca en los logs para hacegurar que inicio correctamente
        │       "stop-app.sh" # toma el pid de la app de un archivo
        │       "pid.txt" # archivo temporal solo esta mientras se esta ejecutando la app y contiene el pid que ocupa esta
        │
        └───logs/ # aqui guardaremos los logs que generara nuestro backend
                "app.log"
```

### 2.3. Explicacion del docker-compose.yml

**Objetivo:** Describir este archivo que es el que creara nuestros
contenedores, indicando cosas como sus volumentes, puertos, usuarios,
red, ip, nombre, etc. lo cual es parte **fundamental** para automatizar
lo cual nos permitira **replicar** lo necesario en cualquier otro servidor.

1, Describimos el archivo [docker-compose.yml](./img/02-docker/docker-compose.yml)

NOTA: tiene una dependencia con [dockerfile-jenkinswdocker](./img/_antes/02-docker/jenkins/dockerfile-jenkinswdocker)
que es el archivo que define la creacion nuestro jenkisn personalizado
para poder usar docker dentro de este.

```yml
version: "3.3"

# es importante que todos tenga la misma red para que se puedan
# comunicar entre ellos sin problemas.
networks:
  usuarios_devops_net:
    driver: bridge
    ipam:
      config:
        - subnet: 166.10.0.0/24

services:

# NOTA: Solo levantamos y usamos no movemos nada para poder usarlo
  gitea_host:
    container_name: gitea
    image: gitea/gitea:1.12.3 # es recomendable definir version ya que si no al futuro descarga otra mas reciente la cual podria traernos problemas, igual a futuro podemos cambiarle manualmente para usar una mas reciente
    environment:
      - USER_UID=1000
      - USER_GID=1000
    restart: unless-stopped # asegura que estara activo incluso cuando reiniciemos la maquina, asta que nosotros la detengamos
    volumes:
      - ./gittea/gitea:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "3000:3000"
      - "222:22"
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.10 # definimos IPs estaticas ya que en lo personal me siento mas comodo usandolas, pero hay otras formas.

# NOTA: Solo levantamos y usamos no movemos nada para poder usarlo
  jenkins:
    container_name: jenkins
    build:
      context: ./jenkins/
      dockerfile: dockerfile-jenkinswdocker
    privileged: true
    user: root
    restart: unless-stopped
    ports:
      - "8082:8080"
    volumes:
      - ./jenkins/jenkins_home:/var/jenkins_home
      - /var/run/docker.sock:/var/run/docker.sock
      - /usr/local/bin/docker:/usr/local/bin/docker
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.11

# NOTA: para iniciar instalar la BD (estructura, datos etc.)  debemos de
# ejecutar dentro del contenedor " sh /var/app/sql/_install.sh ", por lo cual
# "_install.sh" es el que corre los archivos .sql para crear la bd
  postgres:
    container_name: postgres
    image: postgres:10
    ports:
      - 5003:5432
    restart: unless-stopped
    environment:
      POSTGRES_PASSWORD: mario_admin
      POSTGRES_USER: mario_admin
      POSTGRES_DB: usuarios_devops
    volumes:
      - ./postgres/data:/var/lib/postgresql/data
      - ./postgres/app:/var/app/
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.21

# NOTA: Usaremos "tail -f /app/logs/app.log" para mantener el contenedor por lo que despues de
# que se cree el contenedor deberemos de ejecuar en background  "java -jar -Dspring.profiles.active=test usuarios_devops.jar"
# para levantar nuestra aplicacion
  spring:
    container_name: spring
    image: maven:3.6-openjdk-11
    restart: unless-stopped
    ports:
      - "83:8080"
    volumes:
      - ./spring/app:/app/
    command: tail -f /app/logs/app.log
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.22

```

### 2.4. Manos a la obra Crear el ambiente

**IMPORTANTE**: Esto lo probamos en las siguientes versiones de docker
y docker compose.  

- Docker version 19.03.12, build 48a66213fe
- docker-compose version 1.26.2, build eefe0d31

**Objetivo:** vamos a descomprimir el el [comprimido](./img/01-01-docker.zip)
en un host donde tengamos instalado docker y docker compose, usaremos
a `docker-compose.yml` para crear los contenedores de forma automatica.

0, Instalar docker y docker-compose para hacer eso checar `copetencias_tecnicas/sistemas/docker/docker.md`.

1, Ya descomprimido en nuestro host a la altura del archivo `docker-compose.yml`
ejecutamos el comando

```r
docker-compose -f docker-compose.yml up -d
```

**Resultado:** debemos de tener creado los contenedores necesarios
por lo cual podremos hacer lo siguiente.

2, **comprobar** que tenemos correctamente levantados los contenedores

2,1, ejecutamos `docker ps``y podremos ver los 4 siguientes contenedores
levantados.

```r
> docker ps
# salida del comando
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                                         NAMES
699b6e6068e0        maven:3.6-openjdk-11   "/usr/local/bin/mvn-…"   7 hours ago         Up 7 hours          0.0.0.0:83->8080/tcp                          spring
abeef1fb0db7        postgres:10            "docker-entrypoint.s…"   34 hours ago        Up 34 hours         0.0.0.0:5003->5432/tcp                        postgres
c94a77391bd7        docker_jenkins         "/sbin/tini -- /usr/…"   6 days ago          Up 6 days           50000/tcp, 0.0.0.0:8082->8080/tcp             jenkins
a4392a588d5e        gitea/gitea:1.12.3     "/usr/bin/entrypoint…"   7 days ago          Up 6 days           0.0.0.0:3000->3000/tcp, 0.0.0.0:222->22/tcp   gitea
```

2,2, entrar a las siguintes rutas desde el navegador web para ver que
tenemos los sevicios levantados

- Servicion de jenkins: `http://192.168.1.86:8082`
- Servicio de gitea: `http://192.168.1.86:3000/`

2,3, ejecutar los siguientes comandos para entrar a contenedores y
saber que estan bien.

<!-- TODO: extender comprobacion -->

- `docker exec -ti spring bash # y luego java -version debemos tener java11`
- `docker exec -ti postgres bash # y luego nos conectamos a por psql a nuestra db`

**Resultado-Capitulo:** creamos los contenedores y comprobamos que esten
bien levantados.

## 3. base de datos

- RECORDAR QUE

  - Los puntos `3.1` y `3.2` no deben formar parte de nuestro trabajo
    comun, solo los ponemos para entender bien el proceso que se va a
    automatizar

  - en nuestro trabajo comun usaremos solo los pipelines que a su vez esto
    usaran los repositorios en gitea para implementar los cambios en
    el ambiente

  - Aparentemente vamos a tener duplicados nuestros archivos dentro
    de la carpeta `docker/postgres/sql/` pero realmente **NUNCA**
    deberiamos de editar en esta carpeta, ya que aqui esta solo lo del
    ultimo o actual **commit** implementado en el ambiente.

### 3.1. Crear D.B. en local y su repositorio git

**Objetivo:** Crearemos un conjunto de archivos .sql y un script bash
para ejecutar todos estos archivos .sql en orden para crear nuestra db,
y tenerlo en un repositorio `git` para subirlo a `gitea` y asi con `jenkins`
en un pipeline se  pueda descargar hacer deploy en el ambiente que
necesitemos.

1, Creamos una carpeta en donde tendremos todos nuestros scripts .sql
para crear nuestra D.B. por ejemplo `.usuarios_devops_db/` en mi caso.
`voy a terminar` con la siguiente estructura.

```r
│   "jenkinsfile" # archivo que contiene el pipeline para hacer deploy
│
├───backups/ # respaldos que querramos tener en local
│       "relleno.txt"
│
└───bash_script/ # aqui trabajaremos en la creacion de nuestra D.B.
        "_install_schema_without_delete_data.sh" # instalar la estructura sin eliminar data
        "_re_install_database_DELETING_data.sh" # elimina y reinstala la db y usuario sin estructura
        "_re_poblate_data_DELETING_data.sh" # re-establese la data desde cero
        "001_create_table_usuarios.sql" # ejemplo
        "002_poblate_table_usuarios.sql"
```

2, crear archivo `_install_schema_without_delete_data.sh`

- `SCRIPTPATH="$`: Conseguimos la ruta actual, ya que como requisito
  este script debe estar a la misma altura que los archivos .sql y obteniendo
  la ruta absoluta programaticamente nos evitamos problemas de rutas
  en diferentes maquinas.
- `SUPER_USER=`: Usuario que nos servira para conectarnos y que tenga
  los permisos para ejecutar los scripts modificar la escructura de la D.B.
- `CMD_SQL=`: Guardamos el comando que nos sirve para conectarnos y asi
poder reutilizarlo para cada archivo

```bash
#!/bin/bash
# VARIABLES
#   ruta de script
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
#   usuario para aplicacion
SUPER_USER="mario_admin"
DB_NAME="usuarios_devops"
#   comado de ejecucion
CMD_SQL="psql -U $SUPER_USER -d $DB_NAME -a -f"

# EJECUCION
#    crear tabla usuarios
$(echo $CMD_SQL) $SCRIPTPATH/001_create_table_usuarios.sql
#    poblar tabla usuarios
$(echo $CMD_SQL) $SCRIPTPATH/002_poblate_table_usuarios.sql
```

3, Creamos los archivos .sql en mi caso tengo 2, uno para crear y otro
para poblar

3,1, `001_create_table_usuarios.sql`

```sql
DROP TABLE IF EXISTS usuarios;

CREATE TABLE IF NOT EXISTS usuarios (
  id_usuario SERIAL PRIMARY KEY,
  nombre     VARCHAR(40)  NOT NULL,
  clave      VARCHAR(130) NOT NULL,
  email      VARCHAR(40)  NOT NULL,
  rol        VARCHAR(40)  NOT NULL DEFAULT 'user',
  activo     BOOLEAN      NOT NULL,
  create_at  TIMESTAMP    NOT NULL DEFAULT now()+interval '1 day'
);
```

### 3.3. pipeline para subir cambios a ambiente: postgres-deploy

Agregamos el siguiente pipeline en jenkins el cual tenemos 3 pasos

1. Backup
   1. Bajar servicio
   2. hacer respaldo de estructura
   3. hacer respado de datos
2. Deploy que correra los scripts de nuevos esquemas y migraciones
   1. Actualizar el repositorio de scripts al ultimo commit.
   2. Ejecutar los script de migraciones
   3. Recomendable usar scripts que muestre que los cambios fueron bien.
3. En caso de error, Ejecutar los scripts para hacer Rollback
   1. Ejecutar el script de rollback
   2. Eliminar o etiquetar el commit de gitea, porque tiene cambios que
      rompen y dos alguien de los desarrolladores debe de tener respaldado
      el commit en su maquina.

```r
timestamps {

node () {

    stage ('postgres-deploy-2 - Checkout') {
      checkout([$class: 'GitSCM', branches: [[name: '*/test']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gittea-mario', url: 'http://166.10.0.10:3000/mario/usuarios_devops_db']]])
    }

    stage ('postgres-deploy-2 - Build') {

      sh """
        echo "============= BACKUP ============="
        docker exec postgres bash -c "mkdir -p /var/app/backups/"
        docker exec postgres bash -c "pg_dump -d usuarios_devops --data-only -U mario_admin > /var/app/backups/backup1.out"
      """

      sh """
        echo "============= DEPLOY ============="
        docker exec postgres bash -c "rm -rf /var/app/sql/bash_script/"
        docker cp ./bash_script postgres:/var/app/sql/
        docker exec postgres bash -c "sh /var/app/sql/bash_script/_install_bd.sh"
      """
    }
  }
}
```

### 3.4. pipeline para des-hacer cambio subido: postgres-rollback

### 3.5. pipeline para meter determinados .sql y/o respaldo de info: postgres-out-of-commit

En caso de que el rollback no funcione debemos ser capases de levantar
desde el backup, el cual igual se crera un jenkisfile para que esto
se haga de forma mas automatica posible, para evitar el margen de error humano.

## backend

### Aspectos necesarios del backend

- Necesitamos que por cada jar que tengamos, seamos capases de levantar
  un contenedor, el cual su comando principal sera el de ejecutar
  nuestro programa en este caso Spring Boot con servidor envevido.

### Script para iniciar la app y detectar cuando termine de hacerlo

**Objetivo:** Crear un script para luego usarlo en jenkins, el cual
nos dira si laaplicacion inicio correctamente o fallo en algo.

**Requisitos:**
  1, recordar que estamos ejecutando el mismo jar en diferentes ambientes
  por lo que puede que no necesitemos las mismas IPs, usuarios, pass etc.
  al ejecutar en un ambiente u otro por lo que necesitamos que nuestro
  jar pueda hacar uso de perfiles.

```bash
#!/bin/bash

# NOTAS
# CUIDADO: no ejecutar este script con "sh x.sh" por que causara error
# se probo este script con "bash x.sh" con la version 5.0.3(1)-release (x86_64-pc-linux-gnu)

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

ENVIROMENT="test"
MESSAGE_APP_STARTED="aplicacion_usuarios_devops_iniciada"

FILE_LOGS=$SCRIPTPATH/../logs/app.log
JAR_FILE=$SCRIPTPATH/usuarios_devops.jar

DATE=$(date +%Y-%m-%d--%H-%M-%S)

# bash $SCRIPTPATH/stop-app.sh

echo "Backup logs"
cp $FILE_LOGS $FILE_LOGS-$DATE

echo "clean actual logs"
echo "" > $FILE_LOGS

echo "Start app"
java -jar -Dspring.profiles.active=$ENVIROMENT $JAR_FILE >> $FILE_LOGS &

while read line; do
  case "$line" in
    *"$MESSAGE_APP_STARTED"* )
      echo "Iniciado exitoso"
      break;
      ;;
    *"Shutdown completed"* )
      echo "Cerrado"
      exit 1
      ;;
    *"Application run failed"* )
      echo "error al iniciar"
      exit 1
      ;;

    *"WARN"* )
      echo $line
      ;;
    *"Error"* )
      echo $line
      ;;
    *"ERROR"* )
      echo $line
      ;;
    *"Exception"* )
      echo $line
      ;;

    * )
      echo "Cargando..."
      ;;
  esac
done < <(tail -f $FILE_LOGS)

echo "start end"
```

**Resultado:** Al ejecutar el script esta iniciara nuestro .jar y el
script durara asta que encuentre alguan determinada palabra/texto, y
con un estatus, lo cual nos permitira saber si inicio correctamente o
no, lo cual podremos usar en jenkins para facilitarnos el trabajoen
este.

### Pipeline para deploy ultimo commit

```r
timestamps {

node () {

  stage ('spring-deploy - Checkout') {
    checkout([$class: 'GitSCM', branches: [[name: '*/test']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gittea-mario', url: 'http://166.10.0.10:3000/mario/usuarios-devops-backend.git']]])
  }
  stage ('spring-deploy - Build') {

    sh """
      echo "========== BACKUP =========="
      LAST_JAR=$BUILD_NUMBER.usuarios_devops.jar.$(date +"%m-%d-%y--%T")
      docker exec spring bash -c "echo $LAST_JAR > /app/backups/last_funcional_jar"
      docker exec spring bash -c "cp /app/jar/usuarios_devops.jar /app/backups/$LAST_JAR"
    """

    withMaven(maven: 'maven3-6') {
        if(isUnix()) {
          sh "mvn -B -DskipTests clean package "
        } else {
          bat "mvn -B -DskipTests clean package "
        }
      }

    sh """
      echo "========== STOP =========="
      docker exec spring bash -c "ls -l /app/jar/"
      docker exec spring bash -c "bash /app/jar/stop-app.sh"
    """

    sh """
      echo "========== DEPLOY =========="
      docker cp ./target/*.jar spring:./app/jar/usuarios_devops.jar
      docker exec spring bash -c "bash /app/jar/start-app.sh"
    """
  }
}
}
```

## X. EMPEZAR DE NUEVO PERO AUTOMATIZADO

<!-- 
  

  INFO VIEJA  NO BORRO POR SI REUSO ALGO

.

=============================================================================================================

=============================================================================================================

=============================================================================================================

=============================================================================================================

=============================================================================================================

=============================================================================================================

=============================================================================================================

-->

----
----

## Base de datos: Postgres

### Coomandos basicos

Conectarse a bd: `psql -U usuario -h 127.0.0.1 -d nombre_bd`

Ejecutar script: `psql -h 127.0.0.1 -U postgres -d myDataBase -a -f myInsertFile.sql`

Ejemplo:
`psql -v ON_ERROR_STOP=1 -h 127.0.0.1 -U mario_admin -d usuarios_devops -a -f .\bash_script\tlb_usuarios_create.sql`

> **error:** could not connect to server: FATAL:  database "mario_admin"does not exist.  
> **Descripcion:** No puedo entrar, Cree un servidor postgres con un
> [docker.composer.yml](./img/02-docker-compose-PROD.yml) y a la hora
> de entrar con `psql -h ...` me saca este error.  
> **Causa:** El comando `psql -U postgres -h 127.0.0.1` fuciona porque
> hay una base de datos con el mismo nombre, por lo cual implisitamente
> psql suponene que nos queremos conectar a esa BD, pero en el caso de
> `mario_admin` no existe una BD con el mismo nombre por lo saca el error.  
> **Solucion:** Debemos de especificar explisitamente el nombre de base
> de datos, uno que exista.  

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### crear y ejecutar jenkinsfile v1

**CUIDADO**: Cuando no tenemos ningun archivo dentro de una carpeta,
al descargar el proyecto desde git esta carpeta no se crea, por lo que
comandos podrian fallar.

1 Crear contenedor para test con postgres [docker-compose-test.yml](./img/01-docker-compose-test.yml)
y otro para produccion [docker-compose-PROD.yml](./img/02-docker-compose-PROD.yml),
donde solo cambia el puerto y el nombre del contenedor.

2, Crear `jenkinsfile` para desplegar lo que tenemos en las ramas del
repositorio a su respectivo ambiente.

TODO: Pensar en estrategia para cuidar de no eliminar datos al meter cambio

<!--
jenkinsfile obtener nombre de rama en jenkinsfile, pipeline obtener nombre de rama en pipeline
-->

```groovy
pipeline {
  agent any
    environment {
      BRANCH_NAME = "${GIT_BRANCH.split("/")[1]}"   // capturar nombre de la rama
      script_intall="./bash_script/_install_bd.sh"  // posicion de nuestro script que ejecutara los sql necesarios

      // variables necesarias pra desplegar en test
      test="test"
      iphost_test="192.168.1.86"
      puerto_test="5003"
      usuario_test="mario_admin"
      pass_test="mario_admin"

      // variables necesarias pra desplegar en calidad (proximamente)
      calidad="calidad"
      iphost_calidad="192.168.1.86"
      puerto_calidad="5002"
      usuario_calidad="mario_admin"
      pass_calidad="mario_admin"

      // variables necesarias pra desplegar en PRODUCCION
      PRODUCCION="PRODUCCION"
      IPHOST_PRODUCCION="192.168.1.86"
      PUERTO_PRODUCCION="5001"
      USUARIO_PRODUCCION="mario_admin"
      PASS_PRODUCCION="mario_admin"

      // Se asignan las variables segun el ambiente el cual se cual es
      // por medio del nombre de la rama.
      iphost = "${(BRANCH_NAME==test) ? ( iphost_test )  : (BRANCH_NAME==calidad) ? iphost_calidad  : (BRANCH_NAME==PRODUCCION) ? IPHOST_PRODUCCION  : 'error' }"
      user   = "${(BRANCH_NAME==test) ? ( usuario_test ) : (BRANCH_NAME==calidad) ? usuario_calidad : (BRANCH_NAME==PRODUCCION) ? USUARIO_PRODUCCION : 'error' }"
      pass   = "${(BRANCH_NAME==test) ? ( pass_test )    : (BRANCH_NAME==calidad) ? pass_calidad    : (BRANCH_NAME==PRODUCCION) ? PASS_PRODUCCION    : 'error' }"
      puerto = "${(BRANCH_NAME==test) ? ( puerto_test )  : (BRANCH_NAME==calidad) ? puerto_calidad  : (BRANCH_NAME==PRODUCCION) ? PUERTO_PRODUCCION  : 'error' }"
    }

    // obtenemos mas para saber que todo se asigno bien y tenemos
    // las herramientas necesarias disponibles
    stages {
      stage('Info') {
          steps {
              echo '===============- INFO -==============='
              sh 'pg_dump --version'
              sh 'psql --version'
              sh 'ls -l'
              sh 'chmod +x ./bash_script/_install_bd.sh'
              echo "iphost: $iphost";
              echo "user: $user";
              echo "Puerto: $puerto";
          }
      }

      //creamos respaldo antes de meter cambios
      // "PGPASSWORD="$pass"" es para que no pida pass
      // "$(date +"%m-%d-%y--%T")" fecha con formato "dd-mm-yy--h:m:s" ejem "09-11-20--03:39:56"
      stage('Backup') {
        steps {
          script {
            echo "===============- BACKUP -===============";
            if (BRANCH_NAME == 'test') {
              //echo "===============- Se salta backup por ser test -===============";
              sh 'PGPASSWORD="$pass" pg_dump --host $iphost --port $puerto --verbose --username $user --file ./backups/usuarios_devops_$(date +"%m-%d-%y--%T").backup.sql usuarios_devops'
            } else {
              sh 'PGPASSWORD="$pass" pg_dump --host $iphost --port $puerto --verbose --username $user --file ./backups/usuarios_devops_$(date +"%m-%d-%y--%T").backup.sql usuarios_devops'
            }
          }
        }
      }

      // ejecutamos los archivos sql para meter el cambio
      stage('install') {
        steps {
          sh 'echo ===============- INSTALL -===============';
          sh '$script_intall $pass $puerto'
        }
      }
    }
}
```

3, Agregamos un nuevo job tipo pipeline a nuestro jenkins y metemos
la url donde tengamos el proyecto metido

3,1, Solo necesitamos un jenkinsfile ya que este puede hacer lo para
los ambientes diferentes gracias a los condicionales, pero nosotros
agregamos un job por ambiente `seleccionando la rama en el job` para
tener cuidado de no desplegar un cambio en un ambiente dif. por accidente.

**Resultado:** Al ejecutar el job deberiamos de poder ver los cambios
desplegados en cualquiera de las ramas/ambientes test o PRODUCCION.

**Falta:** 1, crear un custom hook en el servidor git (no es lo mismo
que un webhook) para gatillar el job cuando hagamos un push. 2, ver
cuales serian las diferencias con mas de un host.

<!-- 

.

.

=============================================================================================================
=============================================================================================================

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

=============================================================================================================
=============================================================================================================

.

divicion seccion, divicion de seccion 2

.

-->

## Backend: Spring Boot

Crearemos un backend he de igual manera solo con hacer push los cambios
se implementaran diferenciando por ambiente~rama

### Comandos basicos

<!-- definir perfil, asignar perfil, poner perfil, coinstruir jar con perfil, compilar con perfil, -->

- Levantar proyecto definiendo perfil: `java -jar -Dspring.profiles.active=test usuarios_devops.jar`

### Crear proyecto Spring

- Fuente: [Link](https://mkyong.com/spring-boot/spring-boot-jdbc-examples/)

**Objetivo:** Crear el proyecto base con una consulta de todos los
usuarios a la base de datos de test **IMPORTANTE** no usaremos JPA en
su lugar usaremos JDBC, ya que JPA al parecer nos obliga a mapear los
entities a SQL, y nosotros no queremos hacer eso, lo que queremos es
tener bien separado la capa de bd con la de backend.

1, Creamos proyecto con las siguietnes dependencias

```xml
<!-- 
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
-->

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-jdbc</artifactId>
</dependency>

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-rest</artifactId>
</dependency>

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-devtools</artifactId>
  <scope>runtime</scope>
  <optional>true</optional>
</dependency>

<dependency>
  <groupId>org.postgresql</groupId>
  <artifactId>postgresql</artifactId>
  <scope>runtime</scope>
</dependency>

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-test</artifactId>
  <scope>test</scope>
  <exclusions>
    <exclusion>
      <groupId>org.junit.vintage</groupId>
      <artifactId>junit-vintage-engine</artifactId>
    </exclusion>
  </exclusions>
</dependency>
```

2, Agregamos datos de conexion y configuracion

```r

# Acceso y configuracion SQL Data Source: usuarios
spring.datasource.url=jdbc:postgresql://192.168.1.86:5003/usuarios_devops
spring.datasource.username=mario_admin
spring.datasource.password=mario_admin
spring.datasource.driver-class-name=org.postgresql.Driver
spring.jpa.database-platform=org.hibernate.dialect.PostgreSQL95Dialect

spring.datasource.hikari.connectionTimeout=20000
spring.datasource.hikari.maximumPoolSize=5
```

3, Agregamos POJO de Usuario

```java
public class UsuarioEntity {

  private Integer idUsuario;
  private String nombre;
  private String clave;
  private String email;
  private Boolean activo;
  private Date create_at;

  public UsuarioEntity() { }

  public UsuarioEntity(String nombre, String clave, String email, Boolean activo, Date create_at) {
    this.nombre=nombre;
    this.clave=clave;
    this.email=email;
    this.activo = activo;
    this.create_at=create_at;
  }

  public UsuarioEntity(Integer idUsuario, String nombre, String clave, String email, Boolean activo, Date create_at) {
    this.idUsuario=idUsuario;
    this.nombre=nombre;
    this.clave=clave;
    this.email=email;
    this.activo = activo;
    this.create_at=create_at;
  }

  @Override
  public String toString() {
        return String.format("\n{ID: %s, Nombre: %s, Clave: %s, Email: %s, activo: %s, create_at: %s}",
                              idUsuario, nombre, clave, email, activo, create_at);
  }

  //getters/setters
```

4, Agregar repositorio

```java
public interface IUsuarioRepository {
  List<UsuarioEntity> findAll();
}
```

5, Agregar implementacion de repositorio

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioRepositoryPostgres implements IUsuarioRepository {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Override
  public List<UsuarioEntity> findAll() {
    return jdbcTemplate.query(
      "select * from usuarios",
      (rs, rowNum) -> new UsuarioEntity(
        rs.getInt("id_usuario"),
        rs.getString("nombre"),
        rs.getString("clave"),
        rs.getString("email"),
        rs.getBoolean("activo"),
        rs.getDate("create_at")
      )
    );
  }
}
```

6, Ejecutamos el repositorio al terminar de iniciar la app

```java

import com.mario51.usuarios_devops.usuarios.IUsuarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;

@SpringBootApplication
public class UsuariosDevopsApplication implements CommandLineRunner {

  private static final Logger LOG = LoggerFactory.getLogger(UsuariosDevopsApplication.class);

  @Autowired
  IUsuarioRepository usuarioRepository;

  public static void main(String[] args) {
    SpringApplication.run(UsuariosDevopsApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    LOG.info("[FIND_ALL] {}", usuarioRepository.findAll());
  }
}
```

**Resultado:** Podemos ver que nos traemos los datos de la BD sin
modificar nada del DDL (estructura la BD)
`teniendo una separacion clara entre bd y backend` lo cual me parece
mas escalable y seguro.

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

### Cambiar la pagina de errores

Spring boot ya viene configurado para buscar en `/resources/templates/error/`
para mostrar los errores, por ejemplo buscara `404.html` cuando ocurra
un error de este tipo, lo mismo para errores 500 y etc.

## Preparar servicios/servidores para desarrollo

**Objetivo General**: Vamos a levantar los siguientes servicios
en nustro servodor, para tener las herramientas para guardar y
deploy nuestro codigo en diferentes ambientes

### Levantar servicios

1, Creamos el docker-compose para crear el servicio

```yml
##################################################################################################################################
```

2, Levantamos el servicio: `docker-compose -f docker-compose.yml up -d`

2,1, Podemos compobrar que esta corriendo y tiene la IP asignada con

- Ver que este corriendo: `docker ps`
- ver ip de cotenedor: `docker inspect mi-contenedor | grep "IPAddress"`

**Resultado:** Tenemos corriendo un servidor gitea corriendo y podremos
entrar a el con ``http://[IP_DOCKER_HOST]:3000``

### Levantar servicio de Jenkins con docker

**Objetivo:** Vamos a levantar un servidor jenkins para poder correr
nuestros jobs/pipelines para devops.

1, Como vamos a modificar la imagen de jenkins para que tenga a
disposicion docker, vamos a crear el `dockerfile` para hacer esto

- `jenkins:jdk11` usamos esta imagen ya que nuestro proyecto esta usando
  java11

```r
FROM jenkins/jenkins:jdk11

USER root

#instalar Docker
RUN apt update && \
    apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    apt update && \
    apt-cache policy docker-ce && \
    apt install -y docker-ce && \
    systemctl enable docker

# Instalar docker compose
RUN curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose && \
    usermod -aG docker jenkins
```

2, En la misma carpeta creamos el `docker-compose.yml`

```yml
version: "3.3"

networks:
  usuarios_devops_net:
    driver: bridge
    ipam:
      config:
        - subnet: 166.10.0.0/24

services:

  gitea_host:
    container_name: gitea
    image: gitea/gitea:1.12.3
    environment:
      - USER_UID=1000
      - USER_GID=1000
    restart: unless-stopped
    volumes:
      - ./gittea/gitea:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "3000:3000"
      - "222:22"
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.10

  jenkins:
    container_name: jenkins
    build:
      context: ./jenkins/
      dockerfile: dockerfile-jenkinswdocker
    privileged: true
    user: root
    restart: unless-stopped
    ports:
      - "8082:8080"
    volumes:
      - ./jenkins/jenkins_home:/var/jenkins_home
      - /var/run/docker.sock:/var/run/docker.sock
      - /usr/local/bin/docker:/usr/local/bin/docker
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.11

  postgres:
    container_name: postgres
    image: postgres:10
    ports:
      - 5003:5432
    restart: unless-stopped
    environment:
      POSTGRES_PASSWORD: mario_admin
      POSTGRES_USER: mario_admin
      POSTGRES_DB: usuarios_devops
    volumes:
      - ./postgres/data:/var/lib/postgresql/data
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.21

  spring:
    container_name: spring
    image: maven:3.6-openjdk-11
    restart: unless-stopped
    ports:
      - "83:8080"
    volumes:
      - ./spring/app:/app/
#   command: tail -f /app/jar/app.log
    command: java -jar /app/jar/usuarios_devops.jar
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.22

```

### Cambiar .properties sengun ambiente

application-local.properties

```sh
#!/bin/bash

# obtenemos la ruta del script para ejecutar los comandos en esta ruta.
# CUIDADO: el script debe estar guardado en el mismo lugar donde estan guardados los .jar de respaldo.
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# obtenemos el numero del job anterior, usando el archivo (last_job_number) donde jenkins nos guardo el numero del ultimo job
# POR EJEMPLO, el job que fallo y por lo tanto queremos regresar es el 22 entonces con el comando de abajo obtenemos 21
PREV_JOB_NUMBER=$(echo $(($(cat $SCRIPTPATH/last_job_number)-1)))

# obtenermos el jar file con nombre y ruta absoluta
PREV_JAR=$(ls $SCRIPTPATH/$PREV_JOB_NUMBER.usuarios_devops.jar*)

# copiamos el jar a la ruta de ejecucion
cp $PREV_JAR $SCRIPTPATH/../jar/usuarios_devops.jar
```

## Spring deploy pipeline

```r
timestamps {

node () {

  stage ('spring-deploy - Checkout') {
    checkout([$class: 'GitSCM', branches: [[name: '*/test']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gittea-mario', url: 'http://166.10.0.10:3000/mario/usuarios-devops-backend.git']]])
  }

  stage ('spring-deploy - Build') {

// Shell build step
sh """
echo "========== BACKUP =========="
LAST_JAR=$BUILD_NUMBER.usuarios_devops.jar.$(date +"%m-%d-%y--%T")
docker exec spring bash -c "echo $LAST_JAR > /app/backup/last_funcional_jar"
docker exec spring bash -c "cp /app/jar/usuarios_devops.jar /app/backup/$LAST_JAR"
 """

// Maven build step
  withMaven(maven: 'maven3-6') {
      if(isUnix()) {
        sh "mvn -B -DskipTests clean package "
      } else {
        bat "mvn -B -DskipTests clean package "
      }
    }

// Shell build step
sh """
echo "========== DEPLOY =========="
docker stop spring
docker cp ./target/*.jar spring:./app/jar/usuarios_devops.jar
docker start spring
"""
  }
}
}
```

## Spring pipeline backup

copy_previus_jar_to_exec_path.sh

```sh
#!/bin/bash

# obtenemos la ruta del script para ejecutar los comandos en esta ruta.
# CUIDADO: el script debe estar guardado en el mismo lugar donde estan guardados los .jar de respaldo.
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

PREV_JAR=$(cat $SCRIPTPATH/last_funcional_jar)

# copiamos el jar a la ruta de ejecucion
cp $SCRIPTPATH/$PREV_JAR $SCRIPTPATH/../jar/usuarios_devops.jar
```

pipeline

```r
timestamps {

node () {

  stage ('spring-rollback - Build') {
    // Shell build step
    sh """
      # copiar el jar anterior a la ruta de ejecucion
      docker exec spring bash -c 'sh /app/backup/copy_previus_jar_to_exec_path.sh'

      docker stop spring

      docker start spring
    """
  }
}
}
```

## Postgres: Deploy - Deploy

Este pipeline se debe descargar junto con el git de jenkins para que
tenga disponible

```sh
pipeline {
  agent any
    environment {
      BRANCH_NAME = "${GIT_BRANCH.split("/")[1]}"
      script_intall="./bash_script/_install_bd.sh"
      DB_NAME="usuarios_devops"

      test="test"
      iphost_test="166.10.0.21"
      puerto_test="5432"
      usuario_test="mario_admin"
      pass_test="mario_admin"

      calidad="calidad"
      iphost_calidad="192.168.1.86"
      puerto_calidad="5002"
      usuario_calidad="mario_admin"
      pass_calidad="mario_admin"

      PRODUCCION="PRODUCCION"
      IPHOST_PRODUCCION="192.168.1.86"
      PUERTO_PRODUCCION="5001"
      USUARIO_PRODUCCION="mario_admin"
      PASS_PRODUCCION="mario_admin"

      iphost = "${(BRANCH_NAME==test) ? ( iphost_test )  : (BRANCH_NAME==calidad) ? iphost_calidad  : (BRANCH_NAME==PRODUCCION) ? IPHOST_PRODUCCION  : 'error' }"
      user   = "${(BRANCH_NAME==test) ? ( usuario_test ) : (BRANCH_NAME==calidad) ? usuario_calidad : (BRANCH_NAME==PRODUCCION) ? USUARIO_PRODUCCION : 'error' }"
      pass   = "${(BRANCH_NAME==test) ? ( pass_test )    : (BRANCH_NAME==calidad) ? pass_calidad    : (BRANCH_NAME==PRODUCCION) ? PASS_PRODUCCION    : 'error' }"
      puerto = "${(BRANCH_NAME==test) ? ( puerto_test )  : (BRANCH_NAME==calidad) ? puerto_calidad  : (BRANCH_NAME==PRODUCCION) ? PUERTO_PRODUCCION  : 'error' }"
    }

    stages {
      stage('Info') {
          steps {
              echo '===============- INFO -==============='
              sh 'pg_dump --version'
              sh 'psql --version'
              sh 'ls -l'
              sh 'chmod +x ./bash_script/_install_bd.sh'
              echo "iphost: $iphost";
              echo "user: $user";
              echo "Puerto: $puerto";
          }
      }

      stage('Backup') {
        steps {
          script {
            echo "===============- BACKUP -===============";
            if (BRANCH_NAME == 'test') {
              //echo "===============- Se salta backup por ser test -===============";
              sh 'PGPASSWORD="$pass" pg_dump --host $iphost --port $puerto --verbose --username $user --file ./backups/$DB_NAME_$(date +"%m-%d-%y--%T").backup.sql $DB_NAME'
            } else {
              sh 'PGPASSWORD="$pass" pg_dump --host $iphost --port $puerto --verbose --username $user --file ./backups/$DB_NAME_$(date +"%m-%d-%y--%T").backup.sql $DB_NAME'
            }
          }
        }
      }

      stage('install') {
        steps {
          sh 'echo ===============- INSTALL -==============='
          sh '$script_intall $pass $iphost $puerto $user $DB_NAME'
        }
      }
    }
}
```

### escribir pid en archivo

[Link](https://javadeveloperzone.com/spring-boot/spring-boot-applicationpidfilewriter-example-process-id-writer-example/)
