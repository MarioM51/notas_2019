DROP TABLE IF EXISTS usuarios;

CREATE TABLE IF NOT EXISTS usuarios (
  id_usuario SERIAL PRIMARY KEY,
  nombre     VARCHAR(40)  NOT NULL,
  clave      VARCHAR(130) NOT NULL,
  email      VARCHAR(40)  NOT NULL,
  rol        VARCHAR(40)  NOT NULL DEFAULT 'user',
  activo     BOOLEAN      NOT NULL,
  create_at  TIMESTAMP    NOT NULL DEFAULT now()+interval '1 day'
);