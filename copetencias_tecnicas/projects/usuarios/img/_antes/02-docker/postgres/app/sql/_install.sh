#!/bin/bash

# VARIABLES

#   ruta de script
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

#   usuario para aplicacion
SUPER_USER="mario_admin"
DB_NAME="usuarios_devops"

#   comado de ejecucion
CMD_SQL="psql -U $SUPER_USER -d $DB_NAME -a -f"


# EJECUCION

#    crear tabla usuarios
$(echo $CMD_SQL) $SCRIPTPATH/001_create_table_usuarios.sql
