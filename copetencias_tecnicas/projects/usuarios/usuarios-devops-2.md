# Usuarios DevOps 2

- [Usuarios DevOps 2](#usuarios-devops-2)
  - [PENDIENTES](#pendientes)
  - [Resultados Finales](#resultados-finales)
  - [Introduccion](#introduccion)
    - [Idea centrar](#idea-centrar)
  - [Creacion de maquina virtual](#creacion-de-maquina-virtual)
    - [instalar Docker](#instalar-docker)
    - [Instalar docker compose](#instalar-docker-compose)
    - [Instalar/levantar servidor FTP](#instalarlevantar-servidor-ftp)
    - [Asignar una IP estatica](#asignar-una-ip-estatica)
  - [Crear red](#crear-red)
  - [Crear servidor/contenedor Gitea](#crear-servidorcontenedor-gitea)
  - [Crear servidor/contenedor Jenkins](#crear-servidorcontenedor-jenkins)
    - [Instalar plugins necesarios](#instalar-plugins-necesarios)
  - [Crear servidor/contenedor Postgres](#crear-servidorcontenedor-postgres)
    - [Crear servidor/contenedor Java para Spring](#crear-servidorcontenedor-java-para-spring)
  - [Iniciar Capa Base de datos: Postgres](#iniciar-capa-base-de-datos-postgres)
    - [Introduccion DB](#introduccion-db)
    - [Scripts para creacion de Base de datos y Usuarios](#scripts-para-creacion-de-base-de-datos-y-usuarios)
    - [Ejecutar scripts para crear DB](#ejecutar-scripts-para-crear-db)
    - [Crear tabla usuarios](#crear-tabla-usuarios)
    - [Creacion de scripts para rollback](#creacion-de-scripts-para-rollback)
    - [Ejecutar los scripts en tests manualmente para postgres](#ejecutar-los-scripts-en-tests-manualmente-para-postgres)
  - [Implementando Pipelines para postgres](#implementando-pipelines-para-postgres)
    - [Preparativos para crear pipelines](#preparativos-para-crear-pipelines)
      - [Subir proyecto/codigo postgres a Gitea](#subir-proyectocodigo-postgres-a-gitea)
      - [Clonar repo de postgres en jenkis](#clonar-repo-de-postgres-en-jenkis)
      - [Ejecucion de primer pipeline en jenkins (sin acciones/modificaciones)](#ejecucion-de-primer-pipeline-en-jenkins-sin-accionesmodificaciones)
    - [Pipelines para deploy changes](#pipelines-para-deploy-changes)
    - [Pipelines para rollback changes](#pipelines-para-rollback-changes)
    - [Job Rollback en calidad Spring](#job-rollback-en-calidad-spring)
    - [Portgres: Crear respaldo](#portgres-crear-respaldo)
  - [Iniciar Capa Backend: Spring](#iniciar-capa-backend-spring)
    - [Crear proyecto spring](#crear-proyecto-spring)
    - [Listar todos los usuarios](#listar-todos-los-usuarios)
    - [Usar diferentes configuraciones en el jar para los diferentes ambientes](#usar-diferentes-configuraciones-en-el-jar-para-los-diferentes-ambientes)
    - [Preparativos para ejecutar jar en diferentes ambientes](#preparativos-para-ejecutar-jar-en-diferentes-ambientes)
      - [Crear Script install_changes para Spring](#crear-script-install_changes-para-spring)
      - [Crear Script detener app spring (stop_app)](#crear-script-detener-app-spring-stop_app)
        - [Escribir PID asignado del jar que ejecutamos en archivo](#escribir-pid-asignado-del-jar-que-ejecutamos-en-archivo)
        - [Crear script](#crear-script)
      - [Crear Script iniciar app spring (start_app)](#crear-script-iniciar-app-spring-start_app)
    - [Ejecutar los scripts en tests manualmente para spring](#ejecutar-los-scripts-en-tests-manualmente-para-spring)
    - [Crear Script para crear respaldo (_create_backup)](#crear-script-para-crear-respaldo-_create_backup)
    - [Crear Script para rollback al ultimo jar respaldado](#crear-script-para-rollback-al-ultimo-jar-respaldado)
    - [RESUMEN Probando escenario de hacer deploy y luego rollback (manualmente)](#resumen-probando-escenario-de-hacer-deploy-y-luego-rollback-manualmente)
  - [Implementando Pipelines para Spring](#implementando-pipelines-para-spring)
    - [Crear repositorio y subirlo a gitea](#crear-repositorio-y-subirlo-a-gitea)
    - [Descargar-Clonar repositorio Spring en Jenkins](#descargar-clonar-repositorio-spring-en-jenkins)
    - [Crear pipeline para deploy changes](#crear-pipeline-para-deploy-changes)
    - [Crear pipeline para rollback al ultimo resplado creado](#crear-pipeline-para-rollback-al-ultimo-resplado-creado)
    - [RESULTADO: Comprobar pipelines](#resultado-comprobar-pipelines)
  - [Crear ambiente de calidad: Postgres](#crear-ambiente-de-calidad-postgres)
    - [Postgres calidad preparativos](#postgres-calidad-preparativos)
    - [Postgres calidad: Creacion](#postgres-calidad-creacion)
      - [Job deploy en calidad](#job-deploy-en-calidad)
      - [Job reollback en calidad](#job-reollback-en-calidad)
  - [Crear ambiente de calidad: Spring](#crear-ambiente-de-calidad-spring)
  - [Crear ambiente de produccion](#crear-ambiente-de-produccion)
    - [Ambiente produccion: Postgres](#ambiente-produccion-postgres)
    - [Ambiente Produccion: Spring](#ambiente-produccion-spring)
  - [IMPORTANTE: Empaquetar proyecto devops base para futuros proyectos](#importante-empaquetar-proyecto-devops-base-para-futuros-proyectos)
    - [Resultado](#resultado)
      - [Como manejar capa postgres](#como-manejar-capa-postgres)
      - [Como manegar la capa Spring](#como-manegar-la-capa-spring)
  - [Spring: CRUD usuarios](#spring-crud-usuarios)
    - [Agregar roles](#agregar-roles)

## PENDIENTES

- TODO: ocultar contraseñas los jobs de (postgres_rollback): [Investigar](https://stackoverflow.com/questions/52551232/hiding-passwords-in-jenkins-pipeline-log-output-without-using-withcredentials)
- TODO: Al crear un nuevo contenedor y ejecutar el deploy por primera
  vez, este trata de copiar el app/x.jar pero no existe, asi que lo
  estoy creando manualmente, VER COMO automatizar esto.

## Resultados Finales

Codido con DevOps y con base de datos evolutivas, usando jenkins, gitea,
docker, bashScript, Spring.

- ![Codigo](./img/_1_resultado_final_mdlmt.zip)

## Introduccion

Vamos a crear un proyecto multi-capas en este caso con (postgres, spring
y angular) para crear una aplicacion sencilla, ya que la finalidad es
entender la parte de como implementar DevOps.

### Idea centrar

**Resumen**: Usaremos jenkins como si fuera otro desarrollador el cual
descarga el proyecto usando git, y ya teniendo lo necesario este pasara
lo necesario al servidor para hacer sus actividades.

## Creacion de maquina virtual

**Objetivo:** Instalar las herramientas necesarias pra trabajar con
nuestro servidor.

Vamos a usar `ubunto 20.04` y le instalaremos docker y docker compose
para iniciar a configurar nuestro entorno

1, Crear la maquina con un red tipo `bridged Adapter` para poder acceder
   al internet de nuestro equipo.

![01_virtualbox_conf_bridged_adapter](./img/01_virtualbox_conf_bridged_adapter.PNG)

### instalar Docker

- [Fuente](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)

1, Instalacion

```bash
# Actualizamos repositorios
$ sudo apt-get update
# instalamos dependencias para instalar doker
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
# Agregamos repositorio de docker
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# Comprobamos que la huella este bien (checar docu de que debe imprimir)
$ sudo apt-key fingerprint 0EBFCD88
#Agregamos repositorio
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
# Actualizamos repositorios, pero ahora con el de docker que agregamos
sudo apt-get update
#Instalamos docker engine
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

2, Comprobar

- instalacion: `docker --version`
<!-- En mi caso imprimio "Docker version 19.03.13, build 4484c46d9d"-->

- Funcionamiento: `sudo docker run hello-world`
<!-- me imprimio "Hello from Docker y mas" -->

### Instalar docker compose

- Fuente: [Link](https://docs.docker.com/compose/install/#install-compose-on-linux-systems)

1, Instalcion

```bash
# instalacion
> sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# Permisos
> sudo chmod +x /usr/local/bin/docker-compose
```

2, Comprobacion: `docker-compose --version`
<!-- En mi caso imrpimio: docker-compose version 1.27.4, build 40524192 -->

### Instalar/levantar servidor FTP

Fuente: [Link](https://devanswers.co/install-ftp-server-vsftpd-ubuntu-20-04/)

1, Instalcion `sudo apt install vsftpd`

2, Configuracion Seguir la documentacion

3, Termine con la siguiente conf en `/etc/vsftpd.conf` para poder
escribir con mi usuario que es rooteable

```conf
listen=NO
listen_ipv6=YES
anonymous_enable=NO
local_enable=YES
write_enable=YES
local_umask=022
dirmessage_enable=YES
use_localtime=YES
xferlog_enable=YES
connect_from_port_20=YES
chroot_local_user=YES
secure_chroot_dir=/var/run/vsftpd/empty
pam_service_name=vsftpd
force_dot_files=YES
pasv_min_port=40000
pasv_max_port=50000

user_sub_token=$USER
local_root=/home/$USER
allow_writeable_chroot=YES
```

4, Comprobacion

1. Ver la version `vsftpd -v` en mi caso imprimio `vsftpd: version 3.0.3`
2. Ver si esta activo `sudo service vsftpd status`
3. Entrar por un cliente FTP con nuestro usuario y subir un archivo
4. Reiniciar el servidor para ver si se incia el servicio con el S.O.

### Asignar una IP estatica

- Fuente: [Link](http://itprohelper.com/setting-up-static-ip-on-ubuntu-server-20-04-lts/)

1, Ver que interface usar `ip a`

En mi caso imprimio lo siguiente, y me di qcuenta que es la `enp0s3` ya
que es la que se usa para conectarme cpn mi equipo

```r
2: enp0s3: <BROADCAST...> mtu ...
    link/ether ...:fc brd ...
    inet 192.168.1.80/24 brd 192.168.1.255 scope global dynamic enp0s3
```

2, Asegurar que no tenemos algo ya configurado:

Este archivo; `/etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg`
debe de tener nanamas`etwork: {config: disabled}`

3, Configuramos: `sudo nano /etc/netplan/00-installer-config.yaml`

<!-- Copie el gateway del ipconfig de mi maquina -->

```yml
network:
  version: 2
  ethernets:
    enp0s3:
      addresses: [192.168.1.80/24]
      dhcp4: false
      gateway4: 192.168.1.254
      nameservers:
        addresses: [192.168.1.254, 8.8.8.8]
```

4, Aplicamos temporalmente: `sudo netplan try`

5, Ya que vimos que no ahi un error aplicamos permanentemente: `sudo netplan apply`

6, Comprobamos, aun nos debeveremos poder conectar por ssh incluso si
reiniciamos el equipo.

## Crear red

Nustros diferentes contenedores usaran la misma red, y sera una que
crearemos, para poder asignarle IPs estaticas y tener major control
sobre la comunicacion.

```r
sudo docker network create -d bridge --subnet 166.10.0.0/24 --gateway 166.10.0.1 usuarios_devops_net
```

## Crear servidor/contenedor Gitea

1, Pasamos por ftp los archivos necesarios para crear el servicio

```r
./gitea
./gitea/gitea
./gitea/gitea/data
./gitea/docker-compose-gitea.yml
 ```

2, Contenido de `docker-compose-gitea.yml`, es necesario ya tener
creada l red que vimos antes

```yml
version: "3.3"

# indicamos que queremos usar una red ya creada
networks:
  usuarios_devops_net:
    external: true

services:
# NOTA: Solo levantamos y usamos no movemos nada para poder usarlo
  gitea_host:
    container_name: gitea
# es recomendable definir version ya que si no al futuro descarga otra
# mas reciente la cual podria traernos problemas, igual a futuro podemos
# cambiarle manualmente para usar una mas reciente
    image: gitea/gitea:1.12.3
    environment:
# En mi caso el usuario 1000 es el primero que cree rootable "mario",
# en otro caso fue 0, esto lo vemos en "/etc/passwd"
      - USER_UID=1000
      - USER_GID=1000
# asegura que estara activo incluso cuando reiniciemos la maquina,
# asta que nosotros lo detengamos
    restart: unless-stopped
    volumes:
      - ./gitea/data:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "3000:3000"
      - "222:22"
# definimos IPs estaticas ya que en lo personal me siento mas comodo
# usarlas, ya que ahi otras formas.
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.10

```

3, Creamos contenedor: `sudo docker-compose -f ./docker-compose-gitea.yml up -d`

4, Deberiamos poder entrar a `http://192.168.1.80:3000/` y mostrar
el sevicio de gitea

5, Nos metemos a `Registrar` y seguimos instrucciones, para aisgnar info
basica y crear un nuevo usuario que sera el root.

**Resultado:** Podemos entrar desde el navegador al servicio de gitea,
como tambien crear repositorios para subirlos o clonarlos o jalarlos.

![02_terminar-de-crear-serv-gitea](./img/02_terminar-de-crear-serv-gitea.PNG)

## Crear servidor/contenedor Jenkins

**Introduccion:** Para crear este contenedor sera mas especial, ya que
como queremos que pueda usar docker con acceso al nuestros contenedores,
po lo cual tendremos que usar un `dockerfile` personalizado para instalar
lo necesario.

1, Pasamos por FTP los archivos necesarios

```r
./spring_workspace/
./postgres_workspace/
./jenkins_home/
./docker-compose-jenkins.yml
./jenkinswdocker.dockerfile
```

2, Contenido de `jenkinswdocker.dockerfile`

```dockerfile
FROM jenkins/jenkins:jdk11

USER root

#instalar Docker
RUN apt update && \
    apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    apt update && \
    apt-cache policy docker-ce && \
    apt install -y docker-ce && \
    systemctl enable docker

# Instalar docker compose
RUN curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose && \
    usermod -aG docker jenkins

# Instalar client-ftp
RUN apt install -y ftp
```

3, Contenido de `docker-compose-jenkins.yml`

```yml
version: "3.3"

# indicamos que queremos usar una red ya creada
networks:
  usuarios_devops_net:
    external: true

services:
  jenkins:
    container_name: jenkins
    build:
      context: .
      dockerfile: jenkinswdocker.dockerfile
    privileged: true
    user: root
    restart: unless-stopped
    ports:
      - "90:8080"
    volumes:
# carpeta principal de jenkins donde estara toda la info que generemos en jenkins
      - ./jenkins_home:/var/jenkins_home
# Acceso a las carpetas donde nosotros trabajaremos los diferentes proyectos
      - ./spring_workspace:/home/spring_workspace
      - ./postgres_workspace:/home/postgres_workspace
# Damos acceso a docker del docker host
      - /var/run/docker.sock:/var/run/docker.sock
      - /usr/local/bin/docker:/usr/local/bin/docker
# Sincronizamos el tiempo de nuestro docker host con el contenedor
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.11
```

4, Crear contenedor jenkins: `sudo docker-compose -f ./docker-compose-jenkins.yml up -d`

5, Ver logs y esperar a que termine de iniciar `sudo docker logs -f jenkins`

6, Al terminar podremos entrar a `http://192.168.1.80:90/` al entrar
nos pedira una llave que podremos econtrar entr `****` en los logs.

7, Seguimos instrucciones para instalar plugins, en mi caso instale

```r
- Folders               - Workspace Cleanup
- Build Timeout         - Pipeline
- Credentials Binding   - Git
- Timestamper
```

**Resultado:** Tenemos disponible jenkins

![03_terminar-de-crear-serv-jenkins](./img/03_terminar-de-crear-serv-jenkins.PNG)

### Instalar plugins necesarios

- **locale:** para cambiar el ideioma a ingles, ayq eu en español causa
  cosas problemas como, busqueda de errores y cosas raras, ya instalado
  podemos poner en Default Language `Configure System -> Locale -> Default Language: "en"`.

creo faltan mas

## Crear servidor/contenedor Postgres

1, Copiamos los archivos necesarios al servidor por ftp

```r
mario@mario:~/usuarios-devops$ du -a | grep ./postgres/
du: cannot read directory './postgres/postgres/data': Permission denied
du: cannot read directory './gitea/gitea/data/ssh': Permission denied
./postgres/to_change/
./postgres/backups
./postgres/data
./postgres/postgres/data #este usara postgres lo metemos mas adentro para evitar lios de permisos
./postgres/current_state/
./postgres/docker-compose-postgres.yml # el que nos importa
```

2, Contenido de `docker-compose-postgres.yml`

```yml
version: "3.3"

# indicamos que queremos usar una red ya creada
networks:
  usuarios_devops_net:
    external: true

services:
  postgres:
    container_name: postgres
    image: postgres:10
    ports:
      - 5003:5432
    restart: unless-stopped
    environment:
      POSTGRES_PASSWORD: mario_root
      POSTGRES_USER: mario_root
    volumes:
      - ./postgres/data:/var/lib/postgresql/data
# espacio de trabajo en el servidor
      - ./backups:/home/app/backups
      - ./to_change:/home/app/to_change
      - ./current_state:/home/app/current_state
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.21

```

3, Creamos contenedor: `sudo docker-compose -f ./docker-compose-postgres.yml up -d`

 5, Comprobamos que funciona con

<!-- Conectarse a postgres remoto, Conectarse bases de datos remota, Conectarse db remota, Conectarse bd remota -->

- dentro del contenedor (no pedira pass): `psql -U mario_root -d postgres`
- Desde fuera en la misma red (si pedira pass): `psql -h 192.168.1.80 -p 5003 -U mario_root -d postgres`

**Resultado:** Tenemos creado un servidoe de postgres

### Crear servidor/contenedor Java para Spring

1, Copiamos los archivos necesarios desde FTP

```r
./spring/to_change
./spring/to_change/code # aqui ira nuestro codigo en este caso un proyecto spring con maven
./spring/to_change/__spring-deploy-changes.jenkinsfile # para deploy cambios

./spring/backups
./spring/backups/_copy_previus_jar_to_exec_path.sh
./spring/backups/__spring-rollback-to-previus-state.jenkinsfile
./spring/backups/_last_funcional_jar.txt
./spring/backups/_jar_to_recover.txt

./spring/app/__spring-stop-app.jenkinsfile
./spring/app/_stop-app.sh
./spring/app/_start-app.sh
./spring/app/__spring-start-app.jenkinsfile

./spring/app/logs
./spring/app/logs/app.log

./spring/app/jar
./spring/app/jar/usuarios_devops.jar

./spring/docker-compose-spring.yml # nos INTERESA este para crear el contenedor
```

2, Contenido de `docker-compose-spring.yml`

```yml
version: "3.3"

# indicamos que queremos usar una red ya creada
networks:
  usuarios_devops_net:
    external: true

services:
  spring:
    container_name: spring
    image: maven:3.6-openjdk-11
    restart: unless-stopped
    ports:
      - "83:8080"
    volumes:
      - ./app/:/usuarios_devops/app
      - ./backups/:/usuarios_devops/backups
      - ./to_change/:/usuarios_devops/to_change
    command: tail -f /app/logs/app.log
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.22
```

2, Creamos contenedor: `sudo docker-compose -f ./docker-compose-spring.yml up -d`

3, Comprobamos, entrar a contenedor y ver version de java

```r
sudo docker exec -ti spring bash
java -version
```

## Iniciar Capa Base de datos: Postgres

### Introduccion DB

Vamos a tener una base de datos idependiente, con esto me refiero a que
no vamos a usar un ORM como hibernate/JPA que generen la BD por nosotros,
esto a mi parecer, tiene las siguietnes ventajas

- Tener una mejor separacion entre capas
- Mayor control como migraciones, clusters, triggers, etc.
- Si algun dia el proyecto crece poder integrar profecionales de bases
  de datos y a estos no meterles ruido con este tipo de herramientas.
- Uno mismo poder desarrollarse como profecional en un campo concreto
  ya que talves el dia de mañana ya no usamos java y todo lo que aprendimos
  de un ORM en concreto seria mas dificil migrarlo, mientrar que tener
  un conocimiento concreto en B.D. podremos reutilizar este conocimiento
  en diferentes areas idependientemente del backend que usemos.
- Poder seguir un marco de trabajo de bases de datos evolutivas como explican
  las siguientes fuentes.
  - [one does not simply update a database](https://dev.to/pesse/one-does-not-simply-update-a-database--migration-based-database-development-527d)
  - [versionar bases de datos](https://www.javiergarzas.com/2014/11/versionar-bases-de-datos-2.html)
  - [Fear database changes? Get them under control with CI/CD](https://hackernoon.com/database-changes-can-be-scary-how-r1hy2gfe)

Por lo que todo lo que hagamos en la base de datos lo haremos por medio
de scripst idempotentes sobre una D.B. base, y para el versionado usaremos
git, donde si queremos X version de la BD solo heremos a dicho commit
para ejecutar el script que instala la DB.

**Al terminar:** terminaremos una estructura de carpetas y de tablas
como la siguiente **NOTA:** Vamos a crear una tabla **sin normalizaciones**
ya que en un futuro veremos como aplicarselas.

```r
# CARPETAS Y ARCHIVOS-SCRIPTS

/POSTGRES
│   README.md # documentacion
│
├───current_state
│       "_rollback.sh" # al ejecutar hara rollback (quitar los cambios que hayamos aplicado)
│       "00_01_DROP_database_and_user.sql" # scripts necesarios para deshacer los cambios
│       "00_02_DROP_table_usuarios.sql"
│
└───to_change
        "_install_changes.sh" # al ejecutar aplicara todos los cambios
        "00_01_create_if_not_exists_user_and_data_base.sql" # scripts necesarios para aplicar los cambios
        "00_02_create_table_usuarios.sql"
        "00_03_poblate_table_usuarios.sql"

# RESULTADO DE TABLA A CREAR
 id_usuario | titulo_y_nombre |            roles            | activo |         create_at
------------+-----------------+-----------------------------+--------+----------------------------
          1 | ing. Mario      | admin, post_writer, checker | t      | 2020-10-13 18:50:22.477837
```

### Scripts para creacion de Base de datos y Usuarios

Usaremos el siguiente scipt, notar que tambien tenemos el de eliminar
lo cual si ejecutamos el script eliminara la BD y la creara de nuevo.

<!-- 
crear base de datos si no existe, crear db si no existe, crear bd si no existe,
create database if not exists,
crear usuario si no existe, create user if not exists,
FUENTE1: https://stackoverflow.com/questions/18389124/simulate-create-database-if-not-exists-for-postgresql/36218838#36218838
FUENTE2: https://stackoverflow.com/questions/4386118/create-database-using-a-stored-function
-->

2, `.\postgres\to_change\00_01_create_if_not_exists_user_and_data_base.sql`

```sql

--
-- CREAR USUARIO SI NO EXISTE
DO
$do$
BEGIN
  IF EXISTS (SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = 'mario_admin') THEN
    RAISE NOTICE 'User already exists';
  ELSE
    RAISE NOTICE 'Creating user...';
    CREATE USER mario_admin WITH LOGIN NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION
    ENCRYPTED PASSWORD 'mario_admin';
    RAISE NOTICE 'USER CREATED';
  END IF;
END
$do$;

--
-- CREAR BASE DE DATOS SI NO EXISTE
DO
$do$
DECLARE
  _db TEXT := 'usuarios_devops';
  _user TEXT := 'mario_root';
  _password TEXT := '';
BEGIN
  CREATE EXTENSION IF NOT EXISTS dblink; -- enable extension
  IF EXISTS (SELECT 1 FROM pg_database WHERE datname = _db) THEN
    RAISE NOTICE 'Database already exists';
  ELSE
    PERFORM dblink_connect('host=localhost user=' || _user || ' dbname=' || current_database());
    RAISE NOTICE 'Creating database...';
    PERFORM dblink_exec('CREATE DATABASE usuarios_devops OWNER = mario_admin' );
    RAISE NOTICE 'DATABASE CREATED';
  END IF;
END
$do$
```

<!-- 
scripts par ejecutar sql, ejecutar sql con bash, bash para ejecutar sql, bash script para ejecutar sql,
ejecutar sql desde terminal
-->

2, Creamos el bash script para ejecutar nuestros sql scripts, los cuales
ejecutaremos con el usuario root de postgres ya que no tenemos el usr
que usara ba DB y usar el root es peligroso.

2,1, `.\postgres\to_change\_install_changes.sh`

```bash
#!/bin/bash

# DECLARACION
#   ruta de script
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

#   Usuario root
ROOT_USER="mario_root"
ROOT_DB_NAME="postgres"
CMD_SQL_ROOT="psql -v ON_ERROR_STOP=1 -U $ADMIN_USER -d $ADMIN_DB_NAME -a -f"

#    crear tabla usuarios
$(echo $CMD_SQL_ROOT) $SCRIPTPATH/00_02_create_if_not_exists_user_and_data_base.sql
```

### Ejecutar scripts para crear DB

1, En windows iniciamos postgres con: `Win+R -> services.msc -> buscar "postgres" -> clickDer -> start`

- Es recomendable que para nustro equipo local desabilidar la contraseña
  esto lo podemos hacer en `ruta-de-donde-instalamos-nuestro-postgres/data/pg_hba.conf`
  ponemos las IPs locales con el metrodo de trust, en mi caso.

```r
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host    all             all             127.0.0.1/32            trust
host    all             all             ::1/128                 trust
```

2, Comprobamos que el servicio de postgres este funcionando con: `psql -U mario_root -d postgres`

3, Con la consola de `Git bash` ejecutamos el script `_install_changes.sh`
en nustra maquina local para ver si funcionan correctamente.

![04-creacion-de-bd-desde-bash-script-en-local-windows](./img/04-creacion-de-bd-desde-bash-script-en-local-windows.PNG)

4, Entramos a nuestra DB recien creada: `psql -U mario_admin -d usuarios_devops`

4,1, Ejecutamos dentro de psql `\dt` y veremos que aun no tenemos ninguna
tabla creada, pero comprobamos que se creo nuestra DB y el usuario
`mario_admin` puede entrar a esta.

### Crear tabla usuarios

Ya tenemos crada la base de datos ahora vamos a crearle unas tablas
siguiendo la mima estrategia

1, Creamos sql para crear tabla: `postgres\to_change\00_02_create_table_usuarios.sql`

```sql
--
-- CREAR TABLA USUARIOS
CREATE TABLE IF NOT EXISTS usuarios (
  id_usuario        SERIAL        PRIMARY KEY,
  titulo_y_nombre   VARCHAR(40)   NOT NULL,
  roles             VARCHAR(40)   NOT NULL,
  activo            BOOLEAN       NOT NULL,
  create_at         TIMESTAMP     NOT NULL DEFAULT now()+interval '1 day'
);
```

2, Creamos sql pata poblarla: `postgres\to_change\00_03_poblate_table_usuarios.sql`

```sql

--
-- POBLAR TABLA USUARIOS
INSERT INTO usuarios(id_usuario, titulo_y_nombre, roles, activo)
  VALUES(1, 'ing. Mario', 'admin, post_writer, checker', true) ON CONFLICT DO NOTHING;

INSERT INTO usuarios(id_usuario, titulo_y_nombre, roles, activo)
  VALUES(2, 'ing. Neey', 'post_writer, checker', true) ON CONFLICT DO NOTHING;

INSERT INTO usuarios(id_usuario, titulo_y_nombre, roles, activo)
  VALUES(3, 'Lic. Sancho', 'checker', false) ON CONFLICT DO NOTHING;

INSERT INTO usuarios(id_usuario, titulo_y_nombre, roles, activo)
  VALUES(4, 'Civ. Pedro', '', false) ON CONFLICT DO NOTHING;
  
INSERT INTO usuarios(id_usuario, titulo_y_nombre, roles, activo)
  VALUES(4, 'Civ. Pedro', '', false) ON CONFLICT DO NOTHING;
```

3, Agregamos los scripts sql a nuestro bash, pero la tabla la queremos
crear dentro de la base de datos que creamos, por lo que vamos a correr
estos scripts con el usuario y la DB ya creados.

```bash
#!/bin/bash

# DECLARACION
#   ruta de script
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

#   Usuario root
ROOT_USER="mario_root"
ROOT_DB_NAME="postgres"
CMD_SQL_ROOT="psql -v ON_ERROR_STOP=1 -U $ROOT_USER -d $ROOT_DB_NAME -a -f"

#   usuario para aplicacion
ADMIN_USER="mario_admin"
ADMIN_DB_NAME="usuarios_devops"
CMD_SQL_ADMIN="psql -v ON_ERROR_STOP=1 -U $ADMIN_USER -d $ADMIN_DB_NAME -a -f"

#  COMANDO COMO USUARIO ROOT
#    crear usuarioy y base de datos
$(echo $CMD_SQL_ROOT) $SCRIPTPATH/00_02_create_if_not_exists_user_and_data_base.sql

# COMANDO COMO USUARIO ADMIN
#    crear tabla usuarios
$(echo $CMD_SQL_ADMIN) $SCRIPTPATH/00_04_create_table_usuarios.sql

#    poblar tabla usuarios
$(echo $CMD_SQL_ADMIN) $SCRIPTPATH/00_05_poblate_table_usuarios.sql
```

4, Ejecutamos `bash _install_changes.sh` y veremos que solo se crea y
pobla la tabla usuarios, como tenemos el **crea si existe** como ya
existe no crea nada, lo cual nuestros scripts son **idempotentes** por
lo que si ejecutamos luego luego el mismo script no pasara nada.

**Resultado:** Creamos un script para crear tablas y tambien contiene
para crear el usuario y la DB pero como es idempotente podemos ejecutarlo
multiples veces y solo creara lo que no existe.  

**Recordar que** a proposito violamos **normalizacion** para luego
aplicar el cambio con la misma estrategia, viendo como hacerlo sin
perder datos.

![05-creacion-de-tabla-usuarios-y-plablado](./img/05-creacion-de-tabla-usuarios-y-plablado.PNG)

### Creacion de scripts para rollback

Vamos a crear los bash scripts y sql scrips para hacer un rollback, ya
que si algo malo pasa nos veremos en la ncesecidad de quitar los cambios
que aplicamos, en este solo tenemos que hacer lo siguiente

- eliminar usuario mario_admin
- eliminar tabla usuarios
- eliminar la DB usuarios_devops

**NOTA:** en casos futuros esto lo usaremos para quitar cambios sin perder
informacion, pero como estamos creando la DB pues solo se hace esto con
fines demostrativos no practicos.

1, Creamos Script: `postgres\current_state\00_01_DROP_database_and_user.sql`

```sql
--
-- ELIMINAR USUARIO Y DB
DROP DATABASE IF EXISTS usuarios_devops;
DROP USER IF EXISTS mario_admin;
```

2, Creamos script: `postgres\current_state\00_02_DROP_table_usuarios.sql`

```sql
--
-- ELIMINAR TABLA USUARIOS
DROP TABLE IF EXISTS usuarios;
```

3, Creamos script `postgres\current_state\_rollback.sh`

**NOTA:** Como es una operacion peligrosa vamos a pedirle confirmacion
al usuario, y que pueda pasar con una contraseña, para poder en un
**futuro** ejecutar el script desde jenkisn sin problemas.

```bash
#!/bin/bash

# DECLARACION
#   ruta de script
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

#   Usuario root
ROOT_USER="mario_root"
ROOT_DB_NAME="postgres"
CMD_SQL_ROOT="psql -v ON_ERROR_STOP=1 -U $ROOT_USER -d $ROOT_DB_NAME -a -f"

#   usuario para aplicacion
ADMIN_USER="mario_admin"
ADMIN_DB_NAME="usuarios_devops"
CMD_SQL_ADMIN="psql -v ON_ERROR_STOP=1 -U $ADMIN_USER -d $ADMIN_DB_NAME -a -f"

PASS_TO_CHANGE=$1

if [ "$PASS_TO_CHANGE" == "mario" ]
  then
    echo "saltando confirmacion de usuario por pass"
  else
    read -p "ARE YOU SURE THAT YOU WANT RE-CREATE THE DATABASE (YES/NO): " CONFIGM
    if [ "$CONFIGM" != "YES" ]
      then
        echo "cancelado"
        exit 0;
    fi
fi

$(echo $CMD_SQL_ADMIN) $SCRIPTPATH/00_02_DROP_table_usuarios.sql

#    eliminar usuario y base de datos
$(echo $CMD_SQL_ROOT) $SCRIPTPATH/00_01_DROP_database_and_user.sql
```

4, si ejecutamos: `bash current_state/_rollback.sh` qs deberan quitar
los cambios que hicimos, y no habra poblema por que si los queremos
recuperar solo ejecutamos el script para instalar los cambios.

**Resultado:** Tenemos un script para hacer rollback, el cual con solo
ejecutar este se quitaran los cambios que aplicamos, tal como un `CRTL+Z`.

### Ejecutar los scripts en tests manualmente para postgres

**Objetivo:** Lo de arriba lo aplicamos en local, ahora que vimos que
esta bien lo vamos a aplicar a test de forma manual donde

1. Subiremos scripts por FTP
2. Correremos scripts a mano desde la consola

1, Subir por FTP nuestra carpetas, subir de nuevo nuestras carpetas
**CUIDADO** con no eliminar la carpeta de que usar el servicio de
postgres en nuestro servidor.

> **Error:** No se ven los archivos que se subieron por FTP en el contenedor
> y/o tampoco se ven los cambios en estos.  
> **Causa:** Al parecer docker no soporta la actualizacion de volumentes
> en tiempo real o algun tema de permisos, NO SE.  
> **Solucion:** Reiniciar el contenedor `sudo docker restart postgres`
> Cuando subimos por FTP la carpeta/archivos de nuestro docker

![06-subir-scripts-a-servidor](./img/06-subir-scripts-a-servidor.PNG)

2, Entrar al contenedor de postgres: `sudo docker exec -ti postgres bash`
y en `/home/app/` podremos ver los archivos que subimos

> **Error:** `line *: $'\r': command not found`.  
> **Descripcion:** Al ejecutar los scripts que subimos con `bash <script>.sh`
> saca este error.  
> **Causa:** Es un error de **encoding** no son compatible el formato
> de windows con el de linux
> **Solucion:** Convertir los archivos desde el docker host a formato linux
> con `dos2unix ./postgres/*.*`, ahi que asegurar que todos ya tienen
> el formato linux.

3, Ejecutar el script de aplicacr cambios: `bash ./to_change/_install_changes.sh`
como resultado veremos lo mismo que nos mostro en local.

4, Podemos **Comprobar** desde nuestra maquina entrando remotamente a la DB

```r
psql -h 192.168.1.80 -p 5003 -U mario_admin -d usuarios_devops
```

![07-cambios-aplicados-en-test](./img/07-cambios-aplicados-en-test.PNG)

5, Lo mismo pasa en `bash ./current_state/_rollback.sh`, si lo ejecutamos
y tratamos de entrar nos mandara error de que no existe el usuario.

**Resultado:** los scripts para aplicar y quitar los cambios e la DB,
solo tenemos que subirlos y ejecutarlos en el servidor los cuales funcionaran
de la misma manera tanto en local como en nustro servidor en este caso
servidor de test.

## Implementando Pipelines para postgres

**Objetivo:** Vamos a hacer lo mismo que hicimos antes pero ahora
automaticamente desde jenkins, lo cual aremos lo siguiente

1. Subir los scrips con Git
2. Ejecutar los scripst desde un pipeline guardado en jenkinsfile
3. ejecutar el pipeline en jenkins

**Recordar:** Que vamos a usar jenkins como si fuera otro desarrollador
el cual este descargara/actualizara los scripts con git y los ejecutara
con bash scripts, pero estos se ejecutaran desde el contenedor de jenkis
es por eso que es necesario que este tenga acceso a docker y a los
contenedores del docker host.

### Preparativos para crear pipelines

#### Subir proyecto/codigo postgres a Gitea

Como mensionamos necesitamos de git para que jenkins se descarge el
codigo a ejecutar los actualize, asi que vamos a subirlo nuestro proyecto
a Gitea

1, Crear ropositorio en en gitea en mi caso lo cree

1. Nombre: `usuarios_devops_postgres`
2. Vsibilidad: `privado`
3. Rama por defecto: `test`

2, Crear repositorio en nustra carpeta de postgres `./usuarios-devops/postgres> git init`

2,1, Agregamos un Commit `git add . && git commit -m "inicio"`

2,2, Creamos rama y nos cambiamos a esta `git branch test && git checkout test`

3, Agregamos url del repositorio remoto

```r
git remote add origin http://192.168.1.80:3000/mario/usuarios_devops_postgres.git
git push -u origin test
```

**Resultado:** Ya tenemos nuestro codigo subimo al repo remoto y podemos
usarlo desde jenkins para bajar el codigo y luego usarlo.

![08-codigo-para-postgres-subido-al-repo-remoto-de-gitea](./img/08-codigo-para-postgres-subido-al-repo-remoto-de-gitea.PNG)

#### Clonar repo de postgres en jenkis

Como esto solo se va a hacer una ves lo hacemos manualmente, asi que

1, entrar al contenedor postgres

2,vamos a la carpeta donde jenkins tendra el workspace en mi caso `cd cd /home/postgres_workspace/`

3, Indicamos que se usen credenciales guardadas en la maquina `git config --global credential.helper store`

4, Clonamos `git clone http://166.10.0.10:3000/mario/usuarios_devops_postgres.git`.
Si es la primera vez nos pedira introduccier las credenciales, las metemos
y en automatico las guardara.

3, Hacemos un `git pull origin test` para ver si funciona, no nos deberia
de pedir credenciales.

**Resultado:** Tenemos el repositorio dentro del contenedor de jenkins,
el cual podremos trabajar desde jenkins.

![09-codigo-para-postgres-descargado-alcontenedor-de-jenkins](./img/09-codigo-para-postgres-descargado-alcontenedor-de-jenkins.PNG)

#### Ejecucion de primer pipeline en jenkins (sin acciones/modificaciones)

> **Error:** `‘Jenkins’ doesn’t have label *`.  
> **Fuente:** [Link](https://stackoverflow.com/questions/52644447/jenkins-doesnt-have-label-linux).  
> **Descripcion:** Para poder declara un customworkspace lo hacemos dontro
> de agent y este pide un label, pero no sabemos cuales estan disponibles.
> asi que al poner cualquiera saca este error.  
> **Causa:** Debemos de poner un label disponible y acorde a lo que
> estamos haciendo (no se bien la diferencia entre ellos).  
> **Solucion:** Investigar que label tenemos disponible y poner este
> `Go to Manage Jenkins->Manage Nodes`.  

![11-ver-labels-disponibles-en-jenkins](./img/11-ver-labels-disponibles-en-jenkins.PNG)

1, Entramos a jenkins y creamos un job tipo pipeline y por el momento
sera un `Definition: "pipeline script"` para ir probando poco a poco,
por lo que probamos con lo el siguiente el cual deberia de funcionar.

- Fuente: [Documentacion Jenkins -> Pipeline Syntax](https://www.jenkins.io/doc/book/pipeline/syntax)

```groovy
pipeline {
  agent {
    node {
      label 'master'
      customWorkspace '/home/postgres_workspace'
    }
  }
  stages {
    stage('Download code with Git') {
      steps {
        sh 'git pull origin test'
      }
    }

    stage('Deploy') {
      steps {
        sh 'echo empezando deploy'
      }
    }
  }
}
```

2, ejecutamos y deberiamos tener algo como esto

![10-empezando-a-crear-jenkinsfile](./img/10-empezando-a-crear-jenkinsfile.PNG)

**Resultado:** Creamos y ejecutamos un pipeline desde jenkins, el cual
esta trabajando en un folder personalizado, y tambien esta checando
por cualquier cambio al repositorio remoto de gitea.

### Pipelines para deploy changes

**Objetivo:** Vamos a correr el job usando el jenkinsfile que tengamos
en nuestro codigo/proyecto, y este implementara los cambios que tengamos
en el repo remoto en la rama test.

**Nota:** Por ahora nuestros pipelines no van a crear respaldos, ya que
apenas estamos empezando y supongo que no es necesario crear respaldos,
ya que aun no estamos listos para pasar a un ambiente productivo.  
Un paso antes de pasar a produccion agregaremos la parte de creacion de
respaldos, *en este caso antes de aplicar cambios crearemos backups*
caundo haigamos llegado a ese punto.

1, Probamos nuestro pipeline en jenkins como lo venimos haciendo hasta
que funcione, a este lo pasamos a `./to_change/__deploy_changes.jenkinsfile`

```groovy
//http://166.10.0.10:3000/mario/usuarios_devops_postgres.git
pipeline {
    agent {
      node {
        label 'master'
        customWorkspace '/home/postgres_workspace'
      }
    }
    stages {
      stage('Download code with Git') {
        steps {
          sh 'git pull origin test'
        }
      }
      stage('Deploy') {
        steps {
//borrar los scripts anteriores para no ejecutar nada antiguo (cuidado de borrar el contenido no la carpeta)
          sh 'docker exec postgres bash -c "rm -fr /home/app/to_change/*"'
//copiar archivos nuvos a contenedor postgres
          sh 'docker cp ./to_change/. postgres:/home/app/to_change/'
// ejecutar bash script para instalar cambios
          sh 'docker exec postgres bash -c "bash /home/app/to_change/_install_changes.sh"'
        }
      }
    }
}
```

2, Modificamos nuestro job para que traiga el jenkinsfile (pipeline) de git

![12-pipeline-jenkinsfile-desde-git](./img/12-pipeline-jenkinsfile-desde-git.PNG)

3, **Comprobar** Hacemos un cambio en nuestros scripts, en mi caso agrego
un nuevo registro en `postgres/to_change/00_03_poblate_table_usuarios.sql`

```sql
...
INSERT INTO usuarios(id_usuario, titulo_y_nombre, roles, activo)
  VALUES(5, 'Doc. Manjatan', 'root', true) ON CONFLICT DO NOTHING;

SELECT * FROM usuarios;
```

4, hacer un `git add, commit y push` en test, ya que se subio corremos
de nuevo el job y deberian de ejecutarse nuestros nuevos cambios.

![13-ejecucion-deploy-con-jenkinsfile-de-git](./img/13-ejecucion-deploy-con-jenkinsfile-de-git.PNG)

**Resultado:** Ya podemos hacer deploy de nuestros cambios, ya solo
debemos de subir al repo remoto en la rama test y ejecutar el job y
nuestro cambios se aplicaran.

<!-- 
## TODO: PENDIENTE Comprobar persistencia del jenkinsfile

**Introduccion** Una de las ventajas de tener el jenkinsfile en nuestro
proyecto, es que si eliminamos o perdemos el job por cualquier razon,
`facilmente podemos reciperarlo` solo tenemos que crearlo de nuevo
señalando al mismo jenkinsfile de nuestro proyecto, y este seguira
funcionando igual.  
Esto ayuda a la portabilidad entre servidores ya que en un futuro vamos
a implemtar esto mismo pero para diferentes servidores/ambientes, por ejemplo,
para ambieste de calidad o incluso produccion.

**Objetivo:** Vamos a eliminar y crear de nuevo el job para compronbar
que sigue funcionando sin problemas.

1, Eliminamos el Job

-->

### Pipelines para rollback changes

**Introduccion:** Crear este pipeline va a ser muy parecido al que
acabamos de crear, solo que vamos a ejecutar un bash script defierente.

1, Creamos un pipeline que tome el pipeline, igual como antes promero
que tome el pipeline desde el formulario (no desde git) y ya que lo
tengamos funcionando lo pasamos a su jenkinsfile.

2, Ya que tenemos el pipeline creamos `./postgres/current_state/__rollback_changes.jenkinsfile`
en este caso **pedimos una contraseña** ya que es una operacion delicada
que podria hacernos perder datos.

```groovy
pipeline {
  agent {
    node {
      label 'master'
      customWorkspace '/home/postgres_workspace'
    }
  }
  parameters {
    password(name: 'PASS', description: 'Introduce contrasenia de confirmacion')
  }
  stages {
    stage('Download code with Git') {
      steps {
        sh 'pwd'
        sh 'git pull origin test'
        sh 'ls -l'
      }
    }

    stage('Deploy') {
      steps {
//borrar los scripts anteriores para no ejecutar nada antiguo
        sh 'docker exec postgres bash -c "rm -fr /home/app/current_state/*"'
// copiar archivos a contenedor postgres
        sh 'docker cp ./current_state/. postgres:/home/app/current_state/'
// ejecutar bash script para instalar cambios
        sh 'docker exec postgres bash -c "bash /home/app/current_state/_rollback.sh ${PASS}"'
      }
    }
  }
}
```

3, Subimos los nuevos cambios al repositorio remoto de gitea

4, Re-configuramos el job para que tome el jenkinsfile desde Git

**Resultado:** Podemos ejecutar un rollback y para precaucion pedimos
contraseña

![14-ejecucion-rollback-con-jenkinsfile-de-git](./img/14-ejecucion-rollback-con-jenkinsfile-de-git.PNG)

### Job Rollback en calidad Spring

Hacemos lo mismo solo que al crear el job use la rama de calidad

### Portgres: Crear respaldo

**Objetivo:** Cada vez que hagamos un deploy o un rollback vamos a crear
un respaldo, para evitar perder informacion cuando hagamos un cambio.

0, tendremos la siguiente estructura

```r
backups
│   "_create_backup.sh" # script
│   "_last_db_backup.txt" # guarda el nombre del ultimo respaldo para a futuro saber cual recuperar
│
└───outs
        "usuarios_devops-201016-145946.out" # ejemplo de respaldos que se iran creando
```

1, Creamos Script sh para crear respaldo: `postgres\backups\_create_backup.sh`

```sh
#!/bin/bash
set -e # para salir si ahi un error

DB=usuarios_devops_qa
USR=mario_root_qa

NAME_BACKUP="$1"
if [ -z "$NAME_BACKUP" ]; then
  echo "No se definio un nombre de backup definiendo uno";
  NAME_BACKUP="usuarios_devops-$(date +"%y%m%d-%H%M%S").out"
fi

# checar si existe bd, ver si existe base de datos, comprobar que exista bd, existe bd, existe db
EXIST_DB=$(psql -U $USR -dpostgres -tAc "SELECT 1 FROM pg_database WHERE datname = '$DB'")

if ! [ $EXIST_DB = 1 ]; then
  echo "############# No existe la base de datos, SE OMITE CREACION DE RESPALDO ######################";
  exit 0;
fi

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

if ! [ -d "$SCRIPTPATH/outs" ]; then
  echo "No existe directorio $SCRIPTPATH/outs creandolo";
  mkdir -p $SCRIPTPATH/outs
fi

NEW_BACKUP="$SCRIPTPATH/outs/$NAME_BACKUP"

pg_dump -d $DB -U $USR > $NEW_BACKUP

MD5_JAR_BACKUP_CREATED="$(md5sum -t $NEW_BACKUP | cut -d" " -f1)"

# recorrer archivos que terminen en .jar en un for
for fichero in $(ls $SCRIPTPATH/outs/*.out); do
# obtenemos el md5 de cada jar respaldado que tengamos
  MD5_TEMP="$(md5sum -b $fichero | cut -d" " -f1)"
# comprobamos si son iguales
  if [ "$NEW_BACKUP" = "$fichero" ]
    then
      echo "evitamos comparar el mismo archivo"
      continue
  fi

  if [ "$MD5_JAR_BACKUP_CREATED" = "$MD5_TEMP" ]
    then
      echo "Backup ya existe en: $fichero"
      echo "Eliminando backup recien creado"
      rm $NEW_BACKUP
      exit 0
  fi
done

echo $NAME_BACKUP > $SCRIPTPATH/_last_db_backup.txt
echo "Backup Creado: $NAME_BACKUP"
```

2, Agregamos la ejecucion del script tanto en deploy como en rollback

```groovy
stage('Backup') {
  steps {
    sh 'docker exec postgres-test bash -c "mkdir -p /home/app/backups/outs"' //por si no existe
// copiamos el script al contenedor
    sh 'docker cp ./backups/_create_backup.sh postgres-test:/home/app/backups/'
// Mostramos estado/cantidad de los backups
    sh 'docker exec postgres-test bash -c "ls -l /home/app/backups/outs"'
// Ejecutamos el script en el contenedor
    sh 'docker exec postgres-test bash -c "bash /home/app/backups/_create_backup.sh"'
  }
}
```

## Iniciar Capa Backend: Spring

**Objetivo** Vamos a crear un backend el cual solo mostrara los datos
de la tabla usando una plantilla, a futuro vamos a cambiar esto a una
API para ver como se implementan los cambios.

**Al terminar** tendremos una una estructura de carpetas como la
de abajo, como tambien la siguiente vista en el navegador

```r
TODO:
```

### Crear proyecto spring

0, Debemos de tenener instalado el prugin de spring en vscode.

1, Entra a **vs code**, precionamos `Ctrl+Shift+P` y escribimos `spring initializr`
y seleccionamos la opcion para crear proyecto

2, Seguimos instrucciones

```r
Version: 2.3.4
Lang: Java
Grpup: com.mariocorp
ArtifactId: usuarios_devops
Packagin: jar
Java Version: 11
Dependencias:
- DevTools
- Spring Web
- Thymeleaf
- Spring Data JDBC
- Rest Repositories Web
- PostgreSQL Driver
Carpeta: ./spring/to_change/
```

<!-- pom generado-->

**Nota** En mi caso me creo una carpeta y la re-nombre a code y termino
dandome este [pom.xml](./img/15-pom-generado.xml)

3, Abrimos nuestra clase main en mi caso `UsuariosDevopsApplication`
y `vscode` emprezara a desscargar las librerias y al terminar podremos
ejecutar el proyecto.

4,**mientras** compila y descarga dependencias agregamos las variables
de conexion a la base de datos, en mi caso voy a conectarme a test
pero igual podriamos conectarnos a local.

**Recordar que:** tenemos dos instgancias de base de datos una en nuestra
Maquina (en mi caso windows) y otra en una servidor (en mi caso Maquina
virtual ubuntu server) lo cual podemos conectarnos a las dos, pero es
recomendable `conectarnos a la que tenemos en el servidor`, ya que asi
simulamos un abmiente compartido de desarrolla, por ejemplo, nosotros
podriamos trabajar con Backend mientras alguien trabaja en Base de datos
el sube o hace deploy a test y de inmediato veremos el cambio nosotros.

**Recordar que:** en pasos anteriores hicimos rollback por lo cual en este
caso elimina la base de datos, si esta borrada solo corremos el job de
deploy y creara la DB de nuevo, ya que la necesitamos creada para
hacer lo que viene.

En `./spring/to_change/code/src/main/resources/application.properties`

```conf
# Configuracion SQL Data Source: usuarios
spring.datasource.driver-class-name=org.postgresql.Driver
spring.jpa.database-platform=org.hibernate.dialect.PostgreSQL95Dialect
spring.datasource.hikari.connectionTimeout=20000
spring.datasource.hikari.maximumPoolSize=5

# Platilla configuracion
spring.thymeleaf.prefix=classpath:/templates/
spring.thymeleaf.suffix=.html
spring.thymeleaf.encoding=UTF-8
spring.thymeleaf.content-type=text/html

# variables personalizasas
app.log.started-mesagge=aplicacion_usuarios_devops_iniciada

# IP DB
app.net.ipdatabase=192.168.1.80:5003

# Acceso SQL Data Source: usuarios
spring.datasource.url=jdbc:postgresql://${app.net.ipdatabase}/usuarios_devops
spring.datasource.username=mario_admin
spring.datasource.password=mario_admin
```

**Comprobar:** Ejecutamos en proyecto y veremos debera de levantar
correctamente, ahora si entramos a `http://localhost:8080/` no hara
nada, pero lo que pasa es que se esta conectando a la BD.

### Listar todos los usuarios

**Objetivo:** Vamos a consultar la info en la tabla y mostrarla por el
navegador usndo una plantilla en thymeleaf.

**Al terminar** tendremos una estructura de carpetas como la de abajo

```r
│   UsuariosDevopsApplication.java #main
│
└───usuarios
    │   IUsuariosController.java # Cuando inician con una "I" son interfaces
    │   IUsuariosRepository.java
    │   IUsuariosService.java
    │   UsuarioConstants.java
    │
    ├───domain
    │       UsuarioEntity.java
    │
    └───impl
            UsuariosControllerImpl.java
            UsuarioServiceImpl.java
            UsuariosRepositoryPostgres.java
```

1, Crear entity

```java
import java.util.Date;
public class UsuarioEntity {

  private int idUsuario;
  private String tituloYNombre;
  private String roles;
  private boolean activo;
  private Date createAt;

  //getters/setters
}
```

2, Crear las interfaces necesarias (cada uno en su archivo)

```java
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IUsuariosController
public interface IUsuariosController {
  String findallUsuarios(Model model);
}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IUsuariosRepository.java
public interface IUsuariosRepository {

  List<UsuarioEntity> findAll();
  
}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IUsuariosService.java
public interface IUsuariosService {  
  List<UsuarioEntity> findAll();
}
```

3, Implementar el repositorio

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UsuariosRepositoryPostgres implements IUsuariosRepository {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private final String QUERY_GET_ALL_USUARIOS =
  "SELECT id_usuario, titulo_y_nombre, roles, activo FROM usuarios;";

  @Override
  public List<UsuarioEntity> findAll() {
    return jdbcTemplate.query(
      QUERY_GET_ALL_USUARIOS,
      (rs, rowNum) -> new UsuarioEntity(
        rs.getInt("id_usuario"),
        rs.getString("titulo_y_nombre"),
        rs.getString("roles"),
        rs.getBoolean("activo")
      )
    );
  }
}
```

3, Implementar el service

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements IUsuariosService {

  @Autowired
  private IUsuariosRepository iUsuariosRepository;

  @Override
  public List<UsuarioEntity> findAll() {
    return iUsuariosRepository.findAll();
  }
}
```

4, Implementar el Controlador **Nota** se puede hacer de dos formas el
controlador cmo esta comentado y el que no, deje el primero ya que me
parece mas elegante.

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.servlet.ModelAndView;

@Controller
public class UsuariosControllerImpl implements IUsuariosController {

  @Autowired
  IUsuariosService iUsuariosService;

  @Override
  @GetMapping({"/", ""})
  public String findallUsuarios(Model model) {
    List<UsuarioEntity> usuarios = iUsuariosService.findAll();
    model.addAttribute(UsuarioConstants.DTO_LIST_USUARIOS, usuarios);
    return UsuarioConstants.VIEW_LIST_USUARIOS;
  }

  /*
  @Override
  @GetMapping({"/", ""})
  public ModelAndView findallUsuarios() {
    //declaracion
    var models = new HashMap<String, Object>();

    //obtener usuarios
    List<UsuarioEntity> usuarios = iUsuariosService.findAll();

    //empaquetar/enviar
    models.put(UsuarioConstants.DTO_LIST_USUARIOS, usuarios);
    return new ModelAndView( models);
  }
  */
}
```

5, Crear plantilla/vista

<!-- iterar en plantilla iterar, iterar th iterar, iterar con thymeleaf -->

```html
<html>
  <head>
    <title>Lista usuarios</title>
  </head>
  <body>
    <table>
      <thead>
        <td>ID</td>
        <td>Nombre</td>
        <td>Roles</td>
        <td>Activo</td>
      </thead>
      <tr th:each="usuario : ${DTO_LIST_USUARIOS}" >
        <td th:text="${usuario.idUsuario}"></td>
        <td th:text="${usuario.tituloYNombre}"></td>
        <td th:text="${usuario.roles}"></td>
        <td th:text="${usuario.activo}"></td>
      </tr>
    </table>
  </body>
</html>
```

**Resultado:** Al hacer una peticion en `http://localhost:8080` Deberiamos
de poder poder la siguiente vista con los registros que ahi en la BD.

![15-listar-usuario-thymeleaf](./img/15-listar-usuario-thymeleaf.PNG)

### Usar diferentes configuraciones en el jar para los diferentes ambientes

<!--
Construir Jars Diferentes para los diferentes ambientes 
(no es construir un diferente jar ya que usamos el mismo jar pero lo
ejecutamos de forma diferente, pero yo luego lo busco asi jaja)
-->

**Introduccion** Si nos fijamos bien la comunicacion entre contenedores
es diferente ya que no usan la IP de nuestro docker host, es decir, usan
IPs diferentes, por ejemplo, para conectarnos a la db desde windows al
Host usamos una IP que empieza por `192...`, mientras que para conectarnos
del contenedor de jenkis al de gitea usamos una IP `166...`.  
Por esta razon **nos va a dar problemas** si la app que generemos en
nuestra maquina y que vimos que funciona en nuestro local, no funcionara
si lo subimos al contenedor porque la cominicacion entre maquinas y entre
contenedores es diferente.  
**Debemos** de idear una formma de cambiar la IP cuando estemos usando,
y para esto nos sirven los pefiles lo cual nos vasmso va respaldar de
[esta](https://mkyong.com/spring-boot/spring-boot-profile-based-properties-and-yaml-example/) guia

**Al terminar** tendremos 3 `.properties` diferentes ll cual uno sera
para varialbes/configuraciones que compartiran tosds los ambientes
(como es la conf. del html), y otros 2 los cuales tendran las diferentes
IPs, usuarios, pass, etc para comunicarse al ambiente que queremos,

**Objetivo:** Vamos a crear dos perfiles `local y test` para poder construir
un jar para local (mi maquina windows) y otro para el host de test, ya
a futuro agregaremos mas para produccion y/o calidad.

0, Podemos comprobar la comunicacion entre contenedores, metiendonos
al contenedor y haciendo ping a la IP que le pertenece al otro contenedor,
Ips que podemos ver en su respectivo `docker-compose-xxx.yml`

1, Creamos los .properties,

1,1, `application.properties`: son las configuraciones/variavles comparidas
por ejemplo donde estan nuestras plantilla, su cofificacion, el driver
de db, y varibles

<!-- perfil por defecto, poner perfil por defecto -->

```r
# perfil por defecto, el cual si no indicamos ninguno se usara este
spring.profiles.active=local

# Configuracion SQL Data Source: usuarios
spring.datasource.driver-class-name=org.postgresql.Driver
spring.jpa.database-platform=org.hibernate.dialect.PostgreSQL95Dialect
spring.datasource.hikari.connectionTimeout=20000
spring.datasource.hikari.maximumPoolSize=5

# Platilla configuracion
spring.thymeleaf.prefix=classpath:/templates/
spring.thymeleaf.suffix=.html
spring.thymeleaf.encoding=UTF-8
spring.thymeleaf.content-type=text/html
```

1,2, `application-local.properties`: este lo usaremos para el desarrollo
en local, el cual se conecta al host no al contenedor

```r
# Configuracion para LOCAL
app.net.ipdatabase=192.168.1.80:5003

# Acceso SQL Data Source: usuarios
spring.datasource.url=jdbc:postgresql://${app.net.ipdatabase}/usuarios_devops
spring.datasource.username=mario_admin
spring.datasource.password=mario_admin
```

1,3, `application-test.properties`: Este lo usamos en test el cual no
se comunica con el host, se cominica con el contenedor.

```r
# Configuracion para TEST
app.net.ipdatabase=166.10.0.21:5432

# Acceso y configuracion SQL Data Source: usuarios
spring.datasource.url=jdbc:postgresql://${app.net.ipdatabase}/usuarios_devops
spring.datasource.username=mario_admin
spring.datasource.password=mario_admin
```

2, **Comprobar**

2,1, Levantamos el proyecto con nuestro IDE y veremos que funciona igual

2,1, Construimos con maven (la terminal) y ejecutamos y deveria de funcionar igual

```r
# generar jar
\spring\to_change\code>mvn clean packagin
java -jar .\target\usuarios_devops-0.0.1-SNAPSHOT.jar

# En los log deberia de aparecer algo como esto, lo cual significa que
# nuestro perfil por defecto esta funcionando
INFO 1 --- [main] c.m.u.UsuariosDevopsApplication : The following profiles are active: local
```

2,2, Ejecutamos el mismo jar pero ahora con el perfil de test el cual
deberia de fallar (connect timed out) por no poder alcanzar la IP `166...`

<!-- usar perfil en windows, -->

```r
# Ahi algo raro en la terminal integrda de vscode ya no no me acepta el
# comando, pero si el el cmd Error: Unable to access jarfile .profiles.active=test
java -jar -Dspring.profiles.active=test usuarios_devops-0.0.1-SNAPSHOT.jar

# Al principio de los logs deberiamos de ver algo como esto, el cual podemos
# ver que el perfil activo es test
INFO 1 [main] UsuariosDevopsApplication : The following profiles are active: test
```

**Nota:** Si ponemos `-Dspring.profiles.active=cualquiercosa` se va usar
el perfil de local ya que es nuestro default.

**Resultado:** Ya podemos usar el jar que construimos en diferentes
ambientes, solo debemos de especificar el perfil a la hora de ejecutarlo.

### Preparativos para ejecutar jar en diferentes ambientes

**Introduccion:** al igual que hicimos en base de datos donde ejecutamos
un script para instalar los cambios, vamos a hacer lo mismo en este caso
pero para instalar los cambios y ejecutar el jar.

**Aclaracion:** Mientras todos estos scripts los ejecutaremos en local
(en mi caso windows), para saber que to esta funcionando correctamente,
luego los ejecutaremos en linux (ambiente test) y deberian de funcionar
exactemente igual.

**Recordar que** Tenemos varias formas de crear un contenedor el cual
ejecute nuestro jar.

- levantar el contenedor junto con jar y qie este lo mantenga vivo
- levantar el jar mateniendolo vivo imprimiento un archivo de log y
  luego ejecutar el jar que escriba en dicho jar.

En nuestro `docker-compose-spring.yml` vemos que el contenedor se mantiene
imprimiendo app.log, por lo que vamos a optar por la segunda opcion
pero bien podriamos cambiar el `command` por `command: java -jar ...`
y usarioamos la primera opcion.

```yml
services:
  spring:
    container_name: spring
    ...
    volumes:
      - ./app/:/app/
    command: tail -f /app/logs/app.log
    ...
```

**Recordar que** tenemos el codigo en `to_change/` que contiene el jar
que queremos implementar, y tenemos la carpeta `app/` que es la equivalente
a `current_state/` en base de datos que contiene el jar que se esta
ejecutando actualmente en produccion, el cual en su momento dado sera
sustituido por el nuevo jar.

**Recordar que** jenkins funje como otro desarrollador el cual va a
descargar los archivos necesarios con git y hara lo necesario detendra
el jar actual, pondra el nuevo y lo ejecutara.

#### Crear Script install_changes para Spring

**Objetivo:** Sabiendo esto vamos a crear un script que nos ayudara a
hacer crear el jar y ponerlo en el lugar donde los vamos a ejecutar
en este caso en `app/`

1, Lo construimos poco a poco probando con el `git bash` hasta que nos
quede

```bash
#!/bin/bash
set -e # para salir si ahi un error

#conseguimos la ruta donde esta el scritp para no tener problemas de portabilidad
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# creamos jar
cd $SCRIPTPATH/code
mvn -B -DskipTests clean package
cd $SCRIPTPATH

echo "Copiando nuevo jar a ./app/jar"
cp ./code/target/*.jar $SCRIPTPATH/../app/jar/usuarios_devops.jax
echo "Instalacion completa"
```

**Resultado:** Si ejecutamos el script veremos que el nuevo jar lo copia
a la carpeta donde vamos a ejecutar el script

#### Crear Script detener app spring (stop_app)

**Objetivo:** Antes de crear el de iniciar creamos el de detener porque
nos vamos a enfrentar al preblema de como detener la app, lo cual es un
poco mas complicado de lo que parese ya que solo podremos hacerlo por
medio de la terminal y para eso debemos de saber su `PID`.

##### Escribir PID asignado del jar que ejecutamos en archivo

para lograr esto debemos de modificar nuestro codigo para que lo haga

1, Agregamos ruta/archivo en el `.properties` donde queremos que se guarde
el cual va a ser diferente para windows como para linux, esta debe ser
en `spring.pid.file` ya que tenemos una forma preterminada de hacer esto
el cual tomara el valor de esta variable

1,1, En `application-local.properties`

```r
...
# ruta absoluta
spring.pid.file=E:\\cursos\\0activ\\proyectos-test\\usuarios-devops\\spring\\app\\pid.txt
# ruta dinamica que tomara la ruta donde se ejecuto el jar
#spring.pid.file=pid.txt
```

1,2, En `application-test.properties`

```r
...
spring.pid.file=/app/jar/pid.txt
```

2, Implementamos en nuestro main para escribir este archivo, el cual
consta de un listener predefinido el cual

```java
import org.springframework.boot.context.ApplicationPidFileWriter;
...
public static void main(String[] args) {
  SpringApplication springApplication = new SpringApplication(UsuariosDevopsApplication.class);
  springApplication.addListeners(new ApplicationPidFileWriter());
  springApplication.run(args);
}
```

**Comprobar**: Si generamos el jar y ejecutaomos en local veremos que
se crea un archivo (en la ruta absoluta que le indicamos o si fue solo
el nombre de archivo en donde ejecutamos el comando) con un numero que
corresponde al PID

##### Crear script

Ya tenemos el PID ahora vemos a detener la aplicacion usando el PID de
este archivo.

```bash
#!/bin/bash

# NOTAS
# REQUISITOS:
# - RUTA_DE_ESTE_SCRIPT/pid.txt
#     la aplicacion java debe de escribir unicamente el pid en este archivo
#     en caso contrario se dara porsentado que no ahi una aplicacion corriendo, lo cual puede
#     no detener la aplicacion actual y luego mandar un ERROR DE PUERTO YA OCUPADO


SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PREVIUS_PID=$(cat $SCRIPTPATH/pid.txt)

echo "detiniendo PID: $PREVIUS_PID del deploy anterior"

if [ -z $PREVIUS_PID ]
  then
    echo "no hay aplicacion anterior a terminar"
  else
    if [ $OSTYPE = "msys" ]
      then
        echo "taskkill /F /PID $PREVIUS_PID"
        taskkill //F //PID $PREVIUS_PID
    else
      echo "matando PID anterior: $PREVIUS_PID"
      kill -15 $PREVIUS_PID
      echo "pid $PREVIUS_PID terminado"
    fi
fi
```

**Resultado:** Si ejecutamos nuestro jar el cual ya deberia de escribir
su pid al lado de donde esta nuestro script, y ya que este iniciado
ejecutamos este script y matara el proceso de nuestra aplicacion.

![16-ejecucion-script-para-detener-app](./img/16-ejecucion-script-para-detener-app-en-windows.PNG)

#### Crear Script iniciar app spring (start_app)

**Objetivo:** Crearemos un script que nos ayude a ejecutar el jar y que
nos indique y que este dure hasta que se ejecute el jar, ya que vamos a
necesitar de esta caracteristica para del lado de jenkins saber si se
inicio correctamente la aplicacion.

1, Creamos Script

1,1,

```bash
#!/bin/bash
set -e # para salir si ahi un error

# NOTAS
# CUIDADO: no ejecutar este script con "sh x.sh" por que causara error
# se probo este script con "bash x.sh" con la version 5.0.3(1)-release (x86_64-pc-linux-gnu)

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# suponemos que si estamos en windows estamos en local y su estamos en
# linux estamos en un ambiente en este caso solo tenemos test
case "$OSTYPE" in
  msys*)
    ENVIROMENT="local"
  ;;
  linux*)
    ENVIROMENT="test"
  ;;
  *)
    echo "SO desconocido"
  ;;
esac

# texto que buscara en los logs para que sepa que inicio bien
MESSAGE_APP_STARTED="Started UsuariosDevopsApplication in"

# archivo donde se escribiran los logs
FILE_LOGS=$SCRIPTPATH/logs/app.log

# ubicacion de nuestro jar a ejecutar
JAR_FILE=$SCRIPTPATH/jar/usuarios_devops.jar

echo "Start app"

# ejecutamos el jar e indicamos que escriba en nuestro archivo de los
# logs, tambien al final tenemos un "&" para que no se quede el script
# atorado en el comando, ya que este dura lo que la aplicacion este levantada
#y de este modo ejecuta el comando y sigue con el siguiente
java -jar -Dspring.profiles.active=$ENVIROMENT $JAR_FILE >> $FILE_LOGS &

#si estamos en windows en los logs ya que no soporta esta funcion windows
case "$OSTYPE" in
  msys*)
    echo "Busqueda en logs no soportada en windows, ver los logs manualmente con 'tail -f $ENVIROMENT/logs/app.log'"
    exit 0
  ;;
  linux*)
    echo "Esperando inicio"
  ;;
  *)
    echo "SO desconocido"
  ;;
esac

# buscamos en cada linea el texto que nos indica que ya termino de iniciar
# o n caso contrario textos que indican que fallo el inicio
while read line; do
  case "$line" in
    *"$MESSAGE_APP_STARTED"* )
      echo "Iniciado exitoso"
      break;
      ;;
    *"Shutdown completed"* )
      echo "Cerrado"
      exit 1
      ;;
    *"Application run failed"* )
      echo "error al iniciar"
      exit 1
      ;;

    * )
      echo $line
      ;;
  esac
# con esto indicamos que las iteraciones ocurran por cada linea nueva en el log
done < <(tail -f $FILE_LOGS)

echo "start end"
```

**Resultado:** Al ejecutar el script iniciara nuestra aplicacion, y los
logs se guardaran en un archivo, y tambien deberia permenecer el script
ejecutandose asta que termine de iniciarse correctamente o falle al
iniciar.

![17-ejecucion-script-para-iniciar-app-en-windows.PNG](./img/17-ejecucion-script-para-iniciar-app-en-windows.PNG)

### Ejecutar los scripts en tests manualmente para spring

**Objetivo:** tal como tambien lo hicimos en base datos es muy parecido
para spring, vamos a subir por ftp lo necesario y ejecutaremos los sig.
scripts que nos permitiran hacer deploy

1. Subir por FTP la carpeta de Spring al contenedor de spring
2. Ejecutar el script de install_changes, para llevar lo que ahi en el
   codigo al un jar y este jar a la ruta de ejecucion, aqui lo nuevo
   que veremos es que si funcionara la busqueda de texto en logs para
   saber si inicio correctamente nuestra app.
3. Ejecutar el script de start_app, para iniciar el jar que esta en la
   ruta de ejecucion
4. Ejecutar el script de stop_app, para detener nuestra app que levantamos

1, Por un cliente FTP (en mi caso FileZilla) subimos la carpeta de Spring
a la misma que tenemos en el servidor (borrar todo en el host y copiar
todo de nuevo a host).

![18-pasar-carpeta-spring-loca-acarpeta-spring-host](./img/18-pasar-carpeta-spring-loca-acarpeta-spring-host.PNG)

2, Entrar al contenedor de spring y ejecutar los scripts

```r
# construir la app
bash /usuarios_devops/to_change/_install_changes.sh
# Si es la primera vez que lo ejecutamos se descargara las dependencias

# iniciar la app
bash /usuarios_devops/app/_start-app.sh
# deberia de terminar el script asta que la app incie correctamente

# detener
bash /usuarios_devops/app/_stop_app.sh
# deberia de detener la app
```

**Resultado:** Comprobamos que nuestras scripts funcionan correctamente
test, los cuales construlleron, iniciario y detubieron nuestra app.

**IMPORTANTE:** Se hizo esto con `fines demostrativos`, cuando lo auto-
maticemos con jenkins, vamos a construir la app en el contenedor de
jenkins, y ya solo iniciaremos o detendremos la app en el contenedor
de spring, esto para no estar trayendo el codigo de aqui para alla
lo cual no es necesario hacer tanto movimiento lo cual `agrega complejidad`
y por ende mas provavilidad de que falle.

### Crear Script para crear respaldo (_create_backup)

**Introduccion:** Para poder hacer rollback promero debemos de respaldar
el jar que esta actualmente en ejecucion, lo mas recomendable que sea
el mismo que se esta ejecutando en produccion (claro este jar debera
conectarse a test), por lo que vamos a crear de una vez un script para
crear un respaldo.

1, Vamos a crear y ejecutar el script en local para que ya que funcione
probarlo en test

- Lo creamos en `spring\backups\_create_backup.sh`
- No creara respaldos de arhivos ya respaldados, esto lo savemos comparando
  el md5 de los jars que ya existem
- El backup creado el nombre lo guardamos en `backups\last_backup_created.txt`
  para luego usarlo cuando hagamos en rollback

```bash
#!/bin/bash
set -e # para salir si ahi un error

# ruta donde se encuentra nuestro script
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# ruta donde guardaremos los backups de jars
PATH_JAR_BACKUPS="$SCRIPTPATH/jar-backups"

# ruta donde esta el backup de nuestro
JAR_TO_BACKUP="$SCRIPTPATH/../app/jar/usuarios_devops.jar"

# comprobamos que exista el archivo a respaldar
if ! [ -f $JAR_TO_BACKUP ]
  then
    echo "El archivo a respaldar no existe: $JAR_TO_BACKUP"
    exit 1
fi

# obtener md5 del jar a respaldar
MD5_JAR_TO_BACKUP="$(md5sum -b $JAR_TO_BACKUP | cut -d" " -f1)"

# recorrer archivos que terminen en .jar en un for
for fichero in $(ls $PATH_JAR_BACKUPS/*.jar); do
# obtenemos el md5 de cada jar respaldado que tengamos
  MD5_TEMP="$(md5sum -b $fichero | cut -d" " -f1)"
# comprobamos si son iguales
  if [ "$MD5_JAR_TO_BACKUP" = "$MD5_TEMP" ]
    then
      echo "Ya existe el respaldo en: $fichero"
      exit 0
  fi

  echo "fichero: $fichero - md5: $MD5_TEMP";
done

#nombre que tendra nuestro backup
NAME_BACKUP="usuarios_devops-$(date +"%y%m%d-%H%M%S").jar"

cp $JAR_TO_BACKUP $PATH_JAR_BACKUPS/$NAME_BACKUP

# guardamos el nombre del respaldo para luego usarlo al hacer rollback
echo "$NAME_BACKUP" > $SCRIPTPATH/last_backup_created.txt

echo "Backup Creado: $PATH_JAR_BACKUPS/$NAME_BACKUP"
```

2, Subimos la carpeta de bakcups son respaldos, solo es necesario
el script y la carpeta donde se guardaran.

2,1, Ejecutamos `bash usuarios_devops/backups/_create_backup.sh` y
deberia de crear el respaldo o indiar que ya se respaldo.

### Crear Script para rollback al ultimo jar respaldado

1, Lo mismo lo vamos creando en nuestro local y vamos probando ya que
este liesto lo subimos a test y ptobamos que igual funcione.

```bash
#!/bin/bash
set -e # para salir si ahi un error

# ruta donde se encuentra nuestro script
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# ruta donde guardaremos los backups de jars
PATH_JAR_BACKUPS="$SCRIPTPATH/jar-backups"

# extraemos el nombre del ultimo jar respaldado del last_backup_created.txt
NAME_JAR_TO_RECOVER="$(cat $SCRIPTPATH/last_backup_created.txt)"

# el jar que se va a meter
JAR_TO_RECOVER=$PATH_JAR_BACKUPS/$NAME_JAR_TO_RECOVER

#el jar que se va a sustituir
EXEC_JAR="$SCRIPTPATH/../app/jar/usuarios_devops.jar"

echo "Copiando $JAR_TO_RECOVER a $EXEC_JAR"
echo "..."

# averiguamos si existe el archivo a recuperar
if ! [ -f $JAR_TO_RECOVER ]
  then
    echo "El archivo a recuperar NO existe: $JAR_TO_RECOVER"
    exit 1
fi

#obtenemos md5 de ambos jars (el respaldo y el actual) para poser daber que no son los mismos
MD5_EXEC_JAR="$(md5sum -b $EXEC_JAR | cut -d" " -f1)"
MD5_JAR_TO_RECOVER="$(md5sum -b $JAR_TO_RECOVER | cut -d" " -f1)"

if [ "$MD5_JAR_TO_RECOVER" = "$MD5_EXEC_JAR" ]
  then
    echo "El archivo $NAME_JAR_TO_RECOVER ES EL MISMO al que se quiere sustituir"
    exit 1
fi

cp $JAR_TO_RECOVER $EXEC_JAR

echo "Jar recuperado: $JAR_TO_RECOVER"
```

2, Subimos el script a test y al ejecutar deberia de funcionar igual.

### RESUMEN Probando escenario de hacer deploy y luego rollback (manualmente)

**Objetivo** Vamos a seguir los siguientes pasos para simular un flujo
detrabajo, los cuales comprobaremos que nuestros scripts funcionan
de forma correcta.

**Nota:** Detenemos e iniciamos la app varias veces por que no es correcto
sustituir un jar cuando este esta en ejecucion ya que no es correcto,
y podria causar errores raros.

Todo esto en local primero y luego en test

1. Checar estatus actual: Para saber en que estado esta nuestra app
2. Crear un nuevo cambio: Solo vamos a hacer un cambio visual
3. detener la app actual
4. crear respaldo
5. Instalar cambios
6. iniciar app: deberiamos de ver los cambios
7. detener la app> que tiene nuestros cambios
8. hacer rollback
9. iniciar app
10. Deberiamos de tener el estatus que teniamos antes

1, checar estatus actual

1,1, Si no esta iniciada nuestra aplicacion podemos ejecutar: `bash ./spring/app/_start-app.sh`

![19-probar-scripts-spring-estado-actual](./img/19-probar-scripts-spring-estado-actual.PNG)

2, Crear nuevo cambio

2,1, En mi caso solo agrego algo visual en `to_change\code\src\main\resources\templates\usuarios\lista.html`

```html
...
    </table>
    <h3>Cambio a deployar y luego a quitarlo con rollback</h3>
...
```

3, detener la app actual: `bash ./spring/app/_stop_app.sh`, el cual
al hacerlo ya no podremos hacer peticiones web a nuestra app.

```r
# salida
detiniendo PID: 9956 del deploy anterior
taskkill /F /PID 9956
SUCCESS: The process with PID 9956 has been terminated.
```

4, crear respaldo: `bash ./spring/backups/_create_backup.sh`, en mi caso
dise que el backup ya existe, pero si no existiera lo deberia de crear.

5, Instalar cambios con `bash ./spring/to_change/_install_changes.sh` el
cual generar un nuevo jar y ponerlo en `./spring/app/jar/`

6, iniciar app: Con `bash ./spring/app/_start-app.sh` y al final deberia
de verse nuestro cambio.

![20-probar-scripts-spring-estado-despues-de-instalar-cambios](./img/20-probar-scripts-spring-estado-despues-de-instalar-cambios.PNG)

7, detener la app: que tiene nuestros cambios: `bash ./spring/app/_stop_app.sh`
esto para no sustituir un jar que esta en ejecucion ya que hacer esto
no es correcto y podria causar cosas raras.

8, hacer rollback: `bash ./spring/backups/_spring_rollback_to_last_backup.sh`

```r
# SALIDA
Copiando /e/cursos/0activ/proyectos-test/usuarios-devops/spring/backups/jar-backups/usuarios_devops-201014-145434.jar a /e/cursos/0activ/proyectos-test/usuarios-devops/spring/backups/../app/jar/usuarios_devops.jar
...
Jar recuperado: /e/cursos/0activ/proyectos-test/usuarios-devops/spring/backups/jar-backups/usuarios_devops-201014-145434.jar
```

9, iniciar app: `bash ./spring/app/_start-app.sh`

10, Deberiamos de tener el estatus que teniamos antes, y en mi caso es
verdad tengo el

![Estado recuperado](./img/19-probar-scripts-spring-estado-actual.PNG)

11, Ahora hacemos lo mismo pero ahora en test, lo cual deberia de funcionar
igual, es **importante** que nos sepamos este proceso ya si falla algo
sabremos como repararlo y es algo que vamos a hacer incluso cuando estemos
en produccion.

**Resultado:** Comprobamos que nuestros scripts que creamos funcionan
correctamente tanto en test como en produccion, y la **ventaja** es que
hicimos esto de una forma sensilla solo ejecutando scrips, lo cual
nos facilitara la vida estando en jenkins.

## Implementando Pipelines para Spring

**Introduccion:** Ya probamos nuestros scripts manualmente y vimos que
funcionan bien, ahora vamos a ejecutarlos juntos para lograr un objetivo
y de una forma automatica, para esto crearemos los pipelines en formato
jenkinsfile para ejecutarlos desde jenkins.

**Aclaracion:** Vamos tener una pequeña diferencia de como hacerlo en
jenkins, ya que hasta ahora hemos estando ejecutando la construccion del
jar en el contenedor de spring, pero ahora vamos a hacerla construccion
en el contenedor de jenkins para no trae de aqui para alla el codigo,

**Aclaracion2:** Los respaldos los vamos a guardar en el contenedor
de jenkins ya que este tendra la capacidad de subirlos al repositorio
y asi que todos los demas puedan tenelors. Tambien estamos pensando en
jenkins como otro desarrollador el cual hace los respaldos, asi que si
preguntamos por los respaldos le preguntamos a jenkins que es nuestro
desarrollador que tiene esa responsabilidad.

### Crear repositorio y subirlo a gitea

**Objetivo:** La forma en que va a obtener todo lo necesario jenkis
es por medio del un repositorio remoto (en nuestro caso gitea), asi que
vamos subir nuestro codigo al repositorio.

1, Creamos repo remoto en gitea y obtendremos la ruta http para ligarlo

1, Agregamos: `spring/.gitignore`, ya que los sig archivos no es necesario
rastearlos e incluso podrian crear confuciones si se actualizan estos
en otros ambientes.

```r
app/logs/*.log
app/pid.txt
backups/last_backup_created.txt
```

1, Iniciamos git: `\usuarios-devops\spring> git init`

2, Agregamos cambios y hacemos commit

3, Enlazamos a repo remoto: `git remote add origin http://192.168.1.80:3000/mario/usuarios_devops_spring.git`

4, Subimos: `git push origin test` si no tenemos rama test la creamos

**Resultado:** tenemos listo nuestro repo para que se lo descarge jenkins

![21-repositorio-remoto-spring-creado](./img/21-repositorio-remoto-spring-creado.PNG)

### Descargar-Clonar repositorio Spring en Jenkins

**Objetivo:** por motivos de seguridad la primera ves lo vamos a hacer
manualmente, ya que nos pedira credenciales y lugo usara las que se
guarden automaticamente, ya que si automatizamos la clonacion tendriamos
que repartir la contraseña en mas archivos, si no se la piden esque estan
usando el mismo usuario para crear el repo de postgres que ya clonamos
lo cual podria ser una buena idea de seguridad tener un usuario diferente.

1, Entramos al conetendor de jenkins

2, Entramos a la carpeta `cd /home/spring_workspace/`

**CUIDADO:** La ip que nos da gitea para clonar repo y la que tenemos
que usar en el contenedor no son iguales.

3, Clonamos repositorio `git clone http://166.10.0.10:3000/mario/usuarios_devops_spring.git`

4, Nos aseguramos que estemos en la rama test, si no la creamos y nos
cambiamos a esta

**Resultado:** Ya tenemos todo lo necesario de spring en nuestro
contenedor de jenkins

![22-repositorio-clonado-en-contenedor-jenkins](./img/22-repositorio-clonado-en-contenedor-jenkins.PNG)

### Crear pipeline para deploy changes

1, Entramos a jenkisn y creamos job de tipo pipeline

> **Error:** `NoSuchMethodError: No such DSL method 'withMaven'`.  
> **Descripcion:** A la hora de ejecutar el pipeline de abajo muestra
> este error y no se ejecuta el stage del pipeline que contiene el withMaven.  
> **Causa:** No encuentra el metodo por que no tenemos instalado el
> plugin que nos trae este metodo.  
> **Solucion:** Instalar el plugin `Pipeline Maven Integration`

-

> **Error:** `Could not find specified Maven installation 'mavenXXX'`
> **Descripcion:** lo mismo que el error de arriba
> **Causa:** No hemos seleccionado que maven usar para nuestra compilacion
> **Solucion:** En jenkins ir a `Global Tool Configuration -> Maven -> add maven`
> y fijarse que lo que indicamoe en `maven: 'maven3'` sea igual al
> nombre que agregamos

2, En su configuracion del pipeline lo dejamos como `pipeline script`
mientrar para irlo creando y probarlo.

```groovy
pipeline {

  agent {
    node {
      label 'master'
      customWorkspace '/home/spring_workspace'
    }
  }

  stages {

    stage('Download code with Git') {
      steps {
        sh """
          git pull origin test
        """
      }
    }

    //Cuidado ahi que crear primero el respaldo y luego construir ya que aunque sea el
    //mismo codigo al ser diferente contruccion el md5 es diferente
    stage('Create backup') {
      steps {
        sh """
          docker cp spring:/usuarios_devops/app/jar/usuarios_devops.jar ./app/jar/usuarios_devops.jar
          bash ./backups/_create_backup.sh
        """
      }
    }

    stage ('Build') {
      steps {
        withMaven(maven: 'maven3') {
          sh """
            bash to_change/_install_changes.sh
          """
        }
      }
    }

    stage('Stop Spring') {
      steps {
        sh """
          docker exec spring bash -c "bash /usuarios_devops/app/_stop_app.sh"
        """
      }
    }

    // recordar que _install_changes.sh copia el archivo generado a /app/jar/ de la carpeta de jenkins
    stage('Install changes') {
      steps {
        sh """
          docker cp ./app/jar/usuarios_devops.jar spring:/usuarios_devops/app/jar/usuarios_devops.jar
        """
      }
    }

    stage('Start Spring') {
      steps {
        sh """
          docker exec spring bash -c "bash /usuarios_devops/app/_start-app.sh"
        """
      }
    }
  }
}
```

3, Pasamos el pipeline a su propio archivo: `spring\to_change\_spring_deploy_changes.jenkinsfile`

4, Agregamos cambios a commit y subimos

5, Configuramos el job para que lo tome del jenkinsfile

### Crear pipeline para rollback al ultimo resplado creado

1, Creamos un nuevo job en jenkis

2, Creamos el pipeline

```groovy
//url de repo: http://166.10.0.10:3000/mario/usuarios_devops_spring.git
pipeline {
  agent {
    node {
      label 'master'
      customWorkspace '/home/spring_workspace'
    }
  }
  stages {

    stage('Stop Spring') {
      steps {
        sh """
          docker exec spring bash -c "bash /usuarios_devops/app/_stop_app.sh"
        """
      }
    }

    stage ('Rollback') {
      steps {
        sh """
          bash ./backups/_spring_rollback_to_last_backup.sh
          docker cp ./app/jar/usuarios_devops.jar spring:/usuarios_devops/app/jar/usuarios_devops.jar
        """
      }
    }

    stage('Start Spring') {
      steps {
        sh """  
          docker exec spring bash -c "bash /usuarios_devops/app/_start-app.sh"
        """
      }
    }
  }
}
```

3, Pasamos el pipeline a su propio archivo: `spring\backup\_spring_rollback_changes.jenkinsfile`

4, Agregamos cambios a commit y subimos

5, Configuramos el job para que lo tome del jenkinsfile

### RESULTADO: Comprobar pipelines

**Objetivo:** Vamos a eliminay crear de nuevo los pipelines (apuntando
al mismo jenkinsfile) para ver que seguiran funcionando igual, y vamos
a simular meter un error en nuestro codigo donde haremos.

1. eliminar los dos jobs de jenkins
2. Ver el estado actual de nuestra app que funciona bien
3. Meter un error agrede en mi caso un query mal hecho
4. deployar el error y ver que falle
5. hacer un reollback y deberia de funcionar como antes
6. metemos un cambio corrijiendo el error y hacemos deploy
7. la app de ver nuestra app corregida y funcionando

0, eliminar jobs

1, Estado actual

![23-estado-actual-de-app-probando-spring-pipelines](./img/23-estado-actual-de-app-probando-spring-pipelines.PNG)

2, Meter un error, en este caso metemos un campo que no existe, deberiamos
de ver el error cuando desplegemos en local pero esto es un ejemplo
ilustrativo nadamas.

```java
...
public class UsuariosRepositoryPostgres implements IUsuariosRepository {

  private final String QUERY_GET_ALL_USUARIOS =
  "SELECT id_usuario, titulo_y_nombre, roles, activo, noexiste FROM usuarios;";
...
```

3, metemos cambios a commit y subimos a test

4, ejecutamos job para deploy cambios, en el que me da los siguientes
logs (solo dejo lo relevante)

```r
Started by user mario
...
Running on Jenkins in /var/jenkins_home/workspace/spring-deploy-changes
...
Commit message: "meto error"
...
+ git pull origin test
From http://166.10.0.10:3000/mario/usuarios_devops_spring
 * branch            test       -> FETCH_HEAD
Already up to date.
...
[Pipeline] { (Create backup)
[Pipeline] sh
+ docker cp spring:/usuarios_devops/app/jar/usuarios_devops.jar ./app/jar/usuarios_devops.jar
+ bash ./backups/_create_backup.sh
Ya existe el respaldo en: /home/spring_workspace/backups/jar-backups/usuarios_devops-201014-194453.jar
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
...
#### nuevo jar copiado a ./app/jar ####
...
[Pipeline] { (Stop Spring)
+ docker exec spring bash -c bash /usuarios_devops/app/_stop_app.sh
detiniendo PID: 972 del deploy anterior
matando PID anterior: 972
pid 972 terminado
...
+ docker cp ./app/jar/usuarios_devops.jar spring:/usuarios_devops/app/jar/usuarios_devops.jar
...
+ docker exec spring bash -c bash /usuarios_devops/app/_start-app.sh
logs limpiados
Start app
Esperando inicio
Iniciado exitoso
start end
...
Finished: SUCCESS
```

5, puedo ver el error en el nevegador y en los logs de test

```r
> docker exec -ti spring bash
> cat /usuarios_devops/app/logs/app.log

2020-10-15 05:52:09.093 ERROR 1024 --- [nio-8080-exec-1] o.a.c.c.C.[.[.[/].[dispatcherServlet] ...

org.postgresql.util.PSQLException: ERROR: column "noexiste" does not exist
```

![24-estado-con-error-de-app-probando-spring-pipelines.PNG](./img/24-estado-con-error-de-app-probando-spring-pipelines.PNG)

6, Ejecutar el pipeline de rollback, y al terminar veremos que funciona
de nuevo nuestra app, ya que se esta ejecutando el jar que estaba funcionando
bien antes, dejo unos logs que me dejo el pipeline.

```r
Running on Jenkins in /var/jenkins_home/workspace/spring-rollback-to-previus-state
Running in /home/spring_workspace
Commit message: "meto error"
+ docker exec spring bash -c bash /usuarios_devops/app/_stop_app.sh
detiniendo PID: 1024 del deploy anterior
matando PID anterior: 1024
pid 1024 terminado
+ bash ./backups/_spring_rollback_to_last_backup.sh
Copiando /home/spring_workspace/backups/jar-backups/usuarios_devops-201014-194453.jar a /home/spring_workspace/backups/../app/jar/usuarios_devops.jar
...
Jar recuperado: /home/spring_workspace/backups/jar-backups/usuarios_devops-201014-194453.jar
+ docker cp ./app/jar/usuarios_devops.jar spring:/usuarios_devops/app/jar/usuarios_devops.jar
logs limpiados
Start app
Esperando inicio
' |____| .__|_| |_|_| |___, | / / / /
=========|_|==============|___/=/_/_/_/
:: Spring Boot :: (v2.3.4.RELEASE)
Iniciado exitoso
start end
Finished: SUCCESS
```

7, hacer correccion en el codigo o quitar error, mas un cambio visual
para que veamos que es un jar diferente

**Resultado:** Comprobamos que nuestros pipelines funcionan correctamente
y el objetivo que hicimos se conforma de varios pasos pero como lo tenemos
automatisado no nos tomo mas que unos cuantos clicks, y tambien que aunque
eliminemos y cremos de nuevo los pipelines estos seguran funcionando
de la misma manera.

## Crear ambiente de calidad: Postgres

### Postgres calidad preparativos

> **Tip**: Suele ocurrir que nos equivocamos en el manejo de diferentes
> ramas, por ejemplo, metemos un cambio en calidad mientras que en realidad
> lo queriamos meter en test, o incluso hacer commit en la rama prouccion
> lo cual podria ser un error grave.  
> **Consejo:** Agregar un githook en nuestro repo local, (luego veremos
> como subirlo al remoto para que todo los equipos tenga el hook) lo que
> hara este hook es preguntar si esta seguro del commit que va a hacer.  
> %.  
> 1, Nos metemos en `\postgres\.git\hooks` y podremos ver diferentes archivos
> de ejemplo del cual nos podemos basar, en este caso creamos.  
> %.  
> 1,1, `\postgres\.git\hooks\pre-commit`

```bash
#!/bin/sh
# CUIDADO: el "#!/bin/sh" en la primera linea es obligatorio, si no causara error
# para mostrarle l usuario en que rama se encuantra actualmente
git branch
# Para mostrarle que cambios les va a hacer commit
git status
# permitimos entrada de texto
exec < /dev/tty
# preguntamos si esta seguro del commit
read -p "Estas seguro del commit (YES/NO): " CONFIRM
if [ "$CONFIRM" != "YES" ]
  then
    echo "cancelado"
    exit 1
fi
echo "continuar"
```

1, En nuestra carpeta de postgres creamos una nueva rama: `git branch Calidad`

2, Hacemos los cambios para crear otro contenedor de postgres pero
este nos servira para calidad

```yml
version: "3.3"

# indicamos que queremos usar una red ya creada
networks:
  usuarios_devops_net:
    external: true

services:
  postgres-qa:
    container_name: postgres-qa
    image: postgres:10
    ports:
      - 5002:5432
    restart: unless-stopped
    environment:
      POSTGRES_PASSWORD: mario_root_qa
      POSTGRES_USER: mario_root_qa
    volumes:
      - ./postgres_qa/data:/var/lib/postgresql/data
# espacio de trabajo en el servidor
      - ./postgres_qa/backups:/home/app_qa/backups
      - ./postgres_qa/to_change:/home/app_qa/to_change
      - ./postgres_qa/current_state:/home/app_qa/current_state
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.52

```

3, Cambiamos la creacion de nuestro base de datos en mi caso agrege un
"_qa" a cada uno

- Nombre del usuario de la aplicacion y contraseña
- Nombre de la base de datos

En `to_change\_install_changes.sh`

```bash
#   usuario root
ROOT_USER="mario_root_qa" # l usuario root que definimos en el contenedor de calidad
ROOT_DB_NAME="postgres"

#   usuario para aplicacion
ADMIN_USER="mario_admin_qa"  # el usuario que vamos a crear
ADMIN_DB_NAME="usuarios_devops_qa" # la bd que crearemos
```

En `current_state\_rollback.sh`, lo mismo mas la contraeña para meter
el rollback

```sh
#   Usuario root
ROOT_USER="mario_root_qa"
ROOT_DB_NAME="postgres"

ADMIN_USER="mario_root_qa"
ADMIN_DB_NAME="usuarios_devops_qa"

if [ "$PASS_TO_CHANGE" == "mario_qa" ]
```

En `to_change\__deploy_changes.jenkinsfile`

- Nos aseguramos de cambiar de rama y de descargar los cambios de dicha rama
- Las carpetas que cambiamos en nuestro contenedor
- El nomnre a cual copiar los archivos

```groovy
stage('Download code with Git') {
  steps {
    sh 'git checkout calidad'
    sh 'git pull origin calidad'
  }
}
stage('Deploy') {
    steps {
      sh 'docker exec postgres-qa bash -c "rm -fr /home/app_qa/to_change/*"'
      sh 'docker cp ./to_change/. postgres-qa:/home/app_qa/to_change/'
      sh 'docker exec postgres-qa bash -c "bash /home/app_qa/to_change/_install_changes.sh"'
    }
  }
```

En `current_state\__rollback_changes.jenkinsfile`, lo mismo no aseguramos
se cambiarnos y deescargar cambios de la rama de calidad, como las carpetas
de trabajo del contenedor, y el nombre del contenedor al cual se van a
copiar.

**IMPORTANTE** en `current_state\00_01_DROP_database_and_user.sql` hay
que cambiar el numbre del usuario y bd a eliminar ya que si los dejamos
igual podriamos eliminar lo incorrecto por accidente, es por eso que cree
un contenedor diferente para que esto no pueda ocurrir, pero hay que tener
**cuidado con este tipo de detalles**.

```sql
DROP DATABASE IF EXISTS usuarios_devops_qa;
DROP USER IF EXISTS mario_admin_qa;
```

En `to_change\00_01_create_if_not_exists_user_and_data_base.sql` cambiamos
el usuario que vamos a crear y la base dedatos

```sql
...
IF EXISTS (SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = 'mario_admin_qa') THEN
...
CREATE USER mario_admin_qa WITH LOGIN NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION
      ENCRYPTED PASSWORD 'mario_admin_qa';
...
  _db TEXT := 'usuarios_devops_qa';
  _user TEXT := 'mario_root_qa';
...
    PERFORM dblink_exec('CREATE DATABASE usuarios_devops_qa OWNER = mario_admin_qa' );
```

En `to_change\00_03_poblate_table_usuarios.sql` solo cambiamos un registo
para tener una referencia visual de que estamos en otro ambiente.

**Recordar que:** Como creamos nuestro scripts pensando que se van a ejecutar
en la maquina donde vamos a aplicar los cambios, estos scrips que queremos
aplicar en calidad igual nos serviran en nuestra maquina local, solo
tendremos que agregar unas pequeñas cosas en nuestr maquina

4, **Comprobar:** Vamos a crear un usuario y bd que coinsidan los los
usuarios root del nuevo contenedor, y asi podremos ejecutar los scripts
de calidad y estos se conectaran sin problemas en nuestra maquina de
desarrollo.

4,1, Ejecutamos en local (en mi caso windows): `bash ./to_change/_install_changes.sh`
Y veremos que se crea una BD y suaurio en nustra maquina que son los
equivalentes a los que usaremos en test.

5, Ejecutamos `bash ./current_state/_rollback.sh mario_qa` en test

**Nota:** en mi caso se me olvido cambiar el nombre de la bd y usuario
a eliminar y me elimino lo de test, es por eso que es importante probar
primero en nuestra maquina primero.

**Resultado:** Modificamos lo necesario para crear nuestro ambiente de
calidad en postgres, como es el contenedor bd, user, intall.sh etc. y
los probamos en nuestra maquina.

### Postgres calidad: Creacion

1, Solo subimos el `docker-compose-postgres.yml` en su version **de calidad**
a nuestro host y lo ejecutamos

2, Creamos el contenedor: `sudo docker-compose -f docker-compose-postgres.yml up -d`

**Resultado:** Debemos de tener dos contenedores de postgres

![28-ambos-contenedores-de-postgres](./img/28-ambos-contenedores-de-postgres.PNG)

#### Job deploy en calidad

1, Hacermos un commit en calidad, para que tengamos el jenkinsfile de
calidad en jenkins

![25-confirmar-commit-por-githook](./img/25-confirmar-commit-por-githook.PNG)

2, subimos a calidad: `git push origin calidad`

3, Creamos un nuevo job `calidad-porstgres-deploy`

![26_configuracion-jenkinsfile-para-deploy-changes-calidad](./img/26_configuracion-jenkinsfile-para-deploy-changes-calidad.PNG)

4, Ejecutamos el job y deberia de ejecutarse correctamente

**Resultado:** Tenemos dos ambientes levantados, test y calidad y creamos
este ambiente reutilizando muchos elementes

![27_conexion-a-postgres-test-y-calidad](./img/27_conexion-a-postgres-test-y-calidad.PNG)

#### Job reollback en calidad

**Error:**: `database * is being accessed by other users, There are X other sessions using the database`.  
**Descripcion:** A la hora de hacer rollback en donde se elimina la
base de datos muestra este error.  
**Causa:** Ahi conexiones habiertas lo mas seguro por la app de Spring.  
**Solucion:** Debemos de cerrar las conexiones desde postgres primero,
lo podemos hacer con el siguiente codigo.  

```sql
-- CERRAMOS TODAS LAS CONEXIONES
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = '<database-name>'
  AND pid <> pg_backend_pid();
```

1, Lo mismo que hicimos para crear el de rollback

![29-job-calidad-rollback](./img/29-job-calidad-rollback.PNG)

## Crear ambiente de calidad: Spring

**Objetivo:** Va a ser muy similar a lo que hicimos en postgres, solo
que tenemos la tarea adicional de agregar variables de conexion,

1, Agregar properties para ambiente calidad: `to_change\code\src\main\resources\application-calidad.properties`
este lo podemos tener en todas las ramas, mientras lo hago en test para
que cuando cree la rama de calidad se traiga este cambio igual.

1,1, Al tener el nuevo archivo hacemos commit en test

```r
# Configuracion para CALIDAD
app.net.ipdatabase=166.10.0.52:5432

# Acceso y configuracion SQL Data Source: usuarios
spring.datasource.url=jdbc:postgresql://${app.net.ipdatabase}/usuarios_devops_qa
spring.datasource.username=mario_admin_qa
spring.datasource.password=mario_admin_qa

spring.pid.file=/usuarios_devops/app/pid.txt
```

2, Creamos rama en la carpeta de spring `git branch calidad` y modificamos
lo necesario para apuntar a calidad que es

- El nombre del contenedor en los jenkinsfile
- En el docker-compose cambioamos puerto, ip, nombre-contenedor, carpetas
  de trabajo,
  - Tambien le cambie el comando a que tail -f null para no estubiera
    guardando los logs de todos los reinicios, eso mas adelante veremos
    nosotros como respaldar logs entre reinicios.
- en `app\_start-app.sh` agrego `ENVIROMENT="calidad"` al final del codicional
  que crea esta variable, ya que se que este script se ejecuta cuando
  estamos en calidad

```groovy
stage('Download code with Git') {
  steps {
    sh """
      git pull origin calidad
      git checkout calidad
    """
  }
}
stage('Create backup') {
  steps {
    sh """
      docker cp spring_qa...
```

3, Pasamos el `docker-compose` al servidor y lo ejecutamos

```yml
version: "3.3"

# indicamos que queremos usar una red ya creada
networks:
  usuarios_devops_net:
    external: true

services:
  spring_qa: # cambio nombre de servicio
    container_name: spring_qa # cambio nombre de contenedor
    image: maven:3.6-openjdk-11
    restart: unless-stopped
    ports:
      - "82:8080" # cambio puerto
    volumes:
      - ./spring_qa/app/:/usuarios_devops/app # cambio carpetas de trabajo
      - ./spring_qa/backups/:/usuarios_devops/backups # ...
      - ./spring_qa/to_change/:/usuarios_devops/to_change # ...
    command: tail -f /dev/null # cambio comando para no depender del app.log
    networks:
      usuarios_devops_net:
        ipv4_address: 166.10.0.82 # cambio IP

```

- Errores al ejecutar el job en un nuevo contenedor, al parecer ahi
  ciertos archivos que son obligatorios para que pueda funcionar el
  deploy por primera vez
  - No encontro `usuarios_devops/app/jar/usuarios_devops.jar` por lo
    que cree uno con echo
  - No encontro `/usuarios_devops/app/_stop_app.sh` asi que agrege en
    el jenkinsfile que copie el que esta en jenkins al contenedor
  - No encontro `/usuarios_devops/app/logs/app.log` asi que, agrego en
    star-app.sh un si no existe app.log crealo

4, Hacemos commit y subimos en calidad, para tener disponibles el
jenkinsfile de calidad en jenkins

5, Creamos Job para hacer deploy en calidad que use la rama de calidad
y ejecutamos

**Resultado:** Tenemos creado un contenedor de spring de calidad y ahora
podemos hacer deploy y enviarle peticiones http y traera la info de
calidad ya que cambiamos en star-app.sh la su ambiente al ejecutarlo,
tambien si nos fijamos este usa un puerto diferente.  
**Tambien:** creamos el job ya que lo usamos para desplegar un jar para
ver si funcionanba bien y no hacerlo manuelmente.  
**Tambien:** Como podemos ver podemos que tenemos dos ambientes diferentes
cada uno de ellos tralledo la B.D. de su respectivo ambiente.

![30-peticion-a-spring-calidad](./img/30-peticion-a-spring-calidad.PNG)

## Crear ambiente de produccion

**Introduccion:** Como Crear un nuevo ambiente sera lo mismo que hicimos
en para crear el ambiente calidad, vamos a aprovechar para simular que
lo creamos desde cero, para crear la documentacion y pulir aun mas los
detalles para que nuestro proyecto sea mas portable y sin tanto rollo,

1. Crear una nueva rama PRODUCCIoN (postgres)
2. Modificar los docker-compose para crear el contenedor
3. Levantar la app en estos contenedores
4. Modificar los pipelines para este entorno
5. Ejecutar los pipelines de deploy y rollback
6. Repetir para spring

### Ambiente produccion: Postgres

1, Crear nueva rama: `git checkout --orphan PRODUCCIoN`

**CUIDADO:** las mayusculas en postgres se portan raro, por ejemplo, para
nombre de usuarios, bd, etc me dio problemas y pase todo a minusculas y
ya no me dio problemas.

- Por ejemplo

```r
# HACER ESTO ME EVITA PROBLEMAS DE MANEJAR MAYUSCULAS YA QUE AL PARECER
# TIENE TRUCO USAR ESTAS
#   el el usuario y pass no uso mayusculas
docker-compose-postgres.yml"
  POSTGRES_PASSWORD: mario_root_prod
  POSTGRES_USER: mario_root_prod
"
#   lo mismo para hacer referencia a estos
to_change\install_changes.sh'
  ROOT_USER="mario_root_prod"
  ROOT_DB_NAME="postgres"
'
to_change\a00_01_create_if_not_exists_user_and_data_base.sql"
  IF EXISTS (SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = 'mario_admin_prod') THEN
"
```

2, Modificamos las variables/nombres/ips/usr/pass en caso todos los archivos

3, Comprobamos que `_install_changes.sh` y `_rollback.sh` funcionen bien
en nuestra maquina

4, Subimos por ftp el `docker-compose` de postgres en su version de produccion

5, hacemos commit `git add & git commit`

6, subimos a repo remoto: `git push origin PRODUCCIoN`

7, Crear job en jenkins usando `to_change\__deploy_changes.jenkinsfile`

8, Deberia de funcionar correctamente nuestro job para deploy

9, lo mismo creamos y ejecutamos para rollback

**Resultado:** Creamos el ambiente de produccion de postgres y sus pipelines
en jenkins y estan funcionando correctamente, y crean respaldos evitando
crear alguno que sea el mismo.

![31-pipelines-postgres-produccion](./img/31-pipelines-postgres-produccion.PNG)

### Ambiente Produccion: Spring

**Objetivo:** Vamos a seguir los pasos que mensionamos en el h2 de este,
el cual es muy parecido

1, Crear nueva rama

2, Modificar variables para que apunten a produccion

3, Ver que funcionen los scripts en nuestra maquina. por ejemplo, yo
temporalmente deje el .properties de prod, para conectarme desde mi
equipo a produccion con spring. **Cuidado:** Esto solo de forma temporal
ya que agregar ruido a estos archivos delicados puede provocar que
cometamos errores mas facilmente.

```conf
#app.net.ipdatabase=166.10.0.50:5432
app.net.ipdatabase=192.168.1.80:5000
```

4, Ya que funcionan los scripts en nuestra maquina, `crear contenedor`
subiendo el docker-compose por FTP

4, `Creamos los jobs` copiando el contenido del jenkinsfile a este y los
ejecutamos para ver que se ejecutan bien

> **Error**: `No such container:path: spring_PROD:/usuarios_devops/app/jar/usuarios_devops.jar`.  
> **Descripccion**: A la hora de ejecutar el job de deploy en el backup falla.  
> **Causa**: No he automatizado la parte de la primera ejecucion del job, en
> el cual debe de existir el jar a respaldar.  
> **Solucion-rapida**: Meternos al contenedor y crear el jar manualmente.  
> **Solucion:** Creamos un script que haga eso

Creamos `to_change\_check_first_exec.sh`

```bash
#!/bin/bash
set -e # para salir si ahi un error

# Este script se ejecutara antes de los scripts que dan problemas al ser la
# promera vez en ejecutarse, ya que necesitande queya ciertos archivos existan
# para que estos funcionen correctamente

if [ -f /usuarios_devops/app/jar/usuarios_devops.jar ]
  then
    echo "Sin problemas para iniciar respaldo"
  else
    mkdir -p /usuarios_devops/app/jar/
    echo "relleno" > /usuarios_devops/app/jar/usuarios_devops.jar
    echo "##################################"
    echo "Se creo carpeta/archivo /usuarios_devops/app/jar/usuarios_devops.jar para tener algo que respaldar"
fi
```

**Resultado:** Ya tenemos funcionanado el deploy y rollback, de spring
y postgres.

## IMPORTANTE: Empaquetar proyecto devops base para futuros proyectos

- [RESUMEN GRAFICO](./usuarios-devops.drawio)

Llegados a este punto tenemos 3 ambientes (test, calidad y produccion)
y en cada uno podemos hacer deploy y rollback tanto en srping como en
postgres, lo cual se consideraria que ya tenemos completada la primera
parte de DevOps.

**Objetivo** Pero antes de continuar con nustro proyecto vamos a hacer
otros ajustes para que esto sea compatible para cualquier otro proyecto
que creemos, solo tomamos como base lo que hemos creado hasta este punto.

**Motivacion:** Por lo que he visto cuando dejamos por un tiempo un
proyecto y lo queremos re-tomar, ni nosotros mismos que creamos el
proyecto sabemos como levantarlo de nuevo, y nos toma tiempo poder
iniciarlo o aun peor iniciamos otro proyecto desde cero, ya que no
queremos averiguar los detalles especificos del proyecto, ahora imaginemos
una persona ajena al proyecto esta va a tardar aun mas en averiguarlo y
si es necesario que esta lo levante, nos pedira ayuda gastando tiempo
que podriamos ahorrar.

### Resultado

Despues de un una re-estructuracion de los scripst y carpetas empaquete
el proyecto para poderlo utilizar en otros proyectos en este comprimido

- [Proyecto](./img/32-CODIGO-DevopsBase.zip)

**IMPORTANTE**: debemos de saber manejar el git-flow que en este caso
descrimos en el ``

#### Como manejar capa postgres

Tenemos diferentes bash scripts que haran el trabajo por nosotros, pero
primero deberemos de hacer unos cambios para poder empezarlo a usar
como es el

A, Pasos para iniciar un nuevo proyecto

1. En `postgres/constants.sh` en get_root_path (`ROOT_PATH`) debemos
   de poner las rutas donde se encontara nuestro proyecto, para empezar
   en nuestro equipo de desarrollo, para no mover mas rutas solo movemos
   esta ultima.

2. En `postgres/constants.sh` en `ROOT_USER` y mas abajo tenemos los
   diferentes datos de conexion a la bd ya que en **trabajo nuestro**
   crear un usuario root en nuestra maquina, del lado de los contenedores
   este usuario root lo creara el docker-compose

3. En `postgres/1_building/0.0.0` estan los scrips para crear nuestro
   usuario y base de datos inicial, por lo que cambiamos aqui podemos
   personalizar el nombre de nuestro usuario que usara spring y el nombre
   de la base dedatos

B, Pasos para desarrollar

1. **Crear codigo:** Debemos de seguir una nomenclatura que podremos ver
   el el archivo `postgres/README.md` titulo Nomenclatura, que dice como
   crear nuestros scripts sql para que se ejecuten en cierto orden y por
   el usuario admin o por el usuario root

2. **Ejecutar codigo rapido** Ya que tengamos los cambios en nuestros
   sql scripts hacemos uso de `postgres/1_building/_test_version.sh`
   para ejecutar nuestros script para hacer un change (deploy) o un
   undo (soft_rollback).

3. **Ejecutar codigo como se fuera un deploy** Por fines practicos hacemos
   la ejecucion de forma diferente para probarlo y para hacer un deploy,
   para probarlo no hacemos movimiento de archivos, y para hacer deploy
   como si estubieramos en un ambiente pre-productivo o productivo, lo
   dividimos en 2 pasos `postgres/1_building/_create_pkg.sh` el cual juntara
   nuestro codigo en un paquete y luego lo pasara a la carpeta `to_install`
   para luego ejecutar `postgres/2_to_install/_deploy.sh` crear un respaldo,
   ejecutar los cambios y mover el paquet a running.

C, Pasos para crear un ambiente pre-productivo o productivo

1. Modificar los archivos necesarios
   1. Modificar el docker-compose
   2. MOdificar el constants.sh
   3. modificaar los jenkinsfile
   4. modificar los sql
2. Crear conetnedor
   1. Subir por ftp solamente el docker-compose
   2. crear contenedor
3. Agregar carpeta de trabajo en jenkins
   1. Agregar vol en docker-compose de jenkins
   2. reconstruir
   3. Agregar repositorio en la carpeta que se agrego
   4. Crear jobs con los jenkinsfiles
4. listo

D, Pasos para manejar pre-productivo o productivo

1. Tenemos diferentes archivos `jenkinsfile` los cuales podremos agregar
   a jenkins y ejecutarlos desde este.

2. **Deploy:** Para esto usamos `postgres/2_to_install/__postgres_deploy.jenkinsfile`
   el cual podemos agregar el texto o el archivo desde git, el cual al
   ejecutar descargara los cambios con git, creara el paquete, lo ejecutara
   y lo pasara a running.

3. Lo mismo para Rollback, Rollback-Rollback, recover etc.

#### Como manegar la capa Spring

PENDIENTE

## Spring: CRUD usuarios

Fuente: [mkyong.com](https://mkyong.com/spring-boot/spring-boot-jdbc-examples/)

### Agregar roles

1, UI Componente de roles
2, frontend with dummy Roles
3, unit test DAO, Service, Controller
