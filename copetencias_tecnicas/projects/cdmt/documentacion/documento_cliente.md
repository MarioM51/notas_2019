# Documento cliente

- [Documento cliente](#documento-cliente)
  - [Descripcion](#descripcion)
  - [Compromisos minimos](#compromisos-minimos)
    - [Por parte del equipo](#por-parte-del-equipo)
    - [Por parte del cliente](#por-parte-del-cliente)
  - [Costos](#costos)
    - [Presupuesto](#presupuesto)
    - [Disclaimer](#disclaimer)
  - [Detalles del Producto](#detalles-del-producto)
    - [Casos de uso](#casos-de-uso)
    - [Historias de usuario](#historias-de-usuario)
    - [Prototipos](#prototipos)

## Descripcion

Tienda online la cual manejara productos y publicaciones, el primer paso
no contendra interaccion con el usuario, para el blog se comunicara al
equipo de desarrollo para cualquier actualizacion en el contenido, mientras
que el ultimo paso es que tenga los controles necesarios, para que el
cliente pueda controlar la tienda por su cuenta.

## Compromisos minimos

### Por parte del equipo

- Se mostraran los productos mas informacion de contacto tradicionales
  necesaria, para realizar una venta.
- Se mostraran las publicaciones, para poder entregar un mensaje al los
- La informacion obtenida por nuestro equipo, podra ser reutilizable
  por otros equipos.

### Por parte del cliente

- Expresar la lista de codad que quiere del poyecto en un documento con
  una estructura propuesta por el equipo.
- Proporcionar la informacion que quiere que se publique en la plataforma.
- Dar idea y aprobar el prototipo de producto, concentrandoce en su
  funcionalidad y apariencia.

## Costos

### Presupuesto

No se cobra por producto, si no por tiempo la cual consta de 5,000 la
quinsena.

### Disclaimer

El primer entregable cubrira las necesidades minimas, para que el cliente
saque un minimo provecho del entregable, el cual este minimo se deninira
por medio de una lista de caracteristicas de la plataforma, y cada una
de estas caracteristicas estara acompañada pos sus criterios de aceptacion,
los cuales se enumeraran con el concentimeito de ambas partes.

Para que la transaccion sea un riesgo en ambas partes, el equipo cobrara
un 50% menos la primer quinsena, pero en tanto el cliente quiera continuar
con el segundo entregable, se cobrara la cantidad completa.

Ejemplo: El cliente y el equipo negocian que el primer entregable, que debe
servir minimo, para mostrar un catalogo online, el cual se cobraria $10,000
pero por ser el primer entregable, el equipo cobrara solamente $5,000,
y al entregar el produto el cliente no esta satisfecho, entonces tanto
el cliente como el equipo pierden $5,000, pero si el cliente decide
continuar debera de pagar los $5,000 restantes para continuar el proyecto,
y el segundo y demas enregables se le cobrara $10,000.

## Detalles del Producto

### Casos de uso

Podemos ver los casos de uso [aqui](./casos_de_uso_happy_path.drawio).

### Historias de usuario

Podemos ver la lista completa [aqui](./historias_de_usuario.xlsx).

### Prototipos

Podemos ver los siguientes prototipos.

- El Protoripo de bajo nivel lo podemos ver [aqui](./prototipo_bajo_nivel.drawio)
