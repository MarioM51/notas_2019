# Notas del proyecto

Notas particulares del proyecto, y enlaces casos que salieron y se
agregaron a los diferentes temas.

## A. Introduccion

algo

<!-- <div style="break-after:page"></div> -->

### A.1. Resultados

### A.2. Accesos rapidos

## Sistemas

### Crear servidor de gitea

A, Subimos los archivos para crear el servicio

## Bases de Datos

### Crear Estrategia de Respaldos

Fuente:

- [stackoverflow](https://stackoverflow.com/questions/29600369/starting-and-populating-a-postgres-container-in-docker)

1, Crear comando para crear respaldo .sql

- Nota: podeos agregar `--format custom` el cual es mas adecuado para
  respaldos por que esta comprimido.

```r
docker exec cdmt_postgres_prod pg_dump -U mario_prod cdmt_prod > "backup.sql"
```

2, Crear script para crear el respaldo

3, Crear script para recuperar respaldo (levantar contenedor postgres a partir de .sql)


```r
docker run --name pg_prueba --rm \
  -v //my_scripts:/docker-entrypoint-initdb.d \
  postgres
```

4, Crear jenkinsfile para crear respaldo

5, Crear jenkinsfile para recuperar respaldo

6, Crear 

## 1. Gatsby: front-office

## 2. Flutter: back-office AppMobil

## 3. Spring: Backend
