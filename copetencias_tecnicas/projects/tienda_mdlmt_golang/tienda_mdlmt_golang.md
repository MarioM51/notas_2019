# Tienda MDLM, vainillaJS

- [Tienda MDLM, vainillaJS](#tienda-mdlm-vainillajs)
  - [Analisis](#analisis)
    - [Contrato](#contrato)
    - [Objetivo](#objetivo)
    - [Requerimientos tecnicos](#requerimientos-tecnicos)
    - [Requerimientos de negocio](#requerimientos-de-negocio)
  - [Diseño](#diseño)
    - [Historias de usuario](#historias-de-usuario)
  - [Desarrollo Spring 1](#desarrollo-spring-1)
    - [Planeacion](#planeacion)
    - [Svelte](#svelte)

## Analisis

### Contrato

Se seguiran las siguientes pautas trabajar en conjunto

[1] Se manejaran pagos y entregas quinsenales, con un costo de XXX.

[2] El tiempo para definir que es lo que quiere el cliente, si acepta
el fabricante la primer hora sera gratis, en caso de requerir mas, la
hora de definicion/analisis tendra un costo de XXX.

[2] Para iniciar el desarrollo se tendra que pagar la mitad de la quinsena,
como pago de servicios de terceros que se hayan acordado.

[3] Primero se se mostraran las caracteristicas y luego se pagara la
segunda quinsena. En caso de que el cliente no este satisfecho con la
entrega quinsenal, podra negar el pago de la quinsena, pero no podra
conservar las caracteristicas rechazadas.

### Objetivo

Poner un negocio pequeño disponible en la nube, para darle visibilidad
a productos, publicaciones e informacion basica, se podran hacer pedidos
no compras.

El cliente podra realizar pedidos, el constara de ver los productos,
seleccionarlos, y realizar el pedido, finalizando con un mensaje que
el fabricante lo contactara para seguir con el pedido, ya sea indicando
por que no acepto el pedido, o los pasos siguientes para que le continuar
el pedido.

Del lado del fabricante en tanto se haga un pedido este resivira un email,
para notificarle que le llego un pedido, mostrandole los detalles del
pedido.

### Requerimientos tecnicos

El proyecto debera de tener costos minimos y configuraciones minimas.

- Costos minimos: De preferencia un costo menor a $100 mensuales, y la
  compra de un dominio menos de $200

- Configuraciones minimas: La complejidad del proyecto debe ser baja,
  para poderlo ejecutar en cualquier maquina y con cualquier estado,
  por ejemplo, para iniciar el proyecto constara de un comando y opcionalmente
  un respaldo de base de datos.

### Requerimientos de negocio

La plataforma debera solucionar los siguientes problemas.

- Mostrar un catalogo online.
- Mostrar informacion de la empresa.
- Poder realizar compra de productos.
- Panel de administracion para
  - Productos
  - Pedidos
  - Usuarios

Las caracteristicas tienen una complejidad relativa por lo que se iran
entregando de forma progresiva.

## Diseño

### Historias de usuario

[Historias de usuario](./doc/1_backlog.csv)

## Desarrollo Spring 1

### Planeacion

[Historias de usuario](./doc/1_sprint.csv)

### Svelte

- [Fuente](https://www.youtube.com/watch?v=zH2qG9YwN3s)

Crear proyecto: `npm init svelte@next mdlmt`

Agregar tailwind: `npx svelte-add@latest tailwindcss`

Instalar daisyUI y tipografias: `npm i -D daisyui @tailwindcss/typography`

agregamos daisyUI a tailwind [fuente](https://daisyui.com/core/typography)

```js
plugins: [
  require('@tailwindcss/typography'),
  require('daisyui'),
]
```

Iniciar proyecto: `npm run dev -- --open`

Podmeos cambiar el tema en `src\app.html`

```html
<html lang="es" data-theme="synthwave">
  ...
```
