# GoLang

- [GoLang](#golang)
  - [Accesos rapidos](#accesos-rapidos)
    - [Pendientes y Notas](#pendientes-y-notas)
    - [Proyectos Gifs y codigos](#proyectos-gifs-y-codigos)
    - [Reglas](#reglas)
  - [%%%%%%%%%%% INTRODUCCION %%%%%%%%%%%](#-introduccion-)
    - [Basicos](#basicos)
    - [Structs](#structs)
    - [Funciones](#funciones)
      - [Defer](#defer)
      - [argumentos y retornos](#argumentos-y-retornos)
      - [funciones y tipos](#funciones-y-tipos)
    - [Punteros](#punteros)
    - [Paquetes y entorno](#paquetes-y-entorno)
    - [Concurrencia](#concurrencia)
    - [Channels](#channels)
    - [Manejo de errores](#manejo-de-errores)
      - [Manejar errores de terceros](#manejar-errores-de-terceros)
      - [Errores propios y recover](#errores-propios-y-recover)
      - [Uso de Logs](#uso-de-logs)
    - [Escribiendo Documentación](#escribiendo-documentación)
    - [Testing \& Benchmarking](#testing--benchmarking)
      - [Introducion](#introducion)
      - [Tabla de Tests](#tabla-de-tests)
      - [Tests y documentacion](#tests-y-documentacion)
      - [Inspectores de codigo Golint](#inspectores-de-codigo-golint)
      - [Benchmark](#benchmark)
      - [Coverage](#coverage)
      - [RESUMEN: Tests y benchmarking](#resumen-tests-y-benchmarking)
    - [Estructura de datos y Ordenamiento](#estructura-de-datos-y-ordenamiento)
      - [Notacion BigO](#notacion-bigo)
      - [Algoritmo de insercion - Insertion sort](#algoritmo-de-insercion---insertion-sort)
      - [Selection Sort](#selection-sort)
      - [Bubble Sort](#bubble-sort)
  - [%%%%%%%%%%% WEB BASICO %%%%%%%%%%%](#-web-basico-)
    - [Basicos Web](#basicos-web)
      - [Crear servidor web estatico con 2 lineas](#crear-servidor-web-estatico-con-2-lineas)
      - [Ejemplo: crear servidor web y hacerle peticion programaticamente](#ejemplo-crear-servidor-web-y-hacerle-peticion-programaticamente)
      - [23. Usar Fresh para recargar el servidor](#23-usar-fresh-para-recargar-el-servidor)
      - [24. Soporte a templates HTML](#24-soporte-a-templates-html)
      - [25. Usar Variables, condicionales, bucles e includes(header/footer) en el Template](#25-usar-variables-condicionales-bucles-e-includesheaderfooter-en-el-template)
        - [25. Ankis](#25-ankis)
        - [25. Practico](#25-practico)
    - [Proyecto basico](#proyecto-basico)
      - [Servir archivos estaticos](#servir-archivos-estaticos)
      - [Estructurar por templates](#estructurar-por-templates)
      - [Agregar Js y Css a templates desde archivos externos](#agregar-js-y-css-a-templates-desde-archivos-externos)
      - [CODIGO Proyecto basico](#codigo-proyecto-basico)
    - [Sockets](#sockets)
      - [Codigo Sockets, Chat simple](#codigo-sockets-chat-simple)
      - [Ankis Sockets](#ankis-sockets)
    - [Aplicacion CRUD templates, Conexion a BD, http methods, mux](#aplicacion-crud-templates-conexion-a-bd-http-methods-mux)
      - [40. Crear funcion PL-SQL y llamarla](#40-crear-funcion-pl-sql-y-llamarla)
      - [41. Manejar Peticiones Post](#41-manejar-peticiones-post)
      - [Intercambiar info por medio de hidden inputs](#intercambiar-info-por-medio-de-hidden-inputs)
      - [Ejecuatar SQL (Insert, Update, Delete)](#ejecuatar-sql-insert-update-delete)
      - [Uso y Ventajas de Gorilla Mux](#uso-y-ventajas-de-gorilla-mux)
      - [CODIGO: Aplicacion CRUD templates](#codigo-aplicacion-crud-templates)
    - [Aplicacion CRUD JSON: Auth-Cookies, Sockets](#aplicacion-crud-json-auth-cookies-sockets)
  - [%%%%%%%%%%% WEB INTERMEDIO %%%%%%%%%%%](#-web-intermedio-)
    - [Crear Terminal Virtual](#crear-terminal-virtual)
      - [Iniciando proyecto](#iniciando-proyecto)
      - [Creando render de templates con cache](#creando-render-de-templates-con-cache)
      - [Stripe: Crear formulario con input de targeta generado dinamicamente por Stripe](#stripe-crear-formulario-con-input-de-targeta-generado-dinamicamente-por-stripe)
      - [Stripe: Crear Intent de pago](#stripe-crear-intent-de-pago)
      - [Stripe: Realizar el cargo a la targeta](#stripe-realizar-el-cargo-a-la-targeta)
      - [Strapi: Mostrar pantalla cobro exitoso y/o factura](#strapi-mostrar-pantalla-cobro-exitoso-yo-factura)
      - [Strapi: Evitar un segundo cobro al recargar la pagina](#strapi-evitar-un-segundo-cobro-al-recargar-la-pagina)
      - [Strapi: Limpiando detalles](#strapi-limpiando-detalles)
    - [Crear compra de producto](#crear-compra-de-producto)
      - [Crear BD](#crear-bd)
      - [crear servidor estatico y Crear pagina producto](#crear-servidor-estatico-y-crear-pagina-producto)
      - [Mostrar Struct Producto en template](#mostrar-struct-producto-en-template)
      - [Consultar producto en BD y mostrarlo en api-json](#consultar-producto-en-bd-y-mostrarlo-en-api-json)
      - [Migraciones de BD con Soda/Pop (mysql o postgres)](#migraciones-de-bd-con-sodapop-mysql-o-postgres)
      - [Crear modelos para bd y uso soda para hacer cambios en el DB schema](#crear-modelos-para-bd-y-uso-soda-para-hacer-cambios-en-el-db-schema)
      - [Producto de BD a Template](#producto-de-bd-a-template)
      - [Insertar XXX a DB](#insertar-xxx-a-db)
      - [Strapi: conseguir 4 ultimos, expiracion, y codeResultBanck](#strapi-conseguir-4-ultimos-expiracion-y-coderesultbanck)
      - [Guardar cliente preparativos](#guardar-cliente-preparativos)
      - [Guardar cliente, orden y transaccion en BD](#guardar-cliente-orden-y-transaccion-en-bd)
    - [Crear Subscripcion mensual](#crear-subscripcion-mensual)
      - [Resultado creacion de Suscripcion](#resultado-creacion-de-suscripcion)
    - [Autenticacion](#autenticacion)
      - [Creando cabledo para el login](#creando-cabledo-para-el-login)
      - [Auth de usuario](#auth-de-usuario)
      - [Crear token](#crear-token)
      - [Guardar el token en la BD y el localStorage](#guardar-el-token-en-la-bd-y-el-localstorage)
      - [Mostrar/Ocultar si el usuario esta auth o no](#mostrarocultar-si-el-usuario-esta-auth-o-no)
      - [Ver si el usuario esta auth. con AJAX](#ver-si-el-usuario-esta-auth-con-ajax)
      - [Validar token con su hash en la BD](#validar-token-con-su-hash-en-la-bd)
      - [Validar caducidad del token en el backend](#validar-caducidad-del-token-en-el-backend)
      - [Validar token en el middelware del backend](#validar-token-en-el-middelware-del-backend)
    - [Cambiar clave o email](#cambiar-clave-o-email)
      - [Crear cableado para olvide clave y comprobar si existe el email](#crear-cableado-para-olvide-clave-y-comprobar-si-existe-el-email)
      - [Implementar envio de email](#implementar-envio-de-email)
      - [Crear url firmada y crear implementacion de validacion y caducidad](#crear-url-firmada-y-crear-implementacion-de-validacion-y-caducidad)
      - [Encriptacion y desencriptacion](#encriptacion-y-desencriptacion)
      - [Validar la url y cambiar la contraseña](#validar-la-url-y-cambiar-la-contraseña)
    - [Micro-sercicio Rest](#micro-sercicio-rest)
      - [Lamar al microservicio](#lamar-al-microservicio)
    - [Validar del lado del servidor](#validar-del-lado-del-servidor)
  - [%%%%%%%%%%% Tienda MDLM, vainillaJS, ta %%%%%%%%%%%](#-tienda-mdlm-vainillajs-ta-)
  - [Api Usuarios](#api-usuarios)
    - [Cableado web con Gin](#cableado-web-con-gin)
      - [Introduccion: Peticion get para retornar coleccion de structs](#introduccion-peticion-get-para-retornar-coleccion-de-structs)
      - [Peticiones Post](#peticiones-post)
      - [Path parameters con Gin](#path-parameters-con-gin)
  - [Por el camino](#por-el-camino)
    - [Gin](#gin)
      - [Uso de templates con Gin](#uso-de-templates-con-gin)
      - [Subir archivos](#subir-archivos)
      - [Convertir File Multipart a base64](#convertir-file-multipart-a-base64)
      - [Mostrar y/o descargar archivo guardado como base64](#mostrar-yo-descargar-archivo-guardado-como-base64)
      - [Agregar gzip a gin](#agregar-gzip-a-gin)
    - [html/template](#htmltemplate)
      - [Agregar json+ld](#agregar-jsonld)
    - [Problemas con el IDE VsCode](#problemas-con-el-ide-vscode)
      - [Al usar wire no no inspecciona el codigo](#al-usar-wire-no-no-inspecciona-el-codigo)
    - [Wire](#wire)
      - [Introduccion a Wire](#introduccion-a-wire)
      - [Uso de Wire con interfaces](#uso-de-wire-con-interfaces)
    - [GORM](#gorm)
      - [Instalar GORM](#instalar-gorm)
    - [Uso soda](#uso-soda)
      - [Iniciar base de datos](#iniciar-base-de-datos)
      - [Crear tabla simple](#crear-tabla-simple)
    - [Ver servidor desde local network](#ver-servidor-desde-local-network)
  - [%%%%%%%%%%% 6. MODULES %%%%%%%%%%%](#-6-modules-)
    - [6.1. Introduccion](#61-introduccion)

## Accesos rapidos

- Documentacion Oficial: `golang.org`
- Documentacion de GoLang con paquetes de terceros: `godoc.org`
- Documentacion de una hoja: `golang.org/doc/effective_go`
- Libros/Bootcamp: `www.golang-book.com`
- Varios Ejemplos: `gobyexample.com`
- Obtener solo las targetas Anki `sed -n '/```QUITARr/,/```/p' golang.md > golang.ankis`

- Paquete para
  - Generar hashs [bcrypt](https://golang.org/x/crypto/bcrypt)
  - Driver de postgres [pq](https://github.com/lib/pq)
  - Enrrutador [chi](https://github.com/go-chi/chi)
  - SDK en Golang de [Stripe](https://github.com/stripe/stripe-go)
  - Uso de cookies [scs](https://github.com/alexedwards/scs)
  - Configurar Cors [go-chi/cors](https://github.com/go-chi/cors)
  - Enviar emails [simple-mail](https://github.com/xhit/go-simple-mail)
  - Firmar mensajes [go-alone](https://github.com/bwmarrin/go-alone)
  - Crear pdf [gofpdf](https://github.com/phpdave11/gofpdf)
  - ORM [GORM](https://gorm.io/docs/index.html)
  - GZip [Gzip](https://github.com/nanmu42/gzip)

### Pendientes y Notas

- Notas
  - Cuidado las targetas ankis se remplaso el " por ´, para que no
    causara conflictos

- Penddientes
  - EntornoDeDesarr.Paquetes
  - Canales.Fan-In
  - Canales.Fan-Out
  - EscribiendoDocu.EscribiendoDocu+Practicos

### Proyectos Gifs y codigos

- Uso de condi, loop, includes Simple: [.zip](./img/_1_usar_variables_condi_loops_includes.zip)
- Chat simple: [.zip](./img/_3_chat_simple.zip), [.gif](./img/_3_chat_simple.gif)
- Terminal Stripe simple [.zip](./img/_5_01_go_stripe.zip), [.gif](./img/_5_conexion_con_strapi_usr_introduce_el_precio.gif)
- Go-Stripe Terminal, Producto y Suscripcion [.zip](./img/_6_cobros_implementados.zip)
- Go-Stripe Autenticacion con Stateful token [.zip](./img/_7_Authenticacion.zip) [.gif](./img/20_reDireccion_por_no_autorizado.gif)
- Cambio de contraseña via email [.zip](./img/_8_resetear_contrasenia.zip) [.gif](./img/_8_resetear_contrasenia.gif)
- Micro servicio [_9_source-code](./img/_9_source-code.zip)

### Reglas

- Para buscar/etiquetar algo empezar con
  - uso de X
  - obtener X
  - ver X
  - Crear X
  - que es X
  - Guardar X
  - Como [verbo] [objeto] ej. como validar email, como convertir pdf a

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## %%%%%%%%%%% INTRODUCCION %%%%%%%%%%%

Fuente: [Udemy](TODO) http://aa.com/PENDIENTE (PONER CURSO DE UDEMY)

### Basicos

Targetas Ankis

```r
Que es una declaracion corta;"
Sin necesidad de declarar el tipo
<pre>
nombre := ´Mario´;
</pre>"

Para que sirve *var*;"
//Recomendable para declarar variables a nivel de paquete, tambien
se puede declarar dentro de funciones.

<pre>
 var edad = 29;
</pre>"

Como declarar un string de varias lineas;"
Con uso de comillas invertidas

<pre>
var descripcion = 
`  Aqui puedo usar
  tantas lineas como
  yo
  quiera`;
</pre>
"

Valores por defecto de variables;"
<pre class='prefor'>
- para enteros y floats sera 0 o 0.0
- Para booleanos sera falso
- Para strings sera ´´
- Para Pointers, funciones, interfaces, slices, channels, maps sera nil=null
</pre>
"

Como imprimir varias variables en consolas;"
en auto les agrega un espacio vacio

<pre>
fmt.Println(nombre, apellidos);
// %s para string y %v para que el compilador lo infiera
fmt.Printf(`Nombre %s, isActive: %v Edad: %v`, nombre, isActive, edad);
</pre>
"

Como imprimir el tipo de variable;"
Con el verbo %T en Printf

<pre>
activo := true;
fmt.Printf(`variable activo es de tipo %T`, activo);
</pre>
"

Convertir cadena a bytes;"
Con uso de *[]byte(cadena)*

<pre>
var cadena string = ´HOLA GOLANG´;
binario := []byte(cadena);
fmt.Println(binario);
//RESPONSE - COMPARAR CON TALBA ASCII
[72 79 76 65 32 71 79 76 65 78 71] 
</pre>"

## string a int, string to int, string a entero, cadena a numero
Convertir cadena a entero;"
strconv.Atoi(miString)
"

## contiene string, si string tiene un substring, 
Como ver si string contiene una palabra, ej. buscar `el` en `callo el perro` ;"
strings.Contains(`GeeksforGeeks`, `for`)
"

Como iterar un String en GoLang;"
El String funciona como un arreglo

<pre>

for i:=0; i<len(cadena); i++ {
  fmt.Printf(´El hex de %q es: %x \n´, cadena[i], cadena[i]);
}

</pre>"

Como se declara una constante;"
Con la palabra reservada const

<pre>
const CLAVE = ´incambiable´;
</pre>"

Que es Bit Shifting;"
Cuando desplazamos binarios a la izq o der

<pre>
var b1 = 2;       fmt.Printf(´%d\t%b\n´, b1, b1); // 2  10
var b2 = b1 << 1; fmt.Printf(´%d\t%b\n´, b2, b2); // 4  100
var b4 = 64;
var b5 = b4 >> 1; fmt.Printf(´%d\t%b\n´, b5, b5); //32  100000
</pre>"

Sintaxis de un condicional;"
Igual que java pero sin parentesis, solo con llaves

<pre>
if true {
  fmt.Println(´8 is divisible by 4´)
}
if num := 9; num < 0 {
  fmt.Println(num, ´is negative´)
} else if num < 10 {
  fmt.Println(num, ´has 1 digit´)
} else {
  fmt.Println(num, ´has multiple digits´)
}
</pre>"

Sintaxis de switch y para que sirve el *fallthrough*;"
Igual que java, sin parentesis.

<pre>
i := 23
switch i {
  case 1: fmt.Println(´one´)
  case 2, 22: fmt.Println(´two´); fallthrough;
  case 3: fmt.Println(´two plus one´)
  case 4: fmt.Println(´four´)
  default: fmt.Println(´default´)
}
</pre>"

Como se declara un array vacio e cinco posiciones;"
var arrA [5]int"

Como se declara un array con valores;"
var arrB = [3]string{´Perro´,´Gato´,´Jamster´};
"

Cual es la diferencia entre un slice y un array;"
Un array tiene un tamaño definido y un slice uno dinamico

<pre>
difArr   := [3]int{1,2,3}; //ERROR-> difArr   = append(difArr  , 3);
difSlice := [ ]int{1,2,3}; difSlice = append(difSlice , 3);
</pre>"

Como se declara un array con valores e iterarlo como foreach;"
Con *for index, item := range items*
<pre>
  var mascotas = []string{´Perro´,´Gato´,´Jamster´};
  for i, mascota := range mascotas { // ITERAR ARREGLO
    fmt.Printf(´Mascota %v %v: \n´, i, mascota);
  }
</pre>"

## Agregar items a arreglo
Como agregar elementos al final de un arreglo;"
agregado = append(original, x, y, z);"

Como concatenar dos arreglos;"
Con *append(arrA, arrB...)*

<pre>
var arrC = []int{1,2,3}; var arrD = []int{4,5,6};
arrC = append(arrC, arrD...); 
//[1 2 3 4 5 6]
</pre>"

Como Eliminar items de arreglo;"
Cortando las partes que no queremos del arreglo y si esta en medio
concatenar el mismo.

<pre>
var todas = []string{´a´,´b´,´c´,´d´,´e´,´F´,´G´,´H´,´i´};
fmt.Printf(´Todas:\t\t%v\n´, todas);
var sinMayus = append(todas[0:5], todas[8:]...);
fmt.Printf(´Sin Mayus:\t%v\n´, sinMayus);
//Todas:     [a b c d e F G H i]
//Sin Mayus: [a b c d e i]
</pre>"

Como declarar una matriz de 2 dimensiones;"
<pre>
var matriz = [][]float32{
  {1.1, 1.2, 1.3},
  {2.1, 2.2, 2.3},
  {3.1, 3.2, 3.3},
};
</pre>"

Cual es el equivalente de char en java en Go;"
El rune
const letra rune = `H`; //CAMBIAR POR COMILLA SIMPLE
"

## crear mapa, saber si existe un key, comprobar si existe una ker en un mapa,
## checar si existe llave de un mapa
Como declarar un mapa y ver si existe un key;"
var mapa = map[KEY_TYPE]VALUE_TYPE{KEY_VALUE: VALUE, ...};

<pre>
var mapa = map[rune]string{´o´: ´Mario´, ´i´: ´Moi´, ´l´: ´Pel´, ´c´: ´Seco´, ´e´: ´Dake´, }
fmt.Println(mapa[´o´]); // Mario
el9, ok := mapa[´D´];
if !ok { /*Cae Aqui*/ fmt.Println(´La key -D- no existe´);  } else { fmt.Println(el9); }
</pre>"

Como iterar un mapa;"
Con *for llave, valor := range mapa {*

<pre>
var mapa = map[rune]string{´o´: ´Mario´, ´i´: ´Moi´, ´l´: ´Pel´, ´c´: ´Seco´, ´e´: ´Dake´, }
for llave, valor := range mapa {
  fmt.Print(llave, ´:´, valor, ´, ´);
}
</pre>"

## agregar item a mapa
## eliminar item a mapa
Como agregar y eliminar un item de un mapa;"
Con *delete(mapa, ´c´);*

<pre>

var mapa = map[rune]string{´o´: ´Mario´, ´i´: ´Moi´, ´l´: ´Pel´, ´c´: ´Seco´, ´e´: ´Dake´, }
// COMPROVAMOS SI EXISTE Y ELIMINANOS
if item, ok2 := mapa[´c´]; ok2 {
  delete(mapa, ´c´);
  fmt.Println(item, ´ fue eliminado´);
}
// AGREGAMOS
mapa[´x´] = ´Seco´
fmt.Println(´Seco fue agregado´, mapa);

</pre>"

Practica, Como crear un mapa de arreglos;"
Con *delete(mapa, ´c´);*

<pre>

  mapaArray := map[string][2]string{
    ´Frasco´: {´agua´, ´Tamarindo´},
    ´Botella´: {´Cerbeza´, ´Negra´},
    ´Caja´: {´Leche´, ´Natural´},
  }
  for k, contenedores := range mapaArray {
   fmt.Println(´Contenedor:´, k);
   for i, contenedor := range contenedores {
       if i == 0 {
         fmt.Print(´\t´, ´Contenido ´, contenedor);
       } else {
         fmt.Println(´ Tipo´, contenedor);
       }
    }
  }
/* SALIDA
Contenedor: Frasco
  Contenido agua Tipo Tamarindo
Contenedor: Botella
  Contenido Cerbeza Tipo Negra
Contenedor: Caja
  Contenido Leche Tipo Natural
*/
</pre>"
```

Codigo de ejemplo

```go
package main
import ( ´fmt´ )

//var recomendable para variables globales/nivel de paquete, la comilla
//simple sirve para declarar raw strings
var descripcion = 
`  Aqui puedo usar
  tantas lineas como
  yo
  quiera`;

func main() {
//SCOPE
  fmt.Println(descripcion);

//valores por defecto
  // - para enteros y floats sera 0 o 0.0
  // - Para booleanos sera falso
  // - Para strings sera ´´
  // - Para Pointers, funciones, interfaces, slices, channels, maps sera nil=null

//DECLARACION CORTA
  localidad := `tlaxco`;
  localidad = `Tlaxco2`;

//USO DE VERBOS EN PRINTF
  var edad int = 29;
  fmt.Printf(´Edad %v, Localidad: %v \n´, edad, localidad);

//IMPRIMIR TIPO DE VARIABLE
  activo := true;
  fmt.Printf(´variable activo es de tipo %T\n´, activo);

//CONVERTIR STRING A BYTES USANDO EL ESQUEMA DE CODIGO ASCII
  var cadena string = ´HOLA GOLANG´;
  binario := []byte(cadena);
  fmt.Println(binario);

//ITERAR STRING
  for i:=0; i<len(cadena); i++ {
    fmt.Printf(´El hex de %q es: %x \n´, cadena[i], cadena[i]);
  }

//CONSTANTES
  const CLAVE = ´incambiable´;
  fmt.Println(´Constante: ´, CLAVE);

//BIT SHIFTING
  var b1 = 2;       fmt.Printf(´%d\t%b\n´, b1, b1); // 2  10
  var b2 = b1 << 1; fmt.Printf(´%d\t%b\n´, b2, b2); // 4  100
  var b3 = b2 << 1; fmt.Printf(´%d\t%b\n´, b3, b3); // 8  1000
  var b4 = 64;
  var b5 = b4 >> 1; fmt.Printf(´%d\t%b\n´, b5, b5); // 32 100000

//SWITHCH
  i := 23
  switch i {
    case 1: fmt.Println(´one´)
    case 2, 22: fmt.Println(´two´); fallthrough;
    case 3: fmt.Println(´two plus one´)
    case 4: fmt.Println(´four´)
    default: fmt.Println(´default´)
  }

//PRACTICA
  var birthYear = 1992;
  for {
    fmt.Print(birthYear, ´, ´);
    if birthYear >= 2021 { break; }
    birthYear++;
  }
  fmt.Println();

//ARREGLOS VACIOS
 var arrA [5]int; arrA[0] = 5; arrA[4] = 10;
 fmt.Println(´Arreglo vacio: ´, arrA);

//ARREGLOS DECLARADOS
  var mascotas = []string{´Perro´,´Gato´,´Jamster´};
  fmt.Println(´Arreglo declarado: ´, mascotas);

// ITERAR ARREGLO
  for index, mascota := range mascotas {
    fmt.Printf(´Mascota %v %v: \n´, index, mascota);
  }

// AGREGAR ELEMENTO A ARREGLO
  mascotas = append(mascotas, ´Borrego´, ´Serpiente´, ´Chiquina´);
  fmt.Println(´Agregado 3 ultimos:´, mascotas);

// CONCATENAR DOS ARREGLOS
  var arrC = []int{1,2,3}; var arrD = []int{4,5,6};
  arrC = append(arrC, arrD...); fmt.Println(´2 arreglos concatenados:´, arrC);

// ELIMINAR ITEMS DE ARREGLO
  var todas = []string{´a´,´b´,´c´,´d´,´e´,´F´,´G´,´H´,´i´};
  fmt.Printf(´Todas:\t\t%v\n´, todas);
  var sinMayus = append(todas[0:5], todas[8:]...);
  fmt.Printf(´Sin Mayus:\t%v\n´, sinMayus);

//DIFERENCIA SLICE Y ARREGLO
  difArr   := [3]int{1,2,3}; //ERROR-> difArr   = append(difArr  , 3);
  difSlice := [ ]int{1,2,3}; difSlice = append(difSlice , 3);
  fmt.Println(´Arreglo:´, difArr, ´Slice:´, difSlice);

// COMO DECLARAR UNA MATRIZ
  var matriz = [3][3]float32{
    {0.0, 0.1, 0.2},
    {1.0, 1.1, 1.22},
    {2.0, 2.1, 2.2},
  };
  fmt.Println(matriz[1][2]); // 1.22

// DECLARAR UN MAP DE ARRAYS
  mapaArray := map[string][2]string{
    ´Frasco´: {´agua´, ´Tamarindo´},
    ´Botella´: {´Cerbeza´, ´Negra´},
    ´Caja´: {´Leche´, ´Natural´},
  }
  for k, contenedores := range mapaArray {
   fmt.Println(´Contenedor:´, k);
   for i, contenedor := range contenedores {
       if i == 0 {
         fmt.Print(´\t´, ´Contenido ´, contenedor);
       } else {
         fmt.Println(´ Tipo´, contenedor);
       }
    }
  }

//COMO CREAR UN MAPA
  var mapa = map[rune]string{"o": ´Mario´, "i": ´Moi´, "l": ´Pel´, "c": ´Seco´, "e": ´Dake´}
  fmt.Println(mapa["o"]); // Mario
  el9, ok := mapa["D"];
  if !ok { /*Cae Aqui*/ fmt.Println(´La key 9 no existe´);  } else { fmt.Println(el9); }

// COMO ITERAR UN MAPA
  for llave, valor := range mapa { fmt.Print(llave, ´:´, valor, ´, ´); }
  fmt.Println();

// COMO ELIMINAR ITEM DE UN MAPA
  if item, ok2 := mapa["c"]; ok2 { delete(mapa, "c"); fmt.Println(item, ´ fue eliminado´);  }
  fmt.Println(mapa);
}
```

### Structs

Targetas Anki

```r
Que es un Struct;"
Es un tipo de dato, el cual puede contenedor que puede tener otros
tipos de datos."

Practica Declara Struct Persona y dentro Struct Direccion instancia una persona y accede a sus propiedades;"
<pre>

package main
import ( ´fmt´ )

func main() {
  var mario = Persona {
    nombre: ´Mario´,
    edad: 29,
    altura: 1.69,
    activo: true,
    direccion: Direccion{
      codigoPostal: 90250, 
      colonia: ´centro´,
      municipio: ´Tlaxco´,
      estado: ´Tlaxcala´,
    },
  }

  var neey = Persona {
    nombre: ´Neyra´,
    edad: 26,
    altura: 1.58,
    activo: false,
    direccion: Direccion{
      codigoPostal: 30900,
      colonia: ´Ejidal´,
      municipio: ´Tlalpan´,
      estado: ´CdMX´,
    },
  }

  print(mario);
  print(neey);

}

type Direccion struct {
  codigoPostal int;
  colonia string;
  municipio string;
  estado string;
}

type Persona struct {
  nombre string;
  edad int;
  altura float32;
  activo bool;
  direccion Direccion
}

func print(p Persona) {
  fmt.Printf(´Nombre: %v, edad: %v, altura: %v, activo: %v, estado: %v\n´,
    p.nombre, p.edad, p.altura, p.activo, p.direccion.estado);
}

</pre>"

Que significa que que un Struct embebido sea promobido;"
Cuando las propiedades de dos Structs se pueden acceder al mismo nivel,
de hecho, es un analogo a herencia

<pre>
package main import ( ´fmt´ )

type Usuario struct {
  nombre string;
  pass string;
}

type Trabajador struct {
  Usuario;
  profesion string;
  calificacion int;
}

func main() {
  marioDev := Trabajador{ profesion: ´TICs´, calificacion: 10, Usuario: Usuario{nombre:´Mario´, pass:´mipass´ } };
  //SE VE COMO DESDE UN STRUCT *TRABAJADOR* ACCEDEMOS A UNA PROPIEDAD DE *USUARIO*
  fmt.Println(marioDev.nombre)
}
</pre>"

Que es y como declarar un Struct anomimo;"
Son como las clases anonimas en java, donde no tienen nombre, y asi podemos
declarar e instanciar al mismo tiempo.

<pre>

marioDev := struct { email string; password string; } {email:´m@mail.c´, password:´mypass´ };
fmt.Println(´Email:´, marioDev.email, ´Pass:´, marioDev.password)

</pre>
"

¿Go es OOP?;"
Si y no, ya que no tiene objetos pero si Structs los cuales son analogos
a los objetos, los Structs pueden lograr el *A PIE* de la POO
<pre class='prefor'>
- Encapsulacion (Estado, Comportamiento, Exportado/NO Exportado Visible/NO visible)
- Reusabilidad (tipos embebidos)
- Polimorfismo (interfaces)
- Overriding (Promocion)
- No Creas clases, creas tipos
- No creas instancias, creas un valor de un tipo
<pre>
"
```

### Funciones

#### Defer

Documentacion

- [go.dev/blog](https://go.dev/blog/defer-panic-and-recover)

```r
Que significa defer function (diferir una funcion);"
<pre class='prefor'>
<a href=´https://golang.org/ref/spec#Defer_statements´>docu</a>
Hace que la funcion diferida se ejecute al final o cuado retorne, en
la funcion que la contiene. Puede servirnos por Ej. cuando abrimos
una conexion, inmediatamente cerrarla, pero el metodo que cierra
la conexion la diferimos, y de este modo no olvidamos hacerlo y puede
verse mas lejible el codigo.

¿Por que?
  Puede que en un codigo facil como el de abajo no ahiga problema, de
  ver donde cerrar la conexion, pero si se complica, podemos enfrentarnos
  a cosas mas complejas (panics, controles de flujo, etc), y olvidar
  cerrar algo.
</pre>

<pre>
func main() {
  defer uno(); // se ejecuta al final
  dos(); // Se ejecuta primero
}

func uno() { fmt.Println(´Uno´); }
func dos() { fmt.Println(´Dos´); }
</pre>"

## ////////////////////////////
"defer ¿Que imprime?, ¿por que?
<pre>
func a() {
  i := 0
  defer fmt.Print(i) // ¿?
  i++
}
</pre>
";"
<pre class='prefor'>
¿Que imprime?
  ´0´
¿por que?
  El defer no es evaluado y ejecutado al final, si no que, como va
  avanzando el hilo es evaluada/leida la linea, y luego al final es
  ejecutada. Es por eso que imprime ´0´ por que primero evalua un 0,
  espera y luego lo imprime
</pre>
"

##////////////////
"defer, ¿Que imprime?, ¿por que?
<pre>
func main() {
  for i := 1; i <= 5; i++ {
    defer fmt.Print(i, ´, ´)
  }
}
</pre>
";"
<pre class='prefor'>
¿Que imprime?
  5, 4, 3, 2, 1

¿Poe que?
  El defer funciona como una pila (ultimo en llegar, primero en salir
  [Last in First Out]), es decir, al final entro el 5, el primero ens
  alir es el 5, el penultimo en entrar fue el 4 es el segundo en salir,
  y asi susesivamente
</pre>
"

##//////////////////////////////
"¿Que imprime?, ¿por que?, ¿Que demuestra?
<pre>
func main() {
  fmt.Print(c())
}
func c() (name string) {
  defer func() { name += ´A´ }()
  return ´B´
}
</pre>
";"
<pre class='prefor'>
¿Que imprime?
  BA

¿Por que?
  Primero retorna un B, lo cual name vale B, y luego el defer le agrega
  A, por lo que al final retorna un ´AB´

¿Que demuestra?
  Que el defer se ejecuta despues del return
</pre>
"
```

#### argumentos y retornos

```r
Crear funcion que divida entre dos numeros y regrese resultado y resto;"
<pre>

func main() {
  var resultado, resto = divideYResto(5, 2);
  fmt.Printf(´Resultado: %v Resto: %v\n´, resultado, resto);
  // Resultado: 2.5 Resto: 1
}

func divideYResto(dividendo int, divisor int) (float32, int) {
  var result float32 = float32(dividendo) / float32(divisor);
  return result, (dividendo%divisor);
}

</pre>
"

Crear funcion de argumentos dinamicos o argumentos variable;"
Se realiza con **func suma(nums ...int)**

<pre>
func main() {
  var res = suma(5, 2, 3, 5, 2, 2, 1);
  fmt.Println(res);
}

func suma(nums ...int) int {
  var total int;
  for _, num := range nums { total += num; }
  return total;
}
</pre>"

## desplegar arreglo
Que significa despliege del arreglo/slice;"
Es usar cada uno de los valores del arreglo como argumento

<pre>

func main() {
  myArray := []int{5, 2, 3, 5, 2, 2, 1, 10} 
  var res = suma(myArray...); // DESPLIEGE DE MYARRAY
  fmt.Println(res);
}

func suma(nums ...int) int { /* ... */ }
</pre>"

```

#### funciones y tipos

```r
Como asociar una funcion a un typo, (igual que clase metodo);"
Primero se crea el typo y luego a la funcion se le asocia de la sig.
manera.

<pre>

type Persona struct { nombre string; edad int; }

type Alumno struct { Persona; matricula string; activo bool; }

//NOTAR QUE PODEMOS ENTRAR A NOMBRE PORQUE *PERSONA* FUE PROMOBIDO A *ALUMNO*
func (a Alumno) showDetails() { fmt.Println(´Alumno con matricula: ´ + a.matricula + ´, Nombre: ´ + a.nombre); }

func main() {
  mario := Alumno{ Persona: Persona{ nombre: ´Mario´, edad: 29}, matricula:´12345´, activo: true };
  mario.showDetails();
}
</pre>"

Uso de una funcion polimorfica y como se declara una interface;"
En este caso creamos una funcion que tiene como parametro una interface,
y dos objetos=tipos que cumplen con esta interface.

<pre>
func main() {
  carroRojo := Carro{ tipoMotor:´gasolina´ };
  focoAzul := Foco{ iluminacion:´Led´ };
  //NOTAR QUE LA MISMA FUNCION ACEPTA DOS TIPOS DIFERENTES
  turnOn(carroRojo); // Motor gasolina funcionando
  turnOn(focoAzul);  // Ilumonacion Led brillando
}

//------------ INTERFACE
type ISwitch interface { turnOn(); }
func turnOn(s ISwitch) { s.turnOn(); } // FUNCION POLIMORFICA

//------------ CARRO
type Carro struct { tipoMotor string; }
func (c Carro) turnOn() { fmt.Printf(´Motor %v funcionando\n´, c.tipoMotor); }

//------------ FOCO
type Foco struct { iluminacion string; }
func (f Foco) turnOn() { fmt.Printf(´Ilumonacion %v brillando\n´, f.iluminacion); }
</pre>"

Como sabe que tipo es una interface (assertion type), ej. saber si IPersona es IMaestro o IAlumno;"
Podemos hacerlo con *func registrar(p IPersona) { switch p.(type) {*

<pre>
func main() {
  mario := Alumno{ semestre: 5 };
  juanito := Maestro{ especialidad: ´matematicas´ };

  registrar(mario);
  registrar(juanito);
}

//---------------
type IPersona interface{ registrar(); }
func registrar(p IPersona) {
  switch p.(type) {
    case Alumno: fmt.Print(´Nuevo alumno: ´);
    case Maestro: fmt.Print(´Nuevo maestro: ´);
  }
  p.registrar();
}

//---------------
type Alumno struct{ semestre int; }
func (a Alumno) registrar() { fmt.Printf(´Alumno registrado de Semestre: %v\n´, a.semestre); }

//---------------
type Maestro struct{ especialidad string; }
func (m Maestro) registrar() { fmt.Println(´Registrado: maestro con especialedad de ´ + m.especialidad); }
</pre>"

Como crear una funcion anonima;"
Como en js se crea la func dentro de una func pero sin nombre y los
parentesis al final para que se ejecute.

<pre>
func main() {
  func(x int) {
    fmt.Println(´El significado de la vida es:´, x)
  }(42)
}
</pre>"


Como guardar una funcion en una variable y ejecutarla;"
Igual que JS

<pre>
func main() {
  calcYearsOld := func(birdYear int) int {
    return 2021 - birdYear;
  }
  fmt.Printf(´Tu edad es: %v´, calcYearsOld(1994)); //Tu edad es: 27
}
</pre>"

Como hacer un callback, pasar una funcion por parametro;"
Solo se indica la firma del metodo en el lugar donde va el parametro.

<pre>
func main() {
  myNums := []int{1,2,3,4,5,6,7,8,9};
  fmt.Printf(´Arreglo: %v\n´, myNums);

  total := sum(myNums...);
  fmt.Printf(´Resultado suma todo el arreglo: %v\n´, total);

  sumaPares := operaPares(sum, myNums);
  fmt.Printf(´Resultado suma de numeros pares del arreglo: %v\n´, sumaPares) //20

  multiplicaPares := operaPares(multiplica, myNums);
  fmt.Printf(´Resultado multi de num pares del arr: %v\n´, multiplicaPares ) //384
}

// AQUI PODEMOS VER COMO METEMOS UNA FUNCION COMO PARAMETRO
func operaPares(opera func(... int) int, nums []int) int {
  var pares []int;
  for _, actual := range nums {
    if actual%2 == 0 { pares = append(pares, actual); }
  }
  //AQUI USAMOS LA FUNCION QUE NOS LLEGO POR PARAM
  return opera(pares...);
}

func sum(nums ...int) int {
  var total int; for _, value := range nums { total += value; }; return total;
}

func multiplica(nums ...int) int {
  if len(nums) == 0 { return 0 }
  total := 1; for _, value := range nums { total *= value; } return total;
}
</pre>"

Calcular factorial usando recorsividad;"
Llamar *factorial(n-1)* dentro de *factorial* pero si n==1 retornar *n*

<pre>
func main() {
  fmt.Printf(´Factorial de 4 = %v \nFactorial de 5 = %v´, factorial(4), factorial(5));
}

func factorial(n int) int {
  if(n==1) { return n; }
  return n * factorial(n-1);
}
</pre>"

```

### Punteros

```r
Que es un puntero, como se imprime y se cambia su valor almacenado;"
Es la direccion de memoria donde esta guardado cualquier elemento de
nuestro programa.

<pre>

func main() {
  num := 1;
  fmt.Printf(´Puntero: %v\n´, &num);
  fmt.Printf(´Tipo pun.: %T\n´, &num); // *int significa puntero tipo int
 
  var refnum *int = &num;
  *refnum = 2;
  fmt.Printf(´Cambiado desde puntero: %v\n´, num);
}

</pre>"

Cuando pasas un parametro a un metodo;"
Siempre se crea una copia del tipo, y como en go todo es un typo, siempre
se pasa una copia, podemos cambiar esto usando punteros"

Cambiar propiedad de struct usando puntero en metodo separado;"
Pasamos el puntero del struct y para usar una propiedad usamos
(*puntero).propiedad = x

<pre>

func main() {
  mario := Persona{nombre:´Mario M. V.´};
  
  cambiaSinPun(mario);
  fmt.Println(´Cambio sin puntero: ´, mario);// {Mario M. V.}
  
  cambiaConPun(&mario);
  fmt.Println(´Cambio CON puntero: ´, mario);// {Sanco Perez}
}

type Persona struct { nombre string; }

func cambiaSinPun(p Persona) {
  p.nombre = ´Sancho´;
}

func cambiaConPun(p *Persona) {
  (*p).nombre = ´Sanco Perez´;
}
</pre>"
```

### Paquetes y entorno

```r
## Descargar paquete, instalar paquete, 
Instalar un paquete;"
- u: si ya existe lo actualiza
- golang.o...: (Variable) La direccion donde esta el paquete

`go get -u golang.org/x/crypto/bcrypt`

- Luego podemos importar de la siguiente manera

`import ( ´golang.org/x/crypto/bcrypt´ )`
"

¿Como ver las variables de entorno y que podemos encontrar?;"
`go env`

Podemos encontrar
- GOPATH: Que es nuestro entorno de trabajo
- GOROOT: Donde esta los bin propios de go (compilador, etc)
"

## crear proyecto, iniciar proyecto
Como crear un proyecto en cualquier carpeta (fuera del go path);"
1. `go mod init hello_go` // Creara el archivo go.mod
2. `go mod tidy` // descarga los paquetes que falten.
3. `go run my.go` // compila y ejecuta
"

## importar un archivo local, importar paquete del mismo proyecto
## importar funcion de otro archivo, usar funcion en otro archivo
1d4g: Como manejar multiples archivos .go para llamarse entre si;"
<a href=´https://www.golangprograms.com/how-to-use-function-from-another-file-golang.html´>Fuente<a/>

1. Asegurar que tenemos el go.mod correcto (el module debe tener el 
   mismo nombre que la carpeta que contiene nuestro proyecto)
2. La importacion es parecido a java, en que se debe de llamar desde el
   nombre del proyecto, asea no es como js que podemos hacer uso de
   rutas dinamicas para importar,
3. RECORDAR: No funciona como js, donde se importa el archivo y podemos
   usar las funciones directamente, si no que, debemos hacer referencia
   al paquete y desde este hacer referencia a las funciones.

Ejemplo

<pre>
hola_go/
│   go.mod
│   main.go
├───pkg1/
│       front_controller.go
└───pkg_2/
    │   usuario_services.go
    └───pkg_2_libs/
            libs.go
</pre>

3. Podemos llamar de la siguiente forma

<pre>
=============================== go.mod
module hola_go
go 1.16


=============================== main.go
package main
import (
  ´fmt´
  ´hola_go/pkg1´
  ´hola_go/pkg_2/pkg_2_libs´
)
func main() {
  pkg1.ExposeControllers()
  pkg_2_libs.LibX()
  fmt.Println(´Hola desde Go´)
}


=============================== pkg_2/pkg_2_libs/libs.go
package pkg_2_libs
import (
  ´fmt´
)
func LibX() {
  fmt.Println(´Hola desde libreria´)
}


=============================== pkg1/front_controller.go
package pkg1
import (
  ´fmt´
  ´hola_go/pkg_2´
)
func ExposeControllers() {
  pkg_2.GetUsuarios()
  fmt.Println(´Controladores expuestos´)
}


=============================== pkg_2/usuario_services.go
package pkg_2
import (
  ´fmt´
)
func GetUsuarios() {
  fmt.Println(´{ Todos los usuarios }´)
}
</pre>
"

Como importar un paquete nuestro desde github/gitlab;"
PENDIENTE
"

Como compilar;"
El .exe es opcional, pero estoy en windows asi que es recomendable.
Y sepuede ejecutar solo con llamarlo sin ayuda de nada.

`go build -o my.exe my.go`
"


Que es Marshalling, ejemplo y como se llama su contraparte;"
- Es el proceso de pasar un objeto en memoria, a un formato que pueda
  trasferirse a otro programa o computadora.
- Ej. Pasar un struct a json
- Contraparte se llama Unmarshalling"

## convertir a json
Como convertir un struct a json;"
Con **json.Marshal(myStruct)**

<a href=´https://pkg.go.dev/encoding/json@go1.16.7#Marshal´>Fuente</a>

<pre>
func main() {
  mario := Usuario{
    IdUsuario: 3,
    Email:´Mario M. V.´,
    Phones: []string{´123´, ´234´, ´345´},
    password: ´MyPass3´,
  };
  
  marioJson, err := json.Marshal(mario); if err != nil { fmt.Println(´Error´, err); return ; }
  
  // NOTAR QUE COMO *PASSWORD* INICIA CON MIN ESTE NO SE AGREGA AL JSON
  fmt.Println(string(marioJson)) //{´Email´:´Mario M. V.´,´Phones´:[´123´,´234´,´345´],´IdUsuario´:3}

//-----------------------
  neey := Usuario{
    IdUsuario: 4,
    Email:´Neyra´,
    Phones: []string{´987´, ´765´, ´543´},
    password: ´Your4´,
  };
  usuarios := []Usuario{mario, neey};
  usuariosJson, err2 := json.Marshal(usuarios); if err2 != nil { fmt.Println(´Error´, err2); return ; }
  // SIN PROBLEMAS PASA A JSON EL ARREGLO
  fmt.Println(string(usuariosJson));
}

type Usuario struct {
  Email string;
  Phones []string;
  IdUsuario int;
  password string;
}
</pre>"

Como convertir de un string json a struct;"
Con **json.Unmarshal(myJson, &myStruct);**

<pre>
import ( ´encoding/json´; ´fmt´ )

type Animal struct { Name string; Order string }

func main() {
  var myJsonStr string = `[
    {´Name´: ´Perro´  , ´Order´: ´Cuadrupedo´},
    {´Name´: ´Gallina´, ´Order´: ´Ave´}
  ]`;
  var myJsonBytes = []byte(myJsonStr);

  var myStructs []Animal
  //NOTAR QUE LE PASAMOS UN ARREGLO DE BITES Y LA REFERENCIA DEL STRUC
  err := json.Unmarshal(myJsonBytes, &myStructs)
  if err != nil { fmt.Println(´error:´, err) }
  fmt.Printf(´%+v´, myStructs[1].Order) // Ave
}
</pre>"

ordenar un arreglo/slices de primitivos con paquete de golang;"
Importando el paquete *sort* y usando sus metodos segun la docu.

<a href=´https://pkg.go.dev/sort@go1.16.7´>Docu</a>

<pre>
import ( ´fmt´; ´sort´ )
func main() {
  var nums = []int{1,9,2,8,3,7,4,6,5,0};
  sort.Ints(nums);
  fmt.Println(nums); //[0 1 2 3 4 5 6 7 8 9]

  var palabras = []string{´perro´,´gato´,´cocodrilo´,´zorro´,´ave´,´borrego´,´vaca´};
  sort.Strings(palabras);
  fmt.Println(palabras); //[ave borrego cocodrilo gato perro vaca zorro]

  var promedios = []float64{6.7, 9.5, 5.9, 7.7, 10, 8.5};
  sort.Float64s(promedios);
  fmt.Println(promedios); //[5.9 6.7 7.7 8.5 9.5 10] 
}
</pre>"

Como ordenar un arreglo de structs;"
<a href=´https://pkg.go.dev/sort@go1.16.7#example-package´>Docu</a>

Importar el paquete sort, cumplir con la interface (buscar *type Interface interface*),
y luego meterlo en el metodo sort.Sort().

Ej: Ordenar un arreglo de productos por nombre, precio, fecha y disponible
<pre>
import ( ´fmt´;  ´sort´; ´time´; )
type Producto struct {
  nombre     string
  precio     float32
  disponible bool
  caducacion time.Time
}
// equivalente al toString en java
func (p Producto) String() string {
  return fmt.Sprintf(´\nNom: %v - %v$ - Dispo: %v - Cad: %v/%v/%v:%vhr´, p.nombre, p.precio, p.disponible,
         p.caducacion.Year(), p.caducacion.Month(), p.caducacion.Day(), p.caducacion.Hour())
}

type ByNombre []Producto
func (a ByNombre) Len() int           { return len(a) }
func (a ByNombre) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByNombre) Less(i, j int) bool { return a[i].nombre < a[j].nombre }

type ByCaducacion []Producto
func (a ByCaducacion) Len() int           { return len(a) }
func (a ByCaducacion) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByCaducacion) Less(i, j int) bool { return a[i].caducacion.Before(a[j].caducacion) }

type ByPrecio[] Producto
func (a ByPrecio) Len() int           { return len(a) }
func (a ByPrecio) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByPrecio) Less(i, j int) bool { return a[i].precio < a[j].precio }

type ByDisponible[] Producto
func (a ByDisponible) Len() int           { return len(a) }
func (a ByDisponible) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByDisponible) Less(i, j int) bool { return a[i].disponible }

func main() {
  prodA := Producto{´Chetos´, 10, true, time.Date(2021, time.July, 29, 20, 55, 55, 123, time.UTC)}
  prodB := Producto{´Bubulubu´, 15, true, time.Date(2010, time.March, 12, 12, 54, 51, 125, time.UTC)}
  prodC := Producto{´Submarinos´, 20, true, time.Date(2020, time.January, 28, 9, 54, 51, 125, time.UTC)}
  prodD := Producto{´Abejitas´, 29, false, time.Date(2001, time.March, 12, 9, 54, 51, 125, time.UTC)}
  productos := []Producto{prodA, prodB, prodC, prodD}
  
  sort.Sort(ByNombre(productos)); fmt.Println(´Por Nombre: ´, productos);
  sort.Sort(ByCaducacion(productos)); fmt.Println(´Por Caducacion: ´, productos);
  sort.Sort(sort.Reverse(ByPrecio(productos))); fmt.Println(´Por Precio (Reversed): ´, productos);
  sort.Sort(ByDisponible(productos)); fmt.Println(´Por Disponible: ´, productos); 
}
</pre>"

Practica, ordenar un arreglo de enteros sin ningun paquete;"

<pre>
func main() {
  varios := []int{6, -6, -4, 7, 5, -5, -2, 8, -7, -3, 4, 9, -8, 3, 2, 1, 0, -1, -9}
  ordenado := getSortArray(varios)
  fmt.Print(ordenado) // [-9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9]
}

func getSortArray(arrayDesordenado []int) []int {
  ordenado := []int{}
  indexUsados := []int{}
  indexUsado := -1
  for indexAColocar := 0; indexAColocar < len(arrayDesordenado); indexAColocar++ {
    menor := arrayDesordenado[indexAColocar]
    if contains(indexUsados, indexAColocar) {
      for k := 0; k < len(arrayDesordenado); k++ {
        if contains(indexUsados, k) {
          continue
        } else {
          menor = arrayDesordenado[k]
          break
        }
      }
    }

    for indexAComparar := 0; indexAComparar < len(arrayDesordenado); indexAComparar++ {
      if contains(indexUsados, indexAComparar) {
        continue
      }
      temp := arrayDesordenado[indexAComparar]
      if menor >= temp && !contains(indexUsados, indexAComparar) {
        menor = temp
        indexUsado = indexAComparar
      }
    }
    ordenado = append(ordenado, arrayDesordenado[indexUsado])
    indexUsados = append(indexUsados, indexUsado)
  }

  return ordenado
}
</pre>
"

## crear hash, comparar hash
Como encriptar y comparar un encriptado;"
Para encriptar: `bcrypt.GenerateFromPassword(mySecretEnBytes, fuerzaEnInt)`
Para Comparar: `bcrypt.CompareHashAndPassword([]byte(passA), []byte(passB))`

import (
  ´fmt´
  ´golang.org/x/crypto/bcrypt´
)

func main() {
  mySecret := ´miSecreto´;
  mySecretBytes := []byte(mySecret)
  // GENERACION DEL HASH
  myCryptRaw, err := bcrypt.GenerateFromPassword(mySecretBytes, 4)
  if err != nil { fmt.Println(´error: ´, err) }
  myCryptStr := string(myCryptRaw)
  
  fmt.Println(´Original> ´, mySecret)
  fmt.Println(´Crypted> ´, myCryptStr)
  
  inputBadPass := ´incorrecta´
  inputSuccessPass := ´miSecreto´
  
  comparePass(myCryptStr, inputBadPass)
  comparePass(myCryptStr, inputSuccessPass)

}

func comparePass(passA string, passB string) {
  // COMPARACION DEL HASH
  err := bcrypt.CompareHashAndPassword([]byte(passA), []byte(passB));
  passLength := len(passA)
  if err != nil {
    fmt.Println(passA[passLength-5:], ´=´, passB+´?´, ´: son diferentes´)
  } else {
    fmt.Println(passA[passLength-5:], ´=´, passB+´?´, ´: Son Iguales´)
  }
}
"


Como revizar la arquitentura, procesador, sistema operativo, routines;"
Con el paquete runtime

<pre>
import (  ´runtime´ )
...
runtime.GOARCH  ,  runtime.GOOS
runtime.NumCPU(),  runtime.NumGoroutine()
<pre>
"


Que es y como se hace la compilacion cruzada;"
Es cuando se compila en un S.O. distinto al target, ej., si estamos en
windows y queremos que nuestro binario sea para linux o viceversa."

```

### Concurrencia

```r
##//////////////
"Explica porque el codigo de abajo no se ejecuta el metodo *dos()*
<pre>
func main() {
  go dos();
  fmt.Println(``msj main()``);
}

func dos() {
  fmt.Println(``msj dos()``);
}
//OUTPUT
>msj main()
</pre>
";"
Por que el proceso de crear una go-routine tarda mas de lo que tarda
en terminar main, y cuando termina main se cierra el programa, junto
con todas sus go-routines
"

Como hacer para que el main espere a que terminen sus go-routines;"
El paquete sync, tiene **sync.WaitGroup**, con el cual podemos, agregar
routines, terminar y esperar a que terminen.

<pre>
+ import ( ´sync´)

+ var waitGroup sync.WaitGroup // CREAMOS VARIABLE

  func main() {
+   waitGroup.Add(1) // AGREGAMOS UNA ROUTINE
    go dos();
    
    fmt.Println(´msj main()´);
+   waitGroup.Wait()  // ESPERAMOS A QUE TERMIENEN LAS ROUTINES
  }

  func dos() {
    fmt.Println(´msj dos()´);
    // CUIDADO SI NO LANZAMOS EL METODO COMO ROUTINE DARA ERROR
+   waitGroup.Done() // HEY TERMINO ESTA ROUTINE
  }
</pre>
"

Que es *data race*, y da un codigo explicando en el que pase esto;"
Es cuando multiples hilos/rutinas estan leyendo/escribiendo un mismo dato,
lo cual causa inconsistencias sobre este dato.

<pre>
import (
  ´fmt´
  ´math/rand´
  ´sync´
  ´time´
) 

const rutinasACrear = 10

var compartida int = 0
var waitGroup sync.WaitGroup 

func main() {
  rand.Seed(time.Now().UnixNano())

  for i := 0; i < rutinasACrear; i++ {
    go func() {
      waitGroup.Add(1)
      routineWork()
      waitGroup.Done()
    }()
  }

  waitGroup.Wait()
  fmt.Printf(´Resultado Final: %v\n´, compartida)
}

func sleepRandom() int {
  var randomNum = rand.Intn(1000-1) + 1
  var randomTime = time.Duration(randomNum)
  time.Sleep(time.Millisecond * randomTime)
  return int(randomTime)
}

func routineWork() {
  var randomNum = rand.Intn(999-1) + 1
  sleepRandom()

//VARIOS HILOS LEEN *compartida* ANTES DE QUE OTROS HILOS TERMINEN
  var variableDeRutina int = compartida
  fmt.Printf(´[%v]Leeyo: %v\n´, randomNum, compartida)
  variableDeRutina++
  sleepRandom()
//LO MISMO PERO MODIFICAN *compartida* AL MISMO TIEMPO QUE OTROS HILOS,
//PROVOCANDO QUE PLANCHEN EL TRABAJO DE OTROS
  compartida = variableDeRutina
  fmt.Printf(´[%v]Escribio: %v\n´, randomNum, compartida)
}
//------------------
RESULTADO: Las rutinas se hacen al mismo tiempo y es rapido el proceso,
pero el trabajo es inconcistente, es decir, da resultados finales diferentes
[367]Leeyo: 0       [373]Leeyo: 0     [712]Leeyo: 0
[712]Escribio: 1    [336]Leeyo: 1     [379]Leeyo: 1
[355]Leeyo: 1       [190]Leeyo: 1     [202]Leeyo: 1
[379]Escribio: 2    [373]Escribio: 1  [355]Escribio: 2
[367]Escribio: 1    [202]Escribio: 2  [4]Leeyo: 2
[4]Escribio: 3      [773]Leeyo: 3     [336]Escribio: 2
[190]Escribio: 2    [773]Escribio: 4
Resultado Final: 4
<pre>
"

Que significa/soluciona el Mutex y como lo soluciona y ejemplo;"
- Significa: Asi se le llama al proceso cuando se bloquea una variable
escritura y/o lectura.
- Soluciona: Evita el data race.
- Como lo soluciona: Se ejecuta un metodo lock al inicio y el final unlock
y las variables que se encuentren en medio se le aplicara el mutex

<pre>
import ( ´fmt´ ´math/rand´ ´sync´ ´time´ )

const rutinasACrear = 10
var compartida int = 0
var waitGroup sync.WaitGroup
// CREAMOS UNA VARIABLE DE TIPO MUTEX
+ var mutex sync.Mutex

func main() {
  rand.Seed(time.Now().UnixNano())
  for i := 0; i < rutinasACrear; i++ {
    go func() { waitGroup.Add(1) routineWork() waitGroup.Done() }()
  }
  waitGroup.Wait()
  fmt.Printf(´Resultado Final: %v\n´, compartida)
}

func sleepRandom() int {...}

func routineWork() {
  var randomNum = rand.Intn(999-1) + 1
  sleepRandom()
// ENCERRALOS LO QUE QUEREMOS PROTEJER ENTRE *Lock* y *Unlock*
+ mutex.Lock()
  var variableDeRutina int = compartida
  fmt.Printf(´[%v]Leeyo: %v\n´, randomNum, compartida)
  variableDeRutina++
  sleepRandom()
  compartida = variableDeRutina
//DA LA SEÑAL A OTROS HILOS QUE PUEDEN EDITAR
+ mutex.Unlock()
  fmt.Printf(´[%v]Escribio: %v\n´, randomNum, compartida)
}
//------------------
RESULTADO: El proceso sufre pausas pero el trabajo es consistente
[968]Leeyo: 0   [764]Leeyo: 1     [968]Escribio: 1  [764]Escribio: 2
[983]Leeyo: 2   [983]Escribio: 3  [53]Leeyo: 3      [53]Escribio: 4
[258]Leeyo: 4   [258]Escribio: 5  [431]Leeyo: 5     [431]Escribio: 6 
[392]Leeyo: 6   [392]Escribio: 7  [571]Leeyo: 7     [571]Escribio: 8
[303]Leeyo: 8   [303]Escribio: 9  [981]Leeyo: 9     [981]Escribio: 10
Resultado Final: 10
</pre>
"

¿Como evitamos la inconistencia de go-routinas solas y la lentitus de Mutex?;"
Con el uso de atomic

<pre>
import ( ´fmt´ ´math/rand´ ´sync´ ´sync/atomic´ ´time´ )
const rutinasACrear = 10
var waitGroup sync.WaitGroup
// DEBEMOS DE USAR UN TIPO ESPECIFICO DE VARIABLE
+ var compartida int32 = 0

func main() {
  rand.Seed(time.Now().UnixNano())

  for i := 0; i < rutinasACrear; i++ {
    go func() {
      waitGroup.Add(1)
      var randomNum = rand.Intn(999-1) + 1
      sleepRandom()
//TENDREMOS UNA LECTURA INCONCISTENTE
      fmt.Printf(´[%v]Leeyo: %v\n´, randomNum, atomic.LoadInt32(&compartida))
      sleepRandom()
//TENDREMOS UNA ESCRITURA CONSISTENTE,
      atomic.AddInt32(&compartida, 1)
      fmt.Printf(´[%v]Escribio: %v\n´, randomNum, compartida)
      waitGroup.Done()
    }()
  }
  waitGroup.Wait()
  fmt.Printf(´Resultado Final: %v\n´, compartida)
}
//----------------------------
RESULTADO: El resultado es consistente y no se ve muy afectado el performance
[130]Leeyo: 0     [940]Leeyo: 0     [428]Leeyo: 0     [850]Leeyo: 0
[647]Leeyo: 0     [795]Leeyo: 0     [956]Leeyo: 0     [816]Leeyo: 0
[130]Escribio: 1  [150]Leeyo: 1     [107]Leeyo: 1     [428]Escribio: 2
[940]Escribio: 3  [816]Escribio: 4  [107]Escribio: 5  [795]Escribio: 6
[850]Escribio: 7  [647]Escribio: 8  [956]Escribio: 9  [150]Escribio: 10
Resultado Final: 10
</pre>
"
```

### Channels

```r
¿Que es un channel?, ¿Cuantos hay, ¿como se castean entre ellos?;"
¿Que es un channel?
  Es la forma de hacer concurrencia recomendada, igual nos sirve bien
  lock y atomic, pero con channels es mas elegante/recomndable.
    Recordar que en Go todo es un tipo asi que channel lo manejaremos
    como un tipo

¿Cuantos hay?
  hay tres tipos
  1. Bidireccional, puede enviar y resivir informacion
    `soloEnviar := make(chan int, 1)`
  2. Send only `soloEnviar := make(<-chan int, 1)`
  3. Receive only `soloResivir := make(chan<- int, 1)`

¿como se castean entre ellos?
  Podemos castear de mas general a mas especifico pero no al revez, es
  decir se puede de Bidireccional a Send o Receive, pero no de Send o
  Receive a Bidireccional.
"

##//////////////
"Por que da error?, Es un canal con buffer o sin buffer?, Codigo corregido, 
y ¿Es un canal con buffer o sin buffer?
<pre>
func main() {
  miCanal := make(chan int)
  miCanal <- 10
  print(<-miCanal)
}
//-------------------------
Fatal error: all goroutines are asleep - deadlock!
goroutine 1 [chan send]:
</pre>
";"
Por que da error?
  En este caso este canal solo tiene 1 rutina, y los canales que envian
  info, necesitan 2, porque al enviar dato que no llega a ningun lado
  da este error.

Es un canal con buffer o sin buffer?
  Sin Buffer

Codigo corregido
<pre>
func main() {
  miCanal := make(chan int)
  go func() { // Esta go-routine envia info
    time.Sleep(time.Second * 2)
    miCanal <- 10
  }()
  // la go-routine del main resive el dato
  println(´Hola1 main´)
  println(<-miCanal) // Se espera la linea, que le llege la info
  println(´Fin de main´)
}
</pre>"

"¿Es buffer con o sin canal?, ¿Porque falla el codigo?, y Codigo corregido, 
<pre>
func main() {
  miCanal := make(chan int, 1)
  miCanal <- 10
  miCanal <- 12
  println(<-miCanal)
}
//------Fatal error: all goroutines are asleep - deadlock!
</pre>
";"
¿Es buffer con o sin canal?
  Es un canal con buffer, se identifica por el segundo argumento `make(chan int, *1*)`

¿Porque falla el codigo?
  Estamos declarando que el buffer solo sirve para enviar un dato, pero
  le estamos enviando 2, lo cual cuando enviamos el segundo dato, el
  canal ya esta cerrado

codigo corregido
<pre>
func main() {
  miCanal := make(chan int, 2)
  miCanal <- 10
  miCanal <- 12
  println(<-miCanal) // 10
  println(<-miCanal) // 12
}
</pre>"

## ////////////////////////////////
"Crea un canal bidireccional, y dos metodos, enviar() que resivira el
canal como send-only, y resivir() que resivira el canal como receive-only,
y usalos para enviar/resivir un dato usando estos metodos";"

<pre>
func main() {
  canal := make(chan int)
  go enviar(canal, 20)
  resive(canal)
}

func enviar(canalDeEnvio chan<- int, aEnviar int) {
  canalDeEnvio <- aEnviar
}

func resive(canalDeEnvio chan<- int) {
  print(<-canalDeEnvio)
}
</pre>"

Crea un channel en un for, para que itere cada vez que resiva el canal;"
1. Debemos de crear el canal como en ese caso no indicamos un bufer,
   necesitamos enviar la info en una go-routine aparte,
2. Al terminar de enviar info necesitamos errar el canal, si no dara
   error cuando estemos ´escuchando´ el canal
3. Podemos ´escuchar´ el canal con ´for infoCanal := range canal {...´

IMPORTANTE: Si usamos un canal en un range, y no los cerramos, el range
los bloquera

<pre>
func main() {
  flujito := make(chan int)

  go func() {
    for i := 0; i < 10; i++ {
      time.Sleep(time.Millisecond * 100)
      flujito <- i
    }
// CUIDADO: No olvidar cerrar el canal, si no en ´range flujito´ dara error
// cuando trate de iterar y no llege nada
    close(flujito)
  }()

  for gotita := range flujito {
    print(gotita, ´, ´)
  }
}
</pre>
"

select/case ¿Para que es? y Da codigo explicado;"
¿Para que es?
  Para ejecutar un bloque de codigo dependiendo determinado, segun a que
  canal le haiga llegado informacion (como un switch/case).

Explicacion Codigo
  Tenemos 3 canales, y a 2 le enviaremos 10 veces info de forma aleatoria,
  y usaremos un loop infinito para ´escuchar´ los 3 canales,
  y dependiendo del canal que le llege info se ejecutara un bloque u otro,
  y cuando le llege info al tercer canal terminara el loop infinito.

<pre>
func main() {
  rand.Seed(time.Now().UnixNano())

  servidor1 := make(chan int)
  servidor2 := make(chan int)
  servidor3 := make(chan int)

  var randomNum int
  go func() {
    for i := 0; i < 10; i++ {
      randomNum = rand.Intn(30-1) + 1
      if randomNum <= 15 {
        servidor1 <- i
      } else if randomNum >= 16 {
        servidor2 <- i
      }
    }
    // CUIDADO SI NO CERRAMOS DA ´fatal error: all goroutines are asleep - deadlock´
    close(servidor1)
    close(servidor2)
    servidor3 <- 0
  }()

  // un loop infinito asi que necesitamos un break
  for {
    exitLoop := false
    select {
    case numS1 := <-servidor1:
      println(´Send to sev 1:´, numS1)
    case numS2 := <-servidor2:
      println(´Send to sev 2:´, numS2)
    case <-servidor3:
      // AL PARECER AQUI EL BREAK LO CONFUNDE CON EL BREAK DEL CASE
      println(´Salir´)
      exitLoop = true
    }
    if exitLoop {
      break
    }
  }
}
</pre>
"

Como comprobar si un canal esta cerrado o abierto y da codigo explicado;"
Como comprobar?
  Con el uso de la sintaxis coma ok, el cual de un lado obtendremos el
  valor y del otro un booleano que nos dira si esta abierto o cerrado

Explicacion codigo
  Vemos que en el primer bloque nos llega un 1 cuando escuchamos correctamente,
  pero cuando escuchamos cuando esta cerrado nos llega un 0, lo cual
  no podemos saber si nos enviaron un cero, o es un valor por defecto,
  y el canal esta cerrado.
  En el segundo bloque, enviamos un 0, y podemos diferenciar cuando el
  codigo esta abierto o cerrado.

<pre>
func main() {
  {
    canalUno := make(chan int)
    go func() {
      canalUno <- 1
      close(canalUno)
    }()
    println(<-canalUno) // 1
    println(<-canalUno) // 0
  }

  {
    canalDos := make(chan int)
    go func() {
      canalDos <- 0
      close(canalDos)
    }()
    val, ok := <-canalDos
    fmt.Printf(´Val: %v, Ok, %v\n´, val, ok) // Val: 0, Ok, true
    val, ok = <-canalDos
    fmt.Printf(´Val: %v, Ok, %v´, val, ok) // Val: 0, Ok, false
  }
}
</pre>
"

Patron de diseño ´Fan In´ ¿Para que sirve?, ¿En que consiste?, Codigo explicado;"
¿Para que sirve?
  Este patron nos ayuda a menejar codigo concurrente.

¿En que consiste?
  En tomar los valores de multiples canales que trabajan de forma independiente,
  y enviarselos a un unico canal

Codigo explicado 
  A dos canales les entaamos enviando diferentes numeros, y estos canales
  a su bez le envian a un canal, lo cual podemos ver todos los numeros
  en un solo canal

<pre>
func main() {
  unoAlCinco := make(chan int)
  siesAlDiez := make(chan int)
  fanIn := make(chan int)

  // Enviar
  go func() {
    for i := 1; i <= 10; i++ {
      if i < 5 {
        unoAlCinco <- i
      } else {
        siesAlDiez <- i
      }
    }
    close(unoAlCinco)
    close(siesAlDiez)
  }()

  //escuchar
  go func() {
    var wg sync.WaitGroup
    wg.Add(2)

    go func() {
      for numMenor := range unoAlCinco {
        fanIn <- numMenor
      }
      wg.Done()
    }()

    go func() {
      for numMayor := range siesAlDiez {
        fanIn <- numMayor
      }
      wg.Done()
    }()

    wg.Wait()
    close(fanIn)
  }()

  for num := range fanIn {
    println(num)
  }
}
</pre>
"

## PENDIENTE Codigo explicado 
##  Analogia: Tenemos N personas (canales independientes) encontrando
##  cosas de forma aleatoria, y queremos la cantidad de cada una de las
##  cosas encontradas, Asi que cada persona gritara un el nombre de la cosa
##  que encontro, y el contador(canal fan-In) se encargara de contar.

PENDIENTE Patron de diseño ´Fan Out´ ;"
"

Para que sirve el context en las go-routines;"
Nos da una vista general de los procesos, en este caso podemos ver
las routines activas, y podremos cancelarlas para que dejen de consumir
recursos.

Por ejemplo, Si tenemos un web server cada peticion abrira una go-routine,
y esta go-routine puede abrir otras, por ej., para hacer peticiones a
una B.D.. Y con el contex podemos cerrar alguna que no se aiga cerrado"

Uso de context, codigo basico explicado;"

Explicacion:
  Se esta concelando una go-routine fuera de esta, esto lo logramos
  usando context

<pre>
import (
  ´context´
  ´fmt´
  ´runtime´
  ´time´
)

func main() {
  // Obtenemos el context del en este caso del main
  ctx, cancel := context.WithCancel(context.Background())

  fmt.Println(´chequeo de error A:´, ctx.Err())           //: <nil>
  fmt.Println(´num gorutinas A:´, runtime.NumGoroutine()) //: 1

  go doSomeGoRoutine(ctx)

  time.Sleep(time.Second * 1)
  fmt.Println(´\nchequeo de error:´, ctx.Err()) // : <nil>
  // Imprime 2 por que sigue activa la go-routine que abrimos arriba
  fmt.Println(´num gorutinas B:´, runtime.NumGoroutine()) // : 2
  // al cancelar el context ´ctx.Done()´ obtiene un valor (sirve como enviar una señal)
  // y asi con un select/case podemos terminar la go-routina
  cancel()
  fmt.Println(´context cancelado.´)

  time.Sleep(time.Second * 2)
  // Se detecta que se cancelo el context
  fmt.Println(´chequeo de error C:´, ctx.Err()) //: context canceled
  //Ya solo vemos 1 gorutina por que ya se cerro
  fmt.Println(´num gorutinas C:´, runtime.NumGoroutine()) // : 1
}

func doSomeGoRoutine(ctx context.Context) {
  n := 0
  for { // No confundir for-infinito con go-routine, por lo que debemos
        // de romper el ciclo for de alguna manera
    select {
    case <-ctx.Done():
      return
      //break DUDA: Con este dice ´num gorutinas C: 2´, ¿por que se queda viva la go-routine?
    default: // Se repite 5 veces ya que 200 x 5 = 1000 = 1 segundo
      n++
      fmt.Print(´Trabajando´, n, ´, ´)
      time.Sleep(time.Millisecond * 200)
    }
  }
}
</pre>
"
```

### Manejo de errores

#### Manejar errores de terceros

```r
¿Como se manejan los errores?;"
Usan una aproximacion diferente, ya que las funciones pueden
regresar multiples valores, uno de ellos pueden ser error, y Go
recomenda que justo se llama una funcion, hay que checar si tiene
un error, y si existe manejarlo ahi mismo.
"

¿Por que no Existen exepciones?;"
Dicen que estas aumentan la complejidad del codigo, haciendo facilmente
que se ensucie el codigo.
"

¿Que tipo es error?;"
Se maneja como cualquier tipo (ej. int, bool, etc), pero tiene una
interface ´Error() string´, asi que cualquier tipo que lo implemente
podra se manejado como uno.
"

Codea Manejo de errores, al introducir por teclado, crear un archivo, y leer un archivo;"
Leer los comentarios del codigo

<pre>
func main() {

  // vemos como al no tener excepciones estamos obligados a definir que debemos
  // de hacer si ocurre un error
  nombre, errorNombre := pideNombre()
  if errorNombre != nil {
    println(errorNombre)
    return
  }
  println(´Tu nombre es: ´, nombre)

  // Aqui lo mismo al no lanzar un error por que el archivo no existe, debemos
  // de comprobar si existe el archivo y a partir de eso decidir que hacer
  // en cualquiera de los casos
  imprimeArchivo(´inexistente.txt´)
  imprimeArchivo(´siExiste.txt´)

}

func pideNombre() (string, error) {
  fmt.Print(´Nombre: ´)
  var nombre string
  _, err := fmt.Scan(&nombre)
  if err != nil {
    return ´´, err
  }
  return nombre, nil
}

func imprimeArchivo(nombreArchivo string) {
  existeElArchivo := fileExists(nombreArchivo)
  if !existeElArchivo {
    println(´El archivo ´ + nombreArchivo + ´ no existe´)
    return
  }

  miArchivo, errorMiArchivo := os.Open(nombreArchivo)
  if errorMiArchivo != nil {
    print(errorMiArchivo)
    return
  }

  miArchivoBiteString, errorLecturaArchivo := ioutil.ReadAll(miArchivo)
  if errorLecturaArchivo != nil {
    print(errorLecturaArchivo)
    return
  }
  println(string(miArchivoBiteString))
}

func fileExists(filename string) bool {
  info, err := os.Stat(filename)
  if os.IsNotExist(err) {
    return false
  }
  return !info.IsDir()
}
</pre>"
```

#### Errores propios y recover

```r
Como crear un panic;"
<pre>
func main() {
  // se imprimira esta linea, mas info de error, como archivo, linea, etc.
  panic(´Ocurrio un error´)
  // No se ejecutara esta linea
  print(´Fin de main´) 
}
</pre>"

Recover: ¿Que es?, ¿Como funciona?, Codigo explicado;"
¿Que es?
  igual que el catch en java, c#, etc, el cual podemos ejecutar un bloque
  de codigo, si es que ocurre un error

¿Como funciona?
  Es una funcion que retorna el error que metimos en panic, asi podemos
  condicionar si actuamos o no al al error.

Codigo explicado
  En este caso usamos refer ya que panic hara que regrese la funcion,
  y sabemos que defer se ejecutara despues del return, asi que, se
  saltara la linea del main, y luego hara makeError(), lo cual se
  ejecutara handleError()

<pre>
//IMPRIMIRA: Recovered {Error Lanzado}
func main() {
  defer handleError()
  makeError(true)
  print(´main´)
} 

//IMPRIMIRA: main
//func main() { defer handleError() makeError(false) print(´main´) } 

func makeError(doIt bool) {
  if doIt {
    panic(fmt.Sprintf(´{Error Lanzado}´))
  }
}

func handleError() {
  if a := recover(); a != nil {
    fmt.Println(´Recovered´, a)
  }
}
</pre>"


Como castear un error a otro, ej. de error generico a especifico;"<pre>
//'err' es de tipo generico 'error'
  _, err = someting();
  if err != nill {
//Aqui intentamos convertir de tipo `error` a `EspesificTypeError`
    if stripeErr, ok := err.(*stripe.Error); ok {
      msg = cardErrorMessage(stripeErr.Code)
    }

    if specificError, ok := err.(*EspesificTypeError) {
      specificError.something
      ...
    }
  }
</pre>
"

Crea una funcion que arroje error, la maneje y continue ejecutando main;"
Analogia: Se trata de leer en un disco, que dara error cuando no este
disponible, pero si esta o no esta disponible, el main debe seguir
ejecutandose.

<pre>
func main() {
  getInfo(true)
  fmt.Println(´Continua con Main 1´)
  getInfo(false)
  fmt.Println(´Continua con Main 2´)
}

func getInfo(fail bool) {
  defer handleError()
  fmt.Println(´getInfo()...´)
  makeError(fail)
  fmt.Println(´Informacion Recolectada´)
}

func handleError() {
  if error := recover(); error != nil {
    fmt.Println(´Error hendled´, error)
  }
}

func makeError(throwError bool) {
  if throwError {
    fmt.Println(´Panicking!´)
    panic(fmt.Sprintf(´Disco_no_disponible´))
  }
  fmt.Println(´Sin error al leer disco´)
}
//////////////////////IMPRIME:
  getInfo()...
  Panicking!
  Error hendled Disco_no_disponible
  Continua con Main 1
  getInfo()...
  Sin error al leer disco
  Informacion Recolectada
  Continua con Main 2
<pre>"

##///////////////////////////////
"Que imprime el siguiente codigo
<pre>
func main() {
  doSomething()
  fmt.Println(´termino main?´)
}

func doSomething() {
  defer handleError()
  fmt.Println(´llegara a linea 2 de calcSomething()?´)
  calcSomething(1)
  fmt.Println(´llegara a linea 4 de calcSomething()?´)
}

func handleError() {
  if r := recover(); r != nil {
    fmt.Println(´Error controlado:´, r)
  }
}

func calcSomething(i int) {
  if i > 2 {
    fmt.Printf(´Ultimo calc?: %v\n´, i)
    panic(fmt.Sprintf(´FALLO_POR_X_RAZON´))
  }
  defer fmt.Println(´ia´, i)
  fmt.Println(´ir´, i)
  calcSomething(i + 1)
}
</pre>
";"
> llegara a linea 2 de calcSomething()?
> ir 1
> ir 2
> Ultimo calc?: 3
> ia 2
> ia 1
> Error controlado: FALLO_POR_X_RAZON
> termino main?
"

Como crear un error con string;"
¿Como?
  Con el uso de ´errors.New()´

<pre>
func main() {
  if valor, errorValor := doSomething(); errorValor != nil {
    fmt.Printf(´%v´, errorValor)
  } else {
    print(valor)
  }
}

func doSomething() (string, error) {
  miError := errors.New(´ERROR_X´)
  return nil, miError
}
///////////IMPRIME
ERROR_X
</pre>
"

crear error con struct;"
1. Crear struct
2. Ligar nuestro struct al metodo ´Error() string´ pra que cumpla la interface
3. Ya podemos meter nuestro struct a un tipo error
<pre>
// 1
type ErrorResponse struct {
  HttpStatus       int
  Causa            string
  ErrorParaUsuario bool
}

// 2
func (err ErrorResponse) Error() string {
  errorResponse, errorMakingErrorResponse := json.Marshal(err)
  if errorMakingErrorResponse != nil {
    return `{httpStatus: 500, causa: ´Marshaling error on ErrorResponse´ errorParaUsuario: false}`
  }
  return string(errorResponse)
}

func main() {
  if valor, errorValor := doSomething(); errorValor != nil {
    fmt.Printf(´%v´, errorValor)
  } else {
    print(valor)
  }
}

func doSomething() (string, error) {
// 3
  var miError error = ErrorResponse{400, ´Error en doSomething´, true}
  return nil, miError
}

///////////IMPRIME
> {´HttpStatus´:400,´Causa´:´Error en doSomething´,´ErrorParaUsuario´:true}
</pre>
"
```

#### Uso de Logs

```r
Manda a imprimir un mensaje a un log, 1,creaArchivo 2,guarda linea;"
Esto lo logramos con log.Println el cual funcionara como fmt, pero si
le indicamos un archivo, se guardara aqui, con la diferencia que log
nos pondra la fecha y hora que se imprimio

<pre>
func main() {
  crearArchivoLogs()

  filePoint, errorFile := os.Open(´no.txt´)
  defer filePoint.Close()
  if errorFile != nil {
    log.Println(´Ocurrio error´, errorFile)
    return
  }
  print(filePoint)
}

func crearArchivoLogs() {
  fileLogsPointer, errorFile := os.Create(´logs.txt´)
  if errorFile != nil {
    fmt.Println(´Ocurrio error´, errorFile)
    return
  }
  log.SetOutput(fileLogsPointer)
}
</pre>"
```

### Escribiendo Documentación

```r
Como podemos ver la documentacion de algun metodo en nuestro shell;"
- ´-src´: Opcional, para que nos muestre el codigo/interface
- `fmt Print`: Nombre del paquete seguido del metodo

Ejemplo: ´go doc -src fmt Print´

< Package fmt // import ´fmt´
<
< // Print formats using the default formats for its operands and writes to standard output.
< // Spaces are added between operands when neither is a string.
< // It returns the number of bytes written and any write error encountered.
< func Print(a ...interface{}) (n int, err error) {
<         return Fprint(os.Stdout, a...)
< }

CUIDADO: No confundir con godoc, ya que este sirve para levantar un serv
con la socumentacion en el
"

## Crear documentacion online
instalar godoc y levantar http para mostrar documentacion;"
Si al querer correr godoc nos marca el error `command not found` o 
`´godoc´ is not recognized as an internal or external command`

Debemos de instalarlo con: ´go get golang.org/x/tools/cmd/godoc´

Levantar http: `godoc -http :8080`
"
```

### Testing & Benchmarking

#### Introducion

```r
¿Como crear tests?;"
1. El archivo debe de tener una terminacion ´_test.go´
2. Estar en el mismo paquete en el que esta siendo probado
3. El nombre de metodo debe ser ´func TestXxx(t *testing.T)´
4. Corremos todos los test con ´go test ./...´
"

## ejecutar tests, correr tests, iniciar tests
¿Comando ejecutar tests?;"
Para correr los que estan en el dir: `go test`
Para correr los que estan en el dir y subDirectorios: `go test ./...`
"

¿Como definir error en un test?;"
testing.T.Error(´Expected XXX, Got XXX´)
"

Que segnifica el acronimo BET/apostar para buenas practicas de testing;"
B: Benchmarking (pruebas de rendimiento)
E: Examples (Ejemplos para documentacion)
T: Testing (Unit testings)
"

Ejemplo basico de test unitario;"
Solo tenemos una funcion que debe retornar ´hello´ para pasar el test,
en caso contrario fallara el test.
Nota: para correrlo usamos `go test`, o el mismo IDE nos ayuda para ejecutar
solo uno de ellos

------- Implementacion: theSameName.go
<pre>
package theSameName
func HaveIToFail(fail bool) string {
  if fail {
    return ´Bye´
  }
  return ´Hello´
}
</pre>

------- Test: theSameName_test.go
<pre>
// NOTAR QUE EL NOMBRE DE ARCHIVO ES EL MISMO SOLO QUE ´XXX_test.go´
// NOTAR QUE ESTA EN EL MISMO PAQUETE
package theSameName
// NECESITAMOS IMPORTAR LA LIBRERIA DE TESTING
import ´testing´
// NOTAR QUE TIENE EL MISMO NOMBRE ´TestXXX´ PERO EL ARGUMENTO CAMBIA
func TestHaveIToFail(t *testing.T) {
  expected := ´Hello´
  got := HaveIToFail(true)

  if expected != got {
// SI FALLA LANZAMOS ERROR
    t.Errorf(´Expected: %v, Got %v´, expected, got)
  }
}
</pre>"
```

#### Tabla de Tests

```r
Que es una tabla de tests, y da codigo ej. trasformar pesos a dolares;"
¿Que es?
Es cuando probamos la misma funcionalidad varias veces, con diferentes
datos de entrada y salida.

------------- main.go
<pre>
package main
func pesoToDollar(pesos float32) float32 {
  return pesos * 20
}
</pre>

Explicacion Codigo:
  Para crear nuestra tabla creamos un objeto que tiene dos propiedades,
  pesos que es el input del metodo, dollars que es la salida esperada
  del metodo, y creamos un arreglo de este objeto, y lo iteramos y por
  cada iteracion probamos el metodo, y si algo falla, detenemos el test,
  y mostramos que fue lo que fallo.
  NOTA: si no queremos que se detenga el metodo y siga con la tabla de
  test usamos Error en lugar de fatal

------------- main_test.go
<pre>
package main
import ´testing´
func TestPesoToDollar(t *testing.T) {
  type UnitTest struct {
    pesos   float32
    dollars float32
  }
  testsTable := []UnitTest{
    UnitTest{1.0, 20.0},
    UnitTest{2, 40},
    UnitTest{10, 200},
    UnitTest{20, 400},
    UnitTest{65, 1300},
  }
  for _, unitTest := range testsTable {
    got := pesoToDollar(unitTest.pesos)
    if got != unitTest.dollars {
// DETENDRA EL TEST AL PRIMERO QUE FALLE
      t.Fatalf(´Expected: %v, Got: %v´, unitTest.dollars, got)
// CONTINUARA LA TABLA DE TESTS, NO IMPORTA QUE FALLE DESDE EL PRIMERO
//    t.Errorf(´Expected: %v, Got: %v´, unitTest.dollars, got)
    }
  }
}
</pre>"
```

#### Tests y documentacion

Pendiente

- TODO: [Testable Examples in Go](https://go.dev/blog/examples)

A traves de los test se pueden generar ejemplos de como usar algun
metodo directo en la documentacion.

#### Inspectores de codigo Golint

Fuente:

- [golint](https://github.com/golang/lint)

```r
Que es ´Golint´ ´go vet´;"
<pre class='prefor'>
Golint\n
\tEs una herramienta que nos ayuda, a ver si tenemos errores de estilos
\ten nuestro codigo,\n

´go vet`\n
\tNos ayuda a buscar posibles errores, como operaciones con booleanos\n
</pre>
"
```

#### Benchmark

```r
¿Que es un Benchmark?;"
Es el nombre que se le da a las pruebas de rendimieto
"

Benchmark, ¿Pasos para crearlo?;"
<pre class='prefor'>
1. El nombre de archivo y paquete deben ser igual que en test
2. El metodo debe llamarse ´BenchmarkXxxx(b *testing.B)´
</pre>
" 

Benchmark, ¿Como ejecutarlos?;"
Con el comando ´go test -bench .´
"

Benchmark, ¿Como interpretarlos?;"
Al ejecutar el comando tendremos una salida como la siguiente
<pre>
  pkg: hola_go/testing/Benchmark
  cpu: Intel(R) Core(TM) i3-2370M CPU @ 2.40GHz
  BenchmarkFactorial-4    16001491                68.58 ns/op
</pre>
Donde el ´...al-4´ es el numero de procesadores.
el ´68.58 ns/op´ es el tiempo por operacion (ns/op=operacion por nano segunfo)
"

como crear un Benchmark basico, de una funcion factorial;"
----------------------------- Codigo a testear: mainBenchmark.go
<pre>
package main
func Factorial(num int) int {
  if num == 1 {
    return num
  }
  return num*Factorial(num-1) + 1
}
</pre>

----------------------------- Tests: mainBenchmark_test.go
<pre>
package main
func BenchmarkFactorial(b *testing.B) {
  for i := 0; i < b.N; i++ {
    randomNum := rand.Intn(7) + 1
    Factorial(randomNum)
  }
}
</pre>"
```

#### Coverage

```r
¿En test, Que es el Coverage?;"
Es el porcentaje de codigo en lineas (no funciones) que estan cubiertas por tests"

¿Como saber el porcentaje que cubren nuestras pruebas (Coverage)?;"
Con el comando ´go tool cover [-html=salida.html]´
"
```

#### RESUMEN: Tests y benchmarking

```r

Crea un B.E.T. para un paquete Perro, donde tenga un metodo pase años humanos a perro (1h/7p);"
Nos referimos a crear pruebas unitarias, pruebas de rendimiento, y ejemplos
para la documentacion

<pre>
func TestAniosPerroAAniosHumano(t *testing.T) {
  aniosPerro := Perro.AniosPerroAAniosHumano(1)
  if aniosPerro != 7 {
    t.Fatalf(´Expexted: %v, Got: %v´, 7, aniosPerro)
  }
}

func BenchmarkAniosPerroAAniosHumano(b *testing.B) {
  for i := 0; i < b.N; i++ {
    Perro.AniosPerroAAniosHumano(10)
  }
}

func ExampleAniosPerroAAniosHumano() {
  print(Perro.AniosPerroAAniosHumano(1))
  //Output
  //7
}
</pre>

Y para ejecutar todos usamos

<pre>
- test: ´go test´
- benchmarking: ´go test -bench´
- covertura: ´go tool cover -hmtl=salida.html´
</pre>
"
```

### Estructura de datos y Ordenamiento

#### Notacion BigO

```r
BigO para que es y para que sirve;"
Es una notacion para exprezar eficiencia de un algoritmo, porque indicarlo
en tiempo es impresiso, ya que hay muchas variables a tomar en cuenta,
como es la especificaciones de la maquina, la entrada, salida, el medio
de entrega, etc."

En BigO, Que significa ´O(1)´;"
Significa que el tiempo es constante, por ej. si solo imprimimos
algo en consola, la operacion siemre es la misma, no importa cual sea
la entrada o cuantas veces lo ejecutemos.

<pre>
// El tiempo que tardara en ejecutarse no variara.
func main() { print(`Hola`) }

// Lo mismo, incluso si el arreglo tiene 1m de items, ua que este afecatara
// la memoria pero no afectara la cantidad de operaciones.
func main() { print(arr[0]) }
</pre>
"

En BigO, Que significa `O(n)`;"
TIEMPO LINEAL, aqui el tiempo dependera de la entrada

<pre>
  // aqui las opreraciones dependen del tamaño del arreglo
  func main() { for i:=0; i<len(arr); i++ { print(arr[i]) }}
</pre>
"

En BigO, Que significa `O(n^2)`;"
TIEMPO CUADRATICO: Significa que una entrada mas multiplicara el tiempo
que tardara

<pre>
// En este caso si entra 2 se ejecutara 4, o con 3..9, o con 4..16,
// con 5..25 y asi sucesivamente.
for i:=0; i<len(arr); i++ {
  for j:=0; j<len(arr); j++ {
    print(j)
  }
)
</pre>
"

En BigO, Que significa `O(Mn), ej. O(10n)`;"
Significa que el tiempo sera tiempo=10n, donde la entrada multiplicara,
por un numero fijo.

<pre>
// En este caso seria O(10n), ya que por cada elemento en el arreglo,
// se multiplicara por 10, por ej. si entra 2 se ejecutara 20, con 5..50,
// con 10..100, y asi sucesivamente.

for i:=0; i<len(arr); i++ {
  for j:=0; j<10; j++ {
    print(j)
  }
)
</pre>
"
```

#### Algoritmo de insercion - Insertion sort

Targetas Ankis

```r
Insertion sort, ¿como funciona?;"
Este asume que el primer arreglo esta ordenado, y aberigua si el proximo
lo esta, por ej., si para ordenar de menor a mayor, el primer elemento
se asume que es el menor, y lo compara con el que esta al lado, si es
menor lo intercambia, si no, lo deja donde estaba.

TODO: PENDIENTE: Buscar animacion que explique este

<pre>
> i  k               Asigna: i=1 k=4
{ 1  4  3  2  5  6 } Compara (j>i)=(4>1)=false
{ 1  4  3  2  5  6 } Intercambiar: false
------------------------------------

> i     k            Asigna: i=1 k=3
{ 1  4  3  2  5  6 } Compara (j>i)=(3>1)=false
{ 1  4  3  2  5  6 } Intercambiar: false
------------------------------------

> i        k         Asigna: i=1 k=2
{ 1  4  3  2  5  6 } Compara (j>i)=(2>1)=false
{ 1  4  3  2  5  6 } Intercambiar: false
------------------------------------

> i           k      Asigna: i=1 k=5
{ 1  4  3  2  5  6 } Compara (j>i)=(5>1)=false
{ 1  4  3  2  5  6 } Intercambiar: false
------------------------------------

> i              k   Asigna: i=1 k=6
{ 1  4  3  2  5  6 } Compara (j>i)=(6>1)=false
{ 1  4  3  2  5  6 } Intercambiar: false
------------------------------------

>    i  k            Asigna: i=4 k=3
{ 1  4  3  2  5  6 } Compara (j>i)=(3>4)=true
{ 1  3  4  2  5  6 } Intercambiar: true
------------------------------------

>    i     k         Asigna: i=3 k=2
{ 1  3  4  2  5  6 } Compara (j>i)=(2>3)=true
{ 1  2  4  3  5  6 } Intercambiar: true
------------------------------------

>    i        k      Asigna: i=2 k=5
{ 1  2  4  3  5  6 } Compara (j>i)=(5>2)=false
{ 1  2  4  3  5  6 } Intercambiar: false
------------------------------------

>    i           k   Asigna: i=2 k=6
{ 1  2  4  3  5  6 } Compara (j>i)=(6>2)=false
{ 1  2  4  3  5  6 } Intercambiar: false
------------------------------------

>       i  k         Asigna: i=4 k=3
{ 1  2  4  3  5  6 } Compara (j>i)=(3>4)=true
{ 1  2  3  4  5  6 } Intercambiar: true
------------------------------------

>       i     k      Asigna: i=3 k=5
{ 1  2  3  4  5  6 } Compara (j>i)=(5>3)=false
{ 1  2  3  4  5  6 } Intercambiar: false
------------------------------------

>       i        k   Asigna: i=3 k=6
{ 1  2  3  4  5  6 } Compara (j>i)=(6>3)=false
{ 1  2  3  4  5  6 } Intercambiar: false
------------------------------------

>          i  k      Asigna: i=4 k=5
{ 1  2  3  4  5  6 } Compara (j>i)=(5>4)=false
{ 1  2  3  4  5  6 } Intercambiar: false
------------------------------------

>          i     k   Asigna: i=4 k=6
{ 1  2  3  4  5  6 } Compara (j>i)=(6>4)=false
{ 1  2  3  4  5  6 } Intercambiar: false
------------------------------------

>             i  k   Asigna: i=5 k=6
{ 1  2  3  4  5  6 } Compara (j>i)=(6>5)=false
{ 1  2  3  4  5  6 } Intercambiar: false
</pre>
"

Insertion sort, ¿Ventajas y desventajas?;"
<pre>
-Ventajas
  -Es facil de implementar
  -Baja complejidad, ideal para colecciones pequeñas`
-Desventajas
  -Tiene una notacion B(n^2) (exponencial) (muy poco eficiente)
</pre>
"


Insertion sort, ¿Que tipo de notacion BigO tiene? y ¿Codigo?;"
¿Que tipo de notacion BigO tiene?. 
  Tiene una O(n^2) ya que si nos fijamos en el codigo tenemos un doble
  ciclo, de tal forma que recorrera todo el arreglo.

<pre>
func main() {
  aOrdenar := []int{5, 3, 2, 4, 1}
  for i := 0; i < len(aOrdenar); i++ {
    aInsertar := aOrdenar[i] // 9
    for j := 0; j < len(aOrdenar); j++ {
      aComparar := aOrdenar[j] // 0
      inter := aComparar > aInsertar
      if inter {
        temp := aComparar
        aComparar = aInsertar
        aInsertar = temp
        aOrdenar[i] = aInsertar
        aOrdenar[j] = aComparar
      }
      //printArregloConAsignaciones(aOrdenar, i, j, inter)
    }
  }
  fmt.Println(aOrdenar)
}
</pre>
"
```

Implemntacion del algoritmo insertion sort

Version comprimida

```go
func main() {
  aOrdenar := []int{5, 3, 2, 4, 1}
  for i := 0; i < len(aOrdenar); i++ {
    for j := i; j < len(aOrdenar); j++ {
      if aOrdenar[j] > aOrdenar[i] {
        swap(&aOrdenar, i, j)
      }
    }
  }
  fmt.Println(aOrdenar)
}

// usar un arreglo como puntero, arrey como puntero, arreglo puntero arreglo
// pasar un arreglo como puntero, enviar arreglo como puntero, uso de puntero en arreglo
func swap(arr *[]int, posA int, posB int) {
  valorPosA := (*arr)[posA]
  valorPosB := (*arr)[posB]
  (*arr)[posA] = valorPosB
  (*arr)[posB] = valorPosA
}
```

Version que imprime como se va haciendo el proceso

```go
// copiar arreglo
func copy(arr []int) []int {
  copiado := []int{}
  for _, v := range arr {
    copiado = append(copiado, v)
  }
  return copiado
}

func main() {
  operaciones := 0
  aOrdenar := []int{1, 4, 3, 2, 5, 6}
  for i := 0; i < len(aOrdenar); i++ {
    for j := i; j < len(aOrdenar); j++ {
      antesDeOrdenar := copy(aOrdenar)
      inter := aOrdenar[j] < aOrdenar[i]
      if inter {
        swap(&aOrdenar, i, j)
      }
      operaciones++
      printArregloConAsignaciones(&aOrdenar, i, j, inter, &antesDeOrdenar)
    }
  }
  fmt.Println(aOrdenar, "Operaciones", operaciones)
}

func swap(arr *[]int, posA int, posB int) {
  valorPosA := (*arr)[posA]
  valorPosB := (*arr)[posB]
  (*arr)[posA] = valorPosB
  (*arr)[posB] = valorPosA
}

func printArregloConAsignaciones(despues *[]int, i int, k int, inter bool, antes *[]int) {
  if i == k {
    return
  }
  print("> ")
  for z := range *antes {
    if z == i {
      print("i  ")
    } else if z == k {
      print("k  ")
    } else {
      print("   ")
    }
  }
  fmt.Printf(" Asigna: i=%v k=%v", (*antes)[i], (*antes)[k])
  println()
  print("{")
  for _, v := range *antes {
    print(" ", v, " ")
  }
  print("}")
  fmt.Printf(" Compara (j>i)=(%v>%v)=%v\n", (*antes)[k], (*antes)[i], inter)

  print("{")
  for _, v := range *despues {
    print(" ", v, " ")
  }
  print("}")
  fmt.Printf(" Intercambiar: %v", inter)
  println()
  print("------------------------------------")
  println()
  println()
}

```

#### Selection Sort

Implementacion Comprimida

```go
func main() {
  operaciones := 0
  a_ordenar := []int{1, 4, 3, 2, 5, 6}
  for i := 0; i < len(a_ordenar); i++ {
    posicion_minimo := i
// CUIDADO VER QUE ES "i+1" para no tocar la parte ordenada
    for j := i + 1; j < len(a_ordenar); j++ {
// CUIDADO CON HACER LA COMPARACION CORRECTAMENTE
      inter := a_ordenar[posicion_minimo] > a_ordenar[j]
      if inter {
        posicion_minimo = j
      }
      operaciones++
    }
    swap(&a_ordenar, posicion_minimo, i)
  }
  fmt.Println(a_ordenar, "Operaciones", operaciones)
}

func swap(arr *[]int, posA int, posB int) {
  valorPosA := (*arr)[posA]
  valorPosB := (*arr)[posB]
  (*arr)[posA] = valorPosB
  (*arr)[posB] = valorPosA
}
```

```r
Selection Sort, ¿Como funciona?, ¿Diferencia con Insertion sort?;"
<pre class='prefor'>
¿Diferencia con 'Insertion sort'?
  El 'Insertion sort', en tanto la comparacion detecta que la posicion
  actual es menor, intercambia posiciones. En el este caso primero
  busca el minimo en todas las posiciones y al final hace el intercambio

¿Como funciona?
  1. En la 1ra posicion se pone el numero menor que se encuentre
  2. En la 2ra posicion se pone el numero menor que se enuentre
  3. En la 3ra posicion se pone el numero menor que se enuentre
  Asi sucesivamente hasta llegar a la ultima posicion.
<pre>

Ejemplo 1
<pre>
> i
{ 1  9  8  2  7  3  0  6  4  5 } Minimo encontrado: 0
{ 0  9  8  2  7  3  1  6  4  5 }
------------------------------------

>    i
{ 0  9  8  2  7  3  1  6  4  5 } Minimo encontrado: 1
{ 0  1  8  2  7  3  9  6  4  5 }
------------------------------------

>       i
{ 0  1  8  2  7  3  9  6  4  5 } Minimo encontrado: 2
{ 0  1  2  8  7  3  9  6  4  5 }
------------------------------------

>          i
{ 0  1  2  8  7  3  9  6  4  5 } Minimo encontrado: 3
{ 0  1  2  3  7  8  9  6  4  5 }
------------------------------------

>             i
{ 0  1  2  3  7  8  9  6  4  5 } Minimo encontrado: 4
{ 0  1  2  3  4  8  9  6  7  5 }
------------------------------------

>                i
{ 0  1  2  3  4  8  9  6  7  5 } Minimo encontrado: 5
{ 0  1  2  3  4  5  9  6  7  8 }
------------------------------------

>                   i
{ 0  1  2  3  4  5  9  6  7  8 } Minimo encontrado: 6
{ 0  1  2  3  4  5  6  9  7  8 }
------------------------------------

>                      i
{ 0  1  2  3  4  5  6  9  7  8 } Minimo encontrado: 7
{ 0  1  2  3  4  5  6  7  9  8 }
------------------------------------

>                         i
{ 0  1  2  3  4  5  6  7  9  8 } Minimo encontrado: 8
{ 0  1  2  3  4  5  6  7  8  9 }
------------------------------------

>                            i
{ 0  1  2  3  4  5  6  7  8  9 } Minimo encontrado: 9
{ 0  1  2  3  4  5  6  7  8  9 }
------------------------------------

[0 1 2 3 4 5 6 7 8 9] Operaciones 45
</pre>
"

Selection Sort, ¿Codigo y Notacion BigO?;"
Notacion BigO: O(n^2) Exponencial, un poco mas eficiente que 'Insertion sort'
ya que nos ahorramos intercambios

<pre>
Codigo 
func main() {
  operaciones := 0
  a_ordenar := []int{1, 4, 3, 2, 5, 6}
  for i := 0; i < len(a_ordenar); i++ {
    posicion_minimo := i
// CUIDADO VER QUE ES `i+1` para no tocar la parte ordenada
    for j := i + 1; j < len(a_ordenar); j++ {
// CUIDADO CON HACER LA COMPARACION CORRECTAMENTE
      inter := a_ordenar[posicion_minimo] > a_ordenar[j]
      if inter {
        posicion_minimo = j
      }
      operaciones++
    }
    swap(&a_ordenar, posicion_minimo, i)
  }
  fmt.Println(a_ordenar, `Operaciones`, operaciones)
}

func swap(arr *[]int, posA int, posB int) {
  valorPosA := (*arr)[posA]
  valorPosB := (*arr)[posB]
  (*arr)[posA] = valorPosB
  (*arr)[posB] = valorPosA
}
</pre>
"
```

#### Bubble Sort

```r
Bubble Sort, ¿Descripcion, Notacion BigO, Ventajas/Desventajas?;"
Descripcion
  Este va comparando por pares y moviendo a la derecha, al mayor o al
  menor segun sea el caso, lo cual en cada iteracion el ultimo elemento
  quedara ordenado.

Notacion BigO
  O(n^2)

Ventajas/Desventajas:
  Ideal para conjuntos pequeño ya ordenados que por alguna razon tienen
  elementos desordenados, pero no es eficiente para grandes conjuntos
  ordenados.

Animacion
  ![Bubble Sort](./img/01_Bubble_Sort.gif)
"


Bubble Sort, ¿Codigo?;"<pre>
  package main

  func main() {
    to_order := []int{10, 1, 9, 8, 2, 7, 3, 0, 6, 4, 5, 0}

//LAS ITERACIONES DE DETENDRAN CUANDO NO SE HAFAN INTERCAMBIOS
    wasThereSwap := true

//LOS ULTIMOS ELEMENTOS ESTARAN ORDENADOS, POR LO QUE EVITAMOS QUE
//SE COMPAREN ESTOS
    lastPositionsOrdered := 0

    for wasThereSwap {
      wasThereSwap = false
      for i := 0; i < len(to_order)-1-lastPositionsOrdered; i++ {
        if to_order[i] > to_order[i+1] {
// SE INTERCAMBIARA EL IZQ POR EL DER
          swap(&to_order, i)
          wasThereSwap = true
        }
      }
      if wasThereSwap {
        lastPositionsOrdered++
      }
    }
    fmt.Println(to_order) // [0 0 1 2 3 4 5 6 7 8 9 10]
  }

  func swap(to_order *[]int, iIzq int) {
    iDer := iIzq + 1
    temp := (*to_order)[iIzq]
    (*to_order)[iIzq] = (*to_order)[iDer]
    (*to_order)[iDer] = temp
  }
</pre>"

```

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

## %%%%%%%%%%% WEB BASICO %%%%%%%%%%%

Fuente: [Udemy](https://www.udemy.com/course/golang-en-la-web-ajax-json-postgresql-bootstrap-5)

### Basicos Web

```r
Crear un servidor web simple;"
Solo necesitamos `http.ListenAndServe` y dentro de este definir el
puerto. Y podemos agregar handlers con `http.HandleFunc(`

<pre>
  import ( `fmt` `net/http` )
  func main() {

    http.HandleFunc(`/`, func(w http.ResponseWriter, r *http.Request) {
      fmt.Fprint(w, `pagina principal`)
    })

// AL PASAR NIL, DECIMOS QUE USARA EL HTTP HANDLER POR DEFAULT,
// EXISTEN MULTIPLES COMO GORILLA/MUX, GIN. ETC.
+   http.ListenAndServe(`:8282`, nil)
  }
</pre>

Resultado: Tenemos hola mundo de un servidor web.
"

## agregar header, incluir header
Agregar cabezera en respuesta;"<pre>
...(w http.ResponseWriter, r *http.Request) {
  w.Header().Set(`Content-type`,`text/html`)
  ...
</pre>"

## obtener parmetro, obtener argumento de la url, obtener variable de la url, obtener info de la url
Obtener la url y sus parametros en un sev web EJ.´ej.com/parametros?id=1´;"
Estamos capturando, el endpoint, los parametros como map, un parametro por su nombre
<pre>
http.HandleFunc(`/parametros`, func(w http.ResponseWriter, r *http.Request) {

  fmt.Println(`URL:`, r.URL)                             // '/parametros?id=1'
  fmt.Println(`Parametros:`, r.URL.RawQuery)             // 'id=1' (es un map)
  fmt.Println(`Parametro id:`, r.URL.Query().Get(`id`))  // '1'
})
</pre>
"

Obtener parametros en la url ej: ´ej.com/productos/1´;"
<p>En este caso usamos Mux</p>

1. Para indicar que parte de la url sera parametro indicamos con
<pre>
  r.HandleFunc(`/producto/{id}`, request_handlers.CategoriaEditForm)
</pre>

2. Obtenemos de la url
<pre>
  func CategoriaEliminar(w http.ResponseWriter, r *http.Request) {
    id := mux.Vars(r)[`id`]
</pre>

"

Crear url de forma dinamica;"
Puede que querramos crear una url, por ej. definir una url a la que
le enviaremos peticiones.

<pre>
import `net/url`

func main() {
  // definir protocolo, host y endpint
  url, errorUrl := url.Parse(`/usuarios`)
  if errorUrl != nil {
    panic(`Error al crear url`)
  }
  url.Host = `169.0.0.1:8383`
  url.Scheme = `http`

  // definir paramtetros
  params := url.Query()
  params.Add(`id`, `5`)
  params.Add(`orderBy`, `date_asc`)
  url.RawQuery = params.Encode()

  print(url.String()) // http://169.0.0.1:8383/usuarios?id=5&orderBy=date_asc
}
</pre>
"

Hacer una peticion http a servidor programaticamente;"
Debemos de tener un servidor que sirva en la url que estamos apuntando,

<pre>
//- Resultado:
//  - Url: http://localhost:8282/parametros?id=4
//  - Status:  200 OK
//  - Body:  productos, resaltando el id:4

import (
  urlUtils `hola_go/web/02_crearUrl`
  `io/ioutil`
  `net/http`
)

func main() {
  // Para generar por Ej. http://localhost:8282/parametros?id=4
  url := urlUtils.UrlInfo{
    Protocol: `http`,
    Host:     `localhost:8282`,
    Endpoint: `parametros`,
    Params:   map[string]string{`id`: `4`},
  }
  requestProductos, errorReqProd := http.NewRequest(`GET`, url.String(), nil)
  if errorReqProd != nil {
    panic(`Error al crear peticion`)
  }

  println(`Url:`, url.String())
  client := &http.Client{}
  responseProductosRef, errorRespProd := client.Do(requestProductos)
  if errorRespProd != nil {
    panic(`Error: Error al hacer peticion productos\nCause:` + errorRespProd.Error())
  }
  println(`Status: `, responseProductosRef.Status)

  responseProductosBytes, err := ioutil.ReadAll(responseProductosRef.Body)
  if err != nil {
    panic(`Error al leer peticion: ` + err.Error())
  }
  println(`Body: `, string(responseProductosBytes))
}
</pre>
"
```

#### Crear servidor web estatico con 2 lineas

levantar servidor web estatico, servidor estatico

```go
package main
import ( "log" "net/http" )
func main() {
    http.Handle("/", http.FileServer(http.Dir(".")))
    log.Fatal(http.ListenAndServe(":8080", nil))
}
```

#### Ejemplo: crear servidor web y hacerle peticion programaticamente

Objetivo: Vamos a crear 2 programas uno que desplegara el servidor web,
y otro que le hara una peticion y la mostrara, para esto crearemos una
libreria, para que nuestro main solo tenga logica de negocio.

A. Creamos Servidor

```go
package main
import ( "fmt" "net/http" )
func main() {
  http.HandleFunc(`/productos`, func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprint(w, `mostrando producto: `, r.URL.Query().Get("id"))
  })
  http.ListenAndServe(`:8282`, nil)
}
```

B. Creamos logica, para hacer la peticion, la cual usara la libreria que
vamos a crear mas abajo.

```go
package main
import ( Request "hola_go/web/02_Request" )
func main() {
  url := Request.RequestInfo{
    Protocol: "http",
    Host:     "localhost:8282",
    Endpoint: "productos",
    Params:   map[string]string{"id": "2"},
  }

  response, error1 := url.DoRequest()
  if error1 != nil {
    panic("Error al hacer peticion" + error1.Error())
  }

  print(response.Body)
}

```

C. Creamos libreria, para nuestra logica, que consistira en dos structs,
un que tendra la info de la peticion `RequestInfo` y otra que tendra
la info de la respuesta `Response`.

```go
package Request

import (
  "io/ioutil"
  "net/http"
  "net/url"
)

type RequestInfo struct {
  Protocol string
  Host     string
  Endpoint string
  Params   map[string]string
}

type Response struct {
  Status string
  Body   string
}

func (urlInfo *RequestInfo) String() string {
  // definir protocolo, host y endpint
  url, errorUrl := url.Parse(urlInfo.Endpoint)
  if errorUrl != nil {
    panic(`Error al crear url`)
  }
  url.Host = urlInfo.Host
  url.Scheme = urlInfo.Protocol

  // definir paramtetros
  params := url.Query()
  for name, value := range urlInfo.Params {
    params.Add(name, value)
  }
  url.RawQuery = params.Encode()

  return url.String()
}

func (urlInfo *RequestInfo) DoRequest() (*Response, error) {
  response := Response{}

  urlStr := urlInfo.String()
  requestProductos, errorReqProd := http.NewRequest(`GET`, urlStr, nil)
  if errorReqProd != nil {
    return nil, errorReqProd
  }

  println(`Url:`, urlStr)
  client := &http.Client{}
  responseProductosRef, errorRespProd := client.Do(requestProductos)
  if errorRespProd != nil {
    return nil, errorRespProd
  }

  responseProductosBytes, err := ioutil.ReadAll(responseProductosRef.Body)
  if err != nil {
    return nil, err
  }

  response = Response{
    Status: responseProductosRef.Status,
    Body:   string(responseProductosBytes),
  }

  return &response, nil
}
```

**Resultado:** Creamos un servidor y le hicimos una peticion todo desde
programacion.

#### 23. Usar Fresh para recargar el servidor

```r
Libreria para reiniciar el servidor al cambiar codigo, y su instalacion;"
Instalacion: `go get github.com/pilu/fresh` <br />

Uso: En la capeta de nuestro main de nuestro servidor usamos `fresh` <br />

<br />

Da una salida como esta y ya cada vez que guardamos, deberiamos de poder
el cambio al refrescar el navegador.

<pre>
19:53:38 runner      | InitFolders
19:53:38 runner      | mkdir ./tmp
19:53:38 runner      | mkdir ./tmp: Cannot create a file when that file already exists.
19:53:38 watcher     | Watching .
19:53:38 main        | Waiting (loop 1)...
</pre>
"
```

#### 24. Soporte a templates HTML

```r
## traer archivos html, incluir archivos html, responder con html, enviar html,
## servir html como template, servir template, enviar template, responder con template
Usar templates html (se inspeccionaran por codigo);"
<pre class='preform'>
1. Importamos `html/template`
2. Traemos html 'view, err := template.ParseFiles(`public/productos.html`)'
  2.1. Tambien podemos usar 'viewModel := template.Must(template.ParseFiles(`public/productos.html`))'
  para que controle el error por nosotros.
3. Mostramos vista: 'view.Execute(w, nil)'

Nota: La carpeta donde se guarda el html puede tener cualquier nombre
</pre>

Ejemplo

<pre>
func HomeHandler(w http.ResponseWriter, r *http.Request) {
  viewModel := template.Must(
    template.New(`home.html`).ParseFiles(
// EN ESTE CASO AGREGAMOS EL ARCHIVO CON LOS INCLUDES TAMBIEN
// PERO NO ES OBLIGATORIO
      `html/templates/home.html`,
      `html/templates/includes.html`),
  )
  viewModel.Execute(w, nil)
}
</pre>
"
```

- Rutas:
  - ./main_soporte_de_html.go
  - ./public/index.html
  - ./public/productos.html

```go
package main

import (
  "html/template"
  "net/http"
)

func main() {
  http.HandleFunc(`/productos`, func(w http.ResponseWriter, r *http.Request) {
    view, err := template.ParseFiles(`public/productos.html`)
    if err != nil {
      panic("Error al traer html")
    }
    view.Execute(w, nil)
  })
  http.HandleFunc(`/`, func(w http.ResponseWriter, r *http.Request) {
    view, err := template.ParseFiles(`public/index.html`)
    if err != nil {
      panic("Error al traer html")
    }
    view.Execute(w, nil)
  })
  http.ListenAndServe(`:8282`, nil)
}
```

Resultado: Podemos entrar a dihas rutas y nos entregara el html

#### 25. Usar Variables, condicionales, bucles e includes(header/footer) en el Template

##### 25. Ankis

```r
#obtener variable de template, uso de variables en templates
Como pasar/usar variables entre go y html;"
Lo hacemos desde un struct, en `view.Execute(w, modelo)` donde modelo
tenemos toda la info que queremos usar, y '\<h2\>{{.Nombre}}\</h2\>' para
usarla en el html.
"

Cuales son las comparaciones y sus significados ej. `gt` y su uso en los templates;"
<pre class='preform'>
  `eq`: iguales
  `ge`: Mayor o igual
  `gt`: mayor que
  `le`: meno r o igual que
  `lt`: menor que
  `ne`: diferente
</pre>

Uso

<pre>
{{if .Disponible}}
  (div)Cantidad 5(/div)
{{else}}
  (div)Producto Agotado(/div)
{{end}}
</pre>
"

Como usar condicionales(simplres, comparacion, compuestos) en html;"
Condicional simple (uso de variable booleano)

<pre>
{{if .Disponible}}
  \<div\>Cantidad 5\</div\>
{{else}}
  \<div\>Producto Agotado\</div\>
{{end}}
</pre>

Condicional comparaciones

<pre>
{{if (gt .Edad 17)}}
  \<div\>Es mayor de edad\</div\>
{{else}}
  \<div\>Menor de edad\</div\>
{{end}}
</pre>

Condicionales compuestos

<pre>
{{if (and (lt .Edad 17) (eq .Nombre `Admin`) ) }}
  \<div\>Admin de niños\</div\>
{{else}}
  \<div\>admin adulto\</div\>
{{end}}
</pre>
"

Como hacer un bucle de tipos primitivos en un template, iterar en template, iterar dentro de template;"
Con el uso de `{{range .MiArr}}` por ej. si queremos imprimir los tags de un producto,
deberemos de pasar estos por el modelo.

Ejemplo
<pre>

tags := []string{ `Jugete antiguo`, `Artesania`, `Tradicional`, `Torneado`, }
producto := Producto{1, `Trompo chico`, 20.0, true, tags}
view.Execute(w, producto)

==========================================

{{range .Tags}}
  \<li\>{{.}}\</li\>
{{end}}

</pre>
"

Como hacer un bucle de structs en un template;"
Con el uso de range por ej. '{{range .Comentarios}}' y dentro de este
al iniciar con un punto '.' podremos acceder a sus propiedades

Ejemplo: Para imprimir un arreglo de structs comentarios, el cual tiene
como propiedad cuerpo, fecha etc. podemos hacer lo sig.

<pre>

comentarios := []Comentario{
  {1, 1, `Exelente`, time.Now()},
  {2, 1, `Los hacen ahi mismo`, time.Now()},
  {3, 1, `Muy buenos`, time.Now()},
}
producto := Producto{1, `Trompo chico`, 20.0, false, tags, comentarios}
viewModel := template.Must(template.ParseFiles(`public/productos.html`))
viewModel.Execute(w, producto)

==========================================

  \<ul\>
{{range .Comentarios}}
    \<li\>{{.Cuerpo}}\</li\>
{{end}}
  \</ul\>

</pre>
"

Como usar una funcion en un template;"
<p>
  RECORDATORIO: La diferencia entre funcion y metodo, es que el metodo
  esta ligado a un objeto/tipo, mientras que la funcion no esta atada
  a ningun objeto.
<p>

<p>
  Debemos de crear un mapa de funciones con 
  'myFuncs := template.FuncMap{``:GetSome}', y este meterlo a nuestro
  view model con 'template.New(`productos.html`).Funcs(myFuncs).'
</p>

<p>
Por ejemplo: podemos tener una funcion que nos de si hay avisos, y otra
que nos de el menu, para eso 
</p>

<pre>
...Handeler(...) {
  myFuncs := template.FuncMap{ `GetAviso`: GetAviso, `GetMenu`:  GetMenu, }
  viewModel := template.Must( template.New(`productos.html`).Funcs(myFuncs).ParseFiles(`public/productos.html`) )
  viewModel.Execute(w, producto)
}

=========== Funciones

func GetAviso() string { return `Oferta en lapiseros` }

func GetMenu(rol string) []string {
  if rol == `admin` { return []string{...} }
  else { return []string{...} }
}

================================================ USO

\<div style=`background-color: lightgrey; max-width: 400px;`\>Aviso: {{GetAviso}}\</div\>
\<h3\>Menu\</h3\>
\<ul\>
{{range (GetMenu `admin`)}}
    \<li\><a href=`#`\>{{.}}, \</a\>\</li\>
{{end}}
\</ul\>

</pre>
"

Como usar includes en html, por ej. para el header/footer repetitivo;"
<p>
  Para crearlos es dentro del html con `{{define 'nombre'}}..mihtml..{{end}}`
  y para usarlos es '{{template `nombre`}}'.
</p>
<p>
  Pero comunmente vamos a querer que esten en su propio archivo, asi
  que deberemos de agregar 2 archivos al crear nuestro viewmodel es
  decir 'ParseFiles(`...html`, `.../includes.html`),'.
</p>
<p>
  Por ej. Si queremos crear un template para header y footer, tendremos
  que 1, crear el archivo includes.html con los templates, y luego
  incluirlo en la pagina que querramos mostrar
</p>

==============include.html==============

<pre>
  {{define `header`}}
  \<header\>
    \<div style=`background-color: lightgrey; max-width: 400px;`\>Aviso: {{GetAviso}}\</div\>  
    \<h3\>Menu\</h3\>
    \<ul\>
      {{range (GetMenu `admin`)}}
        \<li\>\<a href=`#`\>{{.}}, \</a\>\</li\>
      {{end}}
    \</ul\>
  \</header\>
  {{end}}


  {{define `footer`}}
  \<footer style=`background-color: lightgrey;`\>
    \<h3\>Hola desde el footer\</h3\>
  \</footer\>
  {{end}}
</pre>

==============main.go==============

<pre>
viewModel := template.Must(
  template.New(`productos.html`).
    Funcs(myFuncs).
    ParseFiles(`public/productos.html`, `public/includes.html`),
)
viewModel.Execute(w, producto)
</pre>
"

#payload a struct, peticion a struct, cuerpo de peticion a struct, cuerpo de la peticion a struct
#obtener payload json y convertirlo a struct
Convertir un payload json de una peticion a un struct;"<pre>

func (app *ApiApp) myHandler(w http.ResponseWriter, r *http.Request) {
  var myPayload MyStruct
  err := json.NewDecoder(r.Body).Decode(&myPayload)
  if err != nil { panic(´something in 'payload to struct' was wrong´) }
}

</pre>"

#uso de formulario html
Obtener formulario html y obtener algun dato;"<pre>

// este handler debe ser el que consulte el formulario html
func (app *application) PaymentSucceeded(w http.ResponseWriter, r *http.Request) {
  err := r.ParseForm()
  if err != nil { app.errorLog.Println(err) return }

  elValor := r.Form.Get(`algun_valor`)
}

Recordar que `algun_valor` debe considir con el name del input del form
</pre>"
```

##### 25. Practico

**Objetivo:** Vamos a crear una pagina que solo servira la ruta "/",
pero en esta tendremos uso de todos los temas dichos, que seria.

- Variables: Para mostrar el nombre de algun producto
- condicionales: Para imprimir una u otro html
- bucles: Para imprimir una coleccion de primitivos y complejos
- includes: Para reutilizar el html del footer y header

Codigo: [Usar variables condi loops e includes](./img/_1_usar_variables_condi_loops_includes.zip)

A. Main.go

```go
package main

import (
  "html/template"
  "net/http"
  "time"
)

type Producto struct {
  IdProducto  int
  Nombre      string
  Precio      float32
  Disponible  bool
  Tags        []string
  Comentarios []Comentario
}

type Comentario struct {
  IdComentario int
  FkProducto   int
  Cuerpo       string
  Fecha        time.Time
}

func main() {
  http.HandleFunc(`/`, productosHandler)
  http.ListenAndServe(`:8282`, nil)
}

func productosHandler(w http.ResponseWriter, r *http.Request) {
  tags := []string{
    `Jugete antiguo`,
    `Artesania`,
    `Tradicional`,
    `Torneado`,
  }
  comentarios := []Comentario{
    {1, 1, `Exelente`, time.Now()},
    {2, 1, `Los hacen ahi mismo`, time.Now()},
    {3, 1, `Muy buenos`, time.Now()},
  }
  producto := Producto{1, `Trompo chico`, 20.0, false, tags, comentarios}

  myFuncs := template.FuncMap{
    `GetAviso`: GetAviso,
    `GetMenu`:  GetMenu,
  }

  viewModel := template.Must(
    template.New(`productos.html`).
      Funcs(myFuncs).
      ParseFiles(`public/productos.html`, `public/includes.html`),
  )

  viewModel.Execute(w, producto)
}

func GetAviso() string {
  return `Oferta en lapiseros`
}

func GetMenu(rol string) []string {
  if rol == `admin` {
    return []string{`usuarios`, `pedidos`, `roles`}
  } else {
    return []string{`carrito`, `blog`, `about`}
  }
}
```

B. productos.html

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset='utf-8'>
  <title>Productos</title>
</head>
<body>
  {{template "header"}}
  <h1>Productos</h1>
  <h2>{{.Nombre}}</h2>
  
  {{if .Disponible}}
    <p>Disponible</p>
  {{else}}
    <p>Agotado</p>
  {{end}}

  <h3>Tags</h3>
  <ul>
    {{range .Tags}}
      <li>{{.}}</li>
    {{end}}
  </ul>


  <h3>Comentarios</h3>
  <ul>
    {{range .Comentarios}}
      <li>{{.Cuerpo}}</li>
    {{end}}
  </ul>
  
  {{template "footer"}}
</body>
</html>
```

includes.html

```html
{{define "header"}}
<header>
  <div style="background-color: lightgrey; max-width: 400px;">Aviso: {{GetAviso}}</div>  
  <h3>Menu</h3>
  <ul>
    {{range (GetMenu "admin")}}
      <li><a href="#">{{.}}, </a></li>
    {{end}}
  </ul>
</header>
{{end}}


{{define "footer"}}
<footer style="background-color: lightgrey;">
  <h3>Hola desde el footer</h3>
</footer>
{{end}}
```

Resultado: ![_1_usar_variables_condi_loops_includes](./img/_1_usar_variables_condi_loops_includes.png)

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

### Proyecto basico

#### Servir archivos estaticos

```r
Servir archivos estaticos html (no ser usados como templates);"<pre>
// en este podemos servir sin problemas un proyecto react, svelte etc.
// CUIDADO: Con la ruta debemos de tomar el punto de partida la raiz
// de nuestro proyecto, no importa si el main esta un poco mas adentro.
import ( `log` `net/http` )
func main() {
  http.Handle(`/`, http.FileServer(http.Dir(`./static`)))
  log.Fatal(http.ListenAndServe(`:8081`, nil))
}
</pre>"
```

#### Estructurar por templates

```r
Estructurar html para mostrar por templates y no archivos;"
<p>
  De esta forma podemos manejar nuestro html con puro template, y cuando
  inicia el programa los templates se cargan en memoria, y no en cada
  peticion. 
</p>
<p>
  Primero metemos toda la pagina dentro de un template Con <i>{{define 'home'}} 
  {{end}}</i>, Y segundo lo llamamos <i>viewModels.ExecuteTemplate(w, `nomTemplate`, nomModel)</i>
</p>
<br />
Ejemplo
<br />
Crear nuestro template ej. 'home.html'
<br />
<pre>
+ {{define 'home'}}
  \<html\>
  \<head\>
    \<title\>Productos\</title\>
  \</head\>
  \<body\>
    {{template 'header'}}
    \<h1\>Home\</h1\>
    
    {{template 'footer'}}
  \</body\>
  \</html\>
  {{end}}
</pre>

Incluimos la plantilla en nuestros viewModels

<pre>
  var viewModels = template.Must(
+ template.New(`T`). // indicamos que no es uno en especifico
      Funcs(myFuncs).
      ParseFiles(
        `public/productos.html`,
        `public/carrito.html`,
        `public/about.html`,
        `public/home.html`,
        `public/includes.html`,
    ),
  )

  func homeHandler(w http.ResponseWriter, r *http.Request) {
// DE ESTA MANERA LLAMAMOS AL TEMPLATE nil=MODELO
+   viewModels.ExecuteTemplate(w, `home`, nil)
  }
</pre>
"

Incluir todos los templates de una carpeta y sus subcarpetas;"
Podemos encontrarnos con el problema de que podemos tener cientos o
miles de plantillas, entonces seria un problema declarar una por una,
asi que con <i>ParseGlob(`misTemplates/**/*.html`)</i>

<pre>
// 1. Definimos template
{{define 'home'}} ..MI-HTML.. {{end}}

// 2. Incluimos todos los templates
var viewModels = template.Must(
template.New(`T`).
      Funcs(myFuncs).ParseGlob(
// TODOS LOS TEMPLATES DENTRO DE PUBLIC SERAN AGREGADOS
    `public/**/*.html`,
  ),
)

// 3. Podemos usar nuestro templates
func homeHandler(w http.ResponseWriter, r *http.Request) {
  w.Header().Set(`Content-type`, `text/html`)
  viewModels.ExecuteTemplate(w, `home`, nil)
}
</pre>
"

Manejo de errores, Mostrar pagina de error;"
<p>
  Podemos definir su propio template o archivo y mostrar este, en este
  caso usamos un archivo (No un template)
</p>

CUIDADO: Si usamos un archivo (no template) este no debe terner sintaxis
de template, por que causara problemas

<pre>
// Pude estar y tener el nombre que sea
var viewError5xx = template.Must(template.ParseFiles(`.../5xx.html`))

func showTemplate(w *http.ResponseWriter, templateName string, model interface{}) {
  err := viewModels.ExecuteTemplate(*w, templateName, model)
  if err != nil {
    (*w).WriteHeader(500)
    viewError5xx.Execute(*w, nil)
  }
}
</pre>
"
```

#### Agregar Js y Css a templates desde archivos externos

```r
Agregar Js y Css a templates desde archivos externos;"
<p>
  Usamos el FileServer y le extraemos la ruta para que cuando los
  invoquemos no tengamos que indiacar la ruta.
</p>

Agregarlos desde Go

<pre>
// poner antes ListenAndServe.
// LAS RUTAS LAS TOMARA ABSOLUTAS, O SI SON RELATIVAS, LAS TOMARA COMO
// PUNTO DE PARTIDA NUESTRO ARCHIVO MAIN.EXE
// CUIDADO: FIJARSE SI TENEMOS NUESTRO .EXE EN NUESTRO PROYECTO, YA QUE LUEGO
// SE GENERA EN OTRAS CARPETAS, LO QUE HACE QUE NO TOME LAS RUTAS.
var estaticos = http.FileServer(http.Dir(`public/statics`))
// `/public/statics/ ES LA RUTA QUE PONDREMOS EN EL HTML PARA ACCEDER A LA CARPETA, ASI QUE PODEMOS PONER PEPITO
http.Handle(`/public/statics/`, http.StripPrefix(`/public/statics/`, estaticos))
</pre>

Uncluirlos en nuestro html

<pre>
\<link rel='stylesheet' type='text/css' href='public/statics/css/global.css'\>\</head\>
\<body\>
...
  \<script src='public/statics/js/about.js'\>\</script\>
\</body\>
</pre>
"
```

#### CODIGO Proyecto basico

![_2_servir_carpeta_de_templates](.img/_2_servir_carpeta_de_templates.zip)

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

### Sockets

#### Codigo Sockets, Chat simple

Objetivo: El chat solo nececesitara que se ingrese un username, y si
ya existe uno igual no podra entrar, los mensajes que se veran solo
seran a partir de que el usuario ingrese.

[Codigo](./img/_3_chat_simple.zip)

Resultado

![Chat simple](./img/_3_chat_simple.gif)

- FrontEnd

  - Requisitos
    - JQuery
    - Bootstrap

  - Cosas a notar
    - Peticin `login` checa que no se repita el username
    - peticion `susbribe_chat` sucribe al usuario para enviar mensajes

```html
<!DOCTYPE html>
<html>
<head>
  <title>Mi Chat</title>
  {{template "head"}}
<style>
body { height: 100vh; overflow-y: hidden; }
main { height: 100%; display: flex; flex-direction: column; }
##form_registro { margin: auto; display: contents; width: 90%;
  max-width: 600px; margin-bottom: 2rem;}
##container_chat { flex-grow: 1; display: none; }
##container_chat textarea { height: 50%; width: 100%; }
</style></head>
<body>
  <h1>Mi Chat</h1>
  <main class="container">
    <form id="form_registro" action="#">
      <input type="text" class="form-control" id="input_username" placeholder="Nombre de usuario" autofocus autocomplete="off">
      <div id="msg_error" style="color: red;"></div>
      <input type="submit" value="Ingresar" id="btn_ingresar" class="btn btn-primary" />
    </form>

    <div id="container_chat">
      <textarea class="form-control" disabled id="messages"></textarea>
      <form class="input-group" id="form_chat">
        <span id="lb_usrname" class="input-group-text"></span>
        <input id="input_msg" type="text" placeholder="Nuevo Mensaje" autocomplete="off" class="form-control" />
        <button  type="button" class="btn btn-primary">Enviar</button>
      </form>
    </div>
  </main>
  {{template "global_js"}}
<script>
$(function() {
  var connChat;
  $('#form_registro').on('submit', function(e) {
    e.preventDefault();
    $('#input_username').contents = "";
    document.getElementById("msg_error").innerHTML = "";
    let username = $('#input_username').val();
    $.ajax({type: 'POST', url: 'http://localhost:8282/login', data:{'username': username },
      success: function(data){
        $("#lb_usrname").append(username);
        $('#form_registro').hide();
        $('#container_chat').show();
        document.getElementById("input_msg").focus();
        connChat = new WebSocket("ws://localhost:8282/susbribe_chat?username="+username);
        connChat.onopen = function() {
          connChat.onmessage = function(msg) {
            console.log("Entregado: ", msg.data);
            $('#messages').val($('#messages').val() + msg.data + "\n")
          }
        }
      },
      error: function(errorResponse) {
        if (errorResponse.status == 400 ) {
          document.getElementById("msg_error").innerHTML = errorResponse.responseJSON.message;
        }
      }
    })
  }); // termina submit de #form_registro

  $('#form_chat').on('submit', function(e) {
    e.preventDefault();
    let msg = $('#input_msg').val()
    console.log("Enviando: " + msg);
    connChat.send(msg)
    $('#input_msg').val("")
  });

});
</script>
</body>
</html>
```

Backend

```go
  package main
  import ( "encoding/json" "log" "net/http" "sync" "text/template"
    "github.com/gorilla/websocket"
  )

  func main() {
    var estaticos = http.FileServer(http.Dir(`html/static`))
    http.Handle(`/static/`, http.StripPrefix(`/static/`, estaticos))

    http.HandleFunc(`/`, HomeHandler)
    http.HandleFunc(`/login`, loginHandler)
    http.HandleFunc(`/susbribe_chat`, mySocketHandler)

    http.ListenAndServe(`:8282`, nil)
  }

  func HomeHandler(w http.ResponseWriter, r *http.Request) {
    log.Println("Sirviendo home")
    viewModel := template.Must(
      template.New(`home.html`).ParseFiles(`html/templates/home.html`, `html/templates/includes.html`),
    )
    viewModel.Execute(w, nil)
  }

  func loginHandler(w http.ResponseWriter, r *http.Request) {
    log.Println("Validando usuario")
    code := 500
    msg := "error inesperado"

    if r.Method == "POST" {
      username := r.FormValue(`username`)
      if userExist(username) {
        code = 400
        msg = "El usuario ya existe, intente otro"
      } else {
        code = 201
        msg = "Usuario creado"
      }
    }
    respondJson(&w, Response{msg}, code)
  }

  func mySocketHandler(w http.ResponseWriter, r *http.Request) {
// CREAR SOCKET
+   ws, err := websocket.Upgrade(w, r, nil, 1024, 1024)
    defer ws.Close()
    if err != nil {
      panic(err)
    }
    username := r.URL.Query().Get(`username`)
    addUser(username, ws)

// leer mensajes de un websocket, escuchar websocket
    for {
+     typeMessage, msgBites, err := ws.ReadMessage()
      if err != nil {
        removeUser(username)
        panic(err)
      }
      if msgBites != nil {
        log.Print(typeMessage, ", ", string(msgBites), " | ")
// Concatenar string con bytes, concatenar bytes de tipo string
        msgBites = []byte(username + ": " + string(msgBites))
        broadcastMessage(typeMessage, msgBites)
      }
    }
  }

  func broadcastMessage(typeMessage int, msg []byte) {
    for _, user := range users.m {
// enviar mensaje a todos
+     err := user.WebSocket.WriteMessage(typeMessage, msg)
      if err != nil {
        log.Println("Error: ", err.Error())
        return
      }
    }
  }

  func addUser(username string, ws *websocket.Conn) {
    users.Lock(); defer users.Unlock()
    users.m[username] = User{ColorText: "#fff", WebSocket: ws}
    log.Println("Usuario agregado")
  }

  func removeUser(username string) {
    users.Lock(); defer users.Unlock()
    delete(users.m, username)
    log.Println("Usuario eliminado")
  }

  func respondJson(w *http.ResponseWriter, data interface{}, httpStatus int) {
    (*w).Header().Set(`Content-type`, `Application/json`)
    (*w).WriteHeader(httpStatus)
    json.NewEncoder(*w).Encode(data)
  }

  func userExist(username string) bool {
    users.RLock(); defer users.RUnlock()
    _, exist := users.m[username]
    return exist
  }

  type Response struct {
    Message string `json:"message"`
  }

  var users = struct {
// USUARIO PROMOBIDO A SYNC.RWMUTEX PARA PODER BLOQUERLO Y EVITAR DATA RACING
+   sync.RWMutex
    m map[string]User
  }{m: make(map[string]User)}

  type User struct {
    ColorText string
    WebSocket *websocket.Conn
  }
```

#### Ankis Sockets

```r
webSocket, crear, conectar, escuchar y enviar;"
Con el paquete 'github.com/gorilla/websocket'

<pre>

// EXPONER WEBSOCKET
  http.HandleFunc(`/susbribe_chat`, mySocketHandler)
  ...
  func mySocketHandler(w http.ResponseWriter, r *http.Request) {
// CONVERTIR PETICION HTTP A SOCKET
    ws, err := websocket.Upgrade(w, r, nil, 1024, 1024)
    ...
  }

// ESCUCHAR MENSAJES (DENTRO DE CICLO INFINITO)
  for {
    typeMessage, msgBites, err := ws.ReadMessage()
    ...
  }

// ENVIAR MENSAJE A SUSCRITOS
  for _, user := range users {
    ...
    err := user.WebSocket.WriteMessage(typeMessage, msg)
  }
</pre>

Y podemos hacerle peticion y escuchar y mandar mensaje con JS

<pre>
// ABRIR CONEXION
  let connChat = new WebSocket('ws://localhost:8282/susbribe_chat?username='+username);

// EXCUCHAR MENSAJES
  connChat.onopen = function() {
    connChat.onmessage = function(msg) {
      console.log('Servidor dice: ', msg.data);
      $('#messages').val($('#messages').val() + msg.data + '\n')
    }
  }

// ENVIAR MENSAJE
  connChat.send(msg)

</pre>
"
```

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

### Aplicacion CRUD templates, Conexion a BD, http methods, mux

Veremos como hacer la conexion con Postgres, asi que deberemos tenerlo
activo en nuestro aquipo. Con una base de datos.

En la seccion usaremos `https://github.com/lib/pq`

```r
## libreria para conectarse a bases de datos, paquete para conectarse a postgres
## driver para postgres
Drivers para conectar a BD;"
> go get github.com/lib/pq 
"

Iniciar una conexion a BD y hacer una consulta;"
1. Debemos de tener los siguientes datos de conexion y crear el
siguiente string de conexion.

<pre>
  type ConnectionDBInfo struct {
    host, port, user, pass, db_name string
  }

  func (c *ConnectionDBInfo) GetUrl() string {
    return fmt.Sprintf(`host=%s port=%s user=%s password=%s dbname=%s sslmode=disable`,
      c.host, c.port, c.user, c.pass, c.db_name,
    )
  }
</pre>

2. Crear los datos de conexion y abrir conexion

<pre>
  connInfo := ConnectionDBInfo{
    host: `localhost`, port: `5432`, user: `postgres`, pass: `postgres`, db_name: `mercado`,
  }
  urlCon := connInfo.GetUrl()
  fmt.Println(`URL: `, urlCon)
  db, err := sql.Open(`postgres`, urlCon)
  if err != nil {
    panic(`Error de conexion: ` + err.Error())
  }
<pre>

5. Crear un struct con los mismos nombres de columna que la tabla

<pre> 
  type Categoria struct {
    id_categoria int
    nombre       string
    descripcion  string
  }
</pre>

4. Hacer la peticion y recorrer

<pre>
  allCategoriasQuery := `
    SELECT id_categoria, nombre, descripcion
    FROM categorias;
  `
// AQUI REALIZAMOS LA CONSULTA, LA CUAL NOS DARA UNA/S FILA/S
+ rows, err := db.Query(allCategoriasQuery)
  if err != nil {
    panic(`Error al consultar allCategoriasQuery: ` + err.Error())
  }

  categorias := []Categoria{}
  for rows.Next() {
    categoriaTemp := Categoria{}
    rows.Scan(&categoriaTemp.id_categoria, &categoriaTemp.nombre, &categoriaTemp.descripcion)
    categorias = append(categorias, categoriaTemp)
  }

  for _, c := range categorias {
    fmt.Println(c)
  }
</pre>
"

Pasar parametros a una consulta;"
Se enumeran los parametros con '$1' donde tomara el primer parametro
del query.

<pre>
allCategoriasQuery := `
  SELECT id_categoria, nombre, descripcion
  FROM categorias
  WHERE id_categoria=$1;
`
rows, err := db.Query(allCategoriasQuery, 2)
</pre>
"
```

#### 40. Crear funcion PL-SQL y llamarla

```r
Crear una funcion pl-sql y llamarla;"
<p>
  No es recomendable tener consultas muy largas en nuestro codigo, ya que
  estas pueden cambiar, afectando distintas partes de nuestro codigo.
</p>

0. Si queremos sustituir la sig. consulta

<pre>
allCategoriasQuery := `SELECT id_categoria, nombre, descripcion ROM categorias;`
</pre>

1. Dentro de pgAdmin, sobre nuestra tabla, en el query tool, creamos
la siguiente funcion, la cual contiene nuestra consulta.

<pre>
################################## OPCION QUE YO GENERE

CREATE OR REPLACE FUNCTION categorias_find_all()
RETURNS SETOF categorias AS
$BODY$
BEGIN

  RETURN QUERY
    SELECT id_categoria, nombre, descripcion
    FROM categorias;

END
$BODY$
LANGUAGE plpgsql;

################################## LA OPCION DEL CURSO

CREATE OR REPLACE FUNCTION CategoriasFindAll()
returns table(id_categoria int, nombre varchar(60), descripcion varchar(160))
as
$$
  SELECT id_categoria, nombre, descripcion
  FROM categorias
$$
language sql;

</pre>

2. Podemos llamarla de la siguiente forma, lo cual la llamada es igual
que ejecutar cualquier sql, solo cambia el sql,

<pre>
allCategoriasQuery := `SELECT * FROM categorias_find_all()`
</pre>
"
```

#### 41. Manejar Peticiones Post

```r
## obtener dato de formulario, obtener informacion POST, obtener peticion post, obtener post
Como detectar un POST y obtener los datos del formulario;"
En la funcion handler en ´r http.Request´ esta ´r.Method == `GET`´,
y para captuar un dato del formulario, podemos ´r.FormValue(`nombre`)´,
<br />
por ejemplo

Formulario
<pre>
  \<form method='POST' action='/categorias'\>
    \<input placeholder='Nombre categoria' type='text' name='nombre' \>
    \<button type='submit' \> Button \>/button\>
  \</form\>
</pre>

Captura
<pre>
func CategoriasHandler(w http.ResponseWriter, r *http.Request) {
// AQUI AVERIGUAMOS QUE TIPO DE PETICION ES
+ if (r.Method == ´GET´) {
    categorias, err = dao.FindAllCategorias()
  } else {
// SE CAPTURA EL DATO
+   nombre := r.FormValue(´nombre´)
    categorias, err = dao.FindCategoriasByNombre(r.FormValue(nombre))
  }
  ...
</pre>
"
```

#### Intercambiar info por medio de hidden inputs

```r
Ventajas de intercambiar info por medio de hidden inputs, y ejemplo;"
Podemos hacer un render con js lo cual para cieras tareas es mas facil,
hacerlo, por ej. conservar un cambio en un en html en un select, lo cual
dibujar el cambio con templates seria complicado, pero con js es facil.
<br />
Resultado del condigo: Se guardara el valor del select entre peticiones

<pre>
// GUARDAMOS EL VALOR DE SELECT EN EL HIDDEN
+ \<input type='hidden' value='{{.FiltroCategoria}}' id='f_cat' /\>
  
  \<select onchange='this.form.submit()' name='id_categoria'
    class='form-select' id='f_in_cat'\>
    \<option value='-1'\>Ninguno\</option\>
    {{range .Categorias}}
// PINTER EL SELECTED CON SSR SERIA COMPLICADO
!     \<option value='{{.Id_categoria}}'\>{{.Nombre}}\</option\>
    {{end}}
  \</select\>

  \<script\>
    window.onload = function() {
// CAMBIAMOS EL VALOR
+     document.getElementById('f_in_cat').value = document.getElementById('f_cat').value;
    }
  \</script\>
</pre>
"
```

#### Ejecuatar SQL (Insert, Update, Delete)

```r
Como ejecutar un sql delete, update, insert (sin regresar filas);"
De ´*sql.DB´ usamos ´...Exec(sqlQuery, params...)´ el cual nos regresara
un ´sql.Result´
<br />
Ejemplo: El codigo es para insertar una nueva categoria, donde podemos
ver cosas como redireccion, ejecutar sql en lugar de una query, etc.
<br />
Formulario
<pre>
  \<form method='POST' action='/categorias/agregar'\>
    \<input type='text' class='form-control' id='nombre' name='nombre'\>
    \<textarea name='descripcion'\>\</textarea\>
    \<button type='submit'\>Agregar\</button\>
  \</form\>
</pre>

Handler
<pre>
  // http.HandleFunc(`/categorias/agregar`, request_handlers.CategoriaForm)
  func CategoriaForm(w http.ResponseWriter, r *http.Request) {
    var err error
    var viewModel = CategoriaViewModel{}

    if r.Method == `POST` {
// CAPTURAMOS Y MANDAMOS A GUARDAR
+     viewModel.Categoria.Nombre = r.FormValue(`nombre`)
+     viewModel.Categoria.Descripcion = r.FormValue(`descripcion`)
+     err = dao.SaveCategoria(viewModel.Categoria)
      if err == nil {
// SI SALE TODO BIEN REDIRECCIONAMOS.
+       http.Redirect(w, r, `/categorias`, 301)
        return
      }
    }

    helpers.ShowTemplate(&w, `categorias_form`, nil)
  }
</pre>

DAO
<pre>
  func SaveCategoria(newCategoria models.CategoriaModel) (err error) {
    query := `INSERT INTO categorias(nombre, descripcion) VALUES($1, $2)`
    _, err = helpers.ExecUniqueSQL(query, newCategoria.Nombre, newCategoria.Descripcion)
    return err
  }
</pre>

DBHelper
<pre>
import ( `database/sql` `fmt` _ `github.com/lib/pq`)

var dbConn *sql.DB

  func ExecUniqueSQL(sqlQuery string, params ...interface{}) (sql.Result, error) {
    var errorDB error

    errorDB = OpenDBConn()
// EN EL RESULT PODREMOS VER LA CANTIDAD DE FILAS AFECTADAS
+   result, errorDB := dbConn.Exec(sqlQuery, params...)
    errorDB = CloseDBConn()

    if errorDB != nil {
      return nil, errorDB
    }

    return result, nil
  }
</pre>
"
```

#### Uso y Ventajas de Gorilla Mux

```r
Que es Gorilla Mux ventajas e instalacion;"
Es un http handler de terceros, que puede ser sustituido por el de GOlang
por default, la ventaja es que podemos usar 'HOST/{algun_id}', mientras
que en el handler por defecto no.

<br />

Instalacion: 'go get github.com/gorilla/mux'
"

Como se implementa gorilla/mux;"
<p>
  CUIADO: si serviamos estaticos debemos de cambiar su conf. ya que
  en mi caso dejo de servir sub-carpetas lo que me lio.
</p>

Se teniamos

<pre>
import ( 'net/http'
func main() {
  http.HandleFunc(`/...`, ...)
  http.ListenAndServe(`:...`, nil)
}
</pre>

Migramos con

<pre>

// IMPORTAMOS DEPENDICIA
+ import ( 'net/http' 'github.com/gorilla/mux' )

  func main() {
// CREAMOS INSTANCIA
+   router := mux.NewRouter()

    router.HandleFunc(`/...`, ...)
// LA USAMOS COMO EL HANDLER DE NUESTRO HTTP
+   http.ListenAndServe(`:...`, router)

  }
</pre>

Para los estaticos

<pre>
  var estaticos = http.FileServer(http.Dir('html/statics'))

//CONFIGURACION SIN MUX
  //http.Handle('/html/statics/', http.StripPrefix('/html/statics/', estaticos))

// CONFIGURACION CON MUX
  serveFileHandler := http.StripPrefix('/html/statics/', estaticos)
  router.PathPrefix('/html/statics/').Handler(serveFileHandler)

</pre>
"
```

#### CODIGO: Aplicacion CRUD templates

Resultado: Solo hacemos el crud sin autenticacion, viendo los temas de
esta seccion, se trabajara en el mismo codigo mas adelante, pero se le
agregaremos una API-Json.

- Codigo: [crud_template.zip](./img/_4_crud_template.zip)
- Imagen: [crud_template.png](./img/_4_crud_template.png)

### Aplicacion CRUD JSON: Auth-Cookies, Sockets

```r
Entregar un json;"
  func ShowJson(w *http.ResponseWriter, model interface{}) {
    (*w).Header().Set(`Content-Type`, `appli­cat­ion­/json`)

//Convertir a json
+   jsonByte, err := json.Marshal(model)
    if err != nil {
      panic(ErrorModel{´Error al convertir modelo a json´, 500, nil})
    }
//Mostrar el json en la respuesta
    fmt.Fprint(*w, string(jsonByte))
  }
"

## encriptar
Cifrar un texto;"<pre>

import 'crypto/sha256'
...
byteCifrado := sha256.Sum256([]byte(textoACifrar));
textoCifrado := hex.EncodeToString(byteCifrado[:]);

</pre>"
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## %%%%%%%%%%% WEB INTERMEDIO %%%%%%%%%%%

Fuente: [Udemy](https://www.udemy.com/course/building-web-applications-with-go-intermediate-level/)

### Crear Terminal Virtual

**Objetivo:** Crear un formulario que podramos poner el monto, datos de
la targeta, email, etc. y pueda hacer el cobro en stripe, por el momento
lo tendremos en modo desarrollo.

Codigo terminado: [terminal basica](./img/_5_01_go_stripe.zip)

**En la imagen** podemos ver como metemos una targeta que no sirve y el
formulario maneja el error, y luego metemos una que si sirve y si hace
el el cual podemos ver en el Dashboard de strapi.

![Terminal Basica](./img/_5_conexion_con_strapi_usr_introduce_el_precio.gif)

```r
# Uso de flags
uso de banderas;"<pre>
import ( ´flag´ )

func main() {
  flag.IntVar(&port, ´port´, 4001, ´Server port to listen on´)
  flag.StringVar(&env, ´env´, ´development´, ´Application enviornment {development|production|maintenance}´)
}
</pre>
"

# enviroment variables, variables de ambiente, variables de sistema operativo
uso de varibles entorno o del S.O.;"
<pre>
os.Getenv('NOMBRE_VAR')
</pre>

NOTA: Para asignar una variable en windows usamos en la consola ´setx MI_VAR 'VALOR'´
"

# PENDIENTE|TODO: INVESTIGAR PARA QUE SON LOS OTROS TIMEOUTS
Asignar timeouts como (IdleTimeout, ReadTimeout, etc) a un web server;"
Podemos crear un servidor y definir sus distintos timeouts
<pre>
myServer := &http.Server{
      Addr:              fmt.Sprintf(':%d', app.config.port),
      Handler:           app.routes(),
      IdleTimeout:       30 * time.Second,
      ReadTimeout:       10 * time.Second,
      ReadHeaderTimeout: 5 * time.Second,
      WriteTimeout:      5 * time.Second,
    }
</pre>
"

```

#### Iniciando proyecto

A, Crear proyecto `go mod init hello_go`

B. Creamos estructura de archivos, mas adelante veremos como generar
un binario  para el front y otro para el back, partiendo del mismo
codigo.

A,1, Instalamos dependencias

```r
go get github.com/go-chi/chi/v5
```

```r
/
  cmd/ # Tendra el main de nuestro proyecto
    api/ # Backend
    web/ # Frontend
      main.go
  internal/
```

C. En `cmd/web/main.go` creamos los structs necesarios paraguardar info
para el servidor e iniciamos el servidor

```go
  package main
  import ( "flag" "fmt" "html/template" "log" "net/http" "os" "time" )

  const version = "1.0.0"

  // la usaremos para actualizar el cache de archivos
  // estaticos (css, js, html)
  const staticVersion = "1"

  type Config struct {
    port int
    env  string // TEST, QA, PROD
    api  string // usaremos la api que crearemos aparte
    db   struct {
      dsn string // Data Source Name
    }
    // llaves para consultar stripe
    stripe struct {
      secret string
      key    string
    }
  }

  type Application struct {
    config        Config
    infoLog       *log.Logger
    errorLog      *log.Logger
    templateCache map[string]*template.Template // tiene que ser del html, no de text
    version       string
  }

  func main() {
    var cfg Config

// PEDIREMOS ESTAS FLAGS PARA INICIAR LA APP (TERCER PARAMETRO ES EL DEFAULT)
// obtener variables de consola, parametros de ejecutable, banderas
    flag.IntVar(&cfg.port, "port", 4000, "The server is liseng in")
    flag.StringVar(&cfg.env, "env", "dev", "S-App eviroment {dev|PROD}")
    flag.StringVar(&cfg.api, "api", "http://localhost:4000", "URL to API")
    flag.Parse()

// TOMAREMOS LAS VARIABLES SECRETAS DE LAS VARIABLES DE ENTORNO
    cfg.stripe.key = os.Getenv("STRIPE_KEY")
    cfg.stripe.secret = os.Getenv("STRIPE_SECRET")

    var app = &Application{
      version:       version,
      config:        cfg,
      infoLog:       log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime),
      errorLog:      log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime),
      templateCache: make(map[string]*template.Template),
    }

    err := app.serve()

    if err != nil {
      app.errorLog.Println(err)
      log.Fatal(err)
    }
  }

  func (app *Application) serve() error {
    myServer := &http.Server{
      Addr:              fmt.Sprintf(":%d", app.config.port),
//ESTE METODO ESTA EN ROUTES.GO MOSTRADO ABAJO, DONDE DECLARAREMOS LAS RUTAS
      Handler:           app.routes(),
      IdleTimeout:       30 * time.Second,
      ReadTimeout:       10 * time.Second,
      ReadHeaderTimeout: 5 * time.Second,
      WriteTimeout:      5 * time.Second,
    }

    app.infoLog.Printf("starting web server in %s mode on port %d", app.config.env, app.config.port)

    return myServer.ListenAndServe()
  }
```

D, Creamos solo una ruta temporal para ver si todo levanta bien

```go
  package main
  import ( "fmt" "net/http" "github.com/go-chi/chi/v5" )
  func (app *Application) routes() http.Handler {
    mux := chi.NewRouter()
    mux.Get("/", func(rw http.ResponseWriter, r *http.Request) {
      fmt.Fprint(rw, `Hello`)
    })
    return mux
  }
```

**Resultado:** Si ejecutamos `go run ./cmd/web/` levantara el servidor
por default en el puerto `4000` en el ambiente de desarrollo pero con
`go run ./cmd/web/ -port XXXX -env XXX` podemos cambiarlo al gusto.

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Creando render de templates con cache

```r
#uso de cache, 
Idea general de crear un cache, usar cache;"
<p>
Debemos de crear un struct unico que contenga un mapa donde su llave
sera el nombre del template y de valor el template, y en cada peticion
comprobar si no existe se construlle, y si ya existe utilizar la que esta
dentro del map.
</p>
<p>
Nota: Si estamos en test/desarrollo no usar el cache, ya que nos
complicaria las pruebas.
</p>
"
```

**Objetivo:** Crearemos el render de templates, el cual le prodremos
pasar info, uso de cache, proteccion CSRF etc.

A, Creamos el struct de los datos que podremos utilizar en el template

```go
  package main
  import ( "embed" "fmt" "html/template" "net/http" "strings" )

  type TemplateData struct {
    Texts          map[string]string
    Numbers        map[string]string
    Decimals       map[string]string
    Structs        map[string]string
    CSRFToken      map[string]string
    Flash          string // mensajes tipo notificaciones (un solo uso/efimeros)
    Warning        string
    Error          string
    IsAuth         bool
    API            string
    StaticsVersion string
  }

  var functions = template.FuncMap{}

// IMPORTANTE EL COMENTARIO DE ABAJO, SI NO ESTA FALLARA CON 'template: pattern matches no files: `templates/base.gohtml`'
  //go:embed templates
  var templatesFS embed.FS

//Por ahora no hace nada
  func (app *Application) addDefaultData(td *TemplateData, r *http.Request) *TemplateData {
    return td
  }

  func (app *Application) renderTemplate(w http.ResponseWriter, r *http.Request, page string, td *TemplateData, partials ...string) error {
    var t *template.Template
    var templateToRender = fmt.Sprintf("templates/%s.gohtml", page)
    var err error

    _, templateInMap := app.templateCache[templateToRender]

    if app.config.env == "PROD" && templateInMap {
//USARA EL CACHE DE LA PLANTILLA
      t = app.templateCache[templateToRender]
    } else {
//CONSTRUIRA LA PLANTILLA
      t, err = app.parseTemplate(page, templateToRender, partials)
      if err != nil {
        return err
      }
    }

    if td == nil {
      td = &TemplateData{}
    }

    td = app.addDefaultData(td, r)

    err = t.Execute(w, td)
    if err != nil {
      app.errorLog.Println(err)
      return err
    }

    return nil
  }

  func (app *Application) parseTemplate(page string, templateToRender string, partials []string) (*template.Template, error) {
    var t *template.Template
    var err error

    name := fmt.Sprintf("%s.gohtml", page)
    if len(partials) > 0 {
      for i, partial := range partials {
        partials[i] = fmt.Sprintf("templates/partials/%s.gohtml", partial)
      }

      partialsArr := strings.Join(partials, ",")
      t, err = template.New(name).Funcs(functions).ParseFS(templatesFS, "templates/base.gohtml", partialsArr, templateToRender)
    } else {
      t, err = template.New(name).Funcs(functions).ParseFS(templatesFS, "templates/base.gohtml", templateToRender)
    }

    if err != nil {
      return nil, err
    }
    app.templateCache[templateToRender] = t
    return t, nil
  }
```

B. Creamos las plantilla base `cmd\web\templates\base.gohtml`

```html
{{define "base"}}
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
  <title>{{block "title" .}}{{end}}</title>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col">
        {{block "content" .}}{{end}}
      </div>
    </div>
  </div>
  {{block "js" .}}{{end}}
</body>
</html>
{{end}}
```

C. Creamos la plantilla de nuestra terminal `cmd\web\templates\terminal.gohtml`,
el javascript lo haremos mas adelante, ahora solo nos importa haga la
plantilla.

```html
{{template "base" .}}

{{define "title"}}Virtual Terminal{{end}}

{{define "content"}}
  <h2>Virtual Terminal</h2>
  <hr />
  <div class="alert alert-danger text-center d-none" id="card-messages"></div>

  <form action="/payment-succeeded" method="post"
      name="charge_form" id="charge_form"
      class="d-block needs-validation charge-form"
      autocomplete="off" novalidate="">
  
      <div class="mb-3">
          <label for="amount" class="form-label">Amount</label>
          <input type="text" class="form-control" id="amount" name="amount"
              required="" autocomplete="amount-new">
      </div>
  
      <div class="mb-3">
          <label for="cardholder-name" class="form-label">Cardholder Name</label>
          <input type="text" class="form-control" id="cardholder-name" name="cardholder_name"
              required="" autocomplete="cardholder-name-new">
      </div>
  
      <div class="mb-3">
          <label for="cardholder-email" class="form-label">Cardholder Email</label>
          <input type="email" class="form-control" id="cardholder-email" name="cardholder_email"
              required="" autocomplete="cardholder-email-new">
      </div>
  
      <div class="mb-3">
          <label for="card-element" class="form-label">Credit Card</label>
          <div id="card-element" class="form-control"></div>
          <div class="alert-danger text-center" id="card-errors" role="alert"></div>
          <div class="alert-success text-center" id="card-success" role="alert"></div>
      </div>
  
      <hr>
  
      <a id="pay-button" href="javascript:void(0)" class="btn btn-primary" onclick="val()">Charge Card</a>
      <div id="processing-payment" class="text-center d-none">
          <div class="spinner-border text-primary" role="status">
              <span class="visually-hidden">Loading...</span>
          </div>
      </div>
  
  </form>
{{end}}

{{define "js"}}
 
{{end}}
```

**CODIGO:** [.zip](./img/03_go_stripe_01.zip)

**Resultado:** Podemos ver la plantilla y tiene el mecanismo de uso de
cache.

![02_build_basico_de_plantilla_con_cache](./img/02_build_basico_de_plantilla_con_cache.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Stripe: Crear formulario con input de targeta generado dinamicamente por Stripe

**Objetivo:** Los valores de la tarjeta son validados por Stripe, por
lo que debemos de darle determinados elementos html con un id especifico,
para que la libreria cree el input que valida la targeta.

A, Debemos de tener un una uenta de stripe, y debemos de tener a la mano
la llave publica.

B, Creamos el siguiente formulario

```html
{{template "base" .}}

{{define "title"}}
    Virtual Terminal
{{end}}

{{define "content"}}
<h2 class="mt-3 text-center">Virtual Terminal</h2>
<hr>
<div class="alert alert-danger text-center d-none" id="card-messages"></div>

<form action="/payment-succeeded" method="post"
    name="charge_form" id="charge_form"
    class="d-block needs-validation charge-form"
    autocomplete="off" novalidate="">

    <div class="mb-3">
        <label for="amount" class="form-label">Amount</label>
        <input type="text" class="form-control" id="amount" name="amount"
            required="" autocomplete="amount-new">
    </div>

    <div class="mb-3">
        <label for="cardholder-name" class="form-label">Cardholder Name</label>
        <input type="text" class="form-control" id="cardholder-name" name="cardholder_name"
            required="" autocomplete="cardholder-name-new">
    </div>

    <div class="mb-3">
        <label for="cardholder-email" class="form-label">Cardholder Email</label>
        <input type="email" class="form-control" id="cardholder-email" name="cardholder_email"
            required="" autocomplete="cardholder-email-new">
    </div>

    <div class="mb-3">
        <label for="card-element" class="form-label">Credit Card</label>
        <div id="card-element" class="form-control"></div>
        <div class="alert-danger text-center" id="card-errors" role="alert"></div>
        <div class="alert-success text-center" id="card-success" role="alert"></div>
    </div>

    <hr>

    <a id="pay-button" href="javascript:void(0)" class="btn btn-primary" onclick="val()">Charge Card</a>
    <div id="processing-payment" class="text-center d-none">
        <div class="spinner-border text-primary" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
    </div>

</form>

{{end}}

{{define "js"}}
    <script src="https://js.stripe.com/v3/"></script>

    <script>
    let card;
    let stripe;
    const cardMessages = document.getElementById("card-messages");
    const payButton = document.getElementById("pay-button");
    const processing = document.getElementById("processing-payment");

    stripe = Stripe('CLAVE_PUBLICA');

    function hidePayButton() {
        payButton.classList.add("d-none");
        processing.classList.remove("d-none");
    }

    function val() {
        let form = document.getElementById("charge_form");
        if (form.checkValidity() === false) {
            this.event.preventDefault();
            this.event.stopPropagation();
            form.classList.add("was-validated");
            return;
        }
        form.classList.add("was-validated");
        hidePayButton();
    }

    (function() {
        // create stripe & elements
        const elements = stripe.elements();
        const style = {
            base: {
                fontSize: '16px',
                lineHeight: '24px'
            }
        };

        // create card entry
        card = elements.create('card', {
            style: style,
            hidePostalCode: true,
        });
        card.mount("#card-element");

        // check for input errors
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById("card-errors");
            if (event.error) {
                displayError.classList.remove('d-none');
                displayError.textContent = event.error.message;
            } else {
                displayError.classList.add('d-none');
                displayError.textContent = '';
            }
        });
    })();
    </script>
{{end}}
```

**Resultado** Debemos de tener el input de la tarjeta funcionando (por
el momento en env-desarrollo), el cual podemos usar [targetas de test](https://stripe.com/docs/testing)
y saber si es si es valida, visa, mastercard, validacion del cvv, etc.

![04_formulario_stripe_solo_integracion_del_input_de_stripe](./img/04_formulario_stripe_solo_integracion_del_input_de_stripe.png)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Stripe: Crear Intent de pago

```r
# configuracion de cors, uso de cors, limitar origenes, limitar headers, limitar metodos
Como configurar cors con chia;"<pre>
  import ( ´net/http´ ´github.com/go-chi/chi/v5´ ´github.com/go-chi/cors´ )

  func (app *application) routes() http.Handler {
    mux := chi.NewRouter()
    mux.Use(cors.Handler(cors.Options{
// PODRIAMOS SER MAS ESPECIFICOS COMO ´HTTP://127.0.0.1´ O ALGO ASI
      AllowedOrigins: []string{´https://*´, ´http://*´},
//PODRIAMOS SER MAS GENERALES CON ´*´
      AllowedMethods: []string{´GET´, ´POST´, ´PUT´, ´DELETE´, ´OPTIONS´},
      AllowedHeaders: []string{´Accept´, ´Authorization´, ´Content-Type´, ´X-CSRF-Token´},
      AllowCredentials: false,
      MaxAge: 300, //NO SE QUE ESTO
    }))

    mux.Get(´/api/...´, someMethodHander)
    
    return mux
  }
</pre>"

Que partes/pasos tiene una transaccion con Strapi;"
<ol>
  <li>
    Intent: Es el primero que se hace, el cual indica si todo esta correcto
    para hacer la transaccion. Y es necesario hacerlo ya que el ciclo
    de vida de la transaccion debe de pasar este, para continuar con
    el cargo.
  </li>
  <li>
    Confirm Card Payment: Este ya es el cargo a la targeta, lo cual
    ya debimos haber pasado exitosamente el Intent. Este proceso podemos
    hacerlo de forma inmediata haciendo pensar al usuario que fue una
    unica peticion.
  </li>
</pl>
"
```

**Objetivo:** Crearemos los structs/metodos hacer Intent de stripe,
y expondremos un api endpoint para hacerlo.

**Nota**: Createmos un 2ª main en e proyecto, y lo que pasara es que
cuando compilemos `cmd\api\api.go` se generara un binario, y cuando
compilemos `cmd\web\main.go` se generara otro.

A, instalamos el pagete de stripe `go get github.com/stripe/stripe-go/v72`

B, Creamos el modelo para nuestra targeta en `internal\card.go`

```go
  package card
  import ( "github.com/stripe/stripe-go/v72" "github.com/stripe/stripe-go/v72/paymentintent" )

//Lo usaremos realizar una transaccion
  type Card struct {
    Secret   string
    Key      string
    Currency string
  }

//Detos de la transacion
  type Transaction struct {
    TransactionStatusID int
    Amount              int
    Currency            string
// guardaremos solo los ultimos 4 nums de la targeta, para no guardar
// ningun dato sinsible, para cobrar solo necesitamos enviarle los datos
// a stripe, que en nuestra app no se guarde nada sensible
    LastFour            string
    BankReturnCode      string
  }

  func (c *Card) Charge(currency string, amount int) (*stripe.PaymentIntent, string, error) {
    stripe.Key = c.Secret

    // create a payment intent
    params := &stripe.PaymentIntentParams{
      Amount:   stripe.Int64(int64(amount)),
      Currency: stripe.String(currency),
    }
    //params.AddMetadata("key", "value")
    pi, err := paymentintent.New(params)
    if err != nil {
      msg := ""
      if stripeErr, ok := err.(*stripe.Error); ok {
        msg = cardErrorMessage(stripeErr.Code)
      }
      return nil, msg, err
    }
    return pi, "", nil
  }

  func cardErrorMessage(code stripe.ErrorCode) string {
    var msg = ""
    switch code {
    case stripe.ErrorCodeCardDeclined: msg = "Your card was declined"
    case stripe.ErrorCodeExpiredCard: msg = "Your card is expired"
    case stripe.ErrorCodeIncorrectCVC: msg = "Incorrect CVC code"
    case stripe.ErrorCodeIncorrectZip: msg = "Incorrect zip/postal code"
    case stripe.ErrorCodeAmountTooLarge: msg = "The amount is too large to charge to your card"
    case stripe.ErrorCodeAmountTooSmall: msg = "The amount is too small to charge to your card"
    case stripe.ErrorCodeBalanceInsufficient: msg = "Insufficient balance"
    case stripe.ErrorCodePostalCodeInvalid: msg = "Your postal code is invalid"
    default: msg = "Your card was declined" }
    return msg
  }
```

C, Crear un segundo main en `cmd\api\api.go` el cual generara otro binario
para nuestra api, el cual expondra un endpoint.

```go
  package main

  import ( "flag" "fmt" "log" "net/http" "os" "time" )
  const version = "1.0.0"

  type config struct { port int env  string db   struct { dsn string }
    stripe struct { secret string key    string }
  }

  type application struct { config   config infoLog  *log.Logger errorLog *log.Logger version  string }

  func (app *application) serve() error {
    srv := &http.Server{
      Addr: fmt.Sprintf(":%d", app.config.port),
// TRAEMOS LOS ENDPINTS
      Handler: app.routes(),
// ASIGNAMOS LOS TIMEOUTS
      IdleTimeout: 30 * time.Second, ReadTimeout: 10 * time.Second, ReadHeaderTimeout: 5 * time.Second, WriteTimeout: 5 * time.Second,
    }

    app.infoLog.Println(fmt.Sprintf("API in %s mode on port %d", app.config.env, app.config.port))

    return srv.ListenAndServe()
  }

  func main() {
    var cfg config
    flag.IntVar(&cfg.port, "port", 4001, "Api server port to listen on")
    flag.StringVar(&cfg.env, "env", "development", "Application enviornment {development|production|maintenance}")
    flag.Parse()
    cfg.stripe.key = os.Getenv("STRIPE_KEY")
    cfg.stripe.secret = os.Getenv("STRIPE_SECRET")
    infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
    errorLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
    
    app := &application{ config: cfg, infoLog:  infoLog, errorLog: errorLog, version:  version, }
// INICIAMOS EL API SERVER
    err := app.serve()
    if err != nil { log.Fatal(err) }
  }
```

C,1, usaremos el cors de chia, instalammos `go get github.com/go-chi/cors`

C,2, Configurmaos los cors

```go
  package main
  import ( "net/http" "github.com/go-chi/chi/v5" "github.com/go-chi/cors" )

  func (app *application) routes() http.Handler {
    mux := chi.NewRouter()

    mux.Use(cors.Handler(cors.Options{
      AllowedOrigins:   []string{"https://*", "http://*"},
      AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
      AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
      AllowCredentials: false,
      MaxAge:           300,
    }))

    //mux.Get("/api/payment-intent", app.GetPaymentIntent)

    return mux
  }
```

C,3, Creamos el endpoint que entrege un json dummy por ahora

```go
  package main
  import ( "encoding/json" "net/http" )

  type stripePayload struct {
    Currency string `json:"currency"`
    Amount   string `json:"amount"`
  }

  type jsonResponse struct {
    OK      bool   `json:"ok"`
    Message string `json:"message"`
    Content string `json:"content"`
    ID      int    `json:"id"`
  }

  func (app *application) GetPaymentIntent(w http.ResponseWriter, r *http.Request) {
    j := jsonResponse{
      OK: true,
    }

    out, err := json.MarshalIndent(j, "", "   ")
    if err != nil {
      app.errorLog.Println(err)
    }

    w.Header().Set("Content-Type", "application/json")
    w.Write(out)
  }
```

C.Resultado: **Asta este punto** si ejecutamos `go run ./cmd/api`
podremos apuntar a `localhost:4001/api/payment-intent` y nos dara
un json sencillo/dummy.

D, Conectar el endpoint y la transaccion de stripe `cmd\api\handlers_api.go`

```go
  import ( "01_go_stripe/internal/card" "encoding/json" "net/http" "strconv" )

  type stripePayload struct { Currency string `json:"currency"` Amount   string `json:"amount"` }
  type jsonResponse struct { OK      bool   `json:"ok"` Message string `json:"message,omitempty"` Content string `json:"content,omitempty"` ID      int    `json:"id,omitempty"` }

  func (app *ApiApp) GetPaymentIntent(w http.ResponseWriter, r *http.Request) {
    var payload stripePayload

//CONVERTIMOS EL CUERPO DE LA PETICION EN TIPO `stripePayload`
+   err := json.NewDecoder(r.Body).Decode(&payload)
    if err != nil { app.errorLog.Println(err) return }

//LA CANTIDAD NOS LLEGARA COMO CADENA, ASI QUE LA PASAMOS A NUMERO
+   amount, err := strconv.Atoi(payload.Amount)
    if err != nil { app.errorLog.Println(err) return }

// CREAMOS LA TARGETA CON LA QUE NOS IDENTIFICAREMOS CON STRIPE
+   card := card.Card{
      Secret:   app.config.stripe.secret,
      Key:      app.config.stripe.key,
      Currency: payload.Currency,
    }

    okay := true

//HACEMOS LA PETICION A STRIPE
    /*
      pi, msg, err := card.Charge(payload.Currency, amount)
    */

    //success
    msg := "some"
    pi := jsonResponse{
      OK:      true,
      Message: card.Currency,
      Content: "algun contenido",
      ID:      amount,
    }

    if err != nil {
      okay = false
    }

    if okay {
//RESPONDEMOS EXITO
      out, err := json.MarshalIndent(pi, "", "   ")
      if err != nil {
        app.errorLog.Println(err)
        return
      }
      w.Header().Set("Content-Type", "application/json")
      w.Write(out)
    } else {
//RESPONDEMOS ERROR
      j := jsonResponse{
        OK:      false,
        Message: msg,
        Content: "",
      }
      out, err := json.MarshalIndent(j, "", "   ")
      if err != nil {
        app.errorLog.Println(err)
      }
      w.Header().Set("Content-Type", "application/json")
      w.Write(out)
    }
  }
```

E, Cremos peticion del lado del cliente en ``

```html
  {{template "base" .}} {{define "title"}} Virtual Terminal {{end}} {{define "content"}}
  <form action="/payment-succeeded" method="post" name="charge_form" ...>
      ...
  </form>

<script src="https://js.stripe.com/v3/"></script>
<script>
  function val() {
    ...
    hidePayButton();

//UNA FORMA DE LIDIAR CON LOS DECIMALES ES QUITARLOS MULTIPLICANDO POR 100
//PARA ENVIARLO Y LUEGO DIVIDIR DEL OTRO LADO, PARA QUE VIAJE COMO ENTERO
+       let amountToCharge = String(parseFloat(document.getElementById("amount").value) * 100)

//TODO: CAMBIAR LA MONEDA POR PESOS MEXICANOS       
+       let payload = {
            amount: amountToCharge,
            currency: 'cad',
        }

// PREPARAMOS PETICION
+       const requestOptions = {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
            body: JSON.stringify(payload),
        }

        fetch("http://localhost:4001/api/payment-intent", requestOptions)
 // LO PASAMOS A TEXTO PARA PODER COMPROBAR QUE ES UN JSON O CONTROLAR
 // EN CASO QUE NO LO SEA.
 +          .then(response => response.text())
            .then(response => {
                let data;
                try {
                    data = JSON.parse(response);
                    console.log(data);
                } catch (err) {
                    console.log(err);
                }
            })
    }
</script>
```

**Resultado** Si levantamos la web y la api, y precionamos sobre cargar
deberiamos ver la respuesta, que nos envia nuestra API en la consola
del navegador, la cual contruyo a partir de la respuesta de Stripe.

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Stripe: Realizar el cargo a la targeta

**Objetivo:** Como ya hicimos el Intent para ver si podiamos hacer el
cargo a la targeta, y si pasa, entonces podremos hacer el Cargo, lo cual
implementaremos esta parte, utilizando la respuesta del Intent.

```html
  <div id="card-messages" class="alert alert-danger text-center d-none"></div>
  <form id="charge_form" ...>
    ...
<!--
  USAREMOS ESTOS PARA GUARDAR LA RESPUESTA DEL INTENT EN EL DOCUMENTO
  MIENTRAS EL USUARIO ESTE EN EL DOCUMENTO.
-->
+   <input type="hidden" name="payment_intent" id="payment_intent">
+   <input type="hidden" name="payment_method" id="payment_method">
+   <input type="hidden" name="payment_amount" id="payment_amount">
+   <input type="hidden" name="payment_currency" id="payment_currency">

  </form>

<script>
  const cardMessages = document.getElementById("card-messages");

//AGREGAMOS METODOS PONER LA INTERFACE EN MODO CARGANDO, EXITO, ERROR
+ function showPayButtons() {
    payButton.classList.remove("d-none");
    processing.classList.add("d-none");
  }

+ function showCardError(msg) {
    cardMessages.classList.add("alert-danger");
    cardMessages.classList.remove("alert-success");
    cardMessages.classList.remove("d-none");
    cardMessages.innerText = msg;
  }

+ function showCardSuccess() {
    cardMessages.classList.remove("alert-danger");
    cardMessages.classList.add("alert-success");
    cardMessages.classList.remove("d-none");
    cardMessages.innerText = "Transaction successful";
  }


  function val() {
    ....
    fetch("http://localhost:4001/api/payment-intent", requestOptions).then(response => response.text())
      .then(response => {
//CUANDO TENGAMOS LA RESPUESTA DE STRAPI SEGUIMOS CON EL CAGO
        let data;
        try {
+           data = JSON.parse(response);
//FORZOSAMENTE NECESITAMOS EL "client_secret" QUE LE DIO STRAPI A NUESTRO
//BACKEND PARA PODER SEGUR CON LA TRANSAXION.
+           stripe.confirmCardPayment(data.client_secret, {
//CUIDADO DEBE ESTAR ENVUELTO EN "payment_method"
!               payment_method: {
                    card: card,
                    billing_details: {
                        name: document.getElementById("cardholder-name").value,
                    }
                }
            }).then(function(result) {

+               if (result.error) {                  
//IMPLEMENTAMOS QUE HACER SI ALGO FUE MAL
                    showCardError(result.error.message);
                    showPayButtons();

+              } else if(result.paymentIntent) {
+                   if (result.paymentIntent.status === "succeeded") {
//IMPLEMENTAMOS QUE HACER SI SE CARGO EXITOSAMENTE LA TARGETA
                        // we have charged the card
                        document.getElementById("payment_method").value = result.paymentIntent.payment_method_types[0];
                        document.getElementById("payment_intent").value = result.paymentIntent.id;
                        document.getElementById("payment_amount").value = result.paymentIntent.amount;
                        document.getElementById("payment_currency").value = result.paymentIntent.currency;
                        processing.classList.add("d-none");
                        showCardSuccess();
                        // would submit the form
                    }
                }
            })
        } catch (err) {
// ERROR IMPEVISTO
            console.log(err);
            showCardError("Invalid response from payment gateway!");
            showPayButtons();
        }
      })
    }
</script>
```

**Resultado:** Segun la targeta de test que usemos, el cobro se realizara
o no, y le daremos feedback al usuario en ambos casos.

![05_cargo_a_targeta_exito_rechazada](./img/05_cargo_a_targeta_exito_rechazada.png)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Strapi: Mostrar pantalla cobro exitoso y/o factura

**Objetivo:** Con la informacion del Intent y del exito del cobro, vamos
a darle feedback al usuario mostrando una pantalla, donde podra ver los
detalles del cobro y usarlo como factura.

A, Cuando `cmd\web\templates\terminal.gohtml` haga el cargo de forma
exitosa, hacemos submit al formulario.

```html
<!-- CUDADO: CHECAR QUE APUNTE AL ENDPOINT CORRECTO CON EL METODO CORRECTO -->
! <form id="charge_form" action="/payment-succeeded" method="post" >
    ...
  <script>
  fetch("http://localhost:4001/api/payment-intent", requestOptions)
    .then(response => response.text()).then(response => { let data;
      try { ...
        }).then(function(result) { if (result.error) {...
          } else if(result.paymentIntent) {...
            if (result.paymentIntent.status === "succeeded") {
              ...
//Hacemos Submit al formulario
+            document.getElementById("charge_form").submit();
```

B, En `cmd\web\routes.go` agregar la ruta que cunsultara el formulario

```go
  package main
  import ( "net/http" "github.com/go-chi/chi/v5" )

  func (app *Application) routes() http.Handler { mux := chi.NewRouter()
   
+  mux.Post("/payment-succeeded", app.PaymentSucceeded)

  return mux }
```

C, En `cmd\web\handlers.go` agregar el handler que capturara la info.
del formulario y construira la factura.

```go
+ func (app *Application) PaymentSucceeded(w http.ResponseWriter, r *http.Request) {
    err := r.ParseForm()
    if err != nil { app.errorLog.Println(err) return }

    // read posted data
    cardHolder := r.Form.Get("cardholder_name")
    email := r.Form.Get("email")
    paymentIntent := r.Form.Get("payment_intent")
    paymentMethod := r.Form.Get("payment_method")
    paymentAmount := r.Form.Get("payment_amount")
    paymentCurrency := r.Form.Get("payment_currency")

    data := make(map[string]interface{})
    data["cardholder"] = cardHolder
    data["email"] = email
    data["pi"] = paymentIntent
    data["pm"] = paymentMethod
    data["pa"] = paymentAmount
    data["pc"] = paymentCurrency

    if err := app.renderTemplate(w, r, "succeeded", &TemplateData{
      Data: data,
    }); err != nil {
      app.errorLog.Println(err)
    }
  }
```

D, En `x` creamos la plantilla para la factura

```html
{{template "base" . }}

{{define "title"}}
    Payment Succeeded!
{{end}}

{{define "content"}}
    <h2 class="mt-5">Payment Succeeded</h2>
    <hr>
    <p>Payment Intent: {{index .Data "pi"}}
    <p>Cardholder: {{index .Data "cardholder"}}
    <p>Email: {{index .Data "email"}}
    <p>Payment Method: {{index .Data "pm"}}
    <p>Payment Amount: {{index .Data "pa"}}
    <p>Currency: {{index .Data "pc"}}
{{end}}
```

**Resultado:** Cuando tengamos un cobro/cargo exitoso se nos mostrara
esta la sig. factura.

![06_cuando_cobro_exitoso_mostrar_factura](./img/06_cuando_cobro_exitoso_mostrar_factura.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Strapi: Evitar un segundo cobro al recargar la pagina

**Problema** Cuando hacemos un cobro y estamos en la pagina de exito/factura,
si el usuario recarga/refresh la pagina de exito, el navegador preguntara
si queremos re-enviar la info. lo cual si le da que si el usuario, le
hara un segundo cobro, lo cual debemos de evitar.

**Solucion:** Debemos guardar en cookes la info que le enviamos al
usuario y hacer un re-direct, y en el template que pinta el re-direct
tomar la info de la sesion, de esta manera si el usuario llegara a
recarga, no se re-enviaria la info y evitando un segundo cargo.

<!-- uso de sesiones -->
A, Uso de cookies

A,1, Instalamos libreria para manejar la sesion: `go get github.com/alexedwards/scs/v2`

- Fuente: [github/alexedwards](https://github.com/alexedwards/scs)

A,2, En `cmd\web\main.go` configuramos las cookies

```go
+ import ( "github.com/alexedwards/scs/v2" )

  type Application struct {
    ...
// para que nuestra app tenga acceso a la seccion
+   SessionMng    *scs.SessionManager
  }

  ...
  func main() {
    ...
    defer conn.Close()
+   app.SessionMng = scs.New() // creamos instancia de cookies manager
+   app.SessionMng.Lifetime = 24 * time.Hour //asignamos 24hr de vida de cookies
    ...
  }
```

A,3, Creamos `cmd\web\middleware.go` para leer la cookie cada ves que estre el usuario

```go
  package main import "net/http"

+ func (app *Application) SessionLoad(next http.Handler) http.Handler {
+   return app.SessionMng.LoadAndSave(next)
+ }
```

<!-- Uso de middleware, uso de interceptor de peticiones,  -->

A,4, Agregar middleware a `cmd\web\routes.go`

```go
  func (app *Application) routes() http.Handler {
    mux := chi.NewRouter()
+   mux.Use(app.SessionLoad)
```

<!-- guardar info en las cookies -->
B, Guardar info en la seccion

B,1, En `cmd\web\main.go` Debemos de indicar el tipo de variable guardaremos en la session

```go
+ import "encoding/gob"
  func main() {
+   gob.Register(map[string]interface{}{}) // SE PUEDE CUALQUIER TIPO DE DATO
```

B,2, En `cmd\web\handlers.go` Guardamos la info

```go
  func (app *application) PaymentSucceeded(w http.ResponseWriter, r *http.Request) {
    ... _, err = app.SaveOrder(order) if err != nil { app.errorLog.Println(err) return }
    
    //txnData = map[string]interface{  /*TODA-LA-INFO*/  }
+   app.Session.Put(r.Context(), "receipt", txnData)
+   http.Redirect(w, r, "/receipt", http.StatusSeeOther)
  }
```

C, Agregar el template a mostrar en el redirect

C,1, Agregar ruta en `cmd\web\routes.go`

```go
  func (app *application) routes() http.Handler {
    ...
+   mux.Get("/receipt", app.Receipt)
```

C,2, Agregar handler `cmd\web\handlers.go`, el cual toma la info de la
sesion y se la pasa al template

```go
  func (app *application) Receipt(w http.ResponseWriter, r *http.Request) {
    data := app.Session.Get(r.Context(), "receipt").(map[string]interface{})
    app.Session.Remove(r.Context(), "receipt")
    if err := app.renderTemplate(w, r, "receipt", &templateData{
      Data: data,
    }); err != nil {
      app.errorLog.Println(err)
    }
}
```

C,2, Agregar template, es igual solo que esta vez resivimos la info de
la sesion, y como es un redirect al recargar la pagina no re-enviara el
formulario, pero si recarga vera la info, por que esta guardada en la
sesion del usuario

```html
{{template "base" . }}
{{define "title"}} Payment Succeeded! {{end}}
{{define "content"}}
    <h2 class="mt-5">Payment Succeeded</h2><hr>
    <p>Payment Intent: {{index .Data "pi"}}</p> <p>Customer Name: {{index .Data "first_name"}} {{index .Data "last_name"}}</p>
    <p>Email: {{index .Data "email"}}</p> <p>Payment Method: {{index .Data "pm"}}</p>
    <p>Payment Amount: {{index .Data "pa"}}</p> <p>Currency: {{index .Data "pc"}}</p>
    <p>Last Four: {{index .Data "last_four"}}</p> <p>Bank Return Code: {{index .Data "bank_return_code"}}</p>
    <p>Expiry Date: {{index .Data "expiry_month"}}/{{index .Data "expiry_year"}}</p>
{{end}}
```

**Resultado:** Seguimos mostramos igual el exito del cobro y/o factura,
pero esta vez al hacer un refresh, no le cobrara por accidente al usuario
otra vez.

![11_evitar_cobro_al_recargar_pagina](./img/11_evitar_cobro_al_recargar_pagina.gif)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Strapi: Limpiando detalles

**Objetivo:** Tenemos El public key que puede ser de DEV o de PROD, y
nosotros lo tenemos puesta directamente en la plantilla de la terminal,
como tambien la url de la api, lo cual no queremos que tener que cambiar
directamente estos datos cuando querramos cambiae de ambiente o url.

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

### Crear compra de producto

#### Crear BD

**Objetivo:** Vamos a crear una DB y conectarnos a esta, vamos a usar
postgres.

A, Descargamos drivers para nuestro RDBM `go get github.com/lib/pq`

B, Cremos metodo para conectar a db en `internal\driver\driver.go`

```go
  package driver
  import ( "database/sql" _ "github.com/lib/pq" )

  func OpenDB(dsn string) (*sql.DB, error) {
    db, err := sql.Open(`postgres`, dsn)
    if err != nil {
      return nil, err
    }
// Comprobar la conn
    err = db.Ping()
    if err != nil {
      return nil, err
    }
    return db, nil
  }
```

C, Ahora en la api `cmd\web\main.go` y web `cmd\api\api.go` server abrimos con

```go
var app = &Application{...}

app.config.db.dsn = fmt.Sprintf(`host=%s port=%s user=%s password=%s dbname=%s sslmode=disable`,
  "localhost", "5432", "usuaio", "contrasenia", "widgets",
)

conn, err := driver.OpenDB(app.config.db.dsn)
if err != nil {
  app.errorLog.Fatal(err)
}
defer conn.Close()
```

D, Asta este punto si iniciamos el proyecto no deberiamos tener problema

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### crear servidor estatico y Crear pagina producto

<!-- Crear servidor de archivos con chia -->

A, En `cmd\web\routes.go` agregamos la nueva ruta para consumir los
estaticos

```go
import ( "net/http" "github.com/go-chi/chi/v5" )

func (app *Application) routes() http.Handler {
  mux := chi.NewRouter()
  ...
+ fileSrver := http.FileServer(http.Dir("./cmd/web/statics"))
+ http.Handle("/static/*", http.StripPrefix("/statics", fileSrver))
  ...
}
```

A,1, Agregamos contenido en ´./cmd/web/statics´ como imagenes, pdf, etc.

B, Como vamos a reutilizar codigo que creamos de stripe, vamos a hacer
un parcial (como include en php) que agrege el js en nuestra plantilla.

Este va a tener el mismo js que hicimos en `terminal.gohtml`, haci que
**cortamos** y pegamos

```html
<!-- ES IMPORTANTE EL 'DEFINE'-->
+ {{define "stripejs"}}
  <script src="https://js.stripe.com/v3/"></script>

  <script>
    let card;
    let stripe;
    ...
    stripe = Stripe('{{index .Texts "publishable_key"}}');
    ....TODO EL CONTENIDO...
  </script>
  {{end}}
```

C, Como le cortamos a terminal debemos de agregar el **partial** a
terminal y verificar que siga funcionando correctaqmente.

C,1, En terminal.gohtml agregamos el partial donde cortamos el js

```r
{{define "js"}}
    {{template "stripejs" .}}
{{end}}
```

C,2. Agregamos el partial en `cmd\web\handlers.go`

```go
  func (app *Application) VirtualTerminal(w http.ResponseWriter, r *http.Request) {
    tData := TemplateData{...}
// LE AGREGAMOS EL PARTIAL
!   if err := app.renderTemplate(w, r, "terminal", &tData, "stripejs"); err != nil {
```

C.Resultado: terminal.gohtml debe de seguir funcionando igual.

D. Ahora creamos el template de producto, el cual sera igual al de
terminal con algunos cambios, por ejemplo, el usuario no podra definir
el precio.

Pero esto lo haremos mas adelante.

```html
<!-- Es lo mismo que terminal-->
  {{template "base" .}}
  {{define "title"}}Buy one{{end}}
  {{define "content"}}
  <h2 class="mt-3 text-center">Buy a Widget</h2>
  <hr />
<!-- AQUI TENEMOS LA DIF. QUE CARGAMOS UNA IMAGEN USANDO EL SERVIDOR
ESTATICO QUE JUSTO SE DEFINIO ARRIBA
-->
+ <img src="/statics/001_widget.png" alt="the widget" class="m-2 text-center" />
  <div class="alert alert-danger text-center d-none" id="card-messages"></div>
  <form action="/payment-succeeded" method="post" name="charge_form" id="charge_form" class="d-block needs-validation charge-form" autocomplete="off" novalidate="">
      <div class="mb-3"><label for="amount" class="form-label">Amount</label><input type="text" class="form-control" id="amount" name="amount" required="" autocomplete="amount-new"></div>
      <div class="mb-3"><label for="cardholder-name" class="form-label">Cardholder Name</label><input type="text" class="form-control" id="cardholder-name" name="cardholder_name" required="" autocomplete="cardholder-name-new"></div>
      <div class="mb-3"><label for="cardholder-email" class="form-label">Cardholder Email</label><input type="email" class="form-control" id="cardholder-email" name="email" required="" autocomplete="cardholder-email-new"></div>
      <div class="mb-3"><label for="card-element" class="form-label">Credit Card</label> <div id="card-element" class="form-control"></div><div class="alert-danger text-center" id="card-errors" role="alert"></div> <div class="alert-success text-center" id="card-success" role="alert"></div></div>
      <hr>
      <a id="pay-button" href="javascript:void(0)" class="btn btn-primary" onclick="val()">Charge Card</a>
      <div id="processing-payment" class="text-center d-none"><div class="spinner-border text-primary" role="status"> <span class="visually-hidden">Loading...</span> </div></div>
      <input type="hidden" name="payment_intent" id="payment_intent">
      <input type="hidden" name="payment_method" id="payment_method">
      <input type="hidden" name="payment_amount" id="payment_amount">
      <input type="hidden" name="payment_currency" id="payment_currency">
  </form>
  {{end}}
  {{define "js"}}
      {{template "stripejs" .}}
  {{end}}
```

**Resultado:** Expusimos un servidor de estaticos, extraimos el codigo
de javascript que conecta con strapi para reutilizarlo, y creamos la
la pagina de compra de producto que carga una de nuestras imagenes, pero
aun le faltan detalles para funcionar que veremos adelante.

![compra de producto v001](./img/07_compra_de_producto_v001.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Mostrar Struct Producto en template

A, Creamos el modelo `internal\models\models.go`, el cual tendremos
en un wrapper, para tener acceso a la bd desde el modelo.

```go
package models
import ( "database/sql" "time" )

// DBModel is the type for database connection values
type DBModel struct { DB *sql.DB }

// Models is the wrapper for all models
type Models struct { DB DBModel }

// NewModels returns a model type with database connection pool
func NewModels(db *sql.DB) Models {
  return Models{ DB: DBModel{DB: db}, }
}

// Widget is the type for all widgets
type Widget struct {
  ID             int       `json:"id"`
  Name           string    `json:"name"`
  Description    string    `json:"description"`
  InventoryLevel int       `json:"inventory_level"`
  Price          int       `json:"price"`
  CreatedAt      time.Time `json:"-"`
  UpdatedAt      time.Time `json:"-"`
}
```

B, Pasamos un Widget a la plantilla de producto en `cmd\web\handlers.go`

```go
  func (app *Application) BuyProduct(w http.ResponseWriter, r *http.Request) {

    tData := TemplateData{
      API: app.config.api + "/api/payment-intent",
      Texts: map[string]string{
        "publishable_key": app.config.stripe.key,
      },
+     Data: make(map[string]interface{}),
    }

+   tData.Data["widget"] = models.Widget{
      ID:             1,
      Name:           "Some Widget",
      Description:    "Widget in store",
      InventoryLevel: 10,
      Price:          100,
    }

!   if err := app.renderTemplate(w, r, "buy_product", &tData, "stripejs"); err != nil {
      app.errorLog.Println(err)
    }
  }
```

C, Agregar info de widget al template

C,1, Agregar funcion al template para mostrar precio en `cmd\web\render.go`
<!-- uso de funciones en plantillas, uso de funciones en templates -->
```go
+ func formatCurrency(n int) string {
    f := float32(n/100)
    return fmt.Sprintf("$%.2f", f)
  }

# var functions = template.FuncMap{
    "formatCurrency": formatCurrency,
  }

  func (app *Application) parseTemplate ... {
// FIJARSE QUE ESTEMOS PASANDO LAS FUNCIONES CUANDO CREAMOS LA PLANTILLA
!   t, err = template.New(name).Funcs(functions).ParseFS(...)
  }
```

C,2, Renderizar info del widget en el template en `cmd\web\templates\buy_product.gohtml`

```html
  {{template "base" .}}
  {{define "title"}}Buy one widget{{end}}
  {{define "content"}}
<!-- LO DESTRUCTURAMOS A UNA VARIBLE PARA FACIL ACCESO -->
+ {{$widget := index .Data "widget"}}
  <h2 class="mt-3 text-center">Buy One Widget</h2>
  <hr>
  <img src="/statics/widget.png" alt="widget" class="image-fluid rounded mx-auto d-block">
  <div class="alert alert-danger text-center d-none" id="card-messages"></div>
  <form ...>
<!-- MOSTRAMOS DETALLES DEL STRUCT-->
+     <input type="hidden" name="product_id" value="{{$widget.ID}}">
<!-- ES IMPORTANTE QUE EL ID DE CANTIDAD ESTE PRESENTE, PARA MOSTRAR-->
+     <input type="hidden" name="amount" id="amount" value="{{$widget.Price}}">
<!-- USAMOS FUNCION DEL TEMPLATE -->
+     <h3 class="mt-2 text-center mb-3">{{$widget.Name}}: {{formatCurrency $widget.Price}}</h3>
+     <p>{{$widget.Description}}</p>
      <hr>
      <div class="mb-3"> <label for="cardholder-name" class="form-label">Cardholder Name</label> <input type="text" class="form-control" id="cardholder-name" name="cardholder_name" required="" autocomplete="cardholder-name-new"> </div>
      <div class="mb-3"> <label for="cardholder-email" class="form-label">Cardholder Email</label> <input type="email" class="form-control" id="cardholder-email" name="email" required="" autocomplete="cardholder-email-new"> </div>
      <div class="mb-3"><label for="card-element" class="form-label">Credit Card</label> <div id="card-element" class="form-control"></div> <div class="alert-danger text-center" id="card-errors" role="alert"></div> <div class="alert-success text-center" id="card-success" role="alert"></div></div>
      <hr>
      <a id="pay-button" href="javascript:void(0)" class="btn btn-primary" onclick="val()">Charge Card</a>
      <div id="processing-payment" class="text-center d-none"><div class="spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span> </div></div>
      <input type="hidden" name="payment_intent" id="payment_intent">
      <input type="hidden" name="payment_method" id="payment_method">
      <input type="hidden" name="payment_amount" id="payment_amount">
      <input type="hidden" name="payment_currency" id="payment_currency">
  </form>

  {{end}}

  {{define "js"}}
  {{template "stripe-js" .}}
  {{end}}
```

**Resultado:** Debemos de poder hacer un cobro con el precio fijado por
el producto.

![08_compra_de_un_producto_exitosa](./img/08_compra_de_un_producto_exitosa.png)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Consultar producto en BD y mostrarlo en api-json

```r
Usar timeout en consultas SQL;"<pre>
func (m *DBModel) GetWidget(id int) (Widget, error) {
// ESTAMOS INDICANDO 3seg PARA TIMEOUT
+   ctx, cancel := context.WithTimeout(context.Background(), 3 * time.Second)
    defer cancel()
    row := m.DB.QueryRowContext(ctx, `SELECT ... WHERE id_widget = $1`, id)
    var widget Widget
    err := row.Scan(&widget.ID, &widget.Name)
    if err != nil { return widget, err }
    return widget, nil
  }
</pre>"
```

**Objetivo:** Vamos a extraer los widgets de la BD.

A, Creamos funcion que hara la consulta en `internal\models\models.go`

```go
  import ( "database/sql" "context" "time" )

  func (m *DBModel) GetWidget(id int) (Widget, error) {
// uso de timeout en peticiones sql con timeout, uso de context en consultas sql con timeout
+   ctx, cancel := context.WithTimeout(context.Background(), 3 * time.Second)
    defer cancel()

    var widget Widget

//CUIDADO: en el curso usan "?" como parametro, pero no sirve ya que debe ser "$N" para señalar parametro
    row := m.DB.QueryRowContext(ctx, "SELECT id_widget as ID, name as Name FROM widgets WHERE id_widget = $1", id)
    err := row.Scan(&widget.ID, &widget.Name)
    if err != nil {
      return widget, err
    }

    return widget, nil
  }
```

B, Agregamos ruta en `cmd\api\routes.go`

```go
mux.Get("/api/widget/{id}", app.GetWidgetByID)
```

**Resultado:** Podemos hacer la consulta sql y mostrarla en json

![09_consulta_sql_a_api_json](./img/09_consulta_sql_a_api_json.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Migraciones de BD con Soda/Pop (mysql o postgres)

Fuente: [gobuffalo.io](https://gobuffalo.io/en/docs/overview/)

```r
Que es buffalo;"
Es un macro-framework que usa varias librerias como mux (routing), pop
(ORM), plush(template engine), bootstrap, webpack. Y nos brinda un CLI
para trabajar junto con ellas.
```

**Objetivo:** Vamos a crear todos nuestro schema usando Soda, usando
los archivos que definen el schema sin inportar que sea MySQL o Postgres

A, Instalar soda (En nuestro GO_PATH) [instrucciones](https://gobuffalo.io/es/docs/db/toolbox#instalar-soporte-cli)

B, Descomprimir [10_migrations.zip](./img/10_migrations.zip) cerca de
nuestro proyecto

C, A la altura de database.yml ejecutar `soda migrate`

**Resultado:** Debemos de tener nuestro shema creado

```r
widgets=# \dt
                List of relations
 Schema |         Name         | Type  |  Owner
--------+----------------------+-------+----------
 public | orders               | table | postgres
 public | schema_migration     | table | postgres
 public | statuses             | table | postgres
 public | transaction_statuses | table | postgres
 public | transactions         | table | postgres
 public | users                | table | postgres
 public | widgets              | table | postgres
(7 rows)
```

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Crear modelos para bd y uso soda para hacer cambios en el DB schema

A, Cremos los modelos

```go
  type Order struct {
    ID            int       `json:"id"`
    WidgetId      int       `json:"widget_id"`
    TransactionID int       `json:"transaction_id"`
    StatusID      int       `json:"status_id"`
    Quantity      int       `json:"quantity"`
    Amount        int       `json:"amount"`
    CreatedAt     time.Time `json:"-"`
    UpdatedAt     time.Time `json:"-"`
  }

  type Status struct {
    ID        int       `json:"id"`
    Name      string    `json:"name"`
    CreatedAt time.Time `json:"-"`
    UpdatedAt time.Time `json:"-"`
  }

  type TransactionStatus struct {
    ID        int       `json:"id"`
    Name      string    `json:"name"`
    CreatedAt time.Time `json:"-"`
    UpdatedAt time.Time `json:"-"`
  }

  type Transaction struct {
    ID                  int       `json:"id"`
    Amount              int       `json:"amount"`
    Currency            string    `json:"currency"`
    LastFour            string    `json:"last_four"`
    BankReturnCode      string    `json:"bank_return_code"`
    TransactionStatusId int       `json:"transaction_status_id"`
    CreatedAt           time.Time `json:"-"`
    UpdatedAt           time.Time `json:"-"`
  }

  type User struct {
    ID        int       `json:"id"`
    FirstName string    `json:"first_name"`
    LastName  string    `json:"last_name"`
    Email     string    `json:"email"`
    Password  string    `json:"password"`
    CreatedAt time.Time `json:"-"`
    UpdatedAt time.Time `json:"-"`
  }
```

<!-- uso de soda, uso de migraciones, agregar migraciones-->

B, Crear el cambio con soda

B,1, Hacemos el cambio en nuestro modelo, Agregar columna imagen a nuestro
modelo de productos.

```go
  type Widget struct {
+   Image          string    `json:"image"`
  }
```

B,2, Agregamos el cambio en soda

Fuente: [gobuffalo.io->fizz](https://gobuffalo.io/es/docs/db/fizz#agregar-una-columna)

B,2,1. Agregamos los archivos por CLI `soda generate fizz AddImageColToWidgets`.
Lo cual nos generara 2 archivos, uno XXX.down.fizz y otro XXX.up.fizz
dentro de `migrations/`

En XXX.down.fizz

```r
drop_column("widgets", "image")
```

En XXX.up.fizz

```r
add_column("widgets", "image", "string", {"default":""})
```

<!-- aplicar migraciones, -->
B,2,2. Aplicamos cambios `soda migrate`

**Resultado:** Tenemos los modelos de bd y de json en nuestro codigo, y
aprendimos a hacer migraciones.

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Producto de BD a Template

**Objetivo:** Ya que tenemos nuestros modelos y el manejo de BD resuelto,
vamos a mostrar lo conseguidom en la BD en el template.

A, Traemos la info completa del widget en `internal\models\models.go`

```go
  func (m *DBModel) GetWidget(id int) (Widget, error) {
    ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
    defer cancel()

    var widget Widget

// SOLO AUMENTAMOS LA CANTIDAD DE CAMPOS
#   row := m.DB.QueryRowContext(ctx, `
      select  id, name, description, inventory_level, price, coalesce(image, ''), created_at, updated_at
      from  widgets 
      where id = $1`, id)
    err := row.Scan(
      &widget.ID, &widget.Name, &widget.Description, &widget.InventoryLevel,
      &widget.Price, &widget.Image, &widget.CreatedAt, &widget.UpdatedAt,
    )
    if err != nil {
      return widget, err
    }

    return widget, nil
  }
```

B, Agregamos la ruta en `cmd\web\routes.go`

```go
  func (app *Application) routes() http.Handler {
+   mux.Get("/product/{id}", app.BuyProduct)
```

C, En `cmd\web\handlers.go` agregamos la consulta a BD.

```go
  func (app *Application) BuyProduct(w http.ResponseWriter, r *http.Request) {
// capturamos el id de la ruta
+   id := chi.URLParam(r, "id")
+   widgetID, _ := strconv.Atoi(id)
    tData := TemplateData{ API: app.config.api + "/api/payment-intent", Texts: map[string]string{ "publishable_key": app.config.stripe.key, }, Data: make(map[string]interface{}), }
    var errQuery error
//consultamos en BD el widget
+   tData.Data["widget"], errQuery = app.DB.GetWidget(widgetID)
    
    if errQuery != nil { app.errorLog.Println(errQuery) }
    if err := app.renderTemplate(w, r, "buy_product", &tData, "stripejs"); err != nil { app.errorLog.Println(err) }
  }
```

**Resultado:** Podemos ver la info de la BD en nuestro template.

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Insertar XXX a DB

A, Insertar nuevo widget

```go
  func (m *DBModel) InsertTransaction(txn Transaction) (int, error) {
    ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
    defer cancel()

    stmt := `
      insert into transactions (amount, currency, last_four, bank_return_code, transaction_status_id, created_at, updated_at)
      values (?, ?, ?, ?, ?, ?, ?)
    `

    result, err := m.DB.ExecContext(ctx, stmt, txn.Amount, txn.Currency, txn.LastFour, txn.BankReturnCode, txn.TransactionStatusID, time.Now(), time.Now(), )
    if err != nil { return 0, err }

    id, err := result.LastInsertId()
    if err != nil { return 0, err }

    return int(id), nil
  }
```

B, Insertar nueva orden

```go
  func (m *DBModel) InsertOrder(order Order) (int, error) {
    ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
    defer cancel()

    stmt := `insert into orders (widget_id, transaction_id, status_id, quantity, amount, created_at, updated_at)
      values ($1, $2, $3, $4, $5, $6, $7)`

    result, err := m.DB.ExecContext(ctx, stmt, order.WidgetID, order.TransactionID, order.StatusID, order.Quantity, order.Amount, time.Now(), time.Now(), )
    if err != nil { return 0, err }

    id, err := result.LastInsertId()
    if err != nil { return 0, err }

    return int(id), nil
  }
```

**Resultado:** Tenemos los metodos para hacer las inserciones en la BD.

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Strapi: conseguir 4 ultimos, expiracion, y codeResultBanck

**Objetivo:** Recuperar estos datos para guardarlos, ya que seran utiles
para darle seguimiento a una transaccion, pero igual no son muy sensibles
si es que nos hackean nuestra BD.

A, Cremos los metodos para recuperar de strapi la info quenecesitamos
en `internal\card\card.go`

```go
// DENTRO DE `stripe.PaymentMethod` ESTAN LOS 4 ULTIMOS DIJITOS DE LA TARGETA
// Y OTROS PERO ESO NO INTERESA AHORA
  func (c *Card) GetPaymentMethod(s string) (*stripe.PaymentMethod, error) {
    stripe.Key = c.Secret

    pm, err := paymentmethod.Get(s, nil)
    if err != nil {
      return nil, err
    }
    return pm, nil
  }

//RECUPERAREMOS UN `PaymentIntent` YA EXISTENTE, PERO LO PODREMOS TOMAR
// EN UN CICLO DE VIDA DIFERENTE, DEL CUAL PODREMOS SACAR INFO COMO.
  func (c *Card) RetrievePaymentIntent(id string) (*stripe.PaymentIntent, error) {
    stripe.Key = c.Secret

    pi, err := paymentintent.Get(id, nil)
    if err != nil {
      return nil, err
    }
    return pi, nil
  }
```

B, En `cmd\web\handlers.go` usamos los metodos que3 agregamos y pasamos
la info al template.

```go
  func (app *Application) PaymentSucceeded(w http.ResponseWriter, r *http.Request) {
    err := r.ParseForm() if err != nil { app.errorLog.Println(err) return }

// conseguimos la info y se la pasamos al template
+   data := getPaymentSuccessInfo(app, r)
!   if err := app.renderTemplate(w, r, "succeeded", &TemplateData{ Data: data, }); err != nil { app.errorLog.Println(err) }
  }

  func getPaymentSuccessInfo(app *Application, r *http.Request) map[string]interface{} {
// como consultatemos a strapi, necesitaremos de las llaves
    card := card.Card{ Secret: app.config.stripe.secret, Key: app.config.stripe.key }

// con estos 2 datos usaremos los metodos que creamos
    paymentIntent := r.Form.Get("payment_intent")
    paymentMethod := r.Form.Get("payment_method")

    pi, err := card.RetrievePaymentIntent(paymentIntent)
    if err != nil { app.errorLog.Println(err) return nil }

    pm, err := card.GetPaymentMethod(paymentMethod)
    if err != nil { app.errorLog.Println(err) return nil }

// conseguimos los ultimos 4, expiracion, y codigo que dio el banco del cliente.
+   lastFour := pm.Card.Last4
+   expiryMonth := pm.Card.ExpMonth
+   expiryYear := pm.Card.ExpYear
+   bankResultCode := pi.Charges.Data[0].ID

// pasamos todo a la info de plantilla
    email := r.Form.Get("email") cardHolder := r.Form.Get("cardholder_name")
    paymentAmount := r.Form.Get("payment_amount") paymentCurrency := r.Form.Get("payment_currency")
    data := make(map[string]interface{}) data["cardholder"] = cardHolder data["email"] = email
    data["pi"] = paymentIntent data["pm"] = paymentMethod data["pa"] = paymentAmount data["pc"] = paymentCurrency
    data["last_four"] = lastFour
    data["expiry_date"] = fmt.Sprintf("%v/%v", expiryMonth, expiryYear)
    data["bank_return_code"] = bankResultCode
    return data
  }
```

C, Agregamos la info a la plantilla

```html
  {{template "base" . }}
  {{define "title"}}Payment Succeeded!{{end}}
  {{define "content"}}
    <h2 class="mt-5">Payment Succeeded</h2>
    <hr>
    <p>Payment Intent: {{index .Data "pi"}}</p>
    <p>Cardholder: {{index .Data "cardholder"}}</p>
    <p>Email: {{index .Data "email"}}</p>
    <p>Payment Method: {{index .Data "pm"}}</p>
    <p>Payment Amount: {{index .Data "pa"}}</p>
    <p>Currency: {{index .Data "pc"}}</p>
<!-- AGREGAMOS LA INFO QUE CONSEGUIMOS -->
    <p>Last Four: {{index .Data "last_four"}}</p>
    <p>Bank Return Code: {{index .Data "bank_return_code"}}</p>
    <p>Expiry Date: {{index .Data "expiry_month"}}/{{index .Data "expiry_year"}}</p>
  {{end}}
```

D, Creamos los campos expiry_XXX en la DB

```sql
-- Generamos archivos de migracion soda: `soda generate fizz addColExperyDateToTransaccions`

-- Para down
drop_column("transactions", "expiry_month")
drop_column("transactions", "expiry_year")

-- Para up
add_column("transactions", "expiry_month", "integer", {"default":0})
add_column("transactions", "expiry_year", "integer", {"default":0})
```

D,1, aplicar migraciones: `soda migrate`

E, Creamos los campos expiry_XXX en el modelos

```go
  type Transaction struct {
    ...
+   ExpiryMonth         int       `json:"expiry_month"`
+   ExpiryYear          int       `json:"expiry_year"`
    ...
}
```

**Resultado:** Estamos consiguiendo los datos de señalados, y agregamos
los campos en la DB y modelo para guardarlos.

![10_strapi_consulta_ultimos4_expiracion_y_resultCodeBank](./img/10_strapi_consulta_ultimos4_expiracion_y_resultCodeBank.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Guardar cliente preparativos

**Objetivo:** Vamos a guardar info de los clientes, para fines del curso
solo guardaremos nombre e email, pero guardar demas cosas, como direccion,
contraseña, etc, seria bastante similar.

A, Creamos nueva migracion para crear la tabla clientes.

```sql
-- Generar archivos: `soda generate fizz createTableCustomer`

-- down
drop_table("customers")

--up
create_table("customers") {
  t.Column("id", "integer", {primary: true})
  t.Column("first_name", "string", {"size": 255})
  t.Column("last_name", "string", {"size": 255})
  t.Column("email", "string", {})
}

sql("alter table customers alter column created_at set default now();")
sql("alter table customers alter column updated_at set default now();")
```

<!-- migracion uno a muchos -->
B, Migracion para agregar relacion un cliente tiene muchas ordenes

```sql
-- Generamos archivos `soda generate fizz addColCustomerIDToOrders`

-- DOWN
drop_column("orders", "customer_id")

--UP
add_column("orders", "customer_id", "integer", {"unsigned": true})

add_foreign_key("orders", "customer_id", {"customers": ["id"]}, {
    "on_delete": "cascade",
    "on_update": "cascade",
})
```

C, aplicar migraciones: `soda migrate`, comprobar que los cambioes esten
en nuestra bd.

D, Agregamos modelo clientes

```go
type Customer struct {
  ID        int       `json:"id"`
  FirstName string    `json:"first_name"`
  LastName  string    `json:"last_name"`
  Email     string    `json:"email"`
  CreatedAt time.Time `json:"-"`
  UpdatedAt time.Time `json:"-"`
}
```

D,1, Actualizamos el modelo de orden para que incluya el id del cliente

```go
  type Order struct {
    ...
+   CustomerID    int       `json:"customer_id"`
  }
```

E, Agregar metodo para guardar cliente

```go
func (m *DBModel) InsertCustomer(c Customer) (int, error) {
  ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
  defer cancel()

  stmt := `insert into customers (first_name, last_name, email, created_at, updated_at)
    values ($1, $2, $3, $4, $5)`

  result, err := m.DB.ExecContext(ctx, stmt, c.FirstName, c.LastName, c.Email, time.Now(), time.Now(), )
  if err != nil { return 0, err }

  id, err := result.LastInsertId()
  if err != nil { return 0, err }

  return int(id), nil
}
```

**Resultado:** Estamos listos para guardar cliente en la BD.

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Guardar cliente, orden y transaccion en BD

**Objetivo:** Estamos listos para guardar esta info., asi que vamos
a implementarlo justo cuando la transaccion es exitosa.

A, Em `cmd\web\handlers.go`

```go
  customerID, err := app.SaveCustomer(firstName, lastName, email)
  if err != nil { app.errorLog.Println(err) return }

  amount, _ := strconv.Atoi(paymentAmount)
  txn := models.Transaction{
    Amount:              amount,
    Currency:            paymentCurrency,
    LastFour:            lastFour,
    ExpiryMonth:         int(expiryMonth),
    ExpiryYear:          int(expiryYear),
    BankReturnCode:      pi.Charges.Data[0].ID,
    TransactionStatusID: 2,
  }

```

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

### Crear Subscripcion mensual

A, Registrar suscripcion en strapi

A,1, Debemos meternos en nuestra cuanta de stripe en el dashboard

A,2, products->Add Product y nos fijamos que este en modo recurring
(recurrente) y el billing period en nuestro caso mensual.

**A,Resultado,** Debe darnos un API-ID, el cual guardaremos en nuestra BD
para luego indicarle a strapi que suscripcion cargarle al usuario.

![13_crear_producto_en_stripe](./img/12_crear_producto_en_stripe.PNG)

B, En nuestro front crearemos un `paymentMethlod` (parecido al paymentID)
con la libreria que importamos en nuestro javascript, para obtener un id
el cual enviaremos a nuestro backend para poder hacer el cargo.

```js
  
  function val() {
    //... ES EL MISMO CODIGO DE 'stripe-js.partial.gohtml' SOLO CAMBIA
    //A PARTIR DE QUE OBTENEMOS LA CANTIDAD
    let amountToCharge = document.getElementById("amount").value;

+   stripe.createPaymentMethod({ // USAMOS LA LIBRERIA DE STRIPE
+       type: 'card',
+       card: card,
+       billing_details: {
+           email: document.getElementById("cardholder-email").value,
+       },
+   })
+   .then(stripePaymentMethodHandler); // NOS DARA UN ID, ULTIMOS_4, ETC, QUE USAREMOS LUEGO
  }

// LE ENVIAREMOS EL ID QUE NOS DIO STRAPI A NUESTRO BACKEND
+ function stripePaymentMethodHandler(result) {
      if (result.error) {
          showCardError(result.error.message);
      } else {
+         let payload = {
// '$widget.PlanID' ES EL ID DE NUESTRO PRODUCTO/SERVICIO QUE REGISTRAMOS
// EN STRAPI (EL PASO A DE ESTE TITULO)
+             plan: '{{$widget.PlanID}}',
              payment_method: result.paymentMethod.id,
              email: document.getElementById("cardholder-email").value,
              last_four: result.paymentMethod.card.last4,
          }

          const requestOptions = {
              method: 'post',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify(payload),
          }
// ENVIAMOS PETICION
+         fetch("{{.API}}/api/create-customer-and-subscribe-to-plan", requestOptions)
+         .then(response => response.json())
+         .then(function(data) { console.log(data); processing.classList.add("d-none"); })
      }
  }
```

C, Crear endpoint para suscribir al cliente, usaremos el ID que obtuvimos
al generar el `paymentMethlod` para enviarselo a strapi desde nuestro
backend y asi lograr la suscripcion.

C,1, Creamos ruta y handler para resivir la informacion

C,1,1, EN `routes-api.go` Agregar ruta

```go
  func (app *application) routes() http.Handler {
    mux := chi.NewRouter()
    ...
    mux.Post("/api/create-customer-and-subscribe-to-plan", app.CreateCustomerAndSubscribeToPlan)
    return mux;
  }
```

C,1,2, En `handlers-api.go` pasamos el payload a struct, para luego
pasarselo a `card.go` y que haga la suscripcion con strapi.

```go
// CONVERTIR PAYLOAD A ESTE STRUCT
! type stripePayload struct {
    Currency      string `json:"currency"`
    Amount        string `json:"amount"`
    PaymentMethod string `json:"payment_method"`
    Email         string `json:"email"`
    LastFour      string `json:"last_four"`
    Plan          string `json:"plan"`
  }

  func (app *application) CreateCustomerAndSubscribeToPlan(w http.ResponseWriter, r *http.Request) {
    var data stripePayload
// PAYLOAD A STRUCT
    err := json.NewDecoder(r.Body).Decode(&data)
    if err != nil { app.errorLog.Println(err) return }

    app.infoLog.Println(data.Email, data.LastFour, data.PaymentMethod, data.Plan)

    resp := jsonResponse { OK: true, Message: "dummy-response", }

// STRUCT A RESPUESTA JSON
    out, err := json.MarshalIndent(resp, "", "  ")
    if err != nil { app.errorLog.Println(err) return }
    w.Header().Set("Content-Type", "application/json")
    w.Write(out)
  }
```

C,**Resultado**: En nuestro formulario que nos da el endpoint `/plans/bronze`,
podremos hacer submit y ver el `Println`. Nos falta implementar crear
la suscripcion con stripe.

![13_cableado_de_peticion_a_objeto_para_suscripcion](./img/13_cableado_de_peticion_a_objeto_para_suscripcion.gif)

D, Comunicarnos con stripe para crear un Customer y a este agregarlo
a nuestra suscripcion.

D,1, En `internal\card\card.go` creamos metodo para crear cliente y otro
metodo para crear la suscripcion, ambos haran una peticion a stripe.

```go
import (
  "github.com/stripe/stripe-go/v72/customer"
  "github.com/stripe/stripe-go/v72"
  "github.com/stripe/stripe-go/v72/paymentintent"
  "github.com/stripe/stripe-go/v72/paymentmethod"
  "github.com/stripe/stripe-go/v72/sub"
)

func (c *Card) CreateCustomer(pm, email string) (*stripe.Customer, string, error) {
  stripe.Key = c.Secret

// Creamos payload para stripe
  customerParams := &stripe.CustomerParams{
    PaymentMethod: stripe.String(pm),
    Email: stripe.String(email),
    InvoiceSettings: &stripe.CustomerInvoiceSettingsParams{
      DefaultPaymentMethod: stripe.String(pm),
    },
  }

// Hacemos peticion a Stripe
  cust, err := customer.New(customerParams)
  if err != nil {
    msg := ""
    // saber el tipo de error, ver el tipo de variable, ver el tipo de error.
    if stripeErr, ok := err.(*stripe.Error); ok {
      msg = cardErrorMessage(stripeErr.Code)
    }
    return nil, msg, err
  }
  return cust, "", nil
}

// retornara el id de la suscripcion
func (c *Card) SubscribeToPlan(cust *stripe.Customer, plan, email, last4, cardType string) (string, error) {
// Creamos payload para crear la suscripcion
  items := []*stripe.SubscriptionItemsParams{ {Plan: stripe.String(plan)} }
  params := &stripe.SubscriptionParams{ Customer: stripe.String(cust.ID), Items: items, }
  params.AddMetadata("last_four", last4)
  params.AddMetadata("card_type", cardType)
  params.AddExpand("latest_invoice.payment_intent")

// stripe crear peticion con stripe
  subscription, err := sub.New(params)
  if err != nil { return "", err }

  return subscription.ID, nil
}
```

D,2, En `cmd\api\handlers_api.go` conectamos los dos metodos anteriores
con nuestra peticion

```go
  func (app *application) CreateCustomerAndSubscribeToPlan(w http.ResponseWriter, r *http.Request) {
    var data stripePayload err := json.NewDecoder(r.Body).Decode(&data) if err != nil { app.errorLog.Println(err) return }

//Creamos instancia de card para usar los metodos de crear cliente y crear suscripcion
+   card := cards.Card{Secret: app.config.stripe.secret, Key: app.config.stripe.key, Currency: data.Currency}

// Cremos cliente ya que lo necesitamos para la suscripcion
+   stripeCustomer, msg, err := card.CreateCustomer(data.PaymentMethod, data.Email)
    if err != nil { app.errorLog.Println(err) return }

// suscribimos al cliente al cual acabamos de agregar
+   subscriptionID, err := card.SubscribeToPlan(stripeCustomer, data.Plan, data.Email, data.LastFour, "")
    if err != nil { app.errorLog.Println(err) return }
    app.infoLog.Println("subscription id is", subscriptionID)

// respondemos en json que todo fue bien
    resp := jsonResponse { OK: true, Message: msg, }
    out, err := json.MarshalIndent(resp, "", "  ") if err != nil { app.errorLog.Println(err) return } w.Header().Set("Content-Type", "application/json") w.Write(out)
  }
```

**D,Resultado:** Al ya prodemos hacer una suscripcion a strapi, el cual
podemos comprobar en su pagina si se agrego.

![14_creacion_de_suscripcion](./img/14_creacion_de_suscripcion.gif)

#### Resultado creacion de Suscripcion

[Codigo hasta este punto](./img/_6_cobros_implementados.zip)

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

### Autenticacion

**Objetivo:** Crearemos una autenticacion por atateful tokens, que son
parecidos al jwt, pero estos los guardamos en la BD, lo cual nos da la
ventaja de ya no aceptar determinado toquen.

[codigo final](./img/_7_Authenticacion.zip)

#### Creando cabledo para el login

**Objetivo:** Vamos a crear el fomulario y un endpoint para poder enviar
info y resivir una respuesta.

A, Crear formulario de login

A,1, Crear ruta al login `cmd\web\routes.go`

```go
mux.Get("/login", app.LoginPage)
```

A,2, Agregar handler `cmd\web\handlers.go`

```go
func (app *application) LoginPage(w http.ResponseWriter, r *http.Request) {
  if err := app.renderTemplate(w, r, "login", &templateData{}); err != nil {
    app.errorLog.Print(err)
  }
}
```

A,3, Agregar template `cmd\web\templates\login.page.gohtml`, el cual
enviara una peticion ajax a **/api/authenticate** que crearemos mas
abajo.

```html
{{template "base" .}}
{{define "title"}} Login {{end}}
{{define "content"}}
<div class="row">
<div class="col-md-6 offset-md-3">
    <form action="" method="post" name="login_form" id="login_form" class="d-block needs-validation charge-form" autocomplete="off" novalidate="">
    <h2 class="mt-2 text-center mb-3">Login</h2>
    <hr>
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control" id="email" name="email" required="" autocomplete="email-new">
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Password</label>
        <input type="password" class="form-control" id="password" name="password" required="" autocomplete="password-new">
    </div>
    <hr>
    <a href="javascript:void(0)" class="btn btn-primary" onclick="val()">Login</a>
</form>
</div>
</div>
{{end}}
{{define "js"}}
<script>
function val() {
    let form = document.getElementById("login_form");
    if (form.checkValidity() === false) {
        this.event.preventDefault(); this.event.stopPropagation();
        form.classList.add("was-validated");
        return;
    }
    form.classList.add("was-validated");
    let payload = { email: document.getElementById("email").value, password: document.getElementById("password").value,
    }

    const requestOptions = {
        method: 'post', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        body: JSON.stringify(payload),
    }

    fetch("{{.API}}/api/authenticate", requestOptions)
    .then(response => response.json())
    .then(response => { console.log(response); })
}
</script>
{{end}}
```

**A,Resultado:** Podremos ver el formulario si entramos a '/login' desde
nuestro navegador.

B, Crear endpoint

B,1, En `cmd\api\routes-api.go` Agregamos la ruta

```go
mux.Post("/api/authenticate", app.CreateAuthToken)
```

B,2, Agregamos `cmd\api\helpers.go` para ayudar con darle info al usuario
(leer comentarios)

```go
  package main
  import ( "encoding/json" "errors" "io" "net/http" )

// NOS FACILITARA PASAR DE JSON A STRUCT, RECORDAR QUE 'interface{}' ES UNA
// REFERENCIA, POR LO NO REGRESAMOS EL STRUCT PORQUE LO REUTILIZAMOS EL DATA.
  func (app *application) readJSON(w http.ResponseWriter, r *http.Request, data interface{}) error {

    // CREAMOS UN DECODER QUE SOLO ADMITIRA JSONS DE 1MB DE TAMAÑO MAXIMO
    maxBytes := 1048576 
    r.Body = http.MaxBytesReader(w, r.Body, int64(maxBytes))
    dec := json.NewDecoder(r.Body)
    err := dec.Decode(data)
    if err != nil { return err }

// LOS JSON DEBEN CONTENER SOLO UN EOF(END OF FILE), SI NO, SIGNIFICA QUE
// EL INPUT SE RESIVIO MAL O SE ESTA RESIVIENDO MAS DE UNO LO CUAL NO ES CORRECTO
    err = dec.Decode(&struct{}{})
    if err != io.EOF { return errors.New("body must only have a single JSON value") }
    return nil
  }

//FACILITA LA TAREA DE RESPONDER, SOLO PASAMOS (200, data, headers) Y
//ENTREGA UN JSON
  func (app *application) writeJSON(w http.ResponseWriter, status int, data interface{}, headers ...http.Header) error {
  // DATA A JSON
    out, err := json.MarshalIndent(data, "", "\t")
    if err != nil { return err }
  // AGREGAR HEADERS
    if len(headers) > 0 { for k, v := range headers[0] { w.Header()[k] = v } }
    w.Header().Set("Content-Type", "application/json")
  //AGREGAR HEADERS
    w.WriteHeader(status)
    w.Write(out)

    return nil
  }

// PARA MOSTRAR UN ERROR
  func (app *application) badRequest(w http.ResponseWriter, r *http.Request, err error) error {
    var payload struct {Error bool `json:"error"` Message string `json:"message"`}
    payload.Error = true
    payload.Message = err.Error()
    out, err := json.MarshalIndent(payload, "", "\t")
    if err != nil { return err }
    w.Header().Set("Content-Type", "application/json")
    w.Write(out)
    return nil
  }

  func (app *application) invalidCredentials(w http.ResponseWriter) error {
    var payload struct {
      Error   bool   `json:"error"`
      Message string `json:"message"`
    }

    payload.Error = true
    payload.Message = "invalid authentication credentials"

    err := app.writeJSON(w, http.StatusUnauthorized, payload)
    if err != nil {
      return err
    }
    return nil
  }
```

B,3, En `cmd\api\handlers-api.go` Agregamos el handler, el cual solo
retornara un struct **dummy**, ya que mas adelante implementaremos el auth
y crear token

```go
func (app *application) CreateAuthToken(w http.ResponseWriter, r *http.Request) {
  var userInput struct { Email string `json:"email"` Password string `json:"password"` }

  err := app.readJSON(w, r, &userInput)
  if err != nil { app.badRequest(w, r, err) return }

  var payload struct { Error bool `json:"error"` Message string `json:"message"` }
  payload.Error = false
  payload.Message = "Success!"

  _ = app.writeJSON(w, http.StatusOK, payload)
}
```

**Resultado:** Tenemos el cabledo del login, falta hacer la auth del
usuario y la creacion del token.

![15_login_cableado](./img/15_login_cableado.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Auth de usuario

**Objetivo:** Buscaremos en la BD el usuario por su email, teniendo su
hash de contraseña la comparamos con el hash de la contraseña que el
usuario ingreso.

**Pre-condiciones** debemos tener un usuario registrado en la bd y su
pass guardada en hash.

```sql
insert into users (first_name, last_name, email, password)
values ('Admin','User','admin@example.com', '$2a$12$VR1wDmweaF3ZTVgEHiJrNOSi8VcS4j0eamr96A/7iOe8vlum3O3/q');
```

A, Creamos metodo para comparar contraseñas `cmd\api\helpers.go`

```go
  import ( "golang.org/x/crypto/bcrypt" )
  func (app *application) passwordMatches(hash, password string) (bool, error) {
    err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
    if err != nil {
      switch {
      case errors.Is(err, bcrypt.ErrMismatchedHashAndPassword):
        return false, nil
      default:
        return false, err
      }
    }
    return true, nil
  }
```

B, Buscar usuario por email en la BD `internal/models/models.go`

<!-- uso de hash, comparacion de hash, -->

```go
func (m *DBModel) GetUserByEmail(email string) (User, error) {
  ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
  defer cancel()

  email = strings.ToLower(email)
  var u User

  row := m.DB.QueryRowContext(ctx, `
    select id, first_name, last_name, email, password, created_at, updated_at
    from users where email = ?`, email)

  err := row.Scan(
    &u.ID, &u.FirstName, &u.LastName, &u.Email, &u.Password, &u.CreatedAt, &u.UpdatedAt,
  )

  if err != nil { return u, err }
  return u, nil
}
```

C, Buscamos y comparamos `cmd\api\handlers-api.go`

```go
func (app *application) CreateAuthToken(w http.ResponseWriter, r *http.Request) {
  var userInput struct { Email string `json:"email"` Password string `json:"password"` }
  err := app.readJSON(w, r, &userInput)
  if err != nil { app.badRequest(w, r, err) return }

//Buscar
+ user, err := app.DB.GetUserByEmail(userInput.Email)
+ if err != nil { app.invalidCredentials(w) return }

//Comparar contraselas
+ validPassword, err := app.passwordMatches(user.Password, userInput.Password)
+ if err != nil || !validPassword { app.invalidCredentials(w) return }

  var payload struct { Error   bool   `json:"error"` Message string `json:"message"` }
  payload.Error = false
  payload.Message = "Success!"

  _ = app.writeJSON(w, http.StatusOK, payload)
}
```

**Resultado:** Nuestra api ya puede autenticar usuarios guardados en la
BD, diciendonos si esta bien o mal, .

![16_autenticar_usuario](./img/16_autenticar_usuario.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Crear token

**Objetivo:** En tanto el usuario se autentica sin problemas, entonces
generaremos el token para que el usuario pueda opearar como autenticado.

A, Creamos generador de tokens`internal\models\token.go`

```go
  package models
  import (
    "crypto/rand"     // GENERADOR ALEATORIOS VALIDOS PARA CRIPTOGRAFIA
    "crypto/sha256"   // GENERADOR DE HASH
    "encoding/base32" // GENERADOR DE BASE64
    "time" )
  const ( ScopeAuthentication = "authentication" )

  type Token struct {
    UserID    int64     `json:"-"`     
    PlainText string    `json:"token"`   // token unico del usuario
    Hash      []byte    `json:"-"`       // hash generado a partir del 'PlainText'
    Expiry    time.Time `json:"expiry"`  // fecha de expiracion
    Scope     string    `json:"-"`
  }


/*
userID: FK de  id de usuario
ttl: Tiempo en caducar del usuario
scope: Igual al rol
*/
  func GenerateToken(userID int, ttl time.Duration, scope string) (*Token, error) {
    
    token := &Token{ UserID: int64(userID), Expiry: time.Now().Add(ttl), Scope:  scope, }

//generamos Bites aleatorios
    randomBytes := make([]byte, 16)
    _, err := rand.Read(randomBytes)
    if err != nil { return nil, err }
//De los bites aleatorios generamos texto-base64
    token.PlainText = base32.StdEncoding.WithPadding(base32.NoPadding).EncodeToString(randomBytes)
//Del token generamos su Hash
    hash := sha256.Sum256(([]byte(token.PlainText)))
    token.Hash = hash[:]

    return token, nil
  }
```

B, Usamos el generador de tokens en `cmd\api\handlers-api.go`

```go
  func (app *application) CreateAuthToken(w http.ResponseWriter, r *http.Request) {
    ...
    validPassword, err := app.passwordMatches(user.Password, userInput.Password) if err != nil || !validPassword { app.invalidCredentials(w) return }

    // Generar token
+   token, err := models.GenerateToken(user.ID, 8*time.Hour, models.ScopeAuthentication)
+   if err != nil { app.badRequest(w, r, err) return }

    var payload struct {
      Error   bool          `json:"error"`
      Message string        `json:"message"`
+     Token   *models.Token `json:"token"`
    }
    payload.Error = false
    payload.Message = "Success!"
+   payload.Token = token

    _ = app.writeJSON(w, http.StatusOK, payload)
  }
```

**Resultado:** Si auth sale bien, generamos el token y se lo enviamos
al usuario, si nos fijamos tiene un string random que podremos machear
con un hash, y el expiry una fecha 8hr en el futuro el cual nmos indica
cuando caducara.

![17_generar_token_y_enviar](./img/17_generar_token_y_enviar.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Guardar el token en la BD y el localStorage

**Objetivo:** Estamos generando un Stateful token no un Stateles token (JWT),
lo que significa que lo vamos a guardar en nuestra BD, y cada vez que
lo envie el usuario, nosotros podremos auteticar el usuario.

A, Generar migracion para crear tabla en la BD.

A,1, `soda generate fizz CreateTokenTable`

```sql
-- down
drop_table("tokens")

---------------------------------------

--up
create_table("tokens") {
  t.Column("id", "integer", {primary: true})
  t.Column("user_id", "integer", {"unsigned": true})
  t.Column("name", "string", {"size": 255})
  t.Column("email", "string", {})
  t.Column("token_hash", "string", {})
}

sql("alter table tokens modify token_hash varbinary(255);")
sql("alter table tokens alter column created_at set default now();")
sql("alter table tokens alter column updated_at set default now();")

```

A,2, Implementacion para insertar

```go
  func (m *DBModel) InsertToken(t *Token, u User) error {
    ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
    defer cancel()
// SI EL USUARIO SE VUELVE A LOGUEAR ELIMINAR EL TOKEN ANTERIOR.
    _, err := m.DB.ExecContext(ctx, `delete from tokens where user_id = ?`, u.ID)
    if err != nil { return err }
  
    stmt := `insert into tokens (user_id, name, email, token_hash, created_at, updated_at)
        values (?, ?, ?, ?, ?, ?)`
    _, err := m.DB.ExecContext(ctx, stmt, u.ID, u.LastName, u.Email, t.Hash, time.Now(), time.Now(), )
    if err != nil { return err }
    return nil
  }
```

A,3, Agregarlo al handler-api

```go
  func (app *application) CreateAuthToken(w http.ResponseWriter, r *http.Request) {
    ...
    // Generar token
    token, err := models.GenerateToken(user.ID, ...)  if err != nil { ... }

    //guardar token en BD
+   err = app.DB.InsertToken(token, user)
+   if err != nil { app.badRequest(w, r, err) return }
  }
```

**A,Resultado:** Hasta este punto si probamos ya debemos de poder guardar
el token en la BD

B, Guardar el token en localStorage del navegador, tambien dar feedback

```html
  {{template "base" .}}
  {{define "title"}}Login{{end}}
  {{define "content"}}
  <div class="row"><div class="col-md-6 offset-md-3">
<!--MENSAJE OCULTO, PARA MOSTRARLO SI LAS COSAS VAN BIEN O MAL-->
+     <div class="alert alert-danger text-center d-none" id="login-messages"></div>
      <form action="" method="post" name="login_form" id="login_form" class="d-block needs-validation charge-form" autocomplete="off" novalidate="">
      <h2 class="mt-2 text-center mb-3">Login</h2><hr>
      <div class="mb-3"> <label for="email" class="form-label">Email</label> <input type="email" class="form-control" id="email" name="email" required="" autocomplete="email-new"> </div>
      <div class="mb-3"> <label for="password" class="form-label">Password</label> <input type="password" class="form-control" id="password" name="password" required="" autocomplete="password-new"></div>
      <hr><a href="javascript:void(0)" class="btn btn-primary" onclick="val()">Login</a>
  </form></div>
  </div>
  {{end}}
  {{define "js"}}
  <script>
  let loginMessages = document.getElementById("login-messages");

+ function showError(msg) {
      loginMessages.classList.add("alert-danger");
      loginMessages.classList.remove("alert-success");
      loginMessages.classList.remove("d-none");
      loginMessages.innerText = msg;
  }

+   function showSuccess() {
        loginMessages.classList.remove("alert-danger");
        loginMessages.classList.add("alert-success");
        loginMessages.classList.remove("d-none");
        loginMessages.innerText = "Login successful";
    }

  function val() {
      let form = document.getElementById("login_form");
      if (form.checkValidity() === false) {
          this.event.preventDefault(); this.event.stopPropagation(); form.classList.add("was-validated"); return;
      }
      form.classList.add("was-validated");
      let payload = { email: document.getElementById("email").value, password: document.getElementById("password").value, }
      const requestOptions = { method: 'post', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, body: JSON.stringify(payload), }

      fetch("{{.API}}/api/authenticate", requestOptions)
      .then(response => response.json())
      .then(data => { console.log(data);
+         if (data.error === false) {
// USO DE LOCALSTORAGE
+             localStorage.setItem('token', data.authentication_token.token);
+             localStorage.setItem('token_expiry', data.authentication_token.expiry);
+             showSuccess();
// USO DE REDIRECCION CON JAVASCRIPT
+           location.href = "/";
+         } else {
+             showError(data.message);
+         }
      })
  }
  </script>
  {{end}}
```

**RESULTADO:** Al hacer un login exitoso se guarda el token en el backend
como en el frontend.

![18_guardar_token_en_bd_y_localstorage](./img/18_guardar_token_en_bd_y_localstorage.png)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Mostrar/Ocultar si el usuario esta auth o no

A, En `cmd\web\templates\base.layout.gohtml` por id mostramos y ocultamos
segun si existe un token o no en el localStorage

```html
{{define "base"}}
<!doctype html><html lang="en">
  <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>{{block "title" .}}{{end}}</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">&nbsp;</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item"><a class="nav-link active" aria-current="page" href="/">Home</a></li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Products</a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="/widget/1">Buy one widget</a></li>
              <li><a class="dropdown-item" href="/plans/bronze">Subscription</a></li>
            </ul>
          </li>
<!-- agregamos id para mostrarlo/ocultarlo desde JS-->
#         <li id="vt-link" class="nav-item d-none">
            <a class="nav-link" href="/virtual-terminal">Virtual Terminal</a>
          </li>
        </ul>
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
<!-- agregamos id para mostrarlo/ocultarlo desde JS-->
#         <li id="login-link" class="nav-item d-none">
            <a class="nav-link" href="/login">Login</a></li>
          </li>
        </ul> </div> </div>
  </nav>
    <div class="container"><div class="row"> <div class="col"> {{block "content" .}} {{end}} </div> </div> </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  <script>
+ let loginLink = document.getElementById("login-link");
+ let vtLink = document.getElementById("vt-link");

+ document.addEventListener("DOMContentLoaded", function() {
    if (localStorage.getItem("token") !== null) {
      loginLink.innerHTML = '<a class="nav-link" onclick="logout()" href="#!">Logout</a>';
      vtLink.classList.remove("d-none");
    } else {
      loginLink.innerHTML = '<a class="nav-link" href="/login">Login</a>';
    }
    loginLink.classList.remove("d-none");
  })

// FUNCIONALIDAD DE LOGOUT
+ function logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("token_expiry");
    location.href = "/login";
  }
  </script>
  {{block "js" .}}{{end}}
  </body>
</html>
{{end}}
```

**Resultado:** tenemos redirecciones, ocultar/mostrar para el login y
logout, aun falta inplementar una proteccion del backend.

![login](./img/19_login.gif)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Ver si el usuario esta auth. con AJAX

**Objetivo:** Crear un metodo JS que este disponible en todos los
templates, que mande un request a un endpoint que diga si esta logeado
o no el usuario, y en caso de que no lo redirigira al login.  
Este metodo lo llamaremos en los templates que querremos proteger.

**NOTA:** La proteccion sigue siendo en el frontend, mas adelante
agregaremos proteccion en el backend.

A, En `cmd\web\templates\base.layout.gohtml` agregamos el metodo que
hara la peticion al backend, para que este disponible en todos los
templates

```html
{{define "base"}}
  ...
  <script>
  let loginLink = document.getElementById("login-link");
  ...
// PARA SABER SI ESTA AUTENTICADO
+ function checkAuth() {
    if (localStorage.getItem("token") === null) {
      location.href = "/login"; return;
    } else {
      let token = localStorage.getItem("token");
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Bearer " + token);
      const requestOptions = { method: "POST", headers: myHeaders, }
      fetch("{{.API}}/api/is-authenticated", requestOptions)
      .then(response => response.json())
      .then(function(data){
        if (data.error === true) {
          console.log("not logged in");
          location.href = "/login";
        } else {
          console.log("Logged in");
        }
      })
    }
  }
```

B, Agregar el endpoint `.../api/is-authenticated`, TEMPORAL, mas adelante
veremos la validacion del token y regresar la info del usuario.

```go
func (app *application) CheckAuthentication(w http.ResponseWriter, r *http.Request) {
  app.invalidCredentials(w)
}
```

C, Agregar el uso del metodo en X para protegerla

```html
  ...</form>{{end}}{{define "js"}}<script>
+ checkAuth()
```

**Resultado:** Si el usuario no esta autorizado es redirigido al login,
en las rutas que llamemos el metodo que pregunta al backend si el usuario
esta autenticado, **LO QUE FALTA** es que a todos los usuarios los rechaza,
adelante implementaremos la validacion del token, por si llega un usuario
valido entonces que lo deje pasar.

<!--

######################################################################

-->

----

<!--

######################################################################

-->

<!--  como validar token -->
#### Validar token con su hash en la BD

A, En `internal\models\token.go` agregamos metodo para obtener usuario
por su token

**NOTA:** para validar el token es igual que la contraseña en un login,
el cual resivimos el token en plano, luego en el back obtenemos el hash
del token, y por ultimo con el hash buscamos al usuario en la BD.  
Por esto **NUNCA DEBEMOS GUARDAR UN TOKEN EN LA BD** por si vulneran
nuestra BD, tendran el hash el cual no les sirve para auth ilisitamente.

```go
  func (m *DBModel) GetUserForToken(token string) (*User, error) {
    ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
    defer cancel()

    tokenHash := sha256.Sum256([]byte(token))
    var user User

    query := `
      select u.id, u.first_name, u.last_name, u.email
      from users u inner join tokens t on (u.id = t.user_id)
      where t.hash = ?
    `

    err := m.DB.QueryRowContext(ctx, query, tokenHash[:]).Scan(
      &user.ID, &user.FirstName, &user.LastName, &user.Email,
    )

    if err != nil { log.Println(err) return nil, err }

    return &user, nil
  }
```

B, En `cmd\api\handlers-api.go` agregamos la validacion

```go
  func (app *application) CheckAuthentication(w http.ResponseWriter, r *http.Request) {
// Validar usuario
+   user, err := app.authenticateToken(r)
    if err != nil { app.invalidCredentials(w) return }

// El usuario es valido
    var payload struct { Error   bool   `json:"error"` Message string `json:"message"` }
    payload.Error = false
    payload.Message = fmt.Sprintf("authenticated user %s", user.Email)
    app.writeJSON(w, http.StatusOK, payload)
  }

  func (app *application) authenticateToken(r *http.Request) (*models.User, error) {
//validar que exista el token  obtener header de peticion
+   authorizationHeader := r.Header.Get("Authorization")
    if authorizationHeader == "" { return nil, errors.New("no authorization header received") }

//validar formato del token
    headerParts := strings.Split(authorizationHeader, " ")
    if len(headerParts) != 2 || headerParts[0] != "Bearer" { return nil, errors.New("no authorization header received") }
    token := headerParts[1]
    if len(token) != 26 { return nil, errors.New("authentication token wrong size") }

//Validamos token y obtenemos usuario
    user, err := app.DB.GetUserForToken(token)
    if err != nil { return nil, errors.New("no matching user found") }

    return user, nil
  }
```

**Resultado:** Ya tenemos bien implementado la redireccion si el usuario
es invalido, y lo acepta si es valido.

![19_reDireccion_por_no_autorizado](./img/20_reDireccion_por_no_autorizado.gif)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Validar caducidad del token en el backend

**Objetivo:** Estamos validando el token pero no su caducidad, por lo
que vamos a inplementarlo.

A, Agregamos una columna en la tabla de tokens

```sql
-- CREAMOS ARCHIVOS DE MIGRACION
---------> soda generate fizz AddOnTokensColumnExpiry

-- XXX.down.fizz
drop_column("tokens", "expiry")

--En XXX.up.fizz
add_column("tokens", "expiry", "timestamp", {})

-- APLICAMOS MIGRACION
---------> `soda migrate`
```

B, Agregamos propiedad al model en `internal\models\token.go`

```go
  func (m *DBModel) InsertToken(t *Token, u User) error {
    ...
// AGREGAMOS CAMPO de expiry
#   stmt = `insert into tokens (user_id, name, email, token_hash, expiry, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)`
#   _, err = m.DB.ExecContext(ctx, stmt, u.ID, u.LastName, u.Email, t.Hash, t.Expiry, time.Now(), time.Now(), )
    ...
  }
```

C, Al buscar el token agregamos el filtro de solo retornarlo si la fecha
actual es menor a la fecha de expiracion.

```go
  func (m *DBModel) GetUserForToken(token string) (*User, error) {
    ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
    defer cancel()

    tokenHash := sha256.Sum256([]byte(token))
    var user User

// AGREGAMOS CONDICION `and t.expiry > ?` EN EL WHERE 
    query := ` select u.id, u.first_name, u.last_name, u.email from users u inner join tokens t on (u.id = t.user_id)
#     where t.token_hash = ? and t.expiry > ? `
// EL PÀRAMETRO QUE AGREGAMOS ES EL AHORA
#   err := m.DB.QueryRowContext(ctx, query, tokenHash[:], time.Now())
  }
```

**Resultado:** Ya valida la expiracion, podemos provarla modificando la
fecha y volviendo a entrar a la terminal virtual. **PERO FALTA** proteger
el api/endpoint que realiza el cobro.

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Validar token en el middelware del backend

<!-- uso de middelware a determinadas rutas, agregar validacion a x rutas, validar x rutas-->

A, Creamos middelware en `cmd\api\middleware.go`

```go
  import "net/http"
  func (app *application) Auth(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//USAMOS LA MISMA VALIDACION QUE CREAMOS
!     _, err := app.authenticateToken(r)
      if err != nil { app.invalidCredentials(w) return }
//PARA QUE CONTINUE LA PETICION
!     next.ServeHTTP(w, r)
    })
  }
```

B, Agregamos middelware, solo para determinadas rutas

```go
  func (app *application) routes() http.Handler {
    ...
+   mux.Route("/api/admin", func(mux chi.Router) {
+     mux.Use(app.Auth)
// TODAS LAS RUTAS QUE ESTEN ADENTRO TENDRAN LA VALIDACION EXTRA, Y LA
// RUTA SE ANIDARA EN ESTE CASO SE CONVERTIRA A '/api/admin/test'
!     mux.Get("/test", func(w http.ResponseWriter, r *http.Request){
        w.Write([]byte("got in"))
      })
    })
    return mux
  }
```

**Resultado:** la ruta `...:4001/api/admin/test` esta protegida y solo
obtendremos el mensaje `got in` si le pasamos el token `Authorization: Bearer [EL_TOKEN]`

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

### Cambiar clave o email

**Objetivo:** Implementar el sistema por si el usuario olvida su clave,
o quiere cambiar su email, lo cual tendremos que enviar un email con
un enlace para poder identificar al usuario.

- Resultado: Cambio de contraseña de usuario seguro
  - Validar que exista el email en la BD
  - Generar link firmado y con caducidad
  - Enviar email con el link firmado
  - validar link firmado
  - Envio y resivo de email encriptado para evitar modificaciones

- Resultado
  - [.zip](./img/_8_resetear_contrasenia.zip)
  - [.gif](./img/_8_resetear_contrasenia.gif)

#### Crear cableado para olvide clave y comprobar si existe el email

A, Agregar formulario para enviar el correo y que use un endpoint para
enviar un email indicado.

A,1, agregar enlace

```html
<p class="mt-2">
  <small><a href="/forgot-password">Forgot password?</a>
</p>
```

A,2, Agregar ruta

```go
mux.Get("/forgot-password", app.ForgotPassword)
```

A,3, Agregar handler

```go
func (app *application) ForgotPassword(w http.ResponseWriter, r *http.Request) {
  if err := app.renderTemplate(w, r, "forgot-password", &templateData{}); err != nil {
    app.errorLog.Print(err)
  }
}
```

A,4, Agregar template

```html
{{template "base" .}}
{{define "title"}}Forgot Password{{end}}
{{define "content"}}
<div class="row">
    <div class="col-md-6 offset-md-3">
    <div class="alert alert-danger text-center d-none" id="messages"></div>
        <form action="" method="post" name="forgot_form" id="forgot_form" class="d-block needs-validation" autocomplete="off" novalidate="">
            <h2 class="mt-2 text-center mb-3">Forgot Password</h2>
            <hr>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" name="email" required="" autocomplete="email-new">
            </div>
            <hr>
            <a href="javascript:void(0)" class="btn btn-primary" onclick="val()">Send Password Reset Link</a>
        </form>
    </div>
</div>
{{end}}
{{define "js"}}
<script>
let messages = document.getElementById("messages");
function showError(msg) {...}
function showSuccess() {...}
function val() {
    let form = document.getElementById("forgot_form");
    if (form.checkValidity() === false) {....}
    form.classList.add("was-validated");
// ENVIAMOS SOLAMENTE EL EMAIL    
    let payload = { email: document.getElementById("email").value, }
    const requestOptions = {
        method: 'post',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        body: JSON.stringify(payload),
    }

    fetch("{{.API}}/api/forgot-password", requestOptions)
    .then(response => response.json())
    .then(data => {
        if (data.error === false) { showSuccess(); }
        else { showError(data.message); }
    })
}
</script>
{{end}}
```

B, Agregar endpoint `/api/forgot-password` para que resiva el email,
y haga el proceso de enviar el correo.

B,1, Agregar ruta

```go
mux.Post("/api/forgot-password", app.SendPasswordResetEmail)
```

B,2, Agregar handler, el cual capturara el email y buscara en la BD si
existe, si no existe mandar un no encontrado y detener el envio.

```go
  func (app *application) SendPasswordResetEmail(w http.ResponseWriter, r *http.Request) {
// de payload a struct    
    var payload struct { Email string `json:"email"` }
    err := app.readJSON(w, r, &payload)
    if err != nil { app.badRequest(w, r, err) return }

    app.infoLog.Printf("Sending email to %s... \n", payload.Email)

// COMPROBAR SI EXISTE EL EMAIL
    _, err = app.DB.GetUserByEmail(payload.Email)
    if err != nil {
      var resp struct {
        Error   bool   `json:"error"`
        Message string `json:"message"`
      }
      resp.Error = true
      resp.Message = "No matching email found on our system"
      app.writeJSON(w, http.StatusAccepted, resp)
      return
    }

// de struct a respuesta
    var resp struct { Error   bool   `json:"error"` Message string `json:"message"` }
    resp.Error = false
    app.writeJSON(w, http.StatusCreated, resp)
  }
```

**Resultado:** Tenemos solo la interaccion con el usuario (el cascaron)
de alvide contraseña. **TAMBIEN** comprueba si existe email en la bd, y
si no existe la UI manda mensaje de error.

![21_cableado_olvide_clave](./img/21_cableado_olvide_clave.gif)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Implementar envio de email

A, importar paquete que hara el envio `go get github.com/xhit/go-simple-mail/v72`

B, Necesitamos tener a la mano un servicio/servidor de smtp, en el curso
se usa **mailtrap.io**, el cual podemos crear una cuenta gratuita,
y debemos de obtener las **credenciales** a mano.

C, `cmd\api\api.go` Configurar el plugin, para usar un servicio smtp.

**IMPORTANTE** cambiar las credenciales por nuestro servicio de smtp.

```go
  type config struct {
    ...
    smtp struct {
      host     string
      port     int
      username string
      password string
    }
    secretkey string
    frontend string
  }

  func main() {
    flag.StringVar(&cfg.db.dsn, "dsn", "xxx:xxx@tcp...", "DSN")

// agregamos las variables desde var de entorno o de bash-script
    flag.StringVar(&cfg.smtp.host,     "smtphost",  "smtp.mailtrap.io", "smtp host")
    flag.IntVar(&cfg.smtp.port,        "smtpport",  587,                "smtp port")
    flag.StringVar(&cfg.frontend, "frontend", "http://localhost:4000",  "url to front end")

// ASIGNAMOS CREDENCIALES
    flag.StringVar(&cfg.smtp.username, "smtpuser",  "XXX",              "smtp user")
    flag.StringVar(&cfg.smtp.password, "smtppass",  "XXX",              "smtp password")
    flag.StringVar(&cfg.secretkey,     "secret",    "XXX",              "secret api-key")
```

D, `cmd\api\templates\X.X` Crear las plantillas para enviar en texto
plano o en html. La terminacion del archivo es libre.

D,1, Plantilla en html `cmd\api\templates\password-reset.html.tmpl`

```html
{{define "body"}}
<!doctype html>
<html>
<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head>
<body>
    <p>Hello:</p>
    <p>You recently requested a link to reset your password.</p>
    <p>Click on the link below to get started:</p>
    <p><a href="{{.Link}}">{{.Link}}</a></p>
    <p>This link expires in 60 minutes.</p>
    <p>--<br>
    Widgets Co.
    </p>
</body>
</html>
{{end}}
```

D,1, Plantilla en plano `cmd\api\templates\password-reset.plain.tmpl`

```r
{{define "body"}}
Hello:

You recently requested a link to reset your password.

Visit the link below to get started:

{{.Link}}

This link expires in 60 minutes.

--
Widgets Co.
{{end}}
```

E, `cmd\api\mailer.go` Crear el metodo que enviara el email, se generaran
ambas donde al pasarle una la otra fallara, y se enviara la que si se
genero.

```go
  package main
  import ( "bytes" "embed" "fmt" "html/template" "time"
    mail "github.com/xhit/go-simple-mail/v2"
  )
  //go:embed templates
  var emailTemplateFS embed.FS

  func (app *application) SendMail(from, to, subject, tmpl string, data interface{}) error {

//CREAR PLATILLA EN HTML
    templateToRender := fmt.Sprintf("templates/%s.html.tmpl", tmpl)
    t, err := template.New("email-html").ParseFS(emailTemplateFS, templateToRender)
    if err != nil {
      app.errorLog.Println(err)
      return err
    }
    var tplHtml bytes.Buffer
    if err = t.ExecuteTemplate(&tplHtml, "body", data); err != nil {
      app.errorLog.Println(err)
      return err
    }
    formattedMessage := tplHtml.String()

//CREAR PLANTILLA EN TEXTO PLANO
    templateToRender = fmt.Sprintf("templates/%s.plain.tmpl", tmpl)
    t, err = template.New("email-plain").ParseFS(emailTemplateFS, templateToRender)
    if err != nil {
      app.errorLog.Println(err)
      return err
    }
    var tplPlain bytes.Buffer
    if err = t.ExecuteTemplate(&tplPlain, "body", data); err != nil {
      app.errorLog.Println(err)
      return err
    }
    plainMessage := tplPlain.String()

//LOG DE FUNCIONO LA PLANTILLA
    app.infoLog.Println("PLAIN:", plainMessage)
    app.infoLog.Println("HTML :", formattedMessage)

//CONFIG SMTP
    server := mail.NewSMTPClient()
    server.Encryption = mail.EncryptionTLS
    server.KeepAlive = false
    server.ConnectTimeout = 10 * time.Second
    server.SendTimeout = 10 * time.Second

//SET CREDENCIALS OF SMTP
    server.Host = app.config.smtp.host
    server.Port = app.config.smtp.port
    server.Username = app.config.smtp.username
    server.Password = app.config.smtp.password

//CHECK CONECTION
    smtpClient, err := server.Connect()
    if err != nil { return err }

//CREAR EMAIL
    email := mail.NewMSG()
    email.SetFrom(from).AddTo(to).SetSubject(subject)
    email.SetBody(mail.TextHTML, formattedMessage)
    email.AddAlternative(mail.TextPlain, plainMessage)

//ENVIAR EMAIL
    err = email.Send(smtpClient)
    if err != nil { app.errorLog.Println(err) return err }

//LOG QUE SE ENVIO EXITOSAMENTE
    app.infoLog.Println("email sended succefully")
    return nil
  }
```

F, Agregar el metodo de envio de email al handler

```go
  func (app *application) SendPasswordResetEmail(w http.ResponseWriter, r *http.Request) {
    ...

//LO IMPLEMENTAREMOS LUEGO EL GENERAR UN SECRETO
+   var data struct { Link string }
+   data.Link = "algun secreto"

// send mail
+   err = app.SendMail("info@widgets.com", payload.Email, "Password Reset Request", "password-reset", data)
    if err != nil {
      app.errorLog.Println(err)
      app.badRequest(w, r, err)
      return
    }
    ...
  }
```

**Resultado:** Podemos enviar emails al servicio smtp que hayamos apuntado.

![22_envio_de_email_a_mailtrap](./img/22_envio_de_email_a_mailtrap.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Crear url firmada y crear implementacion de validacion y caducidad

<!-- crear mensaje firmado, uso de mensaje firmado, firmar mensaje-->

**Teoria:** Para validar al usuario podriamos hacerlo de la misma manera
que manejamos el stateful token, pero para evitarnos el tema de generar
token, generar hash guardarlo, etc, vamos a usar un mensaje firmado, el
cual es un mensaje con una llave que valida que no ha sido modificado,
y tambien podremos asignar una caducidad.

**Objetivo:** Generar una url firmada para que no pueda ser modificada,
ser validada y agregarle caducidad, asi sabremos que el usuario

1, Instalar dependencia que nos ayudara a generar el mensaje firmado

```r
go get github.com/bwmarrin/go-alone
```

2, Creamos un wrapper para usar el paquete facilmente, con dos metodos
uno para encriptar y otro para desencriptar.

**Nota** Necesitaremos una clave secreta.

2,1, `cmd\api\api.go` Creamos contraseña para generar las firmas

```go
  type config struct {
    ...
    secretkey string //PARA GENERAR EL FIRMADO
    frontend  string //PARA AGREGAR LA URL INDICADA AL IMAIL
  }

  func main() {
    var cfg config
    ...
    flag.StringVar(&cfg.frontend, "frontend", "http://localhost:4000", "url to front end")
    flag.StringVar(&cfg.secretkey, "secret", "NoShareWithAnyone", "secret key")
```

2,2, Creamos la implementacion `internal\urlsigner\signer.go`

```go
  package urlsigner

  import (
    "fmt" "strings" "time"
    goalone "github.com/bwmarrin/go-alone"
  )

  type Signer struct {
    Secret []byte
  }

  func (s *Signer) GenerateTokenFromString(data string) string {
    var urlToSign string

    crypt := goalone.New(s.Secret, goalone.Timestamp)
//VER SI ES EL PRIMER PARAMETRO O N PARAMETRO
    if strings.Contains(data, "?") {
      urlToSign = fmt.Sprintf("%s&hash=", data)
    } else {
      urlToSign = fmt.Sprintf("%s?hash=", data)
    }

// GENERAMOS FIRMA
    tokenBytes := crypt.Sign([]byte(urlToSign))
    token := string(tokenBytes)
    return token
  }

  func (s *Signer) VerifyToken(token string) bool {
    crypt := goalone.New(s.Secret, goalone.Timestamp)
    _, err := crypt.Unsign([]byte(token))

    if err != nil { fmt.Println(err) return false }

    return true
  }

  func (s *Signer) Expired(token string, minutesUntilExpire int) bool {
    crypt := goalone.New(s.Secret, goalone.Timestamp)
    ts := crypt.Parse([]byte(token))

    return time.Since(ts.Timestamp) > time.Duration(minutesUntilExpire) * time.Minute
  }
```

3, usar el firmador cuando generamos el email `cmd\api\handlers-api.go`.

```go
  func (app *application) SendPasswordResetEmail(w http.ResponseWriter, r *http.Request) {
    ...
    var data struct { Link string }

+   sign := urlsigner.Signer {
      Secret: []byte(app.config.secretkey),
    }
+   link := fmt.Sprintf("%s/reset-password?email=%s", app.config.frontend, payload.Email)
+   data.Link = sign.GenerateTokenFromString(link)

// send mail
!   err = app.SendMail("info@widgets.com", payload.Email, "Password Reset Request", "password-reset", data)
    ...
  }
```

**Resultado:** Si enviamos un correo debemos de la url que tiene un
hash el cual valida que la url no haiga sido modificada.

![23_url_firmada](./img/23_url_firmada.PNG)

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Encriptacion y desencriptacion

**Problema:** Debemos de crear un formulario para cambiar la contraseña
de un usuario, pero podriamos tener una **vulnerabilidad**, porque cualquiera
podria cambiar el email del input oculto de nuestro formulario y cambiar
la contraseña de cualquier usuario, lo cual es muy peligroso.

**Solucion:** Debemos de enviar el email a poner en el formulario de
forma encriptada y resivirlo de forma encriptada, de esta forma no cualquiera
podra cambiar el email, por el de otro usuario, por que necesitara nuestra
contraseña secreta para generar encriptados como los nuestros.

1, Crear encriptador

1,1, Es necesario tener un clave para generar nuestro encriptado de 32
caracteres exactamente `cmd\api\api.go`

```go
flag.StringVar(&cfg.secretkey, "secret", "NoShareWithAnyone_NoShareWithAny", "secret key")
```

1,2, Creamos implmentacion `internal\encryption\encryption.go`

```go
  package encryption

  import (
    "crypto/aes" "crypto/cipher" "crypto/rand" "encoding/base64" "fmt" "io"
  )

  type Encryption struct {
    Key []byte
  }

  func (e *Encryption) Encrypt(text string) (string, error) {
    plaintext := []byte(text)

    block, err := aes.NewCipher(e.Key)
    if err != nil {
      return "", err
    }

    cipherText := make([]byte, aes.BlockSize+len(plaintext))
    iv := cipherText[:aes.BlockSize]
    if _, err := io.ReadFull(rand.Reader, iv); err != nil {
      return "", err
    }

    stream := cipher.NewCFBEncrypter(block, iv)
    stream.XORKeyStream(cipherText[aes.BlockSize:], plaintext)

    return base64.URLEncoding.EncodeToString(cipherText), nil
  }

  func (e *Encryption) Decrypt(cryptoText string) (string, error) {
    cipherText, _ := base64.URLEncoding.DecodeString(cryptoText)

    block, err := aes.NewCipher(e.Key)
    if err != nil {
      return "", err
    }

    if len(cipherText) < aes.BlockSize {
      return "", err
    }

    iv := cipherText[:aes.BlockSize]
    cipherText = cipherText[aes.BlockSize:]

    stream := cipher.NewCFBDecrypter(block, iv)
    stream.XORKeyStream(cipherText, cipherText)

    return fmt.Sprintf("%s", cipherText), nil
  }
```

**Resultado:** Con estos dos metodos podremos encriptar y desencriptar,
de esta forma podremos enviar info para luego resivirla con la seguridad
de que no la cambiaran para malas intenciones.

<!--

######################################################################

-->

----

<!--

######################################################################

-->

#### Validar la url y cambiar la contraseña

1, Crear formulario, este llamara a una api que hara el cambio

1,1, Agregar ruta

```go
mux.Get("/reset-password", app.ShowResetPassword)
```

1,2, Agregar handler, leer comentario

```go
  func (app *application) ShowResetPassword(w http.ResponseWriter, r *http.Request) {
    email := r.URL.Query().Get("email")

//obtener la url, obtener url, ver la url actual
    theURL := r.RequestURI
    testURL := fmt.Sprintf("%s%s", app.config.frontend, theURL)

//VALIDAMOS URL
    signer := urlsigner.Signer{ Secret: []byte(app.config.secretkey), }
    valid := signer.VerifyToken(testURL)
    if !valid { app.errorLog.Println("Invalid url - tampering detected") return }

// make sure not expired
    expired := signer.Expired(testURL, 60)
    if expired { app.errorLog.Println("Link expired") return }

//ENCRIPTAMOS EMAIL PARA QUE NO PUEDA SER CAMBIADO
    encyrptor := encryption.Encryption{ Key: []byte(app.config.secretkey), }
    encryptedEmail, err := encyrptor.Encrypt(email)
    if err != nil { app.errorLog.Println("Encryption failed") return }

//MOSTRAMOS TEMPLATE CON EL EMAIL ENCRIPTADO QUE EL USUARIO QUIERE CAMBIAR
    data := make(map[string]interface{})
    data["email"] = encryptedEmail
    if err := app.renderTemplate(w, r, "reset-password", &templateData{
      Data: data,
    }); err != nil {
      app.errorLog.Print(err)
    }
  }
```

1,3, Crear template `cmd\web\templates\reset-password.page.gohtml`

```html
{{template "base" .}}
{{define "title"}}Reset Password{{end}}
{{define "content"}}
<div class="row">
    <div class="col-md-6 offset-md-3">
    <div class="alert alert-danger text-center d-none" id="messages"></div>
        <form action="" method="post" name="reset_form" id="reset_form" class="d-block needs-validation" autocomplete="off" novalidate="">
            <h2 class="mt-2 text-center mb-3">Reset Password</h2>
            <hr>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" id="password" name="password" required="" autocomplete="password-new">
            </div>
            <div class="mb-3">
                <label for="verify-password" class="form-label">Verify Password</label>
                <input type="password" class="form-control" id="verify-password" name="verify-password" required="" autocomplete="verify-password-new">
            </div>
            <hr>
            <a href="javascript:void(0)" class="btn btn-primary" onclick="val()">Reset Password</a>
        </form>
    </div>
</div>
{{end}}

{{define "js"}}
<script>
let messages = document.getElementById("messages");
function showError(msg) {
  messages.classList.add("alert-danger"); messages.classList.remove("alert-success"); messages.classList.remove("d-none"); messages.innerText = msg;
}

function showSuccess() {
    messages.classList.remove("alert-danger"); messages.classList.add("alert-success"); messages.classList.remove("d-none"); messages.innerText = "Password reset!";
}

function val() {
    let form = document.getElementById("reset_form");
    if (form.checkValidity() === false) {
        this.event.preventDefault();
        this.event.stopPropagation();
        form.classList.add("was-validated");
        return;
    }
    form.classList.add("was-validated");

    if (document.getElementById("password").value !== document.getElementById("verify-password").value) {
        showError("Passwords do not match!")
        return
    }

    let payload = {
        password: document.getElementById("password").value,
//IMPORTANTE QUE EL EL EMAIL LLEGE ENCRIPTADO
        email: "{{index .Data "email"}}",
    }

    const requestOptions = {
        method: 'post',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        body: JSON.stringify(payload),
    }
    fetch("{{.API}}/api/reset-password", requestOptions)
    .then(response => response.json())
    .then(data => {
        console.log(data);
        if (data.error === false) {
            showSuccess();
            setTimeout(function() { location.href = "/login"; }, 2000)
        } else {
            showError(data.message);
        }
    })
}
</script>
{{end}}
```

**Esta este punto** tenemos implementado el front.

2, Crear api que cambiara la contraseña

2,1, Agregar ruta

```go
mux.Post("/api/reset-password", app.ResetPassword)
```

2,2, Agregar handler, leer comentarios

```go
  func (app *application) ResetPassword(w http.ResponseWriter, r *http.Request) {
    var payload struct { Email    string `json:"email"` Password string `json:"password"` }
    err := app.readJSON(w, r, &payload)
    if err != nil { app.badRequest(w, r, err) return }

//DES-ENCRIPTAMOS EMAIL QUE NOS ENVIO EL FORMULARIO
    encyrptor := encryption.Encryption{ Key: []byte(app.config.secretkey), }
    realEmail, err := encyrptor.Decrypt(payload.Email)
    if err != nil { app.badRequest(w, r, err) return }

// NOTAR QUE OBTENEMOS EL EMAIL ENCRIPTADO, LO DESENCRIPTAMOS Y LO PASAMOS
+   user, err := app.DB.GetUserByEmail(realEmail)
    if err != nil { app.badRequest(w, r, err) return }

//GENERAR HASH DE LA NUEVA CONTRASEÑA
    newHash, err := bcrypt.GenerateFromPassword([]byte(payload.Password), 12)
    if err != nil { app.badRequest(w, r, err) return }

//ACTUALIZAR USUARIO CON NUEVA CONTRASEÑA
    err = app.DB.UpdatePasswordForUser(user, string(newHash))
    if err != nil { app.badRequest(w, r, err) return }

// RESPONDER EXITO
    var resp struct { Error   bool   `json:"error"` Message string `json:"message"` }
    resp.Error = false
    resp.Message = "password changed"
    app.writeJSON(w, http.StatusCreated, resp)
  }
```

2,3, Crear metodo para cambiar pass en la bd, el cual se la el hash

```go
  func (m *DBModel) UpdatePasswordForUser(u User, hash string) error {
    ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
    defer cancel()

    stmt := `update users set password = ? where id = ?`
    _, err := m.DB.ExecContext(ctx, stmt, hash, u.ID)
    if err != nil {
      return err
    }

    return nil
  }
```

- Resultado [.zip](./img/_8_resetear_contrasenia.zip)
  - Validar que exista el email en la BD
  - Generar link firmado y con caducidad
  - Enviar email con el link firmado
  - validar link firmado
  - Envio y resivo de email encriptado para evitar modificaciones

![_8_resetear_contrasenia](./img/_8_resetear_contrasenia.gif)

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

<!--

######################################################################
######################################################################

-->

----

<!--

######################################################################
######################################################################

-->

### Micro-sercicio Rest

- Codigo resultado: [_9_source-code](./img/_9_source-code.zip)

**Objetivo**; Vamos a crear un micro-servicio, le pasaremos la informacion
de una factura/venta, y este generara un PDF y lo enviara por correo.

1, Creamos otro proyecto con su propio main, el cual solo necesitara las
credenciales del servidor smtp

```go
package main
import ( "flag" "fmt" "log" "net/http" "os" "time" )
const version = "1.0.0"
type config struct { port int smtp struct { host     string port     int username string password string } frontend string }
type application struct { config   config infoLog  *log.Logger errorLog *log.Logger version  string }
func main() {
  var cfg config
  flag.IntVar(&cfg.port, "port", 5000, "Server port to listen on")
  flag.StringVar(&cfg.smtp.host, "smtphost", "smtp.mailtrap.io", "smtp host")
  flag.StringVar(&cfg.smtp.username, "smtpuser", "XXXX", "smtp user")
  flag.StringVar(&cfg.smtp.password, "smtppass", "XXXXX", "smtp password")
  flag.IntVar(&cfg.smtp.port, "smtpport", 587, "smtp port")
  flag.StringVar(&cfg.frontend, "frontend", "http://localhost:4000", "url to front end")
  flag.Parse()
  infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
  errorLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
  app := &application{ config:   cfg, infoLog:  infoLog, errorLog: errorLog, version:  version, }
  app.CreateDirIfNotExist("./invoices")
  err := app.serve()
  if err != nil { log.Fatal(err) }
}

func (app *application) serve() error {
  srv := &http.Server{ ... }
  app.infoLog.Printf("Starting invoice microservice on port %d\n", app.config.port)
  return srv.ListenAndServe()
}
```

2, Agregamos la ruta que usara otros servicios

```go
package main
import ( "net/http" "github.com/go-chi/chi/v5" "github.com/go-chi/cors" )
func (app *application) routes() http.Handler {
  mux := chi.NewRouter()
  mux.Use(cors.Handler(cors.Options{...}))
  mux.Post("/invoice/create-and-send", app.CreateAndSendInvoice)
  return mux
}
```

<!-- crear pdf-->

3, Instalar dependencia: [gofpdf](https://github.com/phpdave11/gofpdf)

4, Agregamos los handlers, el cual creara el pdf

```go
  package main
  import ( "fmt" "net/http" "time"
    "github.com/phpdave11/gofpdf"
    "github.com/phpdave11/gofpdf/contrib/gofpdi"
  )

  // Order describes the json payload received by this microservice
  type Order struct {
    ID        int       `json:"id"`
    Quantity  int       `json:"quantity"`
    Amount    int       `json:"amount"`
    Product   string    `json:"product"`
    FirstName string    `json:"first_name"`
    LastName  string    `json:"last_name"`
    Email     string    `json:"email"`
    CreatedAt time.Time `json:"created_at"`
  }

  func (app *application) CreateAndSendInvoice(w http.ResponseWriter, r *http.Request) {
// receive json
    var order Order
    err := app.readJSON(w, r, &order)
    if err != nil { app.badRequest(w, r, err) return }

// generate a pdf invoice
    err = app.createInvoicePDF(order)
    if err != nil { app.badRequest(w, r, err) return }

// create mail attachment
    attachments := []string { fmt.Sprintf("./invoices/%d.pdf", order.ID), }

// send mail with attachment
    err = app.SendMail("info@widgets.com", order.Email, "Your invoice", "invoice", attachments, nil)
    if err != nil { app.badRequest(w, r, err) return }

// send reponse
    var resp struct { Error   bool   `json:"error"` Message string `json:"message"` }
    resp.Error = false
    resp.Message = fmt.Sprintf("Invoice %d.pdf created and sent to %s", order.ID, order.Email)
    app.writeJSON(w, http.StatusCreated, resp)
  }

// createInvoicePDF generates a PDF version of the invoice
  func (app *application) createInvoicePDF(order Order) error {
//configuracion
    pdf := gofpdf.New("P", "mm", "Letter", "")
    pdf.SetMargins(10, 13, 10)
    pdf.SetAutoPageBreak(true, 0)

// uso de plantilla
    importer := gofpdi.NewImporter()
    t := importer.ImportPage(pdf, "./pdf-templates/invoice.pdf", 1, "/MediaBox")
    pdf.AddPage()
    importer.UseImportedTemplate(pdf, t, 0, 0, 215.9, 0)

// Escribir info
    pdf.SetY(50)
    pdf.SetX(10)
    pdf.SetFont("Times", "", 11)
    pdf.CellFormat(97, 8, fmt.Sprintf("Attention: %s %s", order.FirstName, order.LastName), "", 0, "L", false, 0, "")
    pdf.Ln(5)
    pdf.CellFormat(97, 8, order.Email, "", 0, "L", false, 0, "")
    pdf.Ln(5)
    pdf.CellFormat(97, 8, order.CreatedAt.Format("2006-01-02"), "", 0, "L", false, 0, "")
    pdf.SetX(58)
    pdf.SetY(93)
    pdf.CellFormat(155, 8, order.Product, "", 0, "L", false, 0, "")
    pdf.SetX(166)
    pdf.CellFormat(20, 8, fmt.Sprintf("%d", order.Quantity), "", 0, "C", false, 0, "")
    pdf.SetX(185)
    pdf.CellFormat(20, 8, fmt.Sprintf("$%.2f", float32(order.Amount / 100.0)), "", 0, "R", false, 0, "")

//Guardar pdf en disco
    invoicePath := fmt.Sprintf("./invoices/%d.pdf", order.ID)
    err := pdf.OutputFileAndClose(invoicePath)
    if err != nil { return err }

    return nil
  }
```

5, Implementamos que se pueda agregar archivos al mail.

```go
  package main

  import ( "bytes" "embed" "fmt" "html/template" "time"
    mail "github.com/xhit/go-simple-mail/v2"
  )

  //go:embed email-templates
  var emailTemplateFS embed.FS

  func (app *application) SendMail(from, to, subject, tmpl string, attachments []string, data interface{}) error {
    templateToRender := fmt.Sprintf("email-templates/%s.html.tmpl", tmpl)
    t, err := template.New("email-html").ParseFS(emailTemplateFS, templateToRender)
    if err != nil { app.errorLog.Println(err) return err }
    var tpl bytes.Buffer
    if err = t.ExecuteTemplate(&tpl, "body", data); err != nil { app.errorLog.Println(err) return err }
    formattedMessage := tpl.String()
    templateToRender = fmt.Sprintf("email-templates/%s.plain.tmpl", tmpl)
    t, err = template.New("email-plain").ParseFS(emailTemplateFS, templateToRender)
    if err != nil { app.errorLog.Println(err) return err }
    if err = t.ExecuteTemplate(&tpl, "body", data); err != nil { app.errorLog.Println(err) return err }
    plainMessage := tpl.String()
    // send the mail
    server := mail.NewSMTPClient()
    server.Host = app.config.smtp.,,,
    smtpClient, err := server.Connect()
    if err != nil { return err }
    email := mail.NewMSG()
    email.SetFrom(from). AddTo(to). SetSubject(subject)
    email.SetBody(mail.TextHTML, formattedMessage)
    email.AddAlternative(mail.TextPlain, plainMessage)

+   if len(attachments) > 0 {
+     for _, x := range attachments {
+       email.AddAttachment(x)
+     }
+   }

    err = email.Send(smtpClient)
    if err != nil { app.errorLog.Println(err) return err }
    app.infoLog.Println("send mail")
    return nil
  }
```

#### Lamar al microservicio

Al tener nuestro servicio expuesto podemos llamarlo de la sig. manera

```go
  func (app *application) PaymentSucceeded(w http.ResponseWriter, r *http.Request) {
    ...
// call microservice
    inv := Invoice{
      ID:        orderID,
      Amount:    order.Amount,
      Product:   "Widget",
      Quantity:  order.Quantity,
      FirstName: txnData.FirstName,
      LastName:  txnData.LastName,
      Email:     txnData.Email,
      CreatedAt: time.Now(),
    }

    err = app.callInvoiceMicro(inv)
    if err != nil {
      app.errorLog.Println(err)
    }
  }

  func (app *application) callInvoiceMicro(inv Invoice) error {
    url := "http://localhost:5000/invoice/create-and-send"
    out, err := json.MarshalIndent(inv, "", "\t")
    if err != nil { return err }

    req, err := http.NewRequest("POST", url, bytes.NewBuffer(out))
    if err != nil { return err }
    req.Header.Set("Content-Type", "application/json")

    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil { return err }
    defer resp.Body.Close()
    return nil
  }
```

**Resultado:** Tenemos un micro-servicio muy simple integrado a nuestro
monolito.

### Validar del lado del servidor

<!-- validar datos en servidor -->

1, Crear validador `./internal/validator/valitator.go`

```go
  package validator

  type Validator struct {
    Errors map[string]string
  }

// Para crear instancias
  func New() *Validator {
    return &Validator{Errors: make(map[string]string)}
  }

  func (v *Validator) Valid() bool {
    return len(v.Errors) == 0
  }

  func (v *Validator) AddError(key, message string) {
    if _, exists := v.Errors[key]; !exists {
      v.Errors[key] = message
    }
  }

  func (v *Validator) Check(ok bool, key, message string) {
    if !ok {
      v.AddError(key, message)
    }
  }
```

2, Cremos metodo para responder con error de validacion

```go
  func (app *application) failedValidation(w http.ResponseWriter, r *http.Request, errors map[string]string) {
    var payload struct {
      Error bool `json:"error"`
      Message string `json:"message"`
      Errors map[string]string `json:"errors"`
    }

    payload.Error = true
    payload.Message = "failed validation"
    payload.Errors = errors
    app.writeJSON(w, http.StatusUnprocessableEntity, payload)
  }
```

3, Usamos el validador `cmd\api\handlers-api.go`

```go
  func (app *application) CreateCustomerAndSubscribeToPlan(w http.ResponseWriter, r *http.Request) {
    var data stripePayload err := json.NewDecoder(r.Body).Decode(&data) if err != nil { app.errorLog.Println(err) return }

    // validate data
    v := validator.New()
    v.Check(len(data.FirstName) > 1, "first_name", "must be at least 2 characters")
    if !v.Valid() {
      app.failedValidation(w, r, v.Errors)
      return
    }
```

4, Podemos capturar el error en js de la sig manera `cmd\web\templates\bronze-plan.page.gohtml`

```html
<div class="mb-3">
    <label for="first_name" class="form-label">First Name</label>
    <input type="text" class="form-control" id="first_name" name="first_name"
        required="" autocomplete="first_name-new">
+   <div id="first_name-help" class="valid-feedback">Looks good! </div>
</div>

<script>
fetch("{{.API}}/api/create-customer-and-subscribe-to-plan", requestOptions)
.then(response => response.json())
.then(function(data) {
  if (data.error === false) {
      ...
  } else {
    document.getElementById("charge_form").classList.remove("was-validated");
    Object.entries(data.errors).forEach((i) => {
      const [key, value] = i;
      document.getElementById(key).classList.add("is-invalid");
      document.getElementById(key + "-help").classList.remove("valid-feedback");
      document.getElementById(key + "-help").classList.add("invalid-feedback");
      document.getElementById(key + "-help").innerText = value;
    })
    showPayButtons();
  }
</script>
```

**Resultado:** Creamos y usamos un validador de info.

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## %%%%%%%%%%% Tienda MDLM, vainillaJS, ta %%%%%%%%%%%

[tienda_mdlmt_golang.md](./../../projects/tienda_mdlmt_golang/tienda_mdlmt_golang.md)

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Api Usuarios

### Cableado web con Gin

#### Introduccion: Peticion get para retornar coleccion de structs

Fuente: [go.dev](https://go.dev/doc/tutorial/web-service-gin)

Instalar dependencia de gin: `go get -u github.com/gin-gonic/gin`

Compilar con [jsoniter](https://github.com/json-iterator/go) `go build -tags=jsoniter .`
que es mas o menos 4x mas rapido

0, Estructura de carpetas [id:3456yhj89x]

```r
.
│   go.mod
│   go.sum
│
├───cmd
│   └───api
│           hadlers.go
│           main_api_users.go
│           routes.go
│
└───internal
    └───users
            user_service.go
```

1, Crear servicio, para consegir los datos

```go
//internal\users\user_service.go

package users
import "time"

type UserModel struct {
  Id         int       `json:"id"`
  Name       string    `json:"name"`
  Password   string    `json:"password"`
  Last_login time.Time `json:"last_login"`
}

func FindAll() []UserModel {
  var users = []UserModel{
    {Id: 1, Name: "Train", Password: "Coltrane", Last_login: time.Date(2021, 2, 13, 0, 0, 0, 0, *&time.UTC)},
    {Id: 2, Name: "Jeru", Password: "Mulligan", Last_login: time.Date(2019, 4, 14, 0, 0, 0, 0, *&time.UTC)},
    {Id: 3, Name: "Sarah", Password: "Vaughan", Last_login: time.Date(2020, 5, 15, 0, 0, 0, 0, *&time.UTC)},
  }
  return users
}
```

2, Crear el handler, metodos que manejan la peticion

```go
//cmd\api\routes.go
  package main
  import ( "net/http" "github.com/gin-gonic/gin" "users_api/internal/users" )

  func getAllUsers(c *gin.Context) {
    allUsers := users.FindAll()
// De struct a json en gin, De arreglo a struct con gin    
+   c.IndentedJSON(http.StatusOK, allUsers)
  }
```

3, Crear el routes, donde declararemos las rutas

```go
package main
import ( "github.com/gin-gonic/gin" )

func setRoutes(router *gin.Engine) {
  router.GET("/users", getAllUsers)
}
```

4, Main, donde inicializamos y iniciamos el servidor web

```go
//cmd\api\main_api_users.go

  package main
  import "github.com/gin-gonic/gin"

  func main() {
// crear e iniciar servidor con gin
    router := gin.Default()
    setRoutes(router)
    router.Run("localhost:8080")
  }
```

**Resultado:** Podemos hacer una peticion a `http://localhost:8080/users`

![24_primer_peticion_con_gin](./img/24_primer_peticion_con_gin.jpg)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Peticiones Post

Nota: estructura de carpeta en [id:3456yhj89x]

1, Creamos service para agregar usuario a nuestra db, en este caso un
arreglo

```go
//internal\users\user_service.go
package users
import "time"
type UserModel struct {
  Id         int       `json:"id"`
  Name       string    `json:"name"`
  Password   string    `json:"password"`
  Last_login time.Time `json:"last_login"`
}

var users = []UserModel{
  {Id: 1, Name: "Train", Password: "Coltrane", Last_login: time.Date(2021, 2, 13, 0, 0, 0, 0, *&time.UTC)},
  ...
}

func AddUser(newUser UserModel) UserModel {
  users = append(users, newUser)
  return newUser
}
```

2, Creamos hadler

```go
//cmd\api\hadlers.go

  package main
  import (
    "net/http"
    "github.com/gin-gonic/gin"
    "users_api/internal/users"
  )

  func addUser(c *gin.Context) {
    var newUser users.UserModel
//De json a struct con gin
+   if err := c.BindJSON(&newUser); err != nil {
      panic(err)
    }
    userAdded := users.AddUser(newUser)
    c.JSON(http.StatusCreated, userAdded)
  }
```

3, Agregar ruta

```go
//cmd\api\main_api_users.go

  package main
  import ( "github.com/gin-gonic/gin" )

  func main() {
    router := gin.Default()
    setRoutes(router)
    router.Run("localhost:8080")
  }

  func setRoutes(router *gin.Engine) {
    router.GET("/users", getAllUsers)
+   router.POST("/users", addUser)
  }
```

**Resultado:** Podemos hacer la siguiente peticion

```r
curl http://localhost:8080/users \
    --include \
    --header "Content-Type: application/json" \
    --request "POST" \
    --data '{ "id": 4, "name": "mario", "password": "mario" }'
```

![25_peticion_post_con_gin.jpg](./img/25_peticion_post_con_gin.jpg)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Path parameters con Gin

**Objetivo:** El uso de parametros en la ruta con gin, para obtener
un usuario segun su ID.

1, Creamos servicio para obtener usuario por Id

```go
//internal\users\user_service.go

package users
import "time"
var users = []UserModel{
  {Id: 1, Name: "Train", Password: "Coltrane", Last_login: time.Date(2021, 2, 13, 0, 0, 0, 0, *&time.UTC)},
  ...
}

func FindUserById(id int) UserModel {
  var userFinded UserModel = UserModel{Id: -1}
  for _, u := range users {
    if u.Id == id {
      return u
    }
  }
  return userFinded
}
```

2, Creamos el hadler para resivir la peticion y usar el servicio

```go
// cmd\api\hadlers.go
  package main
  import ( "net/http" "strconv" "github.com/gin-gonic/gin" "users_api/internal/users" )
  func getUserById(c *gin.Context) {
// obtener parametro de la url con gin
+   idParam := c.Param("id")
    id, err := strconv.Atoi(idParam)
    if err != nil { panic(err) }
    userFinded := users.FindUserById(id)
    if userFinded.Id >= 0 { c.IndentedJSON(http.StatusOK, userFinded) }
// retornar un 404 con gin
    else { c.IndentedJSON(http.StatusNotFound, gin.H{"message": "user not found"}) }
  }
```

3,Agregamos ruta

```go
// cmd\api\main_api_users.go
  package main
  import ( "github.com/gin-gonic/gin" )

  func main() {
    router := gin.Default()
    setRoutes(router)
    router.Run("localhost:8080")
  }

  func setRoutes(router *gin.Engine) {
    router.GET("/users", getAllUsers)
    router.POST("/users", addUser)
//agregar ruta con parametro en la url con gin
+   router.GET("/users/:id", getUserById)
  }
```

**Resultado:** Podmeos hacer la sig. peticion y obtener el usuario
con el ID indicado

```r
curl http://localhost:8080/users/1
```

![26_peticion_con_path_parameter](./img/26_peticion_con_path_parameter.jpg)

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Por el camino

### Gin

#### Uso de templates con Gin

fuente: [GitHub/issues](https://github.com/gin-gonic/gin/issues/320#issuecomment-107253614)

**Importante 1:** hay diferencias entre el `ParseFiles` y el `ParseBlob`

- `ParseFiles` debe de tener un bloque que no este dentro de un define,
  que es lo que mostrara, en este caso esta en el `base.layout`, y el otro
  podemos definir el contenido, en este caso el `product-list`

- `ParseBlob`: compila los templates los cuales debemos de usar el nombre
  de nuestro bloque define, en este caso no se realizo de esta manera.

**Importante 2**: Hay otras maneras de manejarlo, se puede ver en la
documentacion, pero esta es la que me funciono para usar un template
base.

1, Creamos el render y agregamos rutas

```go
import ( "html/template" "net/http" "github.com/gin-gonic/gin" "github.com/gin-gonic/gin/render" )

func main() {
  router := gin.Default()
  router.HTMLRender = createMyRender()
  router.GET("/products", findAll)
  router.GET("/products-details", details)
  router.Run("localhost:8081")
}

func findAll(c *gin.Context) {
  c.HTML(http.StatusOK, "product-list", gin.H{"Products": "hi"})
}

func details(c *gin.Context) {
  c.HTML(http.StatusOK, "product-details", gin.H{"Products": "hi"})
}

type MyHTMLRender struct {
  templates map[string]*template.Template
}

func (r *MyHTMLRender) Add(name string, tmpl *template.Template) {
  if r.templates == nil {
    r.templates = make(map[string]*template.Template)
  }
  r.templates[name] = tmpl
}

func (r *MyHTMLRender) Instance(name string, data interface{}) render.Render {
  return render.HTML{
    Template: r.templates[name],
    Data:     data,
  }
}

func createMyRender() render.HTMLRender {
  r := &MyHTMLRender{}
  baseLayout := "templates/common/base.layout.gohtml"
  r.Add("product-list", template.Must(template.ParseFiles(baseLayout, "templates/products/product-list.gohtml")))
  r.Add("product-details", template.Must(template.ParseFiles(baseLayout, "templates/products/product-details.gohtml")))
  return r
}
```

2, Creamos el template base

```html
<!-- base.layout.gohtml -->
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{block "title" .}}{{end}}</title>
  </head>
  <body>

  <header class="">
    Navegacion
  </header>

  <div class="container">
      <div class="row">
          <div class="col">
              {{block "content" .}} {{end}}
          </div>
      </div>
  </div>

  <script src="/something.js"></script>

  <script>
    console.log("Hi from layout")
  </script>
  {{block "js" .}}
  {{end}}
</body>
</html>
```

3, Creamos nuestras paginas, el cual declaramos el contenido que
falta de nuetro layout.

```html
<!-- product-details.gohtml -->
{{template "base" .}}

{{define "title"}}
    Details product
{{end}}

{{define "content"}}
    <h2 class="mt-5">Product name</h2>
    <hr>
    Some content
{{end}}

{{define "js"}}
<script>
  console.log("Hi from details products")
</script>
{{end}}

```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Subir archivos

- Fuentes
  - [Doc gin-gonic](https://gin-gonic.com/docs/examples/upload-file/single-file/)
  - [Stackoverflow](https://stackoverflow.com/questions/35192841/how-do-i-post-with-multipart-form-data-using-fetch)

IMPORTANTE: El data debe debe de ser de tipo **FormData**. Y tambien
**no debemos de asignar nosotros el header Content-Type**, ya que lo hara de forma
automatica el navegador.

```html
<!-- svelte -->
<input type="file" accept="image/png, image/jpeg" bind:files />
```

```js
const formData  = new FormData();
formData.append('file', files[0]); // files:FileList

const response = await fetch(url, {
  method: 'POST',
  //No el header Content-Type, ya que lo hara el navegador, si podemos
  //agregar otros como el token etc
  body: formData
});
```

```go
func main() {
  router := gin.Default()
  // Set a lower memory limit for multipart forms (default is 32 MiB)
  router.MaxMultipartMemory = 8 << 20  // 8 MiB
  router.POST("/upload", func(c *gin.Context) {
    // single file
    file, _ := c.FormFile("file")
    log.Println(file.Filename)

    // Upload the file to specific dst.
    c.SaveUploadedFile(file, dst)

    c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
  })
  router.Run(":8080")
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Convertir File Multipart a base64

Fuente: [thedeveloperblog](https://thedeveloperblog.com/go/base64-go)

```go
import (
  "bufio"
  "encoding/base64"
  "io/ioutil"
  "mime/multipart"
  "github.com/gin-gonic/gin"
)

func (ProductApiHadler) addImage(c *gin.Context) {
  file, err := c.FormFile("file")
  
  base64 := FileMultiPartToBase64(file)
}

func (ApiHelper) FileMultiPartToBase64(file *multipart.FileHeader) (base64Result *string) {
  f, err := file.Open()
  if err != nil {
    panic("file to base64 open fail")
  }

  // Read entire file into byte slice.
  reader := bufio.NewReader(f)
  content, err2 := ioutil.ReadAll(reader)
  if err2 != nil {
    panic("file to base64 transform fail")
  }

  // Encode as base64.
  base64Result = base64.StdEncoding.EncodeToString(content)

  return base64Result
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Mostrar y/o descargar archivo guardado como base64

<!--
mostrar imagen, descargar imagen, mostrar archivo, descargar archivo,
responder un base64, responder archivo, entregar archivo, entregar imagen
-->

- Fuentes
  - [Mostrar](https://www.sanarias.com/blog/1214PlayingwithimagesinHTTPresponseingolang)
  - [Descargar](https://stackoverflow.com/questions/62003501/gin-serve-byte-as-file)

En este caso un base64 de una imagen se la entregaremos, ya sea para que
la descarge o para que la visualise.

```go
package Fooo

import (
  "encoding/base64"
  "net/http"
  "github.com/gin-gonic/gin"
)

func Foo(c *gin.Context) {

//PASAMOS EL BASE64 A BYTES
  base64String := "/9j/4sgrFcc...B7EqHrYaXp//Z"
  imagebytes, err := base64.StdEncoding.DecodeString(base64String)
  if err != nil {
    panic("Error decoding image")
  }

// ======================== DESARGAR =========================
  /*
  c.Header("Content-Disposition", "attachment; filename=file-name.jpg")
  c.Data(http.StatusOK, "application/octet-stream", imagebytes)
  */

// ======================== MOSTRAR =========================
  c.Writer.Header().Set("Content-Type", "image/jpeg")
  c.Writer.Header().Set("Content-Length", strconv.Itoa(len(imagebytes)))
  _, err2 := c.Writer.Write(imagebytes)
  if err2 != nil {
    panic("Displaying image error")
  }
}
```

#### Agregar gzip a gin

- [Fuente](https://stackoverflow.com/questions/14073393/serving-gzipped-content-for-go)

There is yet another "out of the box" middleware now, supporting net/http and Gin:

```r
https://github.com/nanmu42/gzip
```

net/http example:

```go
import github.com/nanmu42/gzip

func main() {
    mux := http.NewServeMux()
    mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        writeString(w, fmt.Sprintf("This content is compressed: l%sng!", strings.Repeat("o", 1000)))
    })

    // wrap http.Handler using default settings
    log.Println(http.ListenAndServe(fmt.Sprintf(":%d", 3001), gzip.DefaultHandler().WrapHandler(mux)))
}

func writeString(w http.ResponseWriter, payload string) {
    w.Header().Set("Content-Type", "text/plain; charset=utf8")
    _, _ = io.WriteString(w, payload+"\n")
}
```

Gin example:

```go
import github.com/nanmu42/gzip

func main() {
    g := gin.Default()

    // use default settings
    g.Use(gzip.DefaultHandler().Gin)

    g.GET("/", func(c *gin.Context) {
        c.JSON(http.StatusOK, map[string]interface{}{
            "code": 0,
            "msg":  "hello",
            "data": fmt.Sprintf("l%sng!", strings.Repeat("o", 1000)),
        })
    })

    log.Println(g.Run(fmt.Sprintf(":%d", 3000)))
}
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### html/template

#### Agregar json+ld

```go
package main

import (
  "html/template"
  "log"
  "os"
)

type jsonLD struct {
  Foo string `json:"foo"`
}

func main() {
  const code = `<script type="application/ld+json">{{.Val}}</script>`
        type params struct {
          Val jsonLD
        }
  t := template.Must(template.New("").Parse(code))
  p := &params{
    Val: jsonLD{Foo: "John's bike"},
  }

  err := t.Execute(os.Stdout, p)
  if err != nil {
    log.Println("executing template:", err)
  }

}

```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Problemas con el IDE VsCode

#### Al usar wire no no inspecciona el codigo

Fuente: [github.com](https://github.com/google/wire/discussions/283)

Error:

```r
No packages found for open file /some/path/wire.go: <nil>.
If this file contains build tags, try adding "-tags=<build tag>" to your gopls "buildFlag" configuration
```

Descripcion: VsCode no indica los errores que hay en un archivo que esta
tageadodo como `go:build wireinject +build wireinject`

Solucion:

Agregar en el settings.json (file->Preferences->Settings->"buscar: gopls"->Edit in settings.json)

```r
"gopls": {
  "buildFlags": ["-tags=wireinject"]
},
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Wire

Fuente: [github.com](https://github.com/google/wire)

#### Introduccion a Wire

Leer fuente

- Fuente: [github.com](https://github.com/google/wire/tree/main/_tutorial)
- Proyecto: [27_inyeccion_de_dependencias_con_wire](./img/27_inyeccion_de_dependencias_con_wire.zip)

**Importante:** Wire automaticamente mete los argumentos de los "constructores",
y estos deben de ser el struct, por ejemplo.

```go
// NO HACER ESTO, POR QUE DARA ERROR
func InitializeServer(port, path string) ssr.Server {
  wire.Build(ssr.NewServer)
  return ssr.Server{}
}
func NewServer(port, path string) Server {
    return Server{
      port: cfg,
      path: path,
    }
}

//MEJOR HACER ESTO, WIRE CHECARA EN TIPO, Y EN LOS QUE COINCIDA LO INYECTARA
func InitializeServer(cfg ssr.Config) ssr.Server {
  wire.Build(ssr.NewServer, ssr.NewLogger)
  return ssr.Server{}
}

func NewServer(cfg Config) Server {
  return Server{
    port: cfg.port,
    path: cfg.path,
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Uso de Wire con interfaces

Codigo [28_DI_wire_una_interface_multiples_implementaciones.zip](./img/28_DI_wire_una_interface_multiples_implementaciones.zip)

**Objetivo:** Se creran dos implementaciones defirentes de una misma interface,
y en tiempo que se crean las dependencias, se usara una u otra implementacion.
El ejemplo, sera un db_helper que podra ser en sqlite o postgres, y solo
con un cambio de un valor de un enum cambiara la implementacion.

<!-- uso de enum, crear enum, uso enum, como usar enums, -->
1, Creamos en enum para poder definir le caso de postgres o el de enum

```go
//common\config.go
package common

type DB_TYPE int

const (
  Postgres DB_TYPE = iota
  Sqlite
)

type Config struct {
  DBType DB_TYPE
}
```

2, Creamos la interface del db_helper

```go
//db_helper\1_dbh_interface.go
package db_helper
import ( "di/common" "errors" )

type IDBHelper interface {
  FindById(int) string
  Save(string) string
  DeleteById(int) bool
}

func NewDBHelper(cfg common.Config) (dbh IDBHelper, err error) {
  /* MAS ABAJO LO DEFINIREMOS/EXPLICAREMOS*/
}
```

3, Creamos las 2 implementaciones, en este caso para postgres y para sqlite

```go
//db_helper\dbh_postgres.go
package db_helper
import ( "fmt" "strconv" )

type DBHPostgres struct { name string }

func (dbh DBHPostgres) FindById(f int) string {
  resp := dbh.name + "Encontrado: " + strconv.Itoa(f)
  fmt.Println(resp)
  return resp
}
func (dbh DBHPostgres) Save(f string) string {
  resp := dbh.name + "Guardado: " + f
  fmt.Println(resp)
  return resp
}
func (dbh DBHPostgres) DeleteById(f int) bool {
  resp := dbh.name + "Eliminado: " + strconv.Itoa(f)
  fmt.Println(resp)
  return true
}

//===============================

//db_helper\dbh_sqlite.go
package db_helper
import ( "fmt" "strconv" )
type DBHSqlite struct { name string }
func (dbh DBHSqlite) FindById(f int) string {...}
func (dbh DBHSqlite) Save(f string) string {...}
func (dbh DBHSqlite) DeleteById(f int) bool {...}
```

4, Indicamos el constructor=provider el cual en un caso ara uno u otro

```go
//db_helper\1_dbh_interface.go
func NewDBHelper(cfg common.Config) (dbh IDBHelper, err error) {

  if cfg.DBType == common.Postgres {
    dbh = DBHPostgres{
      name: "Postgres: ",
    }

  } else if cfg.DBType == common.Sqlite {
    dbh = DBHSqlite{
      name: "Sqlite: ",
    }
  } else {
    return nil, errors.New("db type is no valid")
  }

  return dbh, nil
}
```

5, Creamos el inicializador el cual usaremos wire de la misma manera

```go
//wire.go
//go:build wireinject
// +build wireinject
package main
import ( "di/common" "di/db_helper" "github.com/google/wire" )
func InitializeDBHelper(cfg common.Config) (db_helper.IDBHelper, error) {
  wire.Build(db_helper.NewDBHelper)
  return db_helper.DBHSqlite{}, nil
}

```

6, Nuestro main podremos definir que implementacion queremos usar solo
con cambiar el valor de un enum

```go
package main
import ( "di/common" )

func main() {
  config := common.Config{
    DBType: common.Sqlite,
  }
  dbh, err := InitializeDBHelper(config)
  if err != nil {
    panic(err)
  }
  dbh.Save("Mario")
}
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### GORM

Fuente: [Documentacion de GORM](https://gorm.io/docs/index.html)

#### Instalar GORM

Instalacion: `go get -u gorm.io/gorm`

Instalacion de drivers

- sqlite: `go get -u gorm.io/driver/sqlite`

- TODO: Como hacer un update con una relacion muchos a muchos   
  - E:\cursos\0activ\proyectos-test\users-go\api\src\users\users_repository.go
  - https://stackoverflow.com/questions/47871117/how-to-update-with-a-many-to-many-relationship
    - ociation("Entities").Append([]*Entity{&Entity{Name: "mynew

<!--

#########################################

-->

----

<!--

#########################################

-->

### Uso soda

Soda es un cli (parte del conjunto de buffalo) que nos ayuda a crear
archivos fizz que son muy parecidos a sql (Un lengiaje DSL), pero este
es compatible con multiples db, el cual debemos de crear un archivo de
coneccion y los fizz, y on estos podemos crear la bd.

#### Iniciar base de datos

- [Fuente](https://gobuffalo.io/documentation/database/soda/)

1, Generamos el archivo de configuracion/coneccion

```r
soda generate config
```

2, Personalizamos los datos de conexion en el archivo `database.yml`
que se genero

```r
development:
  dialect: postgres
  database: XXX
  user: XXX
  password: XXX
  host: 127.0.0.1
  pool: 5
```

3, Generamos la bd (soda se conectara y la generara)

```r
soda create -a
```

4, Podemos eliminarla con

```r
soda drop -a
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Crear tabla simple

- [Fuente](https://gobuffalo.io/documentation/database/fizz/)

A, Generamos los archivos fizz (podemos igual de forma manual)

```r
soda generate fizz SOME_NAME
```

B, Asi podria lucir nuestro archivo fizz, ver la fuente para crear cosas
mas detalladas

```r
create_table("users") {
  t.Column("email", "string", {})
  t.Column("twitter_handle", "string", {"size": 50})
  t.Column("age", "integer", {"default": 0})
  t.Column("admin", "bool", {"default": false})
  t.Column("company_id", "uuid", {"default_raw": "uuid_generate_v1()"})
  t.Column("bio", "text", {"null": true})
  t.Column("joined_at", "timestamp", {})
}

create_table("todos") {
  t.Column("user_id", "integer", {})
  t.Column("title", "string", {"size": 100})
  t.Column("details", "text", {"null": true})
  t.ForeignKey("Column(", {"users": ["id"]}, {"on_delete": "cascade"})
}
```

### Ver servidor desde local network

**Problema:** En el caso de Gin tuve el problema de que me rechasaba la conexion,
cuando trataba de conectar a mi localhost desde mi telefono, a pesar
de que podia hacer ping del cell a lap y lap a cell.

**Causa:** Levante el servicio para que escuchara desde `router.Run("localhost:80")`
lo cual al tener una peticion de fuera la ignoraba.

**Solucion:** Cambie el desde donde queria que escuchara, que es la IP
asignada en mi local network en mi caso `router.Run("192.168.1.81:80")`


<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## %%%%%%%%%%% 6. MODULES %%%%%%%%%%%

### 6.1. Introduccion

1, Creamos dos proyectos como siempre, uno sera el modulo que correra usando tests unitarios y otro sera sera el que tenga el main, el cual podra ser un proyecto CLI, web, mobil, etc.

2, Vinculamos ambos proyectos, usando la caracteristica workspace de go >=1.8, el cual nos permitira trabajar con los dos proyectos como si fueran modulos, y sin la necesidad de estar descargando del repositorio los cambios, si no que nos permitira detectar los cambios de forma local. En cambio a nivel productivo podra funcionar como cualquier libreria que se descarga de forma externa.

```r
1_
"01_toolkit/toolkit"> go mod init toolkit
"01_toolkit/app"    > go mod init main_toolkit

_2
"01_toolkit/app"    > go mod init main_toolkit
```

3, Resultado, creara un archivo llamado `go.work` el cual vinculara los dos proyectos.

```r
go 1.19

use (
  ./app
  ./toolkit
)
```

4, Codigo, vamos a terminar con la siguiente estructura, el cual `hello/` funcionara como como siempre hemos importado sub-carpetas de
nuestro proyecto, y tenemos `toolkit` que funcionara como un modulo o libreria el cual podremos usar en otros proyectos.

```r
go.work

toolkit/
  tools.go
  go.mod
  tools_test.go

app/
  go.mod
  main.go
  hello/
    hello.go
```

main.go

```go
package main

import (
// esta forma indica que es parte del proyecto
  "main_toolkit/hello"
// esta forma indica que es externo, si nos fijamos es la misma
//manera en la que importamos fmt
  "toolkit"
  "fmt"
/*
NOTA: Se recomienda usar de una vez un repositorio como github, gitlab, etc. Para poderlo importar en otros equipos.
*/
)

func main() {
  var tools toolkit.Tools
  fmt.Printf("Random(%v): %s\n", 13, tools.RandomString(13))

  var hi hello.Hello
  hi.SayHi()
}
```

tools.go, en este lo que hacemos es generar string aleatorio

```go
package toolkit
import "crypto/rand"
const randomStringSource = "qwertyuiopasdfghjklzxcvbnm-1234567890_QAZWXEDCRFVTGBYHNUJMIKOLP+"
type Tools struct { }

func (t *Tools) RandomString(length int) string {
  result := make([]rune, length)
  source := []rune(randomStringSource)
  for i := range result {
    randomGen, _ := rand.Prime(rand.Reader, len(source))
    randomNum := randomGen.Uint64()
    limit := uint64(len(source))
    result[i] = source[randomNum%limit]
  }

  return string(result)
}
```
