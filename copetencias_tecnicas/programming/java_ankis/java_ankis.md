# Java

## Seguridad

### Mensajes firmados

#### Como funcionan y para que son

Un mensaje se puede firmar usando una llave privada, luego se envía
este mensaje junto con su firma, y asi otra entidad puede verificar
la autenticidad de un mensaje usando 3 elementos; 1) la llave publica,
2) la firma y 3) el propio mensaje.

Asi sabemos con seguridad que un mensaje fue emitido por una determinada entidad,
y que no fue modificado por un tercero, como también no arriesgar la llave
privada.

#### Ejemplos simple de creacion y verificación de firma

Este ejemplo simple tenemos

- generacion de llaves (publica y privada)
- Cracion de mensaje usando la llave privada
- Validar el mensaje usando la llave publica
- Un mensaje es valido y el otro a sido modificado

```java
import static org.junit.Assert.assertTrue;import java.security.InvalidKeyException;import java.security.KeyPair;import java.security.KeyPairGenerator;import java.security.NoSuchAlgorithmException;import java.security.PrivateKey;import java.security.PublicKey;import java.security.Signature;import java.security.SignatureException;import java.util.Base64;import org.junit.Test;

public class PairKeys {

  @Test
  public void createPairKeys() throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    //generate keys
    KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("DSA");
    keyPairGen.initialize(2048);
    KeyPair pair = keyPairGen.generateKeyPair();
    PrivateKey privKey = pair.getPrivate();
    PublicKey pubKey = pair.getPublic();

    //create message
    Message message = createMessageSigned(privKey);
    message.show("Sended");
    
    //validate valid message
    boolean validated = validateMessage(pubKey, message);
    assertTrue("The signature must pass", validated);

    //validate corrupted messagge
    message.body = "some bad word";
    boolean validated2 = validateMessage(pubKey, message);
    assertFalse("The signature must fail", validated2);
  }


  public Message createMessageSigned(PrivateKey privKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    
    //generate signature
    Signature sign1 = Signature.getInstance("SHA256withDSA");
    sign1.initSign(privKey);
    
    String body = "Hello how are you.";
    byte[] messageSendedByted = body.getBytes();
    sign1.update(messageSendedByted);
    byte[] signatureRaw = sign1.sign();
    String signature = Base64.getEncoder().encodeToString(signatureRaw);

    var msg = new Message(signature, body);

    return msg;
  }

  public boolean validateMessage(PublicKey pubKey, Message message) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    byte[] signarureInBytes = Base64.getDecoder().decode(message.signature);
    var msg = new Message(Base64.getEncoder().encodeToString(signarureInBytes), message.body);
    msg.show("Recibed: ");
    
    Signature signator = Signature.getInstance("SHA256withDSA");
    signator.initVerify(pubKey);
    
    signator.update(message.body.getBytes());
    boolean bool = signator.verify(signarureInBytes);

    if (bool) {
      System.out.println("Message is valid");
    }

    return bool;
  }

}

class Message {
  public String signature;
  public String body;

  public Message(String signature, String body) {
    this.signature = signature;
    this.body = body;
  }

  public void show(String type) {
    System.out.println(String.format(type + " {\n\tMessagge:%s \n\tSignature:%s \n}", body, signature));
  }

}
```

#### Ejemplos OOP de creacion y verificación de firma

```java
import static org.junit.Assert.assertFalse;import static org.junit.Assert.assertTrue;import java.security.InvalidKeyException;import java.security.KeyFactory;import java.security.KeyPair;import java.security.KeyPairGenerator;import java.security.NoSuchAlgorithmException;import java.security.PrivateKey;import java.security.PublicKey;import java.security.SecureRandom;import java.security.Signature;import java.security.SignatureException;import java.security.spec.X509EncodedKeySpec;import java.util.Base64;import org.apache.commons.lang3.RandomStringUtils;import org.junit.Test;

public class PairKeys {

  @Test
  public void createPairKeys() throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    //==================== AUTH SIDE
    var auth = new MessageSignedGenerator("my-secret");
    MessageSigned message = auth.createRandomSignedMessage(10);
    

    //==================== COMMUNICATION
    String publicKeyString = auth.getPubKey();
    System.out.println("Public key to send: " + publicKeyString.substring(1097, 1100));
    message.show("To Send: ");


    //==================== RESOURCE SIDE
    var vali = new MessageSignedValitator(publicKeyString);

    //validate valid message
    boolean validated = vali.validateMessage(message);
    assertTrue("The signature must pass", validated);

    //validate corrupted messagge
    message.body = "some bad word";
    boolean validated2 = vali.validateMessage(message);
    assertFalse("The signature must fail", validated2);
    
  }

}

class MessageSigned {
  public String signature;
  public String body;

  public MessageSigned(String signature, String body) {
    this.signature = signature;
    this.body = body;
  }

  public void show(String type) {
    System.out.println(String.format(type + " {\n\tMessagge:%s \n\tSignature:%s \n}", body, signature));
  }

}





class MessageSignedValitator {
  private PublicKey pubKey;
  private Signature signator;

  public MessageSignedValitator(String pubKey) {
    this.pubKey = decodePublicKey(pubKey);

    generateSignator();
    
  }

  private static PublicKey decodePublicKey(String encodedKey) {
    byte[] publicKeyByteServer = Base64.getDecoder().decode(encodedKey);
    PublicKey publicKeyServer = null;
    try {
      KeyFactory keyFactory = KeyFactory.getInstance("DSA");
      publicKeyServer = (PublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(publicKeyByteServer));
    } catch (Exception e) {
      e.printStackTrace();
    }
    return publicKeyServer;
  }

  private void generateSignator() {
    try {
      signator = Signature.getInstance("SHA256withDSA");
      signator.initVerify(pubKey);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public boolean validateMessage(MessageSigned message) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    message.show("Resived:");

    byte[] signarureInBytes = Base64.getDecoder().decode(message.signature);
    
    signator.update(message.body.getBytes());
    boolean bool = signator.verify(signarureInBytes);

    String msgResult = (bool) ? "Message is Valid" : "Message is Incorrect";
    System.out.println(msgResult);

    return bool;
  }
  
}

class MessageSignedGenerator {

  private PrivateKey privKey;
  private PublicKey pubKey;
  private Signature sign1;

  public MessageSignedGenerator(String secret) {
    generateKeys(secret);
    generateSignator();
  }

  private void generateSignator() {    
    try {
      sign1 = Signature.getInstance("SHA256withDSA");
      sign1.initSign(privKey);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public MessageSigned createRandomSignedMessage(int size) {
    var body = getRandomBody(size);
    return this.createMessageSigned(body);
  }

  public String getRandomBody(int size) {
    String generatedString = RandomStringUtils.randomAlphanumeric(size);
    return generatedString;
  }

  public MessageSigned createMessageSigned(String body) {
    byte[] messageSendedByted = body.getBytes();
    String signature;
    try {
      sign1.update(messageSendedByted);
      byte[] signatureRaw = sign1.sign();
      signature = Base64.getEncoder().encodeToString(signatureRaw);
    } catch (SignatureException e) {
      e.printStackTrace();
      return null;
    }
    var msg = new MessageSigned(signature, body);
    return msg;
  }


  private void generateKeys(String secret) {
    KeyPairGenerator keyPairGen;
    try {
      keyPairGen = KeyPairGenerator.getInstance("DSA");
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      return ;
    }

    SecureRandom random = new SecureRandom();
    byte bytes[] = secret.getBytes();
    random.nextBytes(bytes);
    keyPairGen.initialize(2048, random);
    KeyPair pair = keyPairGen.generateKeyPair();
    privKey = pair.getPrivate();
    pubKey = pair.getPublic();
  }

  public String getPubKey() {
    byte[] publicKeyByte = this.pubKey.getEncoded();
    String publicKeyString = Base64.getEncoder().encodeToString(publicKeyByte);
    return publicKeyString;
  }

}

```
