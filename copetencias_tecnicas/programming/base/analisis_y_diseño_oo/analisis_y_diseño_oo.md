# Analisis y Diseño Orientado a Objetos

- [Analisis y Diseño Orientado a Objetos](#analisis-y-diseño-orientado-a-objetos)
  - [Accesos rápidos](#accesos-rápidos)
  - [Introduccion (Clase 1)](#introduccion-clase-1)
  - [Clase 2: Teoria sobre el diseño O.O](#clase-2-teoria-sobre-el-diseño-oo)
    - [Abstraccion](#abstraccion)
    - [Encapsulacion](#encapsulacion)
    - [Modularidad](#modularidad)
      - [Acoplamiento](#acoplamiento)
      - [Cohesion](#cohesion)
      - [Conclusion de modularidad](#conclusion-de-modularidad)
    - [Polimorfismo](#polimorfismo)
    - [Jerarquia](#jerarquia)
  - [Economia del software](#economia-del-software)
    - [Razones por las que falla un proyecto](#razones-por-las-que-falla-un-proyecto)
    - [Complejidad del software](#complejidad-del-software)
    - [Disciplinas del Software](#disciplinas-del-software)
    - [Problemas vs Soluciones](#problemas-vs-soluciones)
  - [Calidad del Software](#calidad-del-software)
  - [Clase 4, Guias de estilo de codigo](#clase-4-guias-de-estilo-de-codigo)
  - [Analisis](#analisis)
    - [Decidir que clases crear](#decidir-que-clases-crear)
      - [Analisis clásico](#analisis-clásico)
      - [Analisis del comportamiento](#analisis-del-comportamiento)
      - [Invención Pura](#invención-pura)
    - [Herencia](#herencia)
  - [Diseño](#diseño)
    - [Detectar mal olor en el diseño/codigo](#detectar-mal-olor-en-el-diseñocodigo)
      - [Librería incompleta](#librería-incompleta)
      - [Múltiples Responsabilidades](#múltiples-responsabilidades)
      - [Codigo Muerto](#codigo-muerto)
      - [Cohecion de metodos](#cohecion-de-metodos)
      - [Clases tontas](#clases-tontas)
      - [Cirugía a escopetazos](#cirugía-a-escopetazos)
      - [Principio de menor compromiso y la menor sorpresa](#principio-de-menor-compromiso-y-la-menor-sorpresa)
      - [DRY (Don't Repeat Yourself)](#dry-dont-repeat-yourself)
      - [KISS (keep it simple, stupid)](#kiss-keep-it-simple-stupid)
      - [Programación defensiva](#programación-defensiva)
      - [Inapropiada Intimidad o dependencia cíclica](#inapropiada-intimidad-o-dependencia-cíclica)
      - [Envidia de caracteristicas](#envidia-de-caracteristicas)
      - [Herencia Rechazada](#herencia-rechazada)
      - [Jerarquias paralelas de herencia](#jerarquias-paralelas-de-herencia)
        - [Solucion Inversion de control (ver abajo)](#solucion-inversion-de-control-ver-abajo)
      - [Preguntar tipos (instanceOf)](#preguntar-tipos-instanceof)
      - [Hablar con desconocidos](#hablar-con-desconocidos)
    - [Solucciones](#solucciones)
      - [GRASP (Expertos en la informacion)](#grasp-expertos-en-la-informacion)
      - [Template method pattern](#template-method-pattern)
      - [Inversion de control](#inversion-de-control)
      - [Inyeccion de dependencias (DIP)](#inyeccion-de-dependencias-dip)
      - [Inversion de Dependencias](#inversion-de-dependencias)
      - [Visitor Pattern](#visitor-pattern)
      - [Double dispatch](#double-dispatch)
      - [Ley de demeter](#ley-de-demeter)
    - [Relacion entre clases](#relacion-entre-clases)
      - [Diferencia entre Herancia e implementacion (clase abstracta e interface)](#diferencia-entre-herancia-e-implementacion-clase-abstracta-e-interface)
      - [Diferencia entre composición y agregación](#diferencia-entre-composición-y-agregación)
      - [Relacion de composición](#relacion-de-composición)
      - [Relacion de agregación](#relacion-de-agregación)
      - [Relacion de Asociación](#relacion-de-asociación)
      - [Relacion de USO](#relacion-de-uso)
  - [Ejemplos](#ejemplos)
    - [Quitar codigo repetido por Herencia](#quitar-codigo-repetido-por-herencia)

## Accesos rápidos

- Herencia
  - Nos preguntamos `¿es un?`
  - En UML es una linea con una flecha gorda pegada al padre.
- Relaciones entre clases
  - Composición: ej. ¿mi cabeza `es parte de` mi?
    - En UML es un rombo negro/relleno
  - Agregación: ¿forma parte de?, ¿yo `formo parte de` mi familia?
    - En UML es un rombo hueco/blanco
  - Asociación: ¿la claseA `debe usar` solo una instancia de la claseB?
    - En UML es una linea simple
  - Uso ¿La claseA `puede usar` cualquier instancia de la claseB?
    - En UML es una linea punteada

## Introduccion (Clase 1)

Poniendo la analogía de un hijo vs el software, es importante que este
nazca, incluso en éxito de proyectos es que este salga solo con lo, necesario
pero que salga, de ahi en adelante se le agregaran algunas cosillas para
que este crezca poco a poco y llegue a convertirse en algo viable/util.

Este tipo de cosas son

- Legible vs viscoso
- Flexible vs rígido
- Robusto vs frágil
- Reusable vs inmóvil

**Como deberíamos de aprender a programar** la siguiente es una lista
de los pasos que deberíamos de tomar en orden

1. Análisis y diseño O. O. (lo que veremos en este curso)
2. Patrones de diseño: Gestión de dependencias
3. Arquitectura de software: estilos como (MVC, MVP, MVVM...)
4. Pruebas: JUnit, integración
5. TDD: de la mano con refactoring
6. Ecosistema (Forja): GIT, métricas, checkStyle, etc.
7. metodologías: EnCascada, SCRUM, XP...
8. Tecnologías: Persistencia (DAO, ORM...) WebStyle (SPA, Templates...)

**Que veremos**: Temas que incluye el análisis y diseño O. O.

- Principios: DRY, YAGNI, KEEP, Mínima sorpresa.
- Patrones GRASP (General responsabilities Asiggment Software Patterns),
  alta/baja cohesión, acoplamiento etc.
- Relaciones entre clases: Composición/agregación, asociación, dependencia.
- Código Limpio: formato, nombrado, comentarios
- Principios SOLID
- Diseño por contrato
- Código sucio
- Anti-Patrones: BLOB, números mágicos, ...

**Biografiás**: Libros recomendados del contenido visto, en orden recomendado
de lectura.

1. Código Limpio de Robert Martin
2. [Refactoring Improving Design Existing Code By Kent Beck](https://www.amazon.com.mx/Refactoring-Improving-Design-Existing-Code/dp/0201485672)
3. Patrones generales del software para asignación de responsabilidades

**¿Que es un tipo?**: Es un conjunto, por ejemplo, un int en java va del
-32,000,000 al 32,000,000, un booleano es un verdadero o falso, etc. lo
cual un codigo puede interactuar con este tipo de cierta manera, por
ejemplo, un booleano cabe en un condicional pero no en una suma, y un
entero cabe en una suma pero no en un codicional.  
Y lo podemos llevar a las clases, los cuales una clase puede ser de un
tipo, donde un codigo puede meter una clase en un tipo por que cumple
las caracteristicas, y de este modo puede caber en varias partes.

<!--

######################################### división titulo

-->

----

<!--

######################################### división titulo

-->

## Clase 2: Teoria sobre el diseño O.O

Los sistemas se dividen en dos

- **Sistemas simples**: Una sola persona lo puede entender/controlar todo,
  por ejemplo, un blog o tienda pequeña.
- **Sistemas Complejos**: Rebasan la comprensión humana, no es que sean
  imposibles de entender/controlar, pero si se requieren de varias personas
  para hacerlo, por ejemplo, un sistema bancario.

### Abstraccion

<!-- Que es la abstraccion -->

La abstracción de un sistema depende del contexto, por ejemplo, a un
mecánico y un pasajero tendrán abstracciones diferentes de un carro, es
decir, **la abstracción es subjetiva**, porque a uno le interesan cosas
que el otro ignorara, y esto NO significa que una de esas abstracciones
sea incorrecta.

### Encapsulacion

<!-- Que es la encapsulacion -->

La encapsulacion significa ocultar los detalles de un proceso, por ejemplo,
si quiero obtener información, no me interesa como la voy a obtener solo
la quiero obtener y ya, y para esto ocultamos los detalles de que sentencias
SQL, URL, filesystem se usaron para obtenerlo.

Esto aplica para cualquier cosa, por ejemplo, una calculadora al multiplicar
nosotros no nos interesa como lo hace, si es por sumar repetidas, un proceso
parecido al de libreta o etc, solo nos interesa como metemos los datos
y como se regresan.

**Es importante** que no estén ligadas las funcionalidades, por ejemplo,
si el dia de mañana cambia como obtengo la información, si la entrada y
salida son mismas, esto permite que no se rompa nada.

### Modularidad

<!-- Que es la modularidad -->

La modularidad es el proceso de descomposición de un sistema en un conjunto
de piezas poco acopladas y cada pieza con alta cohesión, lo que se busca
es que los cambios en un modulo no afecte a otro.

**Cuidado:** la modularidad no vale si tenemos 1 problema dividido en
3 partes, pero 2 son pequeñas y 1 es grande, los módulos deben de tener
una responsabilidad equitativa.

#### Acoplamiento

Significa que tan pegado esta un modulo del otro, lo cual entre mas
acoplados hay mayor complejidad y afectos secundarios, esto se ve viendo
de que clases depende la claseA, y no nos importa cuantas clases dependen
de la claseA, eso nos preocupa en esas otras clases.

Ahi que considerar también tenemos dependencias indirectas, por ejemplo,
yo claseA dependo directamente de la claseB, y esta claseB como respuesta
me da una claseC y a esta la manipulo, esto significa que la claseA
depende de claseB y de la claseC.

#### Cohesion

Mide la conectividad entre los elementos de un modulo, un modulo con una
buena cohesión, significa que todos sus elementos asociados tienen un sentido
de estar ahi, dándole al conjunto un significado propio.

- Ejemplo: ¿Si al objeto fecha(año, mes dia) le quitamos el mes para manejarlo
  en otro lado esto, tiene sentido? Respuesta: NO, porque la fecha tiene
  una buena cohesión.

- Ejemplo: Si a una clase le quitamos una propiedad o varias y aun tiene
  sentido de ser la clase significa que tenemos una baja cohesión y
  deberíamos pensar en dividir esta clase.

#### Conclusion de modularidad

Por ejemplo, un método que calcula el factorial no debería de tocar,
nada para que este se muestre.

### Polimorfismo

El polimorfismo es cuando un método puede trabajar con una familia de
clases, y no esta ligado a trabajar solo con un tipo de objeto.

Para hacer polimorfismo significa que tenemos herencia, pero tener herencia
no significa que ya hallamos implmementado polimorfismo, es decir, primero
es herencia luego es polimorfismo.

Por ejemplo,

```java
public class TicTacToe {
  ...
  public void play() {
    Controller controller; // esta es una clase abstracta
    do {
      controller = logic.getController(); // se encaga de retoranar el hijo correcto segun el caso/estado
      if (controller != null){
        controller.control(); // aqui trabaja con diferentes hijos
      }
    } while (controller != null);
  }
}

public class Logic {

  private Game game; // aqui guardamos el estado
  // diferentes escenarios de control
  private StartController startController;
  private PutController putController;
  private MoveController moveController;

  public Logic() {
    game = new Game();
    startController = new StartController(game);
    putController = new PutController(game);
    moveController = new MoveController(game);
  }

// retoranar el hijo correcto segun el caso/estado
  public Controller getController() {
    switch (game.getState()){
    case INITIAL:
      return startController;
    case IN_GAME:
      if (!game.getBoard().complete()){
        return putController;
      } else {
        return moveController;
      }
    case FINAL:
    default:
      return null;
    }
  }
}

public abstract class ColocateController extends Controller {
  private String actionTitle;
  private Coordinate target;

  protected ColocateController(Game game, String actionTitle) {
    super(game);
    assert game != null;
    assert actionTitle != null;
    this.actionTitle = actionTitle;
    target = new Coordinate();
  }

// metodo que usara el metodo que hace polimorfismo
  public void control() {
    assert this.getState() == State.IN_GAME;
    IO io = new IO();
    io.writeln(actionTitle + " el jugador " + this.getTurn().take());
// se define las diferencias que tienen los diferentes hijos
    this.colocate();
    this.getBoard().write();
    if (this.getBoard().existTicTacToe(this.getTurn().take())) {
      io.writeln("Victoria!!!! " + this.getTurn().take() + "! " + this.getTurn().take() + "! " + this.getTurn().take() + "! Victoria!!!!");
      this.setState(State.FINAL);
    } else {
      this.getTurn().change();
    }
  }

```

### Jerarquia

Es importante notar que las jerarquías no tienen que ver con clasificaciones,
mas bien dividen **niveles**, en los cuales hay diferentes responsabilidades
aunque todos estos compartan un mismo objetivo.

<!--

######################################### división titulo

-->

----

<!--

######################################### división titulo

-->

## Economia del software

Tenemos un triangulo el siguiente triangulo, el cual si movemos de forma
des-igual cada una de estas variables externas la calidad sufrirá. Y
**no hay relacion sencilla** entre ellas, por ejemplo, decir que el doble
de programadores tendrán un producto en la mitad de tiempo, es como decir
que 9 mujeres tendrán un bebe en 1 mes.

- Definimos "ámbito" como las funcionalidades/requisitos.

![01_economia_de_software_triangulo](./img/01_economia_de_software_triangulo.PNG)

### Razones por las que falla un proyecto

<!-- Por que fracasa el software -->

- Falta de involucracion del usuario                    13%
- Requerimientos y especificaciones poco claras         12%
- Cambio de requerimientos y especificaciones           12%
- Poco apoyo de las gerencias involucradas              8%
- tecnologia deficiente                                 7%
- Falta de recursos                                     6%
- Expectativas poco realistas                           6%
- Objetivos poco claros                                 ..
- Tiempos poco realistas                                ..
- Nuevas tecnologías                                    ..
- Otros                                                 23%

Ahi que notas que el 30% es porque no se supieron definir los requisitos,
y un 20% por falta de apoyo de la gerencia.

### Complejidad del software

**Cambio continuo:** El mundo real cambia por lo que también debe cambiar
el software, de no cambiar el software este se vuelve menos util.

**Complejidad Creciente:** El software crece por evolución, por lo que
se estructura se complica, al menos que se hagan esfuerzos para evitar
este hecho.

**Flexibilidad/Ensamblaje:** El software tiene casi no tiene cosas como
los fontaneros o mecánicos, donde existen medidas estándar los cuales
les permiten quitar y poner, si ahi cosas como las bases de datos, motores
gráficas etc.

**Sistemas discretos:** Se refiere a que tenemos múltiples estados, con
múltiples hilos de control, lo cual agrega la mayor complejidad.

### Disciplinas del Software

<!-- Como evitar que un proyecto fracase -->

Tenemos las siguientes disciplinas

- Requisitos
  - Dirigir el desarrollo al producto correcto, Establecer limites
  
- Analisis
  - Los requisitos están en lenguaje del cliente, mientras que el
    analisis esta en nuestro lenguaje
  - Los requisitos tienen redundancias, el analisis no
  - Requisitos dice que se va a hacer, el analisis como se va a hacer

- Diseño
  - El analisis se refiere a cosas tangibles, mientras que el diseño
    en cosas intangibles, por ejemplo, campos de un usuario es cosa del
    analisis, mientras decidir entre un ORM o PL/SQL son cosas del diseño.
  - El Analisis debe ser fácil de hacer, el diseño se cuidan los detalles.
  - Entre el analisis y el diseño hay una relacion de 1 a 5, por ejemplo,
    si me tardo una hora en analizar me hará 5 en diseñar.
  - Es un analisis mas profundo, viendo cosas como lenguaje de prog,
    reutilizacion de componentes, interface de usuario, E/R de B.D.

- Programación
  - Plasmar el diseño.

- Pruebas
  - Saber que lo hecho si funciona como debe

- Ecosistema de desarrollo
  - Es la etapa donde se monitorear el software y su evolución por ejemplo.
    - MagicDraw: Facilita la edición de analisis y diseños, sacando del
      codigo ya hecho diagramas UML.
    - SonarQube; Medición del grado de bondad de los componentes

### Problemas vs Soluciones

![02_problemas_vs_soluciones](./img/02_problemas_vs_soluciones.PNG)

<!-- 

división seccion, división de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

división seccion, división de seccion 2

-->

## Calidad del Software

<!-- Que es la calidad de software -->

Factores de la calidad del software, las cuales dependen en mayor o menor
medida del diseño.

- Corrección ¿Hace lo que se le pide?
- Fiabilidad: ¿Lo hace de forma fiable todo el tiempo?
- Eficiencia: ¿Que recursos de software y software necesito?
- Integridad: ¿Puedo controlar su uso?
- Usabilidad ¿Es fácil y comodo de manejar?
- Mantenibilidad: ¿Puedo localizar los fallos?
- Flexibilidad: ¿Puedo agregar nuevas opciones?
- Facilidad de pruebas: ¿Puedo probar todas las opciones?
- Portabilidad: ¿Puedo usarlo en otra maquina?
- Reusabilidad: ¿Podre utilizar alguna parte del software en otra aplicación?
- Inter-operabilidad: ¿Podrá comunicarse con otras aplicaciones?

Signos de un mal diseño

- Rigidez vs flexibilidad para adoptar un cambio
- Fragilidad vs Robustez: No tiene propagación de errores.
- Viscosidad vcs claridad: ¿se entiende el código?
- Inmovilidad: Puedo mover el código a otra aplicación?.

**tenemos 3 jerarquías** en nuestro código

1. Código: Tenemos una clase, métodos, atributos.
2. patrones: Tenemos relacion entre clases
3. Arquitectura.

**Reusabilidad** Un caso claro son las librerías y los frameworks, los
cuales.

- librerías hacen cosas muy concretas, las cuales podemos modificar
  para adaptarlas a nuestro proyecto
- frameworks: hacen cosas muy genéricas las cuales nos dejan unos huecos
  que nosotros llenaremos, y nosotros no ejecutamos el framework, el
  framework llama a nuestro código.

<!-- 

división seccion, división de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

división seccion, división de seccion 2

-->

## Clase 4, Guias de estilo de codigo

<!-- estilos, nombres, numero de lineas -->

- Básicos del código
  - Formatear correctamente el código
  - Nombre de variables identificativos, pronunciables, etc.

- Uso de comentarios
  - No hacer
    - No poner adornos para separar secciones
    - No poner comentarios obvios o confusos
    - No es recomendable comentarios de autor, creación, etc,
    - No dejar código comentado como historial o algo asi
    - No poner comentarios de falta hacer aquello
  - Hacer
    - Comentarios oportunos, por ejemplo la explicación de un regex.

- Uso de clases
  - No hacer
    - Clases con mas de 200 lineas
    - Clases con mas de 20 métodos
  - Hacer
    - Clases con nombre de sustantivo (Carro, Evento, Solicitud, SensorDeXX etc.)
      no de acción(Iniciar, Girar, Poner), nombres del mundo tangible.
    - Métodos con nombre de verbo.

<!-- 

división seccion, división de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

división seccion, división de seccion 2

-->

## Analisis

### Decidir que clases crear

<!-- como extraer clases de mundo tangible al software -->
#### Analisis clásico

El analisis clásico es sacar las clases a partir de los derivados del
mundo tangible que recogimos de los requisitos del dominio del problema,
como son

> **Cuidado:** Solo tomar lo que hay en el mundo tangible al software hará que
> las aplicaciones colapsen en algún momento, por lo que debemos tomar el
> mundo tangible como una inspiración, y luego meterle diseño.
>
> - **Por ejemplo:** En nuestro TicTacToe la dimension del Tablero tangiblemente
>   le pertenece al Tablero, pero eso nos obliga a que Coordenada y Tablero
>   tengan una `dependencia cíclica`, entonces por diseño las dimensiones del
>   tablero se las pasamos a coordenadas, por que de todos modos tablero
>   ya conoce a Coordenadas

- Podemos inspirarnos a partir de:

  - Cosas, objetos físicos o grupos de objetos que son tangibles, como
    coches, sensores.

  - Conceptos, principios o ideas no tangibles: Herramientas usadas para
    organizar o realizar seguimiento, como son Préstamo, Reunión, Solicitud

  - Cosas que pasan: como evento, aterrizaje, interrupción, solicitud.

  - Gente: tales como roles, Doctor, Madre, Político, etc.

  - Organizaciones: Como son escuelas, gobierno, empresa, departamento.

  - Lugares Físicos: espacios reservados pasa algo en concreto, oficina,
    sala, salón, etc.

  - Otros sistemas: Sistemas externos e.j. Firebase, OpenDataX, etc.

- Al hablar con expertos del dominio, por ejemplo, preguntarle el rol a
  una persona, y que esta misma me explique de que va el problema a resolver
  y es importante **respetar su vocabulario**,

  - Un rector, almacenista, medico, etc.

#### Analisis del comportamiento

Ahi que entender que el codigo no es exactamente un reflejo del mundo
real, mas bien existe una división de responsabilidades, y una herramienta
son los casos de uso y el UML.

#### Invención Pura

Es cuando no tenemos de otra que crear clases que no tengan relacion
con el mundo, pero son necesarias para mantener una coherencia.

Por ejemplo, a la clase String (cortar, agregar, mayus, contar, etc)
no podemos meterle cosas de detectar si sigue las reglas de un poema,
aun que tenga cohesión el hecho que que un poema este conformado por
un string. La **solución** seria crear un "InterpretadorPoema", que
herede/use a String y agregá cosas para saber si cumple con las reglas
de un poema.

### Herencia

<!-- como distinguir la herencia -->

Tenemos diferentes reglas para construir la herencia, como es.

**IsA** (es un), al preguntarnos si una claseA es un claseB, entonces
podemos decir que aquí tenemos herencia, de otra forma no, `por ejemplo`
una paloma es una ave, un maestro es una persona, un calcetín es ropa
etc.

**Cuidado:** Podemos confundir herencia con composición, y una forma de
salir de la duda es pensar si lo podemos sustituir.

<!-- 

división seccion, división de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

división seccion, división de seccion 2

-->

## Diseño

### Detectar mal olor en el diseño/codigo

Veremos una serie de técnicas o casos comunes, que tienen nombre, que
nos ayudan a detectar si nuestro codigo esta mal.

#### Librería incompleta

Esto ocurre cuando tenemos una clase-A que funciona
bien pero la clase-C de alto nivel le es complicado comunicarse con la
clase-A, esto significa que debemos de hacer una clase-B intermedia
que aun que no tenga equivalente en el mundo tangible, esta se hará
cargo de que convertir una discusión compleja en una discusión mas sencilla.

#### Múltiples Responsabilidades

Una clase solo debe tener una responsabilidad,
y no tiene mayor magia pasarlo a un equivalente al mundo tangible.
{or ejemplo, en el Juego Gato=TicTacToe tenemos una clase Board que
se encarga de mover las fichas e interactuar con el usuario, entonces
debemos de separar esta responsabilidad de interactuar a otra clase,
por ejemplo, a una llamada Jugador.

#### Codigo Muerto

Es codigo que ahi esta y no se usa en ninguna parte,
y suele ser un problema mas común de lo que se cree, y cuando hay codigo
muerto significa que hay mas. Este en varios caso da miedo eliminarlo
pero lo recomendado por los grandes es eliminarlo.

- **YAGNI:** (You aren`t going to need it), es cuando se hace crea
  funcionalidad de mas, que al final nadie ocupa. por ejemplo, crear
  todos los métodos posibles de calcular, mientras que el cliente solo
  pidió sumar y restar.
- **Completa:** Esta es una idea contradictorios a YAGNI, que dice si,
  que si ya estas calientito escribiendo en una clase, haz la funcionalidad
  que estas 80% seguro que la vaz a necesitar, ya que si no lo haces al
  regresar a dicha clase te tomara frio y te tomara mas tiempo hacerlo.

#### Cohecion de metodos

Es cuando un metodo hace mas de una cosa, por ejemplo, le decimos que
elimine, y este tambien actualiza si algo es numeros pares o no, esto
seria un error por que cuando llegue alguien mas a usar nuestro codigo
le costara haberiguar por que se actualiza este numero.

#### Clases tontas

Esto ocurre cuando tenemos una clase que su responsabilidad
es muy tonta, y nos damos cuenta que debemos de que quitarla con las dos
siguientes observaciones.

- Que no hace gran cosa
- Que le complica el trabajo a otra clase/s.
- **Cuidado** No confundir clases tontas con una clase que tiene pocas
  lineas, porque puede darse el caso que tenga pocas lineas, pero estas
  pocas lineas hace que no repita codigo, o me hace mas fácil la lectura
  de clases superiores.
- **Considerar:** Que debemos de meterle imaginación para meterle a las
  clases pequeñas responsabilidad, y asi tener nuestro gran problema
  dividido en pequeños problemas de tamaño equitativo.

#### Cirugía a escopetazos

Este ocurre que al cambiar una cosa, tenemos
que cambiar varias partes de nuestro codigo, un `ejemplo` es es cuando usamos
las contantes como literales, por ejemplo, en ticTacToe una constante
es que el jugador gana al tener 3 casillas, y que este esta relacionado
al tamaño del tablero, y si no metemos esta constante en una variable,
significa que cuando queramos modificar esta constante, nos meteremos
en un lio, cambiándola en diferentes partes de nuestro codigo.

- NOTA: este mismo problema (Cirugía a escopetazos) podemos encontrarlo
  en otras situaciones a parte de uno meter constantes en variables
  `por ejemplo`, con codigo repetido donde al cambiar uno debemos de
  cambiarlo en deferentes partes.

#### Principio de menor compromiso y la menor sorpresa

Esto sucede cuando
tenemos métodos que hacen mas de una cosa, o que sin decir implementan
una función que parece obvia y por eso se metió, por ejemplo, en nuestra
aplicación de TicTacToe siempre que pregunto quien es el siguiente, yo
decido por que me parece obvio cambiar la asignación de turno, entonces
a un programador tercero le estoy agregando sorpresa, y al método algo
que no re corresponde.

#### DRY (Don't Repeat Yourself)

Si dentro de nuestra clase estamos haciendo lo mismo,
en diferentes métodos entonces podría significar que no estamos reutilizando
métodos y que repetimos codigo. Por ejemplo. En la imagen solucionamos
la repetición utilizando métodos que hacían la misma funcionalidad pero
con una peque;a variante.

![03_repeticion_codigo_uso_frecuente_de_parametros](./img/03_repeticion_codigo_uso_frecuente_de_parametros.PNG)

#### KISS (keep it simple, stupid)

Es cuando un método es complejo, y
difícil de leer, también tienden por su complejidad no adaptarse a cambios
externos, `por ejemplo`, en el TicTacToe al calcular quien gano, este
debería ser fácil de leer y si cambiamos la dimension de 3 a 4 casillas
este debería de seguir funcionando,.

![04_kiss-keep-it-simple-stupid_1](./img/04_kiss-keep-it-simple-stupid_1.PNG)

<!-- gestion de errores -->

#### Programación defensiva

Es cuando ocurre tenemos una entrada fuera
de lo ordinario y nosotros obligamos al de arriba a agregar un condicional
o el métodos mismo se encarga de mostrar el error, `por ejemplo`, si tenemos
un métodos que calcula el factorial, y el usuario introduce un numero
negativo, nosotros regresamos un -1 obligando al de arriba (cualquiera que
use el métodos) a mirar si el valor en -1, o agregar un condicional de si
la entrada en negativa, en el mismo métodos que calcula el factorial mostrar
que hubo un error.

**Tener en cuenta:** Son dos cosas diferentes errores fuera de nuestro
sistema y erres dentro de mi sistema, la entrada o respuesta de algo
externo ya sea un usuario u otro sistema, son fallos que debemos de validar
antes de que entren a nuestro sistema, ya cuando entran estos datos deben
ser ideales, por que validar estos en diferentes partes de nuestra lógica
solo nos traerá problemas, por eso la validación de datos es casi obligatoria,
por que eso nos ahorrara muchos problemas y codigo sucio, porque tendremos
métodos mas simples y de única responsabilidad.

**Solución:**: Para solucionarlo hacemos "diseño por contrato", esto
quiere decir, yo creo un métodos al cual tu (mas arriba) debes de llamar
de cierta manera, y si no lo haces te va a reventar, `por ejemplo`,
nuestro métodos de factorial se deben de asegurar arriba de que nunca
me deben de llamar con un valor negativo, esto lo hacemos con una
validación en la entrada de datos, `en la imagen` podemos ver que el
métodos de poner una marca en en el tablero ('X', 'O'), en una coordenada,
creamos un contrato que dice, di tu me llamas con una coordenada o marca
vació/nulo entonces to reviento.

Como lo ve el curso

![05_disenio_por_contrato](./img/05_disenio_por_contrato.PNG)

Idea

![06_idea](./img/06_idea_mandar_error_al_main_controlador_de_algo_que_no_debio_haber_passado_paso.PNG)

#### Inapropiada Intimidad o dependencia cíclica

<!-- dependencias cíclica -->

Es cuando tenemos una relacion ObjetoA que conoce a la ObjetoB, y ObjetoB
conoce al ObjetoA, es bidireccional y peor aun si tenemos esto con tres,
y esto debemos de tratar de romperlo (habrá ocasiones donde no se podrá),
por que esto nos complican el entendimiento, pruebas, etc.

Por ejemplo: para evitar que Board y Coordinate se conozcan mutuamente
lo que hacemos es pasar dimensiones a Coordinate de todos modos Board
ya conoce a Coordinate, pero asi hacemos que Coordinate no tenga porque
conocer a Board.

![08_dependencia_ciclica](./img/08_dependencia_ciclica.PNG)

#### Envidia de caracteristicas

<!-- envidia de características -->

Nos damos cuenta de esto cuando una clase le hace mucho get a otra clase,
cuando esto pasa, lo mas común es crear un nuevo método que haga aquello
que hace la clase de arriba que hace que le pida mucho una propiedad de
esta, si no hay que re-plantearnos el diseño, ya que podría sobrar una
clase o faltar una clase.

**Por ejemplo,** en nuestro TicTacToe en un punto tuvimos que la clase
Board hacia mucho get a la clase Coordenadas para calcular si había 3
en raya, y esto hacia que el codigo de Board se complicara, entonces
le delegamos la tarea de averiguar a Coordenada si dos coordenadas
estaban en linea o no, y asi aun el Board averiguaba quien había ganado,
pero lo hacia con la ayuda de Coordenada y ya era mas fácil para Board
y Coordenada que no hacia gran cosa ya tenia aun mas sentido de existir.

#### Herencia Rechazada

Es cuando tenemos una herencia que le pasa atributos o metodos, que no
le atañen a alguna de sus hijas.

#### Jerarquias paralelas de herencia

Ocurre cuando tenemos una herencia doble como la de abajo, y cada vez
que agregamos nueva funcionalidad, debemos de agregarlo en las dos
herencias de abajo, por ejemplo.

- Ejemplo 1
  - Departamentos de salud privada
    - Departamento de CardiologiaPrivada
    - Departamento de UrgenciasPrivada
  - Departamentos de salud publica
    - Departamento de CardiologiaPublica
    - Departamento de UrgenciasPublica

- Ejemplo 2
  - ColocateController
    - PutController
      - ManualPutController
      - RandomPutController
    - MoveController
      - ManualMoveController
      - RandomMoveController

Cada vez que querramos agregar un nuevo departamento, tendremos que
agregarlo tanto para privada como para publica, o si queremos agregar
un Mover por I.A. tendremos que agregar tanto en Put y Move, y cuando
tengamos que agregar una inversion de control.

##### Solucion Inversion de control (ver abajo)

#### Preguntar tipos (instanceOf)

Segun los grandes (Martin Fowler) cuando preguntas por tipos, estas
violando principios fundamentales del P.O.O. (ej. sustitucion de LisKov)
por lo que ahi que evitarlo a toda costa de las siguientes formas.

1. **Polimorfismo**: (Recomendada) Usando abstracciones (clases padre
   o instancias) que sea cual sea el tipo el objeto se pueda tratar de
   la misma forma.
2. **Tecnica del doble despacho**: (Buscar abajo), usamos esta si no nos
   queda mas remedio que tener cambiar el comportamiento segun el tipo.

#### Hablar con desconocidos

<!-- Hablar con desconocidos -->

- Fuentes
  - [Wikipedia](https://en.wikipedia.org/wiki/Law_of_Demeter)
  - [AditosAlTrabajo](https://www.adictosaltrabajo.com/2015/07/24/ley-de-demeter/)

Es cuando una clase indirectamente conoce a otra, por ejemplo, cuando tenemos
tenemos un objeto que trae por un geter a otro objeto, lo cual esto esta
haciendo que se conoscan entre todos, en el codigo de abajo podemos que
como no tenemos accesso a Profesor, entonces lo hacemos por medio de
asignatura.

```java
System.out.println(
  universidad
    .getDepartamento(“Departamento de Informática”)
    .getAsignatura(“Ingeniería del Software”)
    .getProfesorCoordinador()
);
```

**Solucion**: a travez de la `ley de demeter` podemos hacer lo de abajo,
en el cual tenemos un intermediario, y de esta forma ya no conocemos
a tantas clases.

```java
System.out.println(
  universidad.getProfesorCoordinador(“Departamento de Informática”, “Ingeniería del Software”)
);
```

<!--

######################################### división titulo

-->

----

<!--

######################################### división titulo

-->

### Solucciones

#### GRASP (Expertos en la informacion)

- Fuente: [Wikipedia](https://en.wikipedia.org/wiki/GRASP_(object-oriented_design))

General Responsibility Assignment Software Patterns (GRASP), son un conjunto
de buenas practivas que se concentran, en que debemos de partir las
responsabilidades por medio de la informacion que contienen los objetos,
lo cual da paso a otros patrones como lo son Creator, Controller, Indirection
(intermediario para facilitar comunicacion).

#### Template method pattern

Se homogeneiza ambos métodos y se conforma una jerarquia de herencia de
ambas clases heredando de la clase padre `ver "abstract class ColocateController"`,
ahi podemos ver como un método que usan sus clases hija usa dentro del metodo un
metodo sin definir, lo estamos obligando a que el metodo que vaya a crear
tenga un comportamiento en una especie de plantilla.

**NOta:** Puden ser mas de uno y regresar algo para interactuar con este,
lo podemos ver en el ejemplo, en la parte de `errorToPut()` donde regresa
un booleano, y aparte esta definido en al padre, pero una de sus hijas
lo sobre escribe.

```java
public abstract class ColocateController {
  
  protected void put(String actionTitle, String targetTitle) {
    //... lineas de comportamiento comun
!   this.prePut();
    //... lineas de comportamiento comun
  }
  //comportamiento especifico
  protected abstract void prePut();

  //tambien podria darse el siguiente caso
  protected void put(String actionTitle, String targetTitle) {
    // puede que algunos o incluso todos no los defina el padre, pero
    // obligamos a los hijos a definir lo especifico.
    this.precenter();
    this.prePut();
    this.put();
    this.postPut();
  }
}
//-----------------------------------------------------
public class MoveController extends ColocateController {
  @Override public void control() {
    // se usa comportamiento general combinado con comportamiento generico
    this.put("Mueve", "A");
  }

  @Override protected void prePut() {
    // se define comportamiento comun
  }
```

#### Inversion de control

Es cuando una clase hace un trabajo pero dependiendo de algun estado,
lo hace de una u otra manera, pero como es un lio que esta misma clase
decida cuando hace su trabajo de una manera u otra, entonces le inyectamos
alguien que decida esto (inyectamos control), para que este inyectado,
decida cuando debe de hacer su trabajo de una manera o de otra.

**Analogia**, un foco ilumina de diferentes intencidades, a veces brilla
mucho, a veces brilla poco, y en lugar de hacer un foco que brilla poco
y otro que brilla mucho y usar uno u otro por polimorfismo (lo cual nos
haria caer en **Jerarquias paralelas de herencia**), le inyectamos un
objeto el cual este decidira cuando tendra que brillar mucho y cuando
debera brillar poco.

#### Inyeccion de dependencias (DIP)

<!-- inyeccion de dependencias  -->

Inyeccion de dependencias, es el uso de una interface para que el objeto
dependiente hable y conozca la interface, y no con el objeto, de esta
manera la clase no depende directamente del objeto si no de la interface.
A traves de la inyeccion de dependencias es que logramos la inversion de
control, y estas se diferencian en su objetivo, mientras que inversion de control,
su objetivo es que aoguien mas me controle, inyeccion de dependencias
su objetivo es que es no conocer directamente al objeto que depende.

#### Inversion de Dependencias

Esta sirve para romper acoplamientos comunmente a travez de interfaces,
y en el codigo no hay diferencia con la `inversion de dependencias` pero
si se diferencian en su intencion, es decir, que la inversion de dependencias
es para desacoplar, y la inversion de control es para modificar el
comportamiento de un objeto.

#### Visitor Pattern

- Fuente: [wikipedia](https://en.wikipedia.org/wiki/Visitor_pattern#Java_example)

Esta es cuando el objeto que quiere distinguir el tipo de un objeto que
este usa, y de lugar de preguntar su tipo (buscar "Preguntar tipos"),
en el objeto que entra hacemos que entre el objeto que quiere saber su
tipo, y de esta forma diferenciar que tipo de objeto es.

**Problema que soluciona:** Al evitarme preguntar el tipo me hebito la
cirujia a escopetasos, por que si cada vez que quisiera hacer algo con
persona, es potencial que tenga que preguntar que tipo de persona es en
distintos lugares, lo cual cada vez que agrego otro tipo de persona,
entonces deberia deagregarlo en todas aquellos lugares donde pregunto
esto.

**Ventajas:** Si agrego un nuevo tipo de persona, entonces por la interface
o el metodo abstracto estoy obligado a declarar en el tipo de persona que
estoy creando, que hacer, en los distintos escenarios en los que se puede
encontrar una persona. Lo cual si estamos colaborando con otros desarrolladores
estoy ya sabran que el nuevo tipo que estan creando, deben definir un
escenario para los que se puede encontrar una persona.

**Cuidado:** el primer codigo lo hacemos que la persona conosca al
recepcionista y el recepcionista a la persona, lo que tenemos una
`dependencia cíclica`, y tambien es inadecuado porque si persona debe de interactuar
con mil elementos diferentes, por ejemplo, interactuar con chofer, cocinero,
zapatero, cajero, etc. etc., entonces estamos en un lio, porque por cada
uno de ellos debemos de implementar un metodo inflando mucho nuestra clase.
Entonces la **solucion** (ver `Double dispatch`) seria que la persona y sus diferentes
tipos, conoscan la interface no con quien interactuan, entonces de esta
forma por cada tipo de interaccion podemos crear una clase, lo cual
evitamos que se infle nuestra clase.

**Analogia 1: (Curso)** El recepcionista de un hotel tiene proihibo preguntar
si el que entra es hombre o mujer, entonces este para saber si es hombre
o mujer, este hace hablar a la persona, y por medio de su tono de voz,
entonces el recepcionista ya sabe si es hombre o mujer.

- Al agregar un proveeedor entonces en proveedor estoy abligado a definir
  cual es mi interaccion con el recepcionista, lo que es algo bueno,
  porque asi lo defino en el mismo proveedor y no en distintas parte de
  mi codigo.

```java
public class Restaurante {
  private Recepcionista recepcionista;
  
  private Restaurante(){ recepcionista = new Recepcionista(); }
  
  public static void main(String[] args) {
    new Restaurante().simularEscenario();
  }

  private void simularEscenario() {
    Random random = new Random(System.currentTimeMillis());
    for (int i = 0; i < 5; i++) {
      Persona persona = null;
      if(random.nextInt(2) % 2 == 1) {  persona = new Hombre(); }
      else {                            persona = new Mujer(); }
      this.simularEscenario(persona);
    }
  }
  
  private void simularEscenario(Persona persona) {
    persona.saludar();
    recepcionista.recibir(persona);
    persona.despedirse();
    recepcionista.agradecerVisita();
    System.out.println("... "); System.out.println("... pasa el tiempo!"); System.out.println("... ");
  }
}

public class Recepcionista {
  public void recibir(Persona persona) {
    System.out.println("!! Soy un recpcionista que se alegra de su visita");
    persona.aceptar(this);
  }

  public void visitar(Mujer mujer) {
    System.out.println("!! Soy un recpcionista que se admira de su belleza");
    mujer.escucharHalago(this);
  }

  public void visitar(Hombre hombre) {
    hombre.recibirPalmada(this);
  }

  public void recibirPropina(int euros) { System.out.println("!! Soy un recpcionista que gané "+euros+" euros"); }
  public void agradecerVisita() { System.out.println("!! Soy un recpcionista que agradece la visita"); }
}

public abstract class Persona {
  public abstract void aceptar(Recepcionista recepcionista);

  public void saludar(){ System.out.println("?????????? Soy una persona que saluda al entrar"); }
  public void despedirse() { System.out.println("?????????? Soy una persona que se despide al salir"); }
}

public class Mujer extends Persona {
  public void escucharHalago(Recepcionista recepcionista) {
    System.out.println("++++++++++ Soy una mujer ruborizada");
    recepcionista.recibirPropina(3);
  }

  @Override
  public void aceptar(Recepcionista recepcionista) {
    recepcionista.visitar(this);
  }
}

public class Hombre extends Persona {
  public void recibirPalmada(Recepcionista recepcionista) {
    System.out.println("---------- Soy un hombre lleno de vanidad");
    recepcionista.recibirPropina(3);
  }

  @Override
  public void aceptar(Recepcionista recepcionista) {
    recepcionista.visitar(this);
  }
}
```

**Analogia 2 (Mia):** Si voy a una borrachera y tengo la accion tomar, entonces
segun el tipo de bebida yo tomo mas o menos de esta bebida, por ejemplo,
si es tequila entonces tomo menos, y si es cerveza tomo mas, una forma
seria preguntar si es tequila o cerveza, pero esto esta mal visto, entonces
para saber si es uno u otro, entonces la bebida al darme el sabor, yo
puedo deducir si este es tequila o cerveza.

```java
class Main {

    public static void main(String[] args) {
        List<Bebida> bebidas = getRandomDrinks();
        Persona yo = new Persona();
        for (Bebida bebida : bebidas) {
            yo.tomar(bebida);
        }
    }

    private static List<Bebida> getRandomDrinks() {
        List<Bebida> bebidas = new ArrayList<>();
        Random random = new Random();
        for (int i=0; i<5; i++) {
            if(random.nextInt(2) % 2 == 1) { bebidas.add(new Cerveza()); }
            else { bebidas.add(new Tequila());}
        }
        return bebidas;
    }
}

class Persona {
    void tomar(Bebida bebida) {
// BIEN ECHO
        bebida.getSabor(this);
// MAL HECHO
//      if (bebida instanceof  Tequila) {
//          this.tomarTequila();
//      } else if(bebida instanceof  Cerveza) {
//          this.tomarCerveza();
//      }
    }

    void probar(Cerveza cerveza) { this.tomarCerveza(); }
    private void tomarCerveza() { System.out.println("Es Cerveza, Beberla sola"); }

    void probar(Tequila tequila) { this.tomarTequila(); }
    private void tomarTequila() { System.out.println("Es Tequila, Beberla combinada"); }
}

abstract class Bebida { abstract void getSabor(Persona persona); }

class Cerveza extends Bebida {
    @Override void getSabor(Persona persona) { persona.probar(this); }
}


class Tequila extends Bebida {
    @Override void getSabor(Persona persona) { persona.probar(this); }
}
```

**Cuidado:** Este codigo aun tiene mejoras, en "Double dispatch" vemos
lo mismo pero con una mejor aproximacion.

#### Double dispatch

- Fuente: [Wikipedia](https://en.wikipedia.org/wiki/Double_dispatch)

Como vimos arriba tenemos el problema de que nuestra clase persona,
puede crecer mucho, cuando tenga que interactuar con muchos, ya que por
cada uno por el que tenga que definir tendremos que agregar un metodo,
y tambien tenemos una `dependencia cíclica`.

**Analogia 1:** El **problema^** es que una persona se las ve complicadas,
porque debe de saber exactamente como interactuar en una determinada
persona, es decir, tiene una forma definida de tratar a un recepcionista,
mesero, cajero, estacionador, etc. etc.. Entonces la **solucion** es
abstraer todo este tipo de interacciones, es dicir, a todos ellos los
tratara como Servidores que lo atienden, y por medio de esta atencion
sabra como interactuar.

- Codigo: Como vemos la persona no conoce a Recepcionista ni a mesero,
  solo en el caso del Hombre que este da propina, pero bien podria no
  reconocerlo, a diferencia de la mujer que no reconoce al mesero de
  servidor.

```java
public class Restaurante {

  private Recepcionista recepcionista;

  private Camarero camarero;

  private Restaurante(){
    recepcionista = new Recepcionista();
    camarero = new Camarero();
  }

  public static void main(String[] args) {
    new Restaurante().simularEscenario();
  }

  private void simularEscenario() {
    Random random = new Random(System.currentTimeMillis());
    for (int i = 0; i < 5; i++) {
      Persona persona;
      if (random.nextInt(2) == 0) {
        persona = new Hombre();
      } else {
        persona = new Mujer();
      }
      this.simularEscenario(persona);
    }
  }

  private void simularEscenario(Persona persona) {
    persona.saludar();
    recepcionista.recibir(persona);
    persona.pedir();
    camarero.servir(persona);
    persona.despedirse();
    camarero.recoger();
    recepcionista.agradecerVisita();
    System.out.println("... ");
    System.out.println("... pasa el tiempo!");
    System.out.println("... ");
  }
}

public interface Servidor {
  void atender(Mujer mujer);
  void atender(Hombre hombre);
}

public class Recepcionista implements Servidor{

  public void recibir(Persona persona) {
    System.out.println("!!!!!!!!!! Soy un recpcionista que se alegra de su visita");
    persona.aceptar(this);
  }

  @Override
  public void atender(Mujer mujer) {
    System.out.println("!!!!!!!!!! Soy un recpcionista que se admira de su belleza");
    mujer.escucharHalago();
  }

  @Override
  public void atender(Hombre hombre) {
    hombre.recibirPalmada(this);
  }

  public void recibirPropina(int euros) {
    System.out.println("!!!!!!!!!! Soy un recpcionista que gané " + euros + " euros");
  }

  public void agradecerVisita() {
    System.out.println("!!!!!!!!!! Soy un recpcionista que agradece la visita");
  }

}

public class Camarero implements Servidor {

  public void servir(Persona persona) {
    persona.aceptar(this);
  }

  @Override
  public void atender(Mujer mujer) {
    System.out.println("¡¡¡¡¡¡¡¡¡¡ Soy un camarero que invita a unas fresas");
  }

  @Override
  public void atender(Hombre hombre) {
    System.out.println("¡¡¡¡¡¡¡¡¡¡ Soy un camarero que invita a un coñac");
  }

  public void recoger() {
    System.out.println("¡¡¡¡¡¡¡¡¡¡ Soy un camarero que recoge la mesa");
  }
}

public abstract class Persona {

  public void saludar(){
    System.out.println("?????????? Soy una persona que saluda al entrar");
  }

  public abstract void aceptar(Servidor visitadorPersona);

  public void pedir() {
    System.out.println("?????????? Soy una persona que pide un menú");
  }

  public void despedirse() {
    System.out.println("?????????? Soy una persona que se despide al salir");
  }
}

public class Hombre extends Persona {
  public void recibirPalmada(Recepcionista recepcionista) {
    System.out.println("---------- Soy un hombre lleno de vanidad");
    recepcionista.recibirPropina(3);
  }

  @Override
  public void aceptar(Servidor visitadorPersona) {
    visitadorPersona.atender(this);
  }
}

public class Mujer extends Persona {

  public void escucharHalago() {
    System.out.println("++++++++++ Soy una mujer ruborizada");
  }
  @Override
  public void aceptar(Servidor visitadorPersona) {
    visitadorPersona.atender(this);
  }

}
```

**Analogia 2:** En un taller tienen el **problema** de que cuando quieren
cortar algo, el cortador debe de saber como cortar, madera, fierro, vidrio,
plastico, mantiquella, carne, alambre, etc, etc, lo cual son muchas
cosas, para **solucinarlo** el cortador va a delegar la tarea de cortar
a cada uno de los meteriales, de esta forma cada material se cortara asi
mismo.

- Error: Me estaba complicando la vida, pensando en diferentes tipos
  de cortadores, y diferentes tipos de materiales, y la realidad es que
  el cortator la tiene facil, porque a todos los diferentes materiales solo
  los estamos cortando, y cada material sabe cortarse asi mismo

- En el diagrama podemos ver como la clase de alto nivel, en este caso
  `Machinist` no conoce a ninguna de las implementacion, solo conoce
  a las abstracciones de `material` y de `cutter`, y de esta forma
  no tenemos una barrera donde podriamos tener dos paquetes diferentes,
  tanto un paquete taller y otro de materiales.

<!-- 09_uso_correcto_de_interfaces -->

![09_uso_correcto_de_interfaces](./img/09_uso_correcto_de_interfaces.PNG)

- Codigo

```java
public class Taller {

    public static void main(String[] args) {
        List<Cutter> meteriales = getRandomMaterials();
        Machinist maquinista = new Machinist();
        meteriales.forEach(maquinista::cut);
    }

    private static List<Cutter> getRandomMaterials() {
        List<Cutter> materials = new ArrayList<>();
        Random random = new Random();
        for (int i=0; i<10; i++) {
            int randomNum = random.nextInt(4);
            if(randomNum == 0) {
                materials.add(new Wood());
            } else if(randomNum == 1) {
                materials.add(new Steal());
            } else if(randomNum == 2) {
                materials.add(new Meat());
            }  else if(randomNum == 3) {
                materials.add(new Paper());
            }
        }
        return materials;
    }
}

class Machinist {
    private String lastWork;
    void cut(@NotNull Cutter material) {
        material.breakUp(this);
        System.out.println(this.lastWork + ", Success: " + material.getCutted());
    }
    void setLastWork(String lastWork) { this.lastWork = lastWork; }
    // EVITAMOS QUE LA CLASE ENGORDE
//  void cutWood(Wood material) { System.out.print("cut Wood: " + material.isCutted()); }
//  void cutSteal(Steal material) { System.out.print("cut steal: " + material.isCutted() ); }
}
// ----------------------------------------
interface Cutter {
    boolean getCutted();
    void breakUp(Machinist cutter);
}
// ----------------------------------------
class Wood implements Cutter {
    private boolean cutted = false;
    @Override public void breakUp(Machinist cutter) {
        cutted = true;
        cutter.setLastWork("Wood cutted");
    }
    @Override public boolean getCutted() { return cutted; }
}

class Steal implements Cutter {
    private boolean cutted = false;
    @Override public void breakUp(Machinist cutter) {
        cutted = true;
        cutter.setLastWork("Steal cutted");
    }
    @Override public boolean getCutted() { return cutted; }
}

class Meat implements Cutter {
    private boolean cutted = false;
    @Override public void breakUp(Machinist cutter) {
        cutted = true;
        cutter.setLastWork("Meat cutted");
    }
    @Override public boolean getCutted() { return cutted; }
}

class Paper implements Cutter {
    private boolean cutted = false;
    @Override public void breakUp(Machinist cutter) {
        cutted = true;
        cutter.setLastWork("Papper cutted");
    }
    @Override public boolean getCutted() { return cutted; }
}
```

<!--

######################################### división titulo

-->

----

<!--

######################################### división titulo

-->

#### Ley de demeter

<!-- Ley de demeter -->

- Fuentes
  - [Wikipedia](https://en.wikipedia.org/wiki/Law_of_Demeter)
  - [AditosAlTrabajo](https://www.adictosaltrabajo.com/2015/07/24/ley-de-demeter/)

Solucion proncipalmente de `Hablar con desconocidos` que dice,

Para todas las clases, C, y para todos los métodos, M, contenidos en C,
todos los objetos a los cuáles M envía un mensaje deben de ser instancias
de clases asociadas de la siguiente manera:

- Las clases que definen a los argumentos de M, incluyendo a C
- Las clases que definen a los atributos de C

Se considera que los objetos creados por M, o por métodos a los que se
llama desde M, y las variables globales son argumentos de M.

### Relacion entre clases

#### Diferencia entre Herancia e implementacion (clase abstracta e interface)

<!-- Diferencia entre Herancia e implementacion -->

Por ejemplo, tenemos una persona y una almohada y en algun algoritmo
queremos decimos que le podemos pegar tanto a una persona como a una
almohada, no ahi manera de relacionar estos dos por herencia, ya que
no tienen nada que ver entre ellos, pero podremos implementar una interface
donde a ambos se les puede pegar, aun que no esten relacionados entre
ellos.

<!-- relaciones entre clases/objetos -->

Una relacion entre objetos se define cuando un objeto le manda un
mensaje de uno a otro, y se clasifican en cuatro (mas adelante veremos
los detalles de cada una).

**Cuidado** Cuando hablamos de relacion casi siempre hablamos de objetos,
puede haber relacion clases, pero **creo** cuando hablamos de estáticos
entonces la comunicación es entre clases pero eso algo poco común de tratar,
por ejemplo, las constantes estáticas, ya que si una clase usa una propiedad
de otra ya tienen una relacion.

- Relacion de composición
- Relacion de agregación
- Relacion de asociación
- Relacion de uso o dependencia

**Cuidado:** Casi siempre dudaremos cual relacion tenemos y según los
expertos esta bien porque igual hay matices como se ve en la imagen, y
hay que discutir bien las cosas para resolver las dudas, y lo
mas importante es saber cuales colaboran, y evitar que todos se conozcan
a todos, entre menos se conozcan entre ellas mejor.

![07_grafico-tipo-relaciones](./img/07_grafico-tipo-relaciones.PNG)

Estas cuatro tipos de relaciones tienen un parecido al mundo tangible, como
**analogía** tenemos que, las personas tenemos diferentes relaciones,
como laboral, amistad, familiar, amorosa, enemistad, etc. y en cada una
de estas intervienen diferentes características, como fuerza estrecha o
lejana, sentimientos amor u odio, si es buena o toxica, y esto nos ayuda
a clasificar estas relaciones.

<!-- características de las relaciones -->

Las relaciones entre clases tenemos diferentes características como.

**1, Visibilidad**: relacion de carácter privado o publico, de la colaboración
entre dos objetos, y se refiere si otros pueden disponer del objeto.

- Ejemplo 1: Un profesor colabora de forma privada con su bolígrafo,
  que mordisquea y nadie mas "colabora" con este bolígrafo, por otra
  parte "colabora" con un proyector que otros profesores colaboran con
  este proyector.
- Ejemplo 2: Yo colaboro con el autobús para ir al cole al igual que
  otros compañeros, pero solo yo colaboro con mi celular.

**2, Temporalidad**: Mayor o menor colaboración entre dos objetos, esta
puede ser temporal o permanente.

- Ejemplo: Yo colaboro temporalmente con una puerta, pero colaboro de
  forma permanente con mis pies.

**3, Versatilidad:** Intercambiabilidad de los objetos en la colaboración
con otro objeto, se refiere a que si elimino/eliminan ese objeto aun
puede funcionar la clase.

- Ejemplo: Yo colaboro con un suéter o cualquier suéter, y si explota
  un suéter con los que colabore me da igual, en cambio colaboro solo
  con un par de pulmones y si se descomponen yo me descompongo.

#### Diferencia entre composición y agregación

Entre las diferentes características de las relaciones las podemos ver
las siguientes diferencias de la sig. manera, para saber porque se
clasifican de esta manera

<!-- Diferenciar entre las Relaciones, cuales son los tipos de relaciones -->
| Relacion    | Visibilidad | Temporalidad | Versatilidad |
| :--------:  | ----------- | :----------: | :----------: |
| Composición | privada     | Permanente   | Inflexible |
| Asociación  | publica     | Permanente   | Inflexible |
| Uso         | privada     | Temporal     | Flexible   |
| agregación  | publica     | Temporal     | Flexible   |

Preguntas para descubrir que relacion es

- `¿Los demás pueden disponer de XXX? o ¿con el que colabora colaboran los otros?`
  - R. No = privado, SI = publico

- `¿Cuanto dura XXXX?`
  - R. PERMANENTE / TEMPORAL

- `¿Que pasa si quitan XXXXX?`
  - R. Se descompone = INFLEXIBLE, funciona = Flexible

**Analogía Composición**: Una persona tiene una relacion de tipo composición
con su cabeza, esta solo le pertenece a el y nadie mas (privada), y sin su
cabeza esta no podría sobrevivir (Inflexible), y siempre esta atado
a su cabeza (Permanente).

**Analogía Agregación**: Una persona tiene una relacion de tipo agregación
con su familia, ya que puede pertenecer a la familia donde nació,
y a la familia que formo (publica), pero sin estas 2 la persona puede
sobrevivir (Flexible).

**Analogía Asociación**: Una Escuela tiene un ID que todos deben conocer
para saber su legitimidad de la escuela (Publico), su ID es (Permanente)
ya que debe de siempre identificarlo mientras este exista , y es (Inflexible)
por que sin este no tiene como registrarse ante el estado.

**Analogía Uso**: Un Psicólogo (suponiendo que el psicólogo no necesite
recordar, por que si recuerda seria Asociación por que afectaría,
que me lo cambien porque tendría el cliente que contale todo de nuevo)
tiene un cliente (Privado) ya que nadie tiene que saber que cosa trabaja
con el, su relacion es (Temporal), ya que las secciones tienen horario,
y es versátil por que un psicologo puede atender a varios clientes sin problema.

#### Relacion de composición

Esta relacion suele tener `un-todo y una-parte-de` se descubre preguntándonos
`¿la ClaseB es parte de la ClaseA?`, **cuidado** la composición y agregación
se parecen y nos sirve la misma pregunta, pero se diferencian por sus
características.

- Suele tener una **visibilidad** PRIVADA: por ejemplo, un departamento
  académico pertenecer a una escuela. este departamento tiene una
  visibilidad privada porque no tienen porque venir otros  a
  tocar este departamento, pero puede darse la excepción que
  colabore al menos de forma indirecta.
- Suele tener una **Temporalidad** PERMANENTE: porque un departamento academico
  dura tanto como dure una escuela.
- Suele tener una **versatilidad** INFLEXIBLE: por que si un departamento
  académico falla la escuela no puede funcionar.

```java
// Relacion de composicion, no es estricto que la relacion se de asi
class Persona {
  // PERMANENTE: Mientras yo exista este objeto existira.
  // PRIVADA: Nadie puede acceder a mi cabeza, solo yo
  private Cabeza cabeza;
  public Persona() {
    //comúnmente el mismo objeto crea su propia parte, podría decirse,
    //que mi cabeza me la fabrique yo
    //
    this.cabeza = new Cabeza();
  }
  cualquierMetodoQueOcupeCabeza() {
    // INFLEXIBLE: no puedo funcionar sin mi cabeza
    assert this.cabeza!=null;
  }
}
```

#### Relacion de agregación

Esta relacion suele tener `un-todo con partes-desacopladas` y la descubrimos
preguntándonos `¿la ClaseB forma parte de la ClaseA?` pero esta se
comparte con otras clases.

**Analogía:** Yo formo parte de un club pero el club puede existir sin
mi y yo puedo existir sin el club,

- **Visibilidad**: Tiene una visibilidad PUBLICA, ya que me por factores
  externos mi familia puede aumentar o reducir.
- **Temporalidad**: suele ser TEMPORAL, ya que por factores externos
  (divorcio, muerte, legales, etc), una relacion familiar puede terminar
- **Versatilidad**: Suele ser FLEXIBLE, Ya que puedo sobrevivir sin mi
  familia.

Ejemplo:

```java
// Relacion de agregación, no es estricto que la relacion se de asi
class Persona {
  private Map<FamiliarRelationType, Persona> familiares;
  metodoQueUseFamilia() {
    if(familia.size == 0 ) {
      return "sin familiar vivo";
    }
  }
  // PUBLICA: por que el objeto me lo paso "el todo" (yo no lo cree)
  // asi que otros objetos tienen acceso al objeto que me pasan
  // FLEXIBLE: Me da igual que objeto de tipo familiar me pasen.
  public addFamiliar(...)...
  
  // TEMPORAL: Me pueden quitar el objeto y yo aun funcionar
  public quitarFamiliar(...)...
  
}
```

#### Relacion de Asociación

Es cuando un objetoA necesita al objetoB1 para funcionar correctamente,
y fallaría si le pasáramos el objetoB2 y podríamos descubrir la relacion
con la siguiente pregunta `¿la claseA debe usar solo una instancia de la claseB?`

**Analogía:** Los ciudadanos están asociados con la CFE, los ciudadanos
necesitan de electricidad y CFE se las proporciona, a mi no me conoce ni
me necesita como individuo la CFE para funcionar, y estoy obligado a
usarlo porque 1] necesito electricidad para funcionar plenamente, y
2] solo existe CFE para proporcionarme electricidad.

- **Visibilidad:** Publica, A la CFE la conocen muchas personas.
- **Temporalidad:** Duradero casi permanente, siempre necesitare electricidad
  habrá raras ocasiones donde no la necesite, por ejemplo, una casa que
  abandono unos años.
- **versatilidad:** Inflexible, Necesito de la CFE para conseguir
  electricidad, es complicado conseguir electricidad de otro lado.

**Ejemplo:** Debemos de usar la clase que hace conexión al servidor, y
solo a una instancia de ella, ya que manejar mas de una instancia seria
dar inconsistencias, mientras que a la clase usuario la usamos pero da
igual que instancia de usuario usemos.

```java
// Relacion de Asociación, no es estricto que la relacion se de asi
class ClienteCFE {
  private CFE cfe;
  // PUBLICA: como el objeto me lo paso "el todo", yo como otros tienen acceso a el
  Persona(CFE cfe) {
    // PERMANENTE> Siempre existira CFE conmigo.
    this.cfe = cfe;
  }
  void pagar() {
    //INFLEXIBLE: porque sin el objeto CFE yo no puedo funcionar
    cfe.pagar();
  }
}
```

#### Relacion de USO

- NOTA: También llamado de Dependencia pero es mas confuso.

Es cuando un objetoA usa al objetoB1, pero a la objetoA puede seguir
funcionando correctamente ya sea con el objetoB1 o el objetoB2, y podríamos
preguntarnos `¿La claseA puede usar cualquier instancia de la claseB?`

**Analogía:** Comer una hamburguesa.

- **Visibilidad:** Privada, Solo yo puedo colaborar com mi hamburguesa,
  nedie puede venir a darle una mordida (conocerla) ya que es mia.
- **Temporalidad:** Momentánea, Su relacion solo es mientras me la acabo.
- **versatilidad:** Flexible, me da igual cual me como, con que se pueda
  considerar una hamburguesa, yo me la como.

```java
// Relacion de USO, no es estricto que la relacion se de asi
class Hamburguesa {
  void comer() {
    // PRIVADA: yo la creo y nadie tiene acceso a ella (acceso al objeto, a la clase es diferente),
    // VERSATILIDAD: me da igual que objeto me llegue yo la consumo
    var hamburguesa = new Hamburguesa();
    this.calorias += hamburguesa.calorias;
    // MOMENTÁNEA: en tanto termino el objeto desaparace
  }
}
```

<!-- 

división seccion, división de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

división seccion, división de seccion 2

-->

## Ejemplos

### Quitar codigo repetido por Herencia

Tenemos los dos controladores siguientes, los cuales repiten el codigo
de poner la ficha seleccionada si esta esta vacia, pero esta repetida la
linea lo cual es un problema.

```java
public class PutController {
  private Turn turn;  // encargado de cambiar el tuno y saber de quien es el turno
  private Board board;
  private Coordinate target;
  public PutController(Turn turn, Board board) {
    assert turn != null;
    assert board != null;
    this.turn = turn;
    this.board = board;
    target = new Coordinate();
  }

  public void put(){
//Codigo repetido 
!   IO io = new IO();
!   io.writeln("Pone el jugador " + turn.take());
!   boolean ok;
!   do {
!     target.read("En");
!     ok = board.empty(target);
!     if (!ok) {
!        new IO().writeln("Esa casilla no está vacía");
!     }
!   } while (!ok);
!   board.put(target, turn.take());
!   board.write();
!   if (board.existTicTacToe(turn.take())) {
!     io.writeln("Victoria!!!! " + turn.take() + "! " + turn.take()
!         + "! " + turn.take() + "! Victoria!!!!");
!   } else {
!     turn.change();
!   }
  }
}

```

```java
public class MoveController {

  private Turn turn; // cambia/sabe de quien es el turno
  private Board board; // administra el board
  private Coordinate origin; // la ficha que selecciona el usuario para moverla
  private Coordinate target; // la ficha a donde movera la fiche seleccionada

  public MoveController(Turn turn, Board board) {
    assert turn != null;
    assert board != null;
    this.turn = turn;
    this.board = board;
    origin = new Coordinate();
    target = new Coordinate();
  }

  public void move() {
    IO io = new IO();
    io.writeln("Mueve el jugador " + turn.take());
    origin = new Coordinate();
    boolean ok;
    do {
      origin.read("De");
      ok = board.full(origin, turn.take());
      if (!ok) {
        new IO().writeln("Esa casilla no está ocupada por ninguna de tus fichas");
      }
    } while (!ok);
    board.remove(origin, turn.take());
//Codigo repetido 
!   do {
!     target.read("A");
!     ok = board.empty(target);
!     if (!ok) {
!       new IO().writeln("Esa casilla no está vacía");
// se diferencia en que antes de pones checa si ahi antes estaba la ficha
      } else {
        ok = !origin.equals(target);
        if (!ok) {
          new IO().writeln("No se puede poner de donde se quitó");
        }
      }
!   } while (!ok);
!   board.put(target, turn.take());
!   board.write();
!   if (board.existTicTacToe(turn.take())) {
!     io.writeln("Victoria!!!! " + turn.take() + "! " + turn.take()
!         + "! " + turn.take() + "! Victoria!!!!");
!   } else {
!     turn.change();
!   }
! }
}
```

No debemos de usar Composición (crear una pieza que complete en el constructor)
en parte por que no tiene sentido como se llamaría esta, y dos hace mas
sentido que herede.

```java
public abstract class ColocateController {
  private Turn turn;
  private Board board;
  private Coordinate target;

  protected ColocateController(Turn turn, Board board) {
    assert turn != null;
    assert board != null;
    this.turn = turn;
    this.board = board;
    target = new Coordinate();
  }

// esta sera la accion de mover o poner
  public abstract void control();

  protected void put(String actionTitle, String targetTitle) {
// codigo que reutilizaran
!   IO io = new IO();
!   io.writeln(actionTitle + " el jugador " + turn.take());
// a qui la diferencia al poner y mover, es que el mover debe seleccionar
// una ficha valida para mover, por lo que aqui vamos a implementar esa
// diferencia
!   this.prePut();
!   boolean ok;
!   do {
!     target.read(targetTitle);
!     ok = this.errorToPut();
!   } while (!ok);
!   board.put(target, turn.take());
!   board.write();
!   if (board.existTicTacToe(turn.take())) {
!     io.writeln("Victoria!!!! " + turn.take() + "! " + turn.take() + "! " + turn.take() + "! Victoria!!!!");
!   } else {
!     turn.change();
!   }
  }

// esta sera la implementacion distinta que tienen antes de poner
  protected abstract void prePut();

  protected boolean errorToPut() {
    boolean ok = this.getBoard().empty(this.getTarget());
    if (!ok) { new IO().writeln("Esa casilla no está vacía"); }
    return ok;
  }

  protected Turn getTurn() { return turn; }
  protected Board getBoard() { return board; }
  protected Coordinate getTarget(){ return target; }
}
```

Ahora tenememos el codigo repetido eliminado

```java
public class MoveController extends ColocateController {

// el target esta definido en Padre, esta aqui porque mover no lo necesita
  private Coordinate origin;

  public MoveController(Turn turn, Board board) {
    super(turn, board);
  }

  @Override
  public void control() {
// hacemos la accion de poner el cual se diferencian en el "prePut" y
// el mensaje que le mostramos al usuario
    this.put("Mueve", "A");
  }

  @Override
  protected void prePut() {
    this.remove();
  }

//logica del seleccionar ficha a mover que no tiene el poner
  private void remove(){
    origin = new Coordinate();
    boolean ok;
    do {
      origin.read("De");
      ok = this.errorToMove();
    } while (!ok);
    this.getBoard().remove(origin, this.getTurn().take());
  }

  private boolean errorToMove(){
    boolean ok = this.getBoard().full(origin, this.getTurn().take());
    if (!ok) {
      new IO().writeln("Esa casilla no está ocupada por ninguna de tus fichas");
    }
    return ok;
  }

  @Override
  protected boolean errorToPut() {
    boolean ok = super.errorToPut();
    if (ok){
      ok = !origin.equals(this.getTarget());
      if (!ok) {
        new IO().writeln("No se puede poner de donde se quitó");
      }
    }
    return ok;
  }
}
// -----------------------------
public class PutController extends ColocateController {

  public PutController(Turn turn, Board board) { super(turn, board); }
  
  @Override
  public void control(){ this.put("Pone", "En"); }

// no tiene precondiciones el de poner asi que lo dejamos vacio, (lo cual huele mal)
  @Override
  protected void prePut() { }
}
```
