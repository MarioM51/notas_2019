# Principios SOLID

- [Principios SOLID](#principios-solid)
  - [Introduccion](#introduccion)
    - [002 Acoplamiento](#002-acoplamiento)
    - [003 Cohesion](#003-cohesion)
  - [02 Principio de Responsabilidad Unica - SRP](#02-principio-de-responsabilidad-unica---srp)
    - [Ejemplo violacion de SRP](#ejemplo-violacion-de-srp)
    - [Ejemplo refactorizacion de SRP](#ejemplo-refactorizacion-de-srp)
  - [03 Principio Abierto Cerrado - OCP](#03-principio-abierto-cerrado---ocp)
    - [Ejemplo codigo que incumple el OCP](#ejemplo-codigo-que-incumple-el-ocp)
    - [Corriguiendo para que cumpla OCP](#corriguiendo-para-que-cumpla-ocp)
  - [04 Principio de Substitucion de Liskov - LSP](#04-principio-de-substitucion-de-liskov---lsp)
    - [Ejemplo que incumple LSP](#ejemplo-que-incumple-lsp)
    - [Por que nuestro codigo imcumple LSP](#por-que-nuestro-codigo-imcumple-lsp)
    - [Solucionar, para cumplir con LSP](#solucionar-para-cumplir-con-lsp)
  - [05 Principio de Segregacion de Interfaces - ISP](#05-principio-de-segregacion-de-interfaces---isp)
    - [Ejemplo que imncumple ISP](#ejemplo-que-imncumple-isp)
    - [Por que no se cumple ISP](#por-que-no-se-cumple-isp)
    - [Solucionando violacion ISP](#solucionando-violacion-isp)
  - [06 Principio de Inversion de Dependencias - DIP](#06-principio-de-inversion-de-dependencias---dip)
    - [Ejemplo que incomple DIP](#ejemplo-que-incomple-dip)
    - [Por que no se cumple DIP](#por-que-no-se-cumple-dip)
    - [Solucionando violacion DIP](#solucionando-violacion-dip)

## Introduccion

SOLID es un acronimo para el siguiente conjunto de principios bacicos,
fueron propuestos por **Robert C. Martin** los cuales no llevan un
orden pero todos son importates, para hacer un codigo de mayor calidad

- `S` = Single Responsability.
- `O` = Open/Close.
- `L` = Principio de sustitucion de Liscov.
- `I` = Segregacion de Interface.
- `D` = Inyeccion de dependencias.

Ventajas de usar SOLID.

- Un Software mas robusto
- Codigo reutilizable, mantenible
- Añador nueas funcionalidades de una manera agil

----

### 002 Acoplamiento

Es la forma y el nivel de interdependencia entre componentes de
software, entendemos como componentes a clases. funciones etc.
**Por ejemplo** Si usamos directamente una variable de un componente
en otro, esto quiere decir que los dos componetentes estan acoplados

El Acoplamiento es como el inverso de Cohecion, donde si ahi poco
acoplamineto significa que ahi una alta cohecion.

----

### 003 Cohesion

Es el grado que los elementos de un modulo permanecen juntos, como
explicamos arriba, buscamos tener una alta cohecion para tener una bajo
acoplamiento

----

----

----

## 02 Principio de Responsabilidad Unica - SRP

> Una clase solo debe tener una Y SOLO UNA razon para cambiar, osea
> una clase o funcion solo debe hacer una unica cosa.

----

### Ejemplo violacion de SRP

**Requerimiento:** Queremos que juego con las siguietes caracteristicas

- El juego tiene los siguientes datos
  - int id; String name; int win; int lose; String date;

- Podemos hacer lo siguiente
  - obtener el porcentaje de partidas perdidas
  - obtener el porcentaje de partidas ganadas
  - Guardar la partida
  - Eliminar la partida
  - mostrar en escritorio
  - mostrar en celular

Sin respetar los principios solid tendriamos todo en una sola clase,
como por ejemplo la siguiente.

```js
clase Juego
  int id;
  String name;
  int win;
  int lose;
  String date;

  function obtenerPorcentageGanadas...

  function obtenerPorcentagePerdidas...

  function guardarPartidda

  function eliminarPartida

  function mostrarEscritorio

  function MostrarTelephono
```

Porque esta mal.

Aqui solo hemos declarado pero no implementado nada, y si implementamos
podremos tener una gran cantidad de codigo, y llegar al punto de que
nadie le entienda este codigo.

Tenemos n razones para cambiar el codigo por ejemplo

1. modificar la cantidad de datos del juego
2. modificar la logica como el de obtener porcentajes y otros
3. modificar la capa de datos/persistencia, por ejemplo, camiar de BD
4. modificar algo de la vista

Por cualquier razon cambia nuestra clase, y la regla que mencionamos es
que el elemento debe de cambiar solo por una razon,

----

### Ejemplo refactorizacion de SRP

Tomando el ejemplo anterior, para refactorizar vamos a crear una clase
por cada razon que pusimos, nos quedaria la sig estructura, cada una
encargada de una tarea en especifico y por ende solo cambia por un
motivo en especifico.

```r
juego
  JuegoEntity.code
  JuegoService.code
  JuegoRepository.code
  JuegoView.code
```

Clase encargada de contener los datos

```js
class JuegoEntity
    int id;
    String name;
    int win;
    int lose;
    String date;
```

Clase encargada de la logica de estadisticas del juego

```js
class JuegoService {
  function obtenerPorcentageGanadas {...}
  function obtenerPorcentagePerdidas {...}
```

Clase encargada de manejar la persistencia

```js
class JuegoRepository {
  function obtenerPorcentageGanadas {...}
  function obtenerPorcentagePerdidas {...}
```

Clase encargada de la vista

```js
class JuegoView {
  function mostrarEscritorio {...}
  function MostrarTelephono {..}
```

----

----

----

## 03 Principio Abierto Cerrado - OCP

> Las entidades de software (clases, modulos, funciones), deberian ser
> abiertas para su extencion pero cerradas a la modificacion.

Entendamos extencion como agregar nuevas clases/archivos para agregar
el comportamiento, y a cerrado, como que estamos usando codigo con algo
en su mayoria interfaces y clases abstractas, para evitar agregar codigo
en una clase, en lugar de eso agregar una nueva.

Esto quiere decir que cuando querramos agregar una nueva funcionalidad
no deberiamos de modificar el codgio que ya existe, mas bien deberia
fomentar agregar codigo a la hora de agregar nueva funcionalidad.

Una de las estrategias es la creacion y uso de las **interfaces y**
**clases abstactas**, las cuales nos ayudan desde el compilador a no
modificar lo que se esta usando de dicha clase.

----

### Ejemplo codigo que incumple el OCP

A nuestro ejemplo anterior quieren que le agregemos la caracteristica
de descuentos, en el que tenemos los descuentos de A) apertura,
B) Usuario premium. C) BlackFriday, D) Navidad.

**Importante:** Imaginar que cada vez que agregamos un nuevo descuento,
pasa tiempo, o el cambio es realizado por otra persona.

lo que hariamos con una mala practica seria.

```java
class TiendaService {
  int aplicarDescuento() {
    precioConDescuento = DescuentoManager.descontar(precio, DescuentoApertura);
  }
}
```

A. Creamos clase con para aplicar el descuento

```java
class DescuentoApertura {
  public int aplicar(int precio) {
    return precio*0.5;
  }
}
```

B. Crear una clase que los gestione

```java
class DescuentoManager {
  public int descontar(int precio, DescuentoApertura descuento) {
    return descuento.aplicar();
  }
}
```

Y tenemos los siguientes ficheres de inicio

```r
descuentos/
  descuentos/
    DescuentoApertura.code
  DescuentoManager.code
TiendaService.code
```

Ya tenemos aplicado el descuento de apertura, ahora despues de un
tiempo y por otro programador se va aplicar, el descuento para
usuarios premium.

C. Agregamos clase para aplicar descuento a usuarios premium

```java
class DescuentoUsrPremium {
  public int aplicar(int precio) {
    return precio*0.3;
  }
}
```

D. Modificamos el Manager para aplicar el nuevo descuento. EL cual es
su implementacion puede ir desde solo cambiar el porcentaje, calcular
la lealtad del usuario premium o etc.

```java
class DescuentoManager {
  public int descontar(int precio, DescuentoApertura descuento) {
    return descuento.aplicar();
  }

+ public int descontar(int precio, DescuentoUsrPremium descuento) {
+   return descuento.aplicar();
+ }
}
```

E. Agregamos el cambio en el service

```java
class TiendaService {
  int aplicarDescuento() {
    switch (tipoDescuento) {
      case "Apertura":
        precioConDescuento = DescuentoManager.descontar(precio, DescuentoApertura);
      break;
     case "Usuario_Premium"
+       precioConDescuento = DescuentoManager.descontar(precio, DescuentoUsrPremium);
      break;
    }
  }
}
```

Como vemos modificamos a `DescuentoManager`, agregamos uno nueva clase.

```r
# "A": nuevo archivo/ca  "=": no se toco, "M"=se modifico
=  folder/
=      descuentos/
=          DescuentoApertura.code
A          DescuentoUsrPremium.code
M      DescuentoManager.code
M  TiendaService.code
```

**AQUI ESTA EL ERROR:** Al modificar `DescuentoManager` esta clase ya
no cumple con el principio de abierto/cerrado. ***LO QUE PROBOCARIA**
que la clase `DescuentoManager` que ya sabemos que funciona bien y
tenemos bien testeada, arriesgarnos a meter un bug.

**IMPORTANTE:** El principio de abierto cerrado no se refiere a no
midificar codigo y en lugar de eso copiar/pegar y modificar, lo cual
estariemos agregando codigo sin modificar el ya existente.

Y seguiriamos repitiendo el mismo error, por ejemplo al agregar un
nuevo tipo de descuento.

E. Agregamos clase para aplicar descuento a BlackFriday, lo mismo puede
que apliquemos el cambio meses despues o que otra persona lo aplique.

```java
class DescuentoUsrBlackFriday {
  public int aplicar(int precio) {
    return precio*0.6;
  }
}
```

D. Lo Agregamos en `DescuentoManager` comentiendo el mismo error, ya
que estamos metiendo nuevo codigo y modificando la clase la cual nos
da posibles riesgos.

```java
class DescuentoManager {
  public int descontar(int precio, DescuentoApertura descuento) {
    return descuento.aplicar();
  }

  public int descontar(int precio, DescuentoUsrPremium descuento) {
    return descuento.aplicar();
  }

+  public int descontar(int precio, DescuentoUsrBlackFriday descuento) {
+   return descuento.aplicar();
+ }

}
```

X. Agregamos el cambio en el service

```java
class TiendaService {
  int aplicarDescuento() {
    switch (tipoDescuento) {
      case "Apertura":
        precioConDescuento = DescuentoManager.descontar(precio, DescuentoApertura);
      break;
     case "Usuario_Premium"
        precioConDescuento = DescuentoManager.descontar(precio, DescuentoUsrPremium);
      break;
      case "blackFriday"
+       precioConDescuento = DescuentoManager.descontar(precio, DescuentoUsrBlackFriday);
      break;
    }
  }
}
```

Y los cambios desde fichero serian

```r
# "A": nuevo archivo/ca  "=": no se toco, "M"=se modifico
=  folder/
=      descuentos/
=          DescuentoApertura.code
=          DescuentoUsrPremium.code
A          DescuentoUsrBlackFriday.code
M      DescuentoManager.code
M  TiendaService.code
```

----

### Corriguiendo para que cumpla OCP

Como habiamos mencionado comunmente podemos hacer que se cumpla el
principio, haciendo uso de interfaces, que es lo que vamos a hacer.

Imaginemos que empezamos de cero.

A. Creamos interface las cuales deberan de cumplir las clases de descuento
como es el de `DescuentoUsrPremium``por ejemplo.

```java
public interface IDescuento {
  int aplicar(int precio);
}
```

B. Usamos nuestra interface a Manager

```java
class DescuentoManager {
  public int descontar(int precio, IDescuento descuento) {
    return descuento.aplicar();
  }
}
```

C. Aplicamos nuestra interface en descuento por apertura

```java
class DescuentoApertura implements IDescuento {
  @Override public int aplicar(int precio) {
    return precio*0.5;
  }
}
```

D. Agregamos nuestro Service

```java
class TiendaService {
  int aplicarDescuento() {
    IDescuento tipoDescuento = calcularTipoDescuento();
    precioConDescuento = DescuentoManager.descontar(precio, tipoDescuento);
  }

  private IDescuento calcularTipoDescuento() {
    switch(descuentoAAplicar)
      case "DescuentoApertura":
        return DescuentoApertura;
      break;
  }
}
```

Lo cual tenemos los siguientes archivos de inicio

```r
  folder/
      descuentos/
          DescuentoApertura.code
      DescuentoManager.code
  TiendaService.code
```

**AHORA LA DIFERENCIA** esta en que si agregamos un nuevo cambio o
funcionalidad, lo unico que debemos de hacer es agregar la nueva clase.

```java
class DescuentoUsrPremium implements IDescuento {
  public int aplicar(int precio) {
    return precio*0.3;
  }
}
```

Y ya no tenemos que modificar `DescuentoManager` ya que como podemos
ver estamos usando la interface puede entrar cualquiera de las clases
de descuento.

```java
// NO HACMEOS NUNGUN CAMBIO
class DescuentoManager {
  public int descontar(int precio, IDescuento descuento) {
    return descuento.aplicar();
  }
}
```

X. Agregamos Cambio en nuestro service

```java
class TiendaService {
  int aplicarDescuento() {
    IDescuento tipoDescuento = calcularTipoDescuento();
    precioConDescuento = DescuentoManager.descontar(precio, tipoDescuento);
  }

  private IDescuento calcularTipoDescuento() {
    switch(descuentoAAplicar)
      case "DescuentoApertura":
        return DescuentoApertura;
      break;
      case "DescuentoApertura":
        return DescuentoUsrPremium;
      break;
  }
}
```

Los cambios en archivo serian los siguientes, y podemos ver que ya solo
agregamos una clase y no modificamos ninguna otra.

```r
  folder/
      descuentos/
          DescuentoApertura.code
A         DescuentoUsrPremium.code
      DescuentoManager.code
M TiendaService.code
```

**Resultado:** Tenemos un codigo mas leguible y reutilizable, en el cual
ya solo agregamos la nueva clase con la funcionalidad, y el codigo
especificamente para disponer esa nueva funcionalidad, y estamos
reutilizando codigo, el cual al testear este codigo que utilizamos en
varias partes los test tienen una mayor covertura.

----

----

----

## 04 Principio de Substitucion de Liskov - LSP

> Cualquier clase que sea hija de una clase padre deberia poder usarse
> en lugar de su padre sin nungun compartamiento inesperado.

Quiere decir que al agregar una clase hija nunca deberiamos de alterar
la clase padre.

En otras palabras, si sustituimos el comportamiento de la clase hija
con el comportamiento de la clase padre esta no tendremos un
comportamiento inesperado.

----

### Ejemplo que incumple LSP

Vamos a crear una clase abstracta que reprecenta un juego de tenis,
como es su alto, ancho, pais, fecha de juego, etc.

Nos piden que agregemos un metodo para cortar el cesped, pero para cada
cancha funcionara diferente, por ejemplo, estan en diferentes climas lo
que hace que cresca el cesped a diferente velocidad y espesor.

```java
abstract class AChanchaDeTenis {
  int largo;
  int ancho;
  int pasto;
  void cortarCesped() {
    this.pasto--;
  }
  //getters/settes
}
```

X. Ahora vamos a crear una implelemtacion de una pista de tenis de Mexico

```java
class ChanchaDeTenisMexico extends AChanchaDeTenis {
  @Override void cortarCesped() {
    print("Cortando pasto a la velocidad de Mexico");
  }
}
```

X. Agregamos una nueva pista, pero esta tiene la particularidad de que
no tiene cesped, por ejemplo esta en la playa, entonces el metodo de
cortar cesped nos sobre. Por lo que podemos hacer es mandar una exepcion.

```java
class ChanchaDeTenisColombia extends AChanchaDeTenis {
  @Override void cortarCesped() {
    print("No usar este metodo, ya que esta no tiene cesped");
  }
}
```

X. Agregamos una nueva cancha de tenis de Brasil, y da el caso que esta
chancha es de piso duro, por lo que tambien no, nos vale el metodo de
de cortar cesped

```java
class ChanchaDeTenisBrasil extends AChanchaDeTenis {
  @Override void cortarCesped() {
    print("No usar este metodo, tiene piso duro");
  }
}
```

### Por que nuestro codigo imcumple LSP

La primera es que estamos implementando un metodo que no necesitamos y
dos es como su principio lo dice devemos de poder hacer uso del metodo
de la clase padre y funcionar sin problemas.

Por ejemplo, si agregamos un metodo para que que use los metodos de
cada una de las clases.

Por ejemplo.

```java
void testPistas() {
  List canchas = new List<AChanchaDeTenis>();
  canchas.add(new ChanchaDeTenisMexico());
  canchas.add(new ChanchaDeTenisColombia());
  canchas.add(new ChanchaDeTenisBrasil());
  for(AChanchaDeTenis chancha: canchas) {
    chancha.cortarCesped();
  }
}
```

El codigo hara cortar el cesped solo con mexico, ya que las canchas de
Colombia y Brasil no tienen cesped, por lo que esto indica que nuestro
**el diseño de OOP esta mal hecho** por una parte.

Y por otra parte cuando quitamos el metodo a una de estas clases, por
ejemplo.

```java
class ChanchaDeTenisBrasil extends AChanchaDeTenis { }
```

Lo que pasara es que el metodo que se usara en `testPistas` sera el de
la clase padre, y esta si corta el cesped, por lo que a esto se le
concidera un **comportamiento inesperado**, ya que sustituimos el
metodo de la clase hija por el de la clase padre y este metodo estaria
cortando un cesped que no existe.

### Solucionar, para cumplir con LSP

La solucion seria

A. Quitar el metodo y su variable que depende el metodo del padre

```java
abstract class AChanchaDeTenis {
    int largo;
    int ancho;
//  int pasto;
//  void cortarCesped() {
//      this.pasto--;
//  }
    //getters/settes...
}
```

B. Crear una interface por cada tipo de piso que tiene cada cancha.

B.1. Interface para cancas de pasto

```java
interface ChanchaPasto { void cortarPasto(); }
```

B.2. Interce para canchas de arena

```java
interface ChanchaArena { void aplastarArena(); }
```

B.3. Interface para canchas de Piso duro

```java
interface ChanchaPisoDuro { void barrerPiso(); }
```

C. Ahora en cada implementacion de cancha, implementamos la interface
de cada una de estas.

C.1. Pista de pasto, la cual no cambia mucho ya que esta no violaba LSP

```java
class ChanchaDeTenisMexico extends AChanchaDeTenis implements ChanchaPasto {
  @Override void cortarCesped() {
    print("Cortando pasto a la velocidad de Mexico");
  }
}
```

C.2. Pista de Arena, esta le eliminamos el metodo de cortarCesped ya
que no la aplica el padre ni tiene interface que la obligue, solo
tenemos a `aplastarArena` que tenemos que implementar.

```java
class ChanchaDeTenisColombia extends AChanchaDeTenis implements ChanchaArena {
//  @Override void cortarCesped() {
//      print("No usar este metodo, ya que esta no tiene cesped");
//  }
    @Override void aplastarArena() {
      print("Aplastando arena");
    }
}
```

C.3. Para la cancha de bracil que tiene piso duro

```java
interface  {  }

class ChanchaDeTenisBrasil extends AChanchaDeTenis implements ChanchaPisoDuro {
//  @Override void cortarCesped() { print("No usar este metodo, tiene piso duro"); }
    @Override void barrerPiso() {
      print("Barriendo piso duro");
    }
}
```

E. Agregamos una camcha que tenga pasto, por ejemplo.

```java
class ChanchaDeTenisArgentina extends AChanchaDeTenis {
  @Override void cortarCesped() {
    print("Cortando pasto a la velocidad de Argentina");
  }
}
```

F. Ahora podemos en `testPistas` primero veremos que no podremos usar
`ChanchaDeTenisColombia` y `ChanchaDeTenisBrasil` por que ya no tienen
el metodo de cortar pasto, por lo que ya no estamos implementando un
comportamiento en otras clases que no le concierne, y ya podemos
**evitar comportamiento inesperado** ya que no podemos caer en el
escenario de, cortar el pasto en una cancha donde no existe pasto.

```java
void testPistas() {
#   List canchasPasto = new List<ChanchaPasto>();
    canchasPasto.add(new ChanchaDeTenisMexico());
+   canchasPasto.add(new ChanchaDeTenisArgentina());
//  canchasPasto.add(new ChanchaDeTenisColombia()); //ERROR COMPILACION
//  canchasPasto.add(new ChanchaDeTenisBrasil());   //ERROR COMPILACION
    for(AChanchaDeTenis chancha: canchasPasto) {
        chancha.cortarCesped();
    }
}
```

y como podmos ver si podemos ejecutar el codigo, **sin problemas de diseño**,
ya que incluso queriendo intentarlo el mismo compilador no nos dejaria.

----

----

----

## 05 Principio de Segregacion de Interfaces - ISP

> Ninguna clase debe implementar metodos que no utilize

Muchas veces es mejor usar una varias/muchas interfaces especificas a
usar una interface de uso general.

Las interfaces nos permiten desacoplar los modulos entre si, como
mensionamos esto nos da una baja coehcion.

----

### Ejemplo que imncumple ISP

Nos piden gestionar los pagos de targetas VISA y Amex por lo que hariamos

```java
interface Paymet {
  void calcularCargo();
  void realizarPago();
}
```

Y lo implementamos en Visa

```java
class Visa implements Paymet {
  @Override void calcularCargo() {
    // cargo del 3%
  }
  @Override void realizarPago() {
    // consumo de API para pagar
  }
}
```

Implementamos AMEX

```java
class Amex implements Paymet {
  @Override void calcularCargo() {
    // cargo del 5%
  }
  @Override void realizarPago() {
    // consumo de API para pagar
  }
}
```

Hasta ahora todo bien, pero ahora nos piden que podamos resivir pagos
por transferencia de BMex, por lo que hariamos

```java
class BMexTransfer implements Paymet {
  @Override void calcularCargo() {
    // cargo del 3% normal mas 2% por ser transferencia
  }
  @Override void realizarPago() {
    // AQUI TENEMOS UN PROBLEMA
  }
  @Override void recibirPago() { ... }//lo agregamos abajo
}
```

El PROBLEMA ES QUE SOLO QUERMEOS RECIBIR LOS PAGOS NO REALIZAR UNO,
POR LO QUE ESTE METODO NOS SOBRARIA y dos debemos de agregar un nuevo
metodo a la interface, ya que en donde la usamos solo usamos la interface
y no usamos la clase directamente.

```java
interface Paymet {
    void calcularCargo();
    void realizarPago();
+   void recibirPago();
}
```

Y esto empeora las cosas, por que estamos obligados a implementar el
pago en `Visa` y `Amex` y van a quedar vacios

Lo mismo pasaria si quisieramos agregar un pago en sucursales.

----

### Por que no se cumple ISP

Como el principio lo deja claro es que "Ninguna clase debe implementar
metodos que no utilize" y evidentemente tenemos clases que implementan
metodos que no usan.

----

### Solucionando violacion ISP

Lo que devemos hacer es hacer interfaces especificas para cada pago

```java
interface ICalcularCargo {
    void calcularCargo();
}

interface IRecepcionPagos {
    void recibirPago();
}

interface IEnvioPagos {
    void realizarPago();
}
```

Y ahora vamos a implementar, las interfaces en especifico.

```java
# class Visa implements ICalcularCargo, IEnvioPagos {
      @Override void calcularCargo() {
        // cargo del 3%
      }
      @Override void realizarPago() {
        // consumo de API para pagar
      }
  }


# class Amex implements ICalcularCargo, IEnvioPagos {
      @Override void calcularCargo() {
        // cargo del 5%
      }
      @Override void realizarPago() {
        // consumo de API para pagar
      }
  }


# class BMexTransfer implements ICalcularCargo, IRecepcionPagos  {
      @Override void calcularCargo() {
        // cargo del 3% normal mas 2% por ser transferencia
      }
//    @Override void realizarPago() { } LO ELIMINAMOS YA QUE NO ES NECERARIO
      @Override void recibirPago() {
        // consumo de API para recibir el pago
      }
  }
```

Como podemos ver solucionamos el problema siendo mas especificos

----

----

----

## 06 Principio de Inversion de Dependencias - DIP

> A. Las clases de alto nivel no de deben de depender de las clases de
> bajo nivel
>
> B. Las abstracciones no deben de depender de los detalles. mas bien
> los detalles deben depender de las abstracciones.

Esto nos permitira cosas como que la aplicacion no dependera del motor
de bases de datos, por ejemplo, si se requiere cambiar de DBM solo
tendremos que hacer cambios minimos.

----

### Ejemplo que incomple DIP

Nos piden

- manejar la informacion del usuario.
- Su acceso a la BD
- Mostrar en pantalla

lo que hariamos.

Para el manejo de datos de usuario

```java
class Usuario {
  private int id;
  private int nombre;
  private int email;
  public Usuario(id, nombre, email) { }
  //getters/setters
}
```

Para manejo de la DB con usuario

```java
class UsuarioRepository { // CLASE DE BAJO NIVEL
  public User getUser(int id) {
    // logica para traer el usuario
  }
}
```

Para mostrar en pantalla, lo cual vamos a necetirar del Repositorio para
acceder a los datos

```java
class UserUI { // CLASE DE ALTO NIVEL
  public void mostrarUsuario(id) {
    var repo = new UsuarioRepository();
    Usuario usuario = repo.getUser(id);
    mostrar(usuario);
  }
}
```

----

### Por que no se cumple DIP

Recordando el primer punto del principio dice:

- *que las clases de alto nivel no deben de depender de las de bajo
  nivel*

Y si vemos nuestro diseño vemos que `UserUI` una clase de de alto nivel
depende de `UsuarioRepository` que es una clase de bajo nivel.

![dependencia_alto_nivel_con_bajo_nivel](./img/01_dependencia_alto_nivel_con_bajo_nivel.PNG)

El problema aqui es que `UsuarioRepository` reprecenta la implementacion
especifica de acceso a base de datos, por ejemplo, si queremos cambiar
esto por que cambiamos de DBM, **debemos de meter mano a UserUI** para
realizar el cambio.

Lo ideal seria que al hacer un cambio en `UsuarioRepository` a `UserUI`
no se deberia de ver afectado.

este escenario es un ejemplo de codigo altamente acoplado.

----

### Solucionando violacion DIP

La solucion es crear una abstraccion (una interface) de la cual vamos
con la cual **vamos a intervenir la dependencia**, es decir, que cuando
querramos meter una nueva implementacion.

Por ejemplo, el acceso de un DBM diferente como lo es MongoDB,
tendremos que implementar la interface para poder usarla en `UserUI`.

A. Creamos la interface

```java
interface IUsuarioRepository {
  public User getUser(int id);
}
```

B. Implementamos la interface en nuetro repository actual, y
aprovechamos para cambiarle de nombre, y asi especificar.

```java
class UsuarioRepositoryMysql implements IUsuarioRepository { // CLASE DE BAJO NIVEL
  @Override public User getUser(int id) {
    // logica para traer el usuario
  }
}
```

C. Combiamos `UserUI` para que no use la implementacion, si no que
use la interface.

C.1. Para poder instancia la interface en `UserUI` vamos a crear un
Factory para obtener la implementacion del Repository.

```java
  class UserRepositoryFactory { // CLASE DE ALTO NIVEL
      public static IUsuarioRepository create() {
        DBM dbm = obtenerDBMActual();
        switch(motorDBM) {
          case dbm.MySql
            return new UsuarioRepositoryMysql();
          break;
        }
      }
  }
```

Ahora usamos el `IUsuarioRepository` en `UserUI` y para no meter logica
de como obtener cual implementacion obtener usaremos el Factory.

```java
  class UserUI { // CLASE DE ALTO NIVEL
      public void mostrarUsuario(id) {
#       IUsuarioRepository iRepo = UserRepositoryFactory.create();
#       Usuario usuario = iRepo.getUser(id);
        mostrar(usuario);
      }
  }
```

Y como vemos `UserIU` ya no depende de `UsuarioRepositoryMysql` si no
que depende de `IUsuarioRepository` por lo que ya estamos cumpliendo
el primer punto del principio DIP.

![02_solucion_dependencia_alto_nivel_con_bajo_nivel](./img/02_solucion_dependencia_alto_nivel_con_bajo_nivel.PNG)

