# Preguntas Principios SOLID

- [Preguntas Principios SOLID](#preguntas-principios-solid)
  - [introduccion](#introduccion)
  - [Principio de Responsabilidad Unica](#principio-de-responsabilidad-unica)
  - [Principio Abierto Cerrado](#principio-abierto-cerrado)
  - [Principio de Substitucion de Liskov](#principio-de-substitucion-de-liskov)
  - [Principio de Segregación de Interfaces](#principio-de-segregación-de-interfaces)
  - [Principio de Inversion de Dependencias](#principio-de-inversion-de-dependencias)
  - [IoC](#ioc)


## introduccion

Que significa el acronimo SOLID

- [ ] `S`: Single Responsability.
- [ ] `O`: Open/Close.
- [ ] `L`: Principio de sustitucion de Liscov.
- [ ] `I`: Segregacion de Interface.
- [ ] `D`: Inyeccion de dependencias.

----

## Principio de Responsabilidad Unica

En que consiste Principio de Responsabilidad Unica

- [ ] Una clase solo debe tener una Y SOLO UNA razon para cambiar,
      osea una clase o funcion solo debe hacer una unica cosa.

----

## Principio Abierto Cerrado

En que consiste el Principio Abierto Cerrado

- [ ] Las entidades de software (clases, modulos, funciones), deberian
      ser abiertas para su extencion pero cerradas a la modificacion.

----

Es un ejemplo de violacion y solucion de Abierto Cerrado

- [ ] Usamos un metodo el cual le entra una implementacion y se aplica
      dentro de este metodo, y por cada nueva implementacion que creamos
      agregamos un nuevo metodo, y para SOLUCIONARLO creamos un metodo
      donde entra una interface, y por cada implementacion que creamos
      le aplicamos dicha interface, permitiendo poder reutilizar el
      metodo para cada una de las implementaciones.

----

## Principio de Substitucion de Liskov

```r
principio de substitución Liskov: ¿En que consiste?, ¿Como identificar violación?, ¿Analogías?, ¿Ejemplo?;'
¿En que consiste?
--Poder sustituir una clase padre por una clase hija y no tener errores/comportamientos inesperados.

¿Como identificar violación?
--Con el test de "es un"="is a"

¿Analogías?
--Un cuadro es un rectángulo, pero un rectángulo no es un cuadro
--Si sustituimos una ave por un pato, a la hora de decirle que vuele pum
'

Es un ejemplo de violacion y solucion de Substitucion de Liskov;"
Crear una serie de implementaciones que apliquen una interface, y que cuando utilizamos estas desde la interface en una clase de alto nivel, una de las implementaciones causa problemas, ya sea por que no implementa ningun codigo, o por que utiliza una implementacion por defecto (por ejemplo en una clase abstracta), y este causa algo indeceado o inecesario, para SOLUCIONARLO, seria con las creacion de interfaces o clases abstractas mas pequeñas u definidas.
"
```

----

## Principio de Segregación de Interfaces

```r
En que consiste el Principio de Segregación de Interfaces;"
Ninguna clase debe implementar métodos que no utilize y para solucionarlo seria crear interfaces de una sola responsabilidad.

Preferible tener varias interfaces pequeñas que una grande.
"
```

----

## Principio de Inversion de Dependencias

```r
Principio de Inversion de Dependencias(DIP): ¿En que consiste?, ¿Analogía?;"
¿En que consiste?
--Las clases de alto nivel no de deben de depender de las clases de bajo nivel.
--Las abstracciones no deben de depender de los detalles
--Los detalles deben depender de las abstracciones.
--Es por esto que es util (ISP)

Analogía
--¿Soldarías un foco al socket? o ¿Soldarías un clavija al conector?
----No, Debemos de tener la posibilidad de conectar diferentes tipos de focos al socket, o diferentes tipos de electrónicos al contacto
"

Dependency Injection vs Dependency Inversion Principle vs IoC;"
--(DIP)Dependency Inversion Principle is a general design guideline which recommends that classes should only have direct relationships with high-level abstractions, it's like the theory to can write code with DI, that can be target with several implementation techniques.

--(DI)Dependency Injection is an implementation technique for populating instance variables of a class, you get base in this principle to archive DIP

--(IoC)Injection of Control: It's like the engine that do DI for you, an engine example is the Spring IoC project.
"

Inversion de Dependencias, ¿Analogía?;"
Si tomamos algo directamente del refrigerador podemos generar los siguientes problemas, tomar algo que no no nos pertenece, caducado, algo que se ocupara, etc. por lo que modificamos las estrategia, ha indicar tus necesidades, por ejemplo, indicas que necesitas algo de tomar, entonces alguien se encarga de que exista y de dártelo.
"

Es un ejemplo de violacion y solucion de Inversion de Dependencias;"
Crear un repositorio (clase de bajo nivel) y un controlador (alto nivel) y que creemos y usemos el repositorio en el controlador, provocando un codigo altamente acoplado, para SOLUCIONARLO, seria crear una interface que aplique repositorio, y usar la interface el en el controlador y para obtener la implementación del repositorio por medio de un factory.
"
```

## IoC

```r
IoC: ¿Para que sirve?, ¿Caso de uso?;"
IoC
--Para desacoplar las dependencias, y que que estas sean definidas en tiempo de ejecución.

Caso de uso
--La conexión a bd, de tal manera que podamos cambien entre Oracle, H2, PostgreSQL, etc. sin problemas.
"
```
