# Git control de versiones

- [Git control de versiones](#git-control-de-versiones)
  - [Puntos importantes](#puntos-importantes)
  - [Ejemplos](#ejemplos)
    - [Deshacerse de commit sin eliminar cambios](#deshacerse-de-commit-sin-eliminar-cambios)
    - [Deshacerse de commit ELIMINANDO cambios](#deshacerse-de-commit-eliminando-cambios)
    - [Retroceder de Commit sin eliminar el commit](#retroceder-de-commit-sin-eliminar-el-commit)
    - [Crear rama sin traer los logs o y meter cambios sin traer logs](#crear-rama-sin-traer-los-logs-o-y-meter-cambios-sin-traer-logs)
    - [Eliminar rama](#eliminar-rama)
    - [Renombrar rama](#renombrar-rama)
    - [Usar otro puerto](#usar-otro-puerto)
    - [Agregar uso de llave personalizada para repositorio](#agregar-uso-de-llave-personalizada-para-repositorio)
  - [Clone](#clone)
  - [Config](#config)
    - [Ver user y email](#ver-user-y-email)
  - [Merge y comparaciones](#merge-y-comparaciones)
    - [Comparar dos archivos diferentes](#comparar-dos-archivos-diferentes)
    - [Comparar ramas](#comparar-ramas)
    - [Como hacer un preview de un merge usando vsCode](#como-hacer-un-preview-de-un-merge-usando-vscode)
  - [Errores](#errores)
    - [invalid credentials, Authentication failed for](#invalid-credentials-authentication-failed-for)
    - [Permission denied (publickey,keyboard-interactive)](#permission-denied-publickeykeyboard-interactive)
    - [Error The certificate chain was issued by an authority that is not trusted](#error-the-certificate-chain-was-issued-by-an-authority-that-is-not-trusted)
    - [Could not open a connection to your authentication agent](#could-not-open-a-connection-to-your-authentication-agent)
    - [Al querer descargar rama se hace merge](#al-querer-descargar-rama-se-hace-merge)
  - [Tips](#tips)
  - [Evitar crear ramas de una determinada rama](#evitar-crear-ramas-de-una-determinada-rama)

## Puntos importantes

- Cuando empezemos una nueva caracteristica esta la hacemos en una nueva rama
- Antes de integrar con una rama compartida debemos de hacerr pull,
  para resolver cualquier problemas de cambios que ocurrieron fuera
  de nuestra maquina

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Ejemplos

### Deshacerse de commit sin eliminar cambios

Eliminar ultimo commit de remote (lo consevamos en local): `git push origin +HEAD^:`

Eliminar commit de local y subir rama actual para que se elimine
el ultimo commit del remote (eliminar commit de local y de remote)

```r
# antes, las commillas ("") reprecentan los cambios sin que les ahigamos hecho commit
A-B-"C"
     ↑
   branch

git reset HEAD^ # remove commit locally pero conserva cambios sin agregar a git
git push origin +HEAD # force-push the new HEAD commit

# despues
A-B-C
    ↑
  branch
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Deshacerse de commit ELIMINANDO cambios

Eliminara el ultimo commit con todo y ficheros, asi que cuidado ya que
no podremos recuperar los cambios que hicimos en el commit.

```r
# antes
A-B-C
    ↑
  branch

git reset --hard HEAD~1

# despues
A-B
  ↑
branch
```

Quitar cambio del repo remoto, para esto ya debemos de tener el commit
eliminado en nuestro local, y despues corremos este comando: `git push origin +HEAD`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Retroceder de Commit sin eliminar el commit

Podemos movernos hacia atras de commit sin eliminarlo, y en algun
punto regresar al commit donde estabamos

```r
# antes
A-B-C
    ↑
  branch

git reset HEAD~1

# despues
A-B-C
  ↑
branch
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Crear rama sin traer los logs o y meter cambios sin traer logs

Fuente: [Link](https://stackoverflow.com/questions/20790014/git-copy-source-code-to-new-branch-without-history)

**Objetivo:** Vamos a tener una rama que tiene muchos commit y pasar
a otra rama que queremos que tenga pocos commits

1, Crear rama sin hitorial

```r
git checkout --orphan <rama_ruidosa> # crear rama sin historial
git add .                            # Agregamos commit para que se conserve
git commit -m "Initial"              # commit inicial
```

2, Agregar cambios a la rama sin meter su historial

```r
git checkout <rama_ruidosa>   # nos cambiamos de rama
git commit -m "feature1"
git commit -m "feature2"      # Agregamos cambios
```

3, Meter cambios sin traer los logs

```r
git checkout <rama_sin_ruido>      # Nos cambiamos a la rama sin ruido
git merge --squash <rama_ruidosa>  # traer cambios sin logs
git commit -m "release1"           # agregar commit
```

**Resultado:** Traimos varios-muchos commits de una rama a otra rama
como si fuera solo un commit

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Eliminar rama

remover rama

```r
git branch -d <BRANCH-NAME>
```

eliminar rama remota

**ERROR:** Gitea: branch dev is the default branch and cannot be deleted
! [remote rejected] dev (pre-receive hook declined)
**SOLUCION:** x

```r
git push <REMOTE_NAME> :<BRANCH-NAME>
```


<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cambiar de nombre a rama, rama cambiar nombre. rama renombre
-->

### Renombrar rama

funciona como el mover archivo en linux

```r
git branch -m <oldname> <newname>
```

### Usar otro puerto

<!--
puerto personalizado, custom port, usar un puerto diferente, agregar remote con otro puerto
-->

**Importante**: Cuidado con el usuario uno podria confindirlo con el que
uno usa para logearse como administrador, pero en su mayoria para git
se usa el usuario llamado git

```r
git remote add origin ssh://user@host:1234/srv/git/example
```

### Agregar uso de llave personalizada para repositorio

```r
git config --add --local core.sshCommand 'ssh -i ./relative_path'
```

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Clone

A. Clonar solo una rama, clonar solo rama, clonar una rama, clonar rama

```r
git clone --single-branch --branch <branch> <url>
```

B, Clonar usando llave ssh, clonar usando ssh, clonar con llave, clonar con ssh

```r
git clone ssh://USER@HOST:PORT/path/repo --config core.sshCommand="ssh -i /path//private.key"
```

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Config

### Ver user y email

<!-- asignar usuario, asignar email, confgurar usuario y email-->

Para poder realizar un commit en un repositorio remoto, debemos de tener
configurados estos datos, los cuales podemos poner con

```r
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Merge y comparaciones

### Comparar dos archivos diferentes

- Fuente: [Link](https://stackoverflow.com/questions/4099742/how-to-compare-files-from-two-different-branches)

Ahi otras formas pero este me sirvio bien

```r
git diff branch1..branch2 -- myfile.x
```

### Comparar ramas

Comparar rama actual con otra, usando solo la consola

```r
git diff ...branch2
```

Lo mismo pero usando una interface grafica que **medio ayuda**

```r
gitk ...feature_add_enviroment_variable_artt_host
```

### Como hacer un preview de un merge usando vsCode

Este metodo no trae los logs de la otra rama, por lo cual solo lo usamos
para previsualizar que cambios se estan aplicando, luego desaserlos y
ya luego proceder a hacer un merge normal ya sabiendo que se va a aplicar

Debemos estar dentro de la `rama_a` la cual le vamos aplicar los cambios
que estan el la `rama_b`, pero para que no se apliquen los cambios
al comando merge le pasamos la bandera --no-commit

**Importante:** Los argumentos `--no-commit` `--no-ff` siempre deben
ir  al final del comando, ya que me ha pasado que los ignora si los
pongo al principio.

```r
# --no-commit: no hace commit
# --no-commit: no hace Fast-forward que tambien puede aplicar los cambios
# dentro de la rama sin ponerlos en el stage
(rama_a) > git merge rama_b --no-commit --no-ff
```

Los cambios se traeran y podremos ver las diferencias en el `source control`
del panel izquierdo de nuestro vsCode.

Ya que vimos los cambios

```r
(rama_a) > git merge --abort
```

De aqui ya podemos decidir si hacer o no el merge con la rama que supervisamos

```r
(rama_a) > git merge rama_b
```

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Errores

### invalid credentials, Authentication failed for

> **Error:** `remote: invalid credentials from *, fatal: Authentication failed for *`.  
> **Descripcion:** Al querer usar `push` no sube cambios y marca este error.  
> **Fuente:** [Link](https://stackoverflow.com/questions/15381198/remove-credentials-from-git)
> **Causa:** Estamos usando credenciales guardades en la maquina pero
> estas pueden no ser las correctas.  
> **Solucion-explicacion:** Borrar las llaves actuales para que se creen
> otras nuevas con.

```r
1. Go to Credential Manager
2. Go to Windows Credentials
3. Delete the entries under Generic Credentials
4. Try connecting again. This time, it should prompt you for the correct username and password.
```

<!--
tomar credenciales guardadas, no pedir credenciales, no pedir contraseña, recordar contraseña,
recordar contraseña
 -->

Nota: para indicar a git que tome las credenciales guadadas en el equipo
y no nos la este pidiendo cada vez

```r
git config --local credential.helper store
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Permission denied (publickey,keyboard-interactive)

- Fuente: [gitlab](https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account)

**Descripcion:** Al querer hacer un push a un repo de gitlab me marca
este error
**Error:** Abajo se ve completo.

```shell
$ git push -u origin master

The authenticity of host 'gitlab.com (2606:4700:90:0:f22e:fbec:5bed:a9b9)' ca not be established.
ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'gitlab.com,2606:4700:90:0:f22e:fbec:5bed:a9b9' (ECDSA) to the list of known hosts.
git@gitlab.com: Permission denied (publickey,keyboard-interactive).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.

# TESTEAR MAS PRESISAMENTE
$ ssh -T git@gitlab.com
git@gitlab.com: Permission denied (publickey,keyboard-interactive).
```

**Causa**: En mi caso, no tenia agregada la llave publica de mi maquina
con el servidor de gitlab.

**Sulucion:** Agregar la llave siguiendo los pasos en la fuente, resumen
hice lo de abajo

```shell
# lo hice en "Git Bash"
# Cree la llave, lo importante es memorizar la contraseña que nos pedira
$> ssh-keygen -t ed25519 -C "<comment>"
Enter passphrase (empty for no passphrase): *******
Enter same passphrase again: *******
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):

# En mi caso modifique el nombre despues de crearla asi que la agrege
# con el nombre personalizado
$> ssh-add /c/Users/subject51/.ssh/id_<CUSTOM NAME>

# En la interface de gitlab segui los pasos
# 1. Sign in to GitLab.
# 2. In the top right corner, select your avatar.
# 3. Select Settings.
# 4. From the left sidebar, select SSH Keys.
# 5. In the Key box, paste the contents of your public key. If you manually copied the key, make sure you copy the
#    entire key, which starts with ssh-ed25519 or ssh-rsa, and may end with a comment.

# TESTEAR que nos podemos conectar bien, 
# SI NO FUNCIONA puede ser que reiniciando la terminal u con otra se pueda
$> ssh -Tvvv git@gitlab.com
Welcome to GitLab, @MarioM51!
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Error The certificate chain was issued by an authority that is not trusted

<!-- unable to access 'https://45.56.68.63:3000/mario51/cdmt_postgres.git/': schannel: next InitializeSecurityContext failed: SEC_E_UNTRUSTED_ROOT (0x80090325) - The certificate chain was issued by an authority that is not trusted. -->

**Descripcion:** Al querer hacer push o algun comando parecido, muestra
este error.
**Causa:** En mi caso estoy usando un certificado autofirmado, por lo que
git no confia en estos por default.
**Solucion:** Desactivar PARA NUESTRO PROYECTO NADA MAS la validacion
del certificado.

4, Desactivar validacion de certificado en nuestro repo/proyecto, en el
archivo `<mi-proyecto>/.git/config` agregamos.

```r
[http]
    sslVerify = false 
```

Solucion **no recomendada** desactivar globalmente la validacion de
los sertificados `git config --local http.sslVerify false`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Could not open a connection to your authentication agent

- Fuente: [stackoverflow](https://stackoverflow.com/questions/17846529/could-not-open-a-connection-to-your-authentication-agent)

**Descipcion:** Al querer usar `ssh-add path/my/key` marca este error,
tal como en el titulo.
**Causa:** No se, creo el servicio del ssh esta abajo.
**Solucion:** Iniciar el agente con

```bash
eval `ssh-agent -s`
```

### Al querer descargar rama se hace merge

- Fuente: [stackoverflow](https://stackoverflow.com/questions/1709177/git-pull-a-certain-branch-from-github)

**Descripcion:** Tengo en local solo la rama test, y en mi remoto tento la
rama qa, y al hacer un `git pull origin qa` esta no se descarga, si no
que se hace merge en test.

**Causa:** Ver fuente, parte del funcionamiento del pull es hacer merge,
por lo que para descargar la rama sin afectar la rama actul debemos de
hacerlo de la siguiente manera.

**Duda:** ¿por que habeses con el pull se descarga la rama y otras se
hace merge?

```sh
git pull origin <branch_name>:<branch_name>
```

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Tips

## Evitar crear ramas de una determinada rama

```sh
#!/bin/sh

getBranchName() {
    echo $(git rev-parse --abbrev-ref HEAD)
}

getMergedBranches() {
    echo $(git branch --merged)
}

if [ "$(getBranchName)" != "test" ]; then
    if [[ $(getMergedBranches) == *"test"* ]]; then
        echo "******************************************************"
        echo "SOLO DEBES DE CREAR NUEVAS RAMAS DE PRODUCCION"
        echo "******************************************************"
        exit 1
    fi
fi

if [ "$(getBranchName)" != "qa" ]; then
    if [[ $(getMergedBranches) == *"qa"* ]]; then
        echo "******************************************************"
        echo "SOLO DEBES DE CREAR NUEVAS RAMAS DE PRODUCCION"
        echo "******************************************************"
        exit 1
    fi
fi
```
