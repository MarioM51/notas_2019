# Patrones de Diseño Con Luis Fernandez

## Introducción a Patrones de diseño y patrón Singleton

Los patrones no son mas que diseños O. O. que se volvieron populares,
entre ellos los 23 patrones de **The Gang Of Four**, es decir que hay
mas patrones, pero estos 23 son los mas comunes.

Es importante memorizar el nombre Y que problema resuelven, para saber
identificar cuando y cual aplicarlo.

Los expertos indican que los patrones se trata de reutilizar diseños,
ya que siempre nos veremos en diferentes dominios (escuela, banco,
hospital, etc.), pero los diseños se van a parecer, la idea es la misma
pero esta adaptada al problema actual.

### Como?

Para describir un patron se proponen 13 elementos agrupados en 4 elementos.

- **Nombre**: Nombre y Sinónimo.
- **Problema**: Motivación, Intención y Aplicabilidad
- **Solución**: Estructura, Participantes, Colaboraciones,
  Implementación, Códigos de Ejemplo, Usos Conocidos y
  Patrones Relacionados
- **Consecuencias**: Consecuencias.

### Singleton

1.36
