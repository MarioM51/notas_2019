# Patrones de diseño

- [Patrones de diseño](#patrones-de-diseño)
  - [1 Introduccion](#1-introduccion)
    - [1 Patrones creacionales](#1-patrones-creacionales)
    - [2 Patrones estructurales](#2-patrones-estructurales)
    - [3 Patrones de Comportamiento](#3-patrones-de-comportamiento)
    - [4 Beneficios de los Patrones de Diseño](#4-beneficios-de-los-patrones-de-diseño)
  - [2. UML - Lenguaje Unificado de Modelado](#2-uml---lenguaje-unificado-de-modelado)
    - [2.1. Introduccion](#21-introduccion)
  - [4. Factory Method](#4-factory-method)
    - [4.1. Codigo Factory Method](#41-codigo-factory-method)
    - [4.2. Ventajas](#42-ventajas)
  - [5. Abstract Factory](#5-abstract-factory)
    - [Ejemplo 1, Crear boton de exito o error en linux o windows (una familia)](#ejemplo-1-crear-boton-de-exito-o-error-en-linux-o-windows-una-familia)
    - [Ejemplo 2, Crear Targetas o Formas de pago (dos familias)](#ejemplo-2-crear-targetas-o-formas-de-pago-dos-familias)
    - [Ventajas Desventajas](#ventajas-desventajas)
  - [6. Builder](#6-builder)
    - [6.1. Codigo Builder](#61-codigo-builder)
    - [6.2. Ventajeas](#62-ventajeas)
  - [Prototype](#prototype)
    - [Codigo Prototype](#codigo-prototype)
  - [8. Singlenton](#8-singlenton)
    - [Codigo Singlenton](#codigo-singlenton)
    - [Ventajas](#ventajas)
  - [==== PATRONES COMPORTAMIENTO =====](#-patrones-comportamiento-)
  - [9. Chain of Responsibility](#9-chain-of-responsibility)
    - [Codigo Chain of Responsibility](#codigo-chain-of-responsibility)
    - [Ventajas Chain of Responsibility](#ventajas-chain-of-responsibility)
  - [10. Command](#10-command)
    - [10.1. Teoría Command](#101-teoría-command)
    - [Codigo Command](#codigo-command)
    - [Ventajas Command](#ventajas-command)
  - [11. Iterator](#11-iterator)
    - [11.1. Codigo Iterator](#111-codigo-iterator)
    - [Ventaja Iterator](#ventaja-iterator)
  - [12. Mediator](#12-mediator)
    - [Codigo Mediator](#codigo-mediator)
    - [Ventajas Mediator](#ventajas-mediator)
  - [13. Teoría Memento](#13-teoría-memento)
    - [Codigo Momento](#codigo-momento)
    - [Ventajas Memento](#ventajas-memento)
  - [14. Observer](#14-observer)
    - [Codigo Observer](#codigo-observer)
    - [Ventajas Observer](#ventajas-observer)
  - [15. State](#15-state)
    - [Codigo State](#codigo-state)
  - [16. Interpreter](#16-interpreter)
    - [Codigo Interpreter](#codigo-interpreter)
    - [Ventajas interpreter](#ventajas-interpreter)
  - [17. Strategy](#17-strategy)
    - [Codigo Strategy](#codigo-strategy)
    - [Ventajas Strategy](#ventajas-strategy)
  - [18. Template Method](#18-template-method)
    - [Codigo Template Method](#codigo-template-method)
    - [Ventajas Template Method](#ventajas-template-method)
  - [19. Visitor](#19-visitor)
    - [Codigo Visitor](#codigo-visitor)
    - [Ventajas Visitor](#ventajas-visitor)
  - [==== PATRONES ESTRUCTURALES =====](#-patrones-estructurales-)
  - [Patron Adapter](#patron-adapter)
    - [Codigo Adapter PENDIENTE-TODO](#codigo-adapter-pendiente-todo)
    - [Ventajas Adapter PENDIENTE](#ventajas-adapter-pendiente)
  - [21. Bridge](#21-bridge)
    - [Codigo Bridge](#codigo-bridge)
    - [Ventajas Bridge](#ventajas-bridge)
  - [22. Composite](#22-composite)
    - [Codigo Ejemplo Composite](#codigo-ejemplo-composite)
    - [Ventajas Composite](#ventajas-composite)
  - [23. Decorator](#23-decorator)
    - [Codigo Decorador](#codigo-decorador)
    - [Ventajas Decorator](#ventajas-decorator)
  - [24. Facade](#24-facade)
    - [Codigo Facade](#codigo-facade)
    - [Ventajas Facade](#ventajas-facade)
  - [25. Flyweight](#25-flyweight)
    - [Codigo Flyweight](#codigo-flyweight)
    - [Ventajas Flyweight](#ventajas-flyweight)
  - [26. Proxy](#26-proxy)
    - [Codigo Proxy](#codigo-proxy)
    - [Ventajas Proxy](#ventajas-proxy)

## 1 Introduccion

Un patron de diseño son las tecnicas que nos permiten resolver
problemas comunes, en el mundo de la programacion.

El curso esta basado en **The Gang Of Four** (GoF) que son 4 programadores
de IBM, que fueron capaces de analisar y documentar **23 problemas**
comunes y sus mejores soluciones,

Tenemos 3 categorias de patrones de diseño.

1. Creacionales
2. Estructurales
3. Comportamiento

Los problemas que encontraron GoF, fueron los siguientes

<!-- Problemas solucionan los Patrones de diseño  -->

- ¿Como encontrar los objetos apropiados?
  - Como quitarnos peso o alibianar la carga de crear los objetos correctos

- ¿Cual es la granularidad de los objetos?
  - Dan pautas para partir una clase y asi cuidar su tamaño de esta.

- ¿Cules y como luciran nuestras interfaces?
  - Pautas para diferenciar entre clases abstractas e interfaces, y cuando
    aplicarlas, ya que podemos caer a crear de mas.

- ¿Como especificamos el comportamiento de los objetos en runtime?
  - Pautas para elejir una relacion u otra, por ejemplo, preferible usar
    una relacion de composicion que de herencia, ya que la herencia puede
    desencadenar cambios rio abajo mas facil, que la compocicion ya que
    esta tiene una mejor encapsulacion.

- ¿Como reutilizaremos el codigo, ya sea importarlo o exportarlo?
  - Pautas que nos llevan a la reutilizaccion, como talbien el caso de
    usar compocicion en lugar de herencia, ya que esto nos permite la
    misma clase en otros escenarios, y aun asi podemos cambiar el
    comportamiento en casos concretos.

- ¿Como relacionar estructuras en tiempo de compilacion y ejecucion?
  - Ahi que diferenciar que las relaciones que tenemos en tiempo de
    compilacion es diferente al de ejecucion

### 1 Patrones creacionales

Son utilizados en lugar de la instanciacion directa a travez de un
constructor, es decir concierne al proceso de creacion de objetos. Y
nos ofrecen flexibilidad a la hora de crear esos objetos y como se
inicializan, dentro de los cuales se encuentran.

1. Factory Method
2. Abstract Factory
3. Builder
4. Prototype
5. Singleton

### 2 Patrones estructurales

Tratan la composicion de las clases y objetos, separan la interfaz de
la implementacion con el objetivo de crear estructuras mas complejas.
Y nos ofrecen que cuando una una de las partes cambia, no es necesario
cambiar toda la estructura de la aplicacion, dentro de los cuales se
encuentran.

1. Adapter
2. Bridge
3. Composite
4. Decorador
5. Facade
6. Flyweight
7. Proxy

### 3 Patrones de Comportamiento

Se encargan de definir las formas en que interactuan y reparten
responsabilidades las distintas clases y objetos, dentro de los cuales
se encuentran.

1. Chain of responsability
2. Command
3. Interpreter
4. Interactor
5. Mediator
6. Momento
7. Observer
8. State
9. Strategy
10. Template method
11. visitor

### 4 Beneficios de los Patrones de Diseño

1. Nos ofrecen una manera de solucionar problemas relacionados con el
   desarrollo del software utilizado una solucion altamente probada.
2. La comunicacion entre programadores es mucho mas eficiente. Puesto
   que utilizando un patron todo el equipo entiende el codigo a la
   primera.

----

----

<!-- ############################################################# #############################################################-->

----

----

## 2. UML - Lenguaje Unificado de Modelado

### 2.1. Introduccion

UML (Lenguaje unificado de modelado), es un lenguaje grafico para
visualizar, especificar, construir y documentar un sistema. en el cual
tenemos diferentes expreciones graficas como.

Diagrama de clase

![01_diagrama_para_clase](./img/01_diagrama_para_clase.PNG)

**Simbolo de herencia "─|>"**, lo que vemos es que la clase Perro y Gato,
heredan de la clase animal.

![02_simbolo_herencia](./img/02_simbolo_herencia.PNG)

**Simbolo Asociacion "─>"**. Asociacion significa que una entidad se
construye a partir de otras, abajo vemos `Empledo esta asociado con Equipo`
lo que significa que la clase empleado conoce la existencia de la
clase equipo pero la clase equipo desconoce la clase empleado.

![03_simbolo_asociacion](./img/03_simbolo_asociacion.PNG)

**Simbolo dependencia "··>"**. Es una forma de relacion de clases, en la
que una clase depende de la otra. Abajo leemos `Impresora usa archivo`,
osea la clase impresora conoce la clase archivo, pero archivo desconoce
que impresora exista.

La diferencia con asociacion, es que asociacion la tiene como atributo
y dependencia solo la utiliza, en este caso como entrada de un metodo.

![04_simbolo_dependencia](./img/04_simbolo_dependencia.PNG)

**Simbolo agregacion "─<>"**: Es un tipo de relacion dinamica, en donde
el tiempo de vida del objeto incluido es independiente del que lo incluye
es decir, `si se elimina la clase empresa el cliente sigue existiendo`.

Creo es cuestion de memoria, en donde si eliminamos empresa como
objeto, el cliente como objeto seguira existiendo.

![05_simbolo_agregacion.PNG](./img/05_simbolo_agregacion.PNG)

**Simbolo Composicion "─o"**: Tipo de relacion estatica, el tiempo de
vida del objeto incluido esta condicionado por el tiempo de vida del
objeto que lo incluye,
`Si se borra la clase Empresa se ha de borrar empleado`.

![06_simbolo_composicion](./img/06_simbolo_composicion.PNG)

----

----

<!-- ############################################################# #############################################################-->

----

----

## 4. Factory Method

Escenario: Queremos instanciar un objeto pero no sabemos de que tipo
va a ser, ya sea por que se decide en tiempo de ejecucion o en tiempo
de construccion.

Ejemplo: por ejemplo, tenemos diferentes formas de pago, con visa
mastercard, etc, el cual aplican una interface pago, entonces para
saber hacer la tarea de que instancia crear, y asi poder nosotros meter
la instancia en la interface, usamos un Factory.

Diagrama: Vemos que tenemos `creator` que usa la interface `product`,
el cual la aplicamos en `ConcreteProduct`. Y nuestro factory es
`ConcreteCreator` que es usado por `creator` para obtener la instancia
concreta de y guardarla en la interfce `product`.

![07_diagrama_factory](./img/07_diagrama_factory.PNG)

### 4.1. Codigo Factory Method

A Creamos la interface `pago` que viene a ser nuestro `product` en el
diagrama del patron.

```java
public interface IPago {
  void pagar();
}
```

B. Creamos `ConcreteProduct` que en nuestro ejemplo serian las clases
de Visa y MastarCard

B.1. Creamos Visa

```java
public class PagoVisa implements IPago {
  @Override public void pagar() {
    System.out.println("Pago con Visa");
  }
}
```

B.2. Creamos MasterCard

```java
public class PagoMasterCard implements IPago {
  @Override public void pagar() {
    System.out.println("MasterCard Pago");
  }
}
```

C. Creamos nuestro `PagoFactory` que es equivalente a `ConcreteCreator`
en el diagrama.

C.1. Para poder elegir en el factory creamos un enum, ya que si ponemos
numeros o cadenas, puede ser mas suceptible a errores.

```java
public enum PagosType {
  VISA,
  MASTERCARD
}
```

C.2. Creamos `PagoFactory`.

```java
public class PagoFactory {
  public static IPago buildPago(PagosType pagoType){
  switch (pagoType) {
    case VISA:
        return new PagoVisa();

      case MASTERCARD:
        return new PagoMasterCard();
    }
    return null;
  }
}

```

D. Lo usamos en nuestro Main que vendria siendo el `Creator` en
nuestro diagrama.

```java
public static void main(String[] args ) {
  IPago pago = PagoFactory.buildPago(PagosType.VISA);
}
```

**Resultado:** Estamos aplicando el patron de Factory y tenemos todas
sus ventajas que este trae, abajo vemos el diagrama que es quivalente
al que tenemos arriba.

```r
#                                  con: http://asciiflow.com/
     +----+    dependencia    +-----+
     |Main+------------------>+IPago|
     +-+--+                   +--+--+
       ^                         ^
       |                         |
    Agregacion                Herencia
       |                         |
       |                    +----+-----------------+
       |                    |   ConcreteCreator    |
+------+----+  dependencia  |                      |
|PagoFactory+-------------->+ +----+  +----------+ |
+-----------+               | |Visa|  |MasterCard| |
                            | +----+  +----------+ |
                            |                      |
                            +----------------------+
```

### 4.2. Ventajas

- Tenemos un codigo desacoplado ya que en nuestra clase de
alto nivel (Main) no dependendemos de ninguga implementacion, como lo
es `PagoVisa` o `PagoMasterCard`.

- estamos delegando la tarea de crear dicha implementacion al factory.

- Y si quermeos en un futuro agregar una nueva implementacion, el codigo
de `Main` no lo tocariamos, solo agregamos la nueva implementacion en
el Factory y este decidira cual implementacion usar.

Desventaja: Esl codigo es mas complejo y creamos mas clases de las que
usariamos sin usar patrones.

## 5. Abstract Factory

**Descripcion:** Se podria definir como una fabrica de fabricas, donde
desde esta podemos delegar la tarea de instanciar una fabrica en
concreto, donde separamos estas fabricas de fabricas por categorias.

Diagrama:

`Cliente:` Clase de alto nivel (por ejemplo el Main controlador) el
cual hace uso de `AbstracFactory` para instanciar algun mienbro de
`produtoA` o `productoB`.

`AbstracFactory`: Es la interface que aplicaran las factorias concretas
en este caso, `FactoriaConcreta1` que instanciara objetos de la familia
`ProductoA` como es `ProductoA1` y `ProductoA2`. Y `FactoriaConcreta2`,
instanciara objetos de la familia de ``ProductoB`.

![07_diagrama_factory](./img/08_diagrama_abstract_factory.PNG)

**Uso comun en** los graficos, donde un objeto puede significar
una familia, por ejemplo, cuando si creamos un boton, este boton sera
diferente, ya sea que estemos en windows, linux o mac, y lo que hara
la AbstractFactory sera de crear el boton correcto segun el sistema
operativo.

**Ejemplo-1:** Podemos crear una un AbstractFactory de automoviles,
donde podremos traer una fabrica de camionetas ya sea una Ford-2Tn o
una Ford-1Tn, o una de carros, donde podremos crear un chevy modelo A
o un Chevy modelo B.

Ejemplo de como se vera el codigo al tener finalmente implementada la
AbstractFactory de nuestro ejemplo1.

```java
AbstractFactory trucksFactory = FactoryProvider.getFactory("Trucks");
Truck truck = (Truck)trucksFactory,create("Ford-2Tn");

AbstractFactory carFactory = FactoryProvider.getFactory("Car");
Car car = (Car)carFactory,create("ChevyB");
```

**Ejemplo2:** Podemos crear un AbstractFactory de objetos de pagos,
como una fabrica de targetas, donde podremos crear targetas tipo VISA
o MasterCard, y otra donde cree tipos de pago, en el que podremos crear,
pagos a credito o pafos a debito.

----

### Ejemplo 1, Crear boton de exito o error en linux o windows (una familia)

Codigo (run en TestAbstracFactory): [08_diagrama_abstract_factory_ej_alertas_lin_win_codigo.zip](./img/08_diagrama_abstract_factory_ej_alertas_lin_win_codigo.zip)
Nota, tiene un error crea solo mensajes de linux, solo ahi que agregar
un break en el switch.

**Nota:** Esta ejemplo no es 100& leal al patron, ya que se adapto
a las necesidades del problema, demostrando que los patrones no estan
escritos sobre piedra, y aun asi tenemos un codigo desacoplado a la
creacion de instancias.

A. Es recomensable primero crear el diagrama de nuestra aplicacion
aplicando el patron, usando el diagrama del patron como guia, en mi
caso salio asi, el cual no es exacto pero ayuda bastante.

![08_diagrama_abstract_factory_ej_alertas_lin_win.PNG](./img/08_diagrama_abstract_factory_ej_alertas_lin_win.PNG)

B. Vamos a terminar con la siguiente estructura de archivos

```r
Mensajes
│   └───impl
│   │   ├───data
│   │   │──────MsjType.java
│   │   │──────OSType.java
│   │   │
│   │   ├───factories
│   │   │──────MensajeLinuxFactory.java
│   │   │──────MensajeWindowsFactory.java
│   │   │
│   │   └───Mensajes
│   │       │───MensajeErrorWindows.java
│   │       │───MensajeExitoWindows.java
│   │       │───MensajeLinuxError.java
│   │       │───MensajeLinuxExito.java
│   │
│   │───IMensaje.java
│   │───IMensajesFactory.java
│   │───MensajesFactoryProvider.java
│
│──TestAbstractFactory.java
```

C. Creamos la interface que implementaran los mensajes en diagrama `MSJ`

```java
public interface IMensaje {
  void mostrarMensaje();
}
```

D. Creamos la interface de la fabrica, que implementaran las fabricas
   concretas

```java
public interface IMensajesFactory {
  IMensaje crearMensajeExito();
  IMensaje crearMensajeError();
}
```

D. Creamos la implementacion de las fabricas

```java
public class MensajeLinuxFactory implements IMensajesFactory {
  @Override public IMensaje crearMensajeExito() {
    return new MensajeLinuxExito();
  }
  @Override public IMensaje crearMensajeError() {
    return new MensajeLinuxError();
  }
}
//------
public class MensajeWindowsFactory implements IMensajesFactory {
  @Override public IMensaje crearMensajeExito() {
    return new MensajeExitoWindows();
  }

  @Override public IMensaje crearMensajeError() {
    return new MensajeErrorWindows();
  }  
}
```

F. Creamos nuestros Mensajes usando la interface de Mensaje que usamos.

```java
public class MensajeErrorWindows implements IMensaje {
  @Override public void mostrarMensaje() { System.err.println("Mensaje Windows de error"); }
}
//
public class MensajeExitoWindows implements IMensaje {
  @Override public void mostrarMensaje() { System.out.println("Mensaje Windows de Exito"); }
}
//
public class MensajeLinuxError implements IMensaje {
  @Override public void mostrarMensaje() { System.err.println("MENSAJE DE ERROR DE LINUX"); }
}
//----
public class MensajeLinuxExito implements IMensaje {
  @Override public void mostrarMensaje() { System.out.println("MENSAJE LINUX DE EXITO"); }
}
//----
```

G, Creamos la Fabrica Abstracta, el cual retornara la interface que
estan implementando las fabricas.

```java
public class MensajesFactoryProvider {
  public static IMensaje crearMensaje(OSType osType, MsjType msjType) {
    IMensajesFactory msjFactory = null;
    switch (osType) {
      case WINDOWS: msjFactory = new MensajeWindowsFactory(); break;
      case LINUX:   msjFactory = new MensajeLinuxFactory(); break;
    }
    switch (msjType) {
      case EXITO: return msjFactory.crearMensajeExito();
      case ERROR: return msjFactory.crearMensajeError();
    }
    return null;
  }
}
```

H. Usamos la Fabrica abstracta, donde podemos crear cada una de las
implementaciones de mensaje que tenemos de una forma sencilla.

```java
public class TestAbstractFactory {
  public static void main(String[] args) {
    for (OSType osType : OSType.values()) {
      for (MsjType msjType : MsjType.values()) {
        IMensaje msj = MensajesFactoryProvider.crearMensaje(osType, msjType);
        msj.mostrarMensaje();
      }
    }
  }
}
/* salida
Mensaje Windows de error
Mensaje Windows de Exito
MENSAJE DE ERROR DE LINUX
MENSAJE LINUX DE EXITO */
```

**Resultado:** Estamos aplicando el patron y con sus ventajas, en este
caso podemos crear una gran cantidad de objetos de una familia con
una gran facilidad.

----

### Ejemplo 2, Crear Targetas o Formas de pago (dos familias)

En nuestro ejemplo anterior solo creamos implementaciones de una
interface, (a eso me refiero con una familia), el cual no es 100% leal
al patron, lo que demuestra que los patrones no estan escritoes en
piedra.

En este ejemplo veremos como crear implementaciones de dos interfaces
diferentes, el cual este ejemplo es mas leal al patron.

A. Hacemos diagrama, basandonos en el original

![08_diagrama_abstract_factory_ej_2_targetas-o-pago](./img/08_diagrama_abstract_factory_ej_2_targetas-o-pago.PNG)

B. La estructura de carpeta con la que terminaremos es la siguiente.

```r
│   FinanzasMain.java
│
├───finansas
│   │   FinansasFactoryProvider.java
│   │
│   └───domain
│       │   FinansasConstants.java
│       │   IFinansasFactory.java
│       │   IPago.java
│       │   ITargeta.java
│       │
│       ├───factories
│       │       PagoFactoryImpl.java
│       │       TargetaFactoryImpl.java
│       │
│       └───impl
│               PagoCreditoImpl.java
│               PagoDebitoImpl.java
│               TargetaMasterCardImpl.java
│               TargetaVisaImpl.java
```

C. Creamos la interface de las fabricas, el cual retornara un tipo
generico y entrara un generico tipo enum (el tipo se lo definimos al
crear las clases)

```java
public interface IFinansasFactory<E, T extends Enum<?>> {
  E create(T type);
}
```

D. Creamos las interfaces=familias de nuestras implementaciones

```java
public interface IPago {
  String realizarPago();
}

public interface ITargeta {
  String getNumero();
  int getSaldo();
}
```

E.Cremos las implementaciones de fabrica usando las interfaces que
cremoas

```java
public class PagoCreditoImpl implements IPago {
  @Override public String realizarPago() { return "Pago a credito de 800 a Oxxo de unos pomos realizado"; }
}
// #####
public class PagoDebitoImpl implements IPago {
  @Override public String realizarPago() { return "Pago hecho con DEBITO de 1000 a Suburbia de unos pantalones"; }
}
// #####
public class TargetaMasterCardImpl implements ITargeta {
  @Override public String getNumero() { return "1234-5678-9012-3456"; }
  @Override public int getSaldo() { return 10000; }
}
// #####
public class TargetaVisaImpl implements ITargeta {
  @Override public String getNumero() { return "XXXX-XXXX-XXXX-3456"; }
  @Override public int getSaldo() { return 22222; }
}
```

F. Creamos las constantes para poder indicar que cosa construir

```java
public class FinansasConstants {
  public static enum Cardtyoe { VISA, MASTERCARD }
  public static enum PagoTyoe { DEBITO, CREDITO }
}
```

F. Cremos nuestras fabricas una por fada familia=interface de nuestros
objetos=implementaciones

```java
public class PagoFactoryImpl implements IFinansasFactory<IPago, FinansasConstants.PagoTyoe> {
  @Override public IPago create(FinansasConstants.PagoTyoe type) {
    switch (type) {
      case CREDITO: return new PagoCreditoImpl();
      case DEBITO: return new PagoDebitoImpl();
    } return null;
  }
}
// ####
public class TargetaFactoryImpl implements IFinansasFactory<ITargeta, FinansasConstants.Cardtyoe> {
  @Override public ITargeta create(FinansasConstants.Cardtyoe type) {
    switch (type) {
      case VISA:       return new TargetaVisaImpl();
      case MASTERCARD: return new TargetaMasterCardImpl();
    } return null;
  }
}
```

G. Creamos nuestra fabrica de fabricas, el cual tgendra dos metodos yaque a cada fabrica.

```java
public class FinansasFactoryProvider {
  public static IFinansasFactory<ITargeta, Cardtyoe> getTargetasFactory() {
    return new TargetaFactoryImpl();  
  }

  public static IFinansasFactory<IPago, PagoTyoe> getPagosFactory() {
    return new PagoFactoryImpl();  
  }
}
```

H. Usamos nuestra fabrica de fabricas

```java
public class FinanzasMain {
  public static void main(String[] args) {
    //OBTENEMOS FABRICAS
    IFinansasFactory<ITargeta, Cardtyoe> factory = FinansasFactoryProvider.getTargetasFactory();
    IFinansasFactory<IPago, PagoTyoe> factoryPagos = FinansasFactoryProvider.getPagosFactory();

    // LOGICA DE NEGOCIO
    Cardtyoe targetaCliente = Cardtyoe.MASTERCARD;
    PagoTyoe tipoDePago = PagoTyoe.DEBITO;

    // FABRICAMOS/INSTANCIAMOS HERRAMIENTAS
    ITargeta miTargeta = (ITargeta)factory.create(targetaCliente);
    IPago ipago = (IPago)factoryPagos.create(tipoDePago);

    // REALIZAR ACCIONES
    System.out.println("Operacion: " + ipago.realizarPago());
    System.out.printf("Targeta: %s, sado actual: %s \n", miTargeta.getNumero(), miTargeta.getSaldo());

  }
}
```

**RESULTADO:** Podremos hacer 4 diferentes configuracion y podremos
cambiear entre ellas de manera facil, por ejmplo.

- pagar a credito, con targeta matercard
- pagar a credito, con targeta visa
- pagar a debito, con targeta matercard
- pagar a debito, con targeta visa

### Ventajas Desventajas

- Aislamos las clases de alto nivel con las de bajo nivel, en este
  caso el main no conoce a ninguna de nuesteas implementaciones, ya
  que enmedio estan las interfces de las fabricas, como tanbien de
  los objetos
- Nos fcilita el intercambio de implementacion entre su familia.
- Promueve la concistencia entre objetos

**DESVENTAJA** Al crear un nuevo objeto debemos de modificar las
fabricas abstractas (FactoryProvider) como concretas.

----

----

<!-- ############################################################# #############################################################-->

----

----

----

## 6. Builder

Este nos permite crear un objeto complejo (entiendase complejo como un
objeto con varios atributos) a partir de otro objeto, en el que el objeto
resultante le llamamos producto complejo.

En el diagrama encontramos

- `Director`: Encargado de construir el objeto.
- `Bulder`: Interface que permite la creacion de objetos.
- `ConcreteBuilder`: Es una implementacion del bulder.
- `Product`: El objeto complejo resultante que sera creado por
  CreateBulder.

![09_diagrama_builder](./img/09_diagrama_builder.PNG)

### 6.1. Codigo Builder

**Objetivo:** Vamos a crear un objeto de conexion (objeto complejo) a
travez de un Builder.

A. Creamos Conexion con sus campos.

1. Los campos seran privados y finales.
2. Los campos seran inpmutables, por lo que solo le agregamos los
   geters y seteres
3. Creamos dentro de Conexion una clase Builder donde asignaremos los
   campos desde aqui, tendra en su constructor los campos obligados.
4. Creamos un constructor en Conexion la cual recivira los campos desde
   la clase Bulder para asignar sus campos.

```java
package mario.patrones.creacionales.bulder;

class Conexion {
  private final String url;
  private final String usuario;
  private final String pass;
  private final int puerto;
  private final boolean seguro;
  private final double timeout;

  private Conexion(ConexionBuilder conexionBuilder) {
    this.url = conexionBuilder.url;
    this.usuario = conexionBuilder.usuario;
    this.pass = conexionBuilder.pass;
    this.puerto = conexionBuilder.puerto;
    this.seguro = conexionBuilder.seguro;
    this.timeout = conexionBuilder.timeout;
  }

  public String getUrl() { return url; }
  public String getUsuario() { return usuario; }
  public String getPass() { return pass; }
  public int getPuerto() { return puerto; }
  public boolean isSeguro() { return seguro; }
  public double getTimeout() { return timeout; }

  public static class ConexionBuilder {
    private String url;
    private String usuario;
    private String pass;
    private int puerto;
    private boolean seguro;
    private double timeout;

    public ConexionBuilder(String url, String usuario, String pass) {
      this.url = url;
      this.usuario = usuario;
      this.pass = pass;
    }

    public ConexionBuilder puerto(int puerto) {
      this.puerto = puerto;
      return this;
    }

    public ConexionBuilder seguro(boolean seguro) {
      this.seguro = seguro;
      return this;
    }

    public ConexionBuilder timeout(double timeout) {
      this.timeout = timeout;
      return this;
    }

    public Conexion build() {
      return new Conexion(this);
    }
  }
}
```

B. Usamos el patron

```java
public class Buildermain {
  public static void main(String[] args) {
    Conexion c = new Conexion.ConexionBuilder("miUrl", "MiUsuario", "MiPass")
                  .puerto(80)
                  .seguro(false)
                  .build();
    System.out.println(c);
  }
}
```

**Resultado:** Estamos aplicando el patron builder con sus ventajas

### 6.2. Ventajeas

1. Es un patron muy usado en librerias para crear cofiguraciones de
   objetos como deceamos.
2. Codigo mas ordenado ya que no nos permite modificar el objeto.
3. Codigo desacoplado.

----

----

<!-- ############################################################# #############################################################-->

----

----

----

## Prototype

Este patron tiene el objetivo de crear objetos a partir de un modelo =
prototipo, y especificar que tipo de objeto se creara a travez de este
objeto modelo.

Si tenemos un objeto que ocupa muchos recursos, lo mejor es clonarlo y
podemos usar este patron, lo cual ayudara a crearlo sin nosotros tener
que saber los detalles de como crearlo.

Diagrama: podemos ver que tenemos.

- `Prototype`: Interface que implementaran los prototipos concretos
  para clonar el objeto
- `Client`: El objeto que se va  a clonar.

![10_diagrama_prototype](./img/10_diagrama_prototype.PNG)

### Codigo Prototype

A. Creamos nuestra interface

1. Esta extendera de Cloneable, este no implementa ningun metodo, pero
   indica que cuando usamos la clase clone podra crear una copia mienbro
   a miembro de las instancias de dicha clase.
2. Tendra 2 metodos uno para clonar y otra para obetener el clon

```java
public interface IPrototypeHistorial  extends Cloneable {
  IPrototypeHistorial clone() throws CloneNotSupportedException;
  void getHistorial();
}
```

B. crear los `concretePrototype` que implementaran la interface

1. En el metodo clone usara el metodo clone de su clase padre que
   es el que clonara el objeto, y como indicamos que sea Cloneable
   este sera copiado miembro a mismbro.
2. El Segundo metodo es la reprecentacion de obtener nuestro objeto que
   deberia ser lo bastante grande para implementar este patron.

```java
public class HistorialCobros implements IPrototypeHistorial {
  private String name;

  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  @Override public IPrototypeHistorial clone() throws CloneNotSupportedException {
    System.out.println("clonando historial de cobros");
    return (HistorialCobros) super.clone();
  }

  @Override public void getHistorial() {
    System.out.println("Historial de cobros es el siguiente: 1,2,3,4,5,6,7,8,9....");
  }
}
//###################
public class HistorialPagos implements IPrototypeHistorial {

  private String name;

  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public IPrototypeHistorial clone() throws CloneNotSupportedException {
    System.out.println("CLONANDO historial de PAGOS");
    return (HistorialPagos) super.clone();
  }

  @Override
  public void getHistorial() {
    System.out.println("HISTORIAL de PAGOS es el siguiente: ...9,8,7,6,5,4,3");
  }
}
```

C. Creamos la clase que implementara el metodo clon para nosotros de
una manera mas facil

```java
public class PrototypeFactory {
  public static enum HistorialType { COBROS, PAGOS };

  private static Map<HistorialType, IPrototypeHistorial> prototypes = new HashMap<>();
  
  public static IPrototypeHistorial getInstance(final HistorialType type) throws CloneNotSupportedException {
    return prototypes.get(type).clone();
  }

  //aquicreamos los objetos que queremos clonar
  public static void loadHistorial() {
    HistorialPagos hPagos = new HistorialPagos();
    hPagos.setName("Historial de pagos");
    prototypes.put(HistorialType.PAGOS, hPagos);

    HistorialCobros hCobros = new HistorialCobros();
    hCobros.setName("Historial de COBROS");
    prototypes.put(HistorialType.COBROS, hCobros);
  }
}
```

D. Probamos

```java
package mario.patrones.creacionales;

import mario.patrones.creacionales.prototype.IPrototypeHistorial;
import mario.patrones.creacionales.prototype.PrototypeFactory;
import mario.patrones.creacionales.prototype.PrototypeFactory.HistorialType;

public class PrototypeMain {
  public static void main(String[] args) {
    PrototypeFactory.loadHistorial();
    try {
      IPrototypeHistorial pHistorialPagos = PrototypeFactory.getInstance(HistorialType.PAGOS); //clone
      pHistorialPagos.getHistorial();

      IPrototypeHistorial pHistorialCobros = PrototypeFactory.getInstance(HistorialType.COBROS); //clone
      pHistorialCobros.getHistorial();

      IPrototypeHistorial pHistorialCobros2 = PrototypeFactory.getInstance(HistorialType.COBROS); //clone
      pHistorialCobros2.getHistorial();

      System.out.println("Clon1> " + pHistorialCobros);
      System.out.println("Clon2> " + pHistorialCobros2);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
/*#### SALIDA
CLONANDO historial de PAGOS
HISTORIAL de PAGOS es el siguiente: ...9,8,7,6,5,4,3
clonando historial de cobros
Historial de cobros es el siguiente: 1,2,3,4,5,6,7,8,9....
clonando historial de cobros
Historial de cobros es el siguiente: 1,2,3,4,5,6,7,8,9....
```

**Resultado:** Estamos obteniendo un clon de un objeto el cual puede
ser muy grande reprcentando un problema de rendimiento, lo cual lo
mejor es clonarlo que crearlo.

## 8. Singlenton

Nos sirve para crear un solo objeto de una clase

### Codigo Singlenton

A. Creamos clase que queremos que tenga solo una instancia

```java
public class PDFCreator {
  String atributo;
  private static PDFCreator INSTANCE = null;
  
  //IMPORTANTE: el constructor debe ser privado para que no se pueda
  //instanciar la clase desde fuera, solo desde dentro.
  private PDFCreator() { }

  // synchronized: es importante por si se ejecuta en dos hilos diferentes
  // se espere asta que termine el otro hilo y asi evitamos que se cree otra
  // instancia no deseada.
  public synchronized static PDFCreator getInstance() {
    if(INSTANCE == null) {
      INSTANCE = new PDFCreator();
    }
    return INSTANCE;
  }
}
```

B. Lo probamos

```java
public class SingletonMain {
  public static void main(String[] args) {
    PDFCreator pdfCreator = PDFCreator.getInstance();
    System.out.println(pdfCreator);

    PDFCreator pdfCreator2 = PDFCreator.getInstance();
    System.out.println(pdfCreator2);
  }
}

/* SALIDA
mario.patrones.creacionales.singleton.PDFCreator@15db9742
mario.patrones.creacionales.singleton.PDFCreator@15db9742
```

**RESULTADO:** Podesmo ver como solo tenemos una sola instancia y no
podemos crear otra.

----

### Ventajas

Es una forma sensilla de obtener evitar crear multiples instancias, pero
ya se opta por usar inyeccion de dependencias, ya que al usar singleton
dificulta el uso de tests, pero aun asi sigue siendo muy utilizado el
patron singleton.

----

----

<!-- ############################################################# #############################################################-->

----

----

## ==== PATRONES COMPORTAMIENTO =====

## 9. Chain of Responsibility

Este es un patron de comportamiento que nos evita acoplar una peticion,
de su receptor a su emisor, dando la posibilidad de dar respueta a dicha
peticion a mas de un objeto.

Su uso ideal es cuando la respuesta puede ser atendida por varios
objetos, y no sabemos cual de esos objetos va a resolverlo, solo en
tiempo de ejecucion.

Puede ser gerarquica esta responsabilidad, es decir, que si un objeto
no puede resolver la peticion, esntonces esta le pasa la responsabilidad
a otro objeto.

Diagrama

- `handler`: Es la interface que implementaran los `ConcreteHandlers`
- `ConcreteHandlers`: Son los objetos que trataran de resolver una
  peticion de cliente
- `Client`: Es el que mandara la peticion al `Handler`

![11_diagrama_chain_of_responsibility](./img/11_diagrama_chain_of_responsibility.PNG)

**Por ejemplo:** Al abrir una targeta de credito dependiendo del la
cantidad, se usara un metodo u otro.

- si se pide un credito de 1,000 se usara una targeta basic,
- 10,000 se usara una pro.
- si se pide 100,000 se hara una gold.

Donde cada una de estas targetas tienen un proceso de valicion diferente.

----

### Codigo Chain of Responsibility

A. Creamos el `Hander`

1. `setNext`: Asignamremo el siguiete al que se le hara la peticion si
   el actual no puede
2. `getNext`: Objtendremos el siguiente a poder resolver la peticion
   esto para XXXXXX-PENDIENTE-TODO-XXXXXXXX

```java
public interface IAperturaCredito {
  void setNext(IAperturaCredito creditoHandler);
  IAperturaCredito getNext();
  void abrirPeticion(int totalCredito);
}
```

B. Creamos los `ConcreteHandlers` que resolvera la peticion o se la
pasara al siguiente

- llevaran la misma logica, solamente se cambiara en `abrirPeticion`
  la condiciones para saber si puede o no resolver la peticion, y si
  no puede se lo pasa al siguiete, usando solamante la interface, para
  no acoplar el codigo.

```java
public class CreditoBasico implements IAperturaCredito{
  private IAperturaCredito nextCreditoHandler;

  @Override public void setNext(IAperturaCredito creditoHandler) {
    this.nextCreditoHandler = creditoHandler;
  }

  @Override public IAperturaCredito getNext() {
    return nextCreditoHandler;
  }

  @Override public void abrirPeticion(int totalCredito) {
    if(totalCredito > 1000 && totalCredito <= 10000 ) { //CONDICIONES PARA PROCESAR LA PETICION
      //AQUI PONDRIAMOS LA LOGICA PARA APROBAR EL CREDITO
      System.out.println("Esta solicitud es manejada por el plan basico, ya que es abajo de 1,000");
    } else {
      // SIGNIFICA QUE NO PUEDE ESTA CLASE RESOLVER LA PETICION, ENTONCES LE PASA
      // LA REPONSABILIDAD AL SIGUIENTE HANDLER
      nextCreditoHandler.abrirPeticion(totalCredito);
    }
  }
}

//######## - MLOMISMO SOLO CAMBIARIA CONDICIONES PARA PROCESAR LA PETICION
//           Y ALGORITMO PARA PROCESARLA
public class CreditoPro implements IAperturaCredito{
  private IAperturaCredito nextCreditoHandler;
  @Override public void setNext(IAperturaCredito creditoHandler) { this.nextCreditoHandler = creditoHandler; }
  @Override public IAperturaCredito getNext() { return nextCreditoHandler; }
  @Override public void abrirPeticion(int totalCredito) {
    if(totalCredito > 1000 && totalCredito <= 10000 ) { //CONDICIONES PARA PROCESAR LA PETICION
      System.out.println("Iniciando proceso de plan Pro, ya que esta entre 1,000 y 10,000");
    } else {
      nextCreditoHandler.abrirPeticion(totalCredito);
    }
  }
}

//######## - MLOMISMO SOLO CAMBIARIA CONDICIONES PARA PROCESAR LA PETICION
//           Y ALGORITMO PARA PROCESARLA
public class CreditoGold implements IAperturaCredito{
  private IAperturaCredito nextCreditoHandler;
  @Override public void setNext(IAperturaCredito creditoHandler) { this.nextCreditoHandler = creditoHandler; }
  @Override public IAperturaCredito getNext() { return nextCreditoHandler; }
  @Override public void abrirPeticion(int totalCredito) {
    if(totalCredito > 10000 && totalCredito <= 100000 ) { //CONDICIONES PARA PROCESAR LA PETICION
      System.out.println("PLAN GOLD, iniciando validacion, ya que esta entre 1,000 y 100,000");
    } else {
      nextCreditoHandler.abrirPeticion(totalCredito);
    }
  }  
}
```

C. Creamos la cendena, usando la misma interface

```java
public class PlanesCredito implements IAperturaCredito {
  private IAperturaCredito next;
  
  @Override public void setNext(IAperturaCredito creditoHandler) {
    next = creditoHandler;
  }

  @Override public IAperturaCredito getNext() { return next; }

  @Override public void abrirPeticion(int totalCredito) {
    CreditoBasico creditoBasico = new CreditoBasico();
    this.setNext(creditoBasico);
    CreditoPro creditoPro = new CreditoPro();
    this.setNext(creditoPro);
    CreditoGold creditoGold = new CreditoGold();
    this.setNext(creditoGold);
    next.abrirPeticion(totalCredito);
  }
}
```

D. Usamos el patron

```java
public static void main(String[] args) {
  PlanesCredito planesCredito = new PlanesCredito();
  planesCredito.abrirPeticion(101);
  planesCredito.abrirPeticion(1001);
  planesCredito.abrirPeticion(10001);
  planesCredito.abrirPeticion(100001);
}

/* SALIDA
Esta solicitud es manejada por el plan basico, ya que es abajo de 1,000
Iniciando proceso de plan Pro, ya que esta entre 1,000 y 10,000
PLAN GOLD, iniciando validacion, ya que esta entre 1,000 y 100,000
ERROR AUN NO SE IMPLEMENTA PLAN PARA CREDITOS MAYORES A 100,000
PS E:\cursos\0activ\codigo\patrones> ^C

```

**Resultado:** Estamos enviado una peticion a resolver y esta puede ser
resuelta por varios objetos, y de manera facil estamos usando la
implementacion que pueda resolverlo.

----

### Ventajas Chain of Responsibility

1. Reduce el acoplamiento
2. El cliente no conoce la cadena de responsabilidad
3. Podemos agregar/eliminar/modificar responsables de la cadena de una
   manera muy sencilla.

**Desventajas:** Puede que no se resuelva la peticion, es nuestro
trabajo garantizar que se apliquen todos los casos.

----

----

<!-- ############################################################# #############################################################-->

----

----

## 10. Command

### 10.1. Teoría Command

Es muy util cuando para extraer la logica de negocio en unidades que
llamamos comandos, ayudando a encampsular toda la informacion necesaria
para realizar una accio/evento. Nos ayuda a separar la logica de
negocio de la aplicacion

Diagrama:

- `command`: Es la interface que aplicaran nuestros comandos.
- `ConcreteCommand`: Este hace la conexion entre la informacion y
  el que sabe resolver la operacion.
- `Recelver`: Este Sabe como srsolver una determinada accion.

![12_diagrama_command](./img/12_diagrama_command.PNG)

**Objetivo:** Vamos a crear dos objetos acciones una para dar de alta,
y otra para hacer que pase de año escolar, lo cual debebemos realizar
varios COMANDOS por ejemplo.

A. Dar de alta a estudiante

1. Ver que tenga su domumentacion en orden
2. Que pase el examen de admicion
3. Que pague la inscripcion

B. Avanzar de año a estudiante

1. Checar que aiga pasado todas sus materias
2. Que entregue documentacion de no adeudos
3. Le enviamos un mensaje si su cierre escolar esta completo o no.

----

### Codigo Command

A. Creamos interface que tenga un metodo execute

```java
public interface ICommand {
  void excecute();
}
```

B. Creamos invoquer, que es el que llamara a cualquera de nuestras dos
acciones de negocio

```java
public class EstudianteInvoquer {
  private ICommand iCommand;

  public EstudianteInvoquer() {}

  public void setCommand(ICommand iCommand) {
    this.iCommand = iCommand;
  }

  public void run() {
    this.iCommand.excecute();
  }

}
```

C. Creamos implementaciones de logica de negocio

C.1. Para pase Ciclo escolar de estudiante

```java
public class AprovacionEscolarEstudiante {

  public void isAproboTodasLasMaterias() {
    System.out.println("APROBO todas las materias");
  }

  public void isNoAdeudos() {
    System.out.println("No tienen ADEUDOS");
  }

  public void enviarResultadoCicloEscolar() {
    System.out.println("mensaje ENVIADO de estatus ciclo escolar");
  }
}
```

C.2. Para Inscripcion de estudiante

```java
public class InscripcionEstudiante {
  public void isDocumentacionCorrecta() {
    System.out.println("Documentacion CORRECTA");
  }

  public void pasoExamenDeAdmicion() {
    System.out.println("APROBADO examen de admicion");
  }

  public void pagoInscripcion() {
    System.out.println("Pago de inscripcion CUBIERTO");
  }
}
```

D. Creamos el wrapper de ambas implementaciones para poderlas llamar
desde el comandInvoquer

D.1. Para pase Ciclo escolar de estudiante

```java
public class AprovacionEscolarEstudianteCommand implements ICommand {

  AprovacionEscolarEstudiante aprovacionEscolarEstudiante;

  public AprovacionEscolarEstudianteCommand(AprovacionEscolarEstudiante aprovacionEscolarEstudiante) {
    this.aprovacionEscolarEstudiante = aprovacionEscolarEstudiante;
  }

  @Override
  public void excecute() {
    this.aprovacionEscolarEstudiante.isAproboTodasLasMaterias();
    this.aprovacionEscolarEstudiante.isNoAdeudos();
    this.aprovacionEscolarEstudiante.enviarResultadoCicloEscolar();
  }
  
}
```

D.2. Para inscripcion de estudiante

```java
public class InscripcionEstudianteCommand implements ICommand {
  InscripcionEstudiante inscripcionEstudiante;

  public InscripcionEstudianteCommand(InscripcionEstudiante inscripcionEstudiante) {
    this.inscripcionEstudiante = inscripcionEstudiante;
  }

  @Override public void excecute() {
    this.inscripcionEstudiante.isDocumentacionCorrecta();
    this.inscripcionEstudiante.pasoExamenDeAdmicion();
    this.inscripcionEstudiante.pagoInscripcion();
  }
}
```

E. Usamos patron Command

```java
public static void main(String[] args) {
  EstudianteInvoquer estudianteInvoquer = new EstudianteInvoquer();
  
  InscripcionEstudianteCommand insComand = new InscripcionEstudianteCommand(new InscripcionEstudiante(););
  estudianteInvoquer.setCommand(insComand);
  estudianteInvoquer.run();
  System.out.println("++++++++++++++++++++++++++");
  AprovacionEscolarEstudianteCommand aprComand = new AprovacionEscolarEstudianteCommand(new AprovacionEscolarEstudiante());
  estudianteInvoquer.setCommand(aprComand);
  estudianteInvoquer.run();
}
/* Salida
Documentacion CORRECTA
APROBADO examen de admicion
Pago de inscripcion CUBIERTO
++++++++++++++++++++++++++
APROBO todas las materias
No tienen ADEUDOS
mensaje ENVIADO de estatus ciclo escolar
```

**Resultdo:** tenemos la logica de negocio desacoplada a la aplicacion
donde, el cliente no conoce los detalles de implementacion.

----

### Ventajas Command

1. Nuestro codigo es extendible, podemos agregar mas comandos sin tener
   que modificar el codigo del cliente, Por ejemplo, si tenemos que
   agregar un nuevo paso en nuestras implementaciones el cliente no se
   ve afectado.

----

----

<!-- ############################################################# #############################################################-->

----

----

## 11. Iterator

Es un patron para acceder de forma secuancial a un conjunto de datos,
muchas apis ya nos dan una api para poder recorrer sus colecciones, sin
exponer su logica

Diagrama.

- `Aggregate`: Es la interface para crear un nuevo iterator
- `Iterator`: Es una interface para recorrer el conjunto.
- `ConcreteIterator`: Implementa Iterator que implementa el como
  reccorer la coleccion, y guarda la posicion del elemento actual.
- `ConvcretAggregate`: Implementa la creacion de los iteradores

![13_diagrama_iterator](./img/13_diagrama_iterator.PNG)

----

### 11.1. Codigo Iterator

- Nota: podriamos usar List de java pero para mostrar el patron no la
  usaremos, como si inventaramos el hilo negro.

A. Creamos el objeto que queremos recorrer en este caso estudiante

```java
public class Estudiante {
  private String nombre;
  private boolean activo;

  public Estudiante(String nombre, boolean activo) {
    this.activo=activo;
    this.nombre=nombre;
  }

  public String getNombre() { return nombre; }
  public void setNombre(String nombre) { this.nombre = nombre; }
  public boolean isActivo() { return activo; }
  public void setActivo(boolean activo) { this.activo = activo; }
}
```

B. Cremoas la interface de interator

```java
public interface IIterator {
  boolean hasnext();
  Object next();
  Object currentInter();
}
```

C. Implementamos iterator

```java
public class EstudianteIterator implements IIterator {
  private Estudiante[] estudiantes;
  private int position;

  @Override
  public boolean hasnext() {
    if(position >= estudiantes.length) { return false; }
    else { return true; }
  }

  public EstudianteIterator(Estudiante[] estudiantes) {
    this.estudiantes = estudiantes;
    this.position = 0;
  }

  @Override public Object next() {
    return this.estudiantes[this.position++];
  }

  @Override public Object currentInter() {
    return this.estudiantes[this.position];
  }
}
```

D. Creamos interface para List

```java
public interface IList {
  IIterator iterator();
}
```

E. Implementamos Lista

```java
public class EstudianteList implements IList {

  private Estudiante[] estudiantes;

  public EstudianteList(Estudiante[] estudiantes) {
    this.estudiantes = estudiantes;
  }

  @Override public IIterator iterator() {
    return new EstudianteIterator(estudiantes);
  }
}
```

F. usamos patron iterator

```java
public static void main(String[] args) {
  Estudiante[] estudiantes = new Estudiante[5];
  estudiantes[0] = new Estudiante("Mario", false);
  estudiantes[1] = new Estudiante("Neey", true);
  estudiantes[2] = new Estudiante("Nona", false);
  estudiantes[3] = new Estudiante("Perritin", true);
  estudiantes[4] = new Estudiante("Sancho", true);

  IList estudiantesList = new EstudianteList(estudiantes);
  IIterator iterator = estudiantesList.iterator();
  while(iterator.hasnext()) {
    Estudiante actual = (Estudiante) iterator.next();
    System.out.println(actual.getNombre());
  }
}
/*SALIDA
Mario
Neey
Nona
Perritin
Sancho
```

**RESULTADO:** recorremos la coleccion de manera transparente para el
cliente

### Ventaja Iterator

Podemos recorrer sin tanto lio, pero java ya implementa asi que mejor
evitar crear el hilo negro.

----

----

<!-- ############################################################# #############################################################-->

----

----

## 12. Mediator

El patron mediator nos permite desacoplar los objetos, agregando una
capa intermedia, para que la interaccion ocurra por medio de esta, en
dicha capa se encapsula el comportamiento en un objeto.

Diagrama.

- `Mediator`: Es una interface para comunicarse con los objetos
- `ConcreteMediator`: Implementa la interface que difine como
  interactuan los objetos entre ellos.
- `Collegue`: Define el comportamiento que debe de impleentar cada
  objeto para poderse cominicar con el mediator.
- `ConcreteColleageN`: Son los objetos que pueden comunicarse entre
  ellos

![14_diagrama_mediator](./img/14_diagrama_mediator.PNG)

----

### Codigo Mediator

**Objetivo:** Vamos a construir un chat en los cuales seran varios
objetos que se podran comunicar entre ellos.

A. Creamos interface del Mediator

```java
public interface IMediator {
  void sendMessage(String mesagge, AbColleage colleage);
}
```

B. Creamos clase abstracta que extenderan los `ConcreteColleage`

```java
public abstract class AbColleage {
  
  protected IMediator mediator;

  protected String nombreUsuario;

  public AbColleage(IMediator mediator, String nombreUsuario) {
    this.mediator = mediator;
    this.nombreUsuario = nombreUsuario;
  }

  public abstract void send(String messageToSend);//metodo para enviar mensaje
  public abstract void messageReceibed(String messageReceibed); //metodo para recibir mensaje

}
```

C. Creamos los `ConcreteColleage`

```java
public class ConcreteCollage1 extends AbColleage {
  public ConcreteCollage1(IMediator mediator, String nombreUsuario) {
    super(mediator, nombreUsuario);
  }

  @Override public void send(String message) {
    mediator.sendMessage(message, this);
  }

  @Override public void messageReceibed(String message) {
    System.out.println(super.nombreUsuario + " recibio: " + message);
  }
}
// ################
public class ConcreteCollage2 extends AbColleage {
  public ConcreteCollage2(IMediator mediator, String nombreUsuario) {
    super(mediator, nombreUsuario);
  }
  @Override public void send(String message) {
    mediator.sendMessage(message, this);
  }
  @Override public void messageReceibed(String message) {
    System.out.println(super.nombreUsuario + " recibio: " + message);
  }
}
```

D. Creamos la implementacion del Mediator, con la logica para que se
comuniquen entre objetos.

```java
public class MediatorImpl implements IMediator {

  private ConcreteCollage1 user1;
  private ConcreteCollage2 user2;
  
  public void setUser1(ConcreteCollage1 user1) { this.user1 = user1; }
  public void setUser2(ConcreteCollage2 user2) { this.user2 = user2; }

  @Override public void sendMessage(String mesagge, AbColleage colleage) {
    // AQUI VA LA LOGICA DE COMO SE COMUNICAN ENTRE OBJETOS, LOS CUALES PODRIAN
    //SE MAS OBJETYOS Y PARAMETROS.
    if (colleage == user1) {
      user2.messageReceibed(mesagge);
    } else if(colleage == user2) {
      user1.messageReceibed(mesagge);
    }
  }
}
```

E. Usuamos el patron Mediator

```java
public static void main(String[] args) {
  MediatorImpl mediator = new MediatorImpl();
  ConcreteCollage1 user1 = new ConcreteCollage1(mediator, "Usuario1");
  ConcreteCollage2 user2 = new ConcreteCollage2(mediator, "Usuario2");
  mediator.setUser1(user1);
  mediator.setUser2(user2);

  user1.send("Hola soy el usuario1");
  user2.send("Que tal usuario1, yo soy usuario2");
}
/* SALIDA ############
Usuario2 recibio: Hola soy el usuario1
Usuario1 recibio: Que tal usuario1, yo soy usuario2
```

**RESULTADO:** No sabemos como se comunican entre el usuario1 y 2 solo
sabemos que se comunican entre ellos, el que sabe como lo hacen es el
Mediator.

### Ventajas Mediator

- Desacomplamos los objetos (ConcreteColleages) entre ellos
- Simplifica las interacciones muchos a muchos, por ejemplo si tenemos
  10 usuarios y entre todos ellos se pueden comunicar
- Los `ConcreteCollage` no deben de conocer con quien se va a comunicar
  solo debe de conocer al `Mediator`

----

----

<!-- ############################################################# #############################################################-->

----

----

## 13. Teoría Memento

Se utiliza para guardar y restaurar el estado de un objeto en un
determinado momento

Diagrama:

- `CateTaker`: Es el responsable de guardar todos los memenotos.
- `Memento`: Es el que contiene la informacion de un objeto que
  queremos que se conserve en el tiempo.
- `Originator`: Es el que crea una fotografia del momento

![15_diagrama_memeto](./img/15_diagrama_memeto.PNG)

----

### Codigo Momento

**Objetivo:** Vamos a imageninar que tenemos un documento que tiene su
contenido, autor, titulo, etc, el cual queremos recuperar estados
anteriores de este.

A. Creamos nuestro objeto que queremos registrar

```java
public class Articulo {
  private String autor;
  private String text;

  public Articulo(String autor, String text) {
    this.autor = autor;
    this.text = text;
  }

  public ArticuloMemento createMemento() {
    ArticuloMemento articuloMemento = new ArticuloMemento(autor, text);
    return articuloMemento;
  }

  public void restoreMemento(ArticuloMemento articuloMemento) {
    this.autor = articuloMemento.getAutor();
    this.text = articuloMemento.getText();
  }

  //  SETTERS/GETTERS
}
```

B. Creamos el Memento que es el objeto con lo que queremos registrar,
abra veces que no querremos guardar todo.

```java
public class ArticuloMemento {
  private String autor;
  private String text;

  public ArticuloMemento(String autor, String text) {
    this.autor = autor;
    this.text = text;
  }

  // GETTERS / SETTERS
}
```

C. Creamos el que registrara todos los mementos.

```java
public class ArticuloCaretaker {
  private List<ArticuloMemento> mementos = new ArrayList<>();

  public void addMemento(ArticuloMemento memento) {
    mementos.add(memento);
  }

  public ArticuloMemento getMemento(int index) {
    return mementos.get(index);
  }

  public List<ArticuloMemento> getMementos() {
    return mementos;
  }
}
```

D. usar el patron memento

```java
public static void main(String[] args) {
  ArticuloCaretaker articuloCaretaker = new ArticuloCaretaker();

  Articulo articulo = new Articulo("Mario M.", "primer cambio,");
  articuloCaretaker.addMemento(articulo.createMemento());

  articulo.setText(articulo.getText() + " SEGUNDO CAMBIO,");
  articuloCaretaker.addMemento(articulo.createMemento());

  articulo.setText(articulo.getText() + " cambio tercero,");
  articuloCaretaker.addMemento(articulo.createMemento());

  articulo.setText(articulo.getText() + " CUARTO CAMBIO,");
  articuloCaretaker.addMemento(articulo.createMemento());

  System.out.println("ESTADO ACTUAL: - " + articulo.getText() + "\n");
  for (int i=articuloCaretaker.getMementos().size()-1; i>=0; i--) {
    articulo.restoreMemento(articuloCaretaker.getMemento(i));
    System.out.println("Ctrl+Z:  " + articulo.getText());
  }
}
/* SALIDA
ESTADO ACTUAL:   -   primer cambio, SEGUNDO CAMBIO, cambio tercero, CUARTO CAMBIO,
Ctrl+Z:  primer cambio, SEGUNDO CAMBIO, cambio tercero, CUARTO CAMBIO,
Ctrl+Z:  primer cambio, SEGUNDO CAMBIO, cambio tercero,
Ctrl+Z:  primer cambio, SEGUNDO CAMBIO,
Ctrl+Z:  primer cambio,
```

**Resultado:** Podemos restaurar cualquer estado anterior del objeto,
siempre y cuando lo ahigamos guardado.

----

### Ventajas Memento

- Podemos restaurar un estado guardado de un objeto sin complicarce
- Podemos descartar propiedades que no queremos

**Desventajas:** Puede ocupar mucha memoria y major trabajo de
mantenimiento, ya que ahi que gestionar tambien estas.

----

----

<!-- ############################################################# #############################################################-->

----

----

## 14. Observer

Este nos permite reaccionar a los cambios de estado en el objeto que
estemos observando

Diagrama

- `Subject`: Es el que que reprecenta un tema que los observadores van
  a observar, por ejemplo, cambio en formulario, seria el tema los
  cuales mas de un objeto estara interezado en los cambios de este.
- `Observer`: Seran los objetos que observaran o mas bien seran
  notificados cuando cambie lo que se esta observando.

![16_diagrama_observer](./img/16_diagrama_observer.PNG)

----

### Codigo Observer

**Objetivo:** Observaremos un semaforo (`Subject`) el cual emitira
estados, por ejemplo cuando este en verde, amarillo o rojo, y habra
nuestros `Observers` en este caso un peaton y un carro, que dependiendo
sel estado del semaforo podran pasar o no.

A. Creamos el objeto que queremos observar, en este caso semaforo

```java
public class Semaforo {
  public static enum Color { ROJO, AMARILLO, VERDE }
  
  public Color status;

  public Semaforo(Color status) {
    this.status = status;
  }

  public Color getStatus() {
    return status;
  }
}
```

B. crear interface que implementaran aquellos que observaran, en este
caso seran Carro y Peaton

```java
public interface IObserver {
  void update(Semaforo semaforo);
}
```

C. Crear la implementacion de Observer, que seran los que observaran
al semaforo

```java
public class Peaton implements IObserver {
  @Override public void update(Semaforo semaforo) {
    if (semaforo.status.equals(Color.ROJO)) {
      System.out.println("EL PEATON AVANZA");
    } else if (semaforo.status.equals(Color.VERDE)) {
      System.out.println("EL PEATON SE DETIENE");
    }
  }  
}
//#########
public class Coche implements IObserver {
  @Override public void update(Semaforo semaforo) {
    if (semaforo.status.equals(Color.ROJO)) {
      System.out.println("carro se detiene");
    } else if (semaforo.status.equals(Color.VERDE)) {
      System.out.println("carro avanza");
    }
  }
  
}

```

D. Creamos el Subject, que sera donde se suscribiran los observadores
y este les notificara del cambio.

```java
public interface ISubject {
  void attach(IObserver observer);
  void dettach(IObserver observer);
  void notifyUpdate(Semaforo observer);
}
```

D. Implementamos el Subject del semaforo, que en este caso el tema
sera, cuando el semaforo cambie.

```java
public class MessagePublisher implements ISubject {

  private List<IObserver> observers = new ArrayList<>();

  @Override public void attach(IObserver observer) {
    observers.add(observer);
  }

  @Override public void dettach(IObserver observer) {
    observers.remove(observer);
  }

  @Override public void notifyUpdate(Semaforo semaforo) {
    for (IObserver o : observers) {
      o.update(semaforo);
    }
  }
}
```

E. usar el patron Observer

```java
public static void main(String[] args) {
  Coche coche = new Coche();
  Peaton peaton = new Peaton();
  MessagePublisher messagePublisher = new MessagePublisher();
  messagePublisher.attach(coche);
  messagePublisher.attach(peaton);

  Semaforo semaforo = new Semaforo(Color.ROJO);
  System.out.println("Semaforo cambia a: " + semaforo.status);
  messagePublisher.notifyUpdate(semaforo);
  try { Thread.sleep(2000); } catch (Exception e) { e.printStackTrace(); }
  semaforo.status = Color.VERDE;
  System.out.println("Semaforo cambia a: " + semaforo.status);
  messagePublisher.notifyUpdate(semaforo);
}
/* SALIDA
Semaforo cambia a: ROJO
carro se detiene
EL PEATON AVANZA
Semaforo cambia a: VERDE
carro avanza
EL PEATON SE DETIENE
```

----

### Ventajas Observer

- Podemos notificar a los observadores que querramos (broadcats notifi),
  incluso agregarlos en tiempo de ejecucion
- Saber si al observador le afecta o no el cambio, es implementado en
  el propio observador, por lo que el decide si le afecta o no.

----

----

<!-- ############################################################# #############################################################-->

----

----

## 15. State

Su objetivo es modificar el comportamiento del objeto, dependiendo en
el estado interno que se encuenta, un problema comun al NO usarlo es
que se crea una cantidad muy grande de if-else.

Diagrama

- `Context`: Este guarda una instancia del estado actual
- `State`: Es una interface para ligar un determinado estado a un
  determinado comportamiento
- `ConcreteState`: Implementa `State` el comportamiento de un estado.

![17_diagrama_state](./img/17_diagrama_state.PNG)

----

### Codigo State

**Objetivo:** Vamos a controlar el comportamiento de una alarma de un
movil, en donde puede ser siliciosa, vibrador o con sonido.

A. Cremos interface para implementar la logica para los diferentes
estados

```java
public interface IMovileAlertState {
  void alert(MovileAlertStateContext context);
}
```

B. Creamos la implementacion de comportamiento para cada estado

```java
public class Silencio implements IMovileAlertState {
  @Override public void alert(MovileAlertStateContext context) {
    // AQUI PONDRIAMOS EL COMPORTAMIENTO DEL ESTADO EN CONCRETO
    System.out.println("---Prender luces solamente---");
  }
}
//###################
public class Sonido implements IMovileAlertState {
  @Override public void alert(MovileAlertStateContext context) {
    System.out.println("...((Sonido))...((Sonido))");
  }
}
//###################
public class Vibration implements IMovileAlertState {
  @Override public void alert(MovileAlertStateContext context) {
    // AQUI PONDRIAMOS EL COMPORTAMIENTO DEL ESTADO EN CONCRETO
    System.out.println("Vibrando...Vibrando...");
  }
}
```

C. Creamos el que el arrancara la logica de acuerdo al esta de la
aplicacion,

```java
public class MovileAlertStateContext {
  private IMovileAlertState currentState;

  public MovileAlertStateContext() {
    currentState = new Sonido();  //DEJAMOS EL COMPORTAMIENTO POR DEFAULT DE SONIDO
  }

  public void setCurrentState(IMovileAlertState currentState) {
    this.currentState = currentState;
  }

  public void alert() {
    currentState.alert(this);
  }
}
```

D. Usar patron State

```java
public static void main(String[] args) {
  MovileAlertStateContext alertStateContext = new MovileAlertStateContext();
  alertStateContext.alert();

  alertStateContext.setCurrentState(new Vibration());
  alertStateContext.alert();

  alertStateContext.setCurrentState(new Silencio());
  alertStateContext.alert();
}/* SALIDA
...((Sonido))...((Sonido))
Vibrando...Vibrando...
---Prender luces solamente---
```

**Resultado:** Podemos implementar diferente logica, de acuerdo a un
estado, en este caso, de acuerdo al estado de sonido, vibrar o silencio
podemos cambiar es estado y por ende su comportamiento de forma facil.

----

----

<!-- ############################################################# #############################################################-->

----

----

## 16. Interpreter

Dado un lenguaje define una reprecentacion para su gramatica, este patron
se utiliza para definir un lenguaje, como reprecentar expreciones
regulares

Diagrama.

- `AbstractExprecion`: Es una interface para una determinada accion
  para una determinada exprecion
- `TerminalExprecion`: Define una operacion para los simbolos
  terminales de la gramatica.
- `NonterminalExprecion`: Define una operacion asociada con los
  simbolos no terminales de la gramatica.
- `Contex`: Contiene el contexto para el interprete
- `Client`: Es el arbol sintactico para la gramatica.

![18_diagrama_interpreter](./img/18_diagrama_interpreter.PNG)

----

### Codigo Interpreter

**Objetivo:** Vamos a crear un interprete donde uno vera si la exprecion
contiene un "1" o "0", y haremos otro que vera si tiene un "1" y un "0",
nos retornara un verdadero si la cumplen o un falso si no.

A. Interface que implementaran las que veran si cumplen o no con la
gramatica.

```java
public interface Exprecion {
  boolean interpret(String context);
}
```

B. Cremos el que implementara las reglas

 ```java
public class TerminalExprecion implements Exprecion {
  private String text;
  public TerminalExprecion(String text) {
    this.text = text;
  }

  @Override public boolean interpret(String context) {
    if(context.contains(text)) {
      return true;
    }
    return false;
  }
  
}
 ```

C. Creamos las reglas de nuestra gramatica

 1. Checara si contiene un "0" o un "1"

 1. Checara si contiene un "0" Y un "1"

```java
public class OrExprecion implements Exprecion {

  private Exprecion exprecion1;
  private Exprecion exprecion2;

  public OrExprecion(Exprecion exprecion1, Exprecion exprecion2) {
    this.exprecion1 = exprecion1;
    this.exprecion2 = exprecion2;
  }

  @Override
  public boolean interpret(String context) {
    return exprecion1.interpret(context) || exprecion2.interpret(context);
  }
}
// ###########
public class AndExprecion implements Exprecion {

  private Exprecion exprecion1;
  private Exprecion exprecion2;

  public AndExprecion(Exprecion exprecion1, Exprecion exprecion2) {
    this.exprecion1 = exprecion1;
    this.exprecion2 = exprecion2;
  }

  @Override
  public boolean interpret(String context) {
    return exprecion1.interpret(context) && exprecion2.interpret(context);
  }  
}
```

D. Usar el patron interpreter

```java
public static void main(String[] args) {
  Exprecion cero = new TerminalExprecion("0");
  Exprecion uno = new TerminalExprecion("1");

  Exprecion contieneOr = new OrExprecion(cero, uno);
  Exprecion contieneAnd = new AndExprecion(cero, uno);

  System.out.println(contieneOr.interpret("algo")); // false
  System.out.println(contieneOr.interpret("a0lgo")); // true

  System.out.println(contieneAnd.interpret("0")); // false
  System.out.println(contieneAnd.interpret("0, a, b, 1, c")); // true
}
```

----

### Ventajas interpreter

Es mas facil implementar un interprete de lenguaje

----

----

<!-- ############################################################# #############################################################-->

----

----

## 17. Strategy

Este nos permite escoger una implementacion especifica en tiempo de
ejecucion para la misma tarea, y estas estrategias van a ser facilmente
reutilizados

Diagrama.

- `Strategy`: Es la interface que implementara las diferentes logicas
  = logicas para una tarea.
- `StrategyN`: Sertan nuestras estrategias = logicas implementadas.
- `Context`: Sera la que optara por una estrategia u otra de acuerdo
  al estado/contexto.

![19_diagrama_strategy](./img/19_diagrama_strategy.PNG)

----

### Codigo Strategy

**Objetivo:** Nuestra tarea sera escribir un texto, y tendremos 2
estrategias diferentes, A) escribir en minus. o B) escribir en mayus.

A. Crear interface que implementaran las diferentes estrategias, en
este caso el de formatear texto

```java
public interface IStrategyTextFormat {
  void format(String text);
}
```

B. Creamos el contexto que hara la tarea con una estrategia u otra.

```java
public class Context {
  private IStrategyTextFormat strategyTextFormat;

  public Context(IStrategyTextFormat strategyTextFormat) {
    this.strategyTextFormat = strategyTextFormat;
  }

  public void publicText(String text) {
    this.strategyTextFormat.format(text);
  }
}
```

B. Creamos las diferentes estrategias para la tarea

```java
public class LowerStrategy implements IStrategyTextFormat{
  @Override public void format(String text) {
    //AQUI PODRIA SER TAN COMPLEJO COMO LO REQUERIMOS
    System.out.println("minus: " + text.toLowerCase());
  }  
}
//###############
public class UpperStrategy implements IStrategyTextFormat{
  @Override public void format(String text) {
    System.out.println("MAYUS: " + text.toUpperCase());
  }
}
```

C. usar Strategy

```java
public static void main(String[] args) {
  IStrategyTextFormat strategy = (false) ? new UpperStrategy() : new LowerStrategy();
  Context context = new Context(strategy);
  context.publicText("TEXTO Con DIFERENTES Estrategias a TOMAR");
}
/*SALIDA: dependiendo del boleano
MAYUS: TEXTO CON DIFERENTES ESTRATEGIAS A TOMAR
minus: texto con diferentes estrategias a tomar
```

----

### Ventajas Strategy

- Nustros algoritmos los tenemos encapsulados por lo que podemos agregar
  algono de una manera sencilla
- La estrategia puede cambiar en tiempo de ejecucion.

Desventajas

- La aplicacion debe de conocer todas las estrategias para seleccionar
  una de ellas

----

----

<!-- ############################################################# #############################################################-->

----

----

## 18. Template Method

Define los pasos secuenciales para ejecutar un algoritmo de varios
pasos, es decir define la escructura del algoritmo en general y los
sub/algoritmos seran implementada por las clases hija.

Diagrama.

- `AbstractClass`: tambien conocido "AbstractTemplate", define el
  esqueleto del algoritmo
- `SubClass`: Sera la implementacion de los algoritmos seguiendo la
  template=plantilla definida.

![20_diagrama_template_method](./img/20_diagrama_template_method.PNG)

----

### Codigo Template Method

**Objetivo:** Vamos a crear una plantilla de para el algoritmo de pago
que lo implementaremos para pagos Visa o PayPal, donde tenemos una serie
de pasos obligatorios, como A) validacion requerimientos B) Realizar pago
C) notificar al usuario, no importa si el pago se realiza con visa o con
Paypal, pero dependiendo con cual se pague, se implementaran estos pasos
de diferente forma.

A. Definimos la plantilla en la clase abstracta.

```java
public abstract class AbstractPayment {
  abstract void preStart();
  abstract void start();
  abstract void finish();

  //es importante que sea final, para que no se modifique
  public final void doPayment() { // podemos tener pasos comunes aqui
    preStart();
    start();
    finish();
  }
}
```

B. Implementamos las foams de pago, que extiendan de `AbstractPayment`

```java
package mario.patrones.comportamiento.TemplateMethod;

public class PaypalPayment extends AbstractPayment {

  @Override void preStart() {
    System.out.println("Verificando datos de entrada Paypal");
  }

  @Override void start() {
    System.out.println("Realizar pago por paypal");
  }

  @Override void finish() {
    System.out.println("Notificar al usuario por Email paypal");
  }
  
}
//#########################
public class VisaPayment extends AbstractPayment {

  @Override void preStart() {
    System.out.println("Viendo que este activo Visa");
  }

  @Override void start() {
    System.out.println("Visa: realiando pago");
  }

  @Override void finish() {
    System.out.println("Enviando notificacion de pago visa");
  }
}
```

X. Uso de patron TemplateMethod

```java
AbstractPayment paymentMng = new VisaPayment();
paymentMng.doPayment();
System.out.println("####################");
paymentMng = new PaypalPayment();
paymentMng.doPayment();
/*
Viendo que este activo Visa
Visa: realiando pago
Enviando SMS de pago visa
####################
Verificando datos de entrada Paypal
Realizar pago por paypal
Notificar al usuario por Email paypal
```

**Resultado:** Estamos implementando un algoritmo de pago donde estamos
obligados a seguir una serie de pasos.

----

### Ventajas Template Method

- Nos facilita la reutilizacion de codigo moviendo los pasos comunes
  a la clase abstracta
- Desacoplamos codigo

----

----

<!-- ############################################################# #############################################################-->

----

----

## 19. Visitor

Nos permite separar la logica de la estructura de un objeto y nos
permite agregar o intercambiar logica dependiendo del visitante sin
tener que modificar la estructura del objeto.

Diagrama

- `Visitor`: Interface que implementaran los posibles visitantes
- `Element`: Interface que decclara la operacion de aceptacion de visita

![21_diagrama_visitor](./img/21_diagrama_visitor.PNG)

----

### Codigo Visitor

Veremos un sistema de compra de Cinemex, el cual dependiendo del tipo
de usuario que este pagando, tendra diferentes tipos de descuentos.

Por ejemplo:

- curioso:
  - 10% de descuento en boletos
  - 15% de descuento en tienda
- regular: 30% de descuento
  - 30% de descuento en boletos
  - 35% de descuento en tienda
- Fan: 50% de descuento
  - 50% de descuento en boletos
  - 55% de descuento en tienda

A. Creamos la interface para las diferentes logicas, en este caso los
diferentes descuentos para los diferentes usuarios.

```java
public interface IOfertaElement {
  void aceptar(IUserVisitor userVisitor);
}
```

B. Creamos la interface para los diferentes visitantes, en este caso
los usuarios de diferentes rangos.

```java
public interface IUserVisitor {
  void descuentoBoletos(OfertaBoletos ofertaBoletos);
  void descuentoTienda(OfertaTienda ofertaTienda);
}
```

C. Creamos los diferentes descuentos, que se aplicaran desde el visitor

```java
public class OfertaBoletos implements IOfertaElement {
  @Override public void aceptar(IUserVisitor userVisitor) {
    userVisitor.descuentoBoletos(this);
  }
}
//###########################
public class OfertaTienda implements IOfertaElement {
  @Override public void aceptar(IUserVisitor userVisitor) {
    userVisitor.descuentoTienda(this);
  }
}
```

D. Creamos los visitantes con la logica de descuento para cada uno de
ellos

```java
public class UserCuriosoImpl implements IUserVisitor {
  @Override public void descuentoBoletos(OfertaBoletos ofertaBoletos) {
    System.out.println("usuario CURIOSO, descuento en BOLETOS 10%");
  }
  @Override public void descuentoTienda(OfertaTienda ofertaTienda) {
    System.out.println("usuario CURIOSO, descuento en TIENDA 15%");
  }
}
//###########################
public class UserRegularImpl implements IUserVisitor {
  @Override public void descuentoBoletos(OfertaBoletos ofertaBoletos) {
    System.out.println("usuario -- REGULAR, descuento en BOLETOS -- 30%");
  }  
  @Override public void descuentoTienda(OfertaTienda ofertaTienda) {
    System.out.println("usuario -- REGULAR, descuento en TIENDA -- 35%");
  }
}
//###########################
public class UserFanImpl implements IUserVisitor {
  @Override public void descuentoBoletos(OfertaBoletos ofertaBoletos) {
    System.out.println("usuario -- -- FAN, descuento en BOLETOS -- -- 50%");
  }
  @Override public void descuentoTienda(OfertaTienda ofertaTienda) {
    System.out.println("usuario -- -- FAN, descuento en TIENDA -- -- 55%");
  }
}
```

E. usar el patron Visitor

```java
public static void main(String[] args) {
  IOfertaElement ofertaBoletos = new OfertaBoletos();
  IOfertaElement ofertaTienda = new OfertaBoletos();
  ofertaBoletos.aceptar(new UserCuriosoImpl());
  ofertaTienda.aceptar(new UserCuriosoImpl());

  ofertaBoletos.aceptar(new UserRegularImpl());
  ofertaTienda.aceptar(new UserRegularImpl());

  ofertaBoletos.aceptar(new UserFanImpl());
  ofertaTienda.aceptar(new UserFanImpl());
}
/* SALIDA
usuario CURIOSO, descuento en BOLETOS 10%
usuario CURIOSO, descuento en TIENDA 15%
usuario -- REGULAR, descuento en BOLETOS -- 30%
usuario -- REGULAR, descuento en TIENDA -- 35%
usuario -- -- FAN, descuento en BOLETOS -- -- 50%
usuario -- -- FAN, descuento en TIENDA -- -- 55%
```

**RESULTADO:** Estamos implementado diferente logica desde el objeto
visitor, por lo que nuestro main no se ve afectado al cambiar la logica,
el o agregar nueva no tenemos que modificar solo agregar nuevas clases.

----

### Ventajas Visitor

- Nos facilita agregar nueva logica a la gerarquia de visitantes sin
  tener que modificar la estructura
- **Desventaja:** al tener muchas implementaciones de vicitor puede
  complicarse.

----

----

<!-- ############################################################# #############################################################-->

----

----

## ==== PATRONES ESTRUCTURALES =====

## Patron Adapter

Este logra que dos interfaces incompatibles entre ellas puedan
trabajar juntas, el cual usa un adaptador para convertir la interface
a otra compatible.

Diagrama

- `Target`: es la interface que espera el cliete
- `Adapter`: Es la que convierte una iterface a otra para adaptarla
- `Adaptee`: Es la implementacion del adapter para adaptar

![22_diagrama_adapter](./img/22_diagrama_adapter.PNG)

----

### Codigo Adapter PENDIENTE-TODO

<!-- PENDIENTE CONFIRMAR TODO: CONFITRMAR -->

**Objetivo:** Tenemos 2 interfaces, la de alarma que define solamente alarma, pero
ocurre que no todos los celulares incluyen solo lampara o bocina por
lo que debemos implementar el bridge para poder llamar alarma correctamente
ya sea que los celulares solo tengan bocina p alarma.

A. Interface para la alarma

```java
public interface IAlarma {
  enum celType { BOCINA, LAMPARA }
  void alarma();
}
```

B. Interface para los celulares

```java
public interface ICelular {
  void sanarBocina();
  void prenderLampara();
}
```

C. Creamos implementaciones de celulares

```java
public class CelularSinBocina implements ICelular {
  @Override
  public void sanarBocina() {
    // NO IMPLEMENTAR
  }

  @Override
  public void prenderLampara() {
    System.out.println("Sonando Bocina");
  }
}

//#####################
public class CelularSinLampara implements ICelular {

  @Override
  public void sanarBocina() {
    System.out.println("Prender lampara");
  }

  @Override
  public void prenderLampara() {
    // NO IMPLEMENTAR
  }
}

```

D. Implementamos alarma donde tendremos el Adapter.

```java
public class AlarmaAdapter implements IAlarma {

  private ICelular celular;
  private celType type;

  public AlarmaAdapter(celType typeIn) {
    this.type = typeIn;
    switch (type) {
      case BOCINA: celular = new CelularSinLampara(); break;
      case LAMPARA: celular = new CelularSinBocina(); break;
    }
  }

  @Override
  public void alarma() {
    switch (type) {
      case BOCINA: celular.sanarBocina(); break;
      case LAMPARA: celular.prenderLampara(); break;
    }
  }
  
}
```

E. Usar Adapter

```java
public static void main(String[] args) {
  new AlarmaAdapter(celType.BOCINA).alarma();
  new AlarmaAdapter(celType.LAMPARA).alarma();
} /* SALIDA
Prender lampara
Sonando Bocina
```

### Ventajas Adapter PENDIENTE

aglo

----

----

<!-- ############################################################# #############################################################-->

----

----

## 21. Bridge

la usamos cuando queremos desacoplar una abstraccion de su implementacion
para que los dos puedan variar independientemente, el cual usamos una
interface que funciona como intermediario=puente.

Diagrama

- `Abstraction`: Es una clase abstracta que mantiene usa a Implementor
- `RefinedAbstraction`: Implementa la abstraccion
- `Implementor`: Interface para definir las diferentes implementaciones.

![23_diagrama_adapter](./img/23_diagrama_adapter.PNG)

----

### Codigo Bridge

**Objetivo:** Tenemos dos formas de hacer un cifrado, uno por clave
personalizable, y otro de cifrado estandart, y al enviar mensaje
debemos de poder intercambiar entre cualquiera de estos.

A. Creamos la interface que implementaran los diferentes tipos de
cifrado

```java
public interface ICifrador {
  void cifrar();
}
```

A.1. Implementamos el cifrado Personalizado

```java
public class CifradoPersonalizado implements ICifrador {
  @Override
  public void cifrar() {
    System.out.println("Cifrado PERSONALIZADO aplicado para enviar mensaje");
  }
}
// #########################
public class CifradoEstandart implements ICifrador {
  @Override public void cifrar() {
    System.out.println("Mensaje enviado con cifrado ESTANDART");
  }
}
```

B. Cremos el **Bridge** que nos permitira enviar el mendaje con un X
tipo de cifrado

```java
public abstract class AbstractMensajero {
  protected ICifrador cifrador;
  public AbstractMensajero(ICifrador cifrador) {
    this.cifrador = cifrador;
  }
  public abstract void enviarMensaje();
}
```

C. Extendemos el Brige para implementar el como se enviara el mensaje
pero el como se cifra este sera hecho por alguien mas.

```java
public class MensajeroCifrador extends AbstractMensajero {

  public MensajeroCifrador(ICifrador cifrador) {
    super(cifrador);
  }

  @Override public void enviarMensaje() {
    cifrador.cifrar();
  }
}
```

D. usar el patron Bridge

```java
public static void main(String[] args) {
  new MensajeroCifrador(new CifradoEstandart()).enviarMensaje();
  new MensajeroCifrador(new CifradoPersonalizado()).enviarMensaje();
} /* SALIDA
Mensaje enviado con cifrado ESTANDART
Cifrado PERSONALIZADO aplicado para enviar mensaje
```

**Resultado:** Podemos cifrar con dos diferentes tipos de cifrado y
podemos intercambiar este ellos facilmente, y tambien podemos agregar
otros tipos de cifrado sin tener que afectar la clase abstracta, por
ejemplo, podriamos modificar la forma de enviar el mensaje en
`enviarMensaje` pero eso no afectara al cifrado o viceversa podremos
cambiar o agregar otro cifrado y no afectaria a `cifrar`.

----

### Ventajas Bridge

Ventajas

- Podemos cambiar la implementacion de la obstraccion sin afectar al
  cliente
- Evitamos enlaces entre una abstraccion y una implementacion

----

----

<!-- ############################################################# #############################################################-->

----

----

## 22. Composite

Lo usamos para crear objetos o algoritomos complejos a partir de objetos
o algoritmos mas simples y que son parecidos entre si.

Diagrama.

- `Component`: es la interface para los objetos simples y compejos
- `Leaf`: define el objeto simple. Importante que este no tenga hijos.
- `Composite`: Define el comportamiento de los objetos compejos, este
  objetos tiene hijos del mismo tipo osea que implementen Component.
  
![24_diagrama_composite](./img/24_diagrama_composite.PNG)

----

### Codigo Ejemplo Composite

**Obejetivo** Tenemos una cuenta (objeto complejo) y cuentas-digitales
(objeto simple), el cual una cuanta esta conformada de varias
cuentas-digitales y apartados, en el cual ambas tienen un comportamiento
similar.

- `cuantas-digitales`: (Obj. Simple) tienen un nombre y total
- `apartados`: (Obj. Simple) tienen un nombre y total
- `cuentas`: (Obj. Complejo) tienen titular y un total (suma de las
  digitales)

A. Creamos la interface quer implementaran los objetos simples

```java
public interface ICuenta {
  int getSaldo();
  void depositar(int saldo);
  String getNombre();
}
```

B. Creamos nuestras leaf

```java
public class LeafCuentaApartado implements ICuenta{
  private int saldo = 5000;
  @Override public int getSaldo() {
    return saldo;
  }

  @Override public void depositar(int deposito) {
    this.saldo += deposito;
  }

  @Override public String getNombre() {
    return "Para-Televisor";
  }
}

// ################## ##################
public class LeafCuentaDigital implements ICuenta{
  private int saldo = 1500;

  @Override public int getSaldo() {
    return saldo;
  }

  @Override public void depositar(int deposito) {
    this.saldo += deposito;
  }

  @Override public String getNombre() {
    return "Para-Internet";
  }
}
```

C. Creamos nuestro objeto Complejo Composite, que contendra una lista
de leafs para crearlo.

```java
public class CompositeCuenta implements ICuenta{
  List<ICuenta> cuentas = new ArrayList<ICuenta>();
  int totalCuenta = 10000;
  @Override public int getSaldo() {
    int total=0;
    for (ICuenta cuenta : cuentas) {
      total += cuenta.getSaldo();
    }
    return total + totalCuenta;
  }
  @Override public void depositar(int saldo) {
    totalCuenta +=  saldo+totalCuenta;
  }
  @Override public String getNombre() {
    String nombre = "Cuenta de Mario";
    for (ICuenta cuenta : cuentas) {
      nombre += "\n\t" + cuenta.getNombre();
    }
    return nombre;
  }
  public void addCuenta(ICuenta cuenta) {
    cuentas.add(cuenta);
  }
  //removeCuenta
  //obtenerCuenta
}
```

D. usar composite

```java
public static void main(String[] args) {
  CompositeCuenta cuentaGeneral = new CompositeCuenta(); //SALDO 10,000
  cuentaGeneral.addCuenta(new LeafCuentaApartado()); //SALDO: 5000
  cuentaGeneral.addCuenta(new LeafCuentaDigital()); //SALDO: 1500

  System.out.println(cuentaGeneral.getNombre());
  System.out.println("Cuenta Saldo: " + cuentaGeneral.getSaldo());
}/*SALIDA
Cuenta de Mario
        Para-Televisor
        Para-Internet
Cuenta Saldo: 16500
```

**Resultado:** Implementamos una solucion para objetos que contienen
obejetos parecidos, el cual es una solucion simple pero poderosa.

----

### Ventajas Composite

- Podemos tratar al coposite y alos leaf de forma igual ya que impl la
  misma interface
- Agregar nuevos Leaf es facil
- **Desventaja:** Los objetos estaran encadenados por lo que es mas
  dificil de depurar.

----

----

<!-- ############################################################# #############################################################-->

----

----

## 23. Decorator

Se usa para agregar comportamientos o caracteristicas adicionales, a
una INSTANCIA particular sin afectar a las demas instancias, lo cual
nos agrega mayor flexibilidad. Osea nos permite agregar mas responsa-
bilidad/caracteristicas a los objetos en tiempo de ejecucion.

El patron tambien es conocido como wrapper

Diagrama.

- `Component`: Es el contenedor o envoltorio=wrapper, que es el que
  tendra el extra en tiempo de ejecucion.
- `ConcreteComponent`: Es el componente original al que se le agregaran
  los extras
- `Decorator`: Es una clase abstracta que contendra/usara una interface
  component y tambien implementa la misma interface.
- `ConcreteDecorator`: Es el que implementara una funcionalidad extra
  que querremos agregar a un objeto en runtime.

![25_diagrama_decorator](./img/25_diagrama_decorator.PNG)

----

### Codigo Decorador

**Objetivo:** Tendremos diferentes niveles y combinaciones de navajas
donde partiremos de la basica, y tendran diferentes extras como es,
navaja con; corta-carne y/o pica-hielo y/o desarma-tuercas

A. Crer interface que implementaran las navajas

```java
public interface INavaja {
  void cuchillos();
  void desarmadores();
}
```

B. Creamos las implementaciones de navaja

```java
public class NavajaBasica implements INavaja {
  @Override public void cuchillos() {
    System.out.println("Cuchillos: liso y cierra");
  }
  @Override public void desarmadores() {
    System.out.println("Desarmadores: cruz y plano");
  }
}
// ##########################
public class NavajaCampo implements INavaja {
  @Override public void cuchillos() {
    System.out.println("Cuchillos: cierra grueza, cierra delgada");
  }
  @Override public void desarmadores() {
    System.out.println("Desarmadores: cruz-G, plano-G");
  }
}
```

C. Creamos el **Decorador** que permitra agregar nueva funcionalidad,
haciendo que estas exteindan esta clase abstracta.

```java
public abstract class AbstractNavajaDecorator implements INavaja {
  protected INavaja navaja;
  // LA AGREGAMOS YA QUE NECESITAMOS UN NUEVO METODO, NO QUE EXTIENDA UN METODO QUE YA EXISTE
  public static interface IPicarHielo {
    void picarhielo();
  }
  public AbstractNavajaDecorator(INavaja navaja) {
    this.navaja = navaja;
  }
  @Override public void cuchillos() {
    navaja.cuchillos();
  }
  @Override public void desarmadores() {
    navaja.desarmadores();
  }
}
```

D. Creamos las implementaciones de decoradores

```java
public class CortaCarne extends AbstractNavajaDecorator {
  public CortaCarne(INavaja navaja) {
    super(navaja);
  }
  @Override
  public void cuchillos() {
    navaja.cuchillos();
    System.out.println("\t\t\t y cortar-carne");
  }
}
// ##########################
public class DesarmaTuercas extends AbstractNavajaDecorator {
  public DesarmaTuercas(INavaja navaja) {
    super(navaja);
  }
  @Override public void desarmadores() {
    navaja.desarmadores();
    this.desarmarTuercas();
  }
  private void desarmarTuercas() {
    System.out.println("\t\t\t y desarma-tuercas-ajustable");
  }
}
// ##########################
public class PicaHielo extends AbstractNavajaDecorator implements IPicarHielo {

  public PicaHielo(INavaja navaja) {
    super(navaja);
  }

  public void picarhielo() {
    System.out.println("Picos: pica-hielo");
  }  
}
```

E. Usar patron decorator

```java
public static void main(String[] args) {
  INavaja navajaBasica = new NavajaBasica();
  System.out.println("==== Navaja Basica Original ====");
  navajaBasica.cuchillos();
  navajaBasica.desarmadores();
  System.out.println("==== Navaja Basica EXTRAS ====");
  navajaBasica = new CortaCarne(navajaBasica);
  navajaBasica.cuchillos();

  System.out.println("\n ==== Navaja CAMPO Original ====");
  INavaja navajaCampo = new NavajaCampo();
  navajaCampo.cuchillos();
  navajaCampo.desarmadores();
  System.out.println("==== Navaja CAMPO CON EXTRAS ====");
  navajaCampo = new DesarmaTuercas(navajaCampo);
  navajaCampo.desarmadores();
  navajaCampo = new PicaHielo(navajaCampo);
  ((IPicarHielo)navajaCampo).picarhielo();
}/** SALIDA
==== Navaja Basica Original ====
Cuchillos: liso y cierra
Desarmadores: cruz y plano
==== Navaja Basica EXTRAS ====
Cuchillos: liso y cierra
                         y cortar-carne

 ==== Navaja CAMPO Original ====
Cuchillos: cierra grueza, cierra delgada
Desarmadores: cruz-G, plano-G
==== Navaja CAMPO CON EXTRAS ====
Desarmadores: cruz-G, plano-G
                         y desarma-tuercas-ajustable
Picos: pica-hielo
```

**Resultado:** Creamos objetos con una funcionalidad y caracteristicas
por default, y en lugar de agregar nueva funcionalidad en la clase
agregamos esta funcionalidad solo en algunas instancias.

En este caso tenemos una navaja-basica y navaja-campo y a cualquera de
estas podemos agregar una o mas de cualquiera de los siguietes extras
que son, pica-hielos, corta-carne y desarma-tuercas en runtime.

----

### Ventajas Decorator

- Podemos agregar diferentes combinaciones de funcionalidad/caracteristicas
- Es mas sensillo que utilizar herencia
- **Desventaja>** Creamos mas codigo.

----

----

<!-- 

-

############################################################# #############################################################

-

-->

----

----

## 24. Facade

Busca simplificar el sistema para el cliente, precentando una interface
unidficada, para un subconjunto de subsistema definiendo una interface
de alto nivel, lo usamos cuando tenemos un sistema complejo y queremos
darsela al cliente de forma simplificada.

![26_diagrama_facade](./img/26_diagrama_facade.PNG)

----

### Codigo Facade

**Objetivo:** Tenemos una empresa con 3 diferentes fabricas, cada fabrica
es un systema complejo pero desde un solo punto queremos controlar toda
la empresa, queremos evitar que el cliente maneje cosas complicadas.

A. Creamos la interface para cada fabrica

```java
public interface IFabrica {
  void implementar(String str);
}
```

B. Creamos las implementaciones de fabrica

```java
public class FabricaJuguetes implements IFabrica {
  @Override public void implementar(String str) {
    System.out.println("Se crea utileria UTILERIA");
  }
}
//##########
public class FabricaMateriaPrima implements IFabrica {
  @Override public void implementar(String str) {
    System.out.println("SE HABILITAN 5 UNIDADES DE MATERIA");
  }
}
//##########
public class FabricaUtileria implements IFabrica {
  @Override public void implementar(String str) {
    System.out.println("Se unien componentes para crear UTILERIA");
  }
}
```

C. Creamos el **Facade** donde utilizaremos cada fabrica, simplificando
las tareas para el cliente.

```java
public class FabricaFacade {
  
  private IFabrica habilitacion, armadoJugueteria, armadoUtileria;

  private String resultado;
  private int material=0;

  public FabricaFacade() {
    habilitacion = new FabricaMateriaPrima();
    armadoJugueteria = new FabricaJuguetes();
    armadoUtileria = new FabricaUtileria();
    resultado = "Entran materias primas";
  }

  public void realizarHabilitado() {
    habilitacion.implementar(resultado); material += 5;
    System.out.println("MATERIA DISPONIBLE: " + material);
  }

  public void realizaArmadoJuguetes() {
    if(material<=0) {
      System.out.println("NO HAY MATERIA PRIMA");
    } else {
      armadoJugueteria.implementar(resultado); material -= 5;
    }
  }

  public void realizaArmadoUtileria() {
    if(material<=0) {
      System.out.println("NO HAY MATERIA PRIMA");
    } else {
      armadoUtileria.implementar(resultado); material -= 2;
    }
  }
}
```

D. Usar patron Facade

```java
FabricaFacade fabricaFacade = new FabricaFacade();
fabricaFacade.realizarHabilitado();
fabricaFacade.realizaArmadoJuguetes();
fabricaFacade.realizaArmadoUtileria();//nos quedamos sin materia
fabricaFacade.realizarHabilitado();
fabricaFacade.realizaArmadoUtileria();
/*fabricaFacade.realizarPublisidad();
SALIDA
SE HABILITAN 5 UNIDADES DE MATERIA
MATERIA DISPONIBLE: 5
Se crea utileria UTILERIA
NO HAY MATERIA PRIMA
SE HABILITAN 5 UNIDADES DE MATERIA
MATERIA DISPONIBLE: 5
Se unien componentes para crear UTILERIA
```

**Resultado:**: no es necesario solo una interface, lo que se reprecenta
es que atras tenemos 3 systemas complejos (fabricas) que deben de trabajar
juntas O NO por ejemplo en el caso de PUBLICIDAD, pero queremos que
nuestro clientetienga el control mas facil desde un punto.

----

### Ventajas Facade

- No estamos tratando de hacer mas simple/mantenible el codigo si no
  que estamos ocultando la dificultad al cliente, por lo que es este
  el que se beneficia.

----

----

<!-- 

-

############################################################# #############################################################

-

-->

----

----

## 25. Flyweight

Permite el uso de objetos compartidos para soportar una gran cantidad
de objetos que tienen la misma informacion de manera eficiente, este
es un objeto copartido actuando independiente en cada contexto.

Diagrama.

- `Flyweight`: Es una interface por la cual los objetos podran actuar
  sobre los estados no copartidos
- `Flyweight1`: Podemos tener N numero estos que implementan la
  interface y almacena los datos que se comparten
- `FlyweightUnshared`: Parecido al anterior solo que para los que NO
  comparten datos.
- `FlyweightFactory`: Crea y gestiona los objetos Flyweight

![27_diagrama_flyweight](./img/27_diagrama_flyweight.PNG)

----

### Codigo Flyweight

**Objetivo:** Tenemos un juego en el cual vamos a crear muchos enemigos
estos son parecidos entre si, y estos son un problema de memoria por
lo que queremos, **Escenario** Solo nos importa tener un tipo de
enemigo, ya que todos los tanques y anti-tanques son iguales.

**Importante:** La idea es crear solo un objeto con la misma informacion
por ejemplo, si nuestros tanques varian en su ataque, por ejemplo entre
1 a 4, significa que tendriamos que crear 4 tanque, y si creamos 40
tanques 10 de cada tipo de ataque, entonces usariamos la misma instancia
para mostrar 10 tanques ya que tendriamos 4 instancias.

**Ejemplo Ilistrativo**: Para dejarlo claro otro ejemplo seria que vamos
a crear una lista de 500 computadoras dell, pero vemos que en la
informacion (ram, procesador) solo tenemos un maximo de 20 combinaciones
diferentes, entonces implementariasmos este patron pàra realmente crear
solo estas 20 veriantes en memoria, y de algun modo mostrarle las 500
computadoras al usuario.

A. Creamos la interface que implementaran nuestros enemigos.

```java
public interface IEnemy {
  int getAttack();
}
```

B. Cremos las implementaciones de nuestros enemigos

```java
public class Tank implements IEnemy {

  private int attack;

  public Tank(int attack) {
    this.attack = attack;
  }

  @Override
  public int getAttack() {
    return attack;
  }
}
//#######
public class AntiTank implements IEnemy {

  private int attack;

  public AntiTank(int attack) {
    this.attack = attack;
  }

  @Override
  public int getAttack() {
    return attack;
  }
}
```

C. Cremos de donde vamos a crear los tanques y asegurara de no crear
mas de un mismo, mismisimo tanque

```java
public class EnemyFactory {
  public static  enum EnemyType { TANK, ANTI_TANK }
  private static Random random = new Random();

  public static int antitanques = 0;
  public static int tanques = 0;

  public Map<EnemyType, IEnemy> enemies = new HashMap<>();

  public IEnemy getEnemy(EnemyType type) {
    IEnemy enemy = null;
/*
PARA CREAR LOS TANQUES CON MAYOR DIFERENCIA QUE EL TIPO, POR EJEMPLO
EN LA CANTIDAD DE ATAQUE, VELOCIDAD, RECISTENCIA ETC, MODIFICARIAMOS
ESTE CONDICIONAL, PARA SABER SI YA EXISTE UNO IGUAL EN LA LISTA, SI
AHI UNO IGUAL REGRESAMOS EL QUE SEA IGUAL, SI NO CREAMOS UNO NUEVO.
*/
    if(enemies.containsKey(type)) {
      enemy = enemies.get(type);
    } else {
      switch (type) {
        case TANK:
          //enemy = new Tank(random.nextInt(1)+1);
          enemy = new Tank(5);
        break;
        case ANTI_TANK:
          //enemy = new AntiTank(random.nextInt(3)+1);
          enemy = new AntiTank(10);
        break;
      }
      System.out.println("Se crea un: " + type);
      enemies.put(type, enemy);
    }
    switch (type) {
      case TANK: tanques++; break;
      case ANTI_TANK: antitanques++; break;
    }
    return enemy;
  }

  public static EnemyType randomEnum(){
    int x = random.nextInt(EnemyType.class.getEnumConstants().length);
    return EnemyType.class.getEnumConstants()[x];
  }
}
```

D. Usar el patron flyweight

```java
public class Flyweight {
  public static void main(String[] args) {
    EnemyFactory enemyFactory = new EnemyFactory();
    for (int i=0; i<25; i++) {
      EnemyType type = EnemyFactory.randomEnum();
      IEnemy enemy = enemyFactory.getEnemy(type);
      //System.out.println("Referencia: " + enemy.toString().substring(enemy.toString().lastIndexOf(".")+1) );
      System.out.println(type + " Ataque: " + enemy.getAttack());
    }
    System.out.println("Total Tanques: " + EnemyFactory.tanques);
    System.out.println("Total anti-Tanques: " + EnemyFactory.antitanques);
  }
}/* SALIDA
Se crea un: TANK
TANK Ataque: 5
Se crea un: ANTI_TANK
ANTI_TANK Ataque: 10
TANK Ataque: 5
...
ANTI_TANK Ataque: 10
...
Total Tanques: 13
Total anti-Tanques: 12
```

**Resultado:** Estamos evitando crear objetos que tengan la misma
informacion y por lo tanto ahorrandonos memoria.

### Ventajas Flyweight

Estamos reduciendo el uso de memoria y evitar la redundancia de datos

----

----

<!-- 

-

############################################################# #############################################################

-

-->

----

----

## 26. Proxy

Se utiliza como intermediario para utilizar un objeto lo cual
controlamos el acceso a dicho objeto, el patron se divide en 4 grupos,

1. **Remoto**, Este reprecenta un objeto que esta remoto osea que este
   objeto se accede a el por medio de la red y es el objeto proxy el que
   hace esta comunicacion y el cliente lo usa como si fuera local.
2. **Virtual**: Crea objetos costosos por encargo, por ejemplo lazyload.
3. **Proteccion**: Asegura el acceso a un objeto
4. **Smart**: Suela encargarse de hacer tareas de limpieza adicionales
   cuando el cliente accede a un objeto

Diagrama.

- `Subject`: Es la interface que implementaran el proxy y el realSubject
- `Proxy`: Delegara las tareas al RealSubject.
- `RealSubject`: Es el objetos que hara lo que el cliente quiere pero
  sera accedido por el proxy.

![28_diagrama_proxy](./img/28_diagrama_proxy.PNG)

### Codigo Proxy

**Objetivo:** Crearemos un objeto para conextarnos a una URL, pero por
medio de un proxy indicaremos si se puede o no conectar a esta.

A. Creamos interface que implementara nuestro RealSubject=Conecion y
nuestro proxy

```java
public interface IAccessInternet {
  void accessTo(String url);
}
```

B. Implementamos nuestro objeto de conexion

```java
class AccessInternet implements IAccessInternet {
  @Override
  public void accessTo(String url) {
    System.out.println("Accedemos a: " + url);
  }
}
```

C. Implementamos nuestro proxy mas la tarea del pry en este caso bloquer
las paginas no permitidas.

```java
public class AccessInternetProxy implements IAccessInternet {
  private static List<String> bannedUrls = new ArrayList<>();
  static {
    bannedUrls.add("facebook.com");
    bannedUrls.add("twiter.com");
    bannedUrls.add("xxx.com");
  }

  @Override
  public void accessTo(String url) {
    if(bannedUrls.contains(url)) {
      System.out.println("Acceso no permitido a> " + url);
    } else {
      AccessInternet accessInternet = new AccessInternet();
      accessInternet.accessTo(url);
    }
  }
}
```

D. Usar proxy

```java
public static void main(String[] args) {
  AccessInternetProxy accessInternetProxy = new AccessInternetProxy();
  accessInternetProxy.accessTo("trabajo.com");
  accessInternetProxy.accessTo("facebook.com");
  accessInternetProxy.accessTo("correo.com");
  accessInternetProxy.accessTo("xxx.com");
}
```

### Ventajas Proxy

- Evitamos la creacion de objetos que hagan lo mismo
- Solucion sencilla a enfoques proxy
