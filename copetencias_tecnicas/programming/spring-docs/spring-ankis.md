# Spring Ankis

- [Spring Ankis](#spring-ankis)
  - [Errores](#errores)
    - [Al iniciar](#al-iniciar)
      - [java.security.PrivilegedActionException: null](#javasecurityprivilegedactionexception-null)
  - [Web](#web)
    - [MVC](#mvc)
      - [Como ver los logs de peticiones](#como-ver-los-logs-de-peticiones)
    - [Templates-Thymeleaf](#templates-thymeleaf)
      - [Da error 404](#da-error-404)
  - [Spring Security](#spring-security)
    - [Errores en Spring Security](#errores-en-spring-security)
      - [No usa UserDetailsService personalizado expuesto como Bean](#no-usa-userdetailsservice-personalizado-expuesto-como-bean)
    - [How to handle AccessDeniedException in Spring Security](#how-to-handle-accessdeniedexception-in-spring-security)
    - [Filtros](#filtros)
      - [Que necesitamos para agregar un filtro al filterchain de spring security](#que-necesitamos-para-agregar-un-filtro-al-filterchain-de-spring-security)
      - [Que es y para que es OncePerRequestFilter](#que-es-y-para-que-es-onceperrequestfilter)
      - [Como implementar el login usando Filter](#como-implementar-el-login-usando-filter)
    - [CSRF](#csrf)
      - [Que es y para que sirve](#que-es-y-para-que-sirve)
      - [Donde se guarda](#donde-se-guarda)
      - [Como podemos obtenerla](#como-podemos-obtenerla)
    - [OAuth2](#oauth2)
      - [Que componentes conforman y que rol tiene cada uno](#que-componentes-conforman-y-que-rol-tiene-cada-uno)
      - [Analogía](#analogía)
      - [Que es un grant type y cuantos hay](#que-es-un-grant-type-y-cuantos-hay)
        - [grant\_type: password](#grant_type-password)
        - [grant\_type: Client credentials](#grant_type-client-credentials)
        - [grant\_type: refresh token](#grant_type-refresh-token)
      - [Como levantar un servidor de autorización: grant\_type password](#como-levantar-un-servidor-de-autorización-grant_type-password)
      - [Como levantar un servidor de autorización: grant\_type authorization code](#como-levantar-un-servidor-de-autorización-grant_type-authorization-code)
      - [Servidor de recursos](#servidor-de-recursos)
        - [Como valida la autorización el servidor de recursos](#como-valida-la-autorización-el-servidor-de-recursos)
        - [Porque actualizar las llaves publica-privada con mayor frecuencia](#porque-actualizar-las-llaves-publica-privada-con-mayor-frecuencia)
        - [Como actualizar fácilmente la llave publica en el servidor de recursos](#como-actualizar-fácilmente-la-llave-publica-en-el-servidor-de-recursos)
      - [Como levantar un authServer y resServer con OAuth2 con JWS con llaves asimétricas](#como-levantar-un-authserver-y-resserver-con-oauth2-con-jws-con-llaves-asimétricas)
        - [Servidor de Auth](#servidor-de-auth)
        - [Servidor de Recursos](#servidor-de-recursos-1)
        - [Agregar campos personalizados al cuerpo del token](#agregar-campos-personalizados-al-cuerpo-del-token)
        - [Leer campo personalizado en el resource server](#leer-campo-personalizado-en-el-resource-server)
    - [Global Method security: pre and post authorization](#global-method-security-pre-and-post-authorization)
      - [Que es y como funciona Global Method security](#que-es-y-como-funciona-global-method-security)
      - [Que pre y post authorization](#que-pre-y-post-authorization)
      - [Como activar Global Method security](#como-activar-global-method-security)
      - [Ejemplo de pre authorization](#ejemplo-de-pre-authorization)
      - [Ejemplo de post authorization](#ejemplo-de-post-authorization)
      - [Crear SpEL hasPermission personalizado](#crear-spel-haspermission-personalizado)
    - [Global Method security: Filtering pre\&post](#global-method-security-filtering-prepost)
      - [Que hace el filtering y su diferencia con authorization](#que-hace-el-filtering-y-su-diferencia-con-authorization)
      - [Que hace el Post y Pre filtering](#que-hace-el-post-y-pre-filtering)
      - [Ejemplo de implementación de Post y Pre filtering](#ejemplo-de-implementación-de-post-y-pre-filtering)
      - [Como incluir el usuario autenticado dentro de SQL](#como-incluir-el-usuario-autenticado-dentro-de-sql)
    - [Testing Spring Security](#testing-spring-security)
      - [Crear un test básico de spring security](#crear-un-test-básico-de-spring-security)
      - [Crear un tests para Global Method Security](#crear-un-tests-para-global-method-security)
      - [Crear un tests de autenticación](#crear-un-tests-de-autenticación)
      - [Crear un tests con un CSRF requerido](#crear-un-tests-con-un-csrf-requerido)
      - [Errores en Spring Test](#errores-en-spring-test)
        - [httpMediaTypeNotAcceptableException: Could not find acceptable representation](#httpmediatypenotacceptableexception-could-not-find-acceptable-representation)

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Errores

### Al iniciar

#### java.security.PrivilegedActionException: null

```log
2023-06-09 15:31:34.756 ERROR 62213 --- [           main] c.s.x.b.v2.runtime.reflect.opt.Injector  : null
java.security.PrivilegedActionException: null
  at java.base/java.security.AccessController.doPrivileged(AccessController.java:573) ~[na:na]
  ...
  Caused by: java.lang.NoSuchMethodException: sun.misc.Unsafe.defineClass(java.lang.String,[B,int,int,java.lang.ClassLoader,java.security.ProtectionDomain)
  ...
  Caused by: org.springframework.beans.BeanInstantiationException: Failed to instantiate [javax.servlet.Filter]: Factory method 'springSecurityFilterChain' threw exception; nested exception is java.lang.NullPointerException: Cannot invoke "java.lang.reflect.Method.invoke(Object, Object[])" because "com.sun.xml.bind.v2.runtime.reflect.opt.Injector.defineClass" is null
        at org.springframework.beans.factory.support.SimpleInstantiationStrategy.instantiate(SimpleInstantiationStrategy.java:185) ~[spring-beans-5.3.27.jar:5.3.27]
        at org.springframework.beans.factory.support.ConstructorResolver.instantiate(ConstructorResolver.java:653) ~[spring-beans-5.3.27.jar:5.3.27]
  
```

**Descripción:** Al querer arrancar/levantar el servidor marca este error.

**Fuente:** [link](https://stackoverflow.com/questions/72534992/java-security-privilegedactionexception-null)

**Causa**: JAXB was marked for deprecation in Java 9 and removed in Java11.
If the dependencies are not being provided by other libraries (transitive)
you have in your project then on Java 17 they cannot be found. That is
why you have to add them as direct dependency, **resumen**: conflicto de
codigo que usa java8 con codigo de java11 o adelante, ya que en el inter
quitaron dependencias por default como esta.

**Solución:** Agregar las sig. dependencias de forma manual.

```xml
<dependency>
    <groupId>com.sun.xml.bind</groupId>
    <artifactId>jaxb-impl</artifactId>
    <version>2.3.1</version>
</dependency>
<dependency>
    <groupId>com.sun.xml.messaging.saaj</groupId>
    <artifactId>saaj-impl</artifactId>
    <version>1.5.1</version>
</dependency>
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Web

### MVC

#### Como ver los logs de peticiones

logging.level.org.springframework.web=DEBUG

<!--

#########################################

-->

----

<!--

#########################################

-->

### Templates-Thymeleaf

Por defecto va a buscar en la ruta `auth/src/main/resources/templates/xxx.html`

Es decir que para acceder al archivo xxx.html deberíamos crear el
controlador de la sig, manera

```java
@GetMapping(value="/xxx")
public String showLogin() {
    return "xxx.html";
}
```

Podemos configurar las rutas de la sig. manera, en este caso sustituimos
el directorio templates por other_path

```conf
spring.thymeleaf.prefix=classpath:/other_path/
spring.thymeleaf.suffix=.html
```

#### Da error 404

1, para poder entender que esta pasando mostramos los log web

```conf
logging.level.org.springframework.web=DEBUG
```

2, asegurar que tanto el controlador y el .html están bien ubicados

3, Asegurar que la compilación si copio los archivos .html a target en
`auth/target/classes/templates/myLoginPage.html`,ya que en mi caso
vscode a veces me hace esto.

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Spring Security

### Errores en Spring Security

#### No usa UserDetailsService personalizado expuesto como Bean

**Descripción:** Cree mi UserDetailsService pero no se asigno de forma
automática, y cuando intento un login no se usa el método `loadUserByUsername()`
que yo definí.
**Causa**: No se

```java
@Configuration
@EnableWebSecurity
public class SecurityConfig {

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    http
      .authorizeHttpRequests((authz) -> authz .antMatchers(publicPaths).permitAll() .anyRequest().authenticated() )
      .formLogin(f -> f .defaultSuccessUrl("/private", true) .loginPage("/login") .permitAll() )
      .logout(l -> l.logoutUrl("/logout").logoutSuccessUrl("/login?logout").permitAll()) ;
    return http.build();
  }

  @Bean
  public UserDetailsService userDetailsService() {
    return new UserService();
  }
}
```

**Solución:** En mi caso asigne mi UserDetailsService de forma explicita.

```java
  import org.springframework.context.annotation.Bean;import org.springframework.context.annotation.Configuration;import org.springframework.security.authentication.dao.DaoAuthenticationProvider;import org.springframework.security.config.annotation.web.builders.HttpSecurity;import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;import org.springframework.security.web.SecurityFilterChain;import org.springframework.security.crypto.password.NoOpPasswordEncoder;import mariocorp.secuno.users.UserService;

  @Configuration
  @EnableWebSecurity
  public class SecurityConfig {

    @Autowired
    private UserService userService;
    
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
      http
        .authorizeHttpRequests((authz) -> authz .antMatchers(publicPaths).permitAll() .anyRequest().authenticated() )
        .formLogin(f -> f.defaultSuccessUrl("/private", true).loginPage("/login").permitAll())
        .logout(l ->l.logoutUrl("/logout").logoutSuccessUrl("/login?logout").permitAll())
//Definimos nuestro authenticationProvider de forma explicita/manual
+       .authenticationProvider(authenticationProvider())
      ;
      return http.build();
    }

    public DaoAuthenticationProvider authenticationProvider() {
      DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
//aqui agregamos nuestro UserDetailsService de forma explicita
+     authProvider.setUserDetailsService(userService);
      authProvider.setPasswordEncoder(NoOpPasswordEncoder.getInstance());
      return authProvider;
    }

  }
```
<!--

#########################################

-->

----

<!--

#########################################

-->

<!--

#########################################

-->

----

<!--

#########################################

-->

### How to handle AccessDeniedException in Spring Security

[source](https://stackoverflow.com/questions/8742842/how-to-handle-accessdeniedexception-in-spring-security)

1, Crear una clase que implemente org.springframework.security.web.access.AccessDeniedHandler

```java
  import java.io.IOException; import javax.servlet.ServletException; import javax.servlet.http.HttpServletRequest; import javax.servlet.http.HttpServletResponse; import org.springframework.security.access.AccessDeniedException;import org.springframework.security.web.access.AccessDeniedHandler;

  public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
      AccessDeniedException accessDeniedException) throws IOException, ServletException {
      response.sendRedirect(request.getContextPath() + "/accessDenied");
    }
    
  }
```

2, Agregarla a la configuración de spring

```java
//configuracion de seguridad por bean, usar bean para configurar seguridad
// uso de beans para configurar seguridad de rutas
//configurar seguridad usando beans, configurar con beans la seguridad
  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    http
      .authorizeHttpRequests((authorizeRequests) -> ...)
+       .exceptionHandling(exHandler -> 
          exHandler.accessDeniedHandler(new CustomAccessDeniedHandler())
        )
        .formLogin(login -> ...)
        
      ;

    return http.build();
  }
```

### Filtros

#### Que necesitamos para agregar un filtro al filterchain de spring security

1, Implementar la interface `javax.servlet.Filter`

```java
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RequestValidationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)  throws IOException, ServletException {
        var httpRequest = (HttpServletRequest) request;
        var httpResponse = (HttpServletResponse) response;
        String requestId = httpRequest.getHeader("Request-Id");
        if (requestId == null || requestId.isBlank()) {
            httpResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        filterChain.doFilter(request, response);
    }
}
```

2, Agregarlo con

```java
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
public class ProjectConfig extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.addFilterBefore(
            new RequestValidationFilter(),
            BasicAuthenticationFilter.class)
        .addFilterAfter(
            new AuthenticationLoggingFilter(),
            BasicAuthenticationFilter.class)
        .authorizeRequests()
            .anyRequest()
                .permitAll();
}
```

#### Que es y para que es OncePerRequestFilter

Es una clase abstracta que implementa `javax.servlet.Filter` pero spring no garantiza que estos se ejecuten una sola vez, por lo que si usamos esta ahora si se garantiza

#### Como implementar el login usando Filter

```java
package auth.security;import java.io.IOException;import javax.servlet.FilterChain;import javax.servlet.ServletException;import javax.servlet.http.HttpServletRequest;import javax.servlet.http.HttpServletResponse;import org.slf4j.Logger;import org.slf4j.LoggerFactory;import org.springframework.beans.factory.annotation.Autowired;import org.springframework.http.HttpMethod;import org.springframework.security.authentication.AuthenticationManager;import org.springframework.security.authentication.ProviderManager;import org.springframework.security.authentication.ProviderManager;import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;import org.springframework.security.authentication.dao.DaoAuthenticationProvider;import org.springframework.security.core.Authentication;import org.springframework.security.core.AuthenticationException;import org.springframework.security.core.context.SecurityContextHolder;import org.springframework.security.core.userdetails.UserDetailsService;import org.springframework.security.crypto.password.PasswordEncoder;import org.springframework.stereotype.Component;import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class LoginFilter extends OncePerRequestFilter {

  Logger logger = LoggerFactory.getLogger(LoginFilter.class);
  
  @Autowired
  UserDetailsService userService;

  @Autowired
  PasswordEncoder passEncoder;

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    var username = (String) request.getParameter("username");
    var password = (String) request.getParameter("password");
    if (username == null || username.isBlank() || (password == null || password.isBlank())) {
      response.setStatus(400);
      response.setHeader("cause", "username and password required");
      return ;
    }
    Authentication a = new UsernamePasswordAuthenticationToken(username, password);
    var authMan = new DaoAuthenticationProvider();
    authMan.setUserDetailsService(userService);
    authMan.setPasswordEncoder(passEncoder);
    
    try {
      authMan.authenticate(a);
    } catch (AuthenticationException e) {
      var msg = e.getLocalizedMessage();
      response.sendRedirect("/auth?error=" + msg);
      return ;
    }

    var context = SecurityContextHolder.getContext();
    context.setAuthentication(a);
    response.sendRedirect("/private");
  }
  
  //Solo se ejecuta este filtro cuando la petición es POST:/auth
  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) {
    var isAuthPath = request.getServletPath().equals("/auth");
    var isPostMethod = request.getMethod().equals(HttpMethod.POST.name());
    var skipFilter = (isAuthPath && isPostMethod) == false;
    return skipFilter;
  }

}

```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### CSRF

#### Que es y para que sirve

Es una clave que el servidor genera al hacer una petición GET y le da al usuario,
y luego se la pide si el usuario quiere hacer una petición que cambie el
estado del servidor (POST, PUT, DELETE ...), y si esta no esta o es incorrecta
entonces rechaza la petición.

Sirve para proteger este ataque popular, el cual consiste en que si el
atacante inyecta codigo, entonces este codigo puede ejecutarse en nombre
del usuario registrado.

#### Donde se guarda

Usa CsrfTokenRepository que por default las guarda en variables de session,
pero podemos cambiarlo para que use otra BD.

#### Como podemos obtenerla

Este filtro debe de estar después de que se ejecute el filtro `CsrfFilter`

```java
public class CsrfTokenLogger implements Filter {
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
    Object o = request.getAttribute("_csrf");
    CsrfToken token = (CsrfToken) o;
    logger.info("CSRF token " + token.getToken());
    filterChain.doFilter(request, response);
  }
}
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### OAuth2

#### Que componentes conforman y que rol tiene cada uno

Son ; componentes~servidores; 1)Autenticación, 2)Recursos, 3)Cliente

1)Autenticación: Es el que a traves de las credenciales de usuarios
genera un token, el puede usar el servidor de recursos para autenticar
y validar al usuario.

2)Recursos: Es el que resguarda los recursos del usuario, y la forma de
proteger los recursos del usuario, es a traves de la llave que le proporcione
el servidor de Autenticación.

3)Cliente: Este administra el usuario para que le proporcione las credenciales
al servidor de recursos y lego guarde el token~lleve, y luego la use para
enviarlo al servidor de recursos.

#### Analogía

<!--(#wknqwi7#)-->

Entro a una librería a ver libros, le gusta un libro asi que lo escojo y
lo pago, me dicen que me lo pueden entregar mañana, llega el dia pero
no puedo ir asi que le pido a un amigo que vaya por el.
El amigo llega a la recepción y dice que es Mike amigo mio y que va por mi libro, la
recepcionista me llama por teléfono y me dice que si mande a un amigo Mike
por mi libro, le contesto que es correcto, la recepcionista sabiendo de
mi confirmación le da un boleto, mi amigo entra a la librería a recoger
mi libro para que pase por mi libro.

En este caso tenemos los siguientes actores como.

- Yo como el usuario
- La librería: Como el servidor de recursos
- Mi amigo como el cliente (UI)
- La recepcionista como el servidor de autenticación.

Y tenemos las acciones se pueden interpretar como

- Mi amigo pide mi libro: El cliente le pide al servidor de autenticación
  que le de acceso a los recursos protegidos.
- La recepcionista me llama: El usuario es redirigido al servidor de autenticación
  para preguntarme directamente si quiero que pase dicha acción.
- Le digo que si: Le doy mis credenciales
- La recepcionista se entera de mi confirmación: El servidor de autenticación
  valida las credenciales y asi sabe que soy yo.
- La recepcionista da un boleto: El servidor de auth le entrega el token
  al cliente.
- Mi amigo entra a la librería a recoger el libro: el cliente usando el token
  puede acceder a la acción protegida.

#### Que es un grant type y cuantos hay

Cuando solicitamos un token debemos de indicar que tipo de concesión es,
por default llamado `grant_type`, el cual puede ser de 3 tipos.

- password
- Authentication code
- Refresh token
- Client credentials

##### grant_type: password

Este lo usamos cuando podemos obtener usar el cliente (UI) para enviar
las credenciales, esto es cuando la arquitectura de OAuth2 es implementada
por el mismo fabricante, por lo cual existe la confianza que el cliente
no tiene malas intenciones sobre las credenciales del usuario.

**Analogía**: basándonos en la analogía (#wknqwi7#), la diferencia es que
yo le entrego mi tarjeta de identificación (username y password) a mi amigo,
para que la recepcionista lo deje recoger mi libro.

##### grant_type: Client credentials

Este se usa para autenticar al cliente (UI) no al usuario, este se usa
por ejemplo, para obtener los libros del servidor de recursos, solo para
evitar que clientes externos usen nuestros recursos.

##### grant_type: refresh token

Un token de acceso debe de tener una vida limitada, porque si tiene una
vida eterna entonces el token es tan poderoso como las mismas credenciales
(usuario y contraseña), y como se usan en cada petición aquí podríamos
tener un debilidad de seguridad grave.

Por lo tanto el grant_type:refresh_token sirve para obtener un nuevo token

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Como levantar un servidor de autorización: grant_type password

Recordar que con este obtenemos el token de forma directa, lo que requiere
que el cliente sea confiable, ya que podrá ver las credenciales del usuario.

1, Agregamos las dependencias en el pom.xml

- Necesitamos definir la version de spring-boot, el cual también necesita el
  dependencyManagement

- spring-cloud-starter-oauth2: Este contiene el servidor de auth

- spring-boot-starter-security: Necesario para proteger el propio servidor

- Importante tener la en cuenta la spring-cloud.version

```xml
<?xml version="1.0" encoding="UTF-8"?>
  <project ...>
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.0.RELEASE</version>
        <relativePath/>
    </parent>
    <groupId>com.laurentiuspilca</groupId>
    <artifactId>ssia-ch13-ex1-mio</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>ssia-ch13-ex1-mio</name>
    <description>Demo project for Spring Boot</description>

    <properties>
        <java.version>11</java.version>
        <spring-cloud.version>Hoxton.SR1</spring-cloud.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-oauth2</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-impl</artifactId>
            <version>2.3.1</version>
        </dependency>
        <dependency>
            <groupId>com.sun.xml.messaging.saaj</groupId>
            <artifactId>saaj-impl</artifactId>
            <version>1.5.1</version>
        </dependency>

    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>Hoxton.SR1</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

</project>

```

2, Configuramos la seguridad del propio servidor como también agregamos
usuarios de prueba, lo mas común seria tomar estos de BD, pero para rápido
usamos un UserDetailService en memoria

```java
import org.springframework.context.annotation.Bean;import org.springframework.context.annotation.Configuration;import org.springframework.security.authentication.AuthenticationManager;import org.springframework.security.config.annotation.web.builders.HttpSecurity;import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;import org.springframework.security.core.userdetails.User;import org.springframework.security.core.userdetails.UserDetailsService;import org.springframework.security.crypto.password.NoOpPasswordEncoder;import org.springframework.security.crypto.password.PasswordEncoder;import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@SuppressWarnings("deprecation")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .authorizeRequests((requests) -> requests
        .antMatchers("/public").permitAll()
        .anyRequest().authenticated())
      
      .formLogin((form) -> form
        .permitAll()
      )
      
      .logout((logout) -> logout.permitAll())
    ;
  }

  @Bean
  public UserDetailsService uds() {
    var uds = new InMemoryUserDetailsManager();

    //la pass es obligatoria
    var u = User.withUsername("mario1")
        .password("mario1p")
        .authorities("read")
        .build();

    uds.createUser(u);

    return uds;
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return NoOpPasswordEncoder.getInstance();
  }

  @Override
  @Bean
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

}
```

3, Agregamos la configuración servidor de autenticación

```java
import org.springframework.beans.factory.annotation.Autowired;import org.springframework.context.annotation.Configuration;import org.springframework.security.authentication.AuthenticationManager;import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
    endpoints.authenticationManager(authenticationManager);
  }

/* Lo mismo pero mas largo
  @Override
  public void configure( ClientDetailsServiceConfigurer clients) throws Exception {
    var cd = new BaseClientDetails();
    cd.setClientId("myClient1");
    cd.setClientSecret("myClient1");
    cd.setScope(List.of("read"));
    cd.setAuthorizedGrantTypes(List.of("password"));

    var service = new InMemoryClientDetailsService();
    service.setClientDetailsStore(Map.of("client", cd));
    clients.withClientDetails(service);
  }
*/

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.inMemory()
      .withClient("app1")
      .secret("app1p")
      .scopes("read")
      .authorizedGrantTypes("password")
      //.authorizedGrantTypes("authorization_code")
      //.redirectUris("http://localhost:9090/home")
    ;
      
  }

}
```

4, Ya podremos obtener nuestro token, tener en cuenta que las credenciales
de cliente y de usuario son diferentes

```r
curl -v -X POST -u app1:app1p "http://localhost:8080/oauth/token?grant_type=password&username=mario1&password=mario1p&scope=read" | json_pp

Response
{
  "access_token":"9e66a7cf-34ee-4745-9ef7-165b6440addf",
  "token_type":"bearer",
  "expires_in":43199,
  "scope":"read"
}

```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Como levantar un servidor de autorización: grant_type authorization code

Recordar que este el usuario le envía directamente las credenciales al
servidor de autorización, y luego re-dirige con un codigo al cliente u
otro servidor, de esta forma las credenciales solo se quedan dentro
del servidor de auth, y el cliente con el codigo de autorización (único y de uso único)
puede crearle un token al usuario.

1, La única diferencia con `servidor de autorización: grant_type password`
es en la configuración del servidor de auth, en este caso definimos el
grant_type y la url de re-dirección.

```java
//igual que la clase de arriba
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.inMemory()
      .withClient("app1")
      .secret("app1p")
      .scopes("read")
      //.authorizedGrantTypes("password")
      .authorizedGrantTypes("authorization_code")
      .redirectUris("http://localhost:9090/home")
    ;
      
  }
```

2, Podemos hacer una petición en el navegador al servidor de auth

```r
http://localhost:8080/oauth/authorize?response_type=code&client_id=app1&scope=read
```

![100_auth_server_grant_type_auth_code](./img/100_auth_server_grant_type_auth_code.png)

Podremos ver como al usar la url de arriba, nos redirigirá al login,
luego de login exitoso nos pedirá confirmar los scopes que se piden, y al confirmarlos
se re-direccióna a la url que definimos mas un codigo el cual se puede
generar el token.

3, Ejemplo: Simulando la petición que haría el cliente seria la siguiente,
podemos ver como nos da el token.

```r
curl -v -X POST -u app1:app1p \
  -u app1:app1p \
  "http://localhost:8080/oauth/token?grant_type=authorization_code&scope=read&code=Cl1elJ"

response
{
  "access_token":"54bab5b5-4e4c-4e6d-8d9b-70afb38af4ab",
  "token_type":"bearer",
  "expires_in":43062,
  "scope":"read"
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Servidor de recursos

##### Como valida la autorización el servidor de recursos

1,Le hace una petición directamente al servidor de authorization

- En este caso para que el resSer haga una petición al authSer, es recomendable
  que tenga que autenticarse, en este caso un httpBasic seria suficiente.

2,blackBoarding: Esta técnica es cuando el servidor de auth y de res
comparten una BD en común.

3,Por criptografía comúnmente llave publica y privada.

##### Porque actualizar las llaves publica-privada con mayor frecuencia

El robo de llave puede ocurrir, asi que cambiar cada tanto imposibilitamos
al hacker por haber haberlo logrado, y tal vez la proxima vez que lo intente
no tendrá la misma suerte.

##### Como actualizar fácilmente la llave publica en el servidor de recursos

Hacer que el servidor de autenticación lo exponga por medio de un endpoint,
y Spring security ya tiene una forma pre-implementada de lograr esto, el cual
el servidor de auth ya lo expone por default, solo hay que permitir el
acceso por medio de credenciales de cliente, y en el servidor de recursos
ya lo busca, solo hay que definir la url en donde lo pondrá encontrar.

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

#### Como levantar un authServer y resServer con OAuth2 con JWS con llaves asimétricas

**La diferencie entre llaves simétricas y asimétricas**, es que
las llaves simétricas es como una contraseña, que se usa tanto para firmar
como validar, mientras que la asimétrica esta confirmada de dos partes,
una para firmar y otra para validar, lo cual esta ultima es mas segura.

##### Servidor de Auth

1,Generar llaves

```r
#Generar llave privada
keytool -genkeypair -alias priv1 -keyalg RSA -keypass priv1p -keystore priv1.jks -storepass priv1p

#Generar llave public
#Si vamos a obtener la desde endpoint del auth server no es necesario
keytool -list -rfc --keystore priv1.jks | openssl x509 -inform pem -pubkey
```

2,Dependencias

```xml
<xml ...>
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.7.12</version>
    <relativePath/>
  </parent>

  <groupId>extAuth.res</groupId>
  <artifactId>res</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <name>res</name>
  <description>Demo project for Spring Boot</description>
  <properties>
    <java.version>17</java.version>
    <spring-cloud.version>2022.0.3</spring-cloud.version>
  </properties>
  

  <dependencies>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter</artifactId>
    </dependency>
    
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-oauth2</artifactId>
        <version>2.2.5.RELEASE</version>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-security</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
    </dependency>

    <dependency>
        <groupId>com.sun.xml.bind</groupId>
        <artifactId>jaxb-impl</artifactId>
        <version>2.3.1</version>
    </dependency>
    
    <dependency>
        <groupId>com.sun.xml.messaging.saaj</groupId>
        <artifactId>saaj-impl</artifactId>
        <version>1.5.1</version>
    </dependency>

  </dependencies>


  <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

  <build>
    <plugins>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </build>

</project>
```

3,Incluir la ruta de la llave privada en .properties

```conf
#En mi caso estoy usando spring viejo con spring cloud nuevo
spring.cloud.compatibility-verifier.enabled=false

privateKey=/media/m51/Docs/trabajos/back_spring2_sec_oauth1/secrets/priv1.key
alias=priv1
password=priv1p
```

4, Configurar servidor de Auth

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {

    @Value("${password}")
    private String password;

    @Value("${privateKey}")
    private String privateKey;

    @Value("${alias}")
    private String alias;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("client")
                .secret("secret")
                .authorizedGrantTypes("password", "refresh_token")
                .scopes("read")
                .and()
//agregamos otro cliente el cual representara peticiones entre servidores, en este caso para que
//el resServer use este cliente para acceder a la llave publica
                .withClient("resourceServer")
                .secret("resourceServerSecret");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
                .authenticationManager(authenticationManager)
                .tokenStore(tokenStore())
                .accessTokenConverter(jwtAccessTokenConverter());
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        var file = new FileSystemResource(privateKey);
        var key = new KeyStoreKeyFactory(file, password.toCharArray());

        var converter = new JwtAccessTokenConverter();
        converter.setKeyPair(key.getKeyPair(alias));
        return converter;
    }

//Por defecto /oauth/token_key rechaza todas las peticiones, y con esto indicamos
//que las peticiones autenticadas, si las permita
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security.tokenKeyAccess("isAuthenticated()");
    }

}
```

5, Configurar spring security agregando usuario

```java
package extAuth.res.security;import org.springframework.context.annotation.Bean;import org.springframework.context.annotation.Configuration;import org.springframework.security.authentication.AuthenticationManager;import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;import org.springframework.security.core.userdetails.User;import org.springframework.security.core.userdetails.UserDetailsService;import org.springframework.security.crypto.password.NoOpPasswordEncoder;import org.springframework.security.crypto.password.PasswordEncoder;import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@SuppressWarnings("deprecation")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService uds() {
        var uds = new InMemoryUserDetailsManager();

        //la pass es obligatoria
        var u = User.withUsername("john")
                .password("12345")
                .authorities("read")
                .build();

        uds.createUser(u);

        return uds;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
```

5,Ya deberíamos de poder obtener nuestro token

```r
curl -v -X POST -u client:secret \
"http://localhost:8080/oauth/token?grant_type=password&username=john&password=12345&scope=read"

Response
{
  "access_token":"eyJ...",
  "token_type":"bearer",
  "refresh_token":"R4C...",
  "expires_in":43199
}

#----
#Cuidado: poner bien los parámetros
curl -v -X POST -u client:secret \
"http://localhost:8080/oauth/token?grant_type=password&username=john&pass=12345&scope=read"
{"error":"invalid_grant","error_description":"Bad credentials"}
```

6,COMPROBAR: que podemos acceder a la llave publica

```r
curl -u resourceServer:resourceServerSecret http://localhost:8080/oauth/token_key

{
   "alg" : "SHA256withRSA",
   "value" : "-----BEGIN PUBLIC KEY-----\nmix...AQx\n-----END PUBLIC KEY-----"
}
```

##### Servidor de Recursos

1, Dependencias

- spring-cloud.version
- spring-boot-starter-oauth2-resource-server
- spring-cloud-starter-oauth2
- spring-cloud-dependencies

```xml
<spring-cloud.version>2022.0.3</spring-cloud.version>

...
<?xml version="1.0" encoding="UTF-8"?>
<project ...>
  <modelVersion>4.0.0</modelVersion>
  
  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.7.12</version>
    <relativePath/>
  </parent>

  <groupId>extAuth</groupId>
  <artifactId>res</artifactId>
  <version>0.0.1</version>
  <name>res</name>
  <description>Demo project for Spring Boot</description>

  <properties>
    <java.version>17</java.version>
    <spring-cloud.version>2022.0.3</spring-cloud.version>
  </properties>

  <dependencies>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-oauth2-resource-server</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-oauth2</artifactId>
      <version>2.2.5.RELEASE</version>
    </dependency>

    <dependency>
      <groupId>com.sun.xml.bind</groupId>
      <artifactId>jaxb-impl</artifactId>
      <version>2.3.1</version>
    </dependency>
    
    <dependency>
      <groupId>com.sun.xml.messaging.saaj</groupId>
      <artifactId>saaj-impl</artifactId>
      <version>1.5.1</version>
    </dependency>

  </dependencies>

  <dependencyManagement>
      <dependencies>
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-dependencies</artifactId>
              <version>${spring-cloud.version}</version>
              <type>pom</type>
              <scope>import</scope>
          </dependency>
      </dependencies>
  </dependencyManagement>

  <build>
    <plugins>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </build>

</project>
...

<dependencyManagement>
  <dependencies>
    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-dependencies</artifactId>
      <version>${spring-cloud.version}</version>
      <type>pom</type>
      <scope>import</scope>
    </dependency>
  </dependencies>
</dependencyManagement>
```

2, Agregamos la ruta donde obtendrá la llave publica y las credenciales

```conf
server.port=8081

spring.cloud.compatibility-verifier.enabled=false

security.oauth2.resource.jwt.key-uri=http://localhost:8080/oauth/token_key
security.oauth2.client.client-id=resourceServer
security.oauth2.client.client-secret=resourceServerSecret
```

3, Configuramos el resServer

- En este caso por default configura el JwtStore<-JwtAccessTokenConverter
- de forma automática, gracias a que indicamos donde obtendrá las llaves

```java
package extAuth.res.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
  
}
```

4, Obtenemos un JWS del servidor de autenticación y lo podremos usar
para acceder de forma segura al Servidor de Recursos

```r
curl -v -X POST -u client:secret \
"http://localhost:8080/oauth/token?grant_type=password&username=john&password=12345&scope=read" | json_pp

curl -H "Authorization:Bearer xxx" http://localhost:8081/private
```

##### Agregar campos personalizados al cuerpo del token

1, Creamos un TokenEnhancer

```java
package extAuth.res.security;

import java.time.ZoneId;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

public class CustomTokenEnhancer implements TokenEnhancer {

  @Override
  public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
    var token = new DefaultOAuth2AccessToken(oAuth2AccessToken);
    Map<String, Object> info = Map.of("MySysZone", ZoneId.systemDefault().toString());
    token.setAdditionalInformation(info);
    return token;
  }

}
```

2,Lo agregamos a la conf del servidor de auth

```java
@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {

  @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        //add custom fields to body token
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        var tokenEnhancers = List.of(new CustomTokenEnhancer(), jwtAccessTokenConverter());
        tokenEnhancerChain.setTokenEnhancers(tokenEnhancers);

        endpoints
                ...
                .tokenEnhancer(tokenEnhancerChain)
                ...
    }
```

3,Podemos obtener un token, tomar el cuerpo del token (parte de en medio)
y comprobar que este campo adicional personalizado se encuentre

```r
curl -v -X POST -u client:secret "http://localhost:8080/oauth/token?grant_type=password&username=john&password=12345&scope=read" | json_pp

echo -n ey...J9 | base64 --decode | json_pp

Resultado
{
   "MySysZone" : "America/Mexico_City",
   "authorities" : [
      "read"
   ],
   "client_id" : "client",
   "exp" : 1686662327,
   "jti" : "bee16d58-7696-4985-8452-a2b0a7ef097c",
   "scope" : [
      "read"
   ],
   "user_name" : "john"
}
```

##### Leer campo personalizado en el resource server

1, Creamos un JwtAccessTokenConverter, el cual usaremos un decode por defecto (uso de map string-object)

```java
import java.util.Map;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

public class AdditionalClaimsAccessTokenConverter extends JwtAccessTokenConverter {
  
  @Override
  public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
    var authentication = super.extractAuthentication(map);
    authentication.setDetails(map);
    return authentication;
  }
}
```

2, Lo agriemos a nuestra conf de ResServ

- Notar que como uso el JwtAccessTokenConverter construido en automático,
  lo que hago es, ya que este configurado sobre escribir el AccessTokenConverter,
  ya que si creo uno nuevo, en este caso tendría que hacer la captura de
  certificado publico por url de forma manual.

```java
import org.springframework.beans.factory.annotation.Autowired;import org.springframework.beans.factory.annotation.Value;import org.springframework.context.annotation.Bean;import org.springframework.context.annotation.Configuration;import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;import org.springframework.security.oauth2.jwt.JwtDecoder;import org.springframework.security.oauth2.provider.token.AccessTokenConverter;import org.springframework.security.oauth2.provider.token.TokenStore;import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

  @Autowired
  private JwtAccessTokenConverter a;
  
  @Override
  public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
    super.configure(resources);
    a.setAccessTokenConverter(new AdditionalClaimsAccessTokenConverter());
  }

}
```

3,Podemos ver todos lo detalles de la sig. forma, notar que tenemos nuestro
campo personalizado, y recordar que necesitamos un jws de authServer

```java
@GetMapping("/private")
public String hello(OAuth2Authentication authentication) {
  OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
  var a = details.getDecodedDetails();
  return "private hello: " + a;
}
//{MySysZone=America/Mexico_City, user_name=john, scope=[read], exp=1686665206, authorities=[read], 
//jti=894afc77-2b94-4c88-af7e-c1de1d6b45a9, client_id=client, }
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Global Method security: pre and post authorization

#### Que es y como funciona Global Method security

Podemos asegurar los propios métodos de nuestros beans, lo cual nos da
un control mucho mayor que asegurar los endpoints.

Funciona: por medio de AOP, el cual Spring en lugar de llamar a un método
directamente, por detrás pone un método en medio, y este intermedio aplica
las reglas que le definamos, si las cumple prosigue, si no las cumple lanza
una excepción.

#### Que pre y post authorization

Pre: se refiere a las condiciones para que un método pueda ser llamado,

Post: Se refiere a las condiciones de acuerdo al objeto que retorna el método

- CUIDADO: La post-auth ejecuta el método y luego verifica el filtro
  por lo que si lo que ejecuta el método muta un estado, (por ejemplo, actualiza la BD)
  entonces aunque falle este cambio es aplicado.

#### Como activar Global Method security

Con la anotacion EnableGlobalMethodSecurity, la cual nos permite activar
3 aproximaciones, 1)prePostEnabled, 2)@RolesAllowed y 3)@Secured.

El mas usado es prePostEnabled, asi que estudiaremos mas esta aprox.

```java
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ProjectConfig { }
```

#### Ejemplo de pre authorization

En este caso el username del usuario autenticado y el nombre del argumento
del método `name` deben de coincidir.

```java
@Service
public class NameService {
  private Map<String, List<String>> secretNames = Map.of(
    "Natalie", List.of("Enérgico", "Perfecto"),
    "Emma", List.of("Fantástico")
  );

  @PreAuthorize("#name == authentication.principal.username")
  public List<String> getSecretNames(String name) {
    return secretNames.get(name);
  }
}
```

#### Ejemplo de post authorization

En este caso se retornara una lista de documentos, y el PostAuthorize
buscara en esta lista que los libros tengan el rol reader, de caso contrario
lanzara una excepción.

CUIDADO: El método se ejecuta, y si esta ejecución cambia un estado, aunque
lanze la excepción este cambio de estado de aplicara.

```java
@PostAuthorize("returnObject.roles.contains('reader')")
public Employee getBookDetails(String name) {
  return records.get(name);
}
```

#### Crear SpEL hasPermission personalizado

Para esto debemos de implementar la interface `org.springframework.security.access.PermissionEvaluator`,
luego registrarlo usando la herencia de GlobalMethodSecurityConfiguration,

por ejemplo.

```java
public class Document {
    private String owner;
    ...
}

//En ambos casos serve para lo mismo, 
@Service
public class DocumentService {
  @Autowired private DocumentRepository documentRepository;

  @PostAuthorize("hasPermission(returnObject, 'ROLE_admin')")
  public Document getDocument(String code) {
      return documentRepository.findDocument(code);
  }

  // o

  @PreAuthorize("hasPermission(#code, 'document', 'ROLE_admin')")
  public Document getDocument(String code) {
      return documentRepository.findDocument(code);
  }
}

//registramos
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ProjectConfig extends GlobalMethodSecurityConfiguration {

    @Autowired
    private DocumentsPermissionEvaluator evaluator;

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        var expressionHandler = new DefaultMethodSecurityExpressionHandler();
        expressionHandler.setPermissionEvaluator(evaluator);
        return expressionHandler;
    }
}

//definimos, si no necesitamos las dos implementaciones podemos regresar false o lanzar un no implementado
@Component
public class DocumentsPermissionEvaluator implements PermissionEvaluator {

  @Override
  public boolean hasPermission(Authentication authentication, Object target, Object permission) {
    Document document = (Document) target;
    String p = (String) permission;

    boolean admin = authentication.getAuthorities().stream().anyMatch(
      a -> a.getAuthority().equals(p)
    );
    boolean owner = document.getOwner().equals(authentication.getName());

    return admin || owner;
  }

  @Override
  public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
    String code = targetId.toString();
    Document document = documentRepository.findDocument(code);
    String p = (String) permission;

    boolean admin = authentication.getAuthorities().stream().anyMatch(
      a -> a.getAuthority().equals(p))
    ;
    boolean owner = document.getOwner().equals(authentication.getName());

    return admin || owner;
  }

}
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Global Method security: Filtering pre&post

#### Que hace el filtering y su diferencia con authorization

Authorization rechaza o detiene la llamada al método. Filtering vigila
que los parámetros de entrada y salida sigan determinados reglas, por
ejemplo, para evitar que un usuario no autorizado introduzca ciertos
datos, o que reciba solo la parte de los datos que esta autorizado a ver.

Es decir, si los parametros no siguen dichas reglas no se lanza un excepcion,
solo estas se filtran, evitando que entren o salgan.

CUIDADO: El filtro solo aplca las regla sobre arreglos o coleciones, y
usa la misma instancia del argumento, por lo que, si le pasamos una lista
inmutable, el filtro no poda modificarla y lanzara una excepcion.

#### Que hace el Post y Pre filtering

Pre: Filtra los paramentos antes de llamar al método

Post: Filtra después de la llamada, pudiendo filtrar el valor regresados.
implementación

#### Ejemplo de implementación de Post y Pre filtering

1, Debemos de tener la dependencia de spring security y activar el GlobalMethodSecurity

```java
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ProjectConfig {
```

2, Aplicamos el pre-filtering

```java
  @Service
  public class ProductService {
    
//En este caso solo entraran los productos que cumplan el filtro
//NO-OLVIDAR: que se usa AOP el cual de forma transparente se ejecuta
//un método intermedio, para aplicar el filtro.
    @PreFilter("filterObject.owner == authentication.name")
    public List<Product> sellProducts(List<Product> products) {
      return products;
    }
}



  @RestController
  public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/sell")
    public List<Product> sellProduct() {
      List<Product> products = new ArrayList<>();
      products.add(new Product("beer", "nikolai"));
      products.add(new Product("candy", "nikolai"));
      products.add(new Product("chocolate", "julien"));
  //se pasa todos, pero el productService.sellProducts(), solo resivira los
  //productos que pasen el filtro
      return productService.sellProducts(products);
    }
  }

/*Resultado:

  curl -u nikolai:12345 http://localhost:8080/sell

    Response
      [
        {"name":"beer","owner":"nikolai"},
        {"name":"candy","owner":"nikolai"}
      ]


  curl -u julien:12345 http://localhost:8080/sell

    Response
      [
        {"name":"chocolate","owner":"julien"}
      ]
*/
```

3, Aplicamos el post-filtering

```java
@Service
public class ProductService {

  @PostFilter("filterObject.owner == authentication.name")
  public List<Product> findProducts() {
    List<Product> products = new ArrayList<>();
    products.add(new Product("beer", "nikolai"));
    products.add(new Product("candy", "nikolai"));
    products.add(new Product("chocolate", "julien"));
//El filtro se aplicara sobre la coleccion o arreglo retornado
    return products;
  }
}

//===================================
@RestController
public class ProductController {

  @Autowired
  private ProductService productService;

  @GetMapping("/find")
  public List<Product> findProducts() {
    return productService.findProducts();
  }
}

/*
curl -u julien:12345 http://localhost:8080/find
  Response
    [
      {"name":"chocolate","owner":"julien"}
    ]

curl -u nikolai:12345 http://localhost:8080/find
  Response
    [
      {"name":"beer","owner":"nikolai"},
      {"name":"candy","owner":"nikolai"}
    ]
*/
```

#### Como incluir el usuario autenticado dentro de SQL

Podemos usar un bean el cual indica que vamos a hacer esto, en este caso
necesitamos spring-security y spring-data-jpa.

1, Activamos el method global security

```java
  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true)
  public class ProjectConfig {

//Creamos el bean el cual activara esta funcionalidad
    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
      return new SecurityEvaluationContextExtension();
    }

  }

//-------------------------

  public interface ProductRepository extends JpaRepository<Product, Integer> {

//#{authentication.name} es nuestra SpEL el cual tomara por nosotros el usuario del SpringSecurityContext
    @Query("SELECT p FROM Product p WHERE p.name LIKE %:text% AND p.owner=?#{authentication.name}")
    List<Product> findProductByNameContains(String text);

  }
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Testing Spring Security

#### Crear un test básico de spring security

1, Instalar dependencias

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-test</artifactId>
  <scope>test</scope>
  <exclusions>
    <exclusion>
    <groupId>org.junit.vintage</groupId>
    <artifactId>junit-vintage-engine</artifactId>
    </exclusion>
  </exclusions>
</dependency>

<dependency>
  <groupId>org.springframework.security</groupId>
  <artifactId>spring-security-test</artifactId>
  <scope>test</scope>
</dependency>
```

2, Crear un test unitario

```java
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

  @SpringBootTest
  @AutoConfigureMockMvc
  public class MainTests {
    
    @Autowired
    private MockMvc mvc;


    @Test
    public void helloUnauthenticated() throws Exception {
      mvc.perform(get("/hello"))
        .andExpect(status().isUnauthorized())
      ;
    }



//creamos un usuario mock (1) por default, o con nombre mary
    //(1) @WithMockUser
    @Test
    @WithMockUser(username = "mary")
    public void helloAuthenticated() throws Exception {
      mvc.perform(get("/hello"))
        .andExpect(content().string("Hello!"))
        .andExpect(status().isOk())
      ;
    }

    @Test
    public void helloAuthenticatedWithUser() throws Exception {
      mvc.perform(
        get("/hello")
//Otra forma (con mismo resultado) de crear un usuario mock
          .with(user("mary")))
        .andExpect(content().string("Hello!"))
        .andExpect(status().isOk())
      ;
    }

//usamos el Bean UserDetails que hallamos creado, y cargamos el usuario "john"
//el cual si usamos una implementación JDBC también se usara esta
    @Test
    @WithUserDetails("john")
    public void helloAuthenticated() throws Exception {
      mvc.perform(get("/hello"))
        .andExpect(status().isOk())
      ;
    }



//Tambien podemos crear un objeto Authentication personalizado, para
//eso ver el libro cap 20.3

  }
```

#### Crear un tests para Global Method Security

Para esto usamos la misma aproximación, pero en lugar de llamar un endpoint,
llamamos al método directamente, y este tomara el usuario del spring context,
al igual que los tests a un endpoint

```java
@SpringBootTest
class MainTests {
  
  @Autowired
  private NameService nameService;

  @Test
  void testNameServiceWithNoUser() {
    assertThrows(
      AuthenticationException.class,
      () -> nameService.getName()
    );
  }

  @Test
  @WithMockUser(authorities = "read")
  void testNameServiceWithUserButWrongAuthority() {
    assertThrows(
      AccessDeniedException.class,
      () -> nameService.getName()
    );
  }

  @Test
  @WithMockUser(authorities = "write")
  void testNameServiceWithUserButCorrectAuthority() {
    var result = nameService.getName();
    assertEquals("Fantástico", result);
  }

}
```

#### Crear un tests de autenticación

```java
@SpringBootTest
@AutoConfigureMockMvc
public class MainTests {
  
  @Autowired
  private MockMvc mvc;

//login fallido
  @Test
  public void loggingInWithWrongUser() throws Exception {
    mvc.perform(formLogin()
      .user("joey").password("12345"))
      .andExpect(header().exists("failed"))
      .andExpect(unauthenticated())
      //.andExpect(redirectedUrl("/error")) //si esperamos un redirect
    ;
  }

//login exitoso
  @Test
  public void loggingInWithCorrectAuthority() throws Exception {
    mvc.perform(formLogin().user("bill").password("12345"))
      .andExpect(status().isFound())
      .andExpect(authenticated())
      //.andExpect(redirectedUrl("/home")) //si esperamos un redirect
    ;
  }

}
```

#### Crear un tests con un CSRF requerido

```java
@SpringBootTest
@AutoConfigureMockMvc
public class MainTests {
  @Autowired
  private MockMvc mvc;

  @Test
  public void testHelloPOST() throws Exception {
    mvc.perform(post("/hello"))
      .andExpect(status().isForbidden())
    ;
  }

  @Test
  public void testHelloPOSTWithCSRF() throws Exception {
    mvc.perform(post("/hello").with(csrf()))
      .andExpect(status().isOk())
    ;
  }
}
```

#### Errores en Spring Test

##### httpMediaTypeNotAcceptableException: Could not find acceptable representation

log

```log
DefaultHandlerExceptionResolver : Resolved [org.springframework.web.HttpMediaTypeNotAcceptableException: Could not find acceptable representation]
```

Descripción: Al ejecutar un test me daba este error, algo como un problema
con el encoding como, no se puede transformar a json, o que no se habia
declarado que tipo de respuesta se queria, y daba un error 406 (Client browser doesn't accept the MIME type of the requested page.).

Causa: Configuracion spring incompleta, cundo usanmos `@SpringBootTest(classes = {...})`,
carga una configuracion minima.

Solucion: Incluir la configuracion web a nuestro test con `@EnableWebMvc`

Ejemplo: Si comentamos y descomentamos `@EnableWebMvc` veremos como
aparece/desaparece este error.

```java
import org.junit.jupiter.api.Test;import org.springframework.beans.factory.annotation.Autowired;import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;import org.springframework.boot.test.context.SpringBootTest;import org.springframework.http.MediaType;import org.springframework.test.context.ActiveProfiles;import org.springframework.test.web.servlet.MockMvc;import pan51.posts.security.ResourceServerConfig;import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;import static org.hamcrest.core.Is.is;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootTest(classes = {PostCtrl.class, ResourceServerConfig.class})
//@EnableWebMvc
@ActiveProfiles(value = "unittest")
@AutoConfigureMockMvc()
public class PostCtrlTest {

  @Autowired
  private MockMvc mvc;

  @Test
  public void helloAllowed() throws Exception {
    var response = mvc.perform(
      get("/posts/pages")
        .accept(MediaType.APPLICATION_JSON)
    );

    response
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.length()", is(4)))
    ;
  }

  @Test
  public void helloUnauthenticated() throws Exception {
    var response = mvc.perform(
      post("/posts")
        .accept(MediaType.APPLICATION_JSON)
    );

    response
        .andExpect(status().isUnauthorized())
    ;
  }

}
```
