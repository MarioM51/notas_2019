# Spring Boot

- [Spring Boot](#spring-boot)
  - [Rapidos](#rapidos)
    - [Comandos](#comandos)
    - [Documentacion](#documentacion)
  - [Anki Cards](#anki-cards)
    - [Introdiccion](#introdiccion)
      - [Crear RESTful Web Service](#crear-restful-web-service)
      - [SPRING.FACTORIES](#springfactories)
    - [Curso Spring 6](#curso-spring-6)
      - [Dependency Injection with Spring](#dependency-injection-with-spring)
      - [Spring Bean Life Cycle.mp4](#spring-bean-life-cyclemp4)
      - [4. Introduction to Restful Web Services](#4-introduction-to-restful-web-services)
      - [5. Lombok](#5-lombok)
      - [6. Spring MockMVC](#6-spring-mockmvc)
    - [7. Spring MockMVC Test with Mockito](#7-spring-mockmvc-test-with-mockito)
      - [Exception Handling with Spring MVC](#exception-handling-with-spring-mvc)
      - [Spring Data JPA with Spring MVC](#spring-data-jpa-with-spring-mvc)
      - [10 - Data Validation](#10---data-validation)
      - [11 - MySQL with Spring Boot](#11---mysql-with-spring-boot)
      - [12 - Flyway Migrations](#12---flyway-migrations)
      - [13 - CSV File Uploads](#13---csv-file-uploads)
      - [14. Query Parameters with Spring MVC](#14-query-parameters-with-spring-mvc)
      - [15 - Paging and Sorting](#15---paging-and-sorting)
      - [16 - Database Relationship Mappings](#16---database-relationship-mappings)
      - [19 - Spring RestTemplate](#19---spring-resttemplate)
      - [20 - Testing Spring RestTemplate](#20---testing-spring-resttemplate)
      - [21 - Spring Security Basic Auth](#21---spring-security-basic-auth)
  - [Angular \& Spring 5: Creando web app full stack (Angular 8+)](#angular--spring-5-creando-web-app-full-stack-angular-8)
    - [Accesos Rapidos](#accesos-rapidos)
    - [Seccion 1: Introduccion](#seccion-1-introduccion)
    - [Seccion 2: Primeros pasos con Angular](#seccion-2-primeros-pasos-con-angular)
      - [5. Creando nuestra aplicación Angular](#5-creando-nuestra-aplicación-angular)
      - [6 y 7. Estructura de directorio del proyecto angular](#6-y-7-estructura-de-directorio-del-proyecto-angular)
      - [8. Integrar Bootstrap con Angular](#8-integrar-bootstrap-con-angular)
      - [9. Creando nuevo componente HeaderComponent](#9-creando-nuevo-componente-headercomponent)
      - [11. Creando nuevo componente FooterComponent](#11-creando-nuevo-componente-footercomponent)
      - [12. Directiva estructural \*ngFor](#12-directiva-estructural-ngfor)
      - [12. Directiva estructural \*ngIf](#12-directiva-estructural-ngif)
    - [Sección 3: Angular: Componente clientes](#sección-3-angular-componente-clientes)
      - [14. Creando el componente clientes.component](#14-creando-el-componente-clientescomponent)
      - [15. Listando los objetos del tipo Cliente](#15-listando-los-objetos-del-tipo-cliente)
      - [16. Creando archivo clientes.json.ts con la lista de objetos](#16-creando-archivo-clientesjsonts-con-la-lista-de-objetos)
      - [17. Creando la clase de Servicio ClienteService y la Inyección de Dependencia](#17-creando-la-clase-de-servicio-clienteservice-y-la-inyección-de-dependencia)
      - [18. Implementando Observable en nuestra clase Servicio ClienteService](#18-implementando-observable-en-nuestra-clase-servicio-clienteservice)
      - [19. Implementando Rutas en Angular y navegación](#19-implementando-rutas-en-angular-y-navegación)
      - [21. Configurando e integrando Bootstrap de forma local en nuestra app](#21-configurando-e-integrando-bootstrap-de-forma-local-en-nuestra-app)
      - [22. Instalando Bootstrap utilizando el comando npm desde el terminal](#22-instalando-bootstrap-utilizando-el-comando-npm-desde-el-terminal)
    - [Sección 4: Backend: Spring API REST](#sección-4-backend-spring-api-rest)
      - [25 y 26. Herramientas necesarias Backend](#25-y-26-herramientas-necesarias-backend)
      - [28. Creando Proyecto Backend API REST](#28-creando-proyecto-backend-api-rest)
      - [29. Configurando el Datasource a MySQL en el proyecto backend](#29-configurando-el-datasource-a-mysql-en-el-proyecto-backend)
      - [30. Instalando MySQL](#30-instalando-mysql)
      - [31. Creando la Base de Datos](#31-creando-la-base-de-datos)
      - [32. Añadiendo la clase Entity Cliente al Backend](#32-añadiendo-la-clase-entity-cliente-al-backend)
      - [33. Añadiendo las clases Repository y Service de la lógica de negocio](#33-añadiendo-las-clases-repository-y-service-de-la-lógica-de-negocio)
      - [34. Creando controlador @RestController y EndPoint para listar](#34-creando-controlador-restcontroller-y-endpoint-para-listar)
      - [35. Añadiendo Datos de pueba](#35-añadiendo-datos-de-pueba)
      - [36. Usando Postman para probar nuestras APIs](#36-usando-postman-para-probar-nuestras-apis)
      - [37. Uso de Cors para compartir recursos en API REST](#37-uso-de-cors-para-compartir-recursos-en-api-rest)
      - [38. Implementando Servicio Angular con HttpClient](#38-implementando-servicio-angular-con-httpclient)
      - [40. Escribiendo los métodos del CRUD en la clase ClienteService del Backend](#40-escribiendo-los-métodos-del-crud-en-la-clase-clienteservice-del-backend)
      - [41. Escribiendo los métodos show y create en el Controlador Backend API Rest](#41-escribiendo-los-métodos-show-y-create-en-el-controlador-backend-api-rest)
      - [42. Escribiendo los métodos update y delete en el Controlador Backend API Rest](#42-escribiendo-los-métodos-update-y-delete-en-el-controlador-backend-api-rest)
      - [43. Probando nuestro Backend API Rest con Postman](#43-probando-nuestro-backend-api-rest-con-postman)
      - [45. Creando el componente form.component y la vista del formulario](#45-creando-el-componente-formcomponent-y-la-vista-del-formulario)
      - [46. Configurando la ruta y navegación del formulario](#46-configurando-la-ruta-y-navegación-del-formulario)
      - [47. Escribiendo implementación crear en el cliente.service.ts y en form.component.ts](#47-escribiendo-implementación-crear-en-el-clienteservicets-y-en-formcomponentts)
      - [49. Instalar SweetAlert2 para enviar mensajes de alerta en el cliente](#49-instalar-sweetalert2-para-enviar-mensajes-de-alerta-en-el-cliente)
      - [50. Cargando los datos en el formulario para actualizar](#50-cargando-los-datos-en-el-formulario-para-actualizar)
      - [51. Escribiendo el update en el cliente.service.ts y en form.component.ts](#51-escribiendo-el-update-en-el-clienteservicets-y-en-formcomponentts)
      - [52. Escribiendo el delete en la clase service y en el componente clientes](#52-escribiendo-el-delete-en-la-clase-service-y-en-el-componente-clientes)
      - [53. Overflow en listado de clientes, ajustando layout](#53-overflow-en-listado-de-clientes-ajustando-layout)
      - [54. Validando los clientes en la tabla HTML con directiva ngIf](#54-validando-los-clientes-en-la-tabla-html-con-directiva-ngif)
      - [55. Manejo de error en el Backend en método handler show (obtener por id)](#55-manejo-de-error-en-el-backend-en-método-handler-show-obtener-por-id)
      - [56. Manejo de error en el Backend en método handler create](#56-manejo-de-error-en-el-backend-en-método-handler-create)
      - [57. Manejo de error en el Backend en método handler update](#57-manejo-de-error-en-el-backend-en-método-handler-update)
      - [58. Manejo de error en el Backend en método handler delete](#58-manejo-de-error-en-el-backend-en-método-handler-delete)
      - [59. Manejo de error en el Frontend Angular en obtener por id](#59-manejo-de-error-en-el-frontend-angular-en-obtener-por-id)
      - [60. Manejo de error en el Frontend Angular en create, update y delete](#60-manejo-de-error-en-el-frontend-angular-en-create-update-y-delete)
      - [61. Customizando y arreglando los textos de éxito en crear y actualizar del frontend](#61-customizando-y-arreglando-los-textos-de-éxito-en-crear-y-actualizar-del-frontend)
    - [Sección 8: Validando form por el lado Angular (Frontend)](#sección-8-validando-form-por-el-lado-angular-frontend)
      - [62. Validando form en el template](#62-validando-form-en-el-template)
    - [Sección 9: Validando form en Spring Backend](#sección-9-validando-form-en-spring-backend)
      - [64. Anotaciones JavaBeans Validation en la clase Entity](#64-anotaciones-javabeans-validation-en-la-clase-entity)
      - [65. Implementando anotación @Valid en métodos handler create y update del controller](#65-implementando-anotación-valid-en-métodos-handler-create-y-update-del-controller)
      - [66. Probando validación API REST en POSTMAN](#66-probando-validación-api-rest-en-postman)
      - [67. Manejando los error de validación en Angular](#67-manejando-los-error-de-validación-en-angular)
      - [68. Agregando los mensajes de errores en la plantilla form](#68-agregando-los-mensajes-de-errores-en-la-plantilla-form)
      - [69. Personalizar mensajes de validación en español](#69-personalizar-mensajes-de-validación-en-español)
    - [Sección 10: Transformando datos del Observable: usando operador map y Pipes](#sección-10-transformando-datos-del-observable-usando-operador-map-y-pipes)
      - [70-71. Operador map formato uppercase en Observable](#70-71-operador-map-formato-uppercase-en-observable)
      - [72. Registrando el Locale y los diferentes Pattern para formatear fechas](#72-registrando-el-locale-y-los-diferentes-pattern-para-formatear-fechas)
      - [73. Uso de Pipe para formatear fecha y uppercase en las plantillas html](#73-uso-de-pipe-para-formatear-fecha-y-uppercase-en-las-plantillas-html)
      - [74. Uso del operador tap en el Observable](#74-uso-del-operador-tap-en-el-observable)
    - [Sección 11: Paginación desde el Backend](#sección-11-paginación-desde-el-backend)
      - [75/Descargar - 76. Implementando paginación en API REST y Repository](#75descargar---76-implementando-paginación-en-api-rest-y-repository)
      - [77. Probar paginación API REST en POSTMAN](#77-probar-paginación-api-rest-en-postman)
      - [78. Modificando el Observable de Clientes en el FrontEnd](#78-modificando-el-observable-de-clientes-en-el-frontend)
      - [79. Agregando la ruta page del paginador y el Observable ActivatedRoute](#79-agregando-la-ruta-page-del-paginador-y-el-observable-activatedroute)
      - [80. Creando el componente paginador](#80-creando-el-componente-paginador)
      - [81. Inyectar paginador usando decorador @Input en el componente Paginador](#81-inyectar-paginador-usando-decorador-input-en-el-componente-paginador)
      - [82. Implementando los links de las páginas](#82-implementando-los-links-de-las-páginas)
      - [85. Paginador con muchas páginas: overflow](#85-paginador-con-muchas-páginas-overflow)
    - [Sección 13: Subida de imagen (Upload)](#sección-13-subida-de-imagen-upload)
      - [91. Upload en el API Rest Controller (Backend)](#91-upload-en-el-api-rest-controller-backend)
      - [93. Borrar imagen anterior al actualizar o eliminar](#93-borrar-imagen-anterior-al-actualizar-o-eliminar)
      - [94. Añadiendo método handler ver imagen en el Controlador](#94-añadiendo-método-handler-ver-imagen-en-el-controlador)
      - [95. Añadiendo Logger en back-end](#95-añadiendo-logger-en-back-end)
      - [96. Añadiendo componente detalle para el upload en el front-end](#96-añadiendo-componente-detalle-para-el-upload-en-el-front-end)
      - [97. Implementando la subida de archivo en el front-end](#97-implementando-la-subida-de-archivo-en-el-front-end)
      - [98. Añadiendo la foto y detalle del cliente en angular](#98-añadiendo-la-foto-y-detalle-del-cliente-en-angular)
      - [99. Validando imagen antes de subir](#99-validando-imagen-antes-de-subir)
      - [100. Añadiendo Barra de Progreso](#100-añadiendo-barra-de-progreso)
      - [101. Implemementando Modal como componente anidado (hijo)](#101-implemementando-modal-como-componente-anidado-hijo)
      - [102. Convertiendo a Modal de Bootstrap](#102-convertiendo-a-modal-de-bootstrap)
      - [103. Añadiendo estilos y animación de difuminado al Modal](#103-añadiendo-estilos-y-animación-de-difuminado-al-modal)
      - [104. Añadiendo la foto en el listado](#104-añadiendo-la-foto-en-el-listado)
      - [105. Implementando EventEmitter para actualizar la foto en el listado](#105-implementando-eventemitter-para-actualizar-la-foto-en-el-listado)
    - [Sección 14: Agregando campo select en el formulario y relación de tablas](#sección-14-agregando-campo-select-en-el-formulario-y-relación-de-tablas)
      - [108. Creando nueva clase entity Region](#108-creando-nueva-clase-entity-region)
      - [109. Añadiendo las regiones en la clase service y controller del backend](#109-añadiendo-las-regiones-en-la-clase-service-y-controller-del-backend)
      - [110. Probando API Rest de las regiones en Postman](#110-probando-api-rest-de-las-regiones-en-postman)
      - [111. La clase TypeScript Region en Angular](#111-la-clase-typescript-region-en-angular)
      - [112. Añadiendo el campo select region en el formulario](#112-añadiendo-el-campo-select-region-en-el-formulario)
      - [113. Comparando la región en el campo select para marcarla como seleccionada](#113-comparando-la-región-en-el-campo-select-para-marcarla-como-seleccionada)
      - [114. Añadiendo opción por defecto con el texto seleccionar](#114-añadiendo-opción-por-defecto-con-el-texto-seleccionar)
    - [Sección 15: Autenticación OAuth2 con JWT: Backend Spring](#sección-15-autenticación-oauth2-con-jwt-backend-spring)
      - [116. Introducción a JSON Web Token (JWT)](#116-introducción-a-json-web-token-jwt)
      - [117. Algo más sobre los JWT](#117-algo-más-sobre-los-jwt)
      - [118. Introducción a OAuth2 y añadiendo las dependencias](#118-introducción-a-oauth2-y-añadiendo-las-dependencias)
      - [119. Creando las entidades necesarias Usuario y Role](#119-creando-las-entidades-necesarias-usuario-y-role)
      - [120. Creando el repositorio JPA IUsuarioRepository](#120-creando-el-repositorio-jpa-iusuariorepository)
      - [121. Creando la clase de servicio UsuarioService](#121-creando-la-clase-de-servicio-usuarioservice)
      - [122. Añadiendo la clase SpringSecurityConfig y registrando UserDetailsService JPA](#122-añadiendo-la-clase-springsecurityconfig-y-registrando-userdetailsservice-jpa)
      - [123. Añadiendo la configuración para el servidor de autorización](#123-añadiendo-la-configuración-para-el-servidor-de-autorización)
      - [124. Añadiendo configuración de las aplicaciones clientes y acceso a endpoints](#124-añadiendo-configuración-de-las-aplicaciones-clientes-y-acceso-a-endpoints)
      - [125. Añadiendo la configuración para el servidor de recurso](#125-añadiendo-la-configuración-para-el-servidor-de-recurso)
      - [126. Probando la autenticación con Postman y obteniendo el token JWT](#126-probando-la-autenticación-con-postman-y-obteniendo-el-token-jwt)
      - [127. Asignando la llave secreta Mac para firmar el token JWT](#127-asignando-la-llave-secreta-mac-para-firmar-el-token-jwt)
      - [128. Creando y asignando certificado RSA para firmar el token JWT](#128-creando-y-asignando-certificado-rsa-para-firmar-el-token-jwt)
      - [129. Añadiendo más datos en el token JWT](#129-añadiendo-más-datos-en-el-token-jwt)
      - [130. Complementando con más información adicional del usuario autenticado](#130-complementando-con-más-información-adicional-del-usuario-autenticado)
      - [131. Agregando seguridad a nuestras rutas de forma programática con HttpSecurity](#131-agregando-seguridad-a-nuestras-rutas-de-forma-programática-con-httpsecurity)
      - [132. Añadiendo seguridad a nuestras ruta del controlador usando anotaciones @Secured](#132-añadiendo-seguridad-a-nuestras-ruta-del-controlador-usando-anotaciones-secured)
      - [133. Configurando Cors en Spring Security](#133-configurando-cors-en-spring-security)
    - [Sección 16: Autenticación OAuth2 con JWT​: Frontend Angular](#sección-16-autenticación-oauth2-con-jwt-frontend-angular)
      - [136. Creando componente login y formulario](#136-creando-componente-login-y-formulario)
      - [137. Manejo de error no autorizado y redirección al login](#137-manejo-de-error-no-autorizado-y-redirección-al-login)
      - [138. Añadiendo el método login() en el componente y nueva clase Usuario](#138-añadiendo-el-método-login-en-el-componente-y-nueva-clase-usuario)
      - [139. Creando la clase de servicio AuthService y su método login()](#139-creando-la-clase-de-servicio-authservice-y-su-método-login)
      - [140. Implementando la autenticación en el componente login](#140-implementando-la-autenticación-en-el-componente-login)
      - [141. Guardando datos del usuario y token en el sessionStorage](#141-guardando-datos-del-usuario-y-token-en-el-sessionstorage)
      - [142. Manejando error de credenciales incorrectas en componente login](#142-manejando-error-de-credenciales-incorrectas-en-componente-login)
      - [143. Chequear en el componente login si el usuario ya ha iniciado sesión](#143-chequear-en-el-componente-login-si-el-usuario-ya-ha-iniciado-sesión)
      - [144. Añadiendo en el layout el links logout para cerrar la sesión](#144-añadiendo-en-el-layout-el-links-logout-para-cerrar-la-sesión)
      - [145. Enviando token al backend para acceder a los recursos protegidos](#145-enviando-token-al-backend-para-acceder-a-los-recursos-protegidos)
      - [146. Manejando código de error 403 de Accedo Denegado o Prohibido (Forbidden)](#146-manejando-código-de-error-403-de-accedo-denegado-o-prohibido-forbidden)
      - [147. Ocultando botones en las plantillas según el role del usuario](#147-ocultando-botones-en-las-plantillas-según-el-role-del-usuario)
      - [149. Añadiendo seguridad en las rutas con Guard](#149-añadiendo-seguridad-en-las-rutas-con-guard)
      - [150. Crear Guard router RoleGuard](#150-crear-guard-router-roleguard)
      - [151. Validando fecha de expiración del token en el AuthGuard](#151-validando-fecha-de-expiración-del-token-en-el-authguard)
      - [152. HttpInterceptor para añadir el token en las cabeceras HTTP Authorization](#152-httpinterceptor-para-añadir-el-token-en-las-cabeceras-http-authorization)
      - [153. HttpInterceptor para códigos HTTP 401 y 403](#153-httpinterceptor-para-códigos-http-401-y-403)
    - [Sección 17: Sistema de Facturación](#sección-17-sistema-de-facturación)
      - [155. Demostración de lo que lograremos al finalizar esta sección](#155-demostración-de-lo-que-lograremos-al-finalizar-esta-sección)
      - [156. Análisis y Diseño OO con UML Diagrama de Clases del Dominio](#156-análisis-y-diseño-oo-con-uml-diagrama-de-clases-del-dominio)
      - [157. Asociaciones: ManyToOne Bidireccional - Un Cliente tiene muchas Facturas](#157-asociaciones-manytoone-bidireccional---un-cliente-tiene-muchas-facturas)
      - [158. Asociaciones: OneToMany Unidireccional - Una Factura tiene muchas Lineas](#158-asociaciones-onetomany-unidireccional---una-factura-tiene-muchas-lineas)
      - [159. Asociaciones: ManyToOne Unidireccional - Muchas LineaFactura tiene un Producto](#159-asociaciones-manytoone-unidireccional---muchas-lineafactura-tiene-un-producto)
      - [160. Analizando y revisando las tablas y relaciones en MySQL Workbench](#160-analizando-y-revisando-las-tablas-y-relaciones-en-mysql-workbench)
      - [161. Listando las facturas del Cliente en Postman](#161-listando-las-facturas-del-cliente-en-postman)
      - [162. Añadiendo las clases TypeScript Factura, ItemFactura y Producto en Angular](#162-añadiendo-las-clases-typescript-factura-itemfactura-y-producto-en-angular)
      - [163. Listando las facturas en el componente de detalle del Cliente](#163-listando-las-facturas-en-el-componente-de-detalle-del-cliente)
      - [164. Añadiendo CrudRepository para Factura e implementando métodos en el Service](#164-añadiendo-crudrepository-para-factura-e-implementando-métodos-en-el-service)
      - [165. Creando controlador FacturaRestController con la acción handler show](#165-creando-controlador-facturarestcontroller-con-la-acción-handler-show)
      - [166. Creando Componente detalle de la factura y su clase Service relacionada](#166-creando-componente-detalle-de-la-factura-y-su-clase-service-relacionada)
      - [167. Añadiendo el detalle de la Factura](#167-añadiendo-el-detalle-de-la-factura)
      - [168. Añadiendo las Líneas y Observación en el detalle de la Factura](#168-añadiendo-las-líneas-y-observación-en-el-detalle-de-la-factura)
      - [169. Eliminando la factura y sus líneas (Backend)](#169-eliminando-la-factura-y-sus-líneas-backend)
      - [170. Eliminando la factura y sus líneas (Frontend)](#170-eliminando-la-factura-y-sus-líneas-frontend)
      - [171. Creando el componente formulario de la factura](#171-creando-el-componente-formulario-de-la-factura)
      - [172. Añadiendo campos al formulario factura](#172-añadiendo-campos-al-formulario-factura)
      - [173. Escribiendo código para el Autocomplete Autocompletado usando Angular Material](#173-escribiendo-código-para-el-autocomplete-autocompletado-usando-angular-material)
      - [174. Consulta JPA para buscar productos en el Autocomplete](#174-consulta-jpa-para-buscar-productos-en-el-autocomplete)
      - [175. Modificando el autocomplete para que filtre los productos desde el Backend](#175-modificando-el-autocomplete-para-que-filtre-los-productos-desde-el-backend)
      - [176. Creando las líneas de la Factura](#176-creando-las-líneas-de-la-factura)
      - [177. Actualizando la cantidad de un item de la factura](#177-actualizando-la-cantidad-de-un-item-de-la-factura)
      - [178. Incrementando la cantidad si el producto existe en el detalle (Evitando que se duplique el producto)](#178-incrementando-la-cantidad-si-el-producto-existe-en-el-detalle-evitando-que-se-duplique-el-producto)
      - [179. Eliminar linea de la factura](#179-eliminar-linea-de-la-factura)
      - [180. Calculando el Gran Total de la Factura](#180-calculando-el-gran-total-de-la-factura)
      - [181. Implementando el crear factura](#181-implementando-el-crear-factura)
      - [184. Solucionando un problema de recursión al editar el cliente](#184-solucionando-un-problema-de-recursión-al-editar-el-cliente)
    - [Sección 18: Deployment: despliegue en servidores de producción](#sección-18-deployment-despliegue-en-servidores-de-producción)
      - [186. Modificando y preparando nuestros proyectos (backend y fronend) para producción](#186-modificando-y-preparando-nuestros-proyectos-backend-y-fronend-para-producción)
      - [188. Transpilando nuestra aplicación angular y preparandonos para el despliegue en Apache](#188-transpilando-nuestra-aplicación-angular-y-preparandonos-para-el-despliegue-en-apache)
      - [189. Realizando el despliegue del frontend Angular en servidor Apache 24 (httpd)](#189-realizando-el-despliegue-del-frontend-angular-en-servidor-apache-24-httpd)
      - [190. Realizando el despliegue de nuestra app angular con NodeJS y Express](#190-realizando-el-despliegue-de-nuestra-app-angular-con-nodejs-y-express)
    - [Sección 19: Deployment: despliegue en servidores en la nube Heroku y Google Firebase Hosting](#sección-19-deployment-despliegue-en-servidores-en-la-nube-heroku-y-google-firebase-hosting)
      - [192. Creando app en Heroku | 193. Deploy Backend Spring en Heroku](#192-creando-app-en-heroku--193-deploy-backend-spring-en-heroku)
      - [194. Deploy Frontend Angular en Firebase Hosting](#194-deploy-frontend-angular-en-firebase-hosting)
    - [Sección 20: Sistema de Chat: en tiempo real con WebSocket](#sección-20-sistema-de-chat-en-tiempo-real-con-websocket)
      - [197. Introducción a la tecnología WebSocket](#197-introducción-a-la-tecnología-websocket)
      - [198. ¿Qué es el Protocolo Stomp y cómo se relaciona con WebSocket?\_](#198-qué-es-el-protocolo-stomp-y-cómo-se-relaciona-con-websocket_)
        - [Cliente](#cliente)
        - [Servidor](#servidor)
      - [200. Creando el proyecto backend y configurando el servidor WebSocket (el Broker)](#200-creando-el-proyecto-backend-y-configurando-el-servidor-websocket-el-broker)
      - [201. Creando el controlador ChatController y el Mensaje](#201-creando-el-controlador-chatcontroller-y-el-mensaje)
      - [202. Creando el proyecto frontend Angular](#202-creando-el-proyecto-frontend-angular)
      - [203. Instalando librerías para el cliente WebSocket en Angular (sockjs y stompjs)](#203-instalando-librerías-para-el-cliente-websocket-en-angular-sockjs-y-stompjs)
      - [204. Maquetando y escribiendo el contenido HTML para el chat](#204-maquetando-y-escribiendo-el-contenido-html-para-el-chat)
      - [205. Implementando el conectar y desconectar del servidor WebSocket](#205-implementando-el-conectar-y-desconectar-del-servidor-websocket)
      - [206. Implementando el enviar y recibir mensajes del Chat](#206-implementando-el-enviar-y-recibir-mensajes-del-chat)
      - [207. Notificar cuando un nuevo usuario se conecta al chat](#207-notificar-cuando-un-nuevo-usuario-se-conecta-al-chat)
      - [208. Dando un color al username cuando se conecta al servidor](#208-dando-un-color-al-username-cuando-se-conecta-al-servidor)
      - [209. Notificar cuando un usuario está escribiendo](#209-notificar-cuando-un-usuario-está-escribiendo)
      - [210. Instalación Mongo DB](#210-instalación-mongo-db)
      - [211. Qué es Mongo y algunos ejemplos](#211-qué-es-mongo-y-algunos-ejemplos)
      - [212. Instalando Robo 3T una herramienta GUI para MongoDB](#212-instalando-robo-3t-una-herramienta-gui-para-mongodb)
      - [213. Agregando Clases del Modelo Document y Repository](#213-agregando-clases-del-modelo-document-y-repository)
      - [214. Historial de mensajes del chat con MongoDB](#214-historial-de-mensajes-del-chat-con-mongodb)
    - [215. Agradecimientos y despedida](#215-agradecimientos-y-despedida)
    - [POST-ERRORES](#post-errores)
      - [Error ClassNotFoundException ContextFactory](#error-classnotfoundexception-contextfactory)
      - [Error RequestRejectedException Content-Disposition](#error-requestrejectedexception-content-disposition)
      - [There is no PasswordEncoder mapped for the id "null"](#there-is-no-passwordencoder-mapped-for-the-id-null)
    - [Cosas que me voy encontrando](#cosas-que-me-voy-encontrando)
      - [Enlaces a parecidos](#enlaces-a-parecidos)
      - [Uso de perfiles](#uso-de-perfiles)
      - [Personalizar errores de autenticacion](#personalizar-errores-de-autenticacion)
      - [TODO: Guardar y entregar imagen en base64](#todo-guardar-y-entregar-imagen-en-base64)
        - [Recuperar la imagen en base64 y mostrarla](#recuperar-la-imagen-en-base64-y-mostrarla)
      - [Eliminar en cascada](#eliminar-en-cascada)
      - [Evitar nulos en modificacion](#evitar-nulos-en-modificacion)
      - [Manejo de errres DataIntegrityViolationException](#manejo-de-errres-dataintegrityviolationexception)
      - [Ejecutar comando desde java](#ejecutar-comando-desde-java)
      - [Implementar OAuth con JWT simple](#implementar-oauth-con-jwt-simple)
      - [Hacer una peticion https sin validar certificado (certificado auto firmado)](#hacer-una-peticion-https-sin-validar-certificado-certificado-auto-firmado)
  - [Microservicios con Spring Boot y Spring Cloud Netflix Eureka](#microservicios-con-spring-boot-y-spring-cloud-netflix-eureka)
    - [1. Introducción](#1-introducción)
    - [Sección 2: Microservicios: la base](#sección-2-microservicios-la-base)
      - [7. Creando nuestro microservicio productos](#7-creando-nuestro-microservicio-productos)
      - [9. Creando el repositorio JPA para los productos](#9-creando-el-repositorio-jpa-para-los-productos)
      - [10. Creando el componente service para los productos](#10-creando-el-componente-service-para-los-productos)
      - [11. Creando el controlador rest productos](#11-creando-el-controlador-rest-productos)
      - [12. Agregando datos y probando nuestra API productos con Postman](#12-agregando-datos-y-probando-nuestra-api-productos-con-postman)
      - [13. Creando microservicio items](#13-creando-microservicio-items)
      - [14. Creando componente service en items para consumir API productos](#14-creando-componente-service-en-items-para-consumir-api-productos)
      - [15. Implementando componente service con cliente HTTP RestTemplate](#15-implementando-componente-service-con-cliente-http-resttemplate)
      - [16. Creando controlador en items](#16-creando-controlador-en-items)
      - [17. Usando cliente REST de Feign para consumir nuestra API productos](#17-usando-cliente-rest-de-feign-para-consumir-nuestra-api-productos)
      - [18. Balanceo de carga del lado del cliente con Ribbon](#18-balanceo-de-carga-del-lado-del-cliente-con-ribbon)
      - [19. Probando balanceo de carga en postman](#19-probando-balanceo-de-carga-en-postman)
      - [20. Utilizando Ribbon para balanceo de carga en RestTemplate](#20-utilizando-ribbon-para-balanceo-de-carga-en-resttemplate)
    - [Sección 3: Eureka Server: registrando microservicios](#sección-3-eureka-server-registrando-microservicios)
      - [22. Creando servidor de registro Eureka](#22-creando-servidor-de-registro-eureka)
      - [23. Conectando nuestros servicios como clientes Eureka](#23-conectando-nuestros-servicios-como-clientes-eureka)
      - [24. Escalar microservcios con puerto dinámico](#24-escalar-microservcios-con-puerto-dinámico)
      - [25. Tolerancia de fallos y latencia con Hystrix](#25-tolerancia-de-fallos-y-latencia-con-hystrix)
      - [26. Configurando timeout en Hystrix y Ribbon](#26-configurando-timeout-en-hystrix-y-ribbon)
      - [27. Creando y configurando servidor Zuul API Gateway](#27-creando-y-configurando-servidor-zuul-api-gateway)
      - [28. Zuul Filtro HTTP pre - Tiempo transcurrido](#28-zuul-filtro-http-pre---tiempo-transcurrido)
      - [29. Zuul Filtro HTTP post - Tiempo transcurrido](#29-zuul-filtro-http-post---tiempo-transcurrido)
      - [30. Configurando timeout en Zuul API Gateway](#30-configurando-timeout-en-zuul-api-gateway)
    - [Sección 4: Spring Cloud Config Server: centralizando la configuración](#sección-4-spring-cloud-config-server-centralizando-la-configuración)
      - [32. Creando el servidor de configuración](#32-creando-el-servidor-de-configuración)
      - [33-34-35 Conectando servicio items al Servidor de Configuración](#33-34-35-conectando-servicio-items-al-servidor-de-configuración)
      - [36. Leer configuración desde controlador utilizando anotación @Value](#36-leer-configuración-desde-controlador-utilizando-anotación-value)
      - [37-38 Configurando los ambientes en el repositorio Git  Configurando profiles en servicio items](#37-38-configurando-los-ambientes-en-el-repositorio-git--configurando-profiles-en-servicio-items)
      - [39. Actualizando cambios en configuraciones con @RefreshScope y Actuator](#39-actualizando-cambios-en-configuraciones-con-refreshscope-y-actuator)
      - [40. Creando repositorio remoto GitHub y push](#40-creando-repositorio-remoto-github-y-push)
    - [Sección 5: CRUD: consumiendo nuestras APIs con Feign y Rest Template](#sección-5-crud-consumiendo-nuestras-apis-con-feign-y-rest-template)
      - [42. Creando Service para el CRUD y handler POST en controlador servicio productos](#42-creando-service-para-el-crud-y-handler-post-en-controlador-servicio-productos)
    - [43. Implementando handlers PUT y DELETE en controlador de servicio productos](#43-implementando-handlers-put-y-delete-en-controlador-de-servicio-productos)
      - [44. Consumiendo API Rest con RestTemplate desde servicio items](#44-consumiendo-api-rest-con-resttemplate-desde-servicio-items)
      - [45. Implementando handler CRUD en controlador de servicio items](#45-implementando-handler-crud-en-controlador-de-servicio-items)
      - [46. Probando operaciones CRUD en servicio items con postman](#46-probando-operaciones-crud-en-servicio-items-con-postman)
      - [47. Consumiendo API Rest con cliente Feign](#47-consumiendo-api-rest-con-cliente-feign)
    - [Sección 6: Servicio commons: reutilizando librerías](#sección-6-servicio-commons-reutilizando-librerías)
      - [49. Creando librería commons y generando jar usando maven](#49-creando-librería-commons-y-generando-jar-usando-maven)
      - [50. Modificando servicio productos para agregar dependencia commons](#50-modificando-servicio-productos-para-agregar-dependencia-commons)
      - [51. Modificando servicio items y probando con Postman](#51-modificando-servicio-items-y-probando-con-postman)
    - [Sección 7: Spring Cloud Security: implementando OAuth2 y JWT](#sección-7-spring-cloud-security-implementando-oauth2-y-jwt)
      - [53. Introducción a JSON Web Token (JWT)](#53-introducción-a-json-web-token-jwt)
      - [56. Creando nuestro microservicio usuarios](#56-creando-nuestro-microservicio-usuarios)
      - [57 - 58 Creando las entidades necesarias Usuario y Role / Creando la relación de muchos a muchos entre Usuario y Role](#57---58-creando-las-entidades-necesarias-usuario-y-role--creando-la-relación-de-muchos-a-muchos-entre-usuario-y-role)
      - [59. Creando el repositorio JPA para los usuarios](#59-creando-el-repositorio-jpa-para-los-usuarios)
      - [60. Implementando un repositorio rest para el CRUD](#60-implementando-un-repositorio-rest-para-el-crud)
      - [61. Exportar métodos repositorio rest con endpoint search](#61-exportar-métodos-repositorio-rest-con-endpoint-search)
      - [62. Configurando y exponer id en la respuesta json](#62-configurando-y-exponer-id-en-la-respuesta-json)
      - [63. Creando librería commons usuarios](#63-creando-librería-commons-usuarios)
      - [65. Creando microservicio oauth (Servidor de Autorización)](#65-creando-microservicio-oauth-servidor-de-autorización)
      - [66. Creando nuestra interfaz Feign Client](#66-creando-nuestra-interfaz-feign-client)
      - [7. Implementando servicio UserDetailsService con Feign Client](#7-implementando-servicio-userdetailsservice-con-feign-client)
      - [68. Añadiendo la clase SpringSecurityConfig y registrando UserDetailsService](#68-añadiendo-la-clase-springsecurityconfig-y-registrando-userdetailsservice)
      - [69-70. Añadiendo la configuración para el servidor de autorización en servicio oauth - Añadiendo configuración de los clientes en el Servidor de Autorización](#69-70-añadiendo-la-configuración-para-el-servidor-de-autorización-en-servicio-oauth---añadiendo-configuración-de-los-clientes-en-el-servidor-de-autorización)
      - [71. Agregando rutas de Spring Security a Zuul Probando la autenticación con Postman y obteniendo el token JWT](#71-agregando-rutas-de-spring-security-a-zuul-probando-la-autenticación-con-postman-y-obteniendo-el-token-jwt)
      - [72. Añadiendo más información al token JWT](#72-añadiendo-más-información-al-token-jwt)
      - [73-74. Configurando Zuul como Servidor de Recurso - Probando autorización y accesos a recursos protegidos](#73-74-configurando-zuul-como-servidor-de-recurso---probando-autorización-y-accesos-a-recursos-protegidos)
      - [75. Creando configuración de OAuth en el Servidor de Configuración](#75-creando-configuración-de-oauth-en-el-servidor-de-configuración)
      - [76. Revisando el token de actualización](#76-revisando-el-token-de-actualización)
      - [77. Configurando Cors en Spring Security OAuth2](#77-configurando-cors-en-spring-security-oauth2)
      - [78. Manejando eventos de éxito y fracaso en la autenticación](#78-manejando-eventos-de-éxito-y-fracaso-en-la-autenticación)
      - [79-80. Implementando evento tres intentos en el login - parte 1 y parte 2](#79-80-implementando-evento-tres-intentos-en-el-login---parte-1-y-parte-2)
      - [81. Manejando error 404 en componente de servicio UserDetailsService](#81-manejando-error-404-en-componente-de-servicio-userdetailsservice)
    - [Sección 8: Base de Datos MySQL: configurando MySQL en servicio productos](#sección-8-base-de-datos-mysql-configurando-mysql-en-servicio-productos)
      - [83-84. Instalando MySQL y Creando Database](#83-84-instalando-mysql-y-creando-database)
      - [85. Configurando Datasource MySQL en servicio productos](#85-configurando-datasource-mysql-en-servicio-productos)
      - [86. Configurando ambiente dev con MySQL en Servidor de Configuración](#86-configurando-ambiente-dev-con-mysql-en-servidor-de-configuración)
    - [Sección 9: Base de Datos PostgreSQL: configurando en servicio usuarios](#sección-9-base-de-datos-postgresql-configurando-en-servicio-usuarios)
      - [88. Instalando PostgreSQL y creando la Base de Datos en pgAdmin](#88-instalando-postgresql-y-creando-la-base-de-datos-en-pgadmin)
      - [89. Configurando PostgreSQL en Servidor de Configuración](#89-configurando-postgresql-en-servidor-de-configuración)
      - [90. Configurando repositorio remoto GitHub como privado](#90-configurando-repositorio-remoto-github-como-privado)
      - [Sección 10: Trazabilidad distribuida con Spring Cloud Sleuth y Zipkin](#sección-10-trazabilidad-distribuida-con-spring-cloud-sleuth-y-zipkin)
      - [92. Introducción a la trazabilidad distribuida](#92-introducción-a-la-trazabilidad-distribuida)
    - [Microservicios con Spring Cloud y Angular 9](#microservicios-con-spring-cloud-y-angular-9)
      - [Introduccion Micro](#introduccion-micro)

## Rapidos

### Comandos

```r
#Levantar Mysql
docker run --name mysql1 --rm \
  -v ./data:/etc/mysql/conf.d \
  -e MYSQL_ROOT_PASSWORD=xh_db_pass_dev_r \
  -e MYSQL_DATABASE=xh_db_name_dev \
  -e MYSQL_USER=xh_db_user_dev \
  -e MYSQL_PASSWORD=xh_db_pass_dev \
  -p 3306:3306 \
  mysql:8
```

### Documentacion

- [JPA](https://spring.io/guides/gs/accessing-data-rest/)

- [SpEL](https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#expressions)

## Anki Cards

### Introdiccion

Fuente: [link](https://spring.io/quickstart)

```r
#correr app
#crear proyecto
Como crear un proyecto spring boot fácilmente;'

1. Ir a https://start.spring.io/

2. Seleccionar las opciones que querremos como
  - java o Kotlin
  - maven o gradle
  - version
  - Dependencias

  2.1. Crear con curl
    ´curl https://start.spring.io/starter.tgz -d dependencies=web,security -d name=ui | tar -xzvf -´

  2.2. Crear con el cli de Spring
    ´$ spring init --dependencies web,security ui/ && cd ui´

3. Descargar y descomprimir

4. Compilar: `./mvnw clean package`

5. Iniciar: `java -jar ./target/xxx.jar`

  5.1. ó usando gradle: `./gradlew bootRun`
  5.2. ó usando mvn instalado globalmente `mvn spring-boot:run`

'

Codigo para un hola mundo, con endpoint /hello?name=Mario;'
<pre>
// ./img/01-hello-world.java

package com.m51.docs.uno;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

    public static void main(String[] args) {
      SpringApplication.run(Application.class, args);
    }

    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
      return String.format("Hola %s desde Spring boot 3!", name);
    }

}
<pre>

'

#instalar dependencias del pom.xml, descargar dependencias del pom.xml
Como ver todas las librerías/dependencias de un proyecto;"
Ejecutar en la terminal al nivel del pom.xml (también sirve para descargar dependencias)

`mvn dependency:resolve`

Tambien se puede usar este pero puede causar error: No plugin found for prefix 'dependencies' in the current project and in the plugin groups [org.apache.maven.plugins, org.codehaus.mojo] available from the repositories

´mvn dependency:tree´
"

Como ver todas las propiedades del archivo application.properties;"
Documentación:
https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html#application-properties.core.debug

Usando estos plugins las tenemos usando Ctrl+space
- vscjava.vscode-spring-boot-dashboard
- vmware.vscode-spring-boot
- vscjava.vscode-spring-initializr

"

Dentro del archivo jar, 1,¿Que contiene la carpeta META-INF? 2,¿Donde se definen las clases main y de inicio?;"
1.
  Las necesidades del .jar para ser ejecutado

2. Dentro de {jar}/META-INF/MANIFEST.MF 
  Main-Class: org.springframework.boot.loader.JarLauncher
  Start-Class: com.m51.book01.Main

"

Como traer un valor del .properties y usarlo en una clase/bean;'
1, Creamos la propiedad en el .properties
<pre>
  rest.template.rootUrl=http://localhost:8080
</pre>

2, La traemos

<pre>
import org.springframework.beans.factory.annotation.Value;
@XXX
public class Xxxx {

    @Value("${rest.template.rootUrl}")
    String rootUrl;

    public foo() {
      //solo para asegurarnos que existe, y si falla saber que paso
      assert rootUrl != null;
      print(rootUrl)
    }

</pre>
'

#mostrar los logs, mostrar los logs de peticiones, peticiones logs peticiones
¿Como ver los logs web?;"
logging.level.org.springframework.web=DEBUG
"

```

#### Crear RESTful Web Service

Fuente: [Link](https://spring.io/guides/gs/rest-service/)

```r
Como una variable que incremente con cada petición;'

<pre>
  @RestController
  public class GreetingController {

    private static final String template = "Hello, %s!";
+   private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
      return new Greeting(
+       counter.incrementAndGet(),
        String.format(template, name)
      );
    }
    
  }
</pre>
'

Como configurar un graceful shutdown;"
#spring version +2.3.0
#default immediate
server.shutdown=graceful
spring.lifecycle.timeout-per-shutdown-phase=1m
"

SpringBoot startup events, 1,¿Cuantos hay?, ¿Ejemplos ejecutar metodo en X evento, ej. 2,Inicio 3,App Lista ?;'

1, En total 9, Ver la doc
  https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.spring-application.application-events-and-listeners

3, Evento inicio

<pre>
  package com.m51.book01.starting;

  import java.util.Date;

  import org.springframework.boot.context.event.ApplicationStartingEvent;
  import org.springframework.context.ApplicationListener;

  public class ApplicationStartingEventListener implements ApplicationListener<ApplicationStartingEvent> {
    @Override
    public void onApplicationEvent(ApplicationStartingEvent applicationStartingEvent) {
      System.out.println("Application Starting Event logged at " + new Date(applicationStartingEvent.getTimestamp()));
    }
  }
  //======================
  @SpringBootApplication
  public class Main {
    public static void main(String[] args) {
      SpringApplication springApplication = new SpringApplication(Main.class);
+     springApplication.addListeners(new ApplicationStartingEventListener());
      springApplication.run(args);
    }

<pre>

2, Agregar evento App Lista

<pre>

02-book01.zip/src/main/java/com/m51/book01/Main.java

    @EventListener(ApplicationReadyEvent.class)
    public void applicationReadyEvent(ApplicationReadyEvent applicationReadyEvent) {
      var date = new Date(applicationReadyEvent.getTimestamp());
      var msg = "Application Ready Event generated at " + date;
      System.out.println(msg);
    }

</pre>
'

#mostrar interface de h2
#mostrar consola h2. mostrar h2, 
Mostrar la consola h2;'
  spring.h2.console.enabled=true
  spring.h2.console.path=/h2-console
#opcional
  spring.datasource.url=jdbc:h2:mem:foobar
  logging.level.org.hibernate.SQL=trace

-----

Tambien debemos de aceptar los frames e ignorar el csrf

  @Configuration
  public class ProjectConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http
        .authorizeRequests()
//no se recomienda poner la ruta /h2-console, ya que estaria desprotegida
          .antMatchers("/public").permitAll()
          .anyRequest().authenticated()
        .and()
        .csrf().ignoringAntMatchers("/h2-console/**")
        .and()
        .headers().frameOptions().sameOrigin()
        .and()
        .formLogin(Customizer.withDefaults())
      ;
    }
'
```

#### SPRING.FACTORIES

```r
Para que sirve el archivo xxx/src/main/resources/META-INF/spring.factories, describe ej. de uso;'
Para agregar clases factory en el proceso de construcción

Ejemplo: Para agregar listeners de eventos de inicio
'

Como agregar un event listener usando xxx/src/main/resources/META-INF/spring.factories;'
1. Crear el archivo org.springframework.context.ApplicationListener=com.m51.book01.starting.ApplicationStartingEventListener

2. Agregar la ruta de nuestro factory, ej.

<pre>
org.springframework.context.ApplicationListener=com.m51.book01.starting.ApplicationStartingEventListener
</pre>

3. Definir nuestro factory, en este caso un listener del evento ApplicationStartingEvent

<pre>
  package com.m51.book01.starting;
  import java.util.Date;
  import org.springframework.boot.context.event.ApplicationStartingEvent;
  import org.springframework.context.ApplicationListener;

  public class ApplicationStartingEventListener implements ApplicationListener<ApplicationStartingEvent> {
    @Override
    public void onApplicationEvent(ApplicationStartingEvent applicationStartingEvent) {
      System.out.println("Application Starting Event logged at " + new Date(applicationStartingEvent.getTimestamp()));
    }
  }
</pre>
'

Como agregar un archivo .properties;'
A traves de la anotación @PropertySource("classpath:xxxx.properties") el cual se guardara en el bean Environment,

1,Creamos el archivo src/main/resources/dbConfig.properties
<pre>
  user=sample
  password=11clave11
</pre>

2,Importamos el archivo (puede ser en cualquier clase, en este caso creamos su propia clase)
<pre>
  package com.m51.book01;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.context.annotation.Configuration;
  import org.springframework.context.annotation.PropertySource;
  import org.springframework.core.env.Environment;

  @Configuration
+ @PropertySource("classpath:dbConfig.properties")
  public class DBConfig {
    
+   @Autowired
+   private Environment env;

    @Override
    public String toString() {
+     return "Username: " + env.getProperty("user") + ", Password: " + env.getProperty("password");
    }
  }
</pre>

3, Mostramos
<pre>
  package com.m51.book01;
  import java.util.Date;
  import org.slf4j.Logger;
  import org.slf4j.LoggerFactory;
  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.WebApplicationType;
  import org.springframework.boot.autoconfigure.SpringBootApplication;
  import org.springframework.boot.context.event.ApplicationReadyEvent;
  import org.springframework.context.event.EventListener;


  @SpringBootApplication
  public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
      SpringApplication springApplication = new SpringApplication(Main.class);
      springApplication.setWebApplicationType(WebApplicationType.SERVLET);

      var context = springApplication.run(args);

+     DBConfig dbConfiguration = context.getBean(DBConfig.class);
+     log.info(dbConfiguration.toString());
    }


    @EventListener(ApplicationReadyEvent.class)
    public void applicationReadyEvent(ApplicationReadyEvent applicationReadyEvent) {
      var date = new Date(applicationReadyEvent.getTimestamp());
      var msg = "Application Ready Event generated at " + date;
      System.out.println(msg);
    }

  }
</pre>

4, Resultado
<pre>
... [restartedMain] com.m51.book01.Main : Username: m51, Password: 11clave11
</pre>
'

¿Como crear profiles y cambiar entre ellos?;'
1, Creamos el archivo src/main/resources/application-dev.properties
<pre>
  server.port=8082
</pre>

2, Creamos el archivo src/main/resources/application-test.properties
<pre>
  server.port=8081
</pre>

3, Definimos cual es el profile activo en src/main/resources/application.properties
<pre>
#Solo dejar una
  spring.profiles.active=dev
# spring.profiles.active=test
# spring.profiles.active=default
  server.port=8080
</pre>

4, Al cambiar spring.profiles.active podemos ver que nuestro servidor levanta en un puerto u otro.
'
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->


### Curso Spring 6

#### Dependency Injection with Spring

```r
¿Como obtener una dependencia del contexto?, ¿del main? y ¿de otro bean?;'
Desde el main
<pre>
//Dentro del método main
  var context = springApplication.run(args);
  var beerCtrl = context.getBean(BeerController.class);
  beerCtrl.sayHi()
</pre>

Desde de un test

--bean
<pre>
  package com.m51.book01.beer;
  import org.springframework.stereotype.Controller;
  @Controller
  public class BeerController {
    public String sayHi() {
      return "Hi from BeerController";
    }
  }
</pre>


--desde ApplicationContext y anotación @Autowired
<pre>
  package com.m51.book01.beer;
  import static org.junit.jupiter.api.Assertions.fail;
  import org.junit.jupiter.api.Test;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.boot.test.context.SpringBootTest;
  import org.springframework.context.ApplicationContext;

  @SpringBootTest
  public class BeerControllerTest {

//CUIDADO2: No poner una propiedad @Autowired privada sin constructor, ya
que si Spring no puede verla la buscara por reflexión y esto es lento.
//CUIDADO: La forma ideal de traer una dependencia, poniéndola privada
//e inyectándola por constructor.
+   @Autowired
+   ApplicationContext ctx;

+   @Autowired
+   BeerController beerCtrl;

    @Test
    public void getBeerCtrlFromCtx() {
+     var myCtrl = ctx.getBean(BeerController.class);
      if (myCtrl == null) {
        fail("BeerController no found");
      } else {
        System.out.println(myCtrl.sayHi());
      }
    }

    @Test
    public void getBeerCtrlFromAutowired() {
      var msg = beerCtrl.sayHi();
      if (msg == null) {
        fail("BeerController no found");
      }
    }

  }
</pre>
'

Como luciría creación de dependencia sin inyección de dependencias, ¿cual es el problema?;'
¿cual es el problema?
Tenemos ligada directamente la dependencia, "GreetingServiceImpl" podría ser MariaDB y si queremos cambiar a H2, en todos lados tendríamos que modificar el cual viola los principios SRP y OCP, por que tiene la responsabilidad de crear su dependencia y lo de "MyController", y esta abierto a cambios cuando queramos modificar esta dependencia.

<pre>
@Controller
public class MyController {

    private final GreetingService greetingService;

    public MyController() {
        this.greetingService = new GreetingServiceImpl();
    }

    public String sayHello(){
        System.out.println("Im in the controller");

        return greetingService.sayGreeting();
    }
}
</pre>'

inyección de dependencias sin Spring IoC: ¿cuales son las tres formas de hacerlo?, ¿Como luciría la recomendada?, ¿Problemas?;'
¿cuales son las tres formas de hacerlo?
--Por propiedad "miHightLevelClass.dep=inyectable"
--Por Setter
--Por Constructor = recomendable

¿Problemas?
Mas talacha, por que para hacerlo bien deberíamos checar nulos, y debemos de idear una forma de no contaminar nuestra clase de alto nivel, con detalles de ligar dependencias.

¿Como luciría la recomendada?
<pre>
  public class ConstructorInjectedController {
      private final GreetingService greetingService;

      public ConstructorInjectedController(GreetingService greetingService) {
          this.greetingService = greetingService;
      }

      public String sayHello(){
          return greetingService.sayGreeting();
      }
  }
</pre>
'

Bean primarios/secundarios: ¿Para que son?, ¿Codigo?;'
¿Para que son?
--Podemos tener varias implementaciones de una interface, e Spring no puede decidir por si solo cual usar, por lo que podemos darles prioridad, para que use la primaria.

¿Codigo?
-- En este caso inyectara
<pre>
----- Interface

public interface GreetingService {
  String sayGreeting();
}

----- Implementación Secundaria

import org.springframework.stereotype.Service;
@Service
public class GreetingServiceImpl implements GreetingService {
    @Override
    public String sayGreeting() {
        return "Hello Everyone From Base Service!!!";
    }
}

----- Implementación Primaria

import org.springframework.context.annotation.Primary;
@Primary
@Service
public class GreetingServicePrimary implements GreetingService {
    @Override
    public String sayGreeting() {
        return "Hello From the Primary Bean!!";
    }
}

----- Alto nivel

  @Controller
  public class ConstructorInjectedController {

//SI NO HUBIÉRAMOS DEFINIDO "@Primary" ESTA HUBIERA FALLADO
//ERROR: expected single matching bean but found 2

+     private final GreetingService greetingService;

      public ConstructorInjectedController(GreetingService greetingService) {
          this.greetingService = greetingService;
      }

      public String sayHello(){
          return greetingService.sayGreeting();
      }
  }

-----------------Salida

Hello From the Primary Bean!!

</pre>
'

Qualifier, ¿Para que? ¿Que nombre se ponen los beans por default?, ¿Codigo?;'
¿Para que?
--Ligar dependencias por nombre

¿Que nombre se ponen los beans por default?
--El nombre de clase en minúsculas



¿Codigo?
<pre>
------------------- Dependencia
  import org.springframework.stereotype.Service;
  @Service
  public class GreetingServiceImpl implements GreetingService {
      @Override
      public String sayGreeting() {
          return "Hello Everyone From Base Service!!!";
      }
  }

------------------- Alto nivel

  @Controller
  public class ConstructorInjectedController {
      private final GreetingService greetingService;

+     public ConstructorInjectedController(@Qualifier("greetingServiceImpl") GreetingService greetingService) {
          this.greetingService = greetingService;
      }

      public String sayHello(){
          return greetingService.sayGreeting();
      }
  }
</pre>




¿Como cambiar el nombre del bean?
--Dentro de la anotacion como @Service("x") o @Controller("x"), etc.

<pre>
------------------- Dependencia
  @Service("lightBeer")
  public class BeerServiceImpl2 implements BeerService {
    public String sayHi() {
      return "Hi from BeerServiceImpl2";
    }
  }

------------------- Alto nivel

  @SpringBootTest
  public class BeerControllerTest {

//NECESARIOS LOS DOS
+   @Autowired
+   @Qualifier("blackBeer")
    BeerService beerSrv;

    @Test
    public void getBeerCtrlFromAutowired() {
      beerSrv.sayHi();
    }

  }
</pre>
'

profiles y dependencias: ¿Ej. caso de uso?, ¿Como podemos definir el bean default?, ¿Codigo: como inyectar una dependencias de acuerdo con el profile?;'
¿Ej. caso de uso?
--De a cuerdo al ambiente vamos a querer cargar una DB u otra.

¿Como podemos definir el bean default?
--@Profile({"EN", "default"})

¿Codigo: como inyectar una dependencias de acuerdo con el profile?
--Podemos ver como a pesar de tener el mismo Qualifier, podemos inyectar una dependencia u otra cambiando el profile, y este profile lo podemos cambiar desde el .properties con @ActiveProfiles("EN") en los tests

<pre>
--------------Impl Uno

  package com.m51.book01.services.i18n;
  import org.springframework.context.annotation.Profile;
  import org.springframework.stereotype.Service;
  import com.m51.book01.services.GreetingService;

+ @Profile("EN")
  @Service("i18NService")
  public class EnglishGreetingService implements GreetingService {
      @Override
      public String sayGreeting() {
          return "Hello World - EN";
      }
  }

--------------Impl Dos

+ @Profile("ES")
  @Service("i18NService")
  public class SpanishGreetingService implements GreetingService {
      @Override
      public String sayGreeting() {
          return "Hola Mundo - ES";
      }
  }

--------------Ctrl

  @Controller
  public class Myi18NController {
      private final GreetingService greetingService;

      public Myi18NController(@Qualifier("i18NService") GreetingService greetingService) {
          this.greetingService = greetingService;
      }

      public String sayHello(){
          return greetingService.sayGreeting();
      }

  }

--------------application.properties

spring.profiles.active=ES


--------------TEST
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

//@ActiveProfiles("EN")
@SpringBootTest
public class Myi18NControllerTestEN {

  @Autowired
  Myi18NController myi18NController;

  @Test
  void sayHello() {
    System.out.println(myi18NController.sayHello());
  }
}
<pre>
'
```

#### Spring Bean Life Cycle.mp4

```r
Crea un componente que ejecute indique cada uno de los ciclos que esta;'<pre>
package com.m51.book01.services;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import com.m51.book01.controllers.MyController;

@Component
public class LifeCycleDemoBean implements InitializingBean, DisposableBean, BeanNameAware,
        BeanFactoryAware, ApplicationContextAware, BeanPostProcessor {

    public LifeCycleDemoBean() {
        System.out.println("## Im in the LifeCycleBean Constructor ##");
    }

    private String javaVer;

    @Value("${java.specification.version}")
    public void setJavaVer(String javaVer) {
        this.javaVer = javaVer;
        System.out.println("## 1 Properties Set. Java Ver: " + this.javaVer );
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("## 2 BeanNameAware My Bean Name is: " + name);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("## 3 BeanFactoryAware - Bean Factory has been set");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("## 4 ApplicationContextAware - Application context has been set");
    }

    @PostConstruct
    public void postConstruct(){
        System.out.println("## 5 postConstruct The Post Construct annotated method has been called");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("## 6 afterPropertiesSet Populate Properties The LifeCycleBean has its properties set!");
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("## 7.1.N: postProcessBeforeInitialization: " + beanName);

        if (bean instanceof MyController){
            MyController myController = (MyController) bean;
            System.out.println("Calling before init");
            myController.beforeInit();
        }

        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("## 7.2.N: postProcessAfterInitialization: " + beanName);

        if (bean instanceof MyController){
            MyController myController = (MyController) bean;
            System.out.println("Calling after init");
            myController.afterInit();
        }

        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("## 8 The @PreDestroy annotated method has been called");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("## 9 DisposableBean.destroy The Lifecycle bean has been terminated");
    }
}
</pre>'
```

#### 4. Introduction to Restful Web Services

```r
¿Que es webFlux?;'Para la creacion de servicios web reactivos'

¿Como se llama las librería para consumir web services reactivos y normales?;'
reactivos: WebClient
normales: RestTemplates
'

¿Como se llama el proceso de convertir un objeto/instancia a json y el inverso? ¿cual va con cual Encoding y Decoding?;"
(json->obj)
Marshalling = Decoding

(obj->json)
--UnMarshalling = Encoding
"
```

#### 5. Lombok

```r
Lombok: ¿Motivación de su creación?, ¿Que es? ¿Para que es?, ¿Como funciona?, ¿Que es Delombock?, Link lista de características, Codigo ejemplo;'
¿Motivación de su creación?
--Java es criticado con justificación de que es verboso, ej. muchas veces se debe escribir getters/setters, asi que trata de resolver esto.

¿Que es?
--Es una librería que se puede incluir en un spring starter, pero requiere de un plugin del lado del IDE para funcionar

¿Para que es?
--Generar codigo automáticamente a traves de anotaciones

¿Como funciona?
--Genera el codigo en pre-compilación, es por eso que Los IDEs necesitan el plugin, porque ven que ese codigo no existe, pero si existirá

¿Que es Delombock?
--El proceso de generar el codigo que existirá dentro de nuestro codigo actual, algo asi como el autocomplete.

Link lista de características
--https://projectlombok.org/features/

Codigo
<pre>

+ @Slf4j
  public class Main {

//ESTA LINEA YA NO LA NECESITAMOS HACER NOSOTROS, LA CREA TRAS BAMBALINAS LOMBOK
+   //private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
      ...
      log.info("");
    }

  }
</pre>
'
```

#### 6. Spring MockMVC

```r
#nada especial
```

### 7. Spring MockMVC Test with Mockito

```r
test splices: ¿Porque usarlos (en SpringMVC)?, ¿Que es?;'
¿Que es?
--Son pequeñas partes del spring context, podemos instancias estas partes mas pequeñas, que son mas rápidas de levantar

¿Porque usarlos?
--Para hacer tests de nuestros end-points vamos a requerir del springContext, pero es muy pesado para test unitarios, asi que podemos hacerlos un poco mas ligeros, usando test splices los cuales tenemos varios, por ejemplo, podemos usar el splice @WebMvcTest para obtener
'

#como testear un endpiont, testear endpoint, probar endpoint
Como probar un endpoint (Solo ver que retorne un ok);'
Necesitamos
--Anotar la clase con @WebMvcTest y le pasamos el ctrl a probar en este caso BeerController.class
--MockMvc es el que "hará" las peticiones
--@MockBean para llenar las dependencias de nuestro controlador

<pre>
  package com.mario.mvc.controller;

  import com.mario.mvc.services.BeerService;
  import org.junit.jupiter.api.Test;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
  import org.springframework.boot.test.mock.mockito.MockBean;
  import org.springframework.http.MediaType;
  import org.springframework.test.web.servlet.MockMvc;
  import java.util.UUID;
  import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
  import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

  @WebMvcTest(BeerController.class)
  class BeerControllerTest {

      @Autowired
      MockMvc mockMvc;

      @MockBean
      BeerService beerService;

      @Test
      void getBeerById() throws Exception {
          mockMvc.perform(get("/api/v1/beer/" + UUID.randomUUID())
                  .accept(MediaType.APPLICATION_JSON))
                  .andExpect(status().isOk());
      }
  }  
</pre>


La implementación que probara sera esta


<pre>
  @RequestMapping("/api/v1/beer")
  public class BeerController {
      private final BeerService beerService;
      
      @RequestMapping(value = "{beerId}", method = RequestMethod.GET)
      public Beer getBeerById(@PathVariable("beerId") UUID beerId){
          log.debug("Get Beer by Id - in controller");
          return beerService.getBeerById(beerId);
      }
  }
</pre>
'

#configurar mock, simular respuesta, 

1,¿Como configurar un mock/dependencia para que retorne una determinada respuesta?
2,¿Que lo respondido tenga determinado formato (json)?
2,¿el json retornado tenga ciertos valores?;'

<pre>
  package guru.springframework.spring6restmvc.controller;

  import guru.springframework.spring6restmvc.services.BeerService;
  import guru.springframework.spring6restmvc.services.BeerServiceImpl;
  import lombok.val;
  import org.junit.jupiter.api.Test;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
  import org.springframework.boot.test.mock.mockito.MockBean;
  import org.springframework.http.MediaType;
  import org.springframework.test.web.servlet.MockMvc;
  import java.util.UUID;
  import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
  import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
  import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
  import static org.hamcrest.core.Is.is;
  import static org.mockito.ArgumentMatchers.any;
  import static org.mockito.BDDMockito.given;
  import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

  @WebMvcTest(BeerController.class)
  class BeerControllerTest {

      @Autowired
      MockMvc mockMvc;

      @MockBean
      BeerService beerService;

      private final BeerServiceImpl beerSrv = new BeerServiceImpl();

      @Test
      void getBeerById() throws Exception {

          val beerSpected = beerSrv.listBeers().get(0);
1+        given(beerService.getBeerById(any(UUID.class))).willReturn(beerSpected);

          mockMvc.perform(get("/api/v1/beer/" + testBeer.getId())
                  .accept(MediaType.APPLICATION_JSON))
                  .andExpect(status().isOk())
2+                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
3+                .andExpect(jsonPath("$.id", is(testBeer.getId().toString())))
3+                .andExpect(jsonPath("$.beerName", is(testBeer.getBeerName())))
          ;
      }
  }
</pre>'

Como probar el retorno de una lista en un endpoint;'
1.Indicamos que lista retornara al service (dependencia de nuestro ctrl)
2.Con "$.length()" podemos probar la cantidad retornada

<pre>
    @Test
    void testListBeers() throws Exception {

+1      given(beerService.listBeers()).willReturn(beerServiceImpl.listBeers());

        mockMvc.perform(get("/api/v1/beer")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
+2              .andExpect(jsonPath("$.length()", is(3)));
    }
</pre>
'

Como convertir de objeto a json;'
Spring ya tiene un bean ObjectMapper que podemos inyectar para esto, y conviene
usar este ya que este se puede varias variantes de configuración.

<pre>

  import com.fasterxml.jackson.core.JsonProcessingException;
  import com.fasterxml.jackson.databind.ObjectMapper;

  @WebMvcTest(BeerController.class)
  class BeerControllerTest {

      @Autowired
      ObjectMapper objectMapper;

      @Test
      void testCreateNewBeer() throws JsonProcessingException {
          Beer beer = beerServiceImpl.listBeers().get(0);
+         System.out.println(objectMapper.writeValueAsString(beer));
      }

  }
</pre>
'

# peticion post
¿Como probar un endpoint post?;'
1, Podemos hacerlo indicando que json debe retornar, recordar que mejor usar el bean ObjectMapper que ya tiene spring, para evitar temas de conversiones a json

<pre>
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

    @Test
    void testCreateNewBeer() throws Exception {
        Beer beer = beerServiceImpl.listBeers().get(0);
        beer.setVersion(null);
        beer.setId(null);

        given(beerService.saveNewBeer(any(Beer.class)))
            .willReturn(beerServiceImpl.listBeers().get(1));

+       mockMvc.perform(post("/api/v1/beer")
                .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
+1                      .content(objectMapper.writeValueAsString(beer)))
+               .andExpect(status().isCreated())
+               .andExpect(header().exists("Location"));
    }
</pre>
'

1,como probar una peticion put 2,Como comprobar que el metodo de un mock fue llamado;'
verify: sirve para asegurar que se mando a llamar el metodo del mock, en este caso una vez

<pre>
package guru.springframework.spring6restmvc.controller;

import com.fasterxml.jackson.databind.ObjectMapper;import guru.springframework.spring6restmvc.model.Beer;import guru.springframework.spring6restmvc.services.BeerService;import guru.springframework.spring6restmvc.services.BeerServiceImpl;import org.junit.jupiter.api.BeforeEach;import org.junit.jupiter.api.Test;import org.springframework.beans.factory.annotation.Autowired;import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;import org.springframework.boot.test.mock.mockito.MockBean;import org.springframework.http.MediaType;import org.springframework.test.web.servlet.MockMvc;import java.util.UUID;import static org.mockito.ArgumentMatchers.any;import static org.mockito.Mockito.verify;import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(BeerController.class)
class BeerControllerTest {

    @Autowired MockMvc mockMvc;
    @Autowired ObjectMapper objectMapper;
    @MockBean BeerService beerService;

    BeerServiceImpl beerServiceImpl;

    @BeforeEach
    void setUp() { beerServiceImpl = new BeerServiceImpl(); }

    @Test
    void testUpdateBeer() throws Exception {
        Beer beer = beerServiceImpl.listBeers().get(0);

+1       mockMvc.perform(put("/api/v1/beer/" + beer.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(beer)))
                .andExpect(status().isNoContent());

+2       verify(beerService).updateBeerById(any(UUID.class), any(Beer.class));
    }
}
</pre>'

1,Como probar una peticion delete, y 2,verificar que el argumento de metodo de un mock fue el esperado;'
1,Con mockMvc.perform(delete("/api

2,Con ArgumentCaptor<T>

3,También podemos usar el bean ArgumentCaptor

4,Asi luciría un capturar doble argumento

<pre>
import org.mockito.ArgumentCaptor;import static org.assertj.core.api.Assertions.assertThat;import static org.hamcrest.core.Is.is;import static org.mockito.ArgumentMatchers.any;import static org.mockito.BDDMockito.given;import static org.mockito.Mockito.verify;import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(BeerController.class)
class BeerControllerTest {
    @Autowired MockMvc mockMvc;
    @Autowired ObjectMapper objectMapper;
    @MockBean BeerService beerService;
    BeerServiceImpl beerServiceImpl;
    @BeforeEach
    void setUp() { beerServiceImpl = new BeerServiceImpl(); }

+3  @Captor
+3  ArgumentCaptor<Beer> beerArgumentCaptor;

    @Test
    void testDeleteBeer() throws Exception {
        Beer beer = beerServiceImpl.listBeers().get(0);

+1      mockMvc.perform(delete("/api/v1/beer/" + beer.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

+2      ArgumentCaptor<UUID> uuidArgumentCaptor = ArgumentCaptor.forClass(UUID.class);
+2      verify(beerService).deleteById(uuidArgumentCaptor.capture());
+2      assertThat(beer.getId()).isEqualTo(uuidArgumentCaptor.getValue());

+4      //verify(beerService).patchBeerById(uuidArgumentCaptor.capture(), beerArgumentCaptor.capture());
    }
}
</pre>'


```

#### Exception Handling with Spring MVC

```r
¿Que anotaciones podemos usar para manejar exepciones, a que nivel van?;'
@ExceptionHandler: En método de @Controller y manejara las excepciones en dicho controlador 

@ControllerAdvice: A nivel de clase, para definir un controlador global y cada metodo podemos definir las distintas excepciones que manejara
'

¿Que configuraciones tenemos en el .properties relacionados con las excepciones y para que son?;'
server.error.include-binding-errors - default: never
--Incluir series de mensajes del error

server.error.include-exception - default: false
--

server.error.include-message - default: never


server.error.include-stacktrace - default: never
--No recomendable mostrar el stacktrace ya que podremos mostrar info sensible

server.error.path - default: /error

server.error.whitelabel.enabled - default: true
'

¿1,Como crear una excepcion personalizada, 2,y indicar a un mock que la lance? 3,Como controlarlo solo a nivel de controlador, 4, Como controlarlo a nivel de Global;'

1, Simplemente que implemente la interface RuntimeException

<pre>
public class NotFoundException extends RuntimeException {
    public NotFoundException() { }
    public NotFoundException(String message) { super(message); }
    public NotFoundException(String message, Throwable cause) { super(message, cause); }
    public NotFoundException(Throwable cause) { super(cause); }
    public NotFoundException(String message, Throwable cause, boolean enableSuppression, boolean  writableStackTrace) { super(message, cause, enableSuppression, writableStackTrace); }
}
</pre>

2,

<pre>
//NOTA: Aun no estamos controlando la excepción, por lo que el test a este punto fallara
    @Test
    void getBeerByIdNotFound() throws Exception {
+2    given(beerService.getBeerById(any(UUID.class))).willThrow(NotFoundException.class);
      mockMvc.perform(get(BeerController.BEER_PATH_ID, UUID.randomUUID())).andExpect(status().isNotFound());
    }
</pre>

3, En el controlador que contiene el endpoint bajo test, podemos agregar un método anotado con @ExceptionHandler(MyEx.class), para controlarlo, ahora el test ya debería de pasar.

IMPORTANTE: Este es un método poco común, ya que podríamos caer en repetir codigo en multiples controladores

<pre>
  @RestController
  public class BeerController {

+   @ExceptionHandler(NotFoundException.class)
    public ResponseEntity handleNotFoundException(){
        return ResponseEntity.notFound().build();
    }

    //metodo que lanza la excepcion
    @GetMapping(value = BEER_PATH_ID) public Beer getBeerById(@PathVariable("beerId") UUID beerId){...}
  }
</pre>

4, Esta es alternativa, asi que no convidarla con el punto 3.


<pre>
    import org.springframework.http.ResponseEntity; import org.springframework.web.bind.annotation.ControllerAdvice; import org.springframework.web.bind.annotation.ExceptionHandler;

    @ControllerAdvice
    public class ExceptionController {

      @ExceptionHandler(NotFoundException.class)
      public ResponseEntity handleNotFoundException(){
        return ResponseEntity.notFound().build();
      }

    }
</pre>
'

Como crear una excepcion personalizada que pueda en automatico pueda mostrarse como respuesta http;'
<pre>
    import org.springframework.http.HttpStatus;
    import org.springframework.web.bind.annotation.ResponseStatus;

+   @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Value Not Found")
    public class NotFoundException extends RuntimeException {
      ...

</pre>
'

Como retornar una exepcion o un objeto, y ejemplo;'
Usando java.util.Optional

Service
<pre>
  @Override
  public Optional<Beer> getBeerById(UUID id) {
    return Optional.of(beerMap.get(id));
  }
</pre>

Controller: podemos usar el metodo orElseThrow
<pre>
  @GetMapping(value = BEER_PATH_ID)
  public Beer getBeerById(@PathVariable("beerId") UUID beerId){
    return beerService.getBeerById(beerId).orElseThrow(NotFoundException::new);
  }
</pre>
'

```

#### Spring Data JPA with Spring MVC

```r
JPA vs Spring Data JPA vs Hibernate;'
JPA: Es el estándar (Java Persistente API)

Hibernate: Es una implementación de JPA

Spring Data JPA: Usa como base a Hibernate, puede utilizar otro aparte de Hibernate, pero este es el viejo confiable
'

Como generar un ID de tipo UUID, tamaño 36, tipo varchar, que no se pueda actializar, y no sea nulo;'<pre>
  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Column(length = 36, columnDefinition = "varchar", updatable = false, nullable = false)
  private UUID id;
</pre>'

Como probar Spring JPA;'
Podemos hacerlo con @DataJpaTest

Entity

<pre>
  import jakarta.persistence.*; import lombok.*; import org.hibernate.annotations.GenericGenerator;import java.math.BigDecimal;import java.time.LocalDateTime;import java.util.UUID;

  @Getter @Setter @Builder @Entity @NoArgsConstructor @AllArgsConstructor
  public class Beer {

      @Id
      @GeneratedValue(generator = "UUID")
      @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
      @Column(length = 36, columnDefinition = "varchar", updatable = false, nullable = false)
      private UUID id;

      @Version
      private Integer version;
      ...
  }
</pre>

Repository

<pre>
  import org.springframework.data.jpa.repository.JpaRepository; import java.util.UUID;

  public interface BeerRepository extends JpaRepository<Beer, UUID> {

  }
</pre

Test

<pre>
import org.junit.jupiter.api.Test;import org.springframework.beans.factory.annotation.Autowired;import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class BeerRepositoryTest {

    @Autowired
    BeerRepository beerRepository;

    @Test
    void testSaveBeer() {
        Beer savedBeer = beerRepository.save(Beer.builder()
                        .beerName("My Beer")
                .build());

        assertThat(savedBeer).isNotNull();
        assertThat(savedBeer.getId()).isNotNull();
    }
}  
</pre
'

mapstruct: 1,¿Que y para que es?, 2,¿Porque usarlo?, 3,¿Como se configura? 4,¿Codigo ejemplo?, 5,¿Como comprobar que funciona bien?;'
1,¿Que y para que es?
--Es una libreria parecida a lombok, que genera codigo para convertir objetos parecidos, por ejemplo, para convertir de Entity a DTO, o viceversa.

¿Porque usarlo?
--Comunmente estas convercines se ponen en el mismo Entity o DTO, pero eso romperia el principio de unica responsabilidad, por lo que es recomendable crear un objeto especialozado (Mappers) para que hagan este trabajo

3,¿Como se configura?
--En el archivo pom.xml

2.1.Agregar version a usar, ya que debe de coincidir es facil tener conflicto entre lombok y spring
2.2.Agregar la dependencia org.mapstruct
2.3.Configuramos un maven-plugin para incluir mapstruct en la pre-compilacion

<pre>
    <project ... >
      <properties>
          <java.version>XXX</java.version>
2.1       <org.mapstruct.version>1.5.2.Final</org.mapstruct.version>
      </properties>
      <dependencies>
2.2     <dependency>
            <groupId>org.mapstruct</groupId>
            <artifactId>mapstruct</artifactId>
            <version>${org.mapstruct.version}</version>
        </dependency>
      </dependencies>
      <build>
        <plugins>
2.3        <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.10.1</version>
                <configuration>
                    <source>17</source>
                    <target>17</target>
                    <annotationProcessorPaths>
                        <path>
                            <groupId>org.mapstruct</groupId>
                            <artifactId>mapstruct-processor</artifactId>
                            <version>${org.mapstruct.version}</version>
                        </path>
                        <path>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                            <version>${lombok.version}</version>
                        </path>
                        <path>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok-mapstruct-binding</artifactId>
                            <version>0.2.0</version>
                        </path>
                    </annotationProcessorPaths>
                    <compilerArgs>
                        <compilerArg>-Amapstruct.defaultComponentModel=spring</compilerArg>
                    </compilerArgs>
                </configuration>
          </plugin>
        </plugins>
    </build>
    ...
  </project>
</pre>


4,¿Codigo ejemplo?
--Ya con el la dependecia agregada y configurada podemos crear nuestros mappers de forma "automatica".

<pre>
  package .mappers; import .entities.Beer; import .model.BeerDTO;
  import org.mapstruct.Mapper;

  @Mapper
  public interface BeerMapper {

      Beer beerDtoToBeer(BeerDTO dto);

      BeerDTO beerToBeerDto(Beer beer);

  }
</pre>

5,¿Como comprobar que funciona bien?
--Descompilando el jar deberiemos de tener BeerMapperImpl.class el cual se medio ve como pasas la propiedades de un objeto a otro.
'
```

#### 10 - Data Validation

```r
#validar datos, validad info.
¿Como validamos datos con Spring?;'
Usando Java Bean Validation, es el standart asi que apegarse a el.
'

Java Bean Validation: 1.¿Link de lista de validaciones?, 2.¿Como agregar dependencia?, 3.¿Como validar?, 4.¿Que exepcion lanza si no pasa la validacion?,5.¿Ejemplo con prueba integral?;'
1.¿Link de lista de validaciones?
--https://docs.jboss.org/hibernate/beanvalidation/spec/2.0/api/javax/validation/constraints/package-summary.html


2.¿Como agregar dependencia?
--En el pom

<pre>
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-validation</artifactId>
  </dependency>
</pre>


3.¿Como validar?
Con la anotacion en el controlador

<pre>
+ import org.springframework.validation.annotation.Validated;

  @RestController
  public class BeerController {
    
+   public ResponseEntity handlePost(@Validated @RequestBody BeerDTO beer){
      ...
    }

  }
</pre>

4.¿Que exepcion lanza si no pasa la validacion?
--org.springframework.web.bind.MethodArgumentNotValidException

5.¿Ejemplo con prueba integral?

5,1, Agregamos nuestras validaciones a travez de anotaciones

<pre>
@Data
public class BeerDTO {
    ...
    @jakarta.validation.constraints.NotBlank
    @jakarta.validation.constraints.NotNull
    private String beerName;
    ...

</pre>


5,2, Indicamos que corran las validaciones en el controlador con @Validated
--IMPORTANTE: por default spring no contolara la excepcion, por lo tanto no mostrara nada de informacion, para hacerlo debemos de controlar la execepcion

<pre>
  public ResponseEntity handlePost(@Validated @RequestBody BeerDTO beer){
</pre>


5,3,Controlamos la excepcion y la formateamos para que la muestre de forma mas amigable

<pre>
  import org.springframework.http.ResponseEntity; import org.springframework.web.bind.MethodArgumentNotValidException; import org.springframework.web.bind.annotation.ControllerAdvice; import org.springframework.web.bind.annotation.ExceptionHandler; import java.util.HashMap; import java.util.List; import java.util.Map; import java.util.stream.Collectors;
  import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

  @ControllerAdvice
  public class CustomErrorController {

      @ExceptionHandler(MethodArgumentNotValidException.class)
      ResponseEntity handleBindErrors(MethodArgumentNotValidException exception){

          List errorList = exception.getFieldErrors().stream()
                  .map(fieldError -> {
                      Map<String, String > errorMap = new HashMap<>();
                      errorMap.put(fieldError.getField(), fieldError.getDefaultMessage());
                      return errorMap;
                  }).collect(Collectors.toList());

          return ResponseEntity.badRequest().body(errorList);
      }
  }
</pre>

5,4, Agregamos test unitario que checara que la exepcion es lanzada y de forma correcta

1+:Obtenemos cervaza a agregar desde el mismo repo
2+:Esperamos que sea un badRequest
3+:mostramos el json retornado solo para poder verlo nosotros mismos.

<pre>
  @Test
  void testCreateBeerNullBeerName() throws Exception {

      BeerDTO beerDTO = BeerDTO.builder().build();

1+    given(beerService.saveNewBeer(any(BeerDTO.class))).willReturn(beerServiceImpl.listBeers().get(1));

      MvcResult mvcResult = mockMvc.perform(post(BeerController.BEER_PATH)
                      .accept(MediaType.APPLICATION_JSON)
                      .contentType(MediaType.APPLICATION_JSON)
                      .content(objectMapper.writeValueAsString(beerDTO)))
2+            .andExpect(status().isBadRequest())
              .andExpect(jsonPath("$.length()", is(2)))
              .andReturn();

3+    System.out.println(mvcResult.getResponse().getContentAsString());
      //[{"beerName":"must not be blank"},{"beerName":"must not be null"}]
  }
</pre>
'

Java Bean Validation del lado de BD: ¿Porque?, ¿como hacerlo?, ¿Que excepcion lanza?, ¿Como probar que se lanza una exepcion?, ¿como manejar un error del lado del entity/db 1? ¿Como manejar un error del lado del entity/db mas amigable?;'
¿Porque?
--Validar la entrada tanto de fuera como hacia la bd es mas seguro, que hacerlo en uno de estos.

¿como hacerlo?
--Agregando las validaciones en el entity (bd) como en el dto (web), es por eso que es importante separar el dto y el entity

¿Que excepcion lanza?
--jakarta.validation.ConstraintViolationException

¿Como probar que se lanza una exepcion?

1+ ==

2+ Para que "guarde" a la bd y asi corra la validaciones

<pre>

public class Beer {
  @Id...private UUID id;

  @NotNull @NotBlank @Size(max = 50) @Column(length = 50)
  private String beerName;
}
--------------
  @Test
  void testSaveBeerNameTooLong() {
1+  assertThrows(ConstraintViolationException.class, () -> {
      Beer savedBeer = beerRepository.save(Beer.builder()
        .beerName("My Beer 0123345678901233456789012334567890123345678901233456789012334567890123345678901233456789")
        .beerStyle(BeerStyle.PALE_ALE)
        .upc("234234234234")
        .price(new BigDecimal("11.99"))
        .build());

2+    beerRepository.flush();
    });
  }
  
</pre>


¿Como manejar un error del lado del entity/db 1?
--Importante: Esta no muestra un json amigable, el 2 si lo hace

DUDA: ¿Como se relcionan las clases org.springframework.transaction.TransactionSystemException y jakarta.validation.ConstraintViolationException?

<pre>
  @ControllerAdvice
  public class CustomErrorController {
    @ExceptionHandler
    ResponseEntity handleJPAViolations(TransactionSystemException exception){
        return ResponseEntity.badRequest().build();
    }
</pre>

¿Como manejar un error del lado del entity/db 2?

<pre>

  @ExceptionHandler
  ResponseEntity handleJPAViolations(TransactionSystemException exception){
      ResponseEntity.BodyBuilder respB = ResponseEntity.badRequest();

      if(exception.getCause().getCause() instanceof ConstraintViolationException){
          val ve = (ConstraintViolationException) exception.getCause().getCause();
          List errors = ve.getConstraintViolations().stream()
              .map(cv -> {
                  Map<String, String> errMap = new HashMap<String, String>();
                  errMap.put(cv.getPropertyPath().toString(), cv.getMessage());
                  return errMap;
              }).collect(Collectors.toList());
          
              return respB.body(errors);
      }
      return respB.build();
  }
  -----------------
  //EL CUAL EL TEST DEBERIA DE IMPRIMIR

    @Test
    void testPatchBeerBadName() throws Exception {
      Beer beer = beerRepository.findAll().get(0);

      Map<String, Object> beerMap = new HashMap<>();
      beerMap.put("beerName", "New Name 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

      val resp = mockMvc.perform(patch(BeerController.BEER_PATH_ID, beer.getId())
                      .contentType(MediaType.APPLICATION_JSON)
                      .accept(MediaType.APPLICATION_JSON)
                      .content(objectMapper.writeValueAsString(beerMap)))
              .andExpect(status().isBadRequest()).andReturn()
      ;
      
      val response = resp.getResponse().getContentAsString();

      System.out.println(response); //[{"beerName":"size must be between 0 and 50"}]

  }
</pre>
'
```

#### 11 - MySQL with Spring Boot

```r
Mysql y Spring: 1.¿Que y para que es hikari? 2.¿Errores comunes? 3.¿Como configurar conexion a mysql junto a hikari (usando .properties)?, 4,¿Como hacer que spring genere un .sql, con el ddl que genero al arrancar?;'

1.¿Que y para que es hikari?
--Es la libreria que se encarga de administrar el pool de conexiones, hay otros, pero hikari es el viejo confiable



2.¿Errores comunes?

--No activar el profile

<pre>
    spring.profiles.active=debug,mysqlConf
</pre>

--Tener bien sincronizado los tamaños de datos

<pre>
    @Column(length = 36, columnDefinition = "varchar(36)"  
</pre>

--El tipo de datos, por default guardara binario en UUID, pero eso hara que falle

<pre>
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID id;  
</pre>



3.¿Como configurar conexion a mysql junto a hikari (usando .properties)?

--Recomendable tenerlo en su propio archivo ej. ./resources/application-mysqlConf.properties

<pre>
    spring.datasource.username=restadmin
    spring.datasource.password=password
    spring.datasource.url=jdbc:mysql://127.0.0.1:3306/restdb?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC
    spring.jpa.database=mysql
    spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
    spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQLDialect
    spring.jpa.hibernate.ddl-auto=update

    spring.datasource.hikari.pool-name=RestDB-Pool
    #number of connections
    spring.datasource.hikari.maximum-pool-size=5
    #uso de cache
    spring.datasource.hikari.data-source-properties.cachePrepStmts=true
    spring.datasource.hikari.data-source-properties.prepStmtCacheSize=250
    spring.datasource.hikari.data-source-properties.prepStmtCacheSqlLimit=2048
    spring.datasource.hikari.data-source-properties.useServerPrepStmts=true
    spring.datasource.hikari.data-source-properties.useLocalSessionState=true
    spring.datasource.hikari.data-source-properties.rewriteBatchedStatements=true
    spring.datasource.hikari.data-source-properties.cacheResultSetMetadata=true
    spring.datasource.hikari.data-source-properties.cacheServerConfiguration=true
    spring.datasource.hikari.data-source-properties.elideSetAutoCommits=true
    spring.datasource.hikari.data-source-properties.maintainTimeStats=false


    #Show SQL, mostrar sql
    spring.jpa.properties.hibernate.show_sql=true

    #Format SQL
    spring.jpa.properties.hibernate.format_sql=true

    #Show bind values
    logging.level.org.hibernate.orm.jdbc.bind=trace
</pre>



4,¿Como hacer que spring genere un .sql, con el ddl que genero al arrancar?

<pre>
  spring.jpa.properties.jakarta.persistence.schema-generation.scripts.action=drop-and-create
  spring.jpa.properties.jakarta.persistence.schema-generation.scripts.create-source=metadata
  spring.jpa.properties.jakarta.persistence.schema-generation.scripts.drop-target=drop-and-create.sql
  spring.jpa.properties.jakarta.persistence.schema-generation.scripts.create-target=drop-and-create.sql
</pre>
'
```

#### 12 - Flyway Migrations

```r
Flyway: 0, Documentacion, 1.¿Que es?, 2.¿Como desactivarlo y porque lo hariamos? 3.¿Como agregar dependencia via maven y configurarlo-usarlo?;'
0.Documentacion
--https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto.data-initialization.migration-tool.flyway

1.¿Que es?
--Liberia para hacer migraciones en base de datos y es competencia de Liquibase, y ambos pueden funcionar integrados a spring via gradle o maven, como tambien funcionar de forma independiente.

====================

2.¿Como desactivarlo y porque lo hariamos?
--Desde el .properties con "spring.flyway.enabled=false", y lo hariamos para correr las pruebas unitarias, ej. tener mysql para produccion y h2 para las pruebas.


====================

3.¿Como agregar su dependencia via maven?

3.1.Agregamos dependencia En este caso usamos mysql

<pre>
  <dependency>
      <groupId>org.flywaydb</groupId>
      <artifactId>flyway-mysql</artifactId>
  </dependency>
</pre>

3.2.Agregamos src/main/resources/db/migration/V1__init-mysql-database.sql, el cual es impotante la ubicacion como la nomenclatura de "VN__xxx.sql", donde N indicara el orden de ejecucion.

<pre>
--V1__init-mysql-database.sql

    drop table if exists beer;

    drop table if exists customer;

    create table beer (
      id varchar(36) not null,
      ...
      primary key (id)
    ) engine=InnoDB;

    create table customer (...) engine=InnoDB;

</pre>
'
```

#### 13 - CSV File Uploads

```r
csv: 0.¿Cuales son?, 0.¿Explicacion: Como importar de csv a bd?, 0.¿Codigo: Como importar de csv a bd ref:[v?fnj-u+73?];'
1.¿Cuales son?
--los documentos de datos separados por comas y renglones

======================

2.¿Explicacion: Como importar de csv a bd?

--Usandlo la libreria opencsv (hay otras)
--Nuestro csv necesita que el primer renglon sea el nombre del la columna
--Crear un POJO y con opencsv para mapaearlo y pasarlo a un List
--En el inicio de Spring (CommandLineRunner) pasamos esta lista a BD con un for y save

======================

3.¿Codigo: Como importar de csv a bd?

3.1.Importamos libreria

<pre>
  <dependency>
      <groupId>com.opencsv</groupId>
      <artifactId>opencsv</artifactId>
      <version>5.7.0</version>
  </dependency>
</pre>

3.2.Ponemos en csv en "src/main/resources/csvdata/beers.csv", el cual debe de llevar en el la primera fila el nombre de las clumnas, y las demas los registros

<pre>
"row","count.x","abv","ibu","id","beer","style","brewery_id","ounces","style2","count.y","brewery","city","state","label"
"1",1,0.05,NA,1436,"Pub Beer","American Pale Lager",408,12,NA,409,"10 Barrel Brewing Company","Bend","OR","Pub Beer (10 Barrel Brewing Company)"
...
</pre>

3.3.Creamos el mapper que con ayuda de openCsv solo deberemos de empatar los nombres

<pre>
+ import com.opencsv.bean.CsvBindByName;
  import lombok.AllArgsConstructor; .Data; .NoArgsConstructor;

  @Data @NoArgsConstructor @AllArgsConstructor
  public class BeerCSVRecord {

      @CsvBindByName
      private Integer row;

      @CsvBindByName(column = "count.x")
      private Integer count;

      ...

  }
</pre>

3.4.Creamos el List con los registros del csv, tambien con la ayuda de opencsv

<pre>
  import com.opencsv.bean.CsvToBeanBuilder; import org.springframework.stereotype.Service; import java.io.File; import java.io.FileNotFoundException; import java.io.FileReader; import java.util.List;

  import .BeerCSVRecord;

  @Service
  public class BeerCsvServiceImpl implements BeerCsvService {
      @Override
      public List<BeerCSVRecord> convertCSV(File csvFile) {

          try {
              List<BeerCSVRecord> beerCSVRecords = new CsvToBeanBuilder<BeerCSVRecord>(new FileReader(csvFile))
                      .withType(BeerCSVRecord.class)
                      .build().parse();
              return beerCSVRecords;
          } catch (FileNotFoundException e) {
              throw new RuntimeException(e);
          }
      }
  }
</pre>

3.5. En un CommandLineRunner creamos y corremos el metodo que pase esta lista a la BD

<pre>

@Component @RequiredArgsConstructor
public class BootstrapData implements CommandLineRunner {
  private final BeerCsvService beerCsvService;

  @Transactional @Override
  public void run(String... args) throws Exception {
      loadCsvData();
  }

  private void loadCsvData() throws FileNotFoundException {
        if (beerRepository.count() < 10){
            File file = ResourceUtils.getFile("classpath:csvdata/beers.csv");

            List<BeerCSVRecord> recs = beerCsvService.convertCSV(file);

            recs.forEach(beerCSVRecord -> {
                BeerStyle beerStyle = switch (beerCSVRecord.getStyle()) {
                    case "American Pale Lager"          -> BeerStyle.LAGER;
                    case "American(APA)", "American S"  -> BeerStyle.ALE;
                    case "American IPA"                 -> BeerStyle.IPA;
                    case "Fruit", "Winter", "Berliner"  -> BeerStyle.WHEAT;
                    default -> BeerStyle.PILSNER;
                };

                beerRepository.save(Beer.builder()
                                .beerName(StringUtils.abbreviate(beerCSVRecord.getBeer(), 50))
                                .beerStyle(beerStyle)
                                .price(BigDecimal.TEN)
                                .upc(beerCSVRecord.getRow().toString())
                                .quantityOnHand(beerCSVRecord.getCount())
                        .build());
            });
        }
    }

</pre>
'
```

#### 14. Query Parameters with Spring MVC

```r
Query Parameters: ¿Que es y diferencia con los url parameters?, ¿Como se agregan a un test unitario?, ¿Como se agregan a un Controlador?, ¿Como hacer un test unitario de JPA (extends JpaRepository<T, E>) usando H2 con datos en un csv?;'
¿Que es?
--Son los que pasan por url de esta forma ?name=mario&alive=false
--url parameters = /beer/1/clients

=========================

¿Como se agregan a un test unitario?
<pre>
  @Test
  void tesListBeersByName() throws Exception {
      mockMvc.perform(get(BeerController.BEER_PATH)
+       .queryParam("beerName", "IPA"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.size()", is(100)))
      ;
  }
</pre>


=========================


¿Como se agregan a un Controlador?

--Con @RequestParam, tomara el nombre del argumento del metodo
 
<pre>
    @GetMapping(value = BEER_PATH)
+   public List<BeerDTO> listBeers(@RequestParam(required = false) String beerName){
        return beerService.listBeers();
    }
</pre>


=========================


¿Como hacer un test unitario de JPA (extends JpaRepository<T, E>) usando H2 con datos en un csv?

--Primero tener implementado ref[v?fnj-u+73?], y asegurarse que levanta bien con h2
--Segundo tener en cuenta que debemos usar el splice @DataJpaTest
--Tercero tener en cuenta que demos de traer/ejecutar los beans-dependencias necesarios
--Cuarto tener cuidado con los profiles, de no usar mysql u otro en los tests
--Con esto spring en automatico al correr el test metera los datos al h2 y JPA usara h2

<pre>
  import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
  import org.springframework.context.annotation.Import;
  import static org.assertj.core.api.Assertions.assertThat;

  import .bootstrap.BootstrapData;
  import .services.BeerCsvServiceImpl;

  @DataJpaTest
  @Import({BootstrapData.class, BeerCsvServiceImpl.class})
  class BeerRepositoryTest {

      @Autowired
      BeerRepository beerRepository;

      @Test
      void testGetBeerListByName() {
          List<Beer> list = beerRepository.findAllByBeerNameIsLikeIgnoreCase("%IPA%");

          assertThat(list.size()).isEqualTo(336);
      }
}

=======
//repositorio bajo test, NOTA: La implementacion la crea spring en tiempo de ejecucion

public interface BeerRepository extends JpaRepository<Beer, UUID> {

    List<Beer> findAllByBeerNameIsLikeIgnoreCase(String beerName);
}

</pre>
'
```

#### 15 - Paging and Sorting

```r
Paginacion: ¿Documentacion?, ¿Que se necesita?, ¿Condigo con Test unitario?;'
¿Documentacion?
--Creacion de Repository methods impl en runtime: https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
--Uso de Pageable y Sort dentro de repository: https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.special-parameters

====================================

¿Que se necesita?
--el numero de pagina y el tamaño, y retornar el total de paginas
--Para hacer la peticion a bd necesitamos org.springframework.data.domain.PageRequest, el cual necesitaremos pagina, tamaño y orderby
--En el curso no se crean metodos que tengan 

====================================

¿Condigo con Test unitario?

--Test unitario

<pre>
  @Test
  void testListBeers() throws Exception {
      given(beerService.listBeers(any(), any() , any(), any(), any()))
              .willReturn(beerServiceImpl.listBeers(null, null, false, null, null));

      mockMvc.perform(get(BeerController.BEER_PATH)
              .accept(MediaType.APPLICATION_JSON))
              .andExpect(status().isOk())
              .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//El org.springframework.data.domain.Page guarda el contenido en content
+             .andExpect(jsonPath("$.content.length()", is(3)));
  }
</pre>

Controlador
--Regresamos un Page de BeerDTO
--Necesitaremos pageNumber y pageSize
--

<pre>
  @GetMapping(value = BEER_PATH)
  public Page<BeerDTO> listBeers(@RequestParam(required = false) String beerName,
                                  @RequestParam(required = false) BeerStyle beerStyle,
                                  @RequestParam(required = false) Boolean showInventory,
                                  @RequestParam(required = false) Integer pageNumber,
                                  @RequestParam(required = false) Integer pageSize){
      return beerService.listBeers(beerName, beerStyle, showInventory, pageNumber, pageSize);
  }
</pre>

Service

--Como vemos construimos el page el cual tendra pageNumber y pageSize
--El repository solo cambia en que le podemos meter un Pageable y SpringJPA hara el resto.
--En este creamos el Sort en el service y lo pasamos por el Pageable al Repositorio.

<pre>
  import org.springframework.data.domain.Sort;

  public class BeerServiceJPA implements BeerService {
      private final BeerRepository beerRepository;
      private final BeerMapper beerMapper;
      private static final int DEFAULT_PAGE = 0, DEFAULT_PAGE_SIZE = 25;

      @Override
      public Page<BeerDTO> listBeers(String beerName, BeerStyle beerStyle, Boolean showInventory,
                                    Integer pageNumber, Integer pageSize) {

          PageRequest pageRequest = buildPageRequest(pageNumber, pageSize);

          Page<Beer> beerPage;

          if(StringUtils.hasText(beerName) && beerStyle == null) {
              beerPage = listBeersByName(beerName, pageRequest);
          } else if (!StringUtils.hasText(beerName) && beerStyle != null){
              beerPage = listBeersByStyle(beerStyle, pageRequest);
          } else if (StringUtils.hasText(beerName) && beerStyle != null){
              beerPage = listBeersByNameAndStyle(beerName, beerStyle, pageRequest);
          } else {
              beerPage = beerRepository.findAll(pageRequest);
          }

          if (showInventory != null && !showInventory) {
              beerPage.forEach(beer -> beer.setQuantityOnHand(null));
          }

          return beerPage.map(beerMapper::beerToBeerDto);

      }

      public PageRequest buildPageRequest(Integer pageNumber, Integer pageSize) {
          int queryPageNumber;
          int queryPageSize;

          if (pageNumber != null && pageNumber > 0) {
              queryPageNumber = pageNumber - 1;
          } else {
              queryPageNumber = DEFAULT_PAGE;
          }

          if (pageSize == null) {
              queryPageSize = DEFAULT_PAGE_SIZE;
          } else {
              if (pageSize > 1000) {
                  queryPageSize = 1000;
              } else {
                  queryPageSize = pageSize;
              }
          }

          Sort sort = Sort.by(Sort.Order.asc("beerName"));

          return PageRequest.of(queryPageNumber, queryPageSize, sort);
      }

      private Page<Beer> listBeersByNameAndStyle(String beerName, BeerStyle beerStyle, Pageable pageable) {
          return beerRepository.findAllByBeerNameIsLikeIgnoreCaseAndBeerStyle("%" + beerName + "%",
                  beerStyle, pageable);
      }
</pre>

Repositorio

<pre>
import guru.springframework.spring6restmvc.entities.Beer;import guru.springframework.spring6restmvc.model.BeerStyle;import org.springframework.data.domain.Page;import org.springframework.data.domain.Pageable;import org.springframework.data.jpa.repository.JpaRepository; import java.util.UUID;

public interface BeerRepository extends JpaRepository<Beer, UUID> {

    Page<Beer> findAllByBeerNameIsLikeIgnoreCase(String beerName, Pageable pageable);

    Page<Beer> findAllByBeerStyle(BeerStyle beerStyle, Pageable pageable);

    Page<Beer> findAllByBeerNameIsLikeIgnoreCaseAndBeerStyle(String beerName, BeerStyle beerStyle, Pageable pageable);
}
</pre>
'
```

#### 16 - Database Relationship Mappings

```r
JPA-Flyway: Como agregar una relacion muchos a muchos;'
1.Agregar la migracion ./resources/db/migrations/V4_catogory.sql

<pre>
drop table if exists category;
drop table if exists beer_category;

create table category (
    id                 varchar(36) NOT NULL PRIMARY KEY,
    description        varchar(50),
    created_date       timestamp,
    last_modified_date datetime(6) DEFAULT NULL,
    version            bigint      DEFAULT NULL
) ENGINE = InnoDB;

create table beer_category (
    beer_id     varchar(36) NOT NULL,
    category_id varchar(36) NOT NULL,
    primary key (beer_id, category_id),
    constraint pc_beer_id_fk FOREIGN KEY (beer_id) references beer (id),
    constraint pc_category_id_fk FOREIGN KEY (category_id) references category (id)
) ENGINE = InnoDB;  
</pre>

====================================

2. Agregar los entities con su relacion

<pre>
@Builder @Getter @Setter @NoArgsConstructor @AllArgsConstructor @Entity
public class Category {

    @Id
    private UUID id;
    ...

    @Builder.Default
    @ManyToMany
    @JoinTable(
      name = "beer_category",
      joinColumns = @JoinColumn(name = "category_id"),
      inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> beers = new HashSet<>();

}

................

//Es importante inicializar el Set/Coleccion, si no dara nullException

public class Beer {  
  @Builder.Default
  @ManyToMany
  @JoinTable(
    name = "beer_category",
    joinColumns = @JoinColumn(name = "beer_id"),
    inverseJoinColumns = @JoinColumn(name = "category_id")
  )
  private Set<Category> categories = new HashSet<>();

  public void addCategory(Category category){
      this.categories.add(category);
      category.getBeers().add(this);
  }

  public void removeCategory(Category category){
      this.categories.remove(category);
      category.getBeers().remove(category);
  }

</pre>

=============================

3.Crear el repositorio

<pre>
public interface CategoryRepository extends JpaRepository<Category, UUID> { }
</pre>

=============================

4. Crear el test unitario

package guru.springframework.spring6restmvc.repositories;

import guru.springframework.spring6restmvc.entities.Beer;
import guru.springframework.spring6restmvc.entities.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CategoryRepositoryTest {
    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    BeerRepository beerRepository;
    Beer testBeer;

    @BeforeEach
    void setUp() {
        testBeer = beerRepository.findAll().get(0);
    }

    @Transactional
    @Test
    void testAddCategory() {
        Category savedCat = categoryRepository.save(Category.builder()
                        .description("Ales")
                .build());

        testBeer.addCategory(savedCat);
        Beer saveBeer = beerRepository.save(testBeer);

        //En mi caso si mostro la misma ref/apuntador, pero entiendo no es 100% seguro, lo impor es que coincidan la info
        System.out.println(savedCat);
        System.out.println(saveBeer.getCategories());
    }
}
'
```

#### 19 - Spring RestTemplate

```r
RestTemplate: ¿Como hacer una peticion web, obtener: string,map,JsonNode?, ¿Dependencias necesaria?, ¿Como hacer nuestra propia implementacion para que lo transforme a nuestro objeto?;'
¿Dependencias necesaria?
--Solo con el starter web

En este caso solo estamos trayendo un string, mas adelante veremos como pasarlo a entity
<pre>
import lombok.RequiredArgsConstructor;import org.springframework.boot.web.client.RestTemplateBuilder;import org.springframework.data.domain.Page;import org.springframework.http.ResponseEntity;import org.springframework.stereotype.Service;import org.springframework.web.client.RestTemplate;import com.fasterxml.jackson.databind.JsonNode;

import .model.BeerDTO;

@RequiredArgsConstructor
@Service
public class BeerClientImpl implements BeerClient {

    private final RestTemplateBuilder restTemplateBuilder;

    @Override
    public Page<BeerDTO> listBeers() {
        RestTemplate restTemplate = restTemplateBuilder.build();

//string
        ResponseEntity<String> stringResponse = restTemplate.getForEntity("http://localhost:8080/api/v1/beer", String.class);
        System.out.println(stringResponse.getBody());

//map
        ResponseEntity<Map> mapResponse = restTemplate.getForEntity(BASE_URL + GET_BEER_PATH, Map.class);

//jsonNode
        ResponseEntity<JsonNode> jsonResponse =
                restTemplate.getForEntity(BASE_URL + GET_BEER_PATH, JsonNode.class);
      //navegamos entre el arbol/nodo
       jsonResponse.getBody().findPath("content")
                .elements().forEachRemaining(node -> {
                   System.out.println(node.get("beerName").asText());
                });
        

        return null;
    }
}  
</pre>

====================

¿Como hacer nuestra propia implementacion para que lo transforme a nuestro objeto?
--Con "@JsonCreator" le indicamos a jackson que use ese constructor para convertir

<pre>
import com.fasterxml.jackson.annotation.JsonCreator;import com.fasterxml.jackson.annotation.JsonIgnoreProperties;import com.fasterxml.jackson.annotation.JsonProperty;import org.springframework.data.domain.PageImpl;import org.springframework.data.domain.PageRequest;import org.springframework.data.domain.Pageable;import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true, value = "pageable")
public class BeerDTOPageImpl<BeerDTO> extends PageImpl<BeerDTO> {

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public BeerDTOPageImpl(@JsonProperty("content") List<BeerDTO> content,
                           @JsonProperty("number") int page,
                           @JsonProperty("size") int size,
                           @JsonProperty("totalElements") long total) {
        super(content, PageRequest.of(page, size), total);
    }

    public BeerDTOPageImpl(List<BeerDTO> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public BeerDTOPageImpl(List<BeerDTO> content) {
        super(content);
    }
}
</pre>

--Ahora podemos convertirlo

<pre>
  @Override
  public Page<BeerDTO> listBeers() {
      RestTemplate restTemplate = restTemplateBuilder.build();

      ResponseEntity<BeerDTOPageImpl> stringResponse =
              restTemplate.getForEntity(BASE_URL + GET_BEER_PATH , BeerDTOPageImpl.class);


      return null;
  }
</pre>
'

¿Como configurar RestTemplate para agregar la url base usando Anotaciones/beansDeConf?;'
--Creando un bean RestTemplateBuilder
--importante usar el "builder.uriTemplateHandler"

<pre>
import org.springframework.boot.autoconfigure.web.client.RestTemplateBuilderConfigurer; import org.springframework.boot.web.client.RestTemplateBuilder; import org.springframework.context.annotation.Bean; import org.springframework.context.annotation.Configuration; import org.springframework.web.util.DefaultUriBuilderFactory;

@Configuration
public class RestTemplateBuilderConfig {

    @Bean
    RestTemplateBuilder restTemplateBuilder(RestTemplateBuilderConfigurer configurer){
        RestTemplateBuilder builder = configurer.configure(new RestTemplateBuilder());
        DefaultUriBuilderFactory uriBuilderFactory = new DefaultUriBuilderFactory("http://localhost:8080");
        return builder.uriTemplateHandler(uriBuilderFactory);
    }
}
</pre>
'

RestTemplate: ¿Como agregar query Parameters y url parameters?;'
query Parameters
--Usando org.springframework.web.util.UriComponentsBuilder

<pre>
  @Override
  public Page<BeerDTO> listBeers(String beerName) {
      RestTemplate restTemplate = restTemplateBuilder.build();

+     UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromPath(GET_BEER_PATH);
      if (beerName != null) {
+         uriComponentsBuilder.queryParam("beerName", beerName);
      }

      val endpoint = uriComponentsBuilder.toUriString();
      val response = restTemplate.getForEntity(endpoint, BeerDTOPageImpl.class);
      return response.getBody();
  }
</pre>

========================

Url parameters
--Por medio de una "plantilla url"=/beer/{beerId};

<pre>
public class BeerClientImpl implements BeerClient {
    private final RestTemplateBuilder restTemplateBuilder;
    private static final String GET_BEER_BY_ID_PATH = "/api/v1/beer/{beerId}";

    @Override
    public BeerDTO getBeerById(UUID beerId) {
        RestTemplate restTemplate = restTemplateBuilder.build();
        return restTemplate.getForObject(GET_BEER_BY_ID_PATH, BeerDTO.class, beerId);
    }
</pre>
'

RestTemplate: ¿Como hacer una peticion post?;'
Con: restTemplate.postForEntity(...)

<pre>
    @Override
    public BeerDTO createBeer(BeerDTO newDto) {
        RestTemplate restTemplate = restTemplateBuilder.build();
        ResponseEntity<BeerDTO> response = restTemplate.postForEntity(GET_BEER_PATH, newDto, BeerDTO.class);
//En este caso no retorna nada la peticion post, pero para retornar un objeto hacemos otra una peticion get para retornar algo
        URI uri = restTemplate.postForLocation(GET_BEER_PATH, newDto);
        return restTemplate.getForObject(uri.getPath(), BeerDTO.class);
    }
</pre>
'
```

#### 20 - Testing Spring RestTemplate

```r
Como moquar una peticion get;'
--1.Trabajar con el test splice RestClientTest
--2.En nuestro caso tenemos una conf. personalizada de RestTemplateBuilderConfig, asi que traemos el bean con @Import(RestTemplateBuilderConfig.class), para que el @Autowired lo encuentre
--3.MockRestServiceServer: Simulara ser el servidor remoto, el cual necesitamos indicar que RestTemplateBuilderConfig usara RestTemplateBuilder, que es lo que se hace en "setUp()"

<pre>
//ref[cmsds_kei]
import com.fasterxml.jackson.core.JsonProcessingException;import com.fasterxml.jackson.databind.ObjectMapper;import guru.springframework.spring6resttemplate.config.RestTemplateBuilderConfig;import guru.springframework.spring6resttemplate.model.BeerDTO;import guru.springframework.spring6resttemplate.model.BeerDTOPageImpl;import guru.springframework.spring6resttemplate.model.BeerStyle;import org.junit.jupiter.api.BeforeEach;import org.junit.jupiter.api.Test;import org.mockito.Mock;import org.springframework.beans.factory.annotation.Autowired;import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;import org.springframework.boot.test.web.client.MockServerRestTemplateCustomizer;import org.springframework.boot.web.client.RestTemplateBuilder;import org.springframework.context.annotation.Import;import org.springframework.data.domain.Page;import org.springframework.http.HttpMethod;import org.springframework.http.MediaType;import org.springframework.test.web.client.MockRestServiceServer;import org.springframework.web.client.RestTemplate;import java.math.BigDecimal;import java.util.Arrays;import java.util.UUID;import static org.assertj.core.api.Assertions.assertThat;import static org.mockito.Mockito.when;import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RestClientTest
@Import(RestTemplateBuilderConfig.class)
public class BeerClientMockTest {

    static final String URL = "http://localhost:8080";

    BeerClient beerClient;

    MockRestServiceServer server;

    @Autowired
    RestTemplateBuilder restTemplateBuilderConfigured;

    @Autowired
    ObjectMapper objectMapper;

    @Mock
    RestTemplateBuilder mockRestTemplateBuilder = new RestTemplateBuilder(new MockServerRestTemplateCustomizer());

    @BeforeEach
    void setUp() {
        RestTemplate restTemplate = restTemplateBuilderConfigured.build();
        server = MockRestServiceServer.bindTo(restTemplate).build();
        when(mockRestTemplateBuilder.build()).thenReturn(restTemplate);
        beerClient = new BeerClientImpl(mockRestTemplateBuilder);
    }

    @Test
    void testListBeers() throws JsonProcessingException {

//conviertimos la respuesta simulada a string
        String payload = objectMapper.writeValueAsString(getPage());

//indicamos como se comportara nuestro servidor mokeado
        server.expect(method(HttpMethod.GET))
                .andExpect(requestTo(URL + BeerClientImpl.GET_BEER_PATH))
                .andRespond(withSuccess(payload, MediaType.APPLICATION_JSON));

//Hacemos el test
        Page<BeerDTO> dtos = beerClient.listBeers();
        assertThat(dtos.getContent().size()).isGreaterThan(0);
    }

    BeerDTOPageImpl getPage(){ return new BeerDTOPageImpl(Arrays.asList(getBeerDto()), 1, 25, 1); }

    BeerDTO getBeerDto(){
        return BeerDTO.builder().id(UUID.randomUUID()).price(new BigDecimal("10.99")).beerName("Mango Bobs").beerStyle(BeerStyle.IPA).quantityOnHand(500).upc("123245").build();
    }
}
</pre>
'

Como moquar un metodo que hace una peticion post y una get dentro de el;'
(El codigo es el mimo de ref[cmsds_kei], pero se agrega)

<pre>
import static org.springframework.test.web.client.response.MockRestResponseCreators.withAccepted;import java.net.URI;import org.springframework.web.util.UriComponentsBuilder;

public class BeerClientMockTest {
  ...
    @Test
    void testCreateBeer() throws JsonProcessingException {
        BeerDTO dto = getBeerDto();
        String response = objectMapper.writeValueAsString(dto);
        URI uri = UriComponentsBuilder.fromPath(BeerClientImpl.GET_BEER_BY_ID_PATH)
                        .build(dto.getId());

        server.expect(method(HttpMethod.POST))
                        .andExpect(requestTo(URL +
                                BeerClientImpl.GET_BEER_PATH))
                                .andRespond(withAccepted().location(uri));

        server.expect(method(HttpMethod.GET))
                .andExpect(requestToUriTemplate(URL +
                        BeerClientImpl.GET_BEER_BY_ID_PATH, dto.getId()))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

        BeerDTO responseDto = beerClient.createBeer(dto);
        assertThat(responseDto.getId()).isEqualTo(dto.getId());
    }
</pre>
'

¿Como probar una peticion delete que no retorna contenido y que es lo importante a verificar?;'
--Lo importante a verificar es que el realizo la peticion, y podemos preguntarle eso al mock con verify, asi probamos que esta actividad silenciosa se realizo
--resto del codigo en ref[cmsds_kei]

<pre>
  @Test
  void testDeleteBeer() {
      server.expect(method(HttpMethod.DELETE))
              .andExpect(requestToUriTemplate(URL + BeerClientImpl.GET_BEER_BY_ID_PATH,
                      dto.getId()))
              .andRespond(withNoContent());

      beerClient.deleteBeer(dto.getId());

      server.verify();
  }
</pre>
'

¿Como probar una excepcion not found?:'
--resto del codigo en ref[cmsds_kei]

<pre>
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.springframework.web.client.HttpClientErrorException;

  @Test
  void testDeleteNotFound() {
      server.expect(method(HttpMethod.DELETE))
              .andExpect(requestToUriTemplate(URL + BeerClientImpl.GET_BEER_BY_ID_PATH,
                      dto.getId()))
              .andRespond(withResourceNotFound());

      assertThrows(HttpClientErrorException.class, () -> {
          beerClient.deleteBeer(dto.getId());
      });

      //verificamos que existio interaccion
      server.verify();
  }
</pre>
'

¿Como probar usando query parameters?:'
--resto del codigo en ref[cmsds_kei]

<pre>
 @Test
  void testListBeersWithQueryParam() throws JsonProcessingException {
      String response = objectMapper.writeValueAsString(getPage());

      URI uri = UriComponentsBuilder.fromHttpUrl(URL + BeerClientImpl.GET_BEER_PATH)
              .queryParam("beerName", "ALE")
              .build().toUri();

      server.expect(method(HttpMethod.GET))
              .andExpect(requestTo(uri))
              .andExpect(queryParam("beerName", "ALE"))
              .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

      Page<BeerDTO> responsePage = beerClient
              .listBeers("ALE", null, null, null, null);

      assertThat(responsePage.getContent().size()).isEqualTo(1);
  }
</pre>
'
```

#### 21 - Spring Security Basic Auth

```r
Spring Security HTTP Basic auth: ¿Dependencia? ¿Como cambiar el user y pass?, ¿Como hacer test unitario?, ¿Como ver los logs?, ¿Como desactivar la proteccion csrf?;'
Dependencia
<pre>
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
  </dependency>

  <dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-test</artifactId>
  </dependency>
</pre>

================================

#cambiar la contraseña por default de spring security
¿Como cambiar el user y pass?

#Creo para cuando usamos thymleaf
  rest.template.username=user1
  rest.template.password=password

#me funciono en una version antigua y sensilla de spring
  spring.security.user.name=user1
  spring.security.user.password=user1

========================================

¿Como hacer test unitario que pase la auth?
--Para el resto del codigo buscar numbre/firma de metodo
--

<pre>
@SpringBootTest
class BeerControllerIT {

    @Autowired
    WebApplicationContext wac;

    @BeforeEach
    void setUp() {
+       mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    void testPatchBeerBadName() throws Exception {
        Beer beer = beerRepository.findAll().get(0);

        Map<String, Object> beerMap = new HashMap<>();
        beerMap.put("beerName", "New Name 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

        mockMvc.perform(patch(BeerController.BEER_PATH_ID, beer.getId())
+                       .with(httpBasic(BeerControllerTest.USERNAME, BeerControllerTest.PASSWORD))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(beerMap)))
                .andExpect(status().isBadRequest());

    }
</pre>

================================================================

¿Como hacer test unitario que FALLE en pasar la auth?

<pre>
    @Test
    void testNoAuth() throws Exception {
        //Test No Auth
        mockMvc.perform(get(BeerController.BEER_PATH)
                        .queryParam("beerStyle", BeerStyle.IPA.name())
                        .queryParam("pageSize", "800"))
+               .andExpect(status().isUnauthorized());

    }
</pre>

================================================================

¿Como ver los logs?
--En el .properties agregamos

<pre>
  logging.level.org.springframework.security=trace
</pre>

================================================================

¿Como desactivar la proteccion csrf?
--Indicamos que solo lo haga para las rutas de la api
--Debemos de incluir esta conf en nuestros test unitarios

<pre>
  import org.springframework.context.annotation.Bean;import org.springframework.context.annotation.Configuration;import org.springframework.security.config.annotation.web.builders.HttpSecurity;import org.springframework.security.web.SecurityFilterChain;

  @Configuration
  public class SpringSecConfig {

      @Bean
      public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
          //para no perder la configuracion por default
          .authorizeHttpRequests().anyRequest().authenticated()
          .and().httpBasic(Customizer.withDefaults())

          //desactivacion
          .csrf(csrf -> csrf.ignoringRequestMatchers("/api/**"))
        ;
        return http.build();
      }

  }

//---------------------------
//Para incluir la conf en nuestros test unitarios

  @Import(SpringSecConfig.class)
  class BeerControllerTest {

</pre>

================================================================
'

#Donde entra el username y la contraseña
#Como hacer debug al login
¿Como hacer una implementacion personalizada el proceso de login?;'
Spring aplica una implementacion por default, pero podemos crear una personalizada con.

<pre>
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.context.annotation.Bean;
  import org.springframework.context.annotation.Configuration;
  import org.springframework.security.authentication.AuthenticationProvider;
  import org.springframework.security.authentication.BadCredentialsException;
  import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
  import org.springframework.security.config.annotation.web.builders.HttpSecurity;
  import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
  import org.springframework.security.core.Authentication;
  import org.springframework.security.core.AuthenticationException;
  import org.springframework.security.core.userdetails.UserDetailsService;
  import org.springframework.security.crypto.password.PasswordEncoder;
  import org.springframework.security.web.SecurityFilterChain;
  import lombok.val;
  import lombok.extern.log4j.Log4j2;

  @Log4j2
  @Configuration
  @EnableWebSecurity
  public class WebSecurity implements AuthenticationProvider {

    private final static String[] permitedPaths = { "/home", "/", "/login", "/logout" };

    @Autowired
    private final PasswordEncoder encoder;

    @Autowired
    private final UserDetailsService usersServ;

    public WebSecurity(PasswordEncoder encoderArgs, UserDetailsService usersRepoArg) {
      encoder = encoderArgs;
      usersServ = usersRepoArg;
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
      http.authorizeHttpRequests((authorizeRequests) -> authorizeRequests
          .antMatchers(permitedPaths).permitAll()
          .antMatchers("/admin").hasRole("ADMIN")
          .antMatchers("/**").hasRole("USER"))
          .csrf((csrf) -> csrf.disable())
          .formLogin(login -> login
              .loginPage("/login"));

      return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
      return usersServ;
    }


//CORRERA ESTE METODO CADA VEZ QUE HAGAMOS UN LOGIN.
    @Override
+   public Authentication authenticate(Authentication auth) throws AuthenticationException {
      val username = auth.getPrincipal().toString();
      log.debug("Authenticate Me: " + username);
      val userFound = usersServ.loadUserByUsername(username);

      val passwordGived = auth.getCredentials().toString();
      val passwordSaved = userFound.getPassword();

      boolean isMatch = encoder.matches(passwordGived, passwordSaved);

      if (isMatch == false) {
        throw new BadCredentialsException("Username/Password does not match for " + auth.getPrincipal());
      }

      val userLogged = new UsernamePasswordAuthenticationToken(username, null, userFound.getAuthorities());

      return userLogged;
    }

    @Override
    public boolean supports(Class<?> authentication) {
      return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }

  }  

-------

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.repaso.uno.repository.UnoUserRepository;

import lombok.val;

@Service
public class UnoUsersService implements UserDetailsService {

  @Autowired
  private UnoUserRepository userRepo;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    val found = userRepo.findByUsername(username);
    if (found == null) {
      throw new UsernameNotFoundException("No user with "+username+" exists in the system");
    }

    val foundCasted = User.builder()
      .username(found.getUsername())
      .password(found.getPassword())
      .disabled(!found.isVerified())
      .accountExpired(found.isAccountCredentialsExpired())
      .accountLocked(found.isLocked())
      .roles("USER")
      .build()
    ;
    
    return foundCasted;
  }
  
}

</pre>
'
```

## Angular & Spring 5: Creando web app full stack (Angular 8+)

### Accesos Rapidos

- [Documentacion Repository (query method)](https://docs.spring.io/spring-data/jpa/docs/2.1.9.RELEASE/reference/html/#jpa.query-methods.query-creation)
- [Documentacion lista de propiedades](https://docs.spring.io/spring-boot/docs/2.4.4/reference/html/appendix-application-properties.html#common-application-properties)

### Seccion 1: Introduccion

Se empieza con angular7 pero aclarar que cualquier
version de Angular de la 2 a la 7 por ejemplo toda
estas seran compatibles entre si, asi que un angular2
y un angular7 y talvez en un  futuro un angular8 o 9
no afectara.
Haremos 2 difererentes aplicaciones una del back(Spring) y
otro del front(Angular).

A. [Codigo Terminado Back y Front](https://gitlab.com/MarioM51/clientes-app-back)

B. Implementacion Produccion:
  1.1. [BACK](https://clientes-app-back.herokuapp.com/)
  1.2. [FRONT](https://front-app-10565.firebaseapp.com)

C. Coleccion Postman
  1.1. ![clientes-app.postman_collection](./img//_clientes-app.postman_collection.json)
  1.1. [clientes-app.postman_collection](./img//_clientes-app.postman_collection.json).

D. Instrucciones.
A.1. Descargar codigo.
A.2. En el back abrirlo con eclipse, actualizar dependencias y correr como aplicacion Spring Boot (no en servidor ya que lo tiene envevido).
A.3. En el front darle un npm install para las dependencia y luego un ng serve para levantarlo.

**Nota:** Para la base de datos y los datos de prueba estan en import.sql del back, y la constrasel;as de ambos
usuarios son "12345".

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Seccion 2: Primeros pasos con Angular

A. Instalamos node y verificamos su instalacion viendo su version

```bash
node --version
v10.15.3

npm --version
6.9.0
```

B. Instalacion de TypeScrip

```bash
npm install -g typescript
tsc --version
Version 3.5.
```

C. Instalamos Angular-CL

```bash
npm install -g @angular/cli
ng --version
Salida: Angular CLI: 8.1.
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 5. Creando nuestra aplicación Angular

A. Crear proyecto angular el cual nos creara una carpeta, <!-- iniciar proyecto acgular, iniciar proyecto angular, crear angular, iniciar angular --

<!--
Crear proyecto con yarn, usar yarn para crear proyecto
-->

>
> Nota: Si queremos cambiar a yarn para manejar las dependencias podemos
> ejecutamos `ng config -g cli.packageManager yarn`

Crear proyecto angular: `ng new clientes-ap`

Nos preguntara si queremos usar route le comentamos que no
y que estilos usar le ponemos que css.

B. Iniciamos el proyecto, iniciar aplicacion angular, iniciar servidor
angular. `ng serve`

C. Ya que levanto correctamente el proyecto
podemos empezar a hacerle modificaciones

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 6 y 7. Estructura de directorio del proyecto angular

```r
angular-app/
  e2e/           -> Todo lo necesario para tests end-to-end
  node_modules/  -> Dependencias
  src/           -> Todo el codigo
    app/         -> Van los componentes este es el inicial
      app.component.css
      app.component.html
      app.component.ts
      app.module.ts  -> se registras dependencias, pipes, provides(los que contultas REST), bootstrap(compo main)
    enviroments    -> Configuracion de ambientes test/qa/prod/etc
  .editorconfig  -> Toda la configuracion del IDE como utf8, tabulaciones
  angular.json   -> Configuracion de nuestra app, como el archivo index, donde se pondra lo compilado
  package.json   -> Nombre de la app, version, dependencias para prod, devdepencies
  tsconfig.json  -> cosas como la ayuda de sintaxis en decoradores, compilacion automatica
  tslint.json    -> para mostrar errores de sintaxis de ts
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 8. Integrar Bootstrap con Angular

A. vamos a getbootstrap.com y copiamos el CDN y lo ponemos en
el index, el .js lo ponemos abajo para no tener pex con el DOM
<!-- agregar bootstrap, importar bootstrap, usar bootstrap -->

```html
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>
```

o instalamos desde npm: `npm install bootstrap@4.3.1`

el cual agrega en el package.json

```json
"dependencies": { ...
  "bootstrap": "^4.3.1",
```

B. Creamos nuestro menu. buscamos navbar en la pagina de
bootstrap en mi caso getbootstrap.com/docs/4.3/components/navbar/
y copiamos el primer menu, y lo pegamos en el app-component.

```html
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item"><a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a></li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
```

**Resultado:** ![1_menu_bootstrap](./img//1_menu_bootstrap.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 9. Creando nuevo componente HeaderComponent

<!-- crear componente -->

A. Creamos el componente en una nueva carpeta, y dentro a *header.component.ts*

```yml
=  app/
+    header/
+      header.component.html  <- Aqui ponemos el menu que copiamos
+      header.component.ts
```

B. Agregamos en *app.module.ts* nuestro componente

```ts
  import { HeaderComponent } from './header/header.component';
...
  declarations: [
+   HeaderComponent
  ],
```

C. a *app.component.html* le agregamos nuestro componente

```html
<app-header></app-header>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 11. Creando nuevo componente FooterComponent

A. Creamos su carpeta y nos metemos y corremos

```bash
ng generate class FooterComponent
```

B. Lo agregamos en *app.module.ts* y *app.component.html*
igual como agregamos el header.

**Commit:** gitlab.com/MarioM51/angular_spring_b/commit/591bfef0eecf2db3f3a616033fe7a16f348c9f26

**Resultado:** ![2_footer_bootstrap](./img//2_footer_bootstrap.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 12. Directiva estructural *ngFor

<!-- for en angular -->

A. Ahora vamos a crear un componente de forma automatica.

```bash
ng generate component directiva
```

B. Cremos nuestra lista a iterrar

```javascript
export class DirectivaComponent {
  listaMascotas: string[] = ['Perro', 'Gato', 'Tiburon', 'Cocodrilo'];
```

C. Creamos nuestra lista usando **ngFor**

```html
<article class="card">
    <header class="card-header">Listado de mascotas</header>
    <ul class="list-group">
        <li class="list-group-item" *ngFor="let curso of listaMascotas">{{curso}}</li>
    </ul>
</article>
```

**Resultado:** ![3_ngfor](./img//3_ngfor.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 12. Directiva estructural *ngIf

<!-- if en angular -->

**Actividad:** Crearemos un boton que cuando lo
pulsemos ocultara o mostrara la lista.

A. Creamos el boton

```html
<button type="button" class="btn btn-primary my-3" (click)="mostrar = !mostrar">Ocultar</button>
...
<ul class="list-group" *ngIf="mostrar == true">
```

**Commit:**: gitlab.com/MarioM51/angular_spring_b/commit/412496dd0094cc1d503079a30da399fe9db44445
**Resultado:** ![ngif.gif](./img//4_ngif.gif)

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 3: Angular: Componente clientes

#### 14. Creando el componente clientes.component
<!-- crear lista de elementos, crear tabla de elementos con angular, listar elementos -->
**Actividad:** Crearemos un componente que administre
una coleccion de clientes.

A. Generamos nuestro componente: `ng generate component clientes`

B. Creamos en la misma carpeta de clientes un modelo

```typescript
export class Cliente {
  id: number;
  nombre: string;
  apellido: string;
  createAt: string;
  mail: string;
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 15. Listando los objetos del tipo Cliente

A. Creamos la lista

```javascript
export class ClientesComponent implements OnInit {
clientes: Cliente[] = [
 {id: 1, nombre: "nombre1", apellido: "Apellido1", createAt: "2019/01/01", mail: "mail1"},
 {id: 2, nombre: "nombre2", apellido: "Apellido2", createAt: "2019/01/02", mail: "mail2"},
 {id: 3, nombre: "nombre3", apellido: "Apellido3", createAt: "2019/01/03", mail: "mail3"},
 {id: 4, nombre: "nombre4", apellido: "Apellido5", createAt: "2019/01/04", mail: "mail4"}
];
```

B. Creamos la tabla

```html
<div class="card text-white bg-dark mb-3">
<div class="card-header">Clientes</div>
<div class="card-body">
    <h5 class="card-title">Listado de clientes</h5>
    <table class="table table-bordered table-striped text-white">
        <thead>
            <th>ID</th> <th>Nombre</th> <th>Apellido</th> <th>Creado</th> <th>Mail</th>
        </thead>
        <tbody>
            <tr *ngFor="let cliente of clientes">
                <td>{{cliente.id}}</td>
                ...
            </tr>
        </tbody>
    </table>
</div>
</div>
```

**Commit:** gitlab.com/MarioM51/angular_spring_b/commit/21140593a95de40e2681ac2401e89175ce863315

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 16. Creando archivo clientes.json.ts con la lista de objetos

A. Quitamos el arreglo de nuestro controlador

```javascript
import { CLIENTES } from './clienteS';

export class ClientesComponent implements OnInit {
  clientes: Cliente[] = []
```

B. En un nuevo ts ponemos

```javascript
import { Cliente } from './cliente';
export const CLIENTES: Cliente[] = [
    {id: 1, nombre: "nombre1", apellido: "Apellido1", createAt: "2019/01/01", mail: "mail1"},
    ...
  ]
```

C. Deberia de seguir funcionando igual
**Resultado:** ![5_mostrar_clientes](./img//5_mostrar_clientes.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 17. Creando la clase de Servicio ClienteService y la Inyección de Dependencia

A. nos ubicamos en la carpeta clientes-app\src\app\clientes y creamos
el servicio

```bash
ng g service cliente
```

B. Quitamos el import de los CLIENTES y lo pasamos al service

```typescript
import { Injectable } from '@angular/core';
import { Cliente } from './cliente';
import { CLIENTES } from './clienteS';

@Injectable({providedIn: 'root'})
export class ClienteService {
  constructor() { }
  getClientes(): Cliente[] { return CLIENTES; }
}
```

C. Lo invocamos desde nuestro controlador

```typescript
export class ClientesComponent implements OnInit {
  constructor(private clienteService: ClienteService) { }
  ngOnInit() { this.clientes = this.clienteService.getClientes(); }
}
```

D. Lo agregamos a *app.module* en la parte de providers

```typescript
providers: [ ClienteService ],
```

**Resultado:** Siguie funcionando igual pero tenemos
separada la logica.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 18. Implementando Observable en nuestra clase Servicio ClienteService

Vamos a hacer nuestra peticion de forma reactiva,
para que no se bloquee el flujo mientras resivimos
la respuesta.
Lo que nos ayudara a disparar un evento en tanto cambie
algo del lado de Servidor

```typescript
import { Observable, of } from 'rxjs';
...
export class ClienteService {
...
  getClientes(): Observable<Cliente[]> {
    return of(CLIENTES);
  }
```

clientes.component.ts. Suscribimos lo retornado
para que cada vez que haiga un cambio de ejecute
la funcion de flecha gorda

```typescript
ngOnInit() {
this.clienteService.getClientes().subscribe(
  clientes => this.clientes = clientes
);
}
```

**Resultado:** Tenemos preparado para que cuando tengamos
nuestro back funcione de forma asincrona o en tiempo real.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 19. Implementando Rutas en Angular y navegación

Angular funciona de forma SPA por lo que en la misma
pagina renderiza diferentes vistas

A. Vamos a *app.module* y agregamos la dependencia
de rutas y lo agregamos dentro de imports

```typescript
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {path: '', redirectTo: '/clientes', pathMatch: 'full'},
  {path: 'directivas', component: DirectivaComponent},
  {path: 'clientes', component: ClientesComponent}
];
imports: [ RouterModule.forRoot(routes) ],

```

B. Agregamos enlace

```html
<a routerLink="/clientes" class="...">clientes</a>
```

B. Vamos a *app.component* y agegamos el manejador de rutas

```html
<section class="container">
    <router-outlet></router-outlet>
</section>
```

**Resultado:** ![6_routing](./img//6_routing.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 21. Configurando e integrando Bootstrap de forma local en nuestra app

Descargamos la s dependencias con el link del CDN y las ponemos en clientes-app\src\assets\css, y clientes-app\src\assets\js

```css
<link rel="stylesheet" href="./assets/css/bootstrap.min.css" >
</head>
...
</body>
<script src="./assets/js/jquery-3.3.1.slim.min.js"></script>
<script src="./assets/js/popper.min.js"></script>
<script src="./assets/js/bootstrap.min.js"></script>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 22. Instalando Bootstrap utilizando el comando npm desde el terminal
<!-- instalar bootstrap con npm instalar bootstrap, importar bootstrap -->
A. Instalar dependencias: `npm install bootstrap@4.x jquery popper.js --save`

B. Se agregara la dependencia en *package.json* y se agregan
los archivos en *node_modules*, y podemos agregar en *angular.json*

C. Podemos quitar las dependencias del index.html y
ya que lo agregamos en el *angular.json*.

```json
"styles": [
  "./node_modules/bootstrap/dist/css/bootstrap.css"
],
"scripts": [
  "./node_modules/jquery/dist/jquery.slim.min.js",
  "./node_modules/popper.js/dist/umd/popper.min.js",
  "./node_modules/bootstrap/dist/js/bootstrap.min.js"
]
```

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 4: Backend: Spring API REST

#### 25 y 26. Herramientas necesarias Backend

A. Tener instalado Java y STS.

B. Iniciamos un proyecto Spring

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 28. Creando Proyecto Backend API REST

A. Agregamos las sigientes dependencias a nuestro
nuevo proyecto

- Spring Boot DevTools
- Spring Data JPA
- MySQL Driver
- Spring Web Starter

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 29. Configurando el Datasource a MySQL en el proyecto backend

<!-- lista de propiedades, referencia para variables de configuracion, uso de .properties -->

- Fuente: [Docu](https://docs.spring.io/spring-boot/docs/2.4.4/reference/html/appendix-application-properties.html#common-application-properties)

<!-- 
mostrar consultas sql, mostrar los logs de hibernate, mostrar los logs de JPA
usar variables en .properties, variables en .properties variables
usar variables en properties, variables en properties variables
-->

```conf
#Coneccion y conf del datasorse
trabajos.database-name=bd_app_clientes
spring.datasource.url=jdbc:mysql://localhost/${trabajos.database-name}?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
spring.datasource.username=root
spring.datasource.password=mario
spring.datasource.hikari.driver-class-name=com.mysql.cj.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL57Dialect

server.port=8080

#para que se creen las tablas apartir de los entity
spring.jpa.hibernate.ddl-auto=create-drop

#para mostrar el SQL consultado
logging.level.org.hibernate.SQL=debug

#ayuda a ver las peticiones, ver peticiones, mostrar peticiones en logs, agregar peticiones a logs
logging.level.org.springframework.web=DEBUG

#ver peticiones web/http para spring security
#mostrar logs de security logs, mostrar logs de spring security logs
logging.level.org.springframework.security=DEBUG
```

**Error:** java.sql.SQLException: The server time zone value ... is unrecognized or represents more than one time zone.
**Solucion:** Agregar como esta arriba lo siguiente: __?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC__

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 30. Instalando MySQL

A. vamos a dev.mysql.com/downloads/windows/installer/8.0.html y descargamos
(mysql-installer-community-8.0.16.0.msi)

B. Seleccionamos instalacion personalizada y escogemos los paquetes "mysql Server"
y "mysql workbench"

C. Todo sguiente solo le conf nuestro usuario y pass.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 31. Creando la Base de Datos

A. Conectarse a mysql: `mysql -u root -p`

B. Creamos la base de datos y vemos que exista

```sql
CREATE DATABASE bd_app_clientes;

SHOW DATABASES;
```

C. Iniciamos el proyecto para ver que arranca bien.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 32. Añadiendo la clase Entity Cliente al Backend

A. Creamos el paquetes y la clase de la sig manera

```java
package com.mariocorps.clientesapp.models.entity;
//anotaciones de persistence
@Entity
@Table(name="clientes")
public class ClienteEntity implements Serializable{
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  private String nombre;
  private String apellido;
  private String mail;

  @Column(name="create_at")
  @Temporal(TemporalType.DATE)
  private Date createAt;
```

B. Iniciamos el proyecto y debemos de poder ver que se crea la tabla

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 33. Añadiendo las clases Repository y Service de la lógica de negocio

**Documentacion**: docs.spring.io/spring-data/jpa/docs/2.1.9.RELEASE/reference/html/#jpa.query-methods.query-creation

A. Creamos nuestra capa DAO

```java
package com.mariocorps.clientesapp.models.dao;
import org.springframework.data.repository.CrudRepository;
import com.mariocorps.clientesapp.models.entity.ClienteEntity;
public interface IClienteDao extends CrudRepository<ClienteEntity, Long>{ }
```

B. Creamos nuestra capa service
B.1. Creamos la Interface de nuestro servicio

```java
public interface IClienteService {
  List<ClienteEntity> findAll();
}
```

B.2. Creamos la implementacion.
*@Transactional*: Podemos omitir el uso de esta ya que va de forma
implecita, pero lo ponemos para tenerlo claro y mayot control.

```java
@Service
public class ClienteServiceImpl implements IClienteService {

  @Autowired
  private IClienteDao clienteDao;

  @Override
  @Transactional(readOnly = true)
  public List<ClienteEntity> findAll() {
    return (List<ClienteEntity>)clienteDao.findAll();
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 34. Creando controlador @RestController y EndPoint para listar

<!--
como saber si se creo un bean, ejecutar metodo despues de crear un bean, ver creacion de bean,
mostrar un log cuando se cree un bean, mostrar cuando un bean se cree, mostrar creacion de bean,
cuando se crea un bean, cuando se crea un componente, ciclo de vida componente, ver si se creo un bean
-->

1. Se usa `@PostConstruct` para que se ejecute el metodo cuando termine
   de crearse el bean, lo cual sabremos mostarndo un log.

```java
@RestController
@RequestMapping("/api/clientes")
public class ClientesController {
  Logger logger = LoggerFactory.getLogger(ClientesController.class);

  @Autowired
  private IClienteService clienteService;

  @PostConstruct
  private void onPostConstruct() { logger.info("created api/clientes/all"); }

  @RequestMapping(path="/all", method=RequestMethod.GET)
  public List<ClienteEntity> index() {
    return clienteService.findAll();
  }
```

**Error:** Parecia que no iniciaba el controlodor y no podia
acceder ala ruta, pero si, ya que al parecer Spring Boot 2 ya
no muestra los logs de los beans creados.
**Solucion:** Le meti el PostConstruct para ver que si estaba
iniciando el bean a travez de un log.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 35. Añadiendo Datos de pueba

A. En resourses creamos un archivo sql, es importante que se llame "import.sql"
en ".\src\main\resources\import.sql" para que spring boot lo busque y lo ejecute.

```sql
/* Populate tabla clientes */
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Andrés', 'Guzmán', 'profesor@bolsadeideas.com', '2018-01-01');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Mr. John', 'Doe', 'john.doe@gmail.com', '2018-01-02');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Linus', 'Torvalds', 'linus.torvalds@gmail.com', '2018-01-03');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Rasmus', 'Lerdorf', 'rasmus.lerdorf@gmail.com', '2018-01-04');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Erich', 'Gamma', 'erich.gamma@gmail.com', '2018-02-01');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Richard', 'Helm', 'richard.helm@gmail.com', '2018-02-10');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Ralph', 'Johnson', 'ralph.johnson@gmail.com', '2018-02-18');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('John', 'Vlissides', 'john.vlissides@gmail.com', '2018-02-28');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Dr. James', 'Gosling', 'james.gosling@gmail.com', '2018-03-03');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Magma', 'Lee', 'magma.lee@gmail.com', '2018-03-04');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Tornado', 'Roe', 'tornado.roe@gmail.com', '2018-03-05');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Jade', 'Doe', 'jane.doe@gmail.com', '2018-03-06');
```

B. Ejecutamos el proyecto y podremos ver en los logs como se ejecutan los INSERTs

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 36. Usando Postman para probar nuestras APIs

postman

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 37. Uso de Cors para compartir recursos en API REST

**Teoria**: Este es un mecanismo de seguridad que para saber que de donde estamos$
resiviendo la peticion es seguro.
A. Se realiza una configuracion el la API que se va a exponer
B. Cuando se realiza la peticion a la API se realiza una **pre-peticion** para
saber que permisos tiene el domino de donde llega la peticion.
C. Retorna todos los metodos http que estan permitidos para este dominio.
D. Se hace la peticion y la respuesta si se tiene los permisos.

**Actividad:** Vamos a configurar el cors para que nos permita hacer
peticiones desde angular.

A, Vamos a nuestro RestController y le ponemos a nivel de clase
la anotacion *@CrossOrigin*.

B, Nota: Tambien podemos incluir los metodos permitidos, las cabezeras permitidas
y dejandolo vacio esos 2 deja el por defecto que es permitir todos.

```java
@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
@RequestMapping("/api/clientes")
public class ClientesController {...
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 38. Implementando Servicio Angular con HttpClient

**Actividad:** Vamos a conectar nuestro api con el front

A. Vamos a app.module y agregamos en imports

```java
import { HttpClientModule } from '@angular/common/http';
```

B. Vamos clientes.service y agregamos la peticion y
consumimos nuestra API.

```typescript
import { HttpClient } from '@angular/common/http';
...
export class ClienteService {
   private endpoint:string = "http://localhost:8080/api/clientes";
   constructor(private httpClient: HttpClient) { }
   getClientes(): Observable<Cliente[]> {
    //return of(CLIENTES);
    return this.httpClient.get<Cliente[]>(this.endpoint);
  }
```

Codigo-Back: gitlab.com/MarioM51/clientes-app-back/commit/ed6610bd8c8f3319b2c46879500a68dccf548112
Codigo-Front: gitlab.com/MarioM51/angular_spring_b/commit/05ae935df4bc707968f051b5eb192a8e18b3bf44

**Resultado:** ![consultar_todos](./img//7_consultar_todos.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 40. Escribiendo los métodos del CRUD en la clase ClienteService del Backend

A. Vamos a IClienteService e implementamos los sig. metodos

```java
public interface IClienteService {
  List<ClienteEntity> findAll();
  ClienteEntity findById(Long clienteId);
  ClienteEntity save(ClienteEntity cliente);
  void delete(Long clienteId);
}
```

B. Vamos a la implementacion y agregamos lo sig.

```java
return clienteDao.findById(clienteId).orElse(null);
return clienteDao.save(cliente);
clienteDao.deleteById(clienteId);
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 41. Escribiendo los métodos show y create en el Controlador Backend API Rest

```java
@GetMapping({"/{id}"})
public ClienteEntity show(@PathVariable Long id) {
  return clienteService.findById(id);
}

@PostMapping({"", "/"})
@ResponseStatus(value=HttpStatus.CREATED)
public ClienteEntity save(@RequestBody ClienteEntity cliente) {
  return clienteService.save(cliente);
}
```

Podemos agregar una la fecha desde el entity para que sea mas limpio de la
siguiente manera

```java
@PrePersist
public void prePersist() {
  this.createAt = new Date();
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 42. Escribiendo los métodos update y delete en el Controlador Backend API Rest

```java
@PutMapping("/{id}")
@ResponseStatus(value=HttpStatus.ACCEPTED)
public ClienteEntity update(@PathVariable Long id, @RequestBody ClienteEntity cliente) {
  ClienteEntity clienteActual = clienteService.findById(id);
  clienteActual.setApellido(cliente.getApellido());
  clienteActual.setEmail(cliente.getEmail());
  clienteActual.setNombre(cliente.getNombre());
  return clienteService.save(cliente);
}

@DeleteMapping("/{id}")
@ResponseStatus(value=HttpStatus.NO_CONTENT)
public void delete(@PathVariable Long id) {
  clienteService.delete(id);
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 43. Probando nuestro Backend API Rest con Postman

...

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 45. Creando el componente form.component y la vista del formulario

A. Creamos el componente para el formulario en la carpeta de cliente.
Nota: **--flat** significa que no creara el componente dentro de una carpeta

```batch
ng g c clientes/form.cliente --flat
```

B. Agregamos el modulo de forms en app.modules

```typescript
import { FormsModule } from '@angular/forms';
...
imports: [ FormsModule
...
```

C. Creamos la vista

```html
<article class="card bg-dark text-white">
    <header class="card-header">{{titulo}}</header>
    <div class="card-body">
        <form (ngSubmit)="create()" >
            <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">Nombre: </label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" [(ngModel)]="cliente.nombre" name="nombre" />
                </div>
            </div>
            ...
            <div class="form-group row">
                <button class="btn btn-primary" role="button">Crear</button>
            </div>
        </form>
    </div>
</article>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 46. Configurando la ruta y navegación del formulario

A. importamos la ruta en app.modules

```javascript
const routes: Routes = [
  {path: 'clientes/form', component: FormClienteComponent}
];
```

B. Agregamos la ruta/link en donde esta nuestra tabla

```html
<button class="btn btn-rounded btn-primary" type="button" [routerLink]="['/clientes/form']">Crear Cliente</button>
```

C. **Resultado**: tenemos el uso el link del al formulario
para crear clientes.

![8_formCliente_crear](./img//8_formCliente_crear.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 47. Escribiendo implementación crear en el cliente.service.ts y en form.component.ts

A. Vamos al cliente.service y creamos el metodo
crearcliente

```typescript
private httpHeaders = new HttpHeaders({"Content-Type": "Application/json"});

addCliente(cliente: Cliente): Observable<Cliente>  {
  return this.httpClient.post<Cliente>(this.endpoint, cliente, {headers: this.httpHeaders});
}
```

B. Vamos al form.component de clientes y agregamos
el serivice para poder llamar el nuevo metodo, y cuando
termine redireccionamos al listado para que se vea el
nuevo cliente.

```typescript
create() {
  console.log("init crear");
  this.clienteService.addCliente(this.cliente).subscribe(
    response => this.router.navigate(['/clientes'])
  );
}
```

**Resultado**: Ya podemos crear los clientes en la bd
desde el formulario pasando por la API.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 49. Instalar SweetAlert2 para enviar mensajes de alerta en el cliente

A. Instalamos sweetalert con la opccion --save para que se guarde en package.json

```bash
npm install sweetalert2 --save
```

B. Vamos a form.cliente.component e importamos las alertas y las usamos
justo despues de crearlo

```typescript
import Swal from 'sweetalert2'
...
Swal.fire('Cliente creado',  `cliente ${clienteCreado.nombre} creado con exito`,  'success');
```

![9_alert_success_add](./img//9_alert_success_add.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 50. Cargando los datos en el formulario para actualizar

A. Agregamos la ruta en *app.module.ts*

```typescript
const routes: Routes = [
  {path: 'clientes/form/:id', component: FormClienteComponent}
];
```

B. Vamos a *form.cliente.component.ts* y consumimos el nuevo metodo

```typescript
loadCliente(): void {
  this.activatedRoute.params.subscribe(params => {
    let id = params['id'];
    if(id) {
      this.clienteService.getClienteById(id).subscribe(
        clienteLoaded => this.cliente = clienteLoaded
      );
    }
  });
}
```

C. Vamos al cliente.service.ts y le agregamos el metodo para obtener por id.

```typescript
getClienteById(idCliente: Number): Observable<Cliente> {
  let urlGetCliente = this.endpoint+"/"+idCliente
  return this.httpClient.get<Cliente>(urlGetCliente);
}
```

D. Agregamos el la ruta en el front

```html
<td><button class="btn btn-rounded btn-primary" type="button" [routerLink]="['/clientes/form/',cliente.id]">/</button></td>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 51. Escribiendo el update en el cliente.service.ts y en form.component.ts

A. Vamos al *cliente.service.ts* y creamos el metodo de editar

```typescript
update(cliente: Cliente): Observable<Cliente> {
   let urlGetCliente = this.endpoint+"/"+cliente.id;
   return this.httpClient.put<Cliente>(urlGetCliente, cliente, {headers: this.httpHeaders});
}
```

B. Vamos al *form.component.ts* para crear el metodo update

```typescript
update(): void {
  this.clienteService.update(this.cliente).subscribe(
    clienteActualizado => {
      this.router.navigate(['/clientes']);
      Swal.fire('Cliente modificado',  `cliente ${clienteActualizado.nombre} actualizado con exito`,  'success');
    }
  );
}
```

C. Vamos a la vista *form.cliente.component.html* y agregamos el
boton para editar
  3.1. Quityamos el ngSubmit del form

```html
<button class="btn btn-primary" role="button" (click)="create()" *ngIf="!cliente.id else elseBlock">
  Crear
</button>
<ng-template #elseBlock>
  <button class="btn btn-primary" role="button" (click)="update()">
    Editar
  </button>
</ng-template>
```

**Commit:** gitlab.com/MarioM51/angular_spring_b/commit/5b5dd70b9a947d3b4c6922f2c51063aae8d7cdd1
**Resultado:** ![10_editar_clinete](./img//10_editar_clinete.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 52. Escribiendo el delete en la clase service y en el componente clientes

A. Vamos al *cliente.service.ts* y creamos el metodo de eliminar

```typescript
  delete(idCliente: Number): void {
    let urlGetCliente = this.endpoint+"/"+idCliente
    this.httpClient.delete<Cliente>(urlGetCliente, {headers: this.httpHeaders});
  }
```

A. Vamos a *clientes.component.ts* y agregamos el metodo delete para asociarlo a un boton

```typescript
delete(cliente: Cliente): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "Delete "+ cliente.nombre +"?, You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.clienteService.delete(cliente.id).subscribe(
          clienteEliminado => {
            this.clientes = this.clientes.filter(c => c !== cliente)
            Swal.fire(
              'Deleted!',
              'Client '+clienteEliminado.nombre+' has been deleted.',
              'success'
            )
          }
        );
      }
    })
  }
```

C. Agregamos el boton de eliminar en *clientes.component.html*

```html
<td><button class="btn btn-rounded btn-danger" type="button" (click)="delete(cliente)">X</button></td>
```

**Commit:** gitlab.com/MarioM51/angular_spring_b/commit/7ff2b92683e4774f43b15889760d9c2ae23318b4
**Resultado:** ![11_eliminar_clinete](./img//11_eliminar_clinete.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 53. Overflow en listado de clientes, ajustando layout

Tenemos un problema cuando son muchos registros y la pantalla footer se queda
ensima del contenido de la tabla,

A. Vamos a *styles.css* para agregar estilos generales.

```css
html {
    position: relative;
    min-height: 100%;
}
body {
    margin-bottom: 60px;
}
```

B. Vamos a *app.component.html* y le ponemos un margen al body.

```html
+ <section class="container my-3 pb-5">
=    <router-outlet></router-outlet>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 54. Validando los clientes en la tabla HTML con directiva ngIf

Vamos a cambiar el mensaje en caso de que no tenga ningun registro

A. Vamos a *clientes.component.html* y agregamos unos ngIf

```html
<div *ngIf="clientes?.length == 0" class="alert alert-info">
        No hay registros en la BD
    </div>
    <table class="table table-bordered table-striped" *ngIf="clientes?.length>0">
```

**Resultado:** Cuando no ahi registros se muestra un mensaje indicando
que no hay clientes registrados.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 55. Manejo de error en el Backend en método handler show (obtener por id)

Agregar un error cuando no exista un registro con el id indicado.
A. Vamos a *ClientesController* y cambiamos la respuesta del controlador, por
un responseEntity de tipo generico para poder retornar un cliente o un error

```java
@GetMapping("/{id}")
public ResponseEntity<?> show(@PathVariable Long id) {
  ClienteEntity cliente = null;
  Map<String, Object> errorResponse = new HashMap<String, Object>();
  try { cliente = clienteService.findById(id); }
  catch (Exception e) {
    errorResponse.put("mensaje", "Error al consultar el cliente");
    if(e instanceof DataAccessException) { errorResponse.put("descripcion", ((DataAccessException)e).getMostSpecificCause().getMessage()); }
    else { errorResponse.put("descripcion", e.getMessage()); }
    return new ResponseEntity<Map<String, Object>>(errorResponse, HttpStatus.FAILED_DEPENDENCY);
  }
  if(cliente == null) {
    errorResponse.put("mensaje", "El Cliente con el id "+ id.toString() +" no existe");
    return new ResponseEntity<Map<String, Object>>(errorResponse, HttpStatus.NOT_FOUND);
  }
  return new ResponseEntity<ClienteEntity>(cliente, HttpStatus.OK);
}
```

#### 56. Manejo de error en el Backend en método handler create

Es basicamente lo mismo que el getById

#### 57. Manejo de error en el Backend en método handler update

Es basicamente lo mismo que el getById...

#### 58. Manejo de error en el Backend en método handler delete

Es basicamente lo mismo que el getById...

#### 59. Manejo de error en el Frontend Angular en obtener por id

A. Vamos a *cliente.service.ts* y agregamos un pipe para manejar el error que agregamos en el back

```typescript
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
...
return this.httpClient.get<Cliente>(urlGetCliente).pipe(
  catchError(e => {
  Swal.fire("Error al obtener cliente", e.error.mensaje, "error");
  this.router.navigate(['/clientes']);
  return throwError(e);
  })
);
```

**Resultado:** ![10_alert_cliente_no_existe](./img//10_alert_cliente_no_existe.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 60. Manejo de error en el Frontend Angular en create, update y delete

Es basicamente lo mismo que el getById...

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 61. Customizando y arreglando los textos de éxito en crear y actualizar del frontend

Cambiamos en el back enviar solo al cliente en json a enviarlo dentro de un objeto cliente
por lo que cambiamos como capturamos dicho mensaje para no mostrar undifined.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 8: Validando form por el lado Angular (Frontend)

#### 62. Validando form en el template

A. Para esto hacemos uso de etiquetas html para las validaciones como required, min/max-length
mail, etc. Y para mostrar los errores usamos un ngIf.

```html
<input type="text" class="form-control" [(ngModel)]="cliente.nombre" name="nombre" required minlength="4" maxlength="10" #nombre="ngModel" />
<div class="alert alert-danger" *ngIf="nombre.invalid && (nombre.dirty || nombre.touched)">
  <div *ngIf="nombre.errors.required">El nombre es requerido</div>
  <div *ngIf="nombre.errors.minlength">El nombre debe tener almenos 4 caracteres</div>
  <div *ngIf="nombre.errors.maxlength">El nombre debe tener maximo 10 caracteres</div>
</div>
...
<input type="email" class="form-control" [(ngModel)]="cliente.email" name="email" required email #email="ngModel"  />
<div class="alert alert-danger" *ngIf="email.invalid && (email.dirty || email.touched)">
  <div *ngIf="email.errors.required">El email es requerido</div>
  <div *ngIf="email.errors.email">Debe tener un formato correcto</div>
</div>
```

B. Bloquemos el boton de crear si los datos no son validos, agregamos una variable
con el valor de ngFor

```html
<form #clienteForm="ngForm">
...
<button class="btn btn-primary" role="button" (click)="update()" [disabled]="!clienteForm.form.valid">
```

**Resultado:** ![11_validacion_front](./img//11_validacion_front.PNG)

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 9: Validando form en Spring Backend

#### 64. Anotaciones JavaBeans Validation en la clase Entity

Vamos a hacer uso de validator de java

```java
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
...
@NotEmpty
@Size(min=4, max=10)
private String nombre;

@Column(unique=true)
@NotEmpty
@Email
private String email;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 65. Implementando anotación @Valid en métodos handler create y update del controller

A. Vamos al controlador de crear y agregamos la etiqueta.

Nota: Hacemos uso de stream para evitar el uso del for, en teoria es mas elegante asi.

> - Error: BindingResult no funciona.  
> - Descripcion: Cuando se realiza una peticion el `bindingResult.hasErrors`
>   no muestra que tenga ningun error a pesar que los tiene, por ejemplo,
>   el email no tiene un formato correcto y el bindingResult no marca
>   ningun error.  
> - Causa: Lo mas seguro NO incluimos un validador ya que este lo
>   requiere para funcionar, en mi caso yo agrege este.  
> - Solucion: Solucion: Agregar un validador por ejemplo: [mvnrepo](https://mvnrepository.com/artifact/org.hibernate.validator/hibernate-validator/6.0.2.Final)

```java
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@PostMapping({ "", "/" })
public ResponseEntity<?> save(@Valid @RequestBody ClienteEntity cliente, BindingResult bindingResult) {
  ClienteEntity clienteCreado = null;
  Map<String, Object> errorResponse = new HashMap<String, Object>();

  if(bindingResult.hasErrors()) {
    /*List<String> errores = new ArrayList<String>(); for (FieldError err: bindingResult.getFieldErrors()) {  errores.add("El campo " + err.getField() + " " + err.getDefaultMessage());  }*/
    List<String> errores = bindingResult.getFieldErrors()
        .stream().map(err -> { return "El campo " + err.getField() + " " + err.getDefaultMessage(); })
        .collect(Collectors.toList());

    errorResponse.put("mensaje", "Datos del cliente no validos" + cliente.getNombre());
    errorResponse.put("descripcion", errores);

    return new ResponseEntity<Map<String, Object>>(errorResponse, HttpStatus.BAD_REQUEST);
  }
  try { clienteCreado = clienteService.save(cliente); ...
```

**Resultado:** ![11_validacion_back](./img//11_validacion_back.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 66. Probando validación API REST en POSTMAN

...

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 67. Manejando los error de validación en Angular

A. Vamos a *cliente.service.ts* y si es 400 regresamos el error
para que lo manejemos en el controlador.

```typescript
addCliente(cliente: Cliente): Observable<Cliente>  {
    return this.httpClient.post<Cliente>(this.endpoint, cliente, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        if(e.status == 400) {
          return throwError(e);
        }
        return throwError(e);
      })
    );;
```

B. vamos a *form.cliente.component.ts*

```typescript
private errores: string[];
...
this.clienteService.addCliente(this.cliente).subscribe(
clienteCreado => {
  this.router.navigate(['/clientes']);
  Swal.fire('Cliente creado',  `cliente ${clienteCreado.nombre} creado con exito`,  'success');
},
err => {
  this.errores = err.error.descripcion as string[];
  console.log(this.errores);
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 68. Agregando los mensajes de errores en la plantilla form

A. Ya tenemos los errores en una var ya solo debemos de mostrarla.
Nota: Quitamos la validacion del front

```html
<ul class="alert alert-danger" *ngIf="errores?.length>0">
  <li *ngFor="let err of errores"> {{err}} </li>
</ul>
```

**Resultado:** ![12_validacion_back_front](./img//12_validacion_back_front.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 69. Personalizar mensajes de validación en español

```java
@NotEmpty(message = "No puede estar sin asignarse")
@Size(min=4, max=10, message="rango mal")
```

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 10: Transformando datos del Observable: usando operador map y Pipes

#### 70-71. Operador map formato uppercase en Observable

Vamos a cambiar el formato de la fecha

A. vamos a *cliente.service.ts*

```typescript
return this.httpClient.get(this.endpoint).pipe(
map(response => {
  let clientes = response as Cliente[]
  clientes = clientes.map(
  cliente => {
    let fecha = cliente.createAt.split("-");
    let anio = fecha[0], mes = fecha[1], dia = fecha[2];
    cliente.createAt = anio+"/"+mes+"/"+dia;
    return cliente;
  }
  );
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 72. Registrando el Locale y los diferentes Pattern para formatear fechas

Podemos modificar la fecha a travez de patterns y cambiarlo de ingles a español.
Es lo mismo que lo de abajo.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 73. Uso de Pipe para formatear fecha y uppercase en las plantillas html

A. Vamos a *clientes.component.html* y agregamos los pipes "|"

```typescript
<tr *ngFor="let cliente of clientes">
  ...
  <td>{{cliente.nombre | uppercase }}</td>
  ...
  <td>{{cliente.createAt | date: "EEEE dd, MMMM yyyy" }}</td>
```

B. Veremos que esta en ingles para cambiarlo a español.
Fuente: angular.io/api/core/LOCALE_ID

B.1. Vamos a *app.module.ts*

```typescript
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es-MX';

registerLocaleData(localeES, "es");

...
providers: [
 ...
 {provide: LOCALE_ID, useValue: 'es' }
],
```

**Resultado:** La fecha esta en un formato ejemplo "domingo 31, diciembre 2017"

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 74. Uso del operador tap en el Observable

Este no modifica los datos pero si podemos implementar algun tipo de logica
dentro de este.

A. Vamos a cliente.service.ts al metodo para obtener todos los clientes

```typescript
return this.httpClient.get(this.endpoint).pipe(
  tap(response => {
  let clientes = response as Cliente[]
  clientes.forEach(cliente => {
    console.log(cliente);
  });
  }),
  map(response => {...
```

**Nota:** Es importante el orden donde se llama el tap, ya que si lo
ponemos despues del map, el response ya estara convertido a un
arreglo de clientes, lo cual no seria necesario el "as".
**Nota2:** Podemos usar mas de un tap.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 11: Paginación desde el Backend

#### 75/Descargar - 76. Implementando paginación en API REST y Repository

Spring Data ya nos probee de metodos para paginar, por lo quer vamos a checar
la documentacion

A. vamos a spring.io -> projects -> Spring Data -> Learn -> Spring Data JPA -> learn -> Reference Doc.

  A.1. Aqui podremos ver una interface PagingAndSortingRepository que ya nos
  facilita la paginacion

  A.2. Tambien dice que podemos usar una aun mas completa "JpaRepository" la cual
  tiene estos metodos y mas.

B. vamos a *IClienteDao* y cambiamos la interface por

```java
public interface IClienteDao extends JpaRepository<ClienteEntity, Long>{
```

C. Vamos a *IClienteService* y agregamos

```java
List<ClienteEntity> findAll(org.springframework.data.domain.Pageable pageable);
```

D. Implementamos el metodo

```java
@Override
@Transactional(readOnly = true)
public Page<ClienteEntity> findAll(Pageable pageable) {
  return (Page<ClienteEntity>)clienteDao.findAll(pageable);
}
```

E. Vamos a nuestro controlador

```java
@GetMapping({ "", "/page/{page}" })
public Page<ClienteEntity> index(@PathVariable Integer page) {
  return clienteService.findAll(PageRequest.of(page, 10));
}
```

**Resultado:** Como podemos ver nos da informacion como cuantas paginas son,
si es la ultima o la primera etc que nos ayudaran para paginarlo.

![13_paginacion_postman](./img//13_paginacion_postman.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 77. Probar paginación API REST en POSTMAN

...

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 78. Modificando el Observable de Clientes en el FrontEnd

A. Vamos a *cliente.service.ts*

```typescript
getClientes(page: Number): Observable<any> {
    return this.httpClient.get(this.endpoint+"/page/"+page).pipe(
      map((response: any) => {  return response })
    );
  }
```

B. Vamos a clientes.component.ts

```typescript
ngOnInit() {
    let page = 0;
    this.clienteService.getClientes(page).subscribe(
      response => this.clientes = response.content as Cliente[]
    );
  }
```

**Resultado:** Estamos usando la paginacion por defecto consultamos
la pagina 0, falta implementar la navegacion entre paginas.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 79. Agregando la ruta page del paginador y el Observable ActivatedRoute

A. Vamos a *app.module.ts* y agregamos una nuava ruta

```typescript
const routes: Routes = [
  {path: 'clientes/page/:page', component: ClientesComponent},
];
```

B. Vamos a *clientes.component.ts* y agregamos el activateRoute para
observar la ruta.

C. Cachamos el valos de la ruta y lo mandamos

```typescript
ngOnInit() {
    this,this.activatedRoute.paramMap.subscribe(
      pageParam => {
        let page: number;
        page = +pageParam.get("page");
        if(!page) {
          page = 0;
        }
        this.clienteService.getClientes(page).subscribe(
```

**Resultado:** Podemos poner de forma manual la ruta y navegar entre
paginas de clientes, ejemplo: `http://localhost:4200/webpack-dev-server/clientes/page/3`

**Falta:**

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 80. Creando el componente paginador

A. Creamos un nuevo compoente: `ng g c paginador`

A.1. lo agregamos a *clientes.component.ts*

```html
<app-paginador></app-paginador>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 81. Inyectar paginador usando decorador @Input en el componente Paginador

A. vamos a *clientes.component.ts* y donde hacemos la consulta vamos a asignar
los campos de paginador que nos da spring a un objeto.

```typescript
...
this.clienteService.getClientes(page).subscribe(
  response => {
  this.clientes = response.content as Cliente[]
  this.paginador = response;
  }...
```

B. en *clientes.component.html* le inyectamos el nuevo valor,
B.1. le agregamos un condicional que se cree el componente en tanto
se tenga la informacion del serv.

```html
<app-paginador *ngIf="paginador" [paginador]="paginador"></app-paginador>
```

C. Lo marcamos inyectable y de este sacamos un arreglo de numeros

```typescript
...
  @Input()
  paginador: any;
  paginas: number[];

  ngOnInit() {
    this.paginas = Array(this.paginador.totalPages)
                        .fill(0)
                        .map((_valor, indice) => indice+1);
```

D. Mostramos los valores en forma de lista

```html
<ul *ngIf="paginas?.length>0" class="pagination">
    <li *ngFor="let pageNumber of paginas">
        {{pageNumber}}
    </li>
</ul>
```

**Resultado:** Se muestran los numeros de paginas que tenemos,
**Falta:** Agregar los links a cada uno.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 82. Implementando los links de las páginas

```html
<ul *ngIf="paginas?.length>0" class="pagination">
    <li class="page-item" *ngFor="let pageNumber of paginas">
        <span *ngIf="pagina-1 == paginador.number" style="background: gray">-{{pageNumber}}-</span>
        <a *ngIf="pagina-1 != paginador.number" class="page-link" [routerLink]="['/clientes/page',pageNumber-1]">{{pageNumber}} </a>
    </li>
</ul>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 85. Paginador con muchas páginas: overflow

...

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 13: Subida de imagen (Upload)

#### 91. Upload en el API Rest Controller (Backend)

Para poder subir archivos debemos de hacerlo con form data (No con json)
que soporte multi-part como requerimeinto.

A. Agregamos el campo al entity

```java
...
public class ClienteEntity implements Serializable{
  ...
+ private String imagen;
```

B. Vamos a *ClientesController.java* y creamos la implementacion de
subir el archivo
B.1. Conseguimos el cliente a darle la imagen
B.2. Para que no ahiga problemas con los nombres usamos UUID

```java
@PostMapping("/{id}/uploadImage")
public ResponseEntity<?> upload(@PathVariable Long id, @RequestParam("file") MultipartFile file) {
  Map<String, Object> response = new HashMap<String, Object>();

  //Subir foto
  if(file.isEmpty()) {
    response.put("mensaje", "No se encontro imagen a subir en la peticion");
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
  }

  String nombreArchivo = UUID.randomUUID().toString() + "_" + file.getOriginalFilename().replace(" ", "_");
  Path rutaArchivo = Paths.get("uploads").resolve(nombreArchivo).toAbsolutePath();
  try {
    Files.copy(file.getInputStream(), rutaArchivo);
  } catch (IOException e1) {
    e1.printStackTrace();
    response.put("mensaje", "Error al subir la imagen");
    response.put("descripcion", e1.getMessage());
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
  }


  //consegimos clietne al cual meterle la imagen
  ClienteEntity cliente = null;
  try {
    cliente = clienteService.findById(id);
  } catch (Exception e) {
    response.put("mensaje", "Error al consultar el cliente");
    if (e instanceof DataAccessException) { response.put("descripcion", ((DataAccessException) e).getMostSpecificCause().getMessage()); }
    else { response.put("descripcion", e.getMessage()); }

    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.FAILED_DEPENDENCY);
  }

  //asignamos nombrefot al cliente
  cliente.setImagen(nombreArchivo);

  //guardamos
  clienteService.save(cliente);


  response.put("mensaje", "Se subio correctamente la foto " + nombreArchivo);
  return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
}
```

C. Cambiar el tama;o que acepta la subida, por defecto esta maso 1mb
C.1. Vamos a *application.properties*

```properties
spring.servlet.multipart.max-file-size=10MB
spring.servlet.multipart.max-request-size=10MB
```

**Resultado:** Podremos ver como se sube la imagen en el directorio

![14_subir_img_postman](./img//14_subir_img_postman.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 93. Borrar imagen anterior al actualizar o eliminar

A. Antes de actualizar o eliminar al cliente debemos de eliminar la foto
para que no se queden sin asignacion las fotos.

```java
String fotAnterior = clietneAEliminar.getImagen();
ClienteFileUtils.borrarFoto(fotAnterior);
...
public static void borrarFoto(String fotAnterior) {
  if(fotAnterior != null && fotAnterior.length()>0) {
    Path rutaFAnt = Paths.get("uploads").resolve(fotAnterior).toAbsolutePath();
    File archAnteorior = rutaFAnt.toFile();
    if(archAnteorior.exists() && archAnteorior.canRead()) {
      archAnteorior.delete();
    }
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 94. Añadiendo método handler ver imagen en el Controlador

Vamos a crear una peticion para descargar/mostrar la foto en nuestro back
controlador java.

```java
@GetMapping("/{id}/image")
public ResponseEntity<?> showImage(@PathVariable Long id) {
  Map<String, Object> response = new HashMap<String, Object>();
  //consegimos clietne al cual meterle la imagen
  ClienteEntity cliente = null;
  cliente = clienteService.findById(id);
  //consegimo su imagen
  String nombreImagen = cliente.getImagen();
  if(nombreImagen == null || nombreImagen.isEmpty()) {
    response.put("mensaje", "El clietne no tiene imagen");
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
  }
  Path rutaArchivo = Paths.get("uploads").resolve(nombreImagen).toAbsolutePath();

  Resource responseImage;
  try {
    responseImage = new UrlResource(rutaArchivo.toUri());
  } catch (MalformedURLException e) {
    e.printStackTrace();
    response.put("mensaje", "Error al conseguir la ruta de la imagen");
    response.put("descripcion", e.getMessage());
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  if(!responseImage.exists() && !responseImage.isReadable()) {
    response.put("mensaje", "Error al consultar la imagen");
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  // agregamos header para indicar que es un recurso
  HttpHeaders h = new HttpHeaders();
  h.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+responseImage.getFilename()+"\"");

  //mandamos respuesta
  return new ResponseEntity<Resource>(responseImage, h, HttpStatus.OK);
}
```

**Resultado:** Al consultar `http://localhost:8080/api/clientes/3/image` en el
navegador se descargara la imagen del cliente.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 95. Añadiendo Logger en back-end

```java
Logger logger = LoggerFactory.getLogger(ClientesController.class);
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 96. Añadiendo componente detalle para el upload en el front-end

Vamos a agregar un componente el cual mostrara/descargara desde aqui
su imagen.

A. Generesmos un nuevo componente

```shell
ng g c clientes/clietne-detalle
```

B. El parametro de la ruta lo usamos para obtener el cliente

```typescript
export class ClietneDetalleComponent implements OnInit {
  cliente: Cliente;
  constructor(
    private clienteService: ClienteService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(
      params => {
        let idCliente:number = +params.get("id");
        if(idCliente) {
          this.clienteService.getClienteById(idCliente).subscribe(
            cliente => {
              this.cliente = cliente;
    });}});
```

C. Implementamos la vista con clases bootstrap

```html
<div class="card bg-dark text-white">
    <div class="card-header">{{titulo}}</div>
    <div class="card-body">
        <div class="input-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="inputGroupFile04">
                <label class="custom-file-label" for="inputGroupFile04">Subir Foto</label>
            </div>
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button">Button</button>
            </div>
        </div>
    </div>
</div>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 97. Implementando la subida de archivo en el front-end

A. vamos a *cliente.service.ts* y agregamos el metodo para subir foto

```typescript
subirFoto(id:number, archivo:File): Observable<any> {
  let urlGetCliente = this.endpoint+"/"+id+"/image";

  let formData = new FormData();
  formData.append("file", archivo);

  return this.httpClient.post<any>(urlGetCliente, formData).pipe(
    map((response:any) => response ),
    catchError(e => {
      Swal.fire("Error subir la imagen", e.error.mensaje, "error");
      this.router.navigate(['/clientes']);
      return throwError(e);
    })
  );
}
```

B. Vamos a *clietne-detalle.component.ts* y agregamos el
seleccionar foto y el subir foto.
**NOta:** asiganmos ña imagen a null para luego con un condicional
detecte el cambio y se actualize la foto subida.

```typescript
seleccionarFoto(event) {
 this.fotoSeleccionada = event.target.files[0];
}

subirFoto() {
 this.cliente.imagen = null;
 this.clienteService.subirFoto(this.cliente.id, this.fotoSeleccionada).subscribe(
  response => {
  this.cliente = response.cliente;
  Swal.fire("Subir foto", "Foto " + this.cliente.imagen + " de  " + this.cliente.nombre + " subida exitosamente", "success");
  }
);
}
```

C. Agregamos los eventos en *clietne-detalle.component.html*

```html
<input (change)="seleccionarFoto($event)" type="file" class="custom-file-input" id="inputGroupFile04">
...
<button (click)="subirFoto()" class="btn btn-outline-secondary" type="button">Button</button>
```

**Resultado:** Podemos subir la foto desde el formulario.
**Falta:** Una mejor precentacion para que el usuario.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 98. Añadiendo la foto y detalle del cliente en angular

A. Formateamos la vista y agregamos la imagen de modo que la muestre solo
si esta existe

```html
<div class="row">
<div class="col-sm">
  <ul class="list-group text-dark mb-3" *ngIf="cliente">
    <li class="list-group-item active">{{cliente.nombre}}</li>
    <li class="list-group-item">{{cliente.apellido}}</li>
    <li class="list-group-item">{{cliente.email}}</li>
    <li class="list-group-item">{{cliente.createAt}}</li>
  </ul>
  <div class="input-group">...</div>
</div>
<div class="col-sm" >
  <img *ngIf="cliente?.imagen" src="http://localhost:8080/api/clientes/{{cliente.id}}/image" width="200px"/>
</div>
</div>
```

**Resultado:** Podemos subir fotos y estas se actualizan
en tanto se guardan.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 99. Validando imagen antes de subir

A. Solo agregamos una validacion de que el tipo contenga imagen y
otra validacion de que si es nulo no se pueda subir

```typescript
seleccionarFoto(event) {
  this.fotoSeleccionada = event.target.files[0];
  let tipo:string = this.fotoSeleccionada.type;
  if(tipo.indexOf("image")<0) {
    Swal.fire("Subir foto", "El archivo debe de ser de tipo imagen", "error");
    this.fotoSeleccionada = null;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 100. Añadiendo Barra de Progreso

En la documentacion de angular en `https://angular.io/guide/http#listening-to-progress-events`
podemos encontrar como se hace una peticion con HttpRequest, la copiamos y la sustituimos
en nuestro service

A. Modificamos el Service de esta manera, donde usamos un reportProgress.

```typescript
subirFoto(id:number, archivo:File): Observable<HttpEvent<{}>> {
    let urlGetCliente = this.endpoint+"/"+id+"/image";

    let formData = new FormData();
    formData.append("file", archivo);

    const req = new HttpRequest('POST', urlGetCliente, formData, {
      reportProgress: true
    });

    return this.httpClient.request(req);
  }
```

B. El detallecomponente queda de la siguiente manera

```typescript
this.clienteService.subirFoto(this.cliente.id, this.fotoSeleccionada)
.subscribe(
  event => {
  this.cliente.imagen = null;
  if(event.type === HttpEventType.UploadProgress){
    this.progreso = Math.round((event.loaded/event.total)*100);
  } else if(event.type === HttpEventType.Response) {
    let response:any = event.body;
    this.cliente = response.cliente as Cliente;
    Swal.fire("Subir foto", "Foto " + this.cliente.imagen + " de " + this.cliente.nombre + " subida exitosamente", "success");
  }
  }
```

C. La barra de progreso es la siguinte

```typescript
<div class="col-sm center-block text-center" >
  <img *ngIf="cliente?.imagen" src="http://localhost:8080/api/clientes/{{cliente.id}}/image" class="img-thumbnail rounded" />
  <div class="progress" *ngIf="progreso>0">
    <div class="progress-bar" role="progressbar" [ngStyle]="{width:progreso+'%'}" attr.aria-valuenow="{{progreso}}" aria-valuemin="0" aria-valuemax="100">
      {{progreso}}%
    </div>
  </div>
</div>
```

**Resultado:**![15_subir_img_angular2](./img//15_subir_img_angular2.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 101. Implemementando Modal como componente anidado (hijo)

A. Eliminamos la ruta de detalles del cliente
B. En *clientes.component.html* agregamos el componente de cliente-detalles
asta arriba de todo el codigo.

```html
<clietne-detalle *ngIf="clietneSeleccionado" [cliente]="clietneSeleccionado"></clietne-detalle>
```

A. en *clietne-detalle.component.ts* ponemos su attr cliente
como Input y borramos el ngOnInit

```typescript
@Input()
private cliente: Cliente;

ngOnInit() { }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 102. Convertiendo a Modal de Bootstrap

Necesitaremos un nuevo compoente que contenga el detalle cliente y que
tenga la opcioon de abrir y cerrar.

A. Creamos un service

```shell
ng g s clientes/clietne-detalle/modal
```

B. Agregamos dos metodos sobre modal-service.ts

```typescript
export class ModalService {
  private model:boolean=false;
  constructor() { }
  abrirModal() { this.model = true; }
  cerrarModal(){ this.model = false; }
```

C. Agregamos el service dentro de *clientes.component.ts y clietne-detalle.component.ts*
el primero para abrirlo y el segundo para cerrarlo.
C.1. Lo implementamos en clientes.component

```typescript
abrirModalDetalleCliente(clienteSeleccionadoIn: Cliente) {
  this.clietneSeleccionado = clienteSeleccionadoIn;
  this.modalService.abrirModal();
}
```

C.2. Lo implementamos en *clietne-detalle.component.ts*

```typescript
cerrarModal() {
    this.modalService.cerrarModal();
    this.cliente.imagen = null;
    this.progreso = 0;
  }
```

A. Vamos a la doc de bootstrap buscamos modal y copiamos el
primer codigo

```html
<div *ngIf="modalService.model" class="modal" tabindex="-1" role="dialog" style="display:block;">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{titulo}}</h5>
        <button (click)="cerrarModal()" type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
            ...lo_mismo...
        </div>
    </div>
    <div class="modal-footer">
        <button (click)="cerrarModal()" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
    </div>
</div>
</div>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 103. Añadiendo estilos y animación de difuminado al Modal

Vamos a agregar un fondo gris y una animacion fadeIn para el mame
A. Vamos a *clietne-detalle.component.css*

```css
.abril-modal { background-color: rgba(0, 0, 0, 0.7); position: fixed;
top: 0px;  left: 0px; width: 100%; height: 100%; z-index: 1000; }
.animacion{ animation-duration: 1s; animation-fill-mode: both;
    -webkit-animation-duration: 1s; -webkit-animation-fill-mode: both;
}
.fade-in{ animation-name: fadeIn; -webkit-animation-name: fadeIn; }
@keyframes fadeIn { 0% {opacity:0;}to{opacity:1;} }
@-webkit-keyframes fadeIn { 0% {opacity:0;}to{opacity:1;} }
```

B. Aplicamos estilos *clietne-detalle.component.html* en el primer div

```html
<div class="abril-modal animacion fade-in" *ngIf="modalService.model">
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 104. Añadiendo la foto en el listado

A. Vamos a *clientes.component.html* y agregamos 2 (cuando tiene y cuado
no tiene) img iguales y los ponemos en lugar del boton.

```html
<img *ngIf="cliente?.imagen" (click)="abrirModalDetalleCliente(cliente)"
src="http://localhost:8080/api/clientes/{{cliente.id}}/image"
class="img-thumbnail rounded"
style="width: 64px; height: 64px; cursor:pointer"/>

<img *ngIf="!cliente?.imagen" ... src="http://localhost:8080/img/_default.png"...
```

A. En la carpeta agregamos la carpeta */clientesappback/src/main/resources/static/img*
y dentro ponemos la imagen por defult.

A. En caso de que el cliente tenga una img registrada pero esta
no este en la carpeta, en el back podmeos cambiar para que envie
la img por defecto.

```java
rutaArchivo = Paths.get("src/main/resources/static/img/").resolve("_default.png").toAbsolutePath();
```

**Resultado:** Podemos ver una pequeña imagen en el listado y si
la subimos y luego borramos el archivo a las malas, aun asi todos
regresaran una imagen por defecto.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 105. Implementando EventEmitter para actualizar la foto en el listado

A. Vamos a *modal.service.ts* Creamos un componente de tipo observable

```typescript
export class ModalService {
  ...
  private _notificarUpload = new EventEmitter();

  get notificarUpload() {
    return this._notificarUpload;
  }
  ...
```

A. Vamos a *clietne-detalle.component.ts* y emitimos un evento
mandando el cliente con la nueva imagen para que lo vea la lista.

```typescript
...
this.modalService.notificarUpload.emit(this.cliente);
...
Swal.fire("Subir foto", "Foto " + this.cliente.imagen + " de " + this.cliente.nombre + " subida exitosamente", "success");
```

A. Vamos a *clientes.component.ts* y escuchamos el evento
para cambiar la imagen de la lista iterandola para descubrir
cual fue el que cambio.

```typescript
ngOnInit() {
    this,this.activatedRoute.paramMap.subscribe(...);

    this.modalService.notificarUpload.subscribe(clienteEvento => {
      this.clientes = this.clientes.map(clienteOriginal => {
        if(clienteOriginal.id == clienteEvento.id){
          clienteOriginal.imagen = clienteEvento.imagen;
        }
        return clienteOriginal;
      } )
    });
  }
```

**Commit**: gitlab.com/MarioM51/clientes-app-back/commit/a2228797754b85b8b0ff8aa08950a4aa3d076462
**Resultado:** ![1_menu_bootstrap](./img//15_lista_imagenes.gif)

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 14: Agregando campo select en el formulario y relación de tablas

Vamos a crear una relacion uno a muchos, en este caso una region tiene
muchos clientes, esto lo haremos a traves de mostrar un select con las
diferentes regiones

#### 108. Creando nueva clase entity Region

Todos del paquete persistence
A. Creamos la entidad region

```java
@Entity
@Table(name="regiones")
public class RegionEntity implements Serializable {
  private static final long serialVersionUID = 1l;

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private long id;

  private String nombre;
```

B. Agregamos en clietne la relacion de region

```java
@ManyToOne(fetch=FetchType.LAZY)
@NotNull(message="La region no puede estar vacia")
@JoinColumn(name="region_id")
//ignoramos campos generados automaticamente por el ORM para ahorrarnos problemas con generar el json
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
private RegionEntity region;
```

C. Tenemos dos formas de recuperar/escribir la region una
es crear su propio repository y otra ahi mismo, en este caso
como es poco lo que haremos con la region lo haremos dentro
del repository del cliente.

```java
public interface IClienteDao extends JpaRepository<ClienteEntity, Long>{
  @Query("from RegionEntity")
  public List<RegionEntity> findAllRegiones();
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 109. Añadiendo las regiones en la clase service y controller del backend

A. Vamos a IClienteService.java y agregamos el metodo para consegir todas las regiones.

```java
public interface IClienteService {
  ...
  List<RegionEntity> findAllRegiones();
```

B. Implementamso el metodo

```java
@Override
@Transactional(readOnly = true)
public List<RegionEntity> findAllRegiones() {
  return clienteDao.findAllRegiones();
}
```

C. En el controlador agregamos de clietnes

```java
@GetMapping("/regiones")
public List<RegionEntity> getAllRegiones() {
  return clienteService.findAllRegiones();
}
```

D. Agregamos en modificar el nuevo campo

```java
...
clienteActual.setRegion(cliente.getRegion());
...
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 110. Probando API Rest de las regiones en Postman

A. Insetamos unas regiones por defecto en el sql

```sql
INSERT INTO regiones (id, nombre) VALUES (1, 'Sudamérica');
INSERT INTO regiones (id, nombre) VALUES (2, 'Centroamérica');
INSERT INTO regiones (id, nombre) VALUES (3, 'Norteamérica');
INSERT INTO regiones (id, nombre) VALUES (4, 'Europa');
INSERT INTO regiones (id, nombre) VALUES (5, 'Asia');
INSERT INTO regiones (id, nombre) VALUES (6, 'Africa');
INSERT INTO regiones (id, nombre) VALUES (7, 'Oceanía');
INSERT INTO regiones (id, nombre) VALUES (8, 'Antártida');

INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(1, 'Andrés', 'Guzmán', 'profesor@bolsadeideas.com', '2018-01-01');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(2, 'Mr. John', 'Doe', 'john.doe@gmail.com', '2018-01-02');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(4, 'Linus', 'Torvalds', 'linus.torvalds@gmail.com', '2018-01-03');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(4, 'Rasmus', 'Lerdorf', 'rasmus.lerdorf@gmail.com', '2018-01-04');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(4, 'Erich', 'Gamma', 'erich.gamma@gmail.com', '2018-02-01');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Richard', 'Helm', 'richard.helm@gmail.com', '2018-02-10');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Ralph', 'Johnson', 'ralph.johnson@gmail.com', '2018-02-18');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'John', 'Vlissides', 'john.vlissides@gmail.com', '2018-02-28');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Dr. James', 'Gosling', 'james.gosling@gmail.com', '2018-03-03');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(5, 'Magma', 'Lee', 'magma.lee@gmail.com', '2018-03-04');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(6, 'Tornado', 'Roe', 'tornado.roe@gmail.com', '2018-03-05');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(7, 'Jade', 'Doe', 'jane.doe@gmail.com', '2018-03-06');
```

**Error:** Type definition error: [simple type, class org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor]; nested exception is com.fasterxml.jackson.databind.exc.InvalidDefinitionException: No serializer found for class org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) (through reference chain: com.mariocorps.clientesappback.entity.ClienteEntity[\"region\"]->com.mariocorps.clientesappback.entity.RegionEntity$HibernateProxy$bxGicZ6n[\"hibernateLazyInitializer\"])
**Descripcion:** Al consultar un cliente o varios en postman marca este error.
**Causa:** La dependencia encargada de serializar la clase a json no puede
por la variable que tenemos una relacion muchos a uno.
**Solucion:** Tenemos que ignorar los campos generados automaticamente por el
ORM al tener una relacion.

```java
//SOLUCION
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
private RegionEntity region;
```

**Resultado:** Tenemos la ruta {{base}}/clientes/regiones que nos retorna
todas las regiones.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 111. La clase TypeScript Region en Angular

A. Creamos el nuevo modelo *Region.ts*

```typescript
export class Region {
    id: number;
    nombre: string;
}
```

B. En *cliente.ts* agregamos el nuevo campo

```typescript
import { Region } from "./Region";
export class Cliente {
    ...
    region: Region;
}
```

C. vamos a *clietne-detalle.component.html* y mostramos el nuevo campo

```html
<li class="list-group-item">{{cliente.region.nombre}}</li>
```

D. Vamos a *cliente.service.ts* para consultar las regiones

```typescript
getRegiones(): Observable<Region[]> {
  return this.httpClient.get<Region[]>(this.endpoint+"/regiones");
}
```

E. Vamos a *form.cliente.component.ts* y agregamos el consumo
regiones para llenar el formulario.

```typescript
=ngOnInit() {
=  this.loadCliente();
+  this.clienteService.getRegiones().subscribe(
+    response => {
+      this.regiones = response;
+    }
+  );
=}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 112. Añadiendo el campo select region en el formulario

E. Vamos a *form.cliente.component.html* y mostramos las regiones.

```html
<div class="form-group row">
  <label for="region" class="col-form-label col-sm-2">Region: </label>
  <div class="col-sm-6">
    <select type="text" class="form-control" [(ngModel)]="cliente.region" name="region">
      <option *ngFor="let region of regiones" [ngValue]="region">{{region.nombre}}</option>
    </select>
  </div>
</div>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 113. Comparando la región en el campo select para marcarla como seleccionada

**Problema:** Cuando queremos editar un cliente el
campo region no se selecciona de forma automatica.
Lo que haremos es comparar las regiones con las
del usuario actual para seleccionarlo.

A. En *form.cliente.component.html* agregamos un compareWith

```html
<select [compareWith]="compararRegion" type="text" class="form-control" [(ngModel)]="cliente.region" name="region">
```

B. En el *form.cliente.component.html
<!-- ternario, comprimir cindicion-->
```typescript
compararRegion(regionAComparar:Region, regionActual:Region): boolean {
    let igual = (regionActual==null || regionAComparar==null) ? false : regionActual.id===regionAComparar.id;
    return igual;
  }*
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 114. Añadiendo opción por defecto con el texto seleccionar

A. En *form.cliente.component.html* agregamos una opcion undifined.

```html
<select [compareWit...
    <option [ngValue]="undefined">---Seleccionar region---</option>
...
```

B. Agregamos una condicion que si ambas regiones son null retornamos un true

```typescript
compararRegion(regionAComparar:Region, regionActual:Region): boolean {
  if(regionAComparar==undefined && regionActual==undefined) { return true; }
  let igual = (regionActual==null || regionAComparar==null) ? false : regionActual.id===regionAComparar.id;
  return igual;
}
```

**Resultado:** ![ngif.gif](./img//16_uno_a_muchos.gif)

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 15: Autenticación OAuth2 con JWT: Backend Spring

#### 116. Introducción a JSON Web Token (JWT)

Este nos sirve para no acoplar nuestro back y front, asi pudiendo
usar nuestra API en web, mobil, escritorio, etc sin problemas.
Nos vasaremos en el standard **RFC 7519**

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 117. Algo más sobre los JWT

Nunca debemos de guardar informacion sensible en los JWT ya que
cualquiera con algo de conocimiento en JS podra ver la informacion
que guardamos ahi. Esto decodificando el base64 con la sig. funcion
de javascript.

```javascript
> let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwibmFtZSI6Ik1hcmlvIiwiaWF0IjoxNTE2MjM5MDIyfQ.7tH23HplPRRbdhENHz2XntvCC2mrvca2wMUP_FhRn5Y";
> window.atob(token.split(".")[1]);
< "{"sub":"1","name":"Mario","iat":1516239022}"
```

Lo que hace seguro al JWT es que a travez de una clave secreta
que solotenemos nosotros en el back, podemos ver que el JWT no
fue modificado y fue creado por nosotros.

Nosotros usaremos la especificacion de Spring que podemos ver
en spring.oi -> projects -> Spring Security -> OAuth2 ->
reference docs -> learn [link](https://projects.spring.io/spring-security-oauth/docs/Home.html).

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 118. Introducción a OAuth2 y añadiendo las dependencias

A. Buscamos en mvnrepository.com por spring-security-oauth2 y seleccionamos la
vercion estable mas reciente. y pegamos en nuestro pom

B. Buscamos la dependencia *spring security jwt*

```xml
<!-- https://mvnrepository.com/artifact/org.springframework.security.oauth/spring-security-oauth2 -->
<dependency>
    <groupId>org.springframework.security.oauth</groupId>
    <artifactId>spring-security-oauth2</artifactId>
    <version>2.3.6.RELEASE</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.springframework.security/spring-security-jwt -->
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-jwt</artifactId>
    <version>1.0.10.RELEASE</version>
</dependency>
```

**Nota:** Si usamos el jdk mayor al 8, entonces debemos de incluir
otra dependencia ya que del jdk-9 en adelante se elimino la
libreria jaxb.
En mi caso tenla 1.8 por lo que no fue necesario en ki caso.
C. Buscamos por "jaxb" en mvnrepository.com, y escojemos JAXB API de
javax.xml.bind
C.1. Le quitamos la version ya que esta nos la manejara Spring

```xml
<!-- https://mvnrepository.com/artifact/javax.xml.bind/jaxb-api -->
<dependency>
    <groupId>javax.xml.bind</groupId>
    <artifactId>jaxb-api</artifactId>
</dependency>

<!-- https://mvnrepository.com/artifact/org.glassfish.jaxb/jaxb-runtime -->
<dependency>
    <groupId>org.glassfish.jaxb</groupId>
    <artifactId>jaxb-runtime</artifactId>
</dependency>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 119. Creando las entidades necesarias Usuario y Role

Tendremos el siguiente diagrama, que es una relacion un
usuario puede tener muchos roles y un rol puede tener
muchos roles.
![1_menu_bootstrap](./img//17_diagrama_er_usuarios_m_a_m_roles.PNG)

A. Creamos la entidad usuario

```java
@Entity
@Table(name="usuarios")
public class UsuarioEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  @Column(unique=true, length=20)
  private String username;

  @Column(length=60)
  private String password;

  private Boolean Enabled;

  @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
  private List<RolEntity> roles;
```

B. Creamos la entidad roles, podemos ver que podemos
declarar la relacion pero no la nacesitamos en este
ejemplo.

```java
@Entity
@Table(name="roles")
public class RolEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  @Column(unique=true, length=20)
  private String nombre;

  /* obtemos la relacion inversa pero no la necesitamos
  @ManyToMany(mappedBy="roles")
  List<UsuarioEntity> usuarios;
  */
```

C. **Opcional**: podemos definir nosotros el nombre de la
tabla muchos a muchos, y un constrain para no repetir los
permisos a un usuario.

```java
@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
@JoinTable(
    name="rel_usuarios_roles",//nombre de la tabla muchosAmuchos
    joinColumns=@JoinColumn(name="user_id"), //nombre de la FK de usuarios
    inverseJoinColumns=@JoinColumn(name="role_id"), //nombre de la FK de roles
    //indicamos que no se repita osea que unusr no tenga el mismo permiso 2 veces
    uniqueConstraints= {@UniqueConstraint(columnNames={"user_id", "role_id"}) })
private List<RolEntity> roles;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 120. Creando el repositorio JPA IUsuarioRepository

Los dos metodos hacen lo mismo, pero uno lo hacemos por hql y otro por la construccion
automatica de spring.

```java
public interface IUsuarioRepository extends CrudRepository<UsuarioEntity, Long>{

  UsuarioEntity findByUsername(String username);

  @Query("select u from UsuarioEntity u where u.username=?1")
  UsuarioEntity seleccionaPorNombreusuario(String username);

}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 121. Creando la clase de servicio UsuarioService

A. Creamos el service a travez de la interface que nos provee Spring Security

```java
@Service
public class LoginService implements UserDetailsService{

  Logger logger = LoggerFactory.getLogger(LoginService.class);

  @Autowired
  private IUsuarioRepository usuarioRepository;

  @Override
  @org.springframework.transaction.annotation.Transactional(readOnly=true)
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UsuarioEntity usuario = usuarioRepository.seleccionaPorNombreusuario(username);
    if(usuario==null) {
      logger.error("Login Error, No existe el usuario");
      throw new UsernameNotFoundException("Login Error, No existe el usuario");
    }

    //obtenemos los permisos del usuario y convertimos los obj a GrantedAuthority
    List<GrantedAuthority> auths = usuario.getRoles().stream()
        .map(role -> new SimpleGrantedAuthority(role.getNombre()))
        .peek(authority -> logger.info("Rol: " + authority.getAuthority()))
        .collect(Collectors.toList());

    return new org.springframework.security.core.userdetails.User(usuario.getUsername(), usuario.getPassword(),
        usuario.getEnabled(), true, true, true, auths);
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 122. Añadiendo la clase SpringSecurityConfig y registrando UserDetailsService JPA

Con esto haremos la configuracion de Spring Security respecto a la gestion de
la autenticacion, en el cual registramos el servicio que hicimos arriba.

A. Creamos la clase de configuracion
A.1. La anotamos como configuracion @Configuration
A.2. Importamos el service de login
A.3. hacemos Override sobre configure(AuthenticationManagerBuilder)

```java
@org.springframework.context.annotation.Configuration
public class ConfigSeguridadSpring extends WebSecurityConfigurerAdapter {

  @Autowired
  private LoginService loginService;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(this.loginService)
      .passwordEncoder(passwordEncoder())
    super.configure(auth);
  }

  @Bean //de esta manera registramos lo que retorna en el contexto
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  // Como vamos a utilizarlo mas adelante lo registramos en el spring context
  // le damos nombre para identificarlo facilmente por si hay otro creado
  // Recordar que va a tener el mimo nombre que el metodo por defecto,
  // le definimos un nombre solo para dejarlo de forma explicita
  @Bean("authenticationManager")
  @Override
  protected AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
  }

  // CUIDADO: no se comparan los dos encriptados, el primero es en plano
  // y el segundo va en plano
  // comparar hash, comparar encriptados, ver si dos hash son iguales
  @Test public void escriptados_iguales() {
    BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
    String crip1 = bcrypt.encode("Mario");
    boolean resp = bcrypt.matches("Mario", crip1);
    assertTrue(resp);
  }

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 123. Añadiendo la configuración para el servidor de autorización

Crearemos la cinfiguracion de autenticacion y la creacion del token
para retornarlo.
A. Creamos la clase de configuracion AuthorizationServerConfig

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 124. Añadiendo configuración de las aplicaciones clientes y acceso a endpoints

Configuramos las aplicaciones que tendran acceso a nuestra API, en nuestro caso solo
tenemos una aplicacion (front web con angular)
...Leer comentarios del codigo

```java
package com.mariocorps.clientesappback.auth;
import org.springframework.beans.factory*;
import org.springframework.context.annotation.*;
import org.springframework.security.*;
import org.springframework.security.oauth2.*;

/*
  Esta clase se encargara del token, cosas como crearlo, activarlo,
checar su caducidad etc.
*/
@Configuration
@EnableAuthorizationServer // traemos la configuracion minima, pero tendremos que agregarle nosotros que funcione con jwt
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

  @Autowired
  private BCryptPasswordEncoder passEncoder;

  // Este es el que construimos en WebSecurityConfigurerAdapter.configure(AuthenticationManagerBuilder...
  @Autowired
  @Qualifier("authenticationManager")
  private AuthenticationManager authManager;

  // Este metodo vera la autenticacion y si el usuario y contraseña son correctos entonces
  // generara el token
  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    // Le damos nuestro authenticationManager para que verifique el usuario y contraseña
    endpoints.authenticationManager(this.authManager)
      // este ya ocurre de forma automatica donde registra un "TokenStore" pero lo hacemos de
      // forma explicita para dejarlo e forma explicita
      .tokenStore(tokenStore())
      // el que le pasemos por accessTokenConverter() funcionara como traductor para que pueda
      // guardar/leer la informacion del usuario en el token CUIDADO: no guarar informacion sensible
      // ya que se podrian extraer por un tercero
      .accessTokenConverter(accessTokenConverter())
    ;
  }

  // Debe ser publico para que se registre en nuestro spring context
  //JwtAccessTokenConverter: es solo una interface,
  @Bean
  public JwtAccessTokenConverter accessTokenConverter() {
    JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
    return jwtAccessTokenConverter;
  }

  @Bean
  public TokenStore tokenStore() {
    return new JwtTokenStore(this.accessTokenConverter());
  }

  // No solo los usuarios se identifican tambien se identifican las aplicaciones

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.inMemory() // tipo de almacenamiento del clietne
      .withClient("clientesangularapp") //nombre de la app
      .secret(passEncoder.encode("12345")) //contraseña de la app
      .scopes("read", "write") //los permisos que tiene la app
      // "password" - la forma que obtendremos el token, que es por username y passº
      // "refresh_token" - para que se refresque el token en cada movimiento mientras no caduque
      .authorizedGrantTypes("password", "refresh_token")
      .accessTokenValiditySeconds(120)//tiempo de cadcidad del token
      .refreshTokenValiditySeconds(120);// tiempo en el que se generara un nuevo token
  }

  @Override
  public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
    //Aqui configuraremos las rutas, por ejemplo el login es una ruta publica ya que todos los usuarios
    //pueden/deben de poder consumir la api e login para identificarse
    security
      .tokenKeyAccess("permitAll()")//asi empezaremos a marcar las rutas que tienen acceso publico
      .checkTokenAccess("isAuthenticated()") //aqui configuramos las rutas que el usuario requiere el token pa acceder
    ;
  }

}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 125. Añadiendo la configuración para el servidor de recurso

Configuraremos el ResourceServer, que indicaremos que recurso podemos dar a X usuario,
el cual permitira su acceso si el token es valido en caso contrario lo denegara.

A. Creamos la clase de configuracion ResourceServerConfig,
A.1. Sobre-escribimos el metodo configure

```java
import org.springframework.*;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

  //aqui configuraremos las reglas de seguidad de nuestras rutas
  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
      .antMatchers(HttpMethod.GET, "api/clientes").permitAll() //damos acceso a todos al recurso usuarios
      // Cualquier otra peticion, debe esta autenticado
      // Este al ultimo para que sea la regla que predomine ej. si se nos olvida configurar una nueva ruta
      // por defecto la denegara
      .anyRequest().authenticated()
    ;

  }

}
```

A. Copiamos el metodo tal cual esta y lo pasamos a nuestra impl de
WebSecurityConfigurerAdapter en mi caso clientesappback.auth.ConfigSeguridadSpring
B.1. Desactivamos la proteccion CSRF(proteccion de formularios)

<!-- deshabilitar cookies, quitar cookies -->

```java
public class ConfigSeguridadSpring extends WebSecurityConfigurerAdapter {

  ...

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
      //.antMatchers(HttpMethod.GET, "api/clientes").permitAll() //DESACTIVAMOS ESTA LINEA PARA MANEJARLA CON NUESTRO ResourceServerConfigurerAdapter
      .anyRequest().authenticated() //Cualquier otra peticion, debe esta autenticado
      .and()
      .csrf().disable() //Desactivamos la proteccion CSRF(proteccion de formularios)
      //DESACTIVAMOS EL USO DE SESSIONES
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    ;
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 126. Probando la autenticación con Postman y obteniendo el token JWT

A. Creamos nuestros usuarios de prueba
A.1. Para generar nuestro hash de contrasenia lo hacemos a traves de un
script implementando ComandLineRunner para hacerlo antes de que levante
nuestro back.

```java
@ComponentScan({"com.mariocorps.clientesappback", })
@SpringBootApplication
public class ClientesappbackApplication implements CommandLineRunner {

  @Autowired BCryptPasswordEncoder bCryptPasswordEncoder;

  public static void main(String[] args) { SpringApplication.run(ClientesappbackApplication.class, args); }

  @Override
  public void run(String... args) throws Exception {
    String password = "mario";
    for (int i=0; i<4; i++) { String passEncode = bCryptPasswordEncoder.encode(password); System.out.println(passEncode); }
  }
}
```

A.2. Corremos la aplicacion para que se generen las claves.

> **ERROR:** Error creating bean with name 'springSecurityFilterChain' defined in class path resource [org/springframework/security/config/annotation/web/configuration/WebSecurityConfiguration.class]:
> Bean instantiation via factory method failed; nested exception is org.springframework.beans.BeanInstantiationException: Failed to instantiate [javax.servlet.Filter]: Factory method 'springSecurityFilterChain' threw exception;
> nested exception is java.lang.IllegalArgumentException: You must provide a configuration attribute.
> **SULUCION:** Me falto agregar un attr.

```java
class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
  ...
  @Override public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
    security.tokenKeyAccess("permitAll()")
      .checkTokenAccess("")                   // <-- ESTE CAUSABA EL ERROR
      .checkTokenAccess("isAuthenticated()"); // <-- FORMA CORRECTA
  }
  ...
```

A.3. Al terminar de lebantar nuestro back se mostraran los hash que
mandamos a imprimir, lo cuales usamos para incertar en nuestras tablas.

```sql
/* Creamos algunos usuarios con sus roles */
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('andres','$2a$10$.w48xpCmPnTmtX6Ab26Pje.L3fW9Dk8pNHyD9crHR3Bfc9aJQahy2',1, 'Andres', 'Guzman','profesor@bolsadeideas.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$SeR84JBtVbxOYz/pTnFYnO/tDm6BHvLxSuWjjG2GBfy1KfYcB7Exm',1, 'John', 'Doe','jhon.doe@bolsadeideas.com');

INSERT INTO `roles` (id, nombre) VALUES (1, 'ROLE_USER');
INSERT INTO `roles` (id, nombre) VALUES (2, 'ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);
```

A. **Resultado**: Si queremos consumir cualquier ruta de nuestro back
sin un token este nos mandara no autorizado.

B. Iniciar session desde postman:

```r
REQUEST
  URL
    POST - http://localhost:8080/oauth/token

  AUTH
    Type:      Basic Auth
    Username:  clientesangularapp    //se consiguen en lo que
    Password:  12345                 //pusimos en AuthorizationServerConfigurerAdapter

  HEADERS
    Content-Type:    application/x-www-form-urlencoded
    Authorization:   Basic Y2xpZW50ZXNhbmd1bGFyYXBwOjEyMzQ1  //se genera automaticamente "preview request"

  BODY  //las credenciales de nuestro usr
    username:    andres
    password:    12345
    grant_type:  password

RESPONSE
{
    "access_token": "eyJhbGc...",
    "token_type": "bearer",
    "refresh_token": "eyJhb...",
    "expires_in": 119,
    "scope": "read write",
    "jti": "6ba0d040-f4cb-4e69-aeb3-f276e374efd0"
}
```

C. Hacer una peticion a un recurso pasando el token.
Para esto la conservamos de la misma manera, pero en la
pesaña "Auth" se pone "Bearer Token" y abajo le damos el
token que se nos dio en "access_token". Lo que generara un
header en la peticion "Authorization: 'Bearer eyJhbGciOiJIU'"

**Resultado:** ![1_menu_bootstrap](./img//18_peticion_pasando_token.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 127. Asignando la llave secreta Mac para firmar el token JWT

La firma de nuestro token que creamos se creo de forma automatica, pero podemos
hacer la firma de forma manual. y en el cuarso veremos 2 formas diferentes con el
algoritmo **HS256** que usa una clave MAC (clave de autenticacion de mensaje) y
el **RS256** este ultimo es mas robusto ya que usa clave publica y privada.

A. **Asignar una firma con el algoritmo HS256**
A.1. Vamos a nuestro AuthorizationServerConfigurerAdapter y nos metemos en la clase **JwtAccessTokenConverter**
aqui podremos ver un valor llamado verifierKey que lo trae un metodo `RandomValueStringGenerator().generate();`,
ahi mismo tenemos un metodo llamado *setSigningKey* para poder poner nuestra propia llave.

A.2. definimos nuestra clave secreta

```properties
############  application.properties  ###########

#Configuracion de Seguridad
com.appclientesback.clave-token=mi.clave.secreta.mario
```

A.2. Agregamos nuestra clave secreta a JwtAccessTokenConverter

```java
=  @Configuration
=  @EnableAuthorizationServer
=  public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
+    @Value("${com.appclientesback.clave-token}")
+    private String claveFirmaToken;
=    @Bean
=    public JwtAccessTokenConverter accessTokenConverter() {
=      JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
+      logger.info("Clave Secreta: " + claveFirmaToken);
+      jwtAccessTokenConverter.setSigningKey(claveFirmaToken);
=      return jwtAccessTokenConverter;
=    }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 128. Creando y asignando certificado RSA para firmar el token JWT

A. Vamos a openssl.org -> community -> binaries -> wiki, y aqui podemos
descargar el instalador de windows en mi caso descarge la completa
"Win64 OpenSSL v1.1.0k"

B. Ya instalado, generamos la clave privada.

```shell
openssl genrsa -out jwt.pem
```

B. Mostramos la llave generada para poder copiarla

```shell
openssl rsa -in jwt.pem
```

C. Generamos la clave publica

```shell
openssl rsa -in jwt.pem -pubout
```

D. Guardamos nuestras llaves en nuestro .properties, nota hacemos el salto de
linea con el slash invertido "\"

```properties
com.appclientesback.clave-privada=-----BEGIN RSA PRIVATE KEY-----\
MIIEogIBAAKCAQEAzGvkEUrHzAV+G9tHKCnqXoxnomwszHJlmW6d+UT6opSH2bDY\
...
yosmcSbogn3AiB/yKJXEhkSBDnCBw4NkKephuIZQCNy1pem5EgISlWlYMZkOQd4L\
i9os0mHKBA4j2xQaQjIdm9ZOHKOzJ3MuLVGxr5/rjDW7J/VB4Ls=\
-----END RSA PRIVATE KEY-----

com.appclientesback.clave-publica=-----BEGIN PUBLIC KEY-----\
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzGvkEUrHzAV+G9tHKCnq\
...
pqOsuLe3W+o7aSiNg57SEft66NrEn7IsbzC1XJRksIzeDguNWON2RFXIkFQAhAu7\
mwIDAQAB\
-----END PUBLIC KEY-----
```

E. Los seteamos en nuestra configuracion

```java
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

// traer variables de properties, vearibles del properties, variable de properties,
// traer variables de .properties, vearibles del .properties variable de .properties ,

+  @Value("${com.appclientesback.clave-privada}")
+  private String clavePrivadaFirmaToken;

+  @Value("${com.appclientesback.clave-publica}")
+  private String clavePublicaFirmaToken;

  @Bean
  public JwtAccessTokenConverter accessTokenConverter() {
    JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
+    jwtAccessTokenConverter.setSigningKey(clavePrivadaFirmaToken);
+    jwtAccessTokenConverter.setVerifierKey(clavePublicaFirmaToken);
    return jwtAccessTokenConverter;
  }

```

F. Deberia de segur funcionando igual desde postman, solo que este
sistema es mas robusto/seguro.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 129. Añadiendo más datos en el token JWT

A. Creamos una I para implementar nuestros metodos personalizados
para nuestra impl de UserDetailsService.

```java
package com.mariocorps.clientesappback.service;
import com.mariocorps.clientesappback.entity.UsuarioEntity;
public interface ILoginService {
  UsuarioEntity seleccionaPorNombreusuario(String username);
}
```

B. la implementamos sobre nuestro UserDetailsService

```java
@Service
public class LoginService implements ILoginService, UserDetailsService {
...
  @Override
  public UsuarioEntity seleccionaPorNombreusuario(String username) {
    return usuarioRepository.seleccionaPorNombreusuario(username);
  }
}
```

C. Llamamos el service desde nuestro TokenEnhancer

```java
package com.mariocorps.clientesappback.auth;
import java.util.*;
import org.springframework.security.*;
import com.mariocorps.clientesappback.*;
@Component
public class InfoAdicionalToken implements TokenEnhancer {
  @Autowired ILoginService loginService;
  @Override
  public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
    //obtenemos informacion del usr a travez de su nombre
    UsuarioEntity user = loginService.seleccionaPorNombreusuario(authentication.getName());

    //agregamos info adicional al tokne
    Map<String, Object> info = new HashMap<String, Object>();
    info.put("user_id", user.getId());

    //enviamos info
    ((DefaultOAuth2AccessToken)accessToken).setAdditionalInformation(info);
    return accessToken;
}  }
```

D. Registramos el componente en la configuracion de creacion de token (AuthorizationServerConfigurerAdapter).
en el AuthorizationServerEndpointsConfigurer.

```java
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
  ...
+  @Autowired
+  InfoAdicionalToken infoAdicionalToken;

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
+    TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
+    tokenEnhancerChain.setTokenEnhancers(Arrays.asList(infoAdicionalToken, accessTokenConverter()));
    endpoints.authenticationManager(this.authManager)
    .tokenStore(tokenStore())
    .accessTokenConverter(accessTokenConverter())
+    .tokenEnhancer(tokenEnhancerChain);
  }
```

**Resultado:** Tenemos un nuevo campo en nuestro token, ![1_menu_bootstrap](./img//19_token_info_adicional.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 130. Complementando con más información adicional del usuario autenticado

A. **Recordatorio**. En los @Service manejar lo que es `Transactional(readOnly=true)`
como buena practica, ya que aqui podemos manejar varios DAOs/Repositories y esta etiqueta
ayuda a que la tranzaccion se cierre al terminar estos multiples DAOs/Repositories.

...Se muestra como agregar mas campos pero es lo mismo solo se agregan mas campos en
`enhance(OAuth2AccessToken, OAuth2Authentication)`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 131. Agregando seguridad a nuestras rutas de forma programática con HttpSecurity

**Consejo** para definir la reglas de acceso se recomienda empezar de lo mas especifico
a lo mas generico

<!-- configuracion de seguridad de las rutas, definir seguridad de las rutas, seguridad rutas, seguridad apis-->
```java
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
      .antMatchers(HttpMethod.GET, "/api/clientes", "/api/clientes/page/{page}", "/api/clientes/{id}/image").permitAll() //damos acceso a todos al recurso usuarios
      .antMatchers(HttpMethod.GET, "/api/clientes/{id}").hasAnyRole("USER", "ADMIN")
      .antMatchers("/api/clientes/**").hasRole("ADMIN")//indicamos que esta ruta y X otra cosaen cualquier metodo no definido(POST, PUT, DELETE etc)
      .anyRequest().authenticated() //Cualquier otra peticion, debe esta autenticado
    ;
```

Para esta configuracion deberiamos de tener los siguientes accesos
A. Acceso publico (sin auth)
A.1. enlisatar todos los clientes -   GET {{base}}/clientes
A.2. Enlisatar clientes por paginas -   GET {{base}}/clientes/page/3

B. Solo Admin
B.1. Agregar usuario - POST {{base}}/clientes

...etc

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 132. Añadiendo seguridad a nuestras ruta del controlador usando anotaciones @Secured

**Objetivo:** Haremos Las mismas reglas que arriba pero dede anotaciones.

A. Borramos las reglas de arriba. dejando los publicos y el todo lo demas auth

```java
=  @Override
=  public void configure(HttpSecurity http) throws Exception {
=    http.authorizeRequests()
=      .antMatchers(HttpMethod.GET, "/api/clientes", "/api/clientes/page/{page}", "/api/clientes/{id}/image").permitAll()
-      //.antMatchers(HttpMethod.GET, "/api/clientes/{id}").hasAnyRole("USER", "ADMIN")
-      //.antMatchers("/api/clientes/**").hasRole("ADMIN")
-      .anyRequest().authenticated();
=  }
```

B. En nuestro WebSecurityConfigurerAdapter agregamos **@EnableGlobalMethodSecurity**
que por default esta en true.

```java
+ @EnableGlobalMethodSecurity(securedEnabled=true)
= public class ConfigSeguridadSpring extends WebSecurityConfigurerAdapter {
```

C. En nuestro controlador agregamos los permisos

```java
public class ClientesController {
  ...
  //PARA OBTENER LOS DETALLES DEL CLIENTE
+  @Secured({"ROLE_USER", "ROLE_ADMIN"})
=  @GetMapping("/{id}")
=  public ResponseEntity<?> show(@PathVariable Long id) {
=    ClienteEntity cliente = null;
    ...

  //AGREGAR CLIENTE
+  @Secured("ROLE_ADMIN")
=  @PostMapping({ "", "/" })
=  public ResponseEntity<?> save(@Valid @RequestBody ClienteEntity cliente, BindingResult bindingResult) {
    ...
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 133. Configurando Cors en Spring Security

A. Si levantamos angular y vemos el listado de clientes, estos no
tendran su imagen por default, ya que no definimos esa ruta como
acceso publico y nos esta dando un 401. Por lo que agregamos la
siguiente regla.

```java
= public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
=  @Override
=  public void configure(HttpSecurity http) throws Exception {
=    http.authorizeRequests()
##      .antMatchers(HttpMethod.GET, ..., "/img/**").permitAll() //damos acceso a todos al recurso usuarios
    ...
```

B. Si queremos modificar o eliminar nos va a dar un error *"ccess to XMLHttpRequest at '\*' from origin '\*' has been blocked by CORS policy: Response to preflight request doesn't pass access control check: No 'Access-Control-Allow-Origin' header is present on the requested resource."*
que significa un problema con el CORS, el cual esta dando este
error por la proteccion que le dimos con OAuth2.
de Spring Security.
Solucion: [Aqui](https://docs.spring.io/spring-security/site/docs/5.2.0.M4/reference/htmlsingle/#cors) tenemos un metodo
`CorsConfigurationSource corsConfigurationSource()` el cual vamos a vasarnos para implementar nuestra solucion.

C. vamos a ResourceServerConfigurerAdapter y copiamos el metodo mensionad

```java
= public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
=  @Override public void configure(HttpSecurity http) throws Exception { ... }

    @Bean CorsConfigurationSource corsConfigurationSource() {
     //Configuramos permisos
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200"));//dominios permitidos
    configuration.setAllowedMethods(Arrays.asList("GET","POST", "PUT", "DELETE"));//metodos permitidos
    configuration.setAllowCredentials(true);//para aceptar credenciales
    configuration.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization"));

    //registramos la configuracion
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);//registramos esta configuracion para todas las rutas
    return source;
    }
```

D. Lo registramos en nuestro back (en la misma clase) donde le pasamos el
metodo que creamos arriba.

```java
=  @Override
=  public void configure(HttpSecurity http) throws Exception {
=    http...
+      .and().cors().configurationSource(corsConfigurationSource())
```

E. Creamos un filtro para agregar la configuracion a las rutas de spring
Security como el generar token y validarlo. AL cual le pasamos el metodo
corsConfigurationSource que creamos arriba.

```java
@Bean
public FilterRegistrationBean<org.springframework.web.filter.CorsFilter> corsFilter() {
  FilterRegistrationBean<CorsFilter>
  bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
  bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
  return bean;
}
```

F. Nos va a seguir aventando error pero ya no de CORS si no de que no
estamos auth, lo que **faltaria** seria mandar el token desde el front, para
ya no recibir el error.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 16: Autenticación OAuth2 con JWT​: Frontend Angular

#### 136. Creando componente login y formulario

A. Creacmos el compoentne

```shell
ng g c usuarios/login --flat=true
```

B. Creamos ruta en appModules

```shell
const routes: Routes = [
  {path: 'login', component: LoginComponent},
];
```

C. Agregamos el formulario en *login.component.html*

```html
<div class="card border-primary text-center">
    <div class="card-header">{{titulo}}</div>
    <div class="card-body">
        <form method="POST" >
<!--Username-->
            <div class="form-group col-sm-6">
                <input type="text" class="form-control" id="username" placeholder="Nombre de usuario" autofocus autocomplete="false" />
            </div>
<!--password-->
            <div class="form-group col-sm-6">
                <input type="password" class="form-control" id="pass" placeholder="clave de acceso" />
            </div>
<!--Button-->
            <div class="form-group col-sm-6">
                <input type="submit" class="btn btn-large btn-primary btn-block" value="Login" />
            </div>
        </form>
    </div>
</div>
```

**Resultado:** Tenemos el componente la vista y el enlace al login----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 137. Manejo de error no autorizado y redirección al login

Manejaremos los errores 401 y 403, para redirigir a una pagina de login.
Otra forma de manegarlo es con los guard

A. Vamos a *cliente.service.ts* y agregamos
la validacion de si esta auth o no a travez de un
boleano

```typescript
export class ClienteService {
  ...
  addCliente(cliente: Cliente): Observable<Cliente>  {
    return this.httpClient.post<Cliente>(this.endpoint, cliente, {headers: this.httpHeaders}).pipe(
      catchError(e => {
+        if(this.isNotAuth(e)) { return throwError(e); }
     ...
  }) );; }

  getClienteById(idCliente: Number): Observable<Cliente> {
    ...
    return this.httpClient.get<Cliente>(urlGetCliente).pipe(
      catchError(e => {
+        if(this.isNotAuth(e)) { return throwError(e); }
       ...
  }

  subirFoto(id:number, archivo:File): Observable<HttpEvent<{}>> {
    ...
    return this.httpClient.request(req).pipe(
+      catchError(error=> { this.isNotAuth(error); return throwError(error); })
  );}

  getRegiones(): Observable<Region[]> {
    return this.httpClient.get<Region[]>(this.endpoint+"/regiones").pipe(
+      catchError(error=> { this.isNotAuth(error); return throwError(error); })
  );}

+  private isNotAuth(error): boolean {
+    if(error.status==401 || error.status==403) { this.router.navigate(["/login"]); return true; }
+    return false;
+  }
```

**Resultado:** Cada vez que queramos hacer una actividad que no
tenemos permisos se nos redirigira al formulario de login.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 138. Añadiendo el método login() en el componente y nueva clase Usuario

A. Creamos la clase usuario

```shell
ng g class usuarios/usuario
```

B. Le asignamos propiedades

```typescript
export class Usuario {
     id:Number;
     username:String;
     password:String;
     roles:String[]=[];
}
```

C. vamos a LoginComponent a agregar el evento de login

```typescript
export class LoginComponent implements OnInit {
  private usuario:Usuario;
  constructor() { this.usuario = new Usuario(); }
  login():void {
    if(this.usuario.username==null ||  this.usuario.password==null ) {
      Swal.fire("Error de login", "Llena todos los campos del login", "error"); return;
    }
  }
```

D. Enlazamos el modelo y el metodo login() con el formulario

```html
 <form method="POST" >...
  <input [(ngModel)]="usuario.username"...
  <input [(ngModel)]="usuario.password"...
  <input type="submit" (click)="login()"...
```

**Resultado:** Tenemos una funcionalidad minima en el
formulario login. **Faltaria** mandar la peticion para
generar el token, lo que requeriremos de un nuevo service.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 139. Creando la clase de servicio AuthService y su método login()

Crearemos el service para enviar la peticion del login.

A. Creamos el service

```shell
ng g s usuarios/auth
```

B. Implementamos

```typescript
import { Injectable } from '@angular/core'; import { Observable } from 'rxjs'; import { HttpClient, HttpHeaders} from '@angular/common/http'; import { Usuario } from './usuario';
@Injectable({ providedIn: 'root' })
export class AuthService {
 constructor( private http: HttpClient ) { }
 login(usuario:Usuario):Observable<any> {
  const endpoint = "http://localhost:8080/oauth/token";
  const credencialesApp = btoa("clientesangularapp"+":"+"12345");
  const reqHeaders = new HttpHeaders({ "Content-Type":"application/x-www-form-urlencoded", "Authorization": "Basic "+credencialesApp});
  const params = new URLSearchParams();
  params.set("username", usuario.username);
  params.set("password", usuario.password);
  params.set("grant_type","password");
  return this.http.post<any>(endpoint, params.toString(), {headers: reqHeaders });
 }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 140. Implementando la autenticación en el componente login

A. Vamos a *login.component.ts* e importamos el service

```typescript
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) { this.usuario = new Usuario(); }
  login():void {
    console.log(this.usuario);
    if(this.usuario.username==null ||  this.usuario.password==null ) { Swal.fire("Error de login", "Llena todos los campos del login", "error"); return;
+    this.authService.login(this.usuario).subscribe(
      response => {
+    let payload = JSON.parse(atob(response.access_token.split(".")[1]))
        this.router.navigate(["/clientes/page/0"]);
        Swal.fire("Login", "Login realizado de forma exitosa", "success");
  } ); }
```

A.1. Si nos fijamos abtenemos los datos del token que estan en base64,
obteniendo la segunda seccion, pasandola a string y de ahi a un objeto json

```js
let payload = JSON.parse(atob(response.access_token.split(".")[1]))
```

**Resultado:** Si ponemos bien los datos nos retornara el un msj
de exito y nos redirigira a la lista de clientes.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 141. Guardando datos del usuario y token en el sessionStorage

Vamos a Guardar los datos en el sessionStorage para que el usuario pueda identificarse
con cada peticion.

A. Vamos a *login.component.ts* para usar 2 metodos uno para
guardar los datos del usuario y otro el token

```typescript
 login():void {
  console.log(this.usuario);
  if(this.usuario.username==null ||  this.usuario.password==null ) { ... }
  this.authService.login(this.usuario).subscribe(
   response => {
+   this.authService.guardarUsuario(response);
+   this.authService.guardarToken(response);
   ...});}
```

B. Vamos a *auth.service.ts* y agregamos los dos metodos, como las 2 nuevas
variables con sus sets

```typescript
 guardarUsuario(token: string):void {
    let payload = this.obtenerDatosToken(token);
    this._usuario = new Usuario();
    this._usuario.username = payload.user_name;
    this._usuario.id = payload.user_id;
    this._usuario.roles = payload.authorities;
    this._usuario.password = null;
    sessionStorage.setItem("usuario", JSON.stringify(this._usuario));
  }

  guardarToken(token: any):void {
    this._token = token.access_token;
    sessionStorage.setItem("token", this._token);
  }

  obtenerDatosToken(token:any):any {
    if(token != null) { return JSON.parse(atob(token.access_token.split(".")[1])) } else { return null; }
  }

  public get usuario(): Usuario {
    if(this._usuario != null) {
      return this._usuario;
    } else if(this._usuario != null && sessionStorage.getItem("usuario") != null) {
      this._usuario = JSON.parse(sessionStorage.getItem("usuario")) as Usuario;
      return this._usuario;
    } else {
      return new Usuario();
    }
  }
```

**Resultado:** Cuando Hacemos un login que sale bien se guarda el
usuario y el token en el lado del servidor.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 142. Manejando error de credenciales incorrectas en componente login

A. Vamos a *login.component.ts*, y agregamos la excepcion para controlar un error 400
que lo manejaremos como credenciales incorrectas.

```typescript
  login():void {
    console.log(this.usuario);
    if(this.usuario.username==null ||  this.usuario.password==null ) {...}
    this.authService.login(this.usuario).subscribe(
      response => { ... },
    err => {
        if(err.status == 400) { Swal.fire("Login", "La contraseña o usuario son incorrectos", "error"); }
      }
 ); }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 143. Chequear en el componente login si el usuario ya ha iniciado sesión

A. Agregamos el metodo que validara si esta auth o no en *auth.service.ts*

```typescript
public isUserAuth(): boolean{
 let acToken = this.token;
 let userData = this.obtenerDatosToken(acToken);
 if(userData!=null && userData.user_name && userData.user_name.length>0)  {
   return true;
 }
 return false;
}
```

B. Lo usmao en *login.component.ts*

```typescript
ngOnInit() {
  this.userAuth = this.authService.isUserAuth();
  if(this.userAuth) {
    this.router.navigate(["/clientes/page/0"]);
    Swal.fire("Login", "Ya esta auth", "info");
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 144. Añadiendo en el layout el links logout para cerrar la sesión

A. Vamos a *header.component.ts* e inyectamos el auth.service e
implementamos el metodo logout del evento/controlador

```typescript
constructor(private authService: AuthService) { }

logout():void {
  console.log("Cerrar session");
  this.authService.logout();
  Swal.fire("Login", "Cierre de session exitoso", "info");
  this.router.navigate(["/login"]);
}
```

B. Implementamos el metodo de logout del service en *auth.service.ts*

```typescript
public logout() {
 this._token = null;
 this._usuario = null;
 sessionStorage.removeItem("usuario");
 sessionStorage.removeItem("token");
}
```

C. Cambiamos la plantilla *header.component.html*, para mostrar un boton u otro
dependinedo de que este logueado o no

```html
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    ...
<!--Nav var a la derecha-->
<ul class="navbar-nav nav-bar-rigth">
<li *ngIf="!authService.isUserAuth()" >
  <a routerLink="/login" class="btn btn-outline-primary">Login</a>
</li>

<!--Buton de cuando esta logueado-->
<li class="dropdown" *ngIf="authService.isUserAuth()">
  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  {{authService.usuario.username}}
  </a>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
  <button class="dropdown-item" type="submit" (click)="logout()">Salir</button>
</div></li></ul></div>
```

**Resultado** ...

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 145. Enviando token al backend para acceder a los recursos protegidos

A. Vamos a *clente.service.ts* donde modificamos las peticiones
a nuestros recursos privados.
A.1. Agregamos metodo para agregar la cabezera auth.

```typescript
  private addHeaderAuth(): HttpHeaders {
    let token = this.authService.token;
    if(token == null) {
      console.info("No se agrego cabezera Authorization ya que el token es null");
      return this.httpHeaders;
    }
    return this.httpHeaders.append("Authorization", "Bearer " + token);
  }
```

A.2. Modificamos el header que manejabamos por este para cada uno de los metodos
en el que necesitamos la auth.

```typescript
addCliente(cliente: Cliente): Observable<Cliente>  {
    return this.httpClient.post<Cliente>(this.endpoint, cliente, {headers: this.addHeaderAuth()}).pipe(
  ...
}
```

**Nota**: Para subir foto implementamos en el metodo lo mismo que el addHeaderAuth()
ya que si usamos el mismo obj tiene problemas con la peticion.
**Nota2**: El objeto HttpHeaders es inmutable por lo que necesitamos reasignarlo " = " para
tener el cambio.

**Resultado:** Podemos auth y luego hacer las operaciones que requieren auth. pero
**falta** majar cuando el usuario auth no tiene los permisos.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 146. Manejando código de error 403 de Accedo Denegado o Prohibido (Forbidden)

A. Vamos a *cliente.service.ts* y modificamos isNotAuth

```typescript
private isNotAuth(error): boolean {
    if(error.status==401) { ... }
    if(error.status==403) {
      Swal.fire("No autorizado", "No aut...", "warning"); this.router.navigate(["/clientes/page/0"]); return true;
    }
    return false;
  }
```

**Resultado:** Cuando no tiene los permisos sale un msj
indicando la situacion. ![20_manejo_permisos_sin_esconder_botones](./img//20_manejo_permisos_sin_esconder_botones.gif)----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 147. Ocultando botones en las plantillas según el role del usuario

A. Vamos a *auth.service.ts* y agregamos metodo hasRole.

```typescript
public hasRole(rol:string) {
  if(this.usuario == null) { return false; }
  return this.usuario.roles.includes(rol);
}
```

B. Vamos a *clientes.component.ts* e inyectamos el authService

```typescript
constructor(... private authService: AuthService) { }
```

C. Vamos a *clientes.component.html* y usamos el metodo

```html
<button *ngIf="authService.hasRole('ROLE_ADMIN')"...>Crear Cliente</button>

<table...> <thead>
  ...Mail</th>
  <th *ngIf="authService.hasRole('ROLE_USER')">Editar</th>
  <th *ngIf="authService.hasRole('ROLE_ADMIN')">Eliminar</th>
...

```

**Resultado:** Los botones con las funciones que no son permitidas
no son mostradas al usuario. ![21_manejo_permisos_escondiendo_botones](./img//21_manejo_permisos_escondiendo_botones.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 149. Añadiendo seguridad en las rutas con Guard

Asta el momento tenemos que hacemos la peticion al servidor
y este nos indica si es permitida o no la operacion, pero
para mayor agilidad o no molestar al servidor, hacemos esta
validacion del lado de angular, asi antes de enviar la
peticion indicamos que no se puede.

A. Creamos nuestro guar: `ng g g usuarios/guards/auth`

B. Inyectamos el authservice para saver si esta auth o no el
usuario

```typescript
export class AuthGuard implements CanActivate {
  constructor( private authService: AuthService, private route: Router, ){}
  canActivate(route: ActivatedRouteSnapshot,  state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>  {
    if(this.authService.isUserAuth) {
      return true;
    } else {
      Swal.fire("Login", "Requieres autenticarte para esto", "warning");
      this.route.navigate(["/login"]);
    }
    return false;
} }
```

C. Vamos a *app.module.ts* y agregamos el guar a nuestros
componentes en las rutas con **canActivate**

```typescript
const routes: Routes = [
  ...
  {path: 'clientes/form', component: FormClienteComponent, canActivate: [ AuthGuard ]},
  {path: 'clientes/form/:id', component: FormClienteComponent, canActivate: [ AuthGuard ]},
...
```

**Resultado:** Ahora si quieremos acceder a la ruta
del formulario sin estar auth nos mandara a la pantalla
de login mas un msj para indicarnos que pex.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 150. Crear Guard router RoleGuard

hasta ahora solo identificamos si esta o no
auth el usuario pero no identificamos que rol
tiene este usuario. Ya que ahora el usuario normal
puede acceder al formulario de agregar, lo cual no
deberia ya que solo los admin pueden agregar clientes.

A. Generamos nuestro Guar de roles: `ng g g usuarios/guards/role`

A. Inyectamos el authService, es muy parecido al anterior
solo que obtenemos el rol que puede pasar a travez del data
que le enviamos desde el routes.
**NOTA:** Validamos de nuevo si esta auth o no el usuario
ya que en cierta medida se ejecutan los dos guars

```typescript
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(!this.authService.isUserAuth()) {
      this.route.navigate(["/login"]);
      Swal.fire("Login", "Requieres autenticarte para esto", "warning");
      return false;
    }
    let rol = next.data["role"] as string;
    if(this.authService.hasRole(rol)) {
      return true;
    } else {
      Swal.fire("Login", "No tienes permisos para esto", "warning");
      this.route.navigate(["/clientes/page/0"]);
    }
    return false;
  }
```

A. Agregamos en el Routes este nuevo guard

```typescript
const routes: Routes = [
  {path: 'clientes/form', component: FormClienteComponent, canActivate: [ AuthGuard, RoleGuard], data: { role: "ROLE_ADMIN" } },
  {path: 'clientes/form/:id', component: FormClienteComponent, canActivate: [ AuthGuard, RoleGuard], data: { role: "ROLE_USER"} },
];
```

**Resultado:** Cuando nos auth como usuario podemos
acceder al fomulario de modificar, pero no podemos
entrar al formulario de agregar, y cuando es un usuario
admin sin problemas entramos a los dos formularios.

#### 151. Validando fecha de expiración del token en el AuthGuard

Vamos a hacer lo mismo pero ahora con la validacion
dela expiracion del token.

A. Vamos a *role.guard.ts*

```typescript
canActivate(next... {
    if(!this.authService.isUserAuth()) {...}

    if(this.isTokenExpired()) {
      this.route.navigate(["/login"]);
      Swal.fire("Login", "Secion expirada, inicia sesion de nuevo", "warning");
      return false;
    }
}

private isTokenExpired() {
 let token = this.authService.token;
 let infoToken = this.authService.obtenerDatosToken(token);
 let now = new Date().getTime()/1000;
 if(infoToken.exp < now) {
  return true;//la fecha de expiracion es menor a la fecha actual
 }
 return false;
}
```

**Resultado:** En tanto caduque el token el front
se dara cuenta por lo que le pedira iniciar session
para poder hacer operaciones que requieran de este.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 152. HttpInterceptor para añadir el token en las cabeceras HTTP Authorization

En angular tenemos HttpInterceptor ([DOC](https://angular.io/guide/http#intercepting-requests-and-responses))
el cual podemos indicar que se realice una tarea
antes o despues de hacer cualquier peticion.

A. Generamos una clase para nuestro interceptor. y copiamos
en estael codigo de la documentacion.
A.1. Inportamos AuthService para obtener el token

```typescript
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
  ){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = this.authService.token;
    let infoToken = this.authService.obtenerDatosToken(token);

    if(token != null){
      const authReq = req.clone({headers: req.headers.set('Authorization', "Bearer " + token)});
      console.log("TokenInterceptor - token agregado");
      return next.handle(authReq);//cambiamos la antigua req por la nueva que incluye el token
    } else {
      console.log("TokenInterceptor - No se encontro token para enviar");
    }
    return next.handle(req);//regresamos el req sin ningun cambio
  }
}
```

B. Incluimos nuestro interceptor al app.module lo
incluimos dentro de **providers**

```typescript
providers: [
  ...
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
],
```

C. Ya deberia de funcionar el interceptor, ahora quitarmos
la inclucion del token en nuestro*cliente.service.ts*

```typescript
//  private addHeaderAuth(): HttpHeaders { ... } Agregasmo esta operacion en el tokenTnterceptor
- return this.httpClient.put<Cliente>(urlGetCliente, cliente, {headers: this.addHeaderAuth()}).pipe(
+ return this.httpClient.get<Cliente>(urlGetCliente, {headers: this.addHeaderAuth()}).pipe(
```

**Resultado:** Sigue funcionando igual, solo que ahora
tenemos un codigo mas limpio.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 153. HttpInterceptor para códigos HTTP 401 y 403

De la misma forma podemos incluir esta logica en el Interceptor
para tener un codigo mas limpio.
A. Creamos el Intercepto justo como el anterior, pero ahora
usamos un pipe para manejar logica despues de cualquier respuesta del serv.

```typescript
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router, ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      //SE EJECUTA JUSTO DESPUES DE QUE LLEGUE LA RESPUESTA DEL SERVIDOR
      catchError(err => {

        if (err.status == 401) {
          let posExp = err.error.error_description.indexOf("expired"); if (posExp > 0) { Swal.fire("Login", "Tu sesion a expirado, por favor inicia de nuevo", "warning"); this.authService.logout(); } this.router.navigate(["/login"]);
        }

        else if (err.status == 403) {
          Swal.fire("No autorizado", "Esta operacion o recurso solo puede realizarla los administradores", "warning"); this.router.navigate(["/clientes/page/0"]);
        }

        return throwError(err);
      })
    );
  }
}
```

**Resultado:** Podemos quitar logica cliente.service ya que mandamos logica al
interceptor

**Commit:** [Link](https://gitlab.com/MarioM51/clientes-app-back/commit/a87fa8119834c667fd0e70ae8aeb9fe79551624a)

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 17: Sistema de Facturación

#### 155. Demostración de lo que lograremos al finalizar esta sección

Este sistema que tendra varias tablas relacionadas entre si
todo esto realizado desde JPA.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 156. Análisis y Diseño OO con UML Diagrama de Clases del Dominio

![1_menu_bootstrap](./img//22_diagrama_er_facturacion.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 157. Asociaciones: ManyToOne Bidireccional - Un Cliente tiene muchas Facturas

Vamos a crear la relacion cliente<-1-n->factura

A. Creamos la entidad Factura con la relacion a clientes y el prepersist para
asignar la fecha de creacion.

```java
import javax.persistence.*;
@Entity
@Table(name="facturas")
public class FacturaEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id @GeneratedValue(strategy=GenerationType.IDENTITY) private Long id;
  private String descripcion;
  private String observacion;
  @Column(name="create_at") @Temporal(TemporalType.DATE) private Date createAt;

  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="cliente_id")
  private ClienteEntity cliente;

  @PrePersist
  public void prePersist() { this.createAt = new Date(); }

  //Getters/setters
```

B. Asignamos la relacion factura en cliente.

```java
public class ClienteEntity implements Serializable{
  ...
  @OneToMany(fetch=FetchType.LAZY, mappedBy="cliente", cascade=CascadeType.ALL)
  private List<FacturaEntity> facturas;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 158. Asociaciones: OneToMany Unidireccional - Una Factura tiene muchas Lineas

<!-- relacion uno a muchos -->

A. Creamos LineaFacturaEntity, no ponemos la relacion con factura ya que no
tiene sentido obtener la factura atravez del item, ya que el metodo predefinido
sera obtener el itemFactura atravez de la factura.

```java
@Entity
@Table(name="factura_items")
public class LineaFacturaEntity {

  @Id @GeneratedValue(strategy=GenerationType.IDENTITY) private Long id;

  private Integer cantidad;

  public Double getImporte() { return this.cantidad*this.producto.getPrecio(); }
```

B. Creamos en factura la relacion con itemFactura

```java
public class FacturaEntity implements Serializable {
...
  @OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
  @JoinColumn(name="factura_id") //de esta manera se creara la columna en la tabla factura_items
  private List<LineaFacturaEntity> lineaFactura;

  public FacturaEntity() {
    super();
    this.lineaFactura = new ArrayList<LineaFacturaEntity>();
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 159. Asociaciones: ManyToOne Unidireccional - Muchas LineaFactura tiene un Producto

Aqui solo necesitamos el id del producto en linea_factura nadamas

A. Creamos producto

```java
@Entity
@Table(name="productos")
public class ProductoEntity implements Serializable{
  @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  private String nombre;
  private Double precio;
  @Column(name="create_at") @Temporal(TemporalType.DATE) private Date createAt;
```

B. Creamos relacion en itemFactura

```java
public class LineaFacturaEntity implements Serializable {
  ...
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="producto_id")
  private ProductoEntity producto;
```

C. Creamos el getTotal en factura.
**Nota:** Nombramos el metodo de esta forma para que en el json
se incluya este campo sin declararlo.

```java
public class FacturaEntity implements Serializable {
  public Double getTotal() {
    double total = 0;
    for(LineaFacturaEntity itemFacturaTemp:this.lineaFactura){total+=itemFacturaTemp.getImporte();}
    return total;
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 160. Analizando y revisando las tablas y relaciones en MySQL Workbench

A. Agregamos unos datos de prueba en import.sql

```sql
/* Populate tabla productos */
INSERT INTO productos (nombre, precio, create_at) VALUES('Panasonic Pantalla LCD', 259990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Sony Camara digital DSC-W320B', 123490, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Apple iPod shuffle', 1499990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Sony Notebook Z110', 37990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Hewlett Packard Multifuncional F2280', 69990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Bianchi Bicicleta Aro 26', 69990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Mica Comoda 5 Cajones', 299990, NOW());


/* Creamos algunas facturas */
  /* factura 1 */
INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura equipos de oficina', null, 1, NOW());
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES(1, 1, 1);
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES(2, 1, 4);
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES(1, 1, 5);
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES(1, 1, 7);

  /* factura 2 */
INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura Bicicleta', 'Alguna nota importante!', 1, NOW());
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES(3, 2, 6);
```

A. Levantamos el proyecto y deberiamos de ver las tablas
creadas en mysql y los datos----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 161. Listando las facturas del Cliente en Postman

**Problema:** Si consultamos un cliente por ID en nuestro endpoint
vamos a ver que que **truena** y un monton de contenido en
nuestro json, esto sucede ya que en cliente tenemos factura y
en factura tenemos cliente, lo que hace que surja un efecto
loop infinito.
**Solucion:** Vamos a indicar que en factura no serialize el
cliente.

A. Vamos a la clase cliente e ignoramos a cliente de la factura

```java
public class ClienteEntity implements Serializable{
  ...
  @JsonIgnoreProperties("{cliente}")
  private List<FacturaEntity> facturas;
```

B. Vamos a facturaEntity e ignoramos a facturas del campo cliente

```java
public class FacturaEntity implements Serializable {
  @JsonIgnoreProperties("{facturas}")
  private ClienteEntity cliente;
```

**Problema2:** Ahora si mandamos la peticion, va a dar otro error,
pero esto es por los datos que genera spring automaticamente al
configurar un campo con carga perezosa.
**Error:** type definition error: [simple type, class org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor]; nested exception is com.fasterxml.jackson.databind.exc.InvalidDefinitionException:
No serializer found for class org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS).
**Solucion:** Ignoramos los campos "hibernateLazyInitializer" y
"handler" que tengan la rcarga peresosa. Y asi en cada entity que
tengamos esta relacion.

```java
@JsonIgnoreProperties({"cliente", "hibernateLazyInitializer", "handler"})
@OneToMany(fetch=FetchType.LAZY, mappedBy="cliente", cascade=CascadeType.ALL)
private List<FacturaEntity> facturas;
```

**Resultado:** Ahora podemos consultar clientes con sus
facturas y esta facturas con sus items.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 162. Añadiendo las clases TypeScript Factura, ItemFactura y Producto en Angular

A. Generamos nuestra entidad factura y lineaFactura

```bash
ng g class facturas/models/factura
ng g class facturas/models/lineaFactura
ng g class facturas/models/producto
```

B. Relacionamos a cliente con factura

```typescript
export class Cliente {
...
    facturas: Factura[] = [];
}
```

C. Creamos Factura

```typescript
export class Factura {
    id: number;
    descripcion:string;
    observacion: string;
    lineaFactura: Array<LineaFactura>=[];   //rel
    cliente: Cliente;            //rel
    total: number;
    createAt: string;
}
```

D. Creamos LineaFactura

```typescript
export class LineaFactura {
    producto: Producto;
    cantidad:number = 1;
    importe: number;
}
```

D. Creamos Producto

```typescript
export class Producto {
    id: number;
    nombre: string;
    precio: number;
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 163. Listando las facturas en el componente de detalle del Cliente

Vamos a crear la parte visual para que se vean las facturas de los
clientes.

A. Vamos a *clietne-detalle.component.html*

```html
<div *ngIf="cliente.facturas.length==0" class="alert alert-info my-4">
  No hay facturas asignados en el cliente
</div>

<div class="row" *ngIf="cliente.facturas.length>0">
<table class="table table-bordered table-striped my-4">
  <thead>
    <tr><th>Folio</th><th>Descipcion</th><th>Fecha</th><th>Total</th></tr>
  </thead>
  <tbody>
    <tr *ngFor="let factura of cliente.facturas"><td >{{factura.id}}</td>
      ...
      <td >{{factura.total}}</td>
    </tr>
</tbody></table></div>
```

**Resultado:** Podemos ver si tiene las facturas del cliente
si no un mensaje.

**Resultado:** ![22_diagrama_er_facturacion](./img//23_lista_facturas.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 164. Añadiendo CrudRepository para Factura e implementando métodos en el Service

A. Creamos el IFacturaRepository

```java
public interface IFacturaRepository extends CrudRepository<FacturaEntity, Long> { }
```

B. Vamos a IClienteService para agregar el CRUD de factura, lo hacemos
aqui ya que todo lo relacionado con cliente, lo podemos poner en este
Service, ya que estos son como gestores de DAOs/Repositories.

```java
public interface IClienteService {
...
  FacturaEntity findFacturaById(Long facturaId);

  FacturaEntity SaveFactura(FacturaEntity facturaToSave);

  void deleteFacturaById(Long id);

}
```

C. Vamos a clienteServiceImpl e implementamos.

```java
@Service
public class ClienteServiceImpl implements IClienteService {

@Autowired
private IFacturaRepository facturaRepo;

....

@Override
@Transactional(readOnly = true)
public FacturaEntity findFacturaById(Long facturaId) { return facturaRepo.findById(facturaId).orElseGet(null); }

@Override
@Transactional
public FacturaEntity SaveFactura(FacturaEntity facturaToSave) { return facturaRepo.save(facturaToSave); }

@Override
@Transactional
public FacturaEntity deleteFactura(Long id) {
  FacturaEntity toDelete =  this.findFacturaById(id);
  facturaRepo.delete(toDelete);
  return toDelete;
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 165. Creando controlador FacturaRestController con la acción handler show

Obtendremos la factura para ver los detalles de esta.

A. Creamos el FacturaRestController

```java
@RestController
@CrossOrigin(origins = { "http://localhost:4200", "http://127.0.0.1:8887" })
@RequestMapping("/api/facturas")
public class FacturaRestController {

  @Autowired
  private IClienteService clienteService;

  @GetMapping("/{id}")
  public FacturaEntity show(@PathVariable Long id) {
    return clienteService.findFacturaById(id);
  }
}
```

**Resultado:** Tenemos la api que nos da los detalles de la factura

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 166. Creando Componente detalle de la factura y su clase Service relacionada

A. Creamos el compoente: `ng g c facturas/faturaDetalle --flat`

B. Agregamos la ruta en *app.module.ts*, Nota se devio de agregar auto el componente.

```typescript
const routes: Routes = [ ... {path: 'factura/:id', component: FaturaDetalleComponent}, ];
```

C. Agregamos el enlace en *clietne-detalle.component.html*

```html
...
<td >{{factura.total}}</td>
<td ><button class="btn btn-primary btn-sm" type="button"
      [routerLink]="['/facturas', factura.id]">ver
</button></td>
```

D. Creamos el service: `ng g s facturas/services/facturas`

E. Implementamos

```typescript
export class FaturaDetalleComponent implements OnInit {
  private endpoint:string = "http://localhost:8080/api/facturas";
  constructor(private httpClient: HttpClient) { }

  public getFactura(id:number): Observable<Factura> {
    return this.httpClient.get<Factura>(this.endpoint+"/"+id);
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 167. Añadiendo el detalle de la Factura

A. Vamos a *fatura-detalle.component.ts*

```typescript
export class FaturaDetalleComponent implements OnInit {
  private factura:Factura; private titulo:string="Detalles Facturas";
  constructor( private facturaService: FacturasService, private activatedRoute: ActivatedRoute) { }
  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(param => {
      let id = +param.get("id");
      this.facturaService.getFactura(id).subscribe(factura => { this.factura = factura; });
    });
} }
```

B. Vamos a *fatura-detalle.component.html* y mostramos la info

```html
<div class="card bg-ligth" *ngIf="factura">
    <div class="card-header">{{titulo}} </div>
    <div class="card-body">
        <h4 class="card-title">
            <button routerLink="/clientes/page/0" class="btn btn-ligth btn-xs">&laquo; volver</button>
        </h4>
        <ul class="list-group my-2">
            <li class="list-group-item list-group-item-primary">Datos del cliente</li>
            <li class="list-group-item">{{factura.cliente.nombre}}</li>
            <li class="list-group-item">{{factura.cliente.apellido}}</li>
        </ul>
        <ul class="list-group my-2">
            <li class="list-group-item list-group-item-primary">Datos de la factura</li>
            <li class="list-group-item">Folio: {{factura.folio}}</li>
            <li class="list-group-item">Descripcion: {{factura.descripcion}}</li>
            <li class="list-group-item">Fecha: {{factura.createAt}}</li>
        </ul>
    </div>
</div>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 168. Añadiendo las Líneas y Observación en el detalle de la Factura

Ya tenemos estas solo es cosa de mostrarlas en *fatura-detalle.component.html*

```html
<table class="table table-bordered table-striped table-hover my-3">
  <thead>
    <tr><th>Producto</th><th>Precio</th><th>Cantidad</th><th>Total</th></tr>
  </thead>
  <tbody>
    <tr *ngFor="let item of factura.lineaFactura">
      <td>{{item.producto.nombre}}</td><td>{{item.producto.precio}}</td><td>{{item.cantidad}}</td><td>{{item.importe}}</td>
    </tr>
  </tbody>
</table>
<h5 class="float-right"><span>Gran Total: </span>{{factura.total}}</h5>
<div class="card border-info mb-4" style="max-width: 250px;">
  <div class="card card-header">Observaciones</div>
  <div class="card card-body">
    <p *ngIf="factura.observacion">{{factura.observacion}}</p>
    <p *ngIf="!factura.observacion">Sin observacion</p>
  </div>
</div>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 169. Eliminando la factura y sus líneas (Backend)

```java
public class FacturaRestController {
@DeleteMapping("/{id}")
@ResponseStatus(HttpStatus.ACCEPTED)
public FacturaEntity delete(@PathVariable Long id) {
  return clienteService.deleteFactura(id);
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 170. Eliminando la factura y sus líneas (Frontend)

A. Vamos a *facturas.service.ts* y agregamos el consumo de eliminar

```typescript
export class FacturasService {
...
  public deleteById(id:number): Observable<Factura> {
    return this.httpClient.delete<Factura>(this.endpoint+"/"+id);
  }
```

B. Creamos el enlace en *clietne-detalle.component.html*

```html
<td>
  <button class="btn btn-danger btn-sm" type="button" (click)="deleteFactura()">Eliminar</button>
</td>
```

C. Implementamos el delete en el controlador

```typescript
private deleteFactura(factura: Factura) {
Swal.fire({
  title: 'Are you sure?',
  text: "Delete "+ factura.descripcion +"?, You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
  this.facturaService.deleteById(factura.id).subscribe(
    facturaEliminado => {
    this.cliente.facturas = this.cliente.facturas.filter(f => f !== factura)
    Swal.fire(
      'Deleted!',
      'Client '+facturaEliminado.descripcion+' has been deleted.',
      'success'
    )
    }
  );
  }
})
```

**Resultado:** Podemos eliminar las facturas y si nos fijamos
en los logs de srping podremos ver que se eliminan las
lineasFacturas que perteneciana esta.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 171. Creando el componente formulario de la factura

X. Generar componente para facturas: `ng g c facturas`

A. Creamos la ruta con el componente ya creado en *app.module.ts*, el cual
usamos el idCliente para saber a que cliente crearle la factura

```typescript
const routes: Routes = [
  {path: 'facturas/form/:clienteId', component: FacturasComponent},
```

B. en *facturas.component.ts* agregamos que se obtenga el cliente
cuando se inicie el componente.

```typescript
export class FacturasComponent implements OnInit {
  private titulo:string = "Nueva factura";
  private factura: Factura = new Factura();

  constructor(
    private clienteService: ClienteService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(
      params => {
        let clienteId = +params.get("clienteId");
        this.clienteService.getClienteById(clienteId).subscribe(
          cliente => {
            this.factura.cliente = cliente;
          }
        );
      }
    );
```

C. Agregamos enlace a este componente.

```html
<button class="btn btn-rounded btn-success" type="button" [routerLink]="['/facturas/form/',cliente.id]">
  Crear Factura
</button>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 172. Añadiendo campos al formulario factura

Se agrego cod para el formulario lo pego mas abajo para que este completo.

**Resultado:** Ya tenemos parte del formulario, ahora falta agregar
los productos a la factura.

#### 173. Escribiendo código para el Autocomplete Autocompletado usando Angular Material

Usaremos Angular Material para crearlo.

A. Vamos material.angular.io y podremos ver como instalarlo

```bash
ng add @angular/material
```

B. En nuestros estilos **styles.css** principales al inicio importamos estilos de material

```css
@import "../node_modules/@angular/material/prebuilt-themes/deeppurple-amber.css";
```

C. Importamos el autocomplete en el *app.module* los siguientes componentes, ya que
si nos fijamos en el codigo que vamos a copiar para el autocomplete
tenemos todos estas etiquetas.

```typescript
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

imports: [
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,...
  ],
```

D. En la documentacion de [auto-completado](https://material.angular.io/components/autocomplete/overview#adding-a-custom-filter)
podemos copiar el ejemplo de custom filter.

E. Copiamos codigo html y TS del primer ejemplo y lo pegamos
en *facturas.component.html*

```html
<!-- AUTOCOMPLETGADO DE PRODUCTOS-->
<div class="form-group row" *ngIf="factura.cliente">
<div class="col-sm-6">
<mat-form-field class="example-full-width">
  <input type="text" placeholder="Buscar productos" aria-label="Number" matInput [formControl]="myControl" [matAutocomplete]="auto">
  <mat-autocomplete autoActiveFirstOption #auto="matAutocomplete">
    <mat-option *ngFor="let option of productosEncontrados | async" [value]="option">
    {{option}}
    </mat-option>
  </mat-autocomplete>
</mat-form-field>
</div></div>
```

F. y en *facturas.component.ts* respectivamente.

```typescript
export class FacturasComponent implements OnInit {
  private factura: Factura = new Factura();
  private myControl = new FormControl();
  private productos: string[] = ['One', 'Two', 'Three'];
  private productosEncontrados: Observable<string[]>;

  constructor( private clienteService: ClienteService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(
      params => {
        let clienteId = +params.get("clienteId");
        this.clienteService.getClienteById(clienteId).subscribe(
          cliente => {
            this.factura.cliente = cliente;
    });});

    this.productosEncontrados = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }//end-oninit

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.productos.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
```

**Resultado:** Podemos buscar dentro del arreglo a medida que
escribimos, dejando los resultados que hagan match.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 174. Consulta JPA para buscar productos en el Autocomplete

A. Creamos el DAO de producto (ambos hacen lo mismo)

```java
public interface IProductoRepository extends CrudRepository<ProductoEntity, Long> {

  //@Query("select p from ProductoEntity p where p.nombre like %?1%")
  //List<ProductoEntity> findbyName(String name);

  List<ProductoEntity> findbyNameContainingIgnoreCase(String name);
```

B. Aplicamos el metodo dentro de clienteService

```java
@Service
public class ClienteServiceImpl implements IClienteService {

  @Autowired
  private IProductoRepository productoRepo;

  @Override
  @Transactional(readOnly = true)
  public List<ProductoEntity> findProductoByName(String name) {
    return productoRepo.findbyNameContainingIgnoreCase(name);
  }
```

C. implementamos controlador para buscar producto

```java
public class FacturaRestController {
  @GetMapping("productos/{nombre}")
  @ResponseStatus(HttpStatus.OK)
  public List<ProductoEntity> filterProductosByName(@PathVariable String name) {
    return clienteService.findProductoByName(name);
  }
```

D. Probamos en postman

```request
URL: GET - {{base}}/api/facturas/productos/comoda
Headers: Content-Type:application/json, Authorization: {token}
```

**Resultado:** Desde postman podmos obtener una lista de productos
de acuerdo a x match haga segun le enviemos.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 175. Modificando el autocomplete para que filtre los productos desde el Backend

A. Creamos el service para obtener los productos filtrados del back en *facturas.service.ts*

```typescript
public filterproductosByName(nombreproducto: string):Observable<Producto[]>  {
  return this.httpClient.get<Producto[]>(this.endpoint+"/productos/"+nombreproducto);
}
```

B. Aplicamos el metodo en *facturas.component.ts* y modificamos
el metodo de filter para que resiva los objetos y no el string
como estaba antes.

```typescript
export class FacturasComponent implements OnInit {

  ngOnInit() {
  ...
    this.productosEncontrados = this.myControl.valueChanges.pipe(
      map(value => typeof value === 'string' ? value : value.nombre),
      flatMap(value => value ? this._filter(value) : [])
    );
  }//end-oninit

  private _filter(value: string): Observable<Producto[]> {
    const filterValue = value.toLowerCase();
    return this.facturaService.filterproductosByName(filterValue);
  }
```

**Resultado:** Cada vez que escribimos/cambiamos el valor
se hace una peticion al back con los nombres filtrados
y se muestran las distintas opciones.
**Falta:** Cuando seleccionamos un item de las opciones
se muestra *[object Object]*. **Solucion:** Debemos de
añadir un metodo para pasar lo seleccionado de un objeto
a un string.

A. Vamos *facturas.component.html* en `<mat-autocomplete>` y agregamos
la propiedad displayWith con un metodo

```html
<mat-autocomplete #auto="matAutocomplete" [displayWith]="mostrarNombre" >
```

B. Creamos el metodo

```typescript
private mostrarNombre(producto?: Producto):string | undefined {
  return producto ? producto.nombre : undefined;
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 176. Creando las líneas de la Factura

Vamos a agregar un metodo para que agrege el producto a la factura
en tanto este sea seleccionado.

A. Creamos la opcion *facturas.component.html* y agregamos el evento
optionSelected

```html
<mat-autocomplete ... (optionSelected)="agregarProducto($event)" >
```

B. Creamos el metodo en *facturas.component.ts*

```typescript
private agregarProducto(event: MatAutocompleteSelectedEvent):void {
  let producto = event.option.value as Producto;

  let nuevoItem = new LineaFactura();
  nuevoItem.producto = producto;
  this.factura.lineaFactura.push(nuevoItem);

  this.myControl.setValue('');//para que no tenga que borrarlo el usuario
  event.option.focus();
  event.option.deselect();
}
```

C. Creamos una tabla para mostrar los diferentes items

```html
<table class="table table-bordered table-striped table-hover table-sm">
<thead>
  <tr><th>Nombre</th><th>Precio</th><th>Cantidad</th><th>Importe</th></tr>
</thead>
<tbody>
  <tr *ngFor="let item of factura.lineaFactura">
    <td>{{item.producto.nombre}}</td><td>{{item.producto.precio}}</td><td>{{item.cantidad}}</td><td>{{item.calcularImporte()}}</td>
  </tr>
</tbody>
</table>
```

**Resuldato:** Podemos buscar y agregar un item, por ahora
solo se hace la busqueda en el back y se agrega solo en
el front es decir no se guarda en la BD el item.

![24_agregar_producto_a_factura.gif](./img//24_agregar_producto_a_factura.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 177. Actualizando la cantidad de un item de la factura

A. En la parte que tenemos el input agregamos un input

```html
<td><input type="number" value="{{item.cantidad}}" class="form-control col-sm-4" (change)="actualizarCantidad(item.producto.id, $event)" /> </td>
```

B. Agregamos el metodo en el controlador *facturas.component.ts*

```typescript
private actualizarCantidad(idProducto, event) {
  let cantidad:number = event.target.value as number;
  this.factura.lineaFactura = this.factura.lineaFactura.map(
    (item:LineaFactura) => {
      if(idProducto === item.producto.id) { item.cantidad = cantidad; }
      return item;
});}
```

**Resultado:** Podemos cambiar la cantidad de productos
que tiene una linea factura, y se actualizara en el
modelo del front.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 178. Incrementando la cantidad si el producto existe en el detalle (Evitando que se duplique el producto)

A. en *facturas.component.ts* agregamos los metodos

```typescript
private exiteItem(id:number): boolean {
 let existe:boolean = false;
 this.factura.lineaFactura.forEach((linea: LineaFactura) => {
   if(id === linea.producto.id) { existe = true; }
 });
 return existe;
}

private incrementaCantidad(idProducto:number): void {
 this.factura.lineaFactura = this.factura.lineaFactura.map(
   (item:LineaFactura) => {
    if(idProducto === item.producto.id) { item.cantidad++; } return item;
   }
 );
}
```

B. Modificamos el agregar producto

```typescript
=  private agregarProducto(event: MatAutocompleteSelectedEvent):void {
=    let producto = event.option.value as Producto;
+    if(this.exiteItem(producto.id)) {
+      this.incrementaCantidad(producto.id);
+    } else {
=      let nuevoItem = new LineaFactura();
=      nuevoItem.producto = producto;
=      this.factura.lineaFactura.push(nuevoItem);
=    }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 179. Eliminar linea de la factura

A. Agregamos boton de eliminar en *facturas.component.html*

```html
<td><button class="btn btn-danger btn-sm" type="button" (click)="eliminnarLineaFactura(item.producto.id)">X</button></td>
```

B. Agregamos el metodo de eliminar

```typescript
 private eliminnarLineaFactura(id:number) {
    this.factura.lineaFactura = this.factura.lineaFactura.filter(
      (lineaFactura:LineaFactura) => { id !== lineaFactura.producto.id; }
    );
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 180. Calculando el Gran Total de la Factura

A. Vamos al modelo *factura.ts*

```typescript
calcularGranTotal(): number {
  this.total = 0;
  this.lineaFactura.forEach((item:LineaFactura)=> {
    this.total += item.calcularImporte();
  });
  return this.total;
}
```

B. Usamos el metodo en la vista *facturas.component.html*, abajo de la ultima tabla.

```typescript
<h5 class="float-right">Gran Total:{{factura.calcularGranTotal()}}</h5>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 181. Implementando el crear factura

A. Vamos a crear el controller de lado del back

```java
@RequestMapping("/api/facturas")
public class FacturaRestController {
  @PostMapping("")
  @ResponseStatus(@RequestBody HttpStatus.CREATED)
  public FacturaEntity create(FacturaEntity factura) {
    return clienteService.SaveFactura(factura);
  }
```

**Error:** Al enviar la petcion y el back no
capturaba ninguna propiedad todas estaban en null
**Causa:** Se me olvido pener el @RequestBody U+1F605 U+1F64A se lo devolvi
y ya funciono bien.

B. Vamos a *facturas.service.ts* y creamos el consumo de este.

```typescript
public create(factura:Factura): Observable<Factura> {
  return this.httpClient.post<Factura>(this.endpoint, factura);
}
```

C. Vamos a *facturas.component.html* para agregar el evento

```html
<input type="submit" value="Crear Factura" (click)="crearFactura()" class="btn btn-secondary" />
```

D. Agregamos el controlador del front en *facturas.component.ts*

```ts
private crearFactura():void {
  this.facturaService.create(this.factura).subscribe(
    (facturaCreada:Factura) => {
      Swal.fire("Factura", "Factura "+ facturaCreada.descripcion +"creada con exito", "success");
});}
```

Resultado: ![25_Creacion_de_factura.gif](./img//25_Creacion_de_factura.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 184. Solucionando un problema de recursión al editar el cliente

Si queremos editar un cliente no podremos ya que nos da el error de abajo
y tenemos 2 posibles soluciones una en el back(no settear factura al editar el cliente)
y otra en el front(mandar la factura en null), se recomienda tener ambas.

"status":400,
"message":"JSON parse error:  com.fasterxml.jackson.databind.exc.MismatchedInputException: No _valueDeserializer assigned\n at [Source: (PushbackInputStream); line: 1, column: 183]
(through reference chain: com.mariocorps.clientesappback.entity.ClienteEntity[\"facturas\"]->java.util.ArrayList[0]->com.mariocorps.clientesappback.entity.FacturaEntity[\"id\"])\r\
tat com.fasterxml.jackson.databind.exc.MismatchedInputException.from(MismatchedInputException.java:63)

**Causa:** Por la recursividad en factura lo que hace que quiera
settear los valores a null.
**Solucion BACK:** Le indicaremos que ignore tambie los setter tanto en
el entity factura y cliente.

```java
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, allowSetters=true )
```

**SOLUCION FRONT** En el front en *form.cliente.component.ts* ponemos el valor de facturas
a null cuando querramos modificar un cliente, ya que no debemos de modificar
las facturas cuando modificamos los clientes.

```typescritp
=  update(): void {
+    this.cliente.facturas = null;
=    this.clienteService.update(this.cliente).subscribe(
=  ...
```

Ya de deriamos de poder hacer la modificacion del cliente.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 18: Deployment: despliegue en servidores de producción

#### 186. Modificando y preparando nuestros proyectos (backend y fronend) para producción

Tenemos varios formas para hacer deploy en ambas partes, en front solo necesitamos
un servidor http ya sea con node o apache server o etc. Del lado del Back podremos
ya sea con el servidor envevido como viene por defecto spring boot, o convertirlo a un .war
para publicarlo en un Jboss&Tomcat etc.
En este caso vamos a hacer deploy para el lado del front un servidor node, y para el
lado del back el servidor envevido.

A. Vamos a *ResourceServerConfig.java*, *ClientesController.java* y *FacturaRestController.java* que tenemos
los CORS configurados, y agregamos un \* para que acepte mientras cualquier dominio, solo mientras
tenemos un dominio ya definido al cual se lo cambiaremos despues para no tener problemas de  seguridad.

```java
* configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200", "*"));
* @CrossOrigin(origins = { "http://localhost:4200", "http://127.0.0.1:8887", "*" })
```

B. Vamos a *import.sql* -> click derecho -> propiedades -> Text file encode,
se lo dejamos como "ISO-8859-1"

C. En el FRONT vamos a cambiar en publico las siguientes propiedades ya que dan problemas
al querer acceder desde la plantilla a estas.

  1. clientes.component.ts  modalService y authService a publico
  2. clietne-detalle.component.ts modalService a plublico
  3. form.cliente.component.ts, cliente

D. Tener los el wraper de maven en nuestro proyecto ".mvn/", "mvnw" y "mvnw.cmd" de no tenerlos
podemos copiarlos de otro proyecto, ya que estos nos facilitaran a tener maven en el servidor
de manera sencilla.

E. Vamos a nuestro proyecto back y ejecutamos. Lo que hara es descargar las dependencias e
incluso el maven de forma envevida, todo lo necesario para construir nuestro proyecto, el
cual debe de terminar con un **BUILD SUCCESS**

```bash
./mvnw clean package
```

F. Levantamos el proyecto con el siguiente comando, lo cual seria lo mismo
en cual quier servidor, los cuales cuenten con las dependencias que en este
caso serian java y mysql.
<!-- Deploy .jar, ejecutar .jar, ejecutar jar-->
```bash
java -jar .\target\clientesappback-0.0.1.jar
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 188. Transpilando nuestra aplicación angular y preparandonos para el despliegue en Apache

En la documentacion `https://angular.io/guide/deployment` podremos
ver los detalles del deploy, pero en resumen

A. Vamos a la carpeta del front y corremos, el cual creara una carpeta /dist el
cual contendra los archivos que subiremos al servidor.
<!-- compilar angular compilar, construir angular, compilar proyecto angular compilar, construir proyecto angular,
crear version de produccion, crear aplicacion de producion, compilar para produccion
 -->

```bash
ng build --prod
```

B. Al terminar dentro de la carpeta dist creamos el archivo **.htaccess** con
la siguietne configuracion, que vasicamente dice que cualquier peticion
se ira a index.html por ser una SPA.

```r
RewriteEngine On
#If an existing asset or directory is requested go to it as it is
RewriteCond %{REQUEST_FILENAME} -f [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^ - [L]

#If the requested resource doesn't exist, use index.html
RewriteRule ^ /clientes-app/index.html
```

C. En nuestro index.html cambiamos

```html
<base href="/clientes-app/">
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 189. Realizando el despliegue del frontend Angular en servidor Apache 24 (httpd)

A. Descargamos apache de httpd.apache.org -> binaries -> win32 -> Apache Lounge -> httpd-2.4.41-win64-VS16.zip

B. descomprimimos/vamos a C:\Apache24\conf\httpd.conf, y podemos ver que por defecto en
windows esta la ruta en en "Define SRVROOT "c:/Apache24"".

B.1. Habilitamos: LoadModule rewrite_module modules/mod_rewrite.so

B.2. Cambiamos: Vamos a la configuracion del \<Directory "${SRVROOT}/htdocs"\> y cambiamos
"AllowOverride None" por "AllowOverride All"

A. copiamos el contenido de /dist a C:\Apache24\htdocs\clientes-app\...

B. Levantamos el servidor ejecutando bin/httpd.exe y ya deberiamos de poder ver
nuestra aplicacion en. `http://localhost/clientes-app/`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 190. Realizando el despliegue de nuestra app angular con NodeJS y Express

<!-- levantar con node, hacer deploy con node, levantar servidor con node, levantar servidor http con node, crear servidor con node, levantar servidor node-->
A. Nuestra carpeta dist metemos todo en una carpeta,
E:\cursos\0activ\web\angular-spring\clientesapp\front\dist\myApp

B. A la altura de dist/ ejecutamos y dejamos todo en defecto, lo que hara es crear
el package.json el cual manejara nuestras dependencias

```bash
npm init
## entry point: (index.js) server.j
```

C. Instalamos la dependencia de nuestro servidor Express

```bash
npm install express --save -verbose
```

D. Agregamos archivo *server.js* dentro de dist

```js
let express = require(`express`);
let path = require('path');
let port = 8089;
//obtenemos el contexto del servidor
let app = express();
//nombramos la aplicacipn
app.use(express.static('clientes-app'));
//indicamos que cualquier peticion la mande al index
app.get('*', (request, response, next) => {
  response.sendFile(path.resolve(`./public/index.html`));
});
//levantamos servidor
app.listen(port, ()=>{ console.log(`Servidor inicio en el puerto: `+port); })
```

D.1. Me dio problemas el de arriba en ubuntu server, alñ parecer cada vez
que pedia un archivo script css etc lo sustituia por el contenido del
index.html lo que hacia que chafiara ya que en lugar de cargar un js cargaba
el mismo html del index.
Nota: necesita instalr `npm install --save compressio

```js
"use strict";
const express = require("express");
const compression = require("compression");

const _port = 8089;
const _app_folder = './public/';

const app = express();
app.use(compression());

// ---- SERVE STATIC FILES ---- //
app.get('*.*', express.static(_app_folder, {maxAge: '1y'}));

// ---- SERVE APLICATION PATHS ---- //
app.all('*', function (req, res) {
    res.status(200).sendFile(`/`, {root: _app_folder});
});

// ---- START UP THE NODE SERVER  ----
app.listen(_port, function () {
    console.log("Node Express server for " + app.name + " listening on http://localhost:" + _port);
})
```

E. Ejecutamos nuestro servidor con: `bashnode server.js`

**Cuidado:** En este caso el index debe de estar asi el base `<base href="/">`

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 19: Deployment: despliegue en servidores en la nube Heroku y Google Firebase Hosting

#### 192. Creando app en Heroku | 193. Deploy Backend Spring en Heroku

A. Vamos a nuestro pom.xml
A.1. nos aseguramos que este la v de java 1.8
A.2. Vamos al .properties y agregamos `server.port=${PORT:8080}`
A.3. Al *import.sql* lo retornamos el Encode a UTF-8

A. Vamos a www.heroku.com y nos registramos.
B. Creamos una nueva app y ponemos el modo de deploymet como Heroku-CLI.
C. Registramos una Tarjeta para poder instalar MySQL en el serv.
D. Seleccionamos nuestra app. Seguimos las instruncciones.
  3.1. instalamos el CLI
  3.2. Vamos a la carpeta de nuestro back y ejecutamos `heroku login`
  3.2. Creamos un repo: `git init`
  3.3. Asociamos el repo: `heroku git:remote -a clientes-app-back`
  3.4. Instalamos el plugin de java: `heroku plugins:install java`
  3.5. Agregamos el addons de mysql `heroku addons:create jawsdb` el crea una variable JAWSDB_BLUE_URL que contiene el enlace de coneccion
  3.5. Vemos la var para agregarla a nuestro proyecto `heroku config:get JAWSDB_BLUE_UR

```yml
JAWSDB_BLUE_URL
  ORDEN: mysql://USER:PASS@SERVER:PORT/DB_NAME

ORIGINAL
  spring.datasource.url=jdbc:mysql://localhost/bd_app_clientes?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
  spring.datasource.username=root
  spring.datasource.password=mario

CAMBIADO
  spring.datasource.url=jdbc:mysql://SERVER:PORT/DB_NAME
  spring.datasource.username=USER
  spring.datasource.password=PAS
```

C.6. Nos aseguramos que el dialecto este en el 57
C.7. Generamos de nuevo .jar con los nuevos cambios: `./mvnw clean package`
C.1. Deployamos el .jsr generado: `heroku jar:deploy ./target/clientesappback-0.0.1.jar`
A. Al final nos dara la url donde esta desplegada nuestra app en este caso `https://clientes-app-back.herokuapp.com/`

**Nota:** Podemos ver el log que se genera con: `heroku logs --tail`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 194. Deploy Frontend Angular en Firebase Hosting

Necesitaremos agregar cambios como la url de produccion.

A. Creamo la carpeta y archivo ./src/app/config/config.ts

```typescript
export const URL_BACK:string = "https://clientes-app-back.herokuapp.com";
```

A. Vamos a clientes.service y todos los archivos donde usemos
la url

```typescript
import { URL_BACK } from '../config/config';
...
const endpoint = URL_BACK+"/oauth/token";
```

C. Construimos: `ng build --prod`

D. dentro de dist creamos la carpeta public y metemos todo lo
generado dentro de esta carpeta.

E. Vamos a firebase.google.com, creamos cuenta y proyecto, e instalamos
el cli: `npm install -g firebase-tools`

F. Iniciamos sesion: `firebase login`

G. Iniciamos proyecto: `firebase init` y seguimos instrucciones

H. Desplegamos: `firebase deploy`

**Resultado:** Ya debemos de tener desplegado nuestros estaticos
en firebase en la url que indico el deploy en mi caso
`https://front-app-10565.firebaseapp.com`

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 20: Sistema de Chat: en tiempo real con WebSocket

#### 197. Introducción a la tecnología WebSocket

**Problematica:** Si queremos notificar a un usuario los
cambios de otros usuarios en tiempo real, ej. otro usuario
crea un cliente otro usuario no lo vera asta que este
recarge.
**Posible solucion** es usar un X intervalo de tiempo
en el cual cada vez que se cumpla realizar una peticion,
pero el problema es que el 70% o mas de estas seran vacias
lo cual es un desperdicio de recursos de ambos lados, solo
seria viable para aplicaciones pequeñas.
**Solucion:** El uso de una conexion bi-direccional que
talto el cliente pueda enviar una peticion al usuario
como el servidor tenga registrado el suario para enviarle
enfirmacion para notificarle de un cambio.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 198. ¿Qué es el Protocolo Stomp y cómo se relaciona con WebSocket?_

Este STOMP (Protocolo de Mensajeria Orientado a Texto Simple) es el protocolo
que utiliza Spring para los WebSockets y con el cual vamos  a crear un chat.
Este defina ya una abstraccion con metodos que por detras maneja los Sockets
los cuales son.
A. Connect
B. Subscribe
C. Unsuscribe
D. Send o Publish
E. Disconect

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

##### Cliente

**STOMPJS:** Esta es la libraria que usaremos de lado de angular el cual
basicamente usaremos.
A. Para conectarse y desconectarse: `.onConnect = frame => { }` y
`.onDisconnect = frame => { }` los cuales seran como empezar la coneccion.

B.  Para Subscribirse:  `.subscribe("chat/esta-escribiendo" e => { ... })`
en este nos servira para resibir eventos de determinada naturaleza, por ejemplo para
saber si algun usuario esta escribiendo.

C. Enviar mensaje: para esto usamos el: `..publish({ destination: "app/mensaje", body: JSON.stringify(msj) });`

##### Servidor

**Escuchar Eventos:** Estos los manejamos muy parecido a los metodos del controlador

```java
@MenssageMapping(`/app/mensaje`)
public void recibirmensaje(Msj msj) { ...
```

**Notificar o emitir evento:**

```java
@MenssageMapping(`/app/mensaje`)
@SentTo(`/chat/enviar-mensaje`) //nombre al cual se susbribe el cliente
piblic Msj recibirMensaje(Msj msj) {
  ...
  return msj;
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 200. Creando el proyecto backend y configurando el servidor WebSocket (el Broker)

A. Vamos a crear un proyecto desde cero Eclipse -> new -> Other -> Spring Boot
B. Le agregamos las siguientes 4 dependencias Spring Boot DevTools WebSockets
Spring Data MongoDB Spring Web Starter
C. Por el momento comentamos a MongoDB y a la altura de nuestra clase principal
creamos una clase de configuracion *WebSocketConfig* para agregar la ruta a la
cual se conectara el cliente, tambien agregamos el CORS-origins

```java
package com.marioCorps.frontchat;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@org.springframework.context.annotation.Configuration
@org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry
      .addEndpoint("/chat-websocket")  //Url a la que se conectara el usr
      .setAllowedOrigins("http://localhost:4200") //CORS
      // indicamos que usaremos esta libreria, en caso contrario deberiamos usar la lib nativa de HTML5
      // pero este es mas seguro ya que el otro solo soporta el protocolo ws
      .withSockJS();
  }

  @Override
  public void configureMessageBroker(MessageBrokerRegistry registry) {
    //registramos el prefijo el cual detectara a traves de este los demas canales
    //ejemplo "/chat/msj" por el prefijo de este lo cataloga
    registry.enableSimpleBroker("/chat/");

    //Indicamos el prefijo a las rutas a las que se registrara el usuario ej.
    //"app/mensajes" detecta el prefijo
    registry.setApplicationDestinationPrefixes("/app");
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 201. Creando el controlador ChatController y el Mensaje

A. Creamos el modelo para regresar el cuerpo del mensaje

```java
package com.marioCorps.frontchat.document;
public class Mensaje implements java.io.Serializable {
  private static final long serialVersionUID = 1L;
  private String texto;
  private Long fecha;
  //getter/setters

```

A. Creamos el controlador del chat dentro de su paquete

```java
@org.springframework.stereotype.Controller
public class ChatController {
  @org.springframework.messaging.handler.annotation.MessageMapping("/mensaje")
  @org.springframework.messaging.handler.annotation.SendTo("/chat/mensaje")
  public Mensaje recibeMensaje(Mensaje msj) {
    msj.setFecha(new Date().getTime());
    msj.setTexto("msj Recibido: " + msj);
    return msj;
  }

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 202. Creando el proyecto frontend Angular

A. Cremos proyecto: `ng new chat-front`
B. Creamos componente: `ng g c chat`
C. Agregamos ruta en *app-routing.module.ts

```typescript
const routes: Routes = [
 {path:'', redirectTo: `chat`, pathMatch: `full` },//ruta por defecto
 {path:'chat', component: ChatComponent }
]
```

A. Registramos el compoentne de formulario en *app.module.ts

```typescript
import { FormsModule } from '@angular/forms';
imports: [
 FormsModule,
]
```

A. instalar bootstrap: `npm install bootstrap jquery popper.js --save`

B. En *angular.json* agregamos los archivos de bootstrap, el orden es important

```json
"styles": [
  "./node_modules/bootstrap/dist/css/bootstrap.css"
],
"scripts": [
  "./node_modules/jquery/dist/jquery.slim.min.js",
  "./node_modules/popper.js/dist/umd/popper.min.js",
  "./node_modules/bootstrap/dist/js/bootstrap.min.js"

```

A. levantamos: `ng serve`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 203. Instalando librerías para el cliente WebSocket en Angular (sockjs y stompjs)

A. Podemos ver los paquete en www.npmjs.com, isntalamos

```bash
npm install @stomp/stompjs --save
npm i sockjs-client --save
npm i @types/sockjs-client --save-dev
```

A. Vamos a *chat.component.ts* importamos y conectamos
al broker ( lo que pusimos en WebSocketConfig ... registerStompEndpoints ... addEndpoint)

```typescript
import { Component, OnInit } from '@angular/core';
import { Client } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { PATH_BASE_CHAT } from '../consts';

@Component({ selector: 'app-chat', templateUrl: './chat.component.html', styleUrls: ['./chat.component.css']})
export class ChatComponent implements OnInit {
  private client: Client
  constructor() {  }
  ngOnInit() {
    this.client = new Client();
    this.client.webSocketFactory = ()=> {
      return new SockJS(PATH_BASE_CHAT + '/chat-websocket');
    }
    //asignamos el listener de si se conecta
    this.client.onConnect = (frame) => {
      console.log('Coneccion: ' + this.client.connected);
      console.log("Info broker", frame);
    }
    //iniciamos coneccion
    this.client.activate();
  }
}
```

**Error**: global is not defined
    Object../node_modules/sockjs-client/lib/utils/random.js
    Object../node_modules/sockjs-client/lib/utils/event.js
    Object../node_modules/sockjs-client/lib/transport/websocket.js
    Object../node_modules/sockjs-client/lib/transport-list.js
**Descripcion**: Al levantar el proyecto saca este error
**Causa**: No esta configurado la variable global
**Solucion**: En *polyfills.ts* al final agregamos: `(window as any).global = window;`

A. Al levantar podremos ver los los que pusimos indicando true
que nos dice que conecto bien.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 204. Maquetando y escribiendo el contenido HTML para el chat

A. E chat *chat.component.html

```html
<div class="card">
    <div class="card-header">
        <ul class="nav nav-pills card-header-pills">
            <li class="nav-item">
                <button class="btn btn-success" type="button">Conectar</button>
                <button class="btn btn-warning" type="button">Desconenctar</button>
            </li>
        </ul>
    </div>
    <div class="card-body" style="overflow-y: scroll; height: 360px;">
    </div>
    <div class="card-footer">
        <form class="form-inline">
            <div class="form-group"><input type="text" name ="texto" class="form-control" placeholder="Escribe un mensaje..." ></div>
            <button type="submit" class="btn btn-primary">Enviar</button>
</form></div></div>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 205. Implementando el conectar y desconectar del servidor WebSocket

En *chat.component.ts* agregamos los metodos para conectar y desconectar
al igual que el listener de click.

```typescript
public btnConectar() {
  this.client.activate();
}

public btnDesconectar() {
  this.client.deactivate();
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 206. Implementando el enviar y recibir mensajes del Chat

A. Creamos el modelo mensaje: `ng g class chat/models/mensaj

```typescript
export class Mensaje {
    texto:string = '';
    fecha:Date;
```

B. Nos suscribimos para escuchar si otro usuario envia msj, y definimos
que hacer cuando llege este, y lo guardamos en una propiedad de la clase
los mensajes

```typescript
this.client.onConnect = (frame) => {
  this.conectado = this.client.connected;
  console.log('Coneccion: ' + this.conectado);

  //para suscribirnos y escuchar si otro usuario envia mensaje
  this.client.subscribe('/chat/mensaje', (event)=>{
  let mensajeRecibido:Mensaje = JSON.parse(event.body) as Mensaje;
  this.mensajes.push(mensajeRecibido);
  })
```

C. Creamos el metodo para publicar mensaje.

```typescript
public btnEnviarMensaje() {
 this.client.publish({ destination: `/app/mensaje`, body: JSON.stringify(this.mensaje) });
 this.mensaje.texto = '';
}
```

D. Donde guardamos los mensajes los mostramos.

```typescript
<div class="card-body" style="overflow-y: scroll; height: 360px;">
  <ul class="list-group">
+    <li class="list-group-item list-group-ligth" *ngFor="let mensaje of mensajes">
+      {{mensaje.texto}}
```

E. poblamos el tecto del mensaje antes de enviar, y agregamos el evento click al voton de enviar

```html
input type="text" name ="texto" [(ngModel)]="mensaje.texto"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 207. Notificar cuando un nuevo usuario se conecta al chat

A. Agregamos input para poner el nombre del usuario

```html
<input *ngIf="!conectado" type="text" name ="username" [(ngModel)]="mensaje.username" class="form-control" placeholder="nombre usuario..." />
```

B. Agregamos el envio de la notificacion en tanto se conecte

```typescript
this.client.onConnect = (frame) => {
...
  this.mensaje.tipo = `new_user`;
  this.client.publish({ destination: `/app/mensaje`, body: JSON.stringify(this.mensaje) });
}
```

C. Vamos al back para agregar el tipo de mensaje y usuario al modelo

```java
public class Mensaje implements java.io.Serializable {
  ...
  private String username;
  private String tipo;
  getters/setters
```

D. En el controlador agregamos un condicional, que si este
es un mensaje o es que se conecto un nuevo sua

```java
public Mensaje recibeMensaje(Mensaje msj) {
  msj.setFecha(new Date().getTime());
  msj.setTexto("msj Recibido: " + msj.getTexto());

  if(msj.getTipo().equals("new_user")) {
    msj.setTexto("Nuevo Usuario");
  }
  return msj;
}
```

E. Mostramos el nombre del usuario

```java
<li class="list-group-item list-group-ligth" *ngFor="let mensaje of mensajes">
  <span *ngIf="mensaje.tipo=='new_mensaje'">{{mensaje.fecha | date: "shortTime"}} - {{mensaje.username}} Dice:<br />{{mensaje.texto}}</span>
  <span *ngIf="mensaje.tipo=='new_user'">Se conecto {{mensaje.username}}</span>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 208. Dando un color al username cuando se conecta al servidor

A. Creamos en el controlador un arreglo de colores y se lo asignamos a
la nueva propiedad que le agregamos a Mensaje de forma aleatoria.

```java
= public class ChatController {
+  private String colores[] ={ "red", "green", "blue", "yellow", "orange", ""  };
=  public Mensaje recibeMensaje(Mensaje msj) {
...
+    if(msj.getTipo().equals("new_user")) {
+    msj.setColor(colores[new Random().nextInt(colores.length)]);
```

A. En el front mandamos agregamos el color en el modelo, y si
nuestro mensaje.color esta asignado y el mensaje que lleguo es para
notificar que un usr se conecto y el nombre de suario de nuestro
mensaje es el mismo que el que recibimos el mensaje que llega
lo asignamos al mismo que el que enviamos.

```typescript
= this.client.subscribe('/chat/mensaje', (event)=>{
= let mensajeRecibido:Mensaje = JSON.parse(event.body) as Mensaje;
= this.mensajes.push(mensajeRecibido);
+ if(!this.mensaje.color && mensajeRecibido.tipo==NEW_USER && this.mensaje.username == mensajeRecibido.username) {
+   this.mensaje.color = mensajeRecibido.color
+ }
```

A. Mostramos el color

```typescript
<span *ngIf="mensaje.tipo=='new_mensaje'">{{mensaje.fecha | date: "shortTime"}} - <span [ngStyle]="{'color': mensaje.color}" style="font-weight: bold;">{{mensaje.username}}</span> Dice:<br />{{mensaje.texto}}</span>
<span *ngIf="mensaje.tipo=='new_user'">Se conecto <span style="font-weight: bold;"  [ngStyle]="{'color': mensaje.color}" >{{mensaje.username}}</span></span>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 209. Notificar cuando un usuario está escribiendo

A. Agregamos un nuevo metodo en el controlador

```java
@MessageMapping("/estoyEscribiendo")
@SendTo("/chat/quienEscribe")
public String estaEscribiendo(String username) {
  return username.concat(" esta escribiendo ...");
}
```

B. En el front en el evento keyUp mandamos a llamar al servicio

```html
<input type="text" name="texto" [(ngModel)]="mensaje.texto"
    (keyup)="eventTextoKeyUp()" placeholder="Escribe un mensaje..." />
```

B.1. Agregamos el evento

```typescript
public eventTextoKeyUp(){
  this.client.publish({destination:`/app/estoyEscribiendo`, body: this.mensaje.username});
}
```

C. Agregamos el escucha por si alguien esta escribiendo

```typescript
= ngOnInit() {
...
=  this.client.onConnect = (frame) => {
...
+    this.client.subscribe('/chat/quienEscribe', (event)=>{
+      this.actualMenteEscribiendo = event.body;
+      setTimeout(()=>{
+        this.actualMenteEscribiendo = '';
+      }, 3000)
+    });
```

A. Mostramos el mensaje y agregamos que el Scroll siempre se
muestre asta abajo con los scroll

```html
<div #scrollChat [scrollTop]="scrollChat.scrollHeight" class="card-body" style="overflow-y: scroll; height: 360px;">
...
=    <div class="card-footer">
=  <form>...</form>
+   <div>{{actualMenteEscribiendo}}</div>
```

**Commit:** gitlab.com/MarioM51/chat-app/commit/13607f189566dcccf91498974139a1540af5c98a
**Resultado:** Al llenarse la lista de mensajes este bajara
automaticamente, y se mostrara a todos cuando un usuario envia
un mensaje o se conecta.
![26_Chat_Primero](./img//26_Chat_1.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 210. Instalación Mongo DB

Para guardar los mensaje ussaremos Mongo.
A. Descargamos mongoDB Comunity de su pagina
B. agregamos en variables de entorno
C. Comprobamos en termnal con: `mongo -version`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 211. Qué es Mongo y algunos ejemplos

Documentacion docs.mongodb.com -> getting Starter

#### 212. Instalando Robo 3T una herramienta GUI para MongoDB

Con el sensillito tenemos

#### 213. Agregando Clases del Modelo Document y Repository

A. Descomentamos&Agregamos la dependencia del pom

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-mongodb</artifactId>
</dependency>
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

B. Configuramos nuestra coneccion y hacemos que muestre los logs de mongo

```properties
spring.data.mongodb.url: mongodb://localhost:27017/chat_bd
logging.level.org.springframework.data.mongodb.core.MongoTemplate=debug
```

C. A nuestro modelo Mensaje lo convertimos a un Entity de Mongo y
agrgamos un ID con su gets/sets

```java
@org.springframework.data.mongodb.core.mapping.Document(collection="mensajes")
public class Mensaje implements java.io.Serializable {
  private static final long serialVersionUID = 1L;

  @org.springframework.data.annotation.Id
  private String id;
```

D. Agregamos nuestro Repositorio, el cual es la misma filosofia
que con una BDR

```java
import org.springframework.data.mongodb.repository.MongoRepository;
public interface IMensajesRepository extends MongoRepository<Mensaje, String> {
  public List<Mensaje> findFirst10ByOrderByFechaDesc();
}
```

E. Agregamos Service

```java
public class mensajeServiceImpl implements IMensajeService {
  @Autowired IMensajesRepository mensajesRepo;

  @Override public List<Mensaje> primeros10Msj() {
    return mensajesRepo.findFirst10ByOrderByFechaDesc();
  }

  @Override public Mensaje guardarMensaje(Mensaje msjToSave) {
    return mensajesRepo.save(msjToSave);
  }
}
```

F. Agregamos el service en nuestro controlador

```java
+   @Autowired
+  IMensajeService mensajeService;

=  public Mensaje recibeMensaje(Mensaje msj) {
=  if(msj.getTipo().equals("new_user")) { ...
+  } else {
+    mensajeService.guardarMensaje(msj);
```

**Resultado:** Cada vez que se envie un msj se guardara
en mongo, siempre y cuando no sea un nuevo usaurio
conectandose.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 214. Historial de mensajes del chat con MongoDB

A. Agregamos el metodo para capturar el historial, pero
este debe se traer un Id del usuario para mandarselo
solo al usuario que se conecto y no a todos los demas
usuarios.
Por lo que usaremos un template el cual enviara el cuarpo
del mensaje (ya no lo hara el controlador), esto para que
podamos crear un canal unico al cual se inscribira un solo
cliente y solo se los mandara a este.

```java
@MessageMapping("/historial")
public void historial(String clienteId) {
 this.simpMessagingTemplate.convertAndSend("/chat/historial/"+clienteId, mensajeService.primeros10Msj());
}
```

A. Vamos al *chat.component.ts* Y agregamos una propiedad de clienteID y le
asignamos un valor unico, string unico.

```typescript
constructor() {
this.clienteId = "id-" + new Date().getUTCMilliseconds() +"-" +Math.random().toString(36).substr(2);
}
```

A. Dentro del onConect agregamos el metodo de escucha del historial y pasamos
fecha UTC a obj-JS y volteamos el arreglo para que se vea bien el orden de los msj.
C.1. Ahi mismo agregamos la peticion para que nos llegue el historial
a travez de donde estamos escuchando.

```typescript
//escuchamos
this.client.subscribe("/chat/historial/"+this.clienteId, (event)=> {
  const historial = JSON.parse(event.body) as Mensaje[];
  this.mensajes=historial.map(m=>{m.fecha=new Date(m.fecha);return m;}).reverse();
});

//pedimos
this.client.publish({ destination: "/app/historial", body: this.clienteId})
```

A. Resetemos mensajes en el disconect

```typescript
=  this.client.onDisconnect = (frame) => {
=    this.conectado = this.client.connected;
+    this.mensaje = new Mensaje();
+    this.mensajes = [];
=  }
```

**Commit:** gitlab.com/MarioM51/chat-app/commit/13607f189566dcccf91498974139a1540af5c98a
**Resultado-Final:** Lo mismo pero ahora se guardan los mensajes
en MongoDB y al desconectarse se borran de la pantalla.
![26_Chat_Primero](./img//27_Chat_2.gif)----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 215. Agradecimientos y despedida

Uf uf ufff

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

### POST-ERRORES

#### Error ClassNotFoundException ContextFactory

> **Error:** ClassNotFoundException: com.sun.xml.internal.bind.v2.ContextFactory.  
> **Fuente:** [Link](https://stackoverflow.com/questions/53978612/jaxb-classnotfoundexception-building-spring-boot-app-2-2-0-with-java-11)
> **Descripcion:** programe con java8, pase a java11 y en `mvn package`
> creaba correctamente ej .jar, pero en el `java -jar xxx.jar` me sacaba
> este error y no podia iniciar el servicio.  
> **Causa**: Segun la fuente de java9 en adelante se eliminaron unas
> depenndencias por defecto.  
> **Solucion:** Debemos de agregar estas que fueron eliminadas de forma
> explisita en el pom.xml
>
> ```xml
>     <dependency>
>     <groupId>org.eclipse.persistence</groupId>
>     <artifactId>org.eclipse.persistence.moxy</artifactId>
>     <version>2.7.3</version>
>   </dependency>
>   <dependency>
>     <groupId>javax.xml.bind</groupId>
>      <artifactId>jaxb-api</artifactId>
>     <version>2.3.1</version>
>   </dependency>
>   <dependency>
>       <groupId>com.sun.xml.bind</groupId>
>       <artifactId>jaxb-impl</artifactId>
>       <version>2.3.1</version>
>   </dependency>
>   <dependency>
>       <groupId>com.sun.xml.bind</groupId>
>       <artifactId>jaxb-core</artifactId>
>       <version>2.3.0</version>
>   </dependency>
> ```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### Error RequestRejectedException Content-Disposition

<!-- ejemplo en react-native.md->(Realizando el login con el Backend) -->

**Error:** org.springframework.security.web.firewall.RequestRejectedException: The request was rejected because the parameter name "------WebKitFormBoundaryc6aIabBbRK7O3YZV Content-Disposition: form-data; name" is not allowed.  

**Descripcion:** Intantava pasar el login de postman a su implementacion
con javascript con fetch, pero el codigo que me daba postman me saba
este error.  

**Causa:** Declare mal los valores en postman, la peticiones de tipo
`application/x-www-form-urlencoded`, por lo tanto los campos (usuario, email)
NO se deben de pasar por `form-data` (pestaña de body de postman)

**Solucion:**: pasar los parametros en body->x-www-form-urlencoded

![30-error-login-desde-vainilla-javascript](./img/30-error-login-desde-vainilla-javascript.PNG)

La diferencia esta en como se genera el codigo para consultar, abajo se
ve la diferencia

```js
// INCORRECTO (asi lo genera cuando esta en form-data)
var data = new FormData();
data.append("username", "user@mail.com");
data.append("password", "userpassword");
data.append("grant_type", "password");

// CORRECTO (asi cuando esta en x-www-form-urlencoded)
var urlencoded = new URLSearchParams();
urlencoded.append("username", "user@mail.com");
urlencoded.append("password", "userpassword");
urlencoded.append("grant_type", "password");
```

#### There is no PasswordEncoder mapped for the id "null"

**Error:** `.IllegalArgumentException: There is no PasswordEncoder mapped for the id "null"`  
**Descripcion**: A la hora de hacer un login para obtener un token, me
da este error.  
**Fuente:** [stackoverflow](https://stackoverflow.com/questions/49654143/spring-security-5-there-is-no-passwordencoder-mapped-for-the-id-null)  
**Causa:** Estaba mal configurado el "AuthenticationManagerBuilder", ya
que le faltaba el encoder.
**Solucion:** En mi caso agregar el encoder.

```java
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth
      .userDetailsService(userService)
+     .passwordEncoder(SecretsEncoder.getEncoder()); // le da un org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
  }
}
```

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

### Cosas que me voy encontrando

#### Enlaces a parecidos

- En maven.md - Actualizar dependencia dentro de otra (un parent creo)

#### Uso de perfiles

- Fuente: [Tutorial perfiles](https://mkyong.com/spring-boot/spring-boot-profile-based-properties-and-yaml-example/)
- Ver `usuarios-devops-2.md - Construir Jars Diferentes para los diferentes ambientes`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### Personalizar errores de autenticacion

- Fuente: [stackoverflow](https://stackoverflow.com/questions/19767267/handle-spring-security-authentication-exceptions-with-exceptionhandler)

**Objetivo:** Cambiar el jsson por defecto que da Spring security con
un error de autenticacion.

A. Creamos un handler personalizado

```java
package com.museodelamaderatlaxco.security;

import java.io.IOException;import javax.servlet.ServletException;import javax.servlet.http.HttpServletRequest;import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.AuthenticationException;import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;import org.springframework.web.servlet.HandlerExceptionResolver;

@Component("restAuthenticationEntryPoint")
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Autowired
    @Qualifier("handlerExceptionResolver") // ya lo incluye spring
    private HandlerExceptionResolver resolver;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
      String token = request.getHeader("Authorization");
      if(token == null) {
        exception.addSuppressed(new Throwable("No se encontro el token en la peticion"));
      }
      resolver.resolveException(request, response, null, exception);
    }
}
```

B. En nuestra clase que extendemos de `ResourceServerConfigurerAdapter`
agregamos el handler personalizado.

```java
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

  @Autowired
  @Qualifier("restAuthenticationEntryPoint")
  AuthenticationEntryPoint customAuthEntryPoint;

  @Override
  public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
      resources.authenticationEntryPoint(customAuthEntryPoint);
  }
```

C. Ya podemos capturar la excepcion un adviceControler.

```java
@ControllerAdvice
public class ErrorController {

  @ExceptionHandler(AuthenticationException.class)
  public ResponseEntity<ErrorResponse> userImageExceptionHandler(AuthenticationException userExeption, HttpServletResponse response) {
    List<String> details = new ArrayList<>();
    for (Throwable throwable : userExeption.getSuppressed()) {
      details.add(throwable.getMessage());
    }
    return ErrorResponse.buildResponse(userExeption.getMessage(), details);
  }
  
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### TODO: Guardar y entregar imagen en base64

A. Creamos el campo tipo TEXT en la BD para no tener problemas de tamaño

```java
@Entity
@Table(name="product_images")
@JsonIgnoreProperties({"product"})
public class ProductImage {
  
  @Id
  private Integer idProductImage;
  ...

  @Column(columnDefinition = "TEXT", nullable = false)
//ignorar propiedad json. propiedad json solo de lectura, proteger contraseña
  @JsonProperty(access = Access.WRITE_ONLY)
  private String base64;

  //GETTERS/SETTERS
```

B. Guardamos como simepre usando CrudRepository atraves de un service

```java
public interface IProductImageDAO extends CrudRepository<ProductImage, Integer> {
  
}
...............................................
@Service
public class ProductImageServiceImpl implements IProductImageService {
  @Override
  public ProductImage add(ProductImage newProduct) {
    return iProductImageDAO.save(newProduct);
  }
}
```

C. Creamos el controlador

- `@RequestParam("image") MultipartFile`: Recibimos el archivo de esta manera
- `Authentication auth` Si configuramos Spring Security recibiremos el
  usuario logueado de esta manera.
- `image.getBytes();` Obtenemos los bytes del archivo que recibimos necesarios
  para pasarlos a base64.
- `new String(Base64Utils.encode(bytes)` convertimos los bytes a base64
  para poderoo guardar como texto

```java
@RestController
@RequestMapping("/api/products")
public class ProductImageController {
  @PostMapping("/{idProduct}/images")
  @ResponseStatus(value=HttpStatus.CREATED)
  ProductImage addImage(@PathVariable int idProduct, @RequestParam("image") MultipartFile image, Authentication auth) {
    ProductEntity product = iProductService.findById(idProduct);

    checkPermissionsOverProduct(auth, product);

    //Creamos entidad de imagen con el archivo como base64
    ProductImage newProductImage = new ProductImage();
    newProductImage.setProduct(product);
    newProductImage.setName(image.getOriginalFilename());
    newProductImage.setContentType(image.getContentType());
    try {
      byte[] bytes = image.getBytes();
      newProductImage.setBase64(new String(Base64Utils.encode(bytes)));
    } catch (IOException e) {
      e.printStackTrace();
      throw new ProductException(ErrorEnum.PRODUCT_SAVE_IMAGE_FAIL);
    }

    return iProductImageService.add(newProductImage);
  }

  static private void checkPermissionsOverProduct(Authentication auth, ProductEntity product) {
    int idUser = Integer.parseInt(auth.getName());
    if(product.getUser().getIdUser() != idUser) {
      throw new PermissionException(ErrorEnum.FORBIDDEN, Arrays.asList("El producto no le pertenece al usuario"));
    }
  }
```

D. Podemos enviar la imagen por JS de la sig. manera

```js
const newImageUri = "file:///" + result.uri.split("file:/").join(""); // NOTA: Dejar como viene la uri para ios
const nameX = newImageUri.split("/").pop();
const typeX = mime.getType(newImageUri);
const image = {
  uri : newImageUri,
  type: typeX,
  name: nameX
}
..........................
export function uploadProductPicture(image, idProduct) {
  return new Promise(async function(resolve, reject){
    if(!idProduct) {     reject('Id del producto no existe'   ); return ; }
    if(idProduct <= 0) { reject('Id del producto no es valido'); return ; }
    
    let token = await StorageService.getToken();
    if(isEmpty(token)) { reject('Sin token'); return ; }

    const formData = new FormData();
    formData.append('image', image);

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer "+token);

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: formData
    };

    const url = IP+"/api/products/"+idProduct+"/images";

    fetch(url, requestOptions)
      .then(async (response) => {
        const data = await response.json();
        if(response.ok) { resolve(data); } 
        else {            reject(data); }
      })
      .catch(error => {   reject(error); })
    ;
  })
}
```

**Resultado1:** Con esto deberiamos de poder guardar la imagen

##### Recuperar la imagen en base64 y mostrarla

Solo hacemos lo inverso, es decir recuperamos el base64 y lo pasamos a bytes
de la siguiente forma.

```java
@GetMapping("/images/{idProductImage}")
@ResponseStatus(value=HttpStatus.OK)
ResponseEntity<byte[]> getImageByIdImage(@PathVariable int idProductImage, HttpServletResponse response) {

  // entregar imagen en base64
  ProductImage imageFinded = iProductImageService.findById(idProductImage);

  String base64 = imageFinded.getBase64();
  byte[] bytes = Base64.decodeBase64(base64);
  
  final HttpHeaders headers = new HttpHeaders();
  headers.setContentType(MediaType.IMAGE_PNG);
  
  return new ResponseEntity<byte[]> (bytes, headers, HttpStatus.CREATED);
}
```

#### Eliminar en cascada

**Ejemplo:** Tenemos una relacion una a muchos, un producto tiene muchas
imagenes, y queremos que cuando eliminemos un producto se eliminen todas
sus imagenes.

```java
public class ProductEntity {
  ...

  @OneToMany(fetch=FetchType.LAZY) @JoinColumn(name = "fk_product")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<ProductImage> images;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### Evitar nulos en modificacion

<!-- quitar nulos, modificacion manejo de nulos, evitar sobreescribir campos que lleguen como nulos -->

Usamos la siguiente clase, como en el ejemplo que ahi tiene

- **NOTAR QUE:** Lo que hace es copiar la informacion que no es nula,
  al entity que ya estaba guardado.

```java

import java.util.HashSet;
import java.util.Set; import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper; import org.springframework.beans.BeanWrapperImpl;

/***
 * Fuente: https://stackoverflow.com/questions/27818334/jpa-update-only-specific-fields
 * 
 * Ejemplo:
public ResponseEntity<?> updateUser(@RequestBody User newUserInfoWithNulls) {

  User existing = userRepository.read(user.getId());
  EntityTransform.copyNonNullProperties(newUserInfoWithNulls, existing);
  userRepository.save(existing);

  // ...
}
*/
public class EntityTransform {

  private EntityTransform(){  }

  public static void copyNonNullProperties(Object src, Object target) {
    BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
  }

  private static String[] getNullPropertyNames (Object source) {
      final BeanWrapper src = new BeanWrapperImpl(source);
      java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

      Set<String> emptyNames = new HashSet<String>();
      for(java.beans.PropertyDescriptor pd : pds) {
          Object srcValue = src.getPropertyValue(pd.getName());
          if (srcValue == null) emptyNames.add(pd.getName());
      }
      String[] result = new String[emptyNames.size()];
      return emptyNames.toArray(result);
  } 
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### Manejo de errres DataIntegrityViolationException

<!-- manejar erreres en base de datos, cambiar nombre a constraint, 
asignar nombre a constraint, capturar errores de restriccion -->

Fuente: [stackoverflow](https://stackoverflow.com/questions/2109476/how-to-handle-dataintegrityviolationexception-in-spring)

**Objetivo:** Controlar los conflictos en base de datos, mas especifico
los de violacion de intedridad de datos.

Pre-condiciones. Tengo el siguiente [pom.xml](./img/31_pom_de_manejo_de_errores.xml),
en el cual tengo el starter de JPA (que es el que importa) y el de validation
pre creo este ultimo no influye creo.

- **Nota:** Lo interezante esta en el punto 0 y C.

0, Agregamos el Entity y le definimos el nombre del constrain, para luego
poderla capturar y convertirla en el feddback.

```java
import javax.persistence.UniqueConstraint;

@Entity
@Table(
  name="PRODUCTS",
  uniqueConstraints = {
    //CUIDADO: Si definimos "PRODUCT_ID" y "NAME" estamos diciendo que la combinacion de estos dos debe ser unica.
    // uniqueConstraints = {@UniqueConstraint( columnNames = {"PRODUCT_ID", "NAME"}, name = "PRODUCT_UNIQUE_VIOLATION" )}
    @UniqueConstraint( columnNames = {"name"}, name = "product_name_unique_violation")
  }
)
public class ProductEntity {  
  @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Integer productId;

  @Size(min=3, max=60)
  @Column(name = "name")
  //CUIDADO: Si agregamos lo comentado, no respetara el nombre del constraint que definimos arriba
  //@Column(name = "name", unique = true)
  private String name;
```

A, Tenemos el siguiente DAO y Services sin nada de especial, solo el
Assert pero no influye en nuestro objetivo

```java
import org.springframework.data.repository.CrudRepository;
public interface IProductDAO extends CrudRepository<ProductEntity, Integer> { }

//=========================

import com.mario51.cdmt.error.utils.MyAssert;
import com.mario51.cdmt.utils.EntityTransform; import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired; import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService {
  
  @Autowired
  private IProductDAO productDAO;
  
  @Transactional
  public ProductEntity save(ProductEntity productEntity) {
    MyAssert.assertThat(productEntity.getProductId() == null);
    
    ProductEntity saved = null;
// Tener en cuenta que al haber un error que ahora nos importa lanzara
// la siguiente Exception "org.springframework.dao.DataIntegrityViolationException" 
!   saved = this.productDAO.save(productEntity);
    return saved;
  }

  ...

}
```

B, Agregamos Un `ControllerAdvice` que manejara nuestra exepcion que puede lanzarse
arriba.

```java
import org.springframework.dao.DataIntegrityViolationException;
import com.mario51.cdmt.error.model.*;
import com.mario51.cdmt.error.utils.*;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class ErrorController {

  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseBody
  public ErrorModel dataIntegrityConflic(DataIntegrityViolationException ex) {
    final var dataIntVio = new DataIntegrityViolationHandler();
    final ErrorModel errorModel = dataIntVio.getErrorModel(ex);
    return errorModel;
  }
```

C, Agregamos el que conseguira el constraint que se violo, y retornara
el feedback del error.

```java
import java.util.Map;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import com.mario51.cdmt.error.model.ErrorModel;
import com.mario51.cdmt.error.model.ErrorTypeEnum;

public class DataIntegrityViolationHandler {
  
  private static final Map<String, String> CONSTRAINS = Map.of(
// NOTAR que "product_name_unique_violation" es el nombre que le asignamos
// al constrain al declarar en Entity (Punto 0)
+   "product_name_unique_violation", "Ya existe un producto con ese nombre"
  );

  public ErrorModel getErrorModel(DataIntegrityViolationException ex) {
    String rootMsg = this.getConstraintName(ex);
    if (rootMsg != null) {
        String lowerCaseMsg = rootMsg.toLowerCase();
        for (Map.Entry<String, String> entry : CONSTRAINS.entrySet()) {
            if (lowerCaseMsg.contains(entry.getKey())) {
                return new ErrorModel(ErrorTypeEnum.BAD_INPUT, entry.getValue());
            }
        }
    }
    ex.printStackTrace();
    return new ErrorModel(ErrorTypeEnum.INTERNAL_ERROR, "DataIntegrityViolationException: " + rootMsg);
  }

  private String getConstraintName(DataIntegrityViolationException ex) {
    if(ex.getCause() instanceof ConstraintViolationException) {
      var ConstrainViotEx = (ConstraintViolationException) ex.getCause();
      return ConstrainViotEx.getConstraintName();
    }
    return null;
  }

}
```

**Resuotado:** Cuando se lance un error sera capturado por nuestro
"ControllerAdvice" y controlado por "DataIntegrityViolationHandler" el
cual retornara una respuesta accorde al constraint violado..

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### Ejecutar comando desde java

```java
import java.util.*;
import java.io.*;

public class GoodWinRedirect {

  class StreamGobbler extends Thread {
    
    InputStream is;
      String type;
      OutputStream os;
  
      StreamGobbler(InputStream is, String type) {
          this(is, type, null);
      }
  
      StreamGobbler(InputStream is, String type, OutputStream redirect) {
          this.is = is;
          this.type = type;
          this.os = redirect;
      }
  
      public void run() {
          try
          {
              PrintWriter pw = null;
              if (os != null)
                  pw = new PrintWriter(os);
  
              InputStreamReader isr = new InputStreamReader(is);
              BufferedReader br = new BufferedReader(isr);
              String line=null;
              while ( (line = br.readLine()) != null) {
                  if (pw != null) {
                    pw.println(line);
                  }
                  System.out.println(type + ">" + line);    
              }
              if (pw != null) {
                pw.flush();
              }
          } catch (IOException ioe) {
              ioe.printStackTrace();  
          }
      }
  }

  
    public void mainRun(String directory, String[] command) {

        try {
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            processBuilder.directory(new File(directory));

            //FileOutputStream fos = new FileOutputStream(args[0]);
            Runtime rt = Runtime.getRuntime();
            Process proc = processBuilder.start();
            // any error message?
            StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");

            // any output?
            StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT: " /*, fos*/ );

            // kick them off
            errorGobbler.start();
            outputGobbler.start();

            // any error???
            int exitVal = proc.waitFor();
            System.out.println("ExitValue: " + exitVal);
            //fos.flush();
            //fos.close();
        } catch (Throwable t)
          {
            t.printStackTrace();
          }
    }

    public static void main(String[] args) {
      GoodWinRedirect a = new GoodWinRedirect();
      //a.mainRun("ping 127.0.0.1 -n 2");
      String[] command = {"C:/Users/subject51/AppData/Roaming/npm/gatsby.cmd", "build"};
      a.mainRun("E:/cursos/0activ/proyectos-test/cdmt/gatsby/code", command);
    }
}
```

#### Implementar OAuth con JWT simple

Fuente: https://sunitc.dev/2020/08/23/spring-boot-add-jwt-authentication/

#### Hacer una peticion https sin validar certificado (certificado auto firmado)

Fuente: https://stackoverflow.com/questions/4072585/disabling-ssl-certificate-validation-in-spring-resttemplate

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->


## Microservicios con Spring Boot y Spring Cloud Netflix Eureka

```r
¿Que hace el servidor Ribbon?;'
Es un balecanceador de carga antiguo, pero spring cloud con feing ya se encaga de hacer esto en auto.
'

Eureka: ¿Que es?, ¿Como crear levantar una instancia de eureka?;'
¿Que es?
--Es un servicio-web permitiendo que a este se registren micro-servicios, y al querer comunicarse entre ellos lo hacen a traves de este, no usando una direccion (ej. 127.0.0.1:8080) si no por nombre (ej. api-products)

¿Como crear levantar una instancia de eureka?
--1: Crear un proyecto con las siguientes dependencias
---spring-cloud-starter-netflix-eureka-server
---IMPORTANTE: Si causa errores tener una version mas actualizada de eureka y una un poco debajo de eurecla-client

<?xml version="1.0" encoding="UTF-8"?>
<project ...>
  <modelVersion>4.0.0</modelVersion>
  
  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.7.12</version>
    <relativePath/>
  </parent>
  
  <groupId>mario</groupId>
  <artifactId>eureka</artifactId>
  <version>0.0.1</version>
  <name>eureka</name>
  <description>Demo project for Spring Boot</description>
  
  <properties>
    <java.version>17</java.version>
    <spring-cloud.version>2021.0.7</spring-cloud.version>
  </properties>

  <dependencies>

    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
    </dependency>

  </dependencies>
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-dependencies</artifactId>
        <version>${spring-cloud.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <build>
    <plugins>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </build>
</project>

--2: Poner las siguientes configuraciones

  #nombre de aplicacion
  spring.application.name=srv-eureka

  #opcional
  #cambiar puerto
  server.port=8301

  eureka.instance.hostname=localhost
  eureka.client.service-url.defaultZone=http://localhost:8301/eureka

  #por default eureka server puede funcionar igual como client, pero en este caso no queremos eso
  eureka.client.register-with-eureka=false
  eureka.client.fetch-registry=false

  #para evitarnos el error EMERGENCY! EUREKA MAY BE INCORRECTLY CLAIMING INSTANCES ARE UP WHEN THEY ARE NOT
  eureka.server.renewal-percent-threshold=0.85


--3: por solo con agregar la dependencia se habilita el servidor eureka, pero para hacerlo de forma explicita podemos hacerlo con

    import org.springframework.boot.SpringApplication;
    import org.springframework.boot.autoconfigure.SpringBootApplication;
    import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

    @SpringBootApplication
+   @EnableEurekaServer
    public class EurekaApplication {

      public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class, args);
      }

    }
'

¿Como registrar una instancia a Eureka server, explicacion y ejemplo?, ¿Como configurar un puerto dinamico?;'
Explicacion
--Debemos de agregar una dependencia que hacer que nuestro proyecto funcione como cliente de eureka server, el cual incluye definir el nombre de aplicacion, e indicar la ubicacion/IP del servidor eureka
--Se recomienda de una vez configurar un puerto dinamico, para poder multiples instancias sin lios

Ejemplo
--1:Instalar la dependencia
  <dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
  </dependency>

--2:Agregar la sig. configuracion (incluye puerto dinamico)

  spring.application.name=api-products
  server.port=${PORT:0}
  eureka.instance.instance-id=${spring.application.name}:${spring.application.instance_id:${random.value}}

  eureka.client.service-url.defaultZone=http://localhost:8301/eureka

  logging.level.org.springframework.web=DEBUG

--3:Al levantar deberia de registrarse a eureka, y con eso ya debemos de poder consultarlo a travez de feing
'

Feing: ¿Que es?, ¿Como usarlo?;'
¿Que es?
--Es un cliente rest que puede aprovechar las multiples instancias registradas en eureka

¿Como usarlo?
--1: Asegurarnos que las instancias tienen un numbro
--spring.application.name=api-products

--2:Asegurar que las ambas instancias (la que consulta y la consultada) estan registradas en eureka

--3:Instalar de dependencia
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>

--4:Crear un cliente feing refiriendonos a ella con el nombre de aplicacion
  package mario.bill.webApiDao;
  import java.util.List;
  import org.springframework.cloud.openfeign.FeignClient;
  import org.springframework.web.bind.annotation.GetMapping;
  import org.springframework.web.bind.annotation.PathVariable;
  import mario.commons.ProductDto;

  @FeignClient(name = "api-products")
  public interface RestClientFeing {
    
    @GetMapping("/products")
    public List<ProductDto> findAll();
    
    @GetMapping("/products/{id}")
    public ProductDto findById(@PathVariable Integer id);

  }

--5:ya podemos consultar usando reparticion de carga
  @Service
  public class ProductsWebApiDao {
    
    @Autowired
    private RestClientFeing rc;
    
    public ProductDto findById(int id) {
      var found = rc.findById(id);
      return found;
    }
  }
'

Resilience4J ¿Que hace Hystrix y su relacion con Resilience4J?;'
¿Que hace Hystrix y su relacion con Resilience4J?
--Para tolerancia a fallos, si falla una peticion a una instancia, entonces lo intentara con otra instancia.
--Resilience4J es el sustituto el cual se recomienda usar este
'
```

### 1. Introducción

Los micro-servicios son la solucion para no tener aplicacione monoloticas
en las cuales tenemos varias aplicaciones que conforman un todo, y cada una
de estas aplicaciones pueden ser idenpendientes de personal, tecnologias,
despliegues etc. Por enlistar sus caracteristicas tenemos.

- Autonomos

- Especializados: Reduce su complejidad y aumenta su desempeño

- Registro y auto-descubrimiento de servicios: Se comunica con otros
  serivicios indicando su ubicacion y sus requisitos

- Escalado flixible: Se pueden escalar dependiendo de la demanda, se
  aumenta capasidad solo en aquello que necesita mas recursos,
  a travez de una estrategia de clousters.

- Tolerancia a fallos

- Balanceo de cargas

- Configuracion centralizada: Podemos tener un servidor de configuracion
  para manejar todas las aplicaciones que conforman el proyecto.

- Libertad tecnologica: Podemos usar la tecnologia que mas nos acomode
  o convenga.

- Agilidad y equipos mas pequeños: Cada micro-servicio puede tener un
  equipo o un ontexto.

- Ciclos de desarrollo mas cortos

- Codigo reutilizable: podemos reutilizar un micro-servicios en otros
  micro-servicios.

**Modulos:** Tolo lo trabajaremos con el framework de Spring pero
tendremos diferentes modulos de este como son.

- Spring Boot, IoC, JPA, REST, Security, OAuth2-JWT

- Eureka Netflix/Servidor Eureka Netflix - Entorno de micro-servicios

- Eureka Netflix/Eureka Client - Cada micro-servicio puede funcionar como cliente

- Rest Template - para estadarizar REST

- Faing - para estadarizar REST

- Eureka Netflix/Ribbon - Balanceador de carga

- Eureka Netflix/Hystrix - Para la tolerancia a fallos

- Eureka Netflix/Gateway Zuul -  Servidor Perimetral, es como una la entrada principal o punto de
entrada a todos los micro-servicos, que utiliza a Ribbon y Hystrix

**Diagrama de Modulos:**
![26_Chat_Primero](./img/1_Diagrama_MicroServicios.PNG)

A. En tiempo de desplieque los micro-servicios se registran en
Eureka Server.

A. En tiempo de despliegue se toman las configuracione de Spring Cloud Config el cual
se hacen cambios de configuaracion a travez de Git.

B. El cliente se cominica a GateWay Zuul.

B. Ribbon hace un balanceo de carga.

B. Se consulta al micro-servicio el cual puede consultar otros micro-servicios.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 2: Microservicios: la base

La forma de funcionar de *Eureka Server* es que cuando un micro-serv quiere
comunicarse con otro, este no va directo a donde esta desplegado este, si no
que primero le pregunta a Eureka Server por el servicio por medio de un ID
Eureka le entrega la ubicacion exacta y ahora si puede el micro-serv consultar
directamente a otro micro-servcio, todo esto ocurre de *forma tranzparente*
tal que uno como desarrollador no se<> entera ya que queda abstraido, lo cual ya no
debemos de preocuparnos por IPs o Puertos.

**Auto-Escalado:** Por lo explicado tenemos la posibilidad de agregar un
nodo adicional al clouster y en tiempo real Eureka se encarga de agregarlo
a candidato para el balanceo de carga. Incluso da la posibilidad de quitar
nodos como un **Des-Escalado** el cual puede que tengamos periodos de mayor
o menor demanda, por lo que no necesitaremos todos los dias del año la misma
potencia por lo que podriamos aumentarla oreducirla segun convenga.

**Tolerancia a Fallos:** Puede que la comunicacion falle por multiples razones
el cual puede tener distintas soluciones, entre ellas consultar o otros micro-S
lo pue puede evitar aun mas fallos.

![2_funcionamiento_eureka](./img/2_funcionamiento_eureka.png)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 7. Creando nuestro microservicio productos

**Objetivos:** Por el momento no entraremos mucho en micro-S lo que haremos es
dos servicios Rest y comunicarloes entre ellos.

A. Creamos el proyecto con las siguientes dependencias

- Developer Tools -> Spring Boot Dev/Tools.
- DevTools
- JPA
- H2 Database
- Web Starter

A. Configuramos en el .properties, cambiando el nombre el puerto,

```properties
spring.application.name=productos-app
server.port=8001
```

C. Creamos entity Producto

```java
@Entity
@Table(name="productos")
public class Producto implements Serializable{
  private static final long serialVersionUID = -8244669590998614179L;

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  private String nombre;
  private Double precio;

  @Column(name="create_at")
  @Temporal(TemporalType.DATE)
  private Date createAt;

  setters/getter
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 9. Creando el repositorio JPA para los productos

A. Usaremos `CrudRepository` que ya nos trae las operaciones CRUD que
necesitamos

```java
import org.springframework.data.repository.CrudRepository;
import com.mariocorps.productosapp.models.entity.Producto;
public interface IProductoService extends CrudRepository<Producto, Long>{

}
```

#### 10. Creando el componente service para los productos

```java
import org.springframework.transaction.annotation.Transactional;
public class ProductoServiceImpl implements IProductoService {
  @Autowired
  private IProductoDAO productoDao;

  @Override
  @Transactional(readOnly=true)
  public List<Producto> getAll() {
    return (List<Producto>)productoDao.findAll();
  }
  @Override
  @Transactional(readOnly=true)
  public Producto findById(Long id) {
    return productoDao.findById(id).orElse(null);
  }
}
```

#### 11. Creando el controlador rest productos

```java
@RestController
public class ProductoController {
  @Autowired
  private IProductoService productoService;
  @GetMapping("/all")
  List<Producto> all() {
    return productoService.getAll();
  }
  @GetMapping("/{idProducto}")
  Producto byId(@PathVariable Long idProducto) {
    return productoService.findById(idProducto);
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 12. Agregando datos y probando nuestra API productos con Postman

A. Creamos el archivo import.sql en resources

```sql
INSERT INTO productos (nombre, precio, create_at) VALUES('Panasonic Pantalla LCD', 259990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Sony Camara digital DSC-W320B', 123490, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Apple iPod shuffle', 1499990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Sony Notebook Z110', 37990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Hewlett Packard Multifuncional F2280', 69990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Bianchi Bicicleta Aro 26', 69990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Mica Comoda 5 Cajones', 299990, NOW());
```

B. En los logs marrcara lo siguiente, indicando que ejecuto el script
HHH000476: Executing import script 'ScriptSourceInputFromUrl(file:/E:/cursos/0activ/web/angular-spring/productos-app/target/classes/import.sql)'

C. Probamos en postman

```r
http://localhost:8001/all
http://localhost:8001/ver/1
```

**Resultado:** Ya deberiamos de tener el obtener
todos y el obtener por ID.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 13. Creando microservicio items

Vamos a crear otro proyecto practicamente igual en los
cuales tendremos lineaFactura o item, el cual tendra el
id de producto, la cantidad y el total de estos.

A. Este proyecto solo tendra a Web y DevTools no tendra persistencia
B. Creamos la clase Producto la cual tendran los mismos datos
solo que sin ninguna anotacion.
B.1. Creamos la clase item que hara referencia al producto que creamos.

```java
public class Item {
  private Producto producto;
  private Integer cantidad;

  public Item(Producto producto, Integer cantidad) {
    this.producto = producto;
    this.cantidad = cantidad;
  }
  public Item() { }

  public Double getTotal() {
    return this.cantidad.doubleValue() * this.producto.getPrecio();
  }
  getters/setters
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 14. Creando componente service en items para consumir API productos

A. Asignamos los puertos, la idea que entre proyectos tengan uno diferente.

```properties
spring.application.name=linea-factura
server.port=8002
paths.services.productos.local=http://localhost:8001
```

B. Creamos clase de configuracion de RestTemplate para cominicarnos
con la API que creamos anteriormente.

```java
package com.mariocorps.lineasfactura;
import org.springframework.context.annotation.*
import org.springframework.web.client.RestTemplate;
@Configuration
public class RestTemplateConfig {
  @Bean("clienteRest")//nombre del bean si no toma el del metodo
  public RestTemplate registrarRestTemplate() {
    return new RestTemplate();
  }
}
```

B. Creamos el service con su interface correspondiente y le inyectamos
el bean que acabamos de crear.

```java
import org.springframework.web.client.RestTemplate;
@org.springframework.stereotype.Service
public class LineaFacturaServiceImpl implements IILineaFacturaService {

  @Autowired
  private RestTemplate clienteRest;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 15. Implementando componente service con cliente HTTP RestTemplate

A. Creamos el service el cual obtiene los productos del otro servicio
y los convierte a linea factura, ambos metodos son parecidos.
**Nota de diseño:** El Service aplia para los que obtienen informacion
de alguna fuente, por eso aplica tanto para obtenerlo de una BD como de
otro servicio.

```java
import java.util.*;
import org.springframework.beans.factory.annotation.*;
import com.mariocorps.lineasfactura.models.dto.*;
@org.springframework.stereotype.Service
public class LineaFacturaServiceImpl implements IILineaFacturaService {

  @Autowired
  private RestTemplate client;

  @Value("paths.services.productos.local")
  private String pathPr;

  @Override
  public List<LineaFactura> findAll() {
    //OBTENEMOS LOS PRODUCTOS DEL OTRO SERVICIO
    Producto[] productosArray =  client.getForObject(pathPr+"/all", Producto[].class);
    List<Producto> productos = Arrays.asList(productosArray);;

    //CONVERTIMOS LOS OBJETOS PRODUCTOS A LINEAFACTURA
    List<LineaFactura> lineasFacturas = productos.stream()
        .map(p -> new LineaFactura(p, 1))
        .collect(Collectors.toList());
    return lineasFacturas;
  }

    @Override
    public LineaFactura findbyId(Long id, Integer cantidad) {
        Map<String, String> pathVariables = new HashMap<String, String>();
        pathVariables.put("idProducto", id+"");
        Producto producto =  client.getForObject(pathPr+"/ver/{idProducto}",
                                Producto.class, pathVariables);
        return new LineaFactura(producto, 1);
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 16. Creando controlador en items

```java
@RestController
@RequestMapping("/lineas-factura")
public class LineaFacturaController {

  @Autowired
  IILineaFacturaService lineaFacturaService;

  @GetMapping("")
  List<LineaFactura> findAll() {
    return lineaFacturaService.findAll();
  }

  @GetMapping("/{id}/cantidad/{cantidad}")
  LineaFactura findById(@PathVariable Long id, @PathVariable Integer cantidad) {
    return lineaFacturaService.findbyId(id, cantidad);
  }
```

**Resultado:** Podemos consultar todas las lineasFacturas que son
todos los productos que son consultados en otra API-REST mas una X
cantidad y el por ID y cada uno de estos calcula su total segun
su cantidad.

```r
http://localhost:8002/lineas-factura/1/cantidad/2
http://localhost:8002/lineas-factura
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 17. Usando cliente REST de Feign para consumir nuestra API productos

La libreria Feing (desarrollada por Netflix) es otra forma de
implementar clientes HTTP que nos ayuda con interfaces que nos
facilitan el trabajo con Spring Cloud.
**Ventajas:** 1) Nos ayuda con la convercion de nuestros objetos collecciones

A. Agregamos la dependencia proyecto(clickDerecho)->Spring->Edit Starter
B. Vamos a la clase principal

```java
+ @EnableFeignClients
= @SpringBootApplication
= public class LineasFacturaApplication {
```

A. Agregamos el consumo del servicio con el uso de FeignClient. el cual
en el GetMapping debe ser exactamente ele mismo que que la ruta al que
queremos llamar.

```java
@FeignClient(name="productos-app", url="localhost:8001")
public interface ProductoClientHttpRest {
  @GetMapping("/all")
  List<Producto> findAll();

  @GetMapping("/ver/{idProducto}")
  Producto byId(@PathVariable Long idProducto);
}
```

A. Creamos una nueva implementacion de Service pero ahora
usando Feing

```java
@org.springframework.stereotype.Service
@Primary
public class LineaFacturaServiceImplFeign implements IILineaFacturaService {

  @Autowired
  private ProductoClientHttpRest productoClientHttp;

  @Override
  public List<LineaFactura> findAll() {
    return productoClientHttp.findAll().stream().map(p -> new LineaFactura(p, 1)).collect(Collectors.toList());
  }

  @Override
  public LineaFactura findbyId(Long id, Integer cantidad) {
    return new LineaFactura(productoClientHttp.byId(id), 2);
  }

```

**Importante:** Para cambiar que implementacion se usa
en el controlador podemos hacer uso de `@Primary` para
que tome esta y no la antigua implelemtacion.

Como alternativa podemos hacer uso de `@Qualifier("nombreX")` y
agregamos el nombre del bean al service `@Service("nombre")`
de no indicarlo tomara el nombre de la clase pero la primera
sera minuscula.

**Resultado:** Lo mismo pero mas bonito y bueno y a futuro barato.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 18. Balanceo de carga del lado del cliente con Ribbon

Vamos a agregar una nueva API de productos y por medio de Ribbon
balancearemos la carga entre las dos APIs, lo cual deberemos de
ir a linea-factura para configurarlo.

A. En linea-factura en el pom.xml(click Derecho)->Spring->Edit Starters
y agregamos Ribbon, veremos que se agrega la sig dependencia.

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
</dependency>
```

B. Vamos a *LineasFacturaApplication* y agregamos a ribbon

```java
+ @RibbonClient(name="productos-app")
= public class LineasFacturaApplication {
```

C. Vamos al .properties a agregar los servidores de esta API.

```java
productos-app.ribbon.listOfServers=localhost:8001,localhost:9001
```

D. Vamos a nuestro cliente Http *ProductoClientHttpRest* y
borramos el balor url.

```java
## @FeignClient(name="productos-app")
= public interface ProductoClientHttpRest {
```

E. vamos a productos-app(clickDerecho)->Run As->Run configurations...
-> Arguments -> VM Arguments ponemos -> Run

```java
-Dserver.port=9001
```

![3_run_args_config](./img/3_run_args_config.png)

Al terminar de levantar podremos ver en los logs que se levanto en el
puerto que le indicamos.

F. Ahora corremos la app con los mismo pasos solo que ahora eliminamos
el argumento de arriba y debemos de ver que levanta en el puerto

G. Comprobamos que esten ambos levantados tanto en 9001 y en 8001.
lanzando desde postman y cambiando el puerto.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 19. Probando balanceo de carga en postman

Vamos a probar visualmente donde cual es la instancia que esta
seleccionando el balanceador, mostrando el puerto en una propiedad
de producto que se asigna en alguna de las 2 instancias de *producto-app*
y se muestra en la unica 1 instancia de *linea-factura*.

A. Vamos a productos de *productos-app* y agregamos puerto
B. Agregamos el campo en el producto DTO de *linea-Factura*.
C. En el controlador de *linea-Factura* mostramos el puerto que
se recupero de *linea-Factura*.
D. Para captura rl puerto usamos el cual podemos capturar la
varible del properties agregandole al inicio "local."

```java
=  @Autowired
=  private Environment env;
...
logger.info("Puerto actual: " + env.getProperty("local.server.port"));
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 20. Utilizando Ribbon para balanceo de carga en RestTemplate

[PENDIENTE]

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 3: Eureka Server: registrando microservicios

#### 22. Creando servidor de registro Eureka

Vamos a crear un nuevo servicio que sera el servidor de
Eureka, en el cual ya no tendremos que indicar el url/puerto
ya solo se deberia de registrar el nuevo servicio y Eureka
se encarga de manejar el balancedor.

A. Creamos un nuevo proyecto con las dependencias "Eureka Server"
"SevTools"

B. En el .properties

```R
spring.application.name=carrito-eureka-server

## ya queda el 8761 por defecto pero lo explicitamos
server.port=8761

## auto eureka se registra como cliente, en este caso no queremos eso
eureka.client.register-with-eureka=false
eureka.client.fetch-registry=false
```

C. **Condicional** Agregamos la dependencia JAXB si estamos usando
el jdk11 o 12, si tenemos el 8 no ahi pex. en la [doc](https://cloud.spring.io/spring-cloud-static/Greenwich.SR2/single/spring-cloud.html#_jdk_11_support)
podemos ver detalles de esto.

D. habilitamos eureka server

```java
+  @EnableEurekaServer
=  @SpringBootApplication
=  public class CarritoEurekaServerApplication {
=  ...
```

![4_eureka_WGUI.PNG](./img/4_eureka_WGUI.PNG)
**Resultado:** Podemos ver en localhost:8761 la interface de Eureka Server.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 23. Conectando nuestros servicios como clientes Eureka

Vamos a registrar las 2 APIs que tenemos en el Eureka server

A. damos clickDer en *lineasFactura*  y *productos-app* -> Spring -> Edit Starter y
agregamos *Eureka Discovery Client* para habilitar este servicio
como cliente de Eureka. El cual se agregara *spring-cloud-starter-netflix-eureka-client*
en nuestro pom.

B. De forma auto. habilita el cliente, pero podemos hacerlo de
forma explicita en la clase principal.

```java
@EnableEurekaClient
public class ProductosAppApplication {...
```

C. En ambas APIs indicamos en .properties cual es nuestro
servidor Eureka indicando su ruta, para que estas APIs le
envien una señal comunmente llamada **latido de corazon**
cada 30seg a los 3 latidos se registra la nueva API y si a
los 3 latidos falla se da de baja la API el cual le primera
conexion enviara los datos como nombre de la maquina, ubicacion,
servicios etc. Y en adelante mandara una peticion cada X tiempo
para indicar que sigue vivo.

```properties
eureka.client.service-url.defaultZone=http://localhost:8761
```

D. En nuestro servicio de *lineaFactura* podemos
quetar la configuracion de Ribbon y sus dependencias
ya que Eureka lo gestiona de forma automarica,
D.1. Del *app....properties* quitamos el *listOfServers*
D.2. Del *pom.xml* quitamos *ribbon*.
D.3. Del *LineasFacturaApplication* quitamos *@RibbonClient*

**Nota:** Dejamos *@EnableFeignClients* para que Eureka
pueda configurarlo ya que usa este cliente Eureka.

E. Probamos: Primero arrancamos Eureka server y luego
las otras APIs

**Resultado:** En eureka podemos ver que se registran
las 2 instancias de *productos-app* y 1 de *factura-productos*
y el balanceo de carga ya que de forma automatica. Y por
podemos ver como consulta uno u otro.
![Eureka_Balanceador](./img/5_Eureka_Balanceador.gif)----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 24. Escalar microservcios con puerto dinámico

Vamos a hacer que nuestro servicios se configure el puerto de forma
automatica.

A. Vamos a *.properties* de *productos-app* donde asignamos un
puerto dinamico automatico disponible y segundo le asignamos el
nombre de aplicacion para que lo identifique Eureka

```properties
spring.application.name=productos-app
server.port=${PORT:0}
eureka.instance.instance-id=${spring.application.name}:${spring.application.instance_id:${random.value}}
```

**Resultado:** Podemos levantar las instancias que querramos sin
poner el puerto de forma manual, y aun asi estos se registraran
en aureka y los incluira dentro del baleanceador.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 25. Tolerancia de fallos y latencia con Hystrix

Esta erramienta implementa un patro llamado **circuit breaker** (corto circuito)
el cual evita errores en cascada que se pueden evitar, el cual lo hace a travez de
un hilo de ejecucion separado el cual maneja estos errores.

A. Agregamos el starter Hystrix en *lineas-factura* veremos en nuestro pom

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>
```

B. Vamos la clase principal y habilitamos el Circuit Braker

```java
@EnableCircuitBreaker
...
public class LineasFacturaApplication {
  public static void main(String[] args) {...}
}
```

C. A *productos-app* en el controlador agregamos una ex para que truene a proposito

```java
@GetMapping("/all")
List<Producto> all() throws Exception {
  if(true) { throw new Exception("Chin chin capulin"); }
```

D. Ahora en *lineas-facturas* app en el controlador que consume
el productos que hicimos que fallara agregamos

```java
@RestController @RequestMapping("/lineas-factura")
public class LineaFacturaController {

+  @HystrixCommand(fallbackMethod="metodoAlternativo")
   @GetMapping("")
   List<LineaFactura> findAll() { return lineaFacturaService.findAll(); }//linea que falla

+  List<LineaFactura> metodoAlternativo() {//metodo que se ejecuta si el anterior falla
+    List<LineaFactura> l = new ArrayList<LineaFactura>();
+    return l;
+  }
```

**Resultado:** En este caso simpre va a fallar el servicio, pero por
Hystrix ejecuta un segundo metodo, para evutar errores en cascada.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 26. Configurando timeout en Hystrix y Ribbon

Quitamos la exepcion de antes y ahora agregamos un sleep de tal
forma que el servicio proboque un timeout a proposito, para luego
nosotros poderlo manejar.

```java
public class ProductoController {
  @GetMapping("/all")
  List<Producto> all() throws Exception {
+    Thread.sleep(2000L)
```

Nos mandara un "Read timed out executing" o ejecuta el
el metodo alternativo si tenemos activada la anotacion de
*HystrixCommand*.

**Objetivo:** Aumentar el tiempo para que nos lanze el timeout
ya que actualmente esta en 1seg y el hilo se duerme por 2seg

A. Vamos a spring.io->Spring Cloud->Spring Cloud Netflix->Learn->
[_hystrix_timeouts_and_ribbon_clients](https://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/2.2.0.M2/reference/html/#_hystrix_timeouts_and_ribbon_clients)
podmeos encontrar que Hystrix embuelbe/anida a Ribbon por lo que
Hystrix debe de tener un tiempo de espera mayor que el de Ribbon.

Tambien podemos ver en [uploading_files_through_zuul](https://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/2.2.0.M2/reference/html/#_uploading_files_through_zuul)
las variables para mover el timeout, y lo pegamos en *lineas-facturas*, por
lo que explicamos es recomendable que el de arriba tenga mayor tiempo que el de
abajo, *recomendable* la suma de los 2 de abajo y pico

```properties
hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds: 10100
ribbon.ConnectTimeout: 3000
ribbon.ReadTimeout: 7000
```

**Resultado:** A pesar que el hilo duerme 2seg configuramos para que
diera tiempo sificiente para que alcanzara el timeout de 10seg, por
y en dado caso que pasara los 10seg se ejecutaria un metodo alternativo.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 27. Creando y configurando servidor Zuul API Gateway

Crearemos un nuevo servicio que actuara como la puerta principal de
nuestro proyecto. El cual su tarea mas importante es enrutar a todos
los servicios de forma dinamica, ya se integra con Ribbon de forma
automatica y tenemos opciones/filtros como configurar en esta parte Spring
Security, Metricas, Manejo de errores, etc.

A. Cremoas un nuevo proyecto con DevTools, Eureka Discobery Client, Web, Zuul
B. En la clase principal habilitamos la app como cliente de Eureka y a Zuul

```java
@EnableEurekaClient
@EnableZuulProxy
@SpringBootApplication
public class CarritoZuulServerApplication {
```

C. Configuramos nombre, puerto. Y tambien podemos configurar las
rutas de cada uno de nuestro servicios.

```properties
spring.application.name=carrito-zuul-server
server.port:8090

eureka.client.service-url.defaultZone=http://localhost:8761/eureka

zuul.routes.productos.service-id=productos-app
zuul.routes.productos.path=/api/productos/**

zuul.routes.lineas-factura.service-id=linea-factura
zuul.routes.lineas-factura.path=/api/lineas-factura/**
```

**Cuidado:** Con los espacios en blanco u comentarios delante del
igual de las propiedades ya que pueden tomarlas como parte del valor
y se chocolatea el asunto.

**Resultado:** Podemos consultar todos nuestro servicios tanto de
"productos-app" como de "lineas-facturas" desde el mismo puerto/ip.

```R
Productos
ANTES:   http://localhost:54294/ver/1
DESPUES: http://localhost:8090/api/productos/ver/1

Lineas/facturas
ANTES:   http://localhost:8002/lineas-factura
DESPUES: http://localhost:8090/api/lineas-factura/lineas-factura
```

Ambas rutas siguen funcionando pero el gateway nos permite desde un
solo punto consumir cualquiera.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 28. Zuul Filtro HTTP pre - Tiempo transcurrido

Como comentamos el una de las caracteristicas mas fuertes de Zuul son los
filtos, los cuales tenemos de 3 tipos

A. **pre**: este se ejecuta antes de que el request sea enrutado

B. **post**: Se ejecuta justo despues de que el request haya sido
enrutado. comunmente utilizado para manipular las cabeseras.

C. **route**: Se ejecuta durante el enrutado del request, aqui se
resuleve la ruta

**Objetivo:** En el filtro pre capturaremos el tiempo actual y en el post
tambien y luego sacaremos diferencia, para saber cuanto fue el tiempo que
tardo la peticion en resolverse.

A. Cremos el filtro pre

```java
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
class PreTimerFilter extends ZuulFilter{
  Logger logger = LoggerFactory.getLogger(PreTimerFilter.class);

  //INDICAMOS QUE EN TODAS LAS REQUEST SE EJECUTE EL FILTRO
  @Override public boolean shouldFilter() { return true; }

  @Override public Object run() throws ZuulException {
    //OBTENEMOS EL REQUEST
    RequestContext ctx = RequestContext.getCurrentContext();
    HttpServletRequest request = ctx.getRequest();

    //LE AGREGAMOS DATOS
    Long  tiempoInicio = System.currentTimeMillis();
    request.setAttribute("tiempo_inicio", tiempoInicio);

    logger.info(String.format("%s request enrutado a %s ", request.getMethod(), request.getRequestURL()));
    return null;
  }

  @Override public String filterType() { return "pre"; }

  @Override public int filterOrder() { return 1; }

}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 29. Zuul Filtro HTTP post - Tiempo transcurrido

Lo mismo, solo que capturamos la hora qur aignamos en el pre,
campturamos una nueva hora y mostramos la diferencia.

```java
@Component class PostTimerFilter extends ZuulFilter{
  @Override public boolean shouldFilter() { return true; }
  @Override public Object run() throws ZuulException {
    RequestContext ctx = RequestContext.getCurrentContext();
    HttpServletRequest request = ctx.getRequest();
    Long tiempoInicio = (Long)request.getAttribute("tiempo_inicio");
    Long tiempiFin = System.currentTimeMillis();
    Long tiempoTranscurrido = tiempiFin-tiempoInicio;
    logger.info(String.format("tiempo transcurrido: %s seg.", tiempoTranscurrido.toString()));
    return null;
  }
  @Override public String filterType() { return "post"; }
  @Override public int filterOrder() { return 1; }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 30. Configurando timeout en Zuul API Gateway

Si retornamos el sleep que antes teniamos y nos dara un "Gateway Timeout",
"com.netflix.zuul.exception.ZuulException: Hystrix Readed time out" por
lo que para evitar esto podemos copiar la configuracion que teniamos en
*lineas-facturas* en el properties.

```properties
hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds: 10100
ribbon.ConnectTimeout: 3000
ribbon.ReadTimeout: 7000
```

**Resultado:** Podmemos ver el tiempo que tardo la peticion en mseg.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 4: Spring Cloud Config Server: centralizando la configuración

#### 32. Creando el servidor de configuración

La conf en un ambiente grande podria convertirce en un problema, como
tener que sincronizar ambientes test, prod, contraseñas, puertos perso. etc
el cual para evitar esto podemos centralizar la configuracion.

A. Creamos un nuevo proyecto con "Config Server" y "DevTools". Y en la clase principal
habilitamos el servidor de configuracion.

```java
@EnableConfigServer
@SpringBootApplication
public class CarritoConfigServerApplication {
```

B. Vamos a app.properties

```properties
spring.application.name=carrito-config-server
#valor por defecto pero lo explicitamos
server.port:8888

spring.cloud.config.server.git.uri=file:///E:/cursos/0activ/web/micro-servicios/_config
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 33-34-35 Conectando servicio items al Servidor de Configuración

A. Agregamos un archivo servicio-items.properties en la ruta que declaramos en "...git.url"
y le damos commit

B. levantamos el server config y vamos a `http://localhost:8888/linea-factura/default`
desde postman, donde linea-factura es el nombre del archivo,

> **Error**: If you are using the git profile, you need to set a Git URI in your configuration.
> If you are using a native profile and have spring.cloud.config.server.bootstrap=true, you need to use a composite configuration.
> **Descipcion:** Al tratar de levantar el proyecto da este error.
> **Causa:** Al parecer solo deben de estar los archivos .properties y
> la carpeta .git, intente poner esto donde estaban todas mis proyecto
> pero saco esto.
> **Solucion:** Dejar solamente los archivos .properties y la carpeta .git

**Resultado:** Ya tenemos el servidor de configuraciones que al consultar
la url mensionada nos mostrara un json con las configuraciones que tenemos
en el .properties. **Nota:** Estamos consultando el */default* ya que en
este el entorno(test/prod/etc) pero por ahora solo tenemos una.
**Falta:** Poner nuestras instancias como que querramos conf. dende este
servidor habilitarlos como clientes de el cliente servidor.

A. Vamos a linea-facturas=item-factura para agregarlo como cliente, agregandole
la dependencia desde el starter de **config Client** de spring cloud, se agrega
el pom.xml el

```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-config</artifactId>
</dependency>
```

B. Al lado de el application.properties creamos *bootstrap.properties* y este
le agregamos el dato para que conecte con Server Config.

```properties
spring.application.name=servicio-items
spring.cloud.config.uri=http://localhost:8888
```

**Importante:** el proseso que hece es que
A.) al levantar el client Config este se conectara a server Config.
B.) toma las configuraciones de *bootstrap.properties*.
C.) Toma las configuraciones de *application.properties* pero si ya estan configuradas entonces ya no
las toma en cuenta, osea que las configuraciones de mayor importancia o el .properties que manda es bootstrap.
D.) se conectara a Eureka Server.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 36. Leer configuración desde controlador utilizando anotación @Value

A. Lo caprutamos como siempre ejempl: `@Value("${configuracion.texto}") private String servConfig;`

B. Levantamos el 1) Server Config, 2) Eureka 3) Servers Client

**Error:** No levantaba el ambiente por que mandaba un error de que no
encontraba la variable *configuracion.texto*.  
**Causa:** Le puse mal la ruta en el *bootstrap.properties* la deje
como la consultaba en postman y no es asi.  
`spring.cloud.config.uri=http://localhost:8888/servicio-items/default`.  
**Solucion:** Solo se debe de definir en *bootstrap.properties* el url
base`spring.cloud.config.uri=http://localhost:8888`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 37-38 Configurando los ambientes en el repositorio Git  Configurando profiles en servicio items

Vamos a crear mas de una configuracion en este caso para distintos ambientes.

A. En el mismo lugar donde tenemos el *servicio-items.properties* en mi caso en ./_config/
agregamos dos archivos, uno de desarrollo y otro de produccion.
A.1. servicio-items-develop.properties

```properties
configuracion.texto=Configuracion ambiente DEVELOP
```

A.2. servicio-items-PROD.properties

```properties
server.port=8007
configuracion.texto=Configuracion ambiente PRODUCTION
```

B. justo antes de *bootstrap.properties* de spring.cloud.config.uri agregamos el perfil
donde debe de tener lo que esta entre el guion y el punto, en este coso "develop" para
seleccionar el servicio-items-develop.properties, y en caso de que queramos
agregar el de prod seria "PROD".

```properties
=  spring.application.name=servicio-items
+  spring.profiles.active=develop
=  spring.cloud.config.uri=http://localhost:8888
```

C. Vamos al controlador de items y agregamos un codicional
para saber en que entorno estamos

```java
@Autowired
private org.springframework.core.env.Environment env;
...
 if(env.getActiveProfiles().length>0 && env.getActiveProfiles()[0].equals("develop") ) {
   log.info("ambiente de " + env.getActiveProfiles()[0].toUpperCase());
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 39. Actualizando cambios en configuraciones con @RefreshScope y Actuator

Como esta ahora debemos de reiniciar el Server Config para que tome los
cambios, esto ocurre por el controlador es un objeto tipo singleton, el
hace que los cambios perduran mientras esten la aplicacion corriendo.

Podemos hacer que los cambios que se realicen los cambios en tiempo real
podemos hacerlo con @RefreshScope

A. Vamos al controlador

```java
+ @RefreshScope
= @RestController
= public class ItemController {
```

B. Agregamos dependencia de "Spring Boot Actuator" y habilitamos
el endpoind refresh de actuator en *bootstrap.properties*

```properties
management.endpoints.web.exposure.include=*
```

C. Levantamos de nuevo item-factura y agregamos cambios mas el commit, y aun
NO veremos el cambio en item-factura.

D. Hacemos peticion y nos dara los campos que cambiamos

```R
POST - http://localhost:8005/actuator/refresh

e.g. response
[
    "config.client.version",
    "configuracion.texto"
]
```

E. **Resultado** Consultamos otra vez item-factura y podremos ver que el cambio
ya esta se puede ver en item-factura. **Importante**: Nopodemos hacer cambios
propios de spring como el nombre de la app, o acceso a BD cosas de ese tipo
por que son cosas que hagarra al levantar, pero algunas otras y todas aquellas
var que nosotros hagamos si se puede.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 40. Creando repositorio remoto GitHub y push

Lo mismo pero solo que le pegamos la url de nuestro repo incluyengo el .git

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 5: CRUD: consumiendo nuestras APIs con Feign y Rest Template

#### 42. Creando Service para el CRUD y handler POST en controlador servicio productos

Vamos a implementar el CRUD de productos

A. Creamos interface e impl de CRUD

```java
@Service public class ProductoServiceImpl implements IProductoService{
 @Override
 @Transactional
 public Producto save(Producto p) {
    return productoDao.save(p);
 }

 @Override
 @Transactional
 public void deleteById(Long id) {
   productoDao.deleteById(id);
 }
```

B. Creamos los controladores.

```java
  @PostMapping("/crear")
  @ResponseStatus(HttpStatus.CREATED)
  public Producto crear(@RequestBody Producto p) {
    boolean a = false;
    if(a) { System.out.println("Que pedo"); }

    return productoService.save(p);
  }
```

C. Probamos levantando 1) Eureka, 2) Productos 3) Zuul

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### 43. Implementando handlers PUT y DELETE en controlador de servicio productos

```java
  @PutMapping("/editar/{id}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  Producto actualizar(@RequestBody Producto p, @PathVariable Long id) {
    Producto toE = productoService.findById(id);
    toE.setNombre(p.getNombre());
    toE.setPrecio(p.getPrecio());
    return productoService.save(toE);
  }
  @PutMapping("/eliminar/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void delete(@PathVariable Long id) {
    productoService.deleteById(id);
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 44. Consumiendo API Rest con RestTemplate desde servicio items

Consumiremos el CRUD que creamos desde item-service

A. Vamos a *ItemService*

```java
public class ItemServiceImpl implements ItemService {
  @Override public Producto save(Producto productoIn) {
    HttpEntity<Producto> body = new HttpEntity<Producto>(productoIn);
    ResponseEntity<Producto> response = clienteRest.exchange("http://servicio-productos/crear", HttpMethod.POST, body, Producto.class);
    Producto productoOut = response.getBody();
    return productoOut;
  }

  @Override
  public Producto update(Producto productoIn, Long id) {
    Map<String, String> pathVariables = new HashMap<String, String>();
    pathVariables.put("id", id.toString());
    HttpEntity<Producto> body = new HttpEntity<Producto>(productoIn);
    ResponseEntity<Producto> response = clienteRest.exchange("http://servicio-productos/editar/{id}",
        HttpMethod.PUT, body, Producto.class, pathVariables);
    Producto productoOut = response.getBody();
    return productoOut;
  }

  @Override
  public void delete(Long id) {
    Map<String, String> pathVariables = new HashMap<String, String>();
    pathVariables.put("id", id.toString());
    clienteRest.delete("http://servicio-productos/eliminar/{id}", pathVariables);
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 45. Implementando handler CRUD en controlador de servicio items

Vamos al controlador de item-factura

```java
@PostMapping("/crear")
  @ResponseStatus(HttpStatus.CREATED)
  public Producto crear(@RequestBody Producto productoIn) {
    return itemService.save(productoIn);
  }
  @PutMapping("/editar/{id}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  Producto actualizar(@RequestBody Producto p, @PathVariable Long id) {
    return itemService.update(p, id);
  }
  @DeleteMapping("/eliminar/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void eliminar(@PathVariable Long id) {
    itemService.delete(id);
  }
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 46. Probando operaciones CRUD en servicio items con postman

A. Como hicimos este proceso con el RestTemplate vamos a cambiar la impl
del service que estamos usando en el controlador

```java
=  @Autowired
##  //@Qualifier("serviceFeign")
+  @Qualifier("serviceRestTemplate")
=  private ItemService itemService;
```

<!-- Como levantar microservicios, como levantar microservicios, orden para levantar microservicios,
orden para arrancar microservicios, orden para arrancar micro servicios, orden levantar microservicios,
como probar microservicios -->
B. Probamos Levantando 1) Config Server 2) Eureka 3) productos 4) items 5)Zuul
y en postman podemos desde `http://localhost:8090/api/items/crear`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 47. Consumiendo API Rest con cliente Feign

A. Vamos a *ProductoClienteRest* y creamos consumimos los nuevos
metodos de Producto.

```java
= @FeignClient(name = "servicio-productos")
= public interface ProductoClienteRest {
+   @PostMapping("/crear")
+   Producto crear(@RequestBody Producto p);
+  @PutMapping("/editar/{id}")
+  Producto editar(@RequestBody Producto p, @PathVariable Long id);
+  @DeleteMapping("/eliminar/{id}")
+  void eliminar(@PathVariable Long id);
```

B. Vamos a *ItemServiceFeign*

```java
  @Override
  public Producto save(Producto productoIn) {
    return clienteFeign.crear(productoIn);
  }
  @Override
  public Producto update(Producto productoIn, Long id) {
    return clienteFeign.editar(productoIn, id);
  }
  @Override
  public void delete(Long id) {
    clienteFeign.eliminar(id);
  }
```

B. Cambiamos la impl del service en el controlador

```java
=  @Autowired
+  @Qualifier("serviceFeign")
##  //@Qualifier("serviceRestTemplate")
=  private ItemService itemService;
```

----

----

----

### Sección 6: Servicio commons: reutilizando librerías

#### 49. Creando librería commons y generando jar usando maven

**Objetivo**: Como vemos en Entity Producto tenemos repetido tanto en producto
como en item-producto, vamos a crear un proyecto separado para que lo compartan
y asi solo tengamos una.

A. Creamos un proyecto con las dependencias de JPA y de H2 database
B. Vamos a *CarritoCommonsApplication* y le quitamos el main, ya que no levanta
ninguna app, solo es una dependencia.
C. Vamos al pom y qutamos

```xml
<build><plugins><plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
</plugin></plugins></build>
```

D. Copiamos Producto del proyecto productos y lo pegamos en
un nuevo paquete del commons

D. Generamos el jar y lo subimos a nuestro repo `bash mvnw.cmd install`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 50. Modificando servicio productos para agregar dependencia commons

Podemos no depender de H2 en nuestro commons
A. Lo quitamos de nuestro pom

```java
+ @EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
= @SpringBootApplicationpublic class CarritoCommonsApplication { }
```

B. Vamos al pom y agregamos dependencia a proyectos item y productos

```xml
<dependency>
  <groupId>com.marioCorps.carrito-commons</groupId>
  <artifactId>carrito-commons</artifactId>
  <version>0.0.1</version>
</dependency>
```

C. Cambiamos el uso de Producto de proyecto local(item o producto) por
el del proyecto commons

D. Para que El proyecto de productos mapee el Entity a la BD en este
caso a H2, tenemos que agregar en la clase principal la ruta de este
para que lo agrege en su contexto y lo mapee

```java
+ @EntityScan("com.marioCorps.carritocommons.model")
= public class SpringbootServicioProductosApplication
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 51. Modificando servicio items y probando con Postman

Lo mismo solo agregamos dependecia al commons en el pom, eliminamos
producto que tenia y hacemos referencia al de commons.

**Error:** Me salio en item al levantarlo de que no podia configurar el
data source.  
**Solucion:** le agrege el
`@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})` y
ya dejo de dar lata

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

### Sección 7: Spring Cloud Security: implementando OAuth2 y JWT

#### 53. Introducción a JSON Web Token (JWT)

[YA_LO_SE]

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 56. Creando nuestro microservicio usuarios

Vamos a tener un M.S. encargado de los usuarios como su CRUD, y otro
M.S. encargado de la auth. el cual si las credenciales esta bien otorgara
el token.

Zuul sera desde donde aplicaremos la logica de seguridad y tambien usaremos
el Server Config para obtener la clave secreta y la firma.

![6_diagrama_auth_zuul_clientes](./img/6_diagrama_auth_zuul_clientes.PNG)

A. Cremos el proyecto de clientes con las dependencioas de "Eureka Descobery Client"
"Developer Tools", "Spring Web", "Spring JPA", "H2 DataBase"

B. del proyecto clientes vamos al .properties

```properties
spring.application.name=carrito-clientes
#puerto automatico
server.port=${PORT:0}

#generacion de instancia auto
eureka.instance.instance-id=${spring.application.name}:${spring.application.instance_id:${random.value}}

#comunicacion con eureka
eureka.client.service-url.defaultZone=http://localhost:8761/eureka

#Para que se muestren los logs del SQL
logging.level.org.hibernate.SQL=debug
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 57 - 58 Creando las entidades necesarias Usuario y Role / Creando la relación de muchos a muchos entre Usuario y Role

El siguiente diagrama muestra una relacion muchos a muchos entre
usuarios y roles, es decir "un usuario puede tener muchos roles,
como un rol puede tener muchos usuarios".
![7_diagrama_ER_usuarios_roles](./img/7_diagrama_ER_usuarios_roles.PNG)

A. Creamos las entidades y sus propiedades
B. Creamos sus relaciones

```java
@Entity
@Table(name="usuarios")
public class Usuario implements Serializable {
  private static final long serialVersionUID = 8262498264201684554L;

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  @Column(unique=true, length=20)
  private String usuario;

  @Column(length=60)
  private String password;

  private String nombre;
  private String apellido;

  @Column(unique=true, length=100)
  private String email;

  private boolean enabled;

  //Creara una tabla intermedia con los ID de usr y rol
  @ManyToMany(fetch = FetchType.LAZY)
  //para que no se dupliquen los permisos
  //llave compuesta
  @JoinTable(uniqueConstraints = { @UniqueConstraint(columnNames={"usuario_id", "roles_id"})})
  private List<Rol> roles;

  //getters/setters
```

Entidad Rol, Si nos fijamos no ahi ninguna relacion aqui
con usuarios ya que no tiene sentido a partir de un rol
obtener su usuario. Pero se podria hacerlo con el uso de
"mapedBy" en ManyToMany

```java
@Entity
@Table(name="roles")
public class Rol implements Serializable  {
  private static final long serialVersionUID = 6457998280669502289L;

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  @Column(unique=true, length=20)
  private String nombre;
```

**Resultado:** Si levantamos podremos ver como se crean las
tablas usuarios, roles y usuarios_roles, con sus constrains.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 59. Creando el repositorio JPA para los usuarios

```java
public interface UsuarioDao extends PagingAndSortingRepository<Usuario, Long>{
  public Usuario findByUsuario(String username);
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 60. Implementando un repositorio rest para el CRUD

Vamos a crearlo a partir de una nueva dependencia que nos permite
crear el controlador de forma automatica.

A. edit Starters -> **Rest Repositories**

```java
+ @RepositoryRestResource(path="usaurios")
= public interface UsuarioDao extends PagingAndSortingRepository<Usuario, Long>{
```

C. Agregamos en Zuul nuestras nueva api

```properties
zuul.routes.clientes.service-id=carrito-clientes
zuul.routes.clientes.path=/api/**
```

D. Agregamos usuarios

```sql
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('andres','$2a$10$ykhXmCAam5FUEF9GN.4Z8OwwWJidvMii6VFYe77cmS2X6oF6p4W86',1, 'Andres', 'Guzman','profesor@bolsadeideas.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$qGyDfZLBB.SgLv7GCP3uZe3oM38fVtr58T1iZ1LNOvO61loNUAAaK',1, 'John', 'Doe','jhon.doe@bolsadeideas.com');

INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);
```

**Resultado:** Podremos consultar `http://localhost:8090/api/usaurios` el cual nos dara
una coleccion de usuarios en un estardart **HATEOAS** (Hypermedia as the Engine Of Application State)
o Hipermedia como motor del estado de la aplicacion, el cual aparte de la infortmacion requerida
tambien se proporcionan links como indicando como navegar a travez de la API

```json
{
  "_embedded": {
    "usuarios": [
      {
        "username": "andres",
        "password": "$2a$10$ykhXmCAam5FUEF9GN.4Z8OwwWJidvMii6VFYe77cmS2X6oF6p4W86",
        "nombre": "Andres",
        "apellido": "Guzman",
        "email": "profesor@bolsadeideas.com",
        "enabled": true,
        "roles": [ { "nombre": "ROLE_USER" } ],
        "_links": { "self": { "href": "http://192.168.1.72:49518/usaurios/1" }, "usuario": { "href": "http://192.168.1.72:49518/usaurios/1" } }
      },
      {
        "username": "admin",
        "password": "$2a$10$qGyDfZLBB.SgLv7GCP3uZe3oM38fVtr58T1iZ1LNOvO61loNUAAaK",
        "nombre": "John",
        "apellido": "Doe",
        "email": "jhon.doe@bolsadeideas.com",
        "enabled": true,
        "roles": [ { "nombre": "ROLE_USER" }, {"nombre": "ROLE_ADMIN" }],
        "_links": { "self": { "href": "http://192.168.1.72:49518/usaurios/2" }, "usuario": { "href": "http://192.168.1.72:49518/usaurios/2" }}}]
  },
  "_links": {
    "self": { "href": "http://192.168.1.72:49518/usaurios{?page,size,sort}", "templated": true },
    "profile": { "href": "http://192.168.1.72:49518/profile/usaurios" },
    "search": { "href": "http://192.168.1.72:49518/usaurios/search" }
  },
  "page": { "size": 20, "totalElements": 2, "totalPages": 1, "number": 0 }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 61. Exportar métodos repositorio rest con endpoint search

Con la implementacion `@RepositoryRestResource(path="usaurios")` tenemos
el CRUD creado segun la convecion de una API REST-FULL, pero los metodos que
implementamos lo hacemos a travez del nombre del metodo. por ejemplo.

`http://localhost:8090/api/usaurios/search/findByUsername?username=andres`

Pero si queremos personalizar el nombre lo hacemos con.

```java
@RepositoryRestResource(path="usaurios")
public interface UsuarioDao extends PagingAndSortingRepository<Usuario, Long>{
  @RestResource(path="filtrar")
  public Usuario findByUsername(@Param("nick") String username);
}
```

Ahora para usar el metodo usamos
`http://localhost:8090/api/usaurios/search/filtrar?nick=andres`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 62. Configurando y exponer id en la respuesta json

Como vemos en el json de la respuesta puesto anteriormente este no
tiene lo que es el id, por lo que tendremos que configurar que se
muestre.

```java
@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {
  @Override
  public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    config.exposeIdsFor(Usuario.class, Rol.class);
  }
}
```

**Rsultado:** Si probamos de nuevo podremos ver el id del usuario y del rol

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 63. Creando librería commons usuarios

A futuro vamos a reutilizar Rol asi que lo ponemos en nuestro proyecto de commons
A. Igual que hicimos antes

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 65. Creando microservicio oauth (Servidor de Autorización)

Vamos a crear un nuevo M.S. para la gestion del token.

A. Creamos un nuevo proyecto con las dependencias "DevTools", "Eureka D. Client",
"OpenFeign" "Cloud OAuth2", "Web Starter"

B. pasamos la dependencia de commons a oauth, pero este trae con sigo la
dependencia de JPA el cual va a pedir una configuracion del DataSource una
de las razones por las que hemos exluido esta configuracion.
Pero para mas elegante excluimos la dependencia directamente en el pom

```xml
<dependency>
  <groupId>com.marioCorps.carrito-commons</groupId>
  <artifactId>carrito-commons</artifactId>
  <version>0.0.1</version>
  <exclusions>
    <exclusion>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
    </exclusion>
  </exclusions>
</dependency>
```

C. Habilitamos el Eureka Client en **oauth**

```java
+ @EnableEurekaClient
= @SpringBootApplication public class CarritoOautApplication {
```

D. configuramos el nombre y el puerto

```properties
spring.application.name=carrito-oauth
server.port=9111
eureka.client.service-url.defaultZone=http://localhost:8761/eureka
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 66. Creando nuestra interfaz Feign Client

A. Habilitamos el Feing Client

```java
+ @EnableFeignClients
= public class CarritoOautApplication {
```

B. Creamos el httpClient

```java
@FeignClient(name="carrito-clientes")
public interface UsuarioFeingClient {
  @GetMapping("/usaurios/search/filtrar")
  Usuario findByUsername(@RequestParam String nick);
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 7. Implementando servicio UserDetailsService con Feign Client

Vamos a implementar el servicio encargado de la auth

A. Creamos la clase service en *auth* de usuaros.
  A.1. implementamos de UserDetailsService que sirve para la auth
  independiente del sourceData en este caso a travez de M.S.  
  A.2. Sobre escribimos el metodo `findByUsername`.  
  A.3. Obtemnemos el cliente con `findByUsername` y si no lo
  encuentra mandamos excepcion.
  A.4. Obtenemos los autorities del usuario pero los casteamos
  de tipo *GrantedAuthority* para pasarselos a User.
  A.5. Retornamos un User de Spring Security.

```java
package com.marioCorps.carritooaut.service;
import org.springframework.security.core.*...;
@Service public class UsuarioService implements UserDetailsService {
  @Autowired private UsuarioFeingClient usrClinetHttp;
  Logger logger = LoggerFactory.getLogger(UsuarioService.class);

  @Override public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    //obtenemos
    Usuario usuario = this.usrClinetHttp.findByUsername(username);
    if(usuario == null) { throw new UsernameNotFoundException("Error en el login"); }
    logger.info("Usuario Auth: " + usuario.getNombre());
    //casteamos
    List<GrantedAuthority> auths = usuario.getRoles().stream()
        .map(role -> new SimpleGrantedAuthority(role.getNombre()))
        .peek(auth -> logger.info("Auth: " + auth.getAuthority()) )
        .collect(Collectors.toList())
        ;
    //casteamos/damos a Spring Security
    return new User(usuario.getUsername(), usuario.getPassword(),
        usuario.isEnabled(), true, true, true, auths);
  }

```

**Resultado:** Esta clase la vamos a utilizar en la configuracion de OAuth

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 68. Añadiendo la clase SpringSecurityConfig y registrando UserDetailsService

A. Creamos la configuracion principal de Spring Sec,
A.1. Creamos una clase que extienda de *WebSecurityConfigurerAdapter*
A.2. Inyectamos el Service que creeamos anteriormente que es un *UserDetailsService*
pero nuestra impl la llamamos *UsuarioService*.
A.3. Lo inyectamos en *userDetailsService*.
A.4. Creamos en encriptador con *passwordEncoder()* registrado como bean y
lo regitramos (*Bean*) en el contexto para luego usarlo.
A.5. Registramos nuestro auth manager y lo registramos en el contexto
con *authenticationManager*.

```java
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserDetailsService usuarioService;

  @Override
  @Autowired
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(this.usuarioService)
      .passwordEncoder(this.passwordEncoder());
  }

  @Bean
  private BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Override
  @Bean
  protected AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 69-70. Añadiendo la configuración para el servidor de autorización en servicio oauth - Añadiendo configuración de los clientes en el Servidor de Autorización

Ya configuramos lo que es la configuracion de Spring Sec
como obtiene los datos, como los encripta y damos a disposicion
objetos para usarlo en otros lados.

Ahora vamos a configurar lo que es del token como su generacion
su validacion etc urilizando el `AuthenticationManager` que
registramos antes.

A. Creamos una clase anotada como bean de configuracion y
que extienda de `AuthorizationServerConfigurerAdapter`

B. inyectamos 2 atributos el `BCryptPasswordEncoder` y el
`AuthenticationManager` ambos creados anteriormente.

C. Sobre escribimos los 3 metodos de la superclase ahora
nos concentraremos en el AuthorizationServerEndpointsConfigurer
donde configuraremos que datos se gueardaran en el token.
C.1. Registramos el AuthenticationManager.
C.2. declaramos el token converter accessTokenConverter() el
cual es importante que este registrado como publico.
C.3. Asigansmos codigo secreto en mi caso "mi_codigo_secreto_m1a2r3i4o5".
C.4. Generamos el tokenStorage que guardara/generara el token `tokenStore()`.

D. Vamos a agregar las credenciales de nuestro clientes en `configure(ClientDetailsServiceConfigurer clients)`,
en este caso obtentendremos la configuracion de nuestros clientes a partir de
memoria por eso `inMemory` pero **podriamos cambiar a obtenerlo de una bd o etc**.
D.1. Con withClient y secret agregamos las credenciales.
D.2. con authorizedGrantTypes indicamos que podran obtener el
token a travez del password y que este se refrescara.

E. Configuramos en `configure(AuthorizationServerSecurityConfigurer)`
para darle seguridad a los endpoints.

E.1. en *permitAll()* Agregamos rutas como el login

```java
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
  @Autowired private BCryptPasswordEncoder passEncoder;
  @Autowired private AuthenticationManager authenticationManager;

  @Override public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
    security
      .tokenKeyAccess("permitAll()")
      .checkTokenAccess("isAuthenticated()");
  }

  @Override public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.inMemory()
      .withClient("carrito-front-web")
      .secret(passEncoder.encode("mario"))
      .scopes("read", "write")
      .authorizedGrantTypes("password", "refresh_token")
      .accessTokenValiditySeconds(3600) //1hr
      .refreshTokenValiditySeconds(3500);
      // SI QUEREMOS AGREGAR OTRO CLIENTE
      // .and().withClient("carrito-front-arduino").secret("mario2").scopes("read").authorizedGrantTypes("password", "refresh_token").accessTokenValiditySeconds(3600).refreshTokenValiditySeconds(3500);
  }

  @Override public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints.authenticationManager(authenticationManager)
        .tokenStore(tokenStore())
        .accessTokenConverter(accessTokenConverter()) ;
  }

  @Bean public JwtTokenStore tokenStore() { return new JwtTokenStore(accessTokenConverter()); }

  @Bean public JwtAccessTokenConverter accessTokenConverter() {
    JwtAccessTokenConverter jwtConv = new JwtAccessTokenConverter();
    jwtConv.setSigningKey("mario");
    return null;
}  }

```

F. Creamos hashs para ponerlos en la BD a travez del CommandLineRunner y el bcrypt.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 71. Agregando rutas de Spring Security a Zuul Probando la autenticación con Postman y obteniendo el token JWT

A. Vamos a Zuul a sus .propeties
B. Ademoas **desactivamos la cookies** en zuul que lo requiere para que funcione la auth

```properties
zuul.routes.oauth.service-id=carrito-oauth
zuul.routes.oauth.path=/api/**
zuul.routes.oauth.sensitive-headers=Cookie,Set-Cookie
```

C. Probamos

```R
POST - http://localhost:8090/api/oauth/token

Auth
  Username: carrito-front-web
  password: mario

Headers
  Content-Type: application/x-www-form-urlencoded

Body
  username:   admin
  password:   12345
  grant_type: password
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 72. Añadiendo más información al token JWT

para esto usaremos lo que es un Token Enhancer=potenciador que permite
agregar informacion.
A. Cremos el obj con la nueva info. como componente/Bean para poderlo
paraser por inyeccion.

```java
@Component
public class InfoAddicionalToken implements TokenEnhancer {
  @Autowired private IUsuarioService usuarioService;  
  @Override public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
    Usuario usuario = usuarioService.findByUsername(authentication.getName())
    Map<String, Object> info = new HashMap<String, Object>();
    info.put("nombre", usuario.getNombre());
    info.put("apellido", usuario.getApellido());
    info.put("email", usuario.getEmail());
    ((DefaultOAuth2AccessToken)accessToken).setAdditionalInformation(info);
    return accessToken;
}  }
```

B. Se lo agregamos al token

```java
= @Override
= public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
+  TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
+  tokenEnhancerChain.setTokenEnhancers(Arrays.asList(infoAddicionalToken, accessTokenConverter()));  
=  endpoints
=      .authenticationManager(authenticationManager)
=      .tokenStore(tokenStore())
=      .accessTokenConverter(accessTokenConverter())
+      .tokenEnhancer(tokenEnhancerChain);
```

**Resultado**: Si obtenemos el token (oauth/token) y lo checamos en
[jwt.io](https://jwt.io/) podremos comprobar que nuestro token tiene info
adicional.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 73-74. Configurando Zuul como Servidor de Recurso - Probando autorización y accesos a recursos protegidos

En este caso Zuul va a ser el que proteger nuestros recursos
validando el token los roles etc.

A. Agregamos dependencia (proyecto/clickDer->Spring->Edit Starters)
-> Spring Cloud Security -> Cloud OAuth2.

B. Cremos una clase de configuracion y habilitamos el ResourceServer
que extienda de ResourceServerConfigurerAdapter.

C. Sobreescribimos 2 metodos 1 para proteger las rutas y otro para
configurar el token igual que como lo tenemos en el proyecto oauth
C.1. Compiamos los beans **JwtTokenStore** y **JwtAccessTokenConverter**

package com.formacionbdi.springboot.app.zuul.OAuth2;

```java
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
  @Bean public JwtTokenStore tokenStore() { return new JwtTokenStore(accessTokenConverter()); }
  @Bean public JwtAccessTokenConverter accessTokenConverter() { JwtAccessTokenConverter jwtConv = new JwtAccessTokenConverter(); jwtConv.setSigningKey("mario"); return jwtConv; }

  @Override public void configure(ResourceServerSecurityConfigurer resources) throws Exception { resources.tokenStore(tokenStore()); }


  @Override public void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
    //todos pueden preguntar por el token con el metodo POST y todos
        .antMatchers("/api/oauth/**").permitAll()
    //todos pueden pueden ver las listas de x recurso  
        .antMatchers(HttpMethod.GET, "/api/productos/listar", "/api/items/listar", "/api/usuarios/listar", "/api/usaurios", "/api/productos/{id}", "/api/items/{id}/cantidad/{cantidad}", "/api/usuarios/{id}")
          .permitAll()
    //los Anmin y los users pueden ver los detalles de X recurso
        .antMatchers(HttpMethod.GET, "/api/productos/{id}", "/api/items/{id}/cantidad/{cantidad}", "/api/usuarios/{id}")
          .hasAnyRole("ADMIN", "USER")
    //solo los Admin pueden crear
        .antMatchers(HttpMethod.POST, "/api/productos/crear", "/api/items/crear", "/api/usuarios")
          .hasAnyRole("ADMIN")
    //los Anmin y los users pueden modificar
        .antMatchers(HttpMethod.PUT, "/api/productos/{id}", "/api/items/{id}/cantidad/{cantidad}", "/api/usuarios/{id}")
          .hasAnyRole("ADMIN")
    //los Anmin y los users pueden modificar
        .antMatchers(HttpMethod.DELETE, "/api/productos/{id}", "/api/items/{id}/cantidad/{cantidad}", "/api/usuarios/{id}")
          .hasAnyRole("ADMIN");
    //PODRRIAMOS SIMPLIFICAR CON
    //.antMatchers("/api/productos/**", "/api/items/**", "/api/usuarios/**").hasAnyRole("ADMIN");
  }
}
```

**Resultado:** Si probamos en postman tenemos las reglas tal como las
definimos.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 75. Creando configuración de OAuth en el Servidor de Configuración

<!-- agregar Config client tomar configuraciones del config server, tomar
variable desde el config server, asignar configuracion desde el config server-->
Crearemos variables de configuracion en el Server Config como son los
secrets, id de aplicaciones, etc..

A. Creamos un archivo *application.properties* para que se conparta
con todos aquellas APIs que esten conectadas con este, y le damos commit

```properties
config.security.oauth.client.id=carrito-front-web
config.security.oauth.client.secret=mario

config.security.oauth.jwt.key=mario
```

B. Agregamos dependencia/starter sobre el proyecto *oauth*
y *zuul* "Config Client" de spring cloud.

C. Copiamos el bootstrap.properties de *item* y lo pasamos
a *zuul* y a *oauth*, para aobechar la conf como el de coneccion
con el config server, solo cambiamos el nombre de la app.

D. Sustituimos la configuracion que damos en duro, por las variables que
recien creamos.

```java
-   //jwtConv.setSigningKey("mario");
+   jwtConv.setSigningKey(env.getProperty("config.security.oauth.jwt.key"));
```

E. Si queremos que la configuracion cambie desde actuactor anotamos.

```java
+ @RefreshScope
= public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 76. Revisando el token de actualización

Podemos hacer un refresh de nuestro token usando el token_refresh que nos
da al obtener el token de accesso.

con esto podiramos por ejemplo implementar una logica en nuestro cliente
que cuando este cerca de caducar nuestro token y el usuario a estado
teniendo actividad en la app este lo renueve, para que el usuario no tenga
que hacer login de nuevo mientras tenga actividad en la app.

```r
url
  POST - http://localhost:8090/api/oauth/token

headers:
  Content-Type: application/x-www-form-urlencoded

body
  grant_type:   refresh_token
  refresh_token:   {refresh_token} // el refresh_token entregado cundo se obtuvo el token
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 77. Configurando Cors en Spring Security OAuth2

A. vamos *zuul* -> ResourceServerConfig -> configure(HttpSecurity) donde tenemos
nuestras rutas. en el cual pondremos que dominios, metodos, headers pueden entrar.

```java
=  @Override
=  public void configure(HttpSecurity http) throws Exception {
=  ...  
+    .and().cors().configurationSource(configurationSource());
=  }

+ public CorsConfigurationSource configurationSource() {
+  CorsConfiguration corsConfig = new CorsConfiguration();
+  corsConfig.setAllowedOrigins(Arrays.asList("*"));
+  corsConfig.setAllowedHeaders(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
+  corsConfig.setAllowCredentials(true);
+  corsConfig.setAllowedHeaders(Arrays.asList("Authorization", "Content-type"));
+  
+  UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
+  source.registerCorsConfiguration("/**", corsConfig);
+  
+  return source;
+ }
```

B. ahi mismo agregamos un filtro para toda la app

```java
@Bean
public FilterRegistrationBean<CorsFilter> corsFilter() {
  FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(
      new CorsFilter(configurationSource())
  );
  bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
  return bean;
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 78. Manejando eventos de éxito y fracaso en la autenticación

Podemos agregar eventos que pasen despues o antes de que ocurra
un error o un success del login.

A. Creamos una clase en *auth* que quede registrada en el contexto que
implemente AuthenticationEventPublisher, donde en ambos podemos
obtener el usuario que quiere o se auth.

```java
package com.marioCorps.carritooaut.OAuthConfig;
@Component
public class AuthenticationEventHandler implements AuthenticationEventPublisher {
  Logger logger = LoggerFactory.getLogger(AuthenticationEventHandler.class);

  @Override
  public void publishAuthenticationSuccess(Authentication authentication) {
    UserDetails user = (UserDetails)authentication.getPrincipal();
    logger.info("Success Usuario authenticado: " + user);
  }

  @Override
  public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
    logger.error("Error al autentificarse: " + exception.getMessage());
  }
}
```

B. Registramos el componente en Spring Security, en *auth* -> SpringSecurityConfig

```java
+  @Autowired
+  AuthenticationEventPublisher authEvent;

=  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
...
+    .and().authenticationEventPublisher(authEvent)
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 79-80. Implementando evento tres intentos en el login - parte 1 y parte 2

Implementaremos el clasico de que en cuanto un cliente pase de los tres intentos
este se desabilite por temas de seguridad.

A. Agregamos en *commons* en el modelo Usuarios intentos

```java
= public class Usuario implements Serializable {
+  private Integer intentos;
```

A.2. Recompilamos para que tome los cambios los demas proyectos y actualizamos
dependencas en *oauth*, *usuarios*, *commons*.
<!-- maven saltar test en maven, maven NO ejecutar test en maven, maven evitar test en maven-->

```bash
mvn clean install -DskipTests
```

B. Agregamos consumo para agregar numero de intentos
B.1. En *oAuth* en UsuarioFeingClient

```java
=    public interface UsuarioFeingClient {
+        @PutMapping("/usaurios/{idUsuario}")
+        Usuario update(@RequestBody Usuario usuario, @PathVariable Long idUsuario);
```

D. Este mismo metodo lo pasamos a la Interface del service y lo implementamos

```java
+  @Service public class UsuarioService implements UserDetailsService, IUsuarioService {
=    @Override public Usuario update(Usuario usuario, Long idUsuario) {
=      return this.usrClinetHttp.update(usuario, idUsuario);
=    }
```

E. Inyectamos el service en nuestro AuthenticationEventPublisher

```java
= public class AuthenticationEventHandler implements AuthenticationEventPublisher {
+   @Autowired
+   private IUsuarioService usrService;
```

D. **Aplicamos logica**, de traemos usuario de la BD, obtenemos su numero de intentos,
lo sumamos en uno y lo guardamos y si es mayor a 3 entonces lo bloqueamos, y al final
guardamos en bdm los cambios.

```java
  @Override
  public void publishAuthenticationSuccess(Authentication authentication) {
    UserDetails user = (UserDetails)authentication.getPrincipal();
    logger.info("Success Usuario authenticado: " + user.getUsername());

    String username = authentication.getName();
    Usuario usr = this.usrService.findByUsername(username);
    if(usr.getIntentos() == null) { usr.setIntentos(0); }

    if(usr.getIntentos() > 0) {
      usr.setIntentos(0);
      logger.info(String.format("Numero de intentos de %s reiniciado", username));
      this.usrService.update(usr, usr.getId());
    }
  }

  @Override
  public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
    logger.error("Error al autentificarse: " + exception.getMessage());

    //CONSEGUIMOS USUARIO SI EXISTE A PARTIR DEL USERNAME
    String username = authentication.getName();
    Usuario usr = null;
    try { usr = this.usrService.findByUsername(username); }
    catch (FeignException e) { logger.error(String.format("El usuario %s no existe", username)); return ; }
    if(usr.getIntentos() == null) { usr.setIntentos(0); }

    //SE REGISTRA INTENTO FALLIDO
    usr.setIntentos(usr.getIntentos()+1);
    logger.warn(String.format("El usuario %s hizo intento %s de 3", username, usr.getIntentos()));

    //SI >=3 DE INTENTOS DESHABILITAMOS USUARIO Y GUARDAMOS
    if(usr.getIntentos() >= 3) {
      usr.setEnabled(false);
      logger.error(String.format("El usuario %s fue inabilitado por numero de intentos", username));
    }

    //actualizamos
    this.usrService.update(usr, usr.getId());

  }
```

**Resultado:** Si hacemos 3 intentos fallidos el usuario se bloquera y al
cuarto intento se mostrara un desabilidato.
![8_bloqueo_usr_por_3_intentos_fallidos](./img/8_bloqueo_usr_por_3_intentos_fallidos.gif)----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 81. Manejando error 404 en componente de servicio UserDetailsService

Cambiamos el modo que se maneja la excepcion en *outh* en UsuarioService
ahora con un try-catch

```java
public class UsuarioService implements UserDetailsService, IUsuarioService {
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    try { usuario = this.usrClinetHttp.findByUsername(username); }
    catch (FeignException e) { throw new UsernameNotFoundException("Error en el login"); }
```

**Commit:** gitlab.com/MarioM51/hola-micro-servicios/commit/c45feb5cb87632ae0e397f47011215a523e7a961

### Sección 8: Base de Datos MySQL: configurando MySQL en servicio productos

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 83-84. Instalando MySQL y Creando Database

Creamos la db en mysql

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 85. Configurando Datasource MySQL en servicio productos

A. En *productos* agregamos la dependencia de Diver mysql
B. agregamos la configuracion en application.properties

```properties
spring.datasource.url=jdbc:mysql://localhost:3306/ms_db_carrito_productos?serverTimezone=America/Mexico_City
spring.datasource.username=root
spring.datasource.password=mario
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL57Dialect

spring.jpa.hibernate.ddl-auto=create-drop
logging.level.org.hibernate.SQL=debug
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 86. Configurando ambiente dev con MySQL en Servidor de Configuración

Moveremos la configuracion del properties propio de *productos* al de
Server config para un mayor control.

A. creamos el archivo *servicio-productos-develop.properties* en nuestro
repositorio de configuraciones en mi caso ./_config/ con la misma conf
que se declaro arriba apenas.

B. Creamos el archivo bootstrap.properties en productos.

**Resultado:** Tenemos lo mismo pero ahora funciona con mysql.
**Commit:** gitlab.com/MarioM51/hola-micro-servicios/commit/607284b796d403fdbc1b6cbc202a0e32be81d947----

----

----

### Sección 9: Base de Datos PostgreSQL: configurando en servicio usuarios

#### 88. Instalando PostgreSQL y creando la Base de Datos en pgAdmin

A. Vamos a postgresql.org, descargamos, instalamos y desde pgadmin creamos
la bd.

B. Vamos a nuestro proyectos *clientes* agregamos el starter de postgreSQL

C. Quitamos la comillas fransesas (las que rodean al nombre de la tabla)
ya que no aplican en postgres, como los booleanos en mysql se manejan como
0 o 1 pero en postgreSQL se maneja true o false

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 89. Configurando PostgreSQL en Servidor de Configuración

A. Agregamos la dependencia/starter de "Config client" a *clientes&suarios*
y agregamos el archivo *bootstrap.properties*.

```bash
spring.application.name=carrito-clientes
spring.profiles.active=develop
spring.cloud.config.uri=http://localhost:8888
```

B. Vamos donde tenemos nuestros archivos de configuracion del server config
y creamos un nuevo archivo *carrito-clientes-develop.properties*

```bash
spring.datasource.url=jdbc:postgresql://localhost:5432/ms_carrito_clientes
spring.datasource.username=postgres
spring.datasource.password=mario

spring.datasource.driver-class-name=org.postgresql.Driver
spring.jpa.database-platform=org.hibernate.dialect.PostgreSQL95Dialect

spring.jpa.hibernate.ddl-auto=create-drop
logging.level.org.hibernate.SQL=debug

#para evitar un error/warning que no detiene pero es molesto
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true
```

**Resultado:** Migramos de la base de datos H2 a PostgreSQL y
todo funciona todo.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

#### 90. Configurando repositorio remoto GitHub como privado

A. Ponemos nuestro repositorio como privado en Gitlab o donde
quiera que lo tengamos.
B. Al querer levantar nos dara un error, por lo que deberemos
de agregar en el *application.properties* del proyecto *server_config*
lo siguiente.

```properties
spring.cloud.config.server.git.username=XXX
spring.cloud.config.server.git.password==XXX
```

**Commit:** gitlab.com/MarioM51/hola-micro-servicios/commit/ce823108c65be41127c0a2d536794e1970ab2df5

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

#### Sección 10: Trazabilidad distribuida con Spring Cloud Sleuth y Zipkin

#### 92. Introducción a la trazabilidad distribuida

**¿Que es Spring Cloud Sleuth?**. Es una herramienta que nos provee una
solucion de trazado distribuido para spring claud.
Permite identificar la peticion completa de un microservicio, como un
todo, y en cada llamada individual a otros micro-servicios.

Atributos TraceId y SpanId.

- TraceId: Identificador asociado a la peticion que viaja entre los
  microservicios.
- SpanId: Isentificador de la unidad de trabajo de cada llamada a un
  micro-servicio.

```r
{tipo} [{nombre-servicio},{TraceId},{SpanId},{exportadoAZipkin?}]
Ejemplo
INFO [{servicio-usuarios},09854onhgbdine,wjinogjwevth4,false
```

----

----

----

----

----

----

### Microservicios con Spring Cloud y Angular 9

#### Introduccion Micro

- Cada vez que desplegamos un extra servicio este se registra en eureka-server
  que se encargara de la comunicacion contros micro servicios y del balanceo.

- Un micro-servicio lo podemos instanciar las veces que necesitemos.

- En nuestro caso nuestros mecro-servicios seran eureka-cliens ya que son
  estos los que buscan al eureka-server

- Usaremos Feing que es un cliente http para comunicarnos con micro-servicios
  de manera simple.

- Gateway Zuul o Spring Cloud gateway: ambas librerias centralizan todos los
  puntos de acceso a uno solo, usando proxy y balanceo de carga.

- Ribbon o Spring Cloud Loadbalancer: Ambos nos ayudan con el balanceo de carga.

- Tenemos dos formas de compartir datos entre microserivios.
  - Base de datos compartida: Todos los micro-servicios consultan una misma base de datos.
  - Bse de datos por servicio: Cada micro-servicio tiene su propia B.D. y las relaciones sudecen a
    nivel de microservicio.
