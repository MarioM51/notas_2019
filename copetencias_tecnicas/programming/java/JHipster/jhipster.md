# JHipster

- [JHipster](#jhipster)
  - [Enlaces importantes](#enlaces-importantes)
  - [Introduccion](#introduccion)
    - [35. Yeoman & The JHipster Generator](#35-yeoman--the-jhipster-generator)
    - [38. Create Tasks Application](#38-create-tasks-application)
    - [42. Entity Sub Generator](#42-entity-sub-generator)
    - [46. Creacion de Blog (Code Assignment Review) ME ATORE AQUI CON RENDERIZACION DE HTML](#46-creacion-de-blog-code-assignment-review-me-atore-aqui-con-renderizacion-de-html)
  - [Sección 6: JDL - JHipster Domain Language](#sección-6-jdl---jhipster-domain-language)
    - [49. JDL Studio](#49-jdl-studio)
  - [Sección 7: MongoDB Application Using Spring Boot and Angular](#sección-7-mongodb-application-using-spring-boot-and-angular)
    - [54. Installing MongoDB](#54-installing-mongodb)
    - [58. Running Mongo Tasks Application](#58-running-mongo-tasks-application)
  - [Seccion 8, Micro-servicios app de conferencias](#seccion-8-micro-servicios-app-de-conferencias)
    - [65. Creating the registry application](#65-creating-the-registry-application)
  - [Final Proyect](#final-proyect)
    - [74. JHipster, Functional Requirements Review](#74-jhipster-functional-requirements-review)
    - [Functional Requirements Review](#functional-requirements-review)
    - [75. Creating the Running for Brews Spring Boot + Angular Application](#75-creating-the-running-for-brews-spring-boot--angular-application)

## Enlaces importantes

- [Proyecto ejemplo de JHipster](https://github.com/jhipster/jdl-samples)

## Introduccion

A. Nos aseguramos de tener instalado

  1. java `java -version`
  2. node  `node -v`.
  3. Yarn  `yarn -v`.

### 35. Yeoman & The JHipster Generator

JHipster es basicamente un Yoeman pero que genera aplicaciones
spring y angular.

A. instalar yoeman con yarn: `yarn global add yo`   <!-- instalar con yarn -->

B. instalar jhipster con yarn: `yarn global add generator-jhipster`

### 38. Create Tasks Application

Doc: www.jhipster.tech/creating-an-app/

En la documentacion podemos ver diferentes banderas como, usar yarn en
lugar de npm, quitar la generacion de datos falsos para desarrollo, etc.

- creamos carpeta para proyecto de jhipster <!-- proyecto jhipster -->
  - file://E:\cursos\0activ\web\angular-spring\hello-jhipster

- Iniciar jhipster `jhipster`. <!-- iniciar proyecto jhipster, crear proyecto jhipster, crear proyecto con jhipster -->
  - Seleccionamos "Aplicacion monolitica" para iniciar.
  - Escribimos el nombre de aplicacion y paquete.
  - No, a usar el "JHipster Registry".
  - Seleccionamos JWT para el tipo de autenticacion.
  - El tipo de base de datos "SQL".
  - para BD de produccion usaremos mysql y desarrollo h2 en memoria.

![01_iniciar_jhipster](./img/01_iniciar_jhipster.PNG)

<!-- iniciar backend, iniciar spring con el wrapper, iniciar spring sin ide, iniciar spring sin el ide, iniciar spring fuera del ide,  iniciar spring sin utilizar ide, iniciar spring sin utilizar el ide -->
<!-- levantar spring con el wrapper, levantar spring sin ide, levantar spring sin el ide, levantar spring fuera del ide,  levantar spring sin utilizar ide, levantar spring sin utilizar el ide -->
<!-- ejecutar spring con el wrapper, ejecutar spring sin ide, ejecutar spring sin el ide, ejecutar spring fuera del ide,  ejecutar spring sin utilizar ide, ejecutar spring sin utilizar el ide -->
- Ejecutamos el backend con `./mvnw` en caso de windows

<!-- levantar proyecto jhipster, correr jhipster -->
- Iniciar frontend de la aplicacion con `yarn start`.

![02_proyecto_iniciado](./img/02_proyecto_iniciado.PNG)

**Resultado:** Tenemos un proyecto creado con jhipster que en
primer instancia nos permite login y administracion de la aplicacion
como tal, como es usuarios, estadisticas, salud, etc.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 42. Entity Sub Generator

Fuente: www.jhipster.tech/creating-an-entity/

En la fuente podemos ver una serie de instrucciones para crear entidades,
esto lo podriamos crear por nuestra cuenta ya que nuestro proyecto no es mas
que una aplicacion spring y otra angular que siempre hemos conocido.

En la fuente indica que para la creacion de una nueva entidad requerimos de
los siguientes pasos.

- A database table.
- A Liquibase change set: Este es como un script que nos permitira tener
  un control de cambios sobre nuestra base de datos.
- A JPA Entity.
- A Spring Data JPA Repository.
- A Spring MVC REST Controller, which has the basic CRUD operations.
- An Angular router, a component and a service.
- An HTML view.
- Integration tests, to validate everything works as expected.
- Performance tests, to see if everything works smoothly.

Pero se crearan automaticamente con: `jhipster entity <entityName> --[options]` el cual
podemos ver su ayuda con `jhipster entity -help`

A. Crear entidad en jhipster: `jhipster entity Tareas`

**Error:*

```r
Parameter 1 of constructor in service.impl.\*ServiceImpl required a bean of type 'service.mapper.\*Mapper' that could not be found.
Action:
Consider defining a bean of type 'service.mapper.\*Mapper' in your configuration
```

En la documentacion igual dice que puede sacar este erro

```r
Error2: Plugin execution not covered by lifecycle configuration: org.bsc.maven:maven-processor-plugin:2.2.4:process (execution: process, phase: generate-sources
```

<!-- perfil de maven para jhipster, perfil maven jhipster, jhipster perfiles maven jhipster,  -->

**Descripcion**: Al terminar de agregar una entidad y refrescar los
archivos e iniciar el proyecto muestra este error.
**Causa**: Los Mappers se generan de forma automatica, al parecer
desde maven el cual en un principo no esta bien configurado el IDE para esto.
**Fuente**: www.jhipster.tech/configuring-ide-eclipse/
**Sulucion**: Agregar los perfiles de maven `dev,IDE` y/o seguir lo de
la activacion de los perfiles de  maven (Maven IDE profile) y la
instalacion de los plugins (Configuring MapStruct plugins) ahi
mencionados. En mi caso segui todos los pasos para evitar mar pedos.

<!-- ### 43. Using JHipster in Development -->
<!-- detectar un cambio en html, detectar un cambio en angular, jhipster recargar cambios del front -->

B. Si agregamos un cambio y queremos que detecte el cambio y recarge
el nevegador, esto tendremos que tener el `yarn start` activo para
que esto pase.

**Resultado** Cuando hagamos un cambio en el front este se refrescara automaticamente
y podremos ver nuestro nuevo cambio.

![03_entidad_agregada](./img/03_entidad_agregada.gif)

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 46. Creacion de Blog (Code Assignment Review) ME ATORE AQUI CON RENDERIZACION DE HTML

Creamos el Blog con una entidad de Post exactamente igual que hicimos arriba, solo que
para el cuerpo del Post usamos un campo Blob de tipo texto, el cual nos dara una caja
de texto y no un input.

**Objetivo:** Si agregamos un Post y en el cuarpo le ponemos HTML este al mostrarlo se veran
las etiquetas, para eso modificamos en el Font en el lugar que se puestra el body.

agregar pipe para .<!-- agregar pipeline personalizado, crear pipeline personalizado, crear pipe personalizado -->

```typescript
import { Pipe, PipeTransform, SecurityContext } from "@angular/core";
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
@Pipe({ name: 'safe', pure: false })
export class VarhtmlPipe implements PipeTransform {
    constructor(private _sanitizer: DomSanitizer) {}
    public transform(value: string, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
    switch (type) {
      case 'html':
        return this._sanitizer.bypassSecurityTrustHtml(value);
      case 'style':
        return this._sanitizer.bypassSecurityTrustStyle(value);
      case 'script':
        return this._sanitizer.bypassSecurityTrustScript(value);
      case 'url':
        return this._sanitizer.bypassSecurityTrustUrl(value);
      case 'resourceUrl':
        return this._sanitizer.bypassSecurityTrustResourceUrl(value);
      default:
        throw new Error(`Unable to bypass security for invalid type: ${type}`);
    }
  }
}
```

lo agregamos al kodulo de posts en el archivo *webapp\app\entities\post\post.module.ts*.
**Error**: the pipe 'actStatusPipe' could not be found.
**Descripcion**: Estaba agregando agregando el pipe en el app.module.ts
en declarations y otras formas, pero aun asi marcaba el error de no encontraba
mi pipe personalizado.
**Causa**: Las importaciones de dependencias/declarations estan divididas, por lo que
para que la parte de post (donde lo que ria usar) lo encontrara debe ser en el **post.module.ts**
en mi caso.
**Solucion**: Agregar el pipe en **post.module.ts** en declarations para que lo pueda usar.

```js
import { VarhtmlPipe } from 'app/varhtml.pipe';

@NgModule({...
  declarations: [... VarhtmlPipe]...
})
```

A. Agregar html para que se renderice, buscamos este codigo.

```html
<!-- Original -->
...<tr *ngFor="let post of posts ;trackBy: trackId">...
  <td>{{post.body}}</td>
```

B. Interpretar el html de un campo

```html
<!-- Modificado -->
...<tr *ngFor="let post of posts ;trackBy: trackId">...
  <td [innerHTML]="post.body | safe: 'html'"></td>
```----

<!-- no se muestran estilos, no se renderizan estilos, no se muestra html inyectado, no funciona html inyectado,
no se muestra html agregado-->

**Error**: No se renderizaba las etiquetas html que obtenia del back incluso
cuando estaba usando innerHtml y bypasssecuritytrusthtml.
**Causa**: Por una cagada razon se estaba agregando el estilo de abajo el cual
estaba asiendo que ensimaran el estilos original.

```css
b, strong { font-weight: bolder; }
```

**Debug:** Inspeccionar el elemento y ver que estilos se le estan aplicando,
asi fue como descubri que la culpa eran de estos estilos que se agregaron por
una cagada razon.
**Solucion**: Agregar un estilo en *global.scss* para regresar los estilos como son.

<!--

######################################################################## divicion titulo

-->

----

<!--

######################################################################## divicion titulo

-->

## Sección 6: JDL - JHipster Domain Language

Doc: www.jhipster.tech/jdl/

JDL es la forma en la que podremos generar nuestras entidades desde un
script, el cual podremos generar este script desde una interface grafica
para que sea mucho mas facil de hacerlo.

A. Instalamos el interprete de JDL: `npm install -g jhipster-uml`

B. Descargamos el ejemplo que nos da JHipster que es
[Oracle Human Resources](https://github.com/jhipster/jdl-samples).

C. Crear proyecto jhipster `jhipster` y esperamos que lo cree.

D. Importamos el archivo `jhipster import-jdl my_file.jdl`

en mi caso tiene este contenido, en el que podemos ve

```r
// Creacion de entidades, en este caso los
// IDs se crean de forma automatica
entity Region {
  regionName String
}
entity Country {
  countryName String
}
entity Location {
  streetAddress String, postalCode String, city String, stateProvince String
}
entity Department {
  departmentName String required
}
entity Task {
  title String, description String
}
entity Employee {
  firstName String, lastName String, email String, phoneNumber String, hireDate Instant, salary Long, commissionPct Long
}
entity Job {
  jobTitle String, minSalary Long, maxSalary Long
}
entity JobHistory {
  startDate Instant, endDate Instant, language Language
}
enum Language {
    FRENCH, ENGLISH, SPANISH
}
relationship OneToOne {
  Country{region} to Region
}
relationship OneToOne {
  Location{country} to Country
}
relationship OneToOne {
  Department{location} to Location
}
relationship ManyToMany {
  Job{task(title)} to Task{job}
}
relationship OneToMany {
  Employee{job} to Job,  Department{employee} to Employee
}
relationship ManyToOne {
  Employee{manager} to Employee
}
relationship OneToOne {
  JobHistory{job} to Job, JobHistory{department} to Department, JobHistory{employee} to Employee
}
paginate JobHistory, Employee with infinite-scroll
paginate Job with pagination
service all with serviceImpl except Employee, Jo
```

<!--

######################################################################## divicion titulo

-->

----

<!--

######################################################################## divicion titulo

-->

### 49. JDL Studio
<!-- JHipster Studio -->
Doc: start.jhipster.tech/jdl-studio/
Doc2: www.jhipster.tech/development/ Buscar "Database updates"

**Objetivo:** Ya creamos el proyecto y creamos las entidades, ahora vamos a hacer
un cambio, el cual lo hacemos desde JDL Studio y corremos el mismo comando para que
se vean los cambios en el proyecto.

A. Hacer la modificacion de la entidad, en mi caso desde JDL Studio agrego un nuevo campo
e importo el archivo de nuevo, como hicimos arriba

```r
jhipster import-jdl src/main/resources/scripts/entities.j
```

B. En mi caso me pide sobre escribir el siguiente archivo, lo cual le pongo que NO y a los demas si

```r
overwrite src\main\resources\config\liquibase\changelog\20200205215737_added_entity_Employee.xm
```

C. Correr el comando `mvn liquibase:diff`, en mi caso con h2 en disco me detecta el nuevo campo,
pero igual detecta otros que no ban al caso, esos los borre, dejando solo lo que reconocia.

D. Pase la generacion de fake-data al nuevo changelog abajo de donde se aplican los cambios del nuevo campo

```xml
<?xml version="1.1" encoding="UTF-8" standalone="no"?><databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd">
    <!-- el cambio que hice yo -->
    <changeSet author="subject51 (generated)" id="1581230324795-13">
        <addColumn tableName="employee">
            <column name="descripcion" type="varchar(255)"/>
        </addColumn>
    </changeSet>
    <!-- generacion del fake-data -->
    <changeSet id="20200205215737-1-data" author="jhipster" context="faker">
        <loadData file="config/liquibase/fake-data/employee.csv" separator=";" tableName="employee">
            <column name="id" type="numeric"/> <column name="first_name" type="string"/> <column name="last_name" type="string"/> <column name="email" type="string"/> <column name="phone_number" type="string"/> <column name="hire_date" type="datetime"/> <column name="salary" type="numeric"/> <column name="commission_pct" type="numeric"/>
        </loadData>
    </changeSet>
</databaseChangeLog>
```

E. Ya debe de funcionar, sin problemas si no abajo capture algunos.

<!-- Cambiar h2 en disco a h2 en memoria, configuracion de h2, como configurar h2, configuracion h2 -->
**Actividad Extra:** Cambie el H2 de disco a memoria pero asi el liquidbase hacia un cagadero con el changelog
al parecer siempre detectaba que faltaba todo, mientras qe con H2 en disco si detecta los cambios, solo que
ahi qe borrarle unos extra dejando solo lo que nosotros hicimos, ASI QUE dejar el H2 en disco para evitar problemas con liquidbase.
**Fuente:** www.baeldung.com/spring-boot-h2-database En "Database Configuration"

A. Vamos a *.\src\main\resources\config\application-dev.yml* Buscar "Database Configuration"

```yml
datasource:
    url: jdbc:h2:file:./target/h2db/db/hr;DB_CLOSE_DELAY=-1  # <------- Aqui
```

Cambiamos por

```yml
datasource:
    url: jdbc:h2:mem:testdb
```

B. Levantamos de nuevo el proyecto y ya deberia de funcionar.

----
**Error-A**: Liquibase could not start correctly, your database is NOT ready: Validation Failed: XXX:jhipster was: 8:XXX but is now: 8:XXX.
**Fuente**: stackoverflow.com/questions/43207163/jhipster-liquibase-validation-error-after-modify-entity Buscar "Proper solution".
**Causa/Pre-Condicion**: Aqui lo que paso es que le di en JHipster que sobre-escribiera el archivo de creacion de la entidad con el nuevo
campo, lo que hace que el hash del archivo cambie, lo cual causa el error.
Y tambien es un error sobre-escribirlo ya que pierde el sentido liquidbase, al crear el archivo como si la entidad-tabla se ubiera creado
inicialmente con el campo que recien agregamos, lo correcto es crear un archivo changelog con "liquibase:diff" y agregar este al master para
que se guarde el registro de los cambios en liquibase.
**Solucion**:

  a. Al terminar de hacer el cambio en nuestra entidad ya sea desde importar jdl o modificar la entidad directamente.

  b. Si le dimos sobre-escribir con git retornar al estado original el *config/liquibase/changelog/yyyyMMddhhmmss_added_entity_nameEntity.xml"* que se
  sobre-escribio.

  c. corremos el siguiente comando `mvn liquibase:diff`, lo que generara un nuevo archivo.

  d. El xml recien creado lo agregamos a *resources\config\liquibase\master.xml* de la misma manera
  como se agregn los demas, en mi caso

```xml
<include file="config/liquibase/changelog/20200207201953_changelog.xml" relativeToChangelogFile="false"/
```

----

**Error-B:** Al correr el comando `mvn liquibase:diff` muestra error *ClassNotFoundException*.
**Causa:** Algun tema de error al descargar, vincular, visibilidad con las dependencias.
**Solucion:** Ya sea desde el *IDE->proyecto->cliDer->maven->Update* Dependencies, o desde la consola
`mvn clean install -U -DskipTests`.<!-- actualizar dependencias desde consola, actualizar dependencias con la consola, actualizar dependencias de maven desde consola-->

----

**Error-C:*

```log
Change Set .../changelog/XXX_added_entity_XXX.xml::XXX::jhipster failed. Caused by: liquibase.exception.DatabaseException: Error: org.h2.jdbc.JdbcSQLSyntaxErrorException:
Column "XXX" not found; SQL statement:INSERT INTO PUBLIC.xxx(id, XXX, XXX) VALUES(?, ?, ?
```

**Descripcion:** Al agregar un nuevo campo, creado el changelog, y añadirlo al master.xml marca este error, de que
no enxuentra el nuevo campo que agregamos.
**Causa:** Tenemos en *XXX_added_entity_XXX.xml* un `<changeSet id... context="faker"><loadData...>` que genera la fake-data pero
est en un punto del changelog donde aun no se a generdo el nuevo campo que recien agregamos.
**Solucion:** Mover este `<changeSet id... context="faker"><loadData...>` a un punto posterior al que agregamos las nuevas
columnas.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Sección 7: MongoDB Application Using Spring Boot and Angular

**Escenarios para MongoDB**, mongo es buena para operaciones de alta velocidad pero que los requerimientos
de negocio pueden ser tolerantes a errores, por ejemplo lectura en tiempo real de un sensor, mensajes de texto,
registro de actividad masiva, etc, donde NO hay mucho problema que debes en cuando se pierda algun registro.

**Mongo es rapido**, esto debido a que no lidia con el principio de transacciones CRUD como otras
bases de datos como MySQL u oracle.

**Donde no usar Mongo**, No es una buena idea en datos altamente estructurados (redundancia de datos) y que cada
regustro sea importante, por ejemplo un banco, ventas, CRMs, donde perder registros significan
un problema.

Aqui vemos los elementos equivalentes entre una base de datos relacional, y una
en documentos (sql y noSql).

![04_diferencias_mongo_sqlrelational](./img/04_diferencias_mongo_sqlrelational.PNG)

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 54. Installing MongoDB

<!---- instalacion de mongo -->
- Nosotros usaremos *Mongo Comunity Edition* que es open source y gratis de usar.

- En el curso usaremos mongo en local y en el puerto 27017 (default) para mover menos cosas en JHipster.

A. Creamos un proyecto monolitico, y vamos a [jsl-studio](https://start.jhipster.tech/jdl-studio/) para crear
nuestra entidad de tarea

```r
entity Task {
  id Long,
  name String,
  dueDate LocalDate,
  completed Boolean,
}
```

B. Descargamos el script y lo ejecutamos con xxx,

C. Si nos fijamos veremos en nuestro domain en Task veremos que la entidad se genero
con otras anotaciones, las cuales pertenecen a mongo

```java
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "task")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("due_date")
    private LocalDate dueDate;

    @Field("completed")
    private Boolean completed;
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 58. Running Mongo Tasks Application

Lo corremos como siempre y si ahora nos fijamos en "MongoDB Compass Comunity" y nos conectamos
en la coneccion por default, veremos nuestros nuevos registros que creamos.

![05_mongoDB_Spring_angular](./img/05_mongoDB_Spring_angular.PNG)

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Seccion 8, Micro-servicios app de conferencias

Vamos a hacer una app que manejara los siguienes modulos y sub modulos.

- Conferencias
  - Expositores: Quienes seran los que daran la conferencia, donde
    guardaremos informacion como nombre, especialidad, experiencia.
  - Sesiones: Una conferencia estara compuesta de varias charlas, donde cada una tendra quien
    es el expositor, tema, descripcion etc.
  - Agenda: Una conferencia tendra un horario en donde ocurriran varias sesiones el cual tendran
    un horario de inicio y de fin cada una de ellas.

- Blog: En donde los interesados de las conferencias podran ver noticias, contenido de las estas, etc.

- Registro: En donde identificaremos a cada usuario y su rol.

- Talleres: Aqui los expositores podran dar algun entrenamiento sobre sus conferencias, en el cual
  se podran registrar.

Cada Modulo podemos verlo como un micro-servicio, en el cual por ejemplo tendremos un microservicio de conferencias,
donde solo se encargara de las conferencias, no hara nada de los blogs, y si necesita algo de seguridad, entonces
llamaremos al microservicio de seguridad.

### 65. Creating the registry application

[PENDIENTE]

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Final Proyect

### 74. JHipster, Functional Requirements Review

La aplicacion qe haremos es de seguimiento de leatar de corredores, los cuales
los miembros de un club de maratones cumplan con x requerimientos, estos tendran
ciertos derechos como una cerveza.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Functional Requirements Review

- **Sobre la aplicaion**
  - La aplicacion es para un club de corredores.

  - Esta llevara un seguimiento de lealtad de los miembros del club.

  - Se llevara un seguimiento de la partipaciones de los miembros en las carreras y/o eventos
    en la cual estos rebiran puntos por cada visita.

  - Los miembros reciben premios basado en los puntos que estos tengan.

- **Sobre nuestro cliente**
  - Es el fundador y un organizador del club de carreras llamado "Correr por cervesas"

  - El club tiene como 30 localidades diferentes

  - Cada ruta esta liderada por un organizador

  - Los patrocinadores apoyan a los miembros que complan con ciertos puntos en el club.

- **Requerimientos funcionales**

  - Los usuarios podran loguerse por facebook o email/password

  - El login de facebook deberia de tomar el correo de este.

  - Opcion de cambio de contraseña por el email.

  - Se necesita un seguimiento de los miembros por su ruta y calendario, ya
    que estos ellos lo pueden modificar, por ejemplo en algun momento cambia de
    ruta, o deja de ir los martes para ir los sabados.

  - Se debe crear un identificador unico para cada corredor para vaciar esto en
    un codigo QR o algo similar para identificarlos en los eventos y que el organizador
    pueda agregarle puntos, o ellos mismos por el codigo QR de la pared del punto de reunion.

  - los diferentes roles.
    - **Root**, para el administrador de sistema, el cual podra agregar los roles de admin o de
    organizador.
    - **Admin**, estos podran crear rutas para correr y calendarios de las corridas.
    - **Organizador**, Capas de conseguir los el codigo unico de cada organizador, esto igual puede el admin.
    - **Corredor**, Capas de hacer login con su identificador.
    - **Usuario**, pueden cambiar de contraseña, email, ruta.

  - Se mostraran los mejores corredores de cada ruta.

  - Despues de 10 intentos el usuario sera bloquedo por 10 minutos.

  - La contraseña solo tendra que ser mayor a 6 caracteres.

  - El sistema creara un codigo unico para cada corredor.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 75. Creating the Running for Brews Spring Boot + Angular Application

A. Creamos nuestra aplicacion en jhipster de tipo monolitica la cual nos bastara.

B. seleccionamos una autenticacion d tipo http ya que se lleva mejor con esta, la autenticacion
por redes sociales.

[PENDIENTE]
