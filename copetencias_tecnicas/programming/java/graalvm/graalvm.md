# Proceso para correr Spring com Graalvm

1. Instalacion [aqui](https://www.graalvm.org/docs/getting-started/#install-graalvm)

   1. NO olvidar cambiar la JAVA_HOME
   2. Vamos a neceitar native-image: `gu install native-image`
   3. Seguir instrucciones

2. Configuracion del proyecto Spring [aqui](https://docs.spring.io/spring-native/docs/current/reference/htmlsingle/#getting-started-native-build-tools)

   1. No olvidar Quitar/Comentar dependencia `spring-boot-devtools`
   2. No olvidar cambiar `spring-boot-starter-parent` a la version `2.5.3`
   3. Checar version de java en la terminal donde se construira, ya
      que debe de decir que esta usando la version de graalvm
   4. Seguir instrucciones

Erro
 comando muy largo