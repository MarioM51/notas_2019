# Testing Spring Boot

- JUnit: [Documentacion](https://junit.org/junit5/docs/current/user-guide)
- Spring Testing: [Documentacion](https://docs.spring.io/spring-boot/docs/2.1.6.RELEASE/reference/html/boot-features-testing.html)
- CircleCI: [Documentacion](https://circleci.com/docs/2.0/)
- jayway.jsonpath: [Documentacion](https://github.com/json-path/JsonPath)
- Wiremock: [Documentacion](http://wiremock.org/docs/)
- spring-restdocs: [Documentacion](https://docs.spring.io/spring-restdocs/docs/current/reference/html5/)
- Codigo
  - Seccion 1 a 11: [.zip](./img/05_holaJUnit_fin_mockito.zip)
  - Inicio Seccion 12 a 14: [Link](https://github.com/springframeworkguru/tb2g-testing-spring)
  - Final Seccion 12 a 14: [.zip](./img/08-tb2g-testing-spring.zip)
  - Seccion 16 adelante: [.zip](./img/15_hola-tests-spring-boot.zip)

## Indice

- [Testing Spring Boot](#testing-spring-boot)
  - [Indice](#indice)
  - [16. Introduccion](#16-introduccion)
    - [1.  Degenerate Objects](#1--degenerate-objects)
    - [18. Equality for All](#18-equality-for-all)
    - [19. Privacy](#19-privacy)
    - [20. Franc-ly Speaking](#20-franc-ly-speaking)
    - [22. Apples and Oranges](#22-apples-and-oranges)
    - [23. Making Object](#23-making-object)
    - [24. Times We're Livin' In](#24-times-were-livin-in)
    - [25. Refactorizar multiplicacion](#25-refactorizar-multiplicacion)
    - [26. Refactorizacion Clases de mas y Test duplicados](#26-refactorizacion-clases-de-mas-y-test-duplicados)
    - [27. Iniciar Suma misma moneda](#27-iniciar-suma-misma-moneda)
    - [28. Make It](#28-make-it)
    - [29. Cambiar](#29-cambiar)
    - [Suma Pesos-soles](#suma-pesos-soles)
    - [Conclucion](#conclucion)
  - [Sección 4:Getting Started with JUnit 5](#sección-4getting-started-with-junit-5)
    - [Iniciando proyecto, Configurando Maven y los tests](#iniciando-proyecto-configurando-maven-y-los-tests)
    - [38. Uso de @BeforeEach y @BeforeEach, y @BeforeAll y @AfterAll](#38-uso-de-beforeeach-y-beforeeach-y-beforeall-y-afterall)
    - [48. assertAll: Testeando POJOs/DTOs checar constructores](#48-assertall-testeando-pojosdtos-checar-constructores)
    - [Anidacion de tests, test a constructor de clase padre e hija](#anidacion-de-tests-test-a-constructor-de-clase-padre-e-hija)
    - [50. Skipping JUnit Tests](#50-skipping-junit-tests)
    - [51. @DisplayName](#51-displayname)
    - [Expected Exceptions](#expected-exceptions)
    - [53. Testing Timeouts](#53-testing-timeouts)
    - [54. JUnit Assumptions](#54-junit-assumptions)
    - [55. Conditional JUnit Test Execution](#55-conditional-junit-test-execution)
    - [56. Using AssertJ with JUnit](#56-using-assertj-with-junit)
    - [60. Conclucion](#60-conclucion)
  - [Seccion 6: Advanced JUnit Testing](#seccion-6-advanced-junit-testing)
    - [62. Tagging and Filtering JUnit Tests](#62-tagging-and-filtering-junit-tests)
    - [63. JUnit Nested(anidado) Tests](#63-junit-nestedanidado-tests)
    - [64. Pasar anotaciones por interface a tests](#64-pasar-anotaciones-por-interface-a-tests)
    - [66. Repetir tests](#66-repetir-tests)
    - [66. Argumentos en anotaciones de tests](#66-argumentos-en-anotaciones-de-tests)
    - [73. Usar parametros en tests: .CSV en codigo](#73-usar-parametros-en-tests-csv-en-codigo)
    - [73. Usar parametros en tests: .CSV en archivo](#73-usar-parametros-en-tests-csv-en-archivo)
    - [78. Extenciones JUnit, Crear una extencion](#78-extenciones-junit-crear-una-extencion)
  - [Sección 9: Getting Started with Mockito](#sección-9-getting-started-with-mockito)
    - [102.102 Crear primer mock](#102102-crear-primer-mock)
    - [Crear mock con anotaciones](#crear-mock-con-anotaciones)
    - [105. Usando Extension de Mockito](#105-usando-extension-de-mockito)
    - [106. Crear Mock y verificar que se llama un metodo 1 o n veces](#106-crear-mock-y-verificar-que-se-llama-un-metodo-1-o-n-veces)
    - [110. Retornando valores de un Mock](#110-retornando-valores-de-un-mock)
    - [111. Argument Matchers](#111-argument-matchers)
  - [Sección 10: Behavior Driven Mockito](#sección-10-behavior-driven-mockito)
  - [Sección 11:Advanced Mockito](#sección-11advanced-mockito)
    - [124. Throwing Exceptions with Mockito](#124-throwing-exceptions-with-mockito)
    - [128. Mockito Argment Capture](#128-mockito-argment-capture)
    - [129. Ejemplo como manejar multiples respuestas de Mock](#129-ejemplo-como-manejar-multiples-respuestas-de-mock)
    - [130. Verify Order of Interactions](#130-verify-order-of-interactions)
    - [132. Verify Zero or No More Interactions with Mock](#132-verify-zero-or-no-more-interactions-with-mock)
  - [Sección 12:Testing with Spring Framework](#sección-12testing-with-spring-framework)
    - [137. Spring Pet Clinic Code Review](#137-spring-pet-clinic-code-review)
  - [Sección 13: Spring Framework Testing Context](#sección-13-spring-framework-testing-context)
    - [Yanny or Laurel?_](#yanny-or-laurel_)
    - [143. Creando Spring Context basico para testear](#143-creando-spring-context-basico-para-testear)
    - [144. JUnit 4 Laurel Test, Crear Test JUnit4 para spring basico](#144-junit-4-laurel-test-crear-test-junit4-para-spring-basico)
      - [Inyectar un bean u otro de la misma interface](#inyectar-un-bean-u-otro-de-la-misma-interface)
    - [144. JUnit 4 Laurel Test, Crear Test JUnit5 para spring basico](#144-junit-4-laurel-test-crear-test-junit5-para-spring-basico)
    - [150. Using Inner Class Configuration](#150-using-inner-class-configuration)
    - [151. Usando ComponentScans() es JUnit5](#151-usando-componentscans-es-junit5)
    - [152. Usando perfiles un beans y en tests](#152-usando-perfiles-un-beans-y-en-tests)
    - [153. Testear el propierties de Spring](#153-testear-el-propierties-de-spring)
  - [Sección 14:Spring MVC Test](#sección-14spring-mvc-test)
    - [159-A. Testear controlador Spring solo con Mockito](#159-a-testear-controlador-spring-solo-con-mockito)
    - [159-B. Primer tests con MVC-Test](#159-b-primer-tests-con-mvc-test)
    - [[PENDIENTE] 160. Tomar configuracion de XML](#pendiente-160-tomar-configuracion-de-xml)
    - [163. Peticion GET con argumentos](#163-peticion-get-con-argumentos)
    - [164. Diferentes respuestas de controlador con MockMvc](#164-diferentes-respuestas-de-controlador-con-mockmvc)
    - [166. Petcion POST Valida](#166-petcion-post-valida)
    - [167. Peticion POST NO valida, manejo correcto del error](#167-peticion-post-no-valida-manejo-correcto-del-error)
    - [Argumentos de Path algo/{id}, correcto e incorrecto](#argumentos-de-path-algoid-correcto-e-incorrecto)
  - [Sección 15: Introduction to Testing with Spring Boot](#sección-15-introduction-to-testing-with-spring-boot)
    - [173. Crear proyecto Spring Boot con VSCode](#173-crear-proyecto-spring-boot-con-vscode)
    - [174. Updating Spring Boot 2.x to JUnit 5](#174-updating-spring-boot-2x-to-junit-5)
    - [175. Integrando proyecto SpringBoot Maven con CircleCI](#175-integrando-proyecto-springboot-maven-con-circleci)
    - [176. Proyecto de la seccion, uso de Lombok y mapstruct](#176-proyecto-de-la-seccion-uso-de-lombok-y-mapstruct)
  - [Sección 16:Spring MVC Rest Controller Tests](#sección-16spring-mvc-rest-controller-tests)
    - [Jayway JsonPath](#jayway-jsonpath)
    - [181. Primer test a RestController con JsonPath (sin SpringBoot)](#181-primer-test-a-restcontroller-con-jsonpath-sin-springboot)
    - [182. Usando JsonPath con listas (sin SpringBoot)](#182-usando-jsonpath-con-listas-sin-springboot)
    - [183. Convertir prueba Mockito a SpringBoot Slices](#183-convertir-prueba-mockito-a-springboot-slices)
    - [185. Crear test de Integracion, Spring-Context completo con RestTemplate](#185-crear-test-de-integracion-spring-context-completo-con-resttemplate)
  - [Sección 17:Testing with Wiremock](#sección-17testing-with-wiremock)
    - [191. Primer uso de Wiremock en modo Standalone](#191-primer-uso-de-wiremock-en-modo-standalone)
    - [192. Primer grabacion con Wiremock](#192-primer-grabacion-con-wiremock)
    - [193. Primer Test con wiremock y JUnit](#193-primer-test-con-wiremock-y-junit)
  - [Sección 18: Spring MVC REST Docs](#sección-18-spring-mvc-rest-docs)
    - [199. Primer Documentacion con Spring-MVC-REST-Docs](#199-primer-documentacion-con-spring-mvc-rest-docs)
  - [Tips cosas que voy encontrando](#tips-cosas-que-voy-encontrando)
    - [Construir reporte de tests](#construir-reporte-de-tests)
    - [Testear Validaciones de datos de javax.validation.constraints](#testear-validaciones-de-datos-de-javaxvalidationconstraints)

## 16. Introduccion

**Objetivo:** Tenemos que desarrollar un sistema donde se pueda hacer
operaciones basicas con dinero, pero tinemos diferentes monedas como
es el peso(p), sol(s), en donde en este caso el sol equivale a 0.5p.

A. Creamos un lista de cosas que hacer

- Al sumar pesos y soles por ejemplo: 5p+10s=10p
- Al multiplicar debe dar por ejemplo: 5p*2=10p
- Canridad debe de ser privada
- Que pasa cuanso se redondea?

A. Creamos un proyecto simple en spring boot. Por defecto ya debemos
tener las dependencias de JUnit

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-test</artifactId>
  <scope>test</scope>
  <exclusions>
    <exclusion>
      <groupId>org.junit.vintage</groupId>
      <artifactId>junit-vintage-engine</artifactId>
    </exclusion>
  </exclusions>
</dependency>
```

B. Creamos un test desde paquete->clickDer->New->JUnit Test Case

**Importante**: Primero escribimos el test aunque aun no ahigamos
creado las clases, metodos, variables etc, al terminar el test
empezamos a implementar para que pase el test.

```java
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class CurrencyServiceTest {

  @Test
  void tesMultiplicationt() {
    Dolar cinco = new Dolar(5);
    cinco.multiplicarPor(2);
    assertEquals(10, cinco.cantidad);
  }
}
```

C. Implementamos los tests

```java
public class Dolar {

  public Integer cantidad;

  public Dolar(int cantidad) {
    this.cantidad = cantidad;
  }

  public void multiplicarPor(int multi) {
    this.cantidad *= multi;
  }
}
```

D. Corremos nuestros tests y ya deberian de pasar

![./img/01-correr-tests](./img/01-correr-tests.PNG)

hasta este punto, ya podemos ver que pasan nuestros tests, pero segun
nuestro lista/TODO aun no completamos, por ejemplo, el campo cantidad
es publico y debe ser privado.

### 1.  Degenerate Objects

**Problema:** Vamos a abordar el tema de que nuestra variable cantidad
es publica y mutable, lo cual no se recomienda, por ejemplo si modificamos
nuestro test va a FALLAR.

```java
@Test
void tesMultiplicationt() {
  Dolar cinco = new Dolar(5);
  cinco.multiplicarPor(2);
  assertEquals(10, cinco.cantidad);

+ cinco.multiplicarPor(3);
// FALLA por que se modifico "cantidad" o el objeto, lo cual proboco
// que que no estemos multiplicando "5*2" si no "10*3"
+ assertEquals(15, cinco.cantidad);
}
```

**Para solucionarlo**: Debemos de evitar que nuestro objeto dolar sea
mutable, por lo que en multiplicacion vamos a hacer que retorne su
mismo objeto con el resultado en lugar de modificarlo.

A. Escribimos el test

```java
@Test
void tesMultiplicationt() {
  Dolar cinco = new Dolar(5);
  Dolar resultado = cinco.multiplicarPor(2);
  assertEquals(10, resultado.cantidad);

  resultado = cinco.multiplicarPor(3);
  assertEquals(15, resultado.cantidad);
}
```

B. Hacemos que pase el test

```java
public Dolar multiplicarPor(int multi) {
  return new Dolar(this.cantidad*multi);
}
```

**Resultado:** Nuestro test pasa correctamente, he implementamos una
mejor aproximacion para solucionar el problema.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 18. Equality for All

**Problema:** Si en algun momento queremos usar el `equals` en dos
objetos Peso, que tengan la misma cantidad estos causaran errror
ya que comparara el espacio de memoria no su valos, para **solucionarlo**
debemos sobre escribir la implementacion de este metodo `equals`.

A. Escribimos el Test y vemos que falle

```java
@Test
void tesEquals() {
  Peso cincoA = new Peso(5);
  Peso cincoB = new Peso(5);
  assertEquals(cincoA, cincoB);
  assertEquals(false, cincoA.equals(new Peso(10)));
}
```

C. Hacemos que pase el Test

```java
@Override public boolean equals(Object toCompareFlat) {
  Peso toCompare = (Peso)toCompareFlat;
  return this.cantidad == toCompare.cantidad;
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 19. Privacy

**Problema:** Tenemos "cantidad" publica y esto nos puede traer varios
problemas, por lo que vamos a pasarlo a privado y usaremos el equal
para comparar el numero sin tener que acceder a el.

A. Pasamos "cantidad" a privada y modificamos el test para que compile

```java
@Test
void tesMultiplicationt() {
  Peso cinco = new Peso(5);
  resultado = cinco.multiplicarPor(3);
//assertEquals(15, resultado.cantidad);
  assertEquals(new Peso(15), resultado);
}
```

**Resultado:** Refactorizamos usando codigo ya implementado.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 20. Franc-ly Speaking

**Objetivo:** Vamos a implementar la moneda de "Sol" hasta ahora tiene
la misma funcionalidad, por lo que escribimos los mismos tests y su
implementacion, la cual es copia pega de peso, lo que nos quedaria.

```java
// SOLES
@Test void tesMultiplicationtSol() {
  Sol cinco = new Sol(5);
  Sol resultado = cinco.multiplicarPor(2);
  assertEquals(new Sol(10), resultado);

  resultado = cinco.multiplicarPor(3);
  assertEquals(new Sol(15), resultado);
}

@Test void tesEqualsSol() {
  Sol cincoA = new Sol(5);
  Sol cincoB = new Sol(5);
  assertEquals(cincoA, cincoB);
  assertNotEquals(cincoA, new Sol(10));
}
```

```java
public class Sol {

  private int cantidad;

  public Sol(int cantidad) {
    this.cantidad = cantidad;
  }

  public Sol multiplicarPor(int multi) {
    return new Sol(this.cantidad*multi);
  }

  @Override
  public boolean equals(Object toCompareFlat) {
    Sol toCompare = (Sol)toCompareFlat;
    return this.cantidad == toCompare.cantidad;
  }

}
```

**Problema:** Copiar y pegar es un sintoma de que estamos haciendo
algo mal, por lo que agregamos a la lista, refactorizar para evitar
el codigo duplicado.

A. Creamos una clase "Dinero" para implementar la funcionalidad
compartida que tienen los objetos "Peso" y "Sol", sin **nada de codigo**
luego extendemos y corremos las pruebas y no deberia de romperse nada.

B. Extendemos las clases y quitamos el metodo equals de ambos para
pasarselo a Dinero quedandonos

```java
public class Sol extends Dinero {

  public Sol(int cantidad) {
    this.cantidad = cantidad;
  }

  public Sol multiplicarPor(int multi) {
    return new Sol(this.cantidad*multi);
  }
}

public class Peso extends Dinero {

  public Peso(int cantidad) {
    this.cantidad = cantidad;
  }

  public Peso multiplicarPor(int multi) {
    return new Peso(this.cantidad*multi);
  }
}
```

C. Y sin modificar nada los tests estos deberian de pasar todabia sin
problema

<!--

#########################################

-->

----

<!--

#########################################

-->

### 22. Apples and Oranges

**Problema:**  COn nuestra implementacion actual si comparamos un Peso
y un Sol, nos va a decir que son lo mismo pero no lo es, por lo que
debemos de especificar esto en los tests

```java
@Test void tesPesoNotEqualsToSol() {
  assertNotEquals(new Peso(10), new Sol(10)); // VA A FALLAR
}
```

Esto lo estamos haciendo en "Dinero" por lo que tenemos el problema
que ambos son "Dinero" por lo que demos de implementar un `getClass`
para poder hacerlos.

```java
public class Dinero {

  protected int cantidad;

  @Override public boolean equals(Object toCompareFlat) {
    Dinero toCompare = (Dinero)toCompareFlat;
    return this.cantidad == toCompare.cantidad
+          && this.getClass().equals(toCompareFlat.getClass());
  }
  
}
```

Esto deveria de solucionar nuestro problema y ya deberia de pasar el
tests que recin agregamos.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 23. Making Object

**Objetivo: Refactorizar,** ahora estamos usando Peso y Sol que son
hijas de Dinero, pero se recomienda que no usemos las clases hijas
si no que usemos la clase padre, esto nos ayuda por ejemplo, a que si
hacemos un cambio en el constructor no se nos rompa el codigo en muchas
partes si no solo en la clase padre.

A. EN el tests quitamos la creacion directa de Peso y Sol y usamos un
nuevo metodo que hara esto, y de lugar de usar las implementaciones
usaremos Dinero.

```java
@Test void tesMultiplicationt() {
  Dinero cinco = Dinero.peso(5); //ya no usamos "new" ni "Peso" o "Sol"
  Dinero resultado = cinco.multiplicarPor(2);
  assertEquals(Dinero.peso(10), resultado);

  resultado = cinco.multiplicarPor(3);
  assertEquals(Dinero.peso(15), resultado);
}

//...

@Test void tesEqualsSol() {
  Dinero cincoA = Dinero.sol(5);
  Dinero cincoB = Dinero.sol(5);
  assertEquals(cincoA, cincoB);
  assertNotEquals(cincoA, Dinero.sol(10));
}

@Test void tesPesoNotEqualsToSol() {
  assertNotEquals(Dinero.peso(5), Dinero.sol(5));
}
```

B. Implementamos los cambios para poder pasar los tests

```java
public abstract class Dinero { //agregamos abstract

  protected int cantidad;

+ public abstract Dinero multiplicarPor(int multi);

+ public static Dinero peso(int cantidad) {
    return new Peso(cantidad);
  }

+ public static Dinero sol(int cantidad) {
    return new Sol(cantidad);
  }  
}
```

**Resultado:** Ya no estamos usando las hijas lo cual es una
recomendacion de buenas practicas OO.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 24. Times We're Livin' In

**Objetivo:** Vamos a modificar el constructor agregando una variable
que se debe agregar de forma obligatoria, para ver que la factorizacion
que hicimos si nos ayuda, haciendo que no tengamos que modificar en
todos lados, si no solo en la clase padre.

C. Creamos el test donde usaremos el nuevo constructor que agrega una
nueva variable obligatoria

```java
@Test void tesTipoMoneda() {
  assertEquals("PESO", Dinero.peso(5).getTipo());
  assertEquals("SOL", Dinero.sol(5).getTipo());
}
```

B. Cambiamos el constructor, y como estos dos se comportan igual vamos
a pasarlo al padre

```java
public class Peso extends Dinero {

  public Peso(int cantidad, String tipo) {
    super(cantidad, tipo);
  }...
}
public class Sol extends Dinero {

  public Sol(int cantidad, String tipo) {
    super(cantidad, tipo);
  }...
}
```

B. Cambiamos el padre para agregar y obtener la nueva variable.

```java
public abstract class Dinero {

+ private String tipo;

+ public Dinero(int cantidad, String tipo) {
    this.cantidad = cantidad;
    this.tipo = tipo;
  }

+ public String getTipo() {
    return tipo;
  }
//SE NOS ROMPERAN TODOS LOS LUGARES DONDE USEMOS EL CONSTRUCTOR PERO
//GRACIAS A LA REFACTORIZACION DE ANTES SOLO ES AQUI
public static Dinero peso(int cantidad) {
//return new Peso(cantidad);
  return new Peso(cantidad, "PESO");
}
public static Dinero sol(int cantidad) { return new Sol(cantidad, "SOL"); }
```

c. Corremos los test y deben de seguir pasando todosy el nuvo .

<!--

#########################################

-->

----

<!--

#########################################

-->

### 25. Refactorizar multiplicacion

**Objetivo:** Como vemos la funcion de la multiplicacion es muy similar,
por lo que podemos pasarla al padre, para esto ya no compareremos que
las clases sean iguales, si no que el tipo de moneda sea igual, que es
lo que distinge estre una clase y otra.

A. Lost tests los dejamos igual.

B. Pasamos la funcion multiplicar al padre y creamos una instancia
Dinero, lo cual necesitamos quitar que sea abstracta, y le pasamos el
tipo.

```java
public class Dinero { // le quitamos abstract
  ...
  public Dinero multiplicarPor(int multi) {
    //retornamos una instancia Dinero, pero indicamos su tipo por this que se asigno a la hora de crear la moneda
    return new Dinero(this.cantidad*multi, this.tipo);
  }

  //ventajas de tener la instanciacion solo aqui
  public static Dinero peso(int cantidad) { return new Peso(cantidad, "PESO"); }
  public static Dinero sol(int cantidad) { return new Sol(cantidad, "SOL"); }

C. Si corremos los test estos van a fallar, esto es por que comparamos
el tipo de clase, y podemos lograr el mismo resultado comparando el tipo.
  
  @Override public boolean equals(Object toCompareFlat) {
    Dinero toCompare = (Dinero)toCompareFlat;
    return this.cantidad == toCompare.cantidad
           && this.tipo == toCompare.tipo;
//         && this.getClass().equals(toCompareFlat.getClass());
  }
```

D. Y eliminamos la funcion multiplicar de Sol y Peso, lo cual nos
quedaria lo siguiente.

```java
public class Peso extends Dinero {
  public Peso(int cantidad, String tipo) {
    super(cantidad, tipo);
  }
}

public class Sol extends Dinero {
  public Sol(int cantidad, String tipo) {
    super(cantidad, tipo);
  }
}
```

**Resultado** Refactorisamos pasando la multiplicacion a padre para
reutilizarlo, y cambiamos el enfoque de como identificar los tipos de
moneda que tenemos.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 26. Refactorizacion Clases de mas y Test duplicados

**Problema:** Si nos fijamos en "Peso" y "Sol" ya no tienen razon de ser
estas clases, lo unico que hacen es usar el constructor del padre, lo
que es algo que huele mal, para **soluccionarlo** eliminar estas clases
y nos quedaremos solo con "Dinero" ya que con esta nos es suficiente.

A. Eliminamos las clases "Peso" y "Sol".

B. Cambiamos las instancias que se crean.

```java
public static Dinero peso(int cantidad) {
//return new Peso(cantidad, "PESO");
  return new Dinero(cantidad, "PESO");
}

public static Dinero sol(int cantidad) {
//return new Sol(cantidad, "PESO");
  return new Dinero(cantidad, "SOL");
}
```

C. Corremos los test y no deberia de fallar ninguno, pero si nos fijamos
**tenemos duplicacion en los tests**, por lo que vamos a eliminar estos
dejando solo.

- Testear multiplicacion
- Testear peso y sol deben ser diferentes
- Testear peso y peso, sol y sol deben ser iguales
- Testear misma moneda diferente cantidad NO son iguales

```java
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
class CurrencyServiceTest {

  // Pesos
  @Test void tesMultiplication() {
    Dinero cinco = Dinero.peso(5);
    Dinero resultado = cinco.multiplicarPor(2);
    assertEquals(Dinero.peso(10), resultado);

    resultado = cinco.multiplicarPor(3);
    assertEquals(Dinero.peso(15), resultado);
  }

  @Test void tesEquals() {
    assertEquals(Dinero.peso(5), Dinero.peso(5));
    assertEquals(Dinero.sol(5), Dinero.sol(5));
  }

  @Test void tesPesoEsDiferenteASol() {
    assertNotEquals(Dinero.peso(5), Dinero.sol(5));
  }

  @Test void tesMismaMonedaDiferenteCantidadNoSonIguales() {
    assertNotEquals(Dinero.sol(1), Dinero.sol(10));
    assertNotEquals(Dinero.peso(2), Dinero.peso(200));
  }

  @Test void tesTipoMoneda() {
    assertEquals("PESO", Dinero.peso(5).getTipo());
    assertEquals("SOL", Dinero.sol(5).getTipo());
  }
/* SOLES
  @Test void tesMultiplicationtSol() {
    Dinero cinco = Dinero.sol(5); Dinero resultado = cinco.multiplicarPor(2);
    assertEquals(Dinero.sol(10), resultado);
    resultado = cinco.multiplicarPor(3);
    assertEquals(Dinero.sol(15), resultado);
  }

  @Test void tesEqualsSol() {
    Dinero cincoA = Dinero.sol(5);
    Dinero cincoB = Dinero.sol(5);
    assertEquals(cincoA, cincoB);
    assertNotEquals(cincoA, Dinero.sol(10));
  }*/
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 27. Iniciar Suma misma moneda

**Objetivo:** De nuestra lista tenemos pendiente la suma.

- Agregarle a pesos unos soles, por ejemplo: 5p+10s=10p
- Agregarle a soles unos pesos, por ejemplo: 5s+10p=25s

A. Empesaremos por lo mas simple que es la suma entre la misma moneda
asi que creamos su test.

> **NOTA:** En el ejemplo del libro el test lo escribe asi donde la
> suma retorna una "Exprecion" y esta la recibe un Banco quien es el
> que hace la operacion.

```java
@Test void tesSumaPesos() {
  Banco banco = new Banco();

  Dinero cinco = Dinero.peso(5);
  Exprecion sum = cinco.sumar(cinco);
  Dinero resultado = banco.hacerOperacion(sum, "PESO");

  assertEquals(Dinero.peso(10), resultado);
}
```

B. crear lo minimo para pasar el test

B.1. Creamos interface que retornara nuestra suma

```java
public interface IExprecion { }
```

B.1. En "Dinero" creamos

```java
public IExprecion sumar(Dinero diez) {
  return null;
}
```

B.2. crear la clase Banco, el cual retorna Peso de 10 para pasar el
test

```java
public class Banco {
  public Dinero hacerOperacion(IExprecion sum, String string) {
    return Dinero.peso(10);
  }
}
```

C. Creamos el test y deberia de pasar en verde.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 28. Make It

> Terminologia:
>
> - **Augend (sumador)**: Es el primer numero de la suma ej, es el "3 "de "3+6"
> - **Added (sumado)**: Es el segundo numero de la suma ej, es el "6 "de "3+6"

**Objetivo** Para nuestra implementacion deberemos de diferenciar entre
el sumador y el sumado, por lo que agregamos un nuevo test para saber
que este sale bien

```java
@Test void testAsignarSumadoYSumador() {
  Banco banco = new Banco();

  Dinero cinco = Dinero.peso(5);
  IExprecion ExprecionSuma = cinco.sumar(cinco);
  Suma suma = (Suma)ExprecionSuma;

  assertEquals(cinco, suma.sumador);
  assertEquals(cinco, suma.aSumar);
}
```

a. Creamos metodo que realizara la operacion de cada operacion

```java
public interface IExprecion {
  Dinero reduce(String tipo);
}
```

A. Creamos la Clase Suma que implementara a IExprecion para que pueda
ser casteado.

1. Tambien agregamos la asignacion de aSumar y Sumador
2. Creamos metodo que hara la suma

```java
public class Suma implements IExprecion {

  public Dinero aSumar;
  public Dinero sumador;
  
  public Suma(Dinero aSumar, Dinero sumador) {
    this.aSumar = aSumar;
    this.sumador = sumador;
  }

  @Override public Dinero reduce(String tipo) {
    int resp = aSumar.cantidad + sumador.cantidad;
    return new Dinero(resp, tipo);
  }
}
```

B. Hacemos la suma en "hacerOperacion" la cual no es la mejor manera
pero estamos cocentrados en pasar la prueba

```java
public class Banco {
  public Dinero hacerOperacion(IExprecion exp, String tipo) {
    // este casteo no es necesario ya que es implicito, ya que le pasaremos
    // la operacion que queremos realizar
    //Suma suma = (Suma)exp;
    return exp.reduce(tipo);
  }
}
```

D. Corremos la prueba y deberian de pasar todas.

**Resultado:** Estamos haciendo la suma de la misma moneda, con una
aproximacion que cada operacion tiene una clase diferente.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 29. Cambiar

**Objetivo:** Vamos a crear un metodo que convierta de una moneda a otra.

> Convercion: Es el numero por el que se va a mulplicar para convertir una
> unidad, por ejemplo para convertir de pesos a soles, se debe
> multiplicar por "2" siendo este el Convercion, es decir al multiplicar
> 1peso por el Convercion en este caso "2" se obtiene 2sol

A. Escribiemos el test

1. Cuando la misma moneda la convercion siempre es 1
2. una relacion de 1:2 por ejemplo 1p=2s o 8p=16s

```java
@Test void testConvertirAMismaMoneda() {
  // Al convertir 1p a pesos debe dar 1p y Al convertir 1s a soles debe dar 1s
  Banco banco = new Banco();
  assertEquals(1f, banco.obtenerConvercion(Dinero.peso(1), "PESO"));
  assertEquals(1f, banco.obtenerConvercion(Dinero.sol(1), "SOL"));
}

@Test void tesPesoASol() {
  // al convertir 1peso a soles debe dar 2soles (1p=2s)
  Banco banco = new Banco();
  //se multiplica por dos para obtener los soles equivalentes a pesos
  banco.agregarConvercion("PESO", "SOL", 2f);
  assertEquals(Dinero.sol(2), banco.convertir(Dinero.peso(1), "SOL" ));
  assertEquals(Dinero.sol(16), banco.convertir(Dinero.peso(8), "SOL" ));
}
```

B. Creamos objetos de convercion que se conformaran de "de" y "a"

```java
public class Convercion {
  
  private String tipoInicial;
  private String tipoAConvertir;

  public Convercion(String tipoInicial, String tipoAConvertir) {
    this.tipoAConvertir = tipoAConvertir;
    this.tipoInicial = tipoInicial;
  }

  @Override
  public boolean equals(Object obj) {
    if(super.equals(obj)) { return true; }
    if(obj==null || getClass() != obj.getClass()) { return false; }
    Convercion conv = (Convercion)obj;
    return Objects.equals(tipoInicial, conv.tipoInicial) &&
           Objects.equals(tipoAConvertir, conv.tipoAConvertir);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tipoInicial, tipoAConvertir);
  }

}
```

C. Creamos el metodo de objeter convercion, pero esta convercion la
guardaremos dentro de una coleccion de Converciones de banco

1. `converciones`: Para guardar las converciones de monedas y su valor
2. `obtenerConvercion`: Obtendremos por medio de sus clave, por ejemplo
   debemos indicar "quiero obtener la convercion de sol a peso"
3. `agregarRango`: Para agregar Converciones ej. "para convertir de sol
   a peso el multiplicador es 2"
4. `convertir`: Es el que hara la convercion de moneda usando la
   coleccion de converciones.

```java
public class Banco {

+ private HashMap<Convercion, Float> converciones = new HashMap<>();

  public Dinero hacerOperacion(IExprecion exp, String tipo)  {
#   return exp.reduce(this, tipo); // NO SE PA QUE SE LO ESTA PASANDO
  }

+ public float obtenerConvercion(Dinero de, String a) {
    if(de.getTipo().equals(a)) {
      return 1;
    } else {
      return converciones.get(new Convercion(de.getTipo(), a));
    }
  }

+ public void agregarConvercion(String de, String a, float convercion) {
    converciones.put(new Convercion(de, a), convercion);
  }

+ public Dinero convertir(Dinero de, String a) {
    float conv = obtenerConvercion(de, a);
    return new Dinero(((int)(de.cantidad*conv)), a);
  }
  
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Suma Pesos-soles

A. Creamos el test, que nos va a fallar

```java
@Test void tesSumarPesosUnosSoles() {
  // Al sumar 5pesos y 10soles debe ser igual a 10pesos
  Banco banco = new Banco();
  //Necesario si no va a fallar con un null
  banco.agregarConvercion("SOL", "PESO", 0.5f);
  //test 1
  Dinero cincoPeso = Dinero.peso(5);
  Dinero DiesSol = Dinero.sol(10);
  IExprecion sumExp = cincoPeso.sumar(DiesSol);
  Dinero resultado = banco.hacerOperacion(sumExp, "PESO");
  assertEquals(Dinero.peso(10), resultado);
  //test 2
  Dinero unPeso = Dinero.peso(1);
  Dinero dosSol = Dinero.sol(2);
  sumExp = unPeso.sumar(dosSol);
  resultado = banco.hacerOperacion(sumExp, "PESO");
  assertEquals(Dinero.peso(2), resultado);
}
```

B. Modificamos la clase de suma para que combierta la moneda y luego
sume para tener

```java
@Override
// se llama desde "hacerOperacion" que indica la moneda a manejar
public Dinero reduce(Banco banco, String tipo) {
  Dinero aSumarComv = banco.convertir(aSumar, tipo);
  Dinero sumadorComv = banco.convertir(sumador, tipo);
  int resp =  aSumarComv.cantidad + sumadorComv.cantidad;
  return new Dinero(resp, tipo);
}
```

**Resultado:** Ya podemos sumarle a pesos unos soles.

<!--

#########################################

-->

----

<!--

#########################################

-->

### Conclucion

Dejamos hasta aqui nuestro ejemplo, donde ya vimos el flujo basico de
crear tests, donde empezamos por los tests, implementacion y al final
la refactorizacion que es un proceso repetitivo.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Sección 4:Getting Started with JUnit 5

JUnit fue creado por "Kent Beck" famoso escritor y creador de otras
buenas practicas como "eXtrem programing" y ptras cosas. JUnit5 fue
creado para ser compatible con java8

- Junit Plataform: Es una fundacion para lanzar frameworks en la JVM,
  permitiendo tests en consola o con herramientas como maven o gradle
- Junit Jupiter: Es un modelo de programacion para extender JUnit
- Junit Vintage: Provee retro-compatibilidad con JUnit3 y JUnit4

Entre las anotaciones podemos encontrar

- `@Test`: Marca un metodo como un test
- `@ParameterizedTest`: Permite al metodo tomar parametros
- `@RepeatedTest`: Repite los test N veces
- `@TestFactory`: Fabrica de tests estos seran dinamicos
- `@TestInstance`: Se usa para configurar el ciclo de vida de la
  intancia
- `@TestTemplate`: Crea una plantilla que podemos usar en otros tests
- `@DisplayName`: Asignar nombres amigables a los tests
- `@BeforeEach`: Declarar un metodo que se ejecutara antes de cada test
- `@AfterEach`: Declarar un metodo que se ejecutara despues de cada test
- `@BeforeAll`: Metodo estatico para correr antes de los tests
- `@AfterAll`: Metodo estatico para correr despues de los tests
- `@Nested`: Crea pruebas anidadas
- `@Tag`: Asignar filtros para luego poder filtrarlos
- `@Disabled`: Desactiva un test
- `@ExtendWith`: Registrar extenciones como Mockito y Spring

Ciclo de vida de los tests

![./img/01-correr-tests](./img/02-tests-ciclo-de-vida.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Iniciando proyecto, Configurando Maven y los tests

A. Creamos un proyecto maven

```r
mvn -B archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=com.mycompany.app -DartifactId=my-app
```

B. Nos haceguramos de tener lo siguiente en el pom

```xml
<properties>
    <java.version>1.8</java.version>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <junit-platform.version>5.3.1</junit-platform.version>
    <!-- Si queremos ejecutar con "mvn clean compile exec:java"-->
    <exec.mainClass>mario.App</exec.mainClass>
  </properties>

<dependencies>
  <dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>3.8.1</version>
    <scope>test</scope>
  </dependency>

  <dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-api</artifactId>
    <version>${junit-platform.version}</version>
    <scope>test</scope>
  </dependency>

  <dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-engine</artifactId>
    <version>${junit-platform.version}</version>
    <scope>test</scope>
  </dependency>

</dependencies>

  <build>
      <plugins>
          <plugin> <!-- NOS ASEGURAMOS QUE ESTE ESTE PARA QUE CORRA EL "mvn test"-->
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>3.0.0-M5</version>
          </plugin>
      </plugins>
  </build>
```

C. Creamos nuestra clase a testear

```java
package mario.saludo;

public class Saludo {
  private static String hola = "HOLA";
  private static String mundo = "MUNDO";

  public String saludar() {
    return hola + " " + mundo;
  }

  public String saludarPersona(String nombre) {
    return hola + " " + mundo + ": " + nombre;
  }
}
```

D. Creamos un test y lo ejecutamos para ver que las dependencias
esten bien.

```java
package mario.saludo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class SaludoTest {
  
  @Test public void testSaludo() {
    Saludo saludo = new Saludo();
    assertEquals("HOLA MUNDO", saludo.saludar());
  }

  @Test public void testSaludoPersona() {
    Saludo saludo = new Saludo();
    assertEquals("HOLA MUNDO: Mario", saludo.saludarPersona("Mario"));
  }
}
```

D. Lo corremos desde nuestro IDE o desde maven con `mvn test`

**Resultado** Ejecutamos nuestros test desde IDE o desde maven

```log
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running mario.AppTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.007 sec
Running mario.saludo.SaludoTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Results :
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 38. Uso de @BeforeEach y @BeforeEach, y @BeforeAll y @AfterAll

**Objetivo:** Si nos fijamos en "SaludoTest" veremos que tenemos codigo
duplicado, ya que estamos creando una instancia de Saludo, y para
eliminarlo podemos hacer uso de "@BeforeEach" que se ejecutara por cada
uno de los test, creando una instancia para cada metodo.

A. Cremos los test con logs para ver como se ejecuta el ciclo de vida

![ciclo de vida](./img/02-tests-ciclo-de-vida.PNG)

<!-- 
Crear loggin para test, imprimir logs en tests, logs en tests
-->

```java
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.logging.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SaludoTest {
  
  Saludo saludo;

  private static Logger log = Logger.getLogger(SaludoTest.class.getName());

  @BeforeAll public static void beforeClass() {
    log.info("Iniciando Tests de: " + SaludoTest.class.getName());
  }

  @BeforeEach public void setup() {
    log.info("Iniciando Test...");
    saludo = new Saludo();
  }
  
  @Test public void testSaludo() {
    //Saludo saludo = new Saludo();
    log.info("Prueba> " + new Object(){}.getClass().getEnclosingMethod().getName());
    assertEquals("HOLA MUNDO", saludo.saludar());
  }

  @Test public void testSaludoPersona() {
    //Saludo saludo = new Saludo();
    log.info("Prueba> " + new Object(){}.getClass().getEnclosingMethod().getName());
    assertEquals("HOLA MUNDO: Mario", saludo.saludarPersona("Mario"));
  }

  @AfterEach public void teardown() {
    log.info("------- Test terminado");
  }

  @AfterAll public static void afterClass() {
    log.info("===================== terminando Tests de: " + SaludoTest.class.getName());
  }
}
```

B. Ejecutamos con nuestro IDE (VS-Code en mi caso en `DEBUG CONSOLE`)
y veremos el seguiente resultado.

```log
Iniciando Tests de: mario.saludo.SaludoTest
Iniciando Test...
------- Test terminado
Iniciando Test...
------- Test terminado
===================== terminando Tests de: mario.saludo.SaludoTest
```

C. Ahora ejecutamos con maven `mvn test`

> - ERROR: `java.lang.NullPointerException at mario.saludo.SaludoTest.testSaludoPersona(SaludoTest.java:37)`
> - DESCRIPCION: Al ejecutar los test fallan testSaludo y testSaludoPersona
> ambos sacando el mismo error de nullPoint.
> - CAUSA: Maven por defecto no incluye una plataforma de tests por
> lo que devemos de agregarsela manualmente
> - SOLUCION: agregar en el `pom.xml` el siguiente build para que se
> agrege la plataforma de tests que queremos en mi caso.

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>mario</groupId>
  <artifactId>holaJUnit</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>holaJUnit</name>
  <url>http://maven.apache.org</url>
  
  <properties>
    <java.version>1.8</java.version>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <junit-platform.version>5.6.2</junit-platform.version>
    <exec.mainClass>mario.App</exec.mainClass>
  </properties>
  
  <dependencies> <!-- NOS ASEGURAMOS QUE ESTE ESTA -->
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-api</artifactId>
      <version>${junit-platform.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>


  <build>
      <plugins>
          <plugin> <!-- NOS ASEGURAMOS QUE ESTE ESTE -->
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>3.0.0-M5</version>
          </plugin>
      </plugins>
  </build>
</project>

```

**Resultado:** Podemos correr los tests correctamente ya sea por medio
del IDE o por maven, incluyendo partes del before y after, en el caso
de maven me da la siguiente salida.

```log
[INFO]
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running mario.AppTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.049 s - in mario.AppTest
[INFO] Running mario.entity.PersonaTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.006 s - in mario.entity.PersonaTest
[INFO] Running mario.saludo.SaludoTest
Jul 07, 2020 2:09:28 PM mario.saludo.SaludoTest beforeClass
INFO: Iniciando Tests de: mario.saludo.SaludoTest
Jul 07, 2020 2:09:28 PM mario.saludo.SaludoTest setup
INFO: Iniciando Test...
Jul 07, 2020 2:09:28 PM mario.saludo.SaludoTest testSaludoPersona
INFO: Prueba> testSaludoPersona
Jul 07, 2020 2:09:28 PM mario.saludo.SaludoTest teardown
INFO: ------- Test terminado
Jul 07, 2020 2:09:28 PM mario.saludo.SaludoTest setup
INFO: Iniciando Test...
Jul 07, 2020 2:09:28 PM mario.saludo.SaludoTest testSaludo
INFO: Prueba> testSaludo
Jul 07, 2020 2:09:28 PM mario.saludo.SaludoTest teardown
INFO: ------- Test terminado
Jul 07, 2020 2:09:28 PM mario.saludo.SaludoTest afterClass
INFO: ===================== terminando Tests de: mario.saludo.SaludoTest
[INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.154 s - in mario.saludo.SaludoTest
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 4, Failures: 0, Errors: 0, Skipped: 0
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  02:14 min
[INFO] Finished at: 2020-07-07T14:09:29-05:00
[INFO] ------------------------------------------------------------------------
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 48. assertAll: Testeando POJOs/DTOs checar constructores

**Objetivo:** Vamos a checar que un costructor asigne correctamente los
datos que le pasamos, y para no tener varios assets vamos a crear un
conjunto de estos para tenerlos mas ordenados.

A. Creamos nuestro test, notar que para hacer el conjunto de asserts
usamos expreciones lamda que solo son compatibles con java8 y superior

```java
public class PersonaTest {
  @Test public void testConstructorPersona() {
    //dado
    int idExpected = 1;
    String nombreExpected = "Sancho";
    String apellidoExpected = "Pansa";
    Persona persona = new Persona(idExpected, nombreExpected, apellidoExpected);
    //entonces
    assertAll("Testear asignacion de valores de costructor Persona",
        ()->assertEquals(idExpected, persona.getId()),
        ()->assertEquals(nombreExpected, persona.getNombre()),
        ()->assertEquals(apellidoExpected, persona.getApellidos())
    );
  }
  // lo mismo para getters/setters
}
```

B. Creamos nuestro pojo

```java
public class Persona {
  private int id;
  private String nombre;
  private String apellidos;
  public Persona(int id, String nombre, String apellidos) {
    this.id = id;
    this.nombre = nombre;
    this.apellidos = apellidos;
  }
  //GETTERS---SETTERS
}
```

**Resultado:** Hizimos uso de assertAll para asegurar que la asignacion
de valores por medio del constructor y los setters/getters

<!--

#########################################

-->

----

<!--

#########################################

-->

### Anidacion de tests, test a constructor de clase padre e hija

**Objetivo:** Vamos a tener una una sub clase llamada Jefe que sera
una sub-clase de Persona, y para testear el costructor y setters vamos
a hacer uso de anidar los tests para separar la comprovacion de Jefe
y de persona.

A. Dada nuestra clase Jefe, (supomngamos que ya la tenemos)

```java
public class Jefe extends Persona {
  
  private String departamento;
  private boolean permisosCt;

  public Jefe(int id, String nombre, String apellidos, String departamento, boolean permisosCt) {
    super(id, nombre, apellidos);
    this.departamento = departamento;
    this.permisosCt = permisosCt;
  }
  //public void set...
}
```

B. Creamos nuestro tests para checar el constructor de Persona y Jefe

```java
@Test
public void testConstructorPersonaYJefe() {
  //dado Persona
  int idExpected = 1;
  String nombreExpected = "Sancho";
  String apellidoExpected = "Pansa";
  Persona persona = new Persona(idExpected, nombreExpected, apellidoExpected);
  //Dado jefe
  int idJefeExpected = 2;
  String nombreJefeExpected = "Pedro";
  String apellidoJefeExpected = "Pedratales";
  String departamentoJefeExpected = "Esignaciones";
  boolean permisosCtJefeExpected = false;
  Jefe jefe = new Jefe(idJefeExpected, nombreJefeExpected, apellidoJefeExpected, departamentoJefeExpected, permisosCtJefeExpected);
  
  //entonces
  assertAll(
    () -> {
      assertAll("Testear asignacion de valores de costructor Persona",
        ()->assertEquals(idExpected, persona.getId()),
        ()->assertEquals(nombreExpected, persona.getNombre()),
        ()->assertEquals(apellidoExpected, persona.getApellidos())
      );
    },
    () -> {
      assertAll("Testear asignacion de valores de costructor Jefe",
        ()->assertEquals(idJefeExpected, jefe.getId()),
        ()->assertEquals(nombreJefeExpected, jefe.getNombre()),
        ()->assertEquals(apellidoJefeExpected, jefe.getApellidos()),
        ()->assertEquals(departamentoJefeExpected, jefe.getDepartamento()),
        ()->assertEquals(permisosCtJefeExpected, jefe.isPermisosCt())
      );
    }
  );
}
```

**Resultado:** Estamos haciendo anidacion de tests que nos servira para
ordenarlos, y si nos da un problema nos dira los siguiente en maven

```log
[ERROR] Tests run: 1, Failures: 1, Errors: 0, Skipped: 0, Time elapsed: 0.021s
[ERROR] in mario.entity.PersonaTest
[ERROR] testConstructorPersonaYJefe  Time elapsed: 0.008 s  <<< FAILURE!
org.opentest4j.MultipleFailuresError:
Multiple Failures (1 failure)
        Testear asignacion de valores de costructor Jefe (1 failure)
        expected: <Pedro> but was: <PedroB>
        at mario.entity.PersonaTest.testConstructorPersonaYJefe(PersonaTest.java:26)
```

**Problema:** No nos indica exactemente que falla o en que linea falla,
por lo que debemos de buscar manualmente, lo cual pude ser un problema,
podemos agregar lo siguiente para que nos diga que fallo exactamente

```java
assertAll(
      () -> { ... },
      () -> {
        assertAll("Testear asignacion de valores de costructor Jefe",
          ...
+         ()->assertEquals(apellidoJefeExpected, jefe.getApellidos()+"a", "Fallo asginacion de apellidos en: Jefe"),
```

Y al fallar nos mandara el mensaje que indicamos haciendo que encontremos
el error de manera mas exacta

```log
[ERROR] Tests run: 1, Failures: 1, Errors: 0, Skipped: 0, Time elapsed: 0.021s
[ERROR] in mario.entity.PersonaTest
[ERROR] testConstructorPersonaYJefe  Time elapsed: 0.002 s  <<< FAILURE!
org.opentest4j.MultipleFailuresError:
Multiple Failures (1 failure)
        Testear asignacion de valores de costructor Jefe (1 failure)
        Fallo asginacion de apellidos en: Jefe ==> expected: <Pedratales> but was: <Pedratalesa>
        at mario.entity.PersonaTest.testConstructorPersonaYJefe(PersonaTest.java:26)
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 50. Skipping JUnit Tests

**Objetivo:** Vamos a aprender a desactivar o saltar un test que por
X o Y vamos a querer hacer esto.

```java
@Disabled // para bloquer todos los que estan en la clase
public class PersonaTest {

  // para bloquear solo uno, y le damos una explicacion del por que se desabilito
  @Disabled(value = "Lo desactivamos por que se desavilito el modulo")
  @Test public void testConstructorPersonaYJefe() {...
```

Al correr los tests nos mostrara lo siguiente

```log
[WARNING] Tests run:1, Failures:0, Errors:0, Skipped:"1", Time elapsed: 0.007s
in mario.entity.PersonaTest
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 51. @DisplayName

Esta etiqueta nos sirve para darle un nombre descriptivo a nuestro
tests.

```java
public class PersonaTest {
  @DisplayName("Test Creacion de Persona y Jefe")
  @Test public void testConstructorPersonaYJefe() {...
```

**Resultado:** Podemos ver el nombre

![./img/03-displayname](./img/03-displayname.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Expected Exceptions

**Objetivo:** Vamos a testear que un metodo nos de una determinada
excepcion que esperamos, para que tenga un correcto funcionamiento.

A. Tenemos el siguiente "controlador" que debe mandar excepciones
cuando le dan un id menor a 0.

```java
public class FrontController {
  public String home(int id) {
    if(id<0) { throw new IdNotFoundException(); }
    return "ok";
  }
}
```

B. Creamos nuestro test

```java
@Test
  @DisplayName("Id menores a cero en home no existen")
  public void testIdNotFoundExeption() {
    FrontController frontController = new FrontController();
    assertThrows(IdNotFoundException.class, ()->{frontController.home(-1);}, "Un Id");
  }
```

**Resultado:** Nos aseguramos que la excepcion salga tal como queremos

<!--

#########################################

-->

----

<!--

#########################################

-->

### 53. Testing Timeouts

**Objetivo:** Podemos hacer pruebas de rendimiento, pero hay que tener
cuidado ya que estas dependeran del CPU donde se corran.

**Nota:** Tenemos 2 formas un test donde si se termina el tiempo este
se seguira ejecutando asta que termine, y otro (Preemptively) que
detendra el test

```java
@Test public void testTimeout() {
  //este test durara 110ms
  assertTimeout(Duration.ofMillis(100), ()-> {
    Thread.sleep(110);
    log.info("Termino test"); // si lo veremos en la consola
  });
}

@Test public void testTimeoutPrevent() {
  //este test durara 100ms
  assertTimeoutPreemptively(Duration.ofMillis(100), ()-> {
    Thread.sleep(110);
    log.info("Termino test 222"); // NO lo veremos en la consola
  });
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 54. JUnit Assumptions

Las asunciones(Assumptions) son diferentes a las aseguraciones(assert)
ya que las asunciones solo correran en determinadas condiciones, por
ejemplo, el entorno, fuente de datos, usuario, etc.

Un assert que no pase se conviderara una falla, mientras que un assume
que falle sera conciderado un aborto/ignorado.

**Objetivo:** Vamos a crear un supuesto donde se vera si estamos en un
entorno PROD o TEST, el cual si no es asi este no se ejecutara.

```java
//este test no se cumple pero se tomara como ignorado/abortado, pero
//no marcara que algo fallo
@Test public void testEnvPROD() {
  assumeTrue("TEST".equals(System.getenv("ENTORNO")));
  //no se ejecutaran las siguientes lineas
  log.info("Fin Assume1");
  assertTrue(true);//el no llegara a esta parte por lo que seguira
                   //siendo tomandose como ignorado
}

//este test pasara como exitoso
@Test public void testEnvTest() {
  assumeTrue("TEST".equals("TEST"));
  log.info("Fin Assume2");
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 55. Conditional JUnit Test Execution

Fuente: [Documentacion](https://junit.org/junit5/docs/current/user-guide/#writing-tests-conditional-execution)

**Aprenderemos:** como ejecutar o NO un test segun una condicion, tenemos
diferentes opciones como por

- Sistema operativo
- Version de maquina virtual
- Variables de entorno
- Script-bases (ej. @DisableIf("'PROD' == getSomething()"))
- usuario quien ejecuta el programa
- etc

```java
@EnabledOnOs(OS.WINDOWS)
@Test public void tes() { ... }

@EnabledOnOs(OS.MAC)
@Test public void test() { ... }

@EnabledOnJre(JRE.JAVA_8)
@Test public void test() { ... }

@EnabledOnJre(JRE.JAVA_11)
@Test public void test() { ... }

//podemos checarlo con "echo $USER"
@EnabledIfEnvironmentVariable(named="USER", matches = "mario")
@Test public void test() { ... }

//podemos checarlo con "echo $USER"
@EnabledIfEnvironmentVariable(named="USER", matches = "mario")
@Test public void test() { ... }
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 56. Using AssertJ with JUnit

- Fuente: [Documentacion](https://assertj.github.io/doc/)

**Teoria:** AssertJ es una libreria que podemos usar dentro de JUnit
que hara nos hara mas facil los tests y de lectura mas facil.

**Objetivo:** Vamos a agregarlo para usarlo dentro de JUnit, pero nove
remos a prefundidad ya que es un tema extenso, y se puede hacer lo mismo
con JUnit solo que mas bonito.

A. Agregamos Dependencia de AssertJ

```xml
<dependency>
  <groupId>org.assertj</groupId>
  <artifactId>assertj-core</artifactId>
  <version>3.16.1</version> <!-- use 2.9.1 for Java 7 projects -->
  <scope>test</scope>
</dependency>
```

B. Ya poremos usarlo

```java
@Test public void testEnvTest2() {
  FrontController frontController = new FrontController();
  org.assertj.core.api.Assertions.assertThat(frontController.home(1)).isEqualTo("ok")
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 60. Conclucion

Vimos los casos mas comunes de unit testings los cuales lo que usamos
sera el mas o menos un 80% en los casos comunes.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, separacion seccion separacion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, separacion seccion separacion

.

-->

----

## Seccion 6: Advanced JUnit Testing

En esta seccion veremos en caracteristicas avanzadas de Junit que tarde
o temprano nos crusaremos con ellas pero seran casos mas raros, mas o
menos un 20% en aplicaciones reales.

----

### 62. Tagging and Filtering JUnit Tests

Los tags nos sirven para agrupar nuestros tests en alguna categoria
y asi poderlos encontrar de una manera mas facil, pudiendo asi tambien
ejecutar solo los que tengan cierto o ciertos tags, y podemos aplicarlo
a nivel de clase o de tests.

**Nota:** Un uso comun es para separar

A. Agregamos los tags que queremos

```java
@Tag("controller")
public class FrontControllerTest {
  @Test
  @Tag("failExpected")
  public void testIdNotFoundExeption() {
    FrontController frontController = new FrontController();
    assertThrows(IdNotFoundException.class, ()->{frontController.home(-1);}, "Un Id");
  }
```

<!-- 
ejecutar solo tests con x tag, ejecutar tests por tag, ejecutar por tag
correr solo tests con x tag, correr tests por tag, correr por tag
-->

B. Ejecutar tests con x tag: `mvn test -Dgroups=failExpected`

C. NO ejecutar tests con x tag: `mvn test -DexcludedGroups=controller`

**Resultado:** Tageamos nuestros tests los que nos facilitara su
agrupacion y busqueda, y ejecutamos o evitamos ejecutar los tests que
tengan solo x tag usando maven, pero igual se puede desde el IDE.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 63. JUnit Nested(anidado) Tests

Con estas podemos crear tests anidados los cuales estaran en su propia
clase lo cual nos da la ventaja de tener otras anotaciones before y after

A. Tenemos nuestra clase que queremos testear

```java
class EmpleadosRepository {
  private HashMap empleados = new HashMap<Integer, Empleado>();

  public Empleado agregar(Empleado toAdd) { ... }
  public Empleado searchById(int id) { ... }
  public HashMap<Integer, Empleado> getAll(int id) { ... }
  public Empleado update(Empleado toDelete) { ... }
  public Empleado eliminar(int IdtoDelete) { ... }

}
```

B. podemos tener el siguiente test

```java
class EmpleadosRepositoryTest {
  
  @BeforeEach setup1() { .... }

  @Test public iniciarConCero() { ... }

  @Test public eliminarActualizarBuscarCuandoNohayNada() {}

  //TENEMOS UNA TESTS/CLASE ANIDADA
  @Nested class TestCreateEmpleados {
    @BeforeEach setup2() { .... }
    @Test public iniciarConCero() { ... } //SE EJECUTARA SETUP1 LUEGO SETUP2
  }
}
```

**Resultado:** Podemos tener un ciclo de vida dentro de otro

<!--

#########################################

-->

----

<!--

#########################################

-->

### 64. Pasar anotaciones por interface a tests

Podemos implementar una interface sobre nuestras clases que contien los
tests y las anotaciones que tengan estas interfaces se las pasaran a
nuestras clases, por ejemplo.

A. Creamos unas interfaces con los siguientes anotaciones

```java
@Tag("ModelTests")
public interface ModelTests { }

----

@Tag("ControlerTests")
public interface ControllerTests { }
```

B. Implementamos dichas implementaciones para tener las anotaciones
puestas en nuestras clases.

```java
class HomeDto implements ModelTests { ... }
class EmpleadoEntity implements ModelTests { ... }
class EstudianteEntity implements ModelTests { ... }

----

class ControllerFrontOffice implements ControllerTests { ... }
class ControllerEmpleados implements ControllerTests { ... }
class ControllerEstudiantes implements ControllerTests { ... }
```

**Resultado:** Podremos ejecutar los tests ya sea por "ModelTests" o
por "ControlerTests" y se ejecutaran los tests dentro de clas clases
con la anotacion.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 66. Repetir tests

El test se repetira 5 veces

```java
@RepeatedTest(5)
@Test public void testRepeat() {
  log.info("testRepeat");
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 66. Argumentos en anotaciones de tests

Cuando corremos algun tests Junit puede pasarle unos prametros por
defecto los cuales podemos usar, por ejemplo para obtener el total
de repeticiones, el tag que tiene el test o etc.

```java
@Test
@RepeatedTest(5)
public void testRepeat2(TestInfo testInfo, RepetitionInfo repetitionInfo) {
  log.info(testInfo.getDisplayName() + " - " + repetitionInfo.getCurrentRepetition());
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 73. Usar parametros en tests: .CSV en codigo

**Objetivo:** Esto nos puede servir por ejemplo para definir un o varios
escenarios dentro de un csv, ta.

A. Agregamos dependencia para poder usar parametros

```xml
<dependency>
  <groupId>org.junit.jupiter</groupId>
  <artifactId>junit-jupiter-params</artifactId>
  <version>${junit-platform.version}</version>
  <scope>test</scope>
</dependency>

```

B. Creamos nuestro tests

```java
@Test
@ParameterizedTest(name = "{displayName} - [{index}] {arguments}")
@CsvSource({
  "mario, 27, TICs, true",
  "neey, 25, LAWS, false",
  "beto, 51, TESTS, true"
})
public void testparametrizado(String val1, int val2, boolean val3) {
  log.info(val1 + ", " + val2 + ", " + val3);
}
```

C. Salida por consola.

```js
INFO: mario, 27, TICs, true
INFO: neey, 25, LAWS, false
INFO: beto, 51, TESTS, true
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 73. Usar parametros en tests: .CSV en archivo

Es lo mismo del anterior solo que guardamos el .csv en test/resources

A. Guardamos csv en `src\test\resources\test_input_case_a.csv`

```csv
val1,   val2, val3,   val4
mario,  27,   TICs,   true
neey,   25,   LAWS,   false
beto,   51,   TESTS,  true
```

B. Creamos nuestro tests

```java
@Test
@ParameterizedTest(name = "{displayName} - [{index}] {arguments}")
@CsvFileSource(resources = "/test_input_case_a.csv")
public void testparametrizado2(String val1, int val2, String val3, boolean val4) {
  log.info(val1 + ", " + val2 + ", " + val3 + ", " + val4);
}
```

C. tendremos la misma salida por consola del anterior

<!--

#########################################

-->

----

<!--

#########################################

-->

### 78. Extenciones JUnit, Crear una extencion

Las extenciones en JUnit nos dan la posivilidad de agregar funcionalidad
a nuestros tests, como lo es Mockito (falsear objetos), o Spring (para
testear respuestas http, JPA, etc). Pero estas las veremos en su momento

En este caso veremos como se crear una extencion que es lo mismo que
hace Spring o Mocketo solo cambia la escala.

A. Creamos la extencion

```java
import java.util.logging.Logger;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import java.lang.reflect.Method;

public class TimingExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {

  private static Logger log = Logger.getLogger(TimingExtension.class.getName());
  private static final String START_TIME = "START_TIME";

  @Override public void beforeTestExecution(ExtensionContext context) throws Exception {
    getStore(context).put(START_TIME, System.currentTimeMillis());;
  }
  
  @Override public void afterTestExecution(ExtensionContext context) throws Exception {
    Method testMethod = context.getRequiredTestMethod();
    long startTime = getStore(context).remove(START_TIME, long.class);
    long duration = System.currentTimeMillis() - startTime;
    log.info(() -> String.format("Method [%s] took %s ms", testMethod.getName(), duration));
  }

  private ExtensionContext.Store getStore(ExtensionContext context) {
    return context.getStore(ExtensionContext.Namespace.create(getClass(), context.getRequiredTestMethod()));
  }
}

```

<!-- añadir extencion, agregar extencion-->

B. implementamos la extencion con `ExtendWith`

```java
import mario.MisExtenciones.TimingExtension;
@ExtendWith(TimingExtension.class)
public class BackControllerTest {
  @Test public void test25() { assertTimeout(Duration.ofMillis(1000), ()-> { Thread.sleep(25); }); }
  @Test public void test50() { assertTimeout(Duration.ofMillis(1000), ()-> { Thread.sleep(50); }); }
  @Test public void test75() { assertTimeout(Duration.ofMillis(1000), ()-> { Thread.sleep(75); }); }
  @Test public void test100() { assertTimeout(Duration.ofMillis(1000), ()-> { Thread.sleep(100); }); }
}
```

**Resultado:** Como vemos nuestro test no tiene mas que el assert pero
como le agregamos nuestra extencion podemos ver los logs de cuanto
tomo cada uno de los tests.

```log
INFO: Method [test100] took 159 ms
INFO: Method [test25] took 27 ms
INFO: Method [test50] took 50 ms
INFO: Method [test75] took 77 ms
```

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, separacion seccion separacion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, separacion seccion separacion

.

-->

----

## Sección 9: Getting Started with Mockito

Mockito es una extencion/libreria de JUnit que nos permitira crear
objetos falsos, para poder testear las clases que tengan alguna
inyecccion de clases, esta inyeccion la podremos simular/falsear para
no tener que traer el objeto original que suelen ser pesadas. Por
ejemplo, tenemos el Spring-Context para poder testear funciones con
Web-mvc, jpa, bachs y etc. lo cual podemos flsear cualquiera de estos
y poder centrarnos en testear lo que queremos.

Por ejemlo, al testear si funciona el login, realmente no estamos testeando
que el usaurio/contraseña coincidan con la base de datos, si no estamos
testeando que es que el algoritmo funciona bien con un objeto que dice
que pasa bien la autenticacion, por lo que podemos falsear la base de datos.

Tipos de Mocks

- **Dummy**: Usados solo para que el codigo pueda compilar
- **fake**: Objeto que tiene una implementacion pero no para produccion
- **Stub**: Objeto con respuestas predefinidas en llamada de metodos
- **Mock**: Un Stub mas espectativas de ejecucion, el cual puede mandar
  exepciones si una invocacion inesperada ocurre, lo cual puede ser
  provicado por los argumentos.
- **Spy**: Es un mok con un wrapper

Termilogia

- **Verify**: Usado para verificar las veces que ha sido llamado un
  metodo del objeto falseado, por ejemplo, queremos que un metodo
  dentro del objeto solo se llame 1 vez o de cero a n veces.
- **Argument Matcher:** Son argumentos para el mock para que coincidan,
  por ejemplo, espero una cadena "Pedro" dentro del metodo llamado.
- **Argument captor:** Captura argumentos pasados al metodo mock,
  asegurando que metodos se le pasaron a este metodo.

Anotaciones

- **@Mock:** Usado para crear un mock
- **@Spy:** Crear un spy
- **@InjectMocks:** inyecta mocks/spys a una clase bajo test
- **Captor:** Captura argumentos del mock

<!--

#########################################

-->

----

<!--

#########################################

-->

### 102.102 Crear primer mock

**Objetivo:** Vamos a agregar lo necesario para poder usar Mockito y
crearemos uno para ver que lo hicimos correctamente.

A. Agregamos las dependencias "mockito-core" y "mockito-junit-jupiter"
(este ultimo para integracion), en el pom.xml con la misma version

```xml
<properties>
  <junit-mockito.version>3.3.3</junit-mockito.version>
</properties>

<!-- https://mvnrepository.com/artifact/org.mockito/mockito-core -->
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-core</artifactId>
    <version>${junit-mockito.version}</version>
    <scope>test</scope>
</dependency>

<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-junit-jupiter</artifactId>
    <version>${junit-mockito.version}</version>
    <scope>test</scope>
</dependency>
```

B. Usamos mock ejecutando el Test en nuestro IDE como cualquier otro.

```java
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(mario.MyExtencions.TimingExtension.class)
public class HolaMockito {
  @Test public void testMockito() {
    Map myMOck = mock(Map.class);
    assertEquals(0, myMOck.size());
  }
}
```

**Nota:** Lo que pasara es que nuestro test pasara, esto por que por
**defecto** Mockito al ver que regresa un entero crea un mock que
regresa un cero.

<!--- 
No se ejecuta test en maven, no se ejecuta clase de tests no se ejecuta 
en maven, no se ejecutan tests en maven, maven no ejecuta tests, maven no 
ejecuta clase de tests, No se corre test en maven, no se corre clase de
tests no se corre, no corren tests en maven, maven no corre tests, maven
no corre clase de tests, no corren los tests, no se ejecutan los tests en maven
maven no ejecuta los tests
-->

> **Error:** Maven no ejecuta tests
> **Descripcion:** Puedo correr los tests en el IDE pero al ejecutar en
> maven no se ejecuta el test ni la clase.
> **Causa-1:** Al parecer maven para ejecutar una clase en sus test esta
> debe de terminar con la palabra "Test".
> **Causa-2:** Puede que tengamos una configuracion diferente de que
> tests incluir, por ejemplo un `<include>**/*Tests.java</include>` lo
> que significa que para que se agregen los tests deben de terminar con
> "Tests.java" de otra manera no se agregaran/ejecutaran.
> **Solucion-1:** Agregar "Test" al nombre de la clase.
> **Solucion-2:** Apegarse o modificar la configuracion de nuestro Include
> en nuestro archivo pom.xml.

```java
// SOLUCION ERROR
...
public class HolaMockitoTest { ...
```

C. Comprobamos que si se ejecute nuestro test en Maven

```log
[INFO] Running mario.saludo.HolaMockitoTest
INFO: Method [testMockito] took 1348 ms
```

**Resultado:** Podemos usar Mockito en nuestro proyecto y funciona tanto
en nuestro IDE como en maven

<!--

#########################################

-->

----

<!--

#########################################

-->

### Crear mock con anotaciones

**Objetivo:** En el codigo anterior creacmos nuestro mock en inline,
en este ejemplo vamos a crearlo por medio de una anotacion, pero para
esto se debe de llamarse a un metodo para que los cree.

A. Creamos

1. Variable de clase para crear nuestro mock con anotacion
2. Creamos un metodo `BeforeEach` para llamar a `initMocks` para que
   cada test tenga su propio mock.

```java
public class HolaMockitoTest {
  @Mock
  Map<String, Object> myMapMock;

  @BeforeEach
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }
  
  @Test public void testMockitoAnotation() {
    assertEquals(0, myMapMock.size());
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 105. Usando Extension de Mockito

Una forma mas facil de crear los mock es con la anotacion pero para
iniciar la creacion de mock usaremos la extencion, para que lo haga
esta y no nosotros.

1. Importante: `@ExtendWith(MockitoExtension.class)`

```java
@ExtendWith(MockitoExtension.class)
public class HolaExtencionMockTest {
  @Mock
  Map<String, Object> myMapMock;
  @Test public void testMockitoAnotation() {
    assertEquals(0, myMapMock.size());
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 106. Crear Mock y verificar que se llama un metodo 1 o n veces

- De este [codigo](./img/04_holaJUnit_inicio_mockito.zip)

**Objetivo:** Tenemos un Service que llama a un metodo de un Repository
para eliminar un Entity, para testear esto debemos de saber que el
metodo `eliminar()` service esta llamando al `eliminar()` del repositorio
para poder saber esto usamos un Mock.

**Nota:** Como estamos usando una libreria (Repository de JPA) que ya
tiene sus propios tests (`SpecialtyRepository`), no es nuestro trabajo
saber si el metodo de eliminar lo hace bien (ya que es trabajo de la
libreria), nuestro trabajo es testear que el se este llamando el
metodo correcto de la libreria.

A. Codigo que vamos a testear.

```java
import java.util.HashSet;
import java.util.Set;

public class SpecialitySDJpaService implements SpecialtyService {

    private final SpecialtyRepository specialtyRepository;

    @Override
    public void delete(Speciality object) {
        specialtyRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        specialtyRepository.deleteById(aLong);
    }
}
```

B. Creamos nuestro test

1. `@Mock...` Creamos mock de la clase que depende nuestro service
2. `@InjectMocks` Crea una instancia y le inyecta el Mock que creamos
3. `verify` Verificamos que el metodo se llame y las veces correctas

**Notar:** Que no nos da un nullpoint porque las instacias las esta
creando Mockito

```java
@ExtendWith(MockitoExtension.class)
public class SpecialitySDJpaServiceTest {

  @Mock
  private SpecialtyRepository specialtyRepository;

  @InjectMocks
  private SpecialitySDJpaService serviceATestear;

  @Test
  public void testDelete() {
    Speciality speciality = new Speciality();
    serviceATestear.delete(speciality);
  //Mockito.verify(specialtyRepository).delete(new Speciality()); FALLA YA QUE ES DIFERENTE INSTANCIA
  //Mockito.verify(specialtyRepository).deleteById(1L); //FALLA YA QUE NO SE LLAMA EN NINGUN MOMENTO
    Mockito.verify(specialtyRepository).delete(speciality);
  }

  @Test
  public void testDeleteById() {
    serviceATestear.deleteById(1L);
  //serviceATestear.deleteById(1L); //fallaria ya que se llama dos veces el metodo
    Mockito.verify(specialtyRepository).deleteById(1L);
  }

  @Test
  public void testDeleteByIdTwoTimes() {
    serviceATestear.deleteById(1L);
    serviceATestear.deleteById(1L);
  //Mockito.verify(specialtyRepository).deleteById(1L); //falla ya que esperamos 1 llamada
    Mockito.verify(specialtyRepository, times(2)).deleteById(1L);
  }

  @Test
  public void testDeleteByIdTwoTimes() {
    serviceATestear.deleteById(1L);
    Mockito.verify(specialtyRepository, never()).delete(new Speciality());
  }
  
}
```

**Resultado:** Podemos comprovar si se manda a llamar un metodo de un
Mock inyectado a una clase que queremos provar.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 110. Retornando valores de un Mock

**Objetivo:** Como vemos abajo en `SpecialitySDJpaService` tenemos un
`findById` que retorna un objeto Specialty, lo cual vamos a asegurar
que.

1. No retorne un vacio
2. Que el objeto retornado por `findById` del `service` sea el mismo
   que  retorno el `repositorio`.
3. Que el metodo `findById` del `repositorio` sea llamado de x manera

```java
import java.util.HashSet;
import java.util.Set;

public class SpecialitySDJpaService implements SpecialtyService {

    private final SpecialtyRepository specialtyRepository;

    @Override
    public Speciality findById(Long aLong) {
        return specialtyRepository.findById(aLong).orElse(null);
    }

    public Speciality find(SearchSpecialty searchSpecialty) {
        Speciality speciality = null;
        if(searchSpecialty.isJpaArquitecture()) {
            speciality = specialtyRepository.findById(searchSpecialty.getId()).orElse(null);
        } else {
            int id = specialtyRepository.addTag(searchSpecialty.getContains()+"-B");
            speciality = specialtyRepository.findById((long)id).orElse(null);
        }
        return speciality;
    }
}
```

A. Creamos test

1. `when` indicamos que cuando llame el metodo retorne x objeto

```java
@ExtendWith(MockitoExtension.class)
public class SpecialitySDJpaServiceTest {

  @Mock
  private SpecialtyRepository specialtyRepository;

  @InjectMocks
  private SpecialitySDJpaService serviceATestear;

  @Test
  public void testFindById() {
    //definimos simulacro
    Speciality expectedSpeciality = new Speciality();
    when(specialtyRepository.findById(1L)).thenReturn(Optional.of(expectedSpeciality));
    //hacemos la llamada del metodo
    Speciality actualSpeciality = serviceATestear.findById(1L);
    //comprobaciones
    assertTrue(actualSpeciality!=null);
    assertEquals(expectedSpeciality, actualSpeciality);
    verify(specialtyRepository).findById(1L);
  }

    @Test
  public void testFindById() {
    Speciality expectedSpeciality = new Speciality();
    when(specialtyRepository.findById(1L)).thenReturn(Optional.of(expectedSpeciality));

    Speciality actualSpeciality = serviceATestear.findById(1L);

    assertTrue(actualSpeciality!=null);
    assertEquals(expectedSpeciality, actualSpeciality);
    verify(specialtyRepository).findById(1L);
  }
  
  @Test
  public void testFindJpaArch() {
    //definir escenario / simulacro
    Speciality expectedSpeciality = new Speciality();
    SearchSpecialty searchSpecialty = new SearchSpecialty(1L, "Algo", false);
    when(specialtyRepository.addTag(searchSpecialty.getContains()+"-B")).thenReturn(2);
    when(specialtyRepository.findById(2L)).thenReturn(Optional.of(expectedSpeciality));

    //METODO A TESTEAR
    Speciality actualSpeciality = serviceATestear.find(searchSpecialty);

    //asegura
    assertTrue(actualSpeciality!=null); //que lo devuelto no sea nulo
    assertEquals(expectedSpeciality, actualSpeciality); //que lo retornado sea lo mismo
                                                        // que retorno por el repo

    //verificar
    verify(specialtyRepository).addTag("Algo-B");  // que se llamo asi el addTag
    verify(specialtyRepository).findById(2L);     //asegurar que se llamo asi el findbyId
  }
```

**Que no esta pasando:** No estamos testeando que se retorne una Speciality
en cocreto con un valor en concreto, ya que podriamos pensar que debemos
de testear la base de datos, que al id uno le corresponde un Speciality
en concreto que esta en la base de datos y eso no es lo que queremos hacer.

**Que si esta pasando:** Estamos hacegurando que cuado le pasemos un x
argumento al `service`, este llame a un metodo determinado del `repository`
con un argumento determinado. Por ejemplo en `testFindJpaArch` estamos
pasando un objeto y el metodo del service `findJpaArch()` debe de llamar
a los metodos `addTag` y `findById` de `repository` con argumentos que
no necesariamente son los mismos argumentos que le pasamos al `service`.

**Por que esta pasando:** Como indicamos estamos haciendo pruebas unitarias
lo cual nos interesa como esta funcionando el metodo, que en este caso
es que pase llame correctamente los metodos necesarios, no que estos
metodos retornen un valor determinado. Y que de querer que un determinado
id regrese una determinada Especialidad guardada en la base de datos,
eso ya es una prueba de integracion y son mas tardadas.

**Resultado:** Preparamos un simulacro donde indicamos que el mock
debe de llamar a un determinado metodo y debe de pasarle un determinado
argumento, por ejemplo, si `serviceATestear` cambia algo.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 111. Argument Matchers

En el codigo anterior definimos tipos exactos que queriamos retornar,
pero tenemos otras opciones como.

- `anyLong()` para indicar cualquier long `verify(specialtyRepository).findById(anyLong());`
- tambien podemos indicar cualquier objeto con `any(Especialidad.class)`

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Sección 10: Behavior Driven Mockito

Behavior Driven Mockito es una sintaxis equivalente a AssertJ pero para
Mockito no tendremos que agregar una libraria ya que lo incluye,
el cual podremos hacer las mismas cosas pero el codigo sera mas leible
y limpio. Tambien esta pensado para segur la filosofia de **BDD**.

PENDIENTE

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Sección 11:Advanced Mockito

Veremos un 20% de lo que se usa comunmente en Mockito pero son
caracteristicas que tarde o temprano nos las podremos encontrar.

### 124. Throwing Exceptions with Mockito

**Objetivo:** Vamos a lanzar dentro de un Mock alguna exepcion que
sabremos que nos podra lanzar, por ejemplo que fallo la conexion, la
BD esta vacia, peticion incorrecta, etc. y asi poder probar si esta
esta manejando bien la excepcion o no.

A. Del siguiente codigo

```java
public class SpecialitySDJpaService implements SpecialtyService {

    private final SpecialtyRepository specialtyRepository;

    @Override public void delete(Speciality object) {
        specialtyRepository.delete(object);
    }

    @Override public void deleteById(Long aLong) {
        specialtyRepository.deleteById(aLong);
    }
}
```

B. Creamos el siguiente Test, en el cual indicamos que el `delete()`
del `repositorio` cuando se llame va a lanzar una X excepcion.

```java
@Test
public void testDoThrow() {
  // cuando llame al eliminar del repositorio lanzara una exepcion
  doThrow(new RuntimeException("Pum")).when(specialtyRepository).delete(any());
  // cuando llamemos al service debemos de obtener el tipo de excepcion dicha
  assertThrows(RuntimeException.class, () -> serviceATestear.delete(new Speciality()));
  // nos aseguramos que el metodo delete de reposirrio ahiga sido llamado
  verify(specialtyRepository).delete(any());
}
```

### 128. Mockito Argment Capture

Hay dos formas de capturar argumentos ya sea inline o por anotaciones

**Objetivo:** Del siguiente codigo, vemos que a al argumento nombre
`owner.getApellidos()` se le estan agregando dos signos `%`. Asi que lo
que queremos es **capturar** este dato que entra como argumento y
asegurar que estamos agrendo esos simbolos.

A. Codigo a testear

```java
public class JefeController {

  private JefeService ownerService;

  public String processFindForm(Jefe owner, BindingResult result, Model model){
    if (owner.getApellidos() == null) { owner.setApellidos(""); }

    List<Jefe> results = ownerService.findAllByLastNameLike("%"+ owner.getApellidos() + "%");

    if (results.isEmpty()) {
        result.rejectValue("lastName", "notFound", "not found");
        return "owners/findOwners";
    } else if (results.size() == 1) {
        owner = results.get(0);
        return "redirect:/owners/" + owner.getId();
    } else {
        model.addAttribute("selections", results);
        return "owners/ownersList";
    }
  }
}
```

B. Creamos nuestro test, donde aseguramos que el parametro que le
pasamos sea el apellido rodeado del simbolo porcentaje.

```java
import mario.controller.utilsController.BindingResult;
import mario.controller.utilsController.Model;
import mario.entity.Jefe;
import mario.service.JefeService;
@ExtendWith(MockitoExtension.class)
public class JefeControllerTest {

  @Mock private JefeService ownerService;
  @Mock private BindingResult bindingResult;
  @InjectMocks private JefeController jefeCtrlATestear;

  @Test public void testArgumentCaptor() {
  //dado
    Jefe jefeToSearch = new Jefe(11, "mario", "marquez", "compras", false);
    List<Jefe> jefes = new ArrayList<>(); //lo que se retornara
    final ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
    when(ownerService.findAllByLastNameLike(captor.capture())).thenReturn(jefes);

  //cuando
    jefeCtrlATestear.processFindForm(jefeToSearch, bindingResult, new Model());

  //asegura
    //el esperado debe de ser el apellido rodeado de los simbolos %, por ejemplo,
    //si enta marquez, el parametro pasado debe de ser "%marquez%" (sin comillas)
    String expected = "%"+jefeToSearch.getApellidos()+"%";
    assertEquals(expected, captor.getValue());
  }

  // LO MISMO PERO CREAR EL CAPTOR POR ANOTACIONES

  @Captor
  ArgumentCaptor<String> stringCaptor;
  
  @Test
  public void testArgumentCaptorAnotation() {
    Jefe jefeToSearch = new Jefe(11, "mario", "marquez", "compras", false);
    List<Jefe> jefes = new ArrayList<>(); //lo que se retornara
    when(ownerService.findAllByLastNameLike(stringCaptor.capture())).thenReturn(jefes);

    jefeCtrlATestear.processFindForm(jefeToSearch, bindingResult, new Model());

    String expected = "%"+jefeToSearch.getApellidos()+"%";
    assertEquals(expected, stringCaptor.getValue());

  }
}
```

**Resultado:** Estamos asegurando que el pase de parametro a un metodo
dentro de la clase testeada se le pase tal como esperamos. E hicimos
esto creando el `ArgumentCaptor` por inline o por anotacion.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 129. Ejemplo como manejar multiples respuestas de Mock

**Teoria:** Para hacer mas facil la visualizacion de como un mock
puede devolver diferentes resultados segun su entrada, pero nosotros
controlamos cada respuesta que puede dar, lo podemos hacer de la
siguiente manera para `comprender` mejor los esta idea.

**Objetivo:** Si nos fijamos el siguiente metodo del codigo a testear
vemos que dependiendo de lo que retorne `findAllByLastNameLike` entrara
en 3 diferentes condicionales, las cuales retornaran alguna `vista`
diferentes, 1) cuando `result` este vacio, 2) cuando tenga uno y 1)
`else` cuando sean varios.

A. Codigo a testear

```java
public class JefeController {

  private JefeService ownerService;

  public String processFindForm(Jefe owner, BindingResult result, Model model){
    if (owner.getApellidos() == null) { owner.setApellidos(""); }

    // NOS INTERESA TESTEAR QUE SE RETORNE EL STRING CORRECTO SEGUN
    // EL CASO, PARA ELLO NOS INTERESA MODIFICAR LO QUE HAY EN "results"
    // PARA PODER CONSTRUIR NUESTROS TESTS
    List<Jefe> results = ownerService.findAllByLastNameLike("%"+ owner.getApellidos() + "%");
    if (results.isEmpty()) {
        // no owners found
        result.rejectValue("lastName", "notFound", "not found");
        return "owners/findOwners";//vista cuando no hubo resultados
    } else if (results.size() == 1) {
        // 1 owner found
        owner = results.get(0);
        return "redirect:/owners/" + owner.getId(); //vista para un resultado
    } else {
        // multiple owners found
        model.addAttribute("selections", results);
        return "owners/ownersList";//vista para multiples resultados
    }
  }
}
```

B. Vamos a crear 3 tests 1 para cada condicional

1. Agregamos un metodo el cual dependiendo de la entrada se configurara
   de diferente maneraq lo que va a retornar.
2. Agregamos el `assertEquals("redirect:/owners/11", result)` para
   ver que se rotorna lo que esperamos
3. Lo reutilizamos para cada caso

```java
// importaciones arriba
@ExtendWith(MockitoExtension.class)
public class JefeControllerTest {

  @Mock private JefeService ownerService;
  @Mock private BindingResult bindingResult;
  @Mock private Model model;
  @InjectMocks private JefeController jefeCtrlATestear;
  @Captor ArgumentCaptor<String> stringCaptor;


  @Test
  public void testSinResultados() {
    //given
    Jefe jefeToSearch = new Jefe(0, "", "noMeEncuentres", "", false);
    when(ownerService.findAllByLastNameLike(stringCaptor.capture()))
      .thenReturn( this.creaListaJefes(jefeToSearch) );

    String result = jefeCtrlATestear.processFindForm(jefeToSearch, bindingResult, null);

    assertEquals("%noMeEncuentres%", stringCaptor.getValue());
    assertEquals("owners/findOwners", result);
  }


  @Test
  public void testUnResultado() {
    //given
    Jefe jefeToSearch = new Jefe(0, "", "marquez", "", false);
    when(ownerService.findAllByLastNameLike(stringCaptor.capture()))
      .thenReturn( this.creaListaJefes(jefeToSearch) );

    String result = jefeCtrlATestear.processFindForm(jefeToSearch, bindingResult, null);

    assertEquals("%marquez%", stringCaptor.getValue());
    assertEquals("redirect:/owners/11", result);
  }

  @Test
  public void testVariosResultados() {
    //given
    Jefe jefeToSearch = new Jefe(0, "", "cbtis", "", false);
    when(ownerService.findAllByLastNameLike(stringCaptor.capture()))
      .thenReturn( this.creaListaJefes(jefeToSearch) );

    String result = jefeCtrlATestear.processFindForm(jefeToSearch, bindingResult, model);

    assertEquals("%cbtis%", stringCaptor.getValue());
    assertEquals("owners/ownersList", result);
  }

// ESTE METODO NOS AYUDA A CONFIGURAR QUE RETORNARA SEGUN SU ENTRADA Y
// ASI ENTENDER MEJOR LA IDEA.
  private List<Jefe> creaListaJefes(Jefe jefe) {
    List<Jefe> jefes = new ArrayList<>(); //lo que se retornara
    String app = jefe.getApellidos();

    if(app.equals("marquez")) {
      jefes.add(new Jefe(11, "mario", "marquez", "compras", false));
    } else if(app.equals("cbtis")) {
      jefes.add(new Jefe(11, "Mario", "cbtis", "compras", false));
      jefes.add(new Jefe(11, "Pedro", "cbtis", "algo", false));
    }

    return jefes;
  }

}
```

**Resultado:** Hicimos un test unitario de un `controlador` para que
asegurar que su interaccion con el `service` es correcta.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 130. Verify Order of Interactions

**Teoria:** Podemos comprobar el orden de interacciones, es decir, que
podemos indicar que primero se interactue con ObjetoA y luego con ObjetoB,
y no.

1. Que pase al revez ej. ObjetoB luego ObjetoA
2. Que no ocurra una ya sea se interactuo con ObjetoA pero no con ObjetoB
   o viseversa

**Objetivo:** Abajo vemos que se ejecuta `ownerService.findAllByLastNameLike`,
para buscar, y cuando son varios resultados entra en el else, y aqui se
llama al modelo en `addAttribute`, por lo que **queremos asegurar** que
esta interaccion ocurra y ocurra en dicho orden, osea primero el
`service` y luego el `model`.

```java
public String processFindForm(Jefe owner, BindingResult result, Model model){
  if (owner.getApellidos() == null) { owner.setApellidos(""); }

  List<Jefe> results = ownerService.findAllByLastNameLike("%"+ owner.getApellidos() + "%");

  if (results.isEmpty()) {
      result.rejectValue("lastName", "notFound", "not found");
      return "owners/findOwners";
  } else if (results.size() == 1) {
      owner = results.get(0);
      return "redirect:/owners/" + owner.getId();
  } else {
    //NOS INTEREZA ASEGURAR QUE SE LLAME "addAttribute" PARA ASEGURAR
    //QUE LA VISTA TENGA LA INFORMACION DEL RESULTADO DE BUSQUEDA DISPONIBLE
      model.addAttribute("selections", results);
      return "owners/ownersList";
  }
}
```

A. Creamos el test

1. Usamos `InOrder` para definir el orden de interaccion

```java
  @Test
  public void testInOrder() {
    // verificar que primero se llame a "ownerService.processFindForm"()
    // y luego se pase el resultado a la vista usando el "model.addAttribute()"
    Jefe jefeToSearch = new Jefe(0, "", "cbtis", "", false);
    when(ownerService.findAllByLastNameLike(stringCaptor.capture()))
      .thenReturn( this.creaListaJefes(jefeToSearch) );

    InOrder inOrder = inOrder(ownerService, model);

    jefeCtrlATestear.processFindForm(jefeToSearch, bindingResult, model);

    //order asserts
    inOrder.verify(ownerService).findAllByLastNameLike(anyString());
    inOrder.verify(model).addAttribute(anyString(), anyList());
  }
```

**Nota:** por alguna razon no pude usar `addAttribute("selections"...`
para asegurar que se llame con el atributo correcto, pero con el any
se muestra en el tutorial por lo que creo esta bien, y para comprobar
que se llama con el verify solo ejemplo

```java
  @Test
  public void testInOrder() {
    // verificar que primero se llame a "ownerService.processFindForm"()
    // y luego se pase el resultado a la vista usando el "model.addAttribute()"
    Jefe jefeToSearch = new Jefe(0, "", "cbtis", "", false);

    List<Jefe> jefes = this.creaListaJefes(jefeToSearch);
    when(ownerService.findAllByLastNameLike(stringCaptor.capture()))
      .thenReturn( jefes );

    InOrder inOrder = inOrder(ownerService, model);

    jefeCtrlATestear.processFindForm(jefeToSearch, bindingResult, model);

    verify(model).addAttribute("selections", jefes);

    //order asserts
    inOrder.verify(ownerService).findAllByLastNameLike(anyString());
    inOrder.verify(model).addAttribute(anyString(), anyList());
  }
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 132. Verify Zero or No More Interactions with Mock

**Teoria:** Podemos asegurar interacciones de forma mas especifcas,
como para asegurar que no ahiga ninguna interaccion, o que ahiga una
y de ahi no aiga mas interacciones.

- Ver codigo a testear de arriba

**Objetivo:** Vamos a asegurar que no ahiga interacciones con el
modelo cuando tengamos un solo resultado.

```java
 @Test
public void testUnResultado() {
  //given
  Jefe jefeToSearch = new Jefe(0, "", "marquez", "", false);
  when(ownerService.findAllByLastNameLike(stringCaptor.capture()))
    .thenReturn( this.creaListaJefes(jefeToSearch) );
  String result = jefeCtrlATestear.processFindForm(jefeToSearch, bindingResult, null);
  assertEquals("%marquez%", stringCaptor.getValue());
  assertEquals("redirect:/owners/11", result);
  
  verifyNoInteractions(model); // <<<<<<<<<<<<------------
}
```

**Objetivo2:** En el siguiente queremos asegurar que una vez habiendo
interactuado con este ya no lo haremos mas, por lo que si agregamos un
nuevo uso de model en `processFindForm` el test fallara.

```java
@Test
  public void testInOrder() {
    ...
    jefeCtrlATestear.processFindForm(jefeToSearch, bindingResult, model);

    //order asserts
    inOrder.verify(ownerService).findAllByLastNameLike(anyString());
    inOrder.verify(model).addAttribute(anyString(), anyList());
    verifyNoMoreInteractions(model);
  }
```

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Sección 12:Testing with Spring Framework

En esta seccion veremos como testear lo que es el Spring Core, lo cual
nos dara un mayor entendimiento de como funciona sin SpringBoot, para
luego saltar a otros modulos como el MVC.

Tenemos los siguientes Modulos que podemos/debemos sacar un Mock para
poder hacer nuestras pruebas rapido/sin dependencias

- **Enviroment:** Falsea la origen de las propiedades y ambiente
- **JNDI:** Falsear el JNDI es para no tener un servlet en ejecucion
- **Servlet API:** Para testear el ambiente web
- **Spring Web Reactive:** Para testear el ambiente web reactivo
- **Reflectiontestutil:** Para usar reflexion y modificar campos privados
- **AOP:** Ayuda a testear el AOP (Progracion Orientada a Aspectos)

Tenemos el **Spring MVC Test** que es un framework para testear las
interaccionnes del controlador, sin tener que correr un contenedor o
contexto de spring, permitiendo tests unitarios reales.

En el cual podemos encotrar los siguientes elementos

- **MockHttpServletRequest:** Falsea la implementacion peticion/respuesta
- **MockHttpSession:** Fasea las sesiones
- **ModleAndView:** utileria para aserciones

Es **importante:** falsear el springContext ya que este puede tomar unos
30seg (dependiendo del PC), y usando el Spring-MVC-Test nos ayudara con
esto, lo cual se cacheara el contexto falso entre tests para mejorar
el rendimiento.

Por el lado de **JDBC testing Support** Spring va a configurar el **JDBCTemplate**
para el soporte de tests, y tendremos el **JDBCTestsutils** que nos
ayudaran con utilizados como.

- countRowsInTable
- countRowsInTableWhere
- deleteFronTables
- deleteFronTablesWhere
- dropTable
- etc.

La base de datos **H2** esta favorecida para tests como rendimiento
y facilidad de uso.

**Anotaciones Spring Framework testing** Tenemos varias anotaciones como

- Nivel de Clase
  - **@BootstrapWith**: Configura como el contexto de test iniciara
  - **@ContextConfiguration**: Para configurar el contexto para test
  - **@WebAppConfiguration**:  Para configurar el contexto web para test
  - **@ContextHiarchy:**: Para asignar multiples "@ContextConfiguration"
  - **@ActiveProfiles**: para activar perfiles para testear
  - **@TestPropertySource**: Para asignar la fuente de las propiedades para test
  - **@DirtiesContext**: (Tambien Nivel metodo) Indica re-cargar el
    contexto para test (cuidado alenta los tests) reiniciando el cache del contexto
  - **@TestExecutionListeners**: Configura las ejecucion de listeners para tests
  - **@Commit**: (Tambien Nivel metodo) acciona un commit a la BD
  - **@Rollback**: (tambien Nivel metodo) acciona un rollback en la BD
  - **@BeforeTransaction:** Corre un metodo void antes de que una tranccion inicie
  - **@AfterTransaction:** Corre un metodo void despues de que una tranccion termine
  - Etc.

### 137. Spring Pet Clinic Code Review

Vamos a iniciar con el [proyecto de mascotas](https://github.com/springframeworkguru/tb2g-testing-spring),
, el proyecto no usa Spring Boot solo Spring, por lo que en el README
esta como ejecutar el proyecto y se levantara en el localhost:8080

Para hacer un test como los que hemos estado haciendo solo debemos de
usar JUnit y Mockito, no es necesario traer todo el contexto, lo cual
es algo **importante** ya que si no estariamos agregando mucho peso
a los test trallendo el contexto en el que podemos precindir de el.

Ejemplo.

A. Codigo a testear

```java
@Service
public class ClinicServiceImpl implements ClinicService {

    private PetRepository petRepository;

    @Autowired
    public ClinicServiceImpl(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<PetType> findPetTypes() throws DataAccessException {
        return petRepository.findPetTypes();
    }
}
```

B. Creamos nuestro test

```java
@ExtendWith(MockitoExtension.class)
public class ClinicServiceImplTest {

  @Mock
  private PetRepository petRepository;

  @InjectMocks
  private ClinicServiceImpl clinicServiceImplTest;//a testear

  @Test
  public void findPetTypes() {

    List<PetType> expected = new ArrayList<PetType>();
    expected.add(new PetType());
    when(petRepository.findPetTypes()).thenReturn(expected);

    List<PetType> actual = (List<PetType>)clinicServiceImplTest.findPetTypes();

    assertEquals(expected, actual);
    verify(petRepository).findPetTypes();
  }
}
```

**Resultado:** Estamos haciendo un test sobre un proyecto Spring, pero
no por eso tenemos que traer el contexto para crear tests simples.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Sección 13: Spring Framework Testing Context

Como ya mencionamos en la seccion de antes algo importante es el Spring
Context, que es en lo que vamos a profundizar en esta seccion

### Yanny or Laurel?_

Con "Yanny or Laurel?" hacemos referencia a un audio que se hizo viral,
ya que causo un debate de que si escuchaba Yanny o Laurel este, y en
realidad era solo custion de perpectiva.  

Lo mismo haremos para hacer los tests en lugar del famoso "Hello word"
para testear.

- Testear el autowiring de beans
- Testear configuracion de perfiles activos
- Testear Configuracion de Spring

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 143. Creando Spring Context basico para testear

**Objetivo:** Crearemos dos Beans los cuales uno dira "Yanny" y
otro "Laurel", con los cuales jugaremos para hacer los test en los
usando el SpringContext, y por medio de los tests saber cual de estos
dos inyecto Sprign a nuestro bean que testearemos.

A. Creamos el que generara una de estas palabras, a travez de una
interface y dos implementaciones.

```java
public interface IGeneradorpalabra {
  String getPalabra();
}

-----

public class GeneradorYanny implements IGeneradorpalabra {
  @Override
  public String getPalabra() {
    return "Yanny";
  }
}

-----

public class GeneradorLaurel implements IGeneradorpalabra {
  @Override
  public String getPalabra() {
    return "Laurel";
  }
}
```

B. Creamos el oyente, que consumira uno de estos generadores de palabras
que obtendra por inyeccion de control.

```java
public class Oyente {

  private static Logger log = Logger.getLogger(Oyente.class.getName());
  
  private IGeneradorpalabra generadorpalabra;

  public Oyente(IGeneradorpalabra generadorpalabra) {
    this.generadorpalabra = generadorpalabra;
  }

  public String loQueEscucheFue() {
    String palabra = generadorpalabra.getPalabra();
    log.info("loQueEscucheFue: " + palabra);
    return palabra;
  }
}
```

C. Vamos a agregar las anotaciones **para que sprign haga la IoC**,
en el mismo codigo que agregamos (refactorizacion).

```java
import org.springframework.stereotype.Component;

+ @Component
+ @Primary // se lo agregamos al Bean que queremos que se inyecte
           // ya que no indicar cual queremos Spring no sabra cual usar
  public class GeneradorYanny implements IGeneradorpalabra {...}

------------------------

+ @Component
  public class GeneradorLaurel implements IGeneradorpalabra { ... }

------------------------

@Service
public class Oyente {

  private static final Logger LOG = LoggerFactory.getLogger(BaseConfig.class);
  
  //@Autowired - CUIDADO CAUSARA ERROR YA QUE LO VAMOS A HACER POR CONFIGURACIONES
  private IGeneradorpalabra generadorpalabra;

  @PostConstruct
  private void onPostConstruct() {
    LOG.info("created Service - org.springframework.samples.petclinic.juegoPalabras.Oyente");
  }

  public Oyente(IGeneradorpalabra generadorpalabra) {
    this.generadorpalabra = generadorpalabra;
  }

  public String loQueEscucheFue() {
    String palabra = generadorpalabra.getPalabra();
    LOG.info("######## EJECT: loQueEscucheFue: " + palabra);
    return palabra;
  }
}
```

D. Crearemos los Beans de `Oyente` y este a su vez necesita otro Bean
de tipo `IGeneradorpalabra` en este caso de `Laurel` el cual necesitaremos
crear otra configuracion para que cree este y asi que spring pueda tener
ambos.

D.1. Creamos la Conf. para crear Oyente

```java
import org.slf4j.Logger; // al parecer el proyecto de petclinic solo saca logs de este
import org.slf4j.LoggerFactory;

@Configuration
public class BaseConfig {

  private static final Logger LOG = LoggerFactory.getLogger(BaseConfig.class);
  
  @PostConstruct
  private void onPostConstruct() {
    LOG.info("created Configuration - org.springframework.samples.petclinic.juegoPalabras.BaseConfig");
  }

  @Bean
  public Oyente oyente(IGeneradorpalabra generadorpalabra) {
    Oyente oyente = new Oyente(generadorpalabra);
    oyente.loQueEscucheFue();
    return oyente;
  }
}
```

D.2. Creamos la Conf. para crear un IGeneradorpalabra, en este caso el
de `Laurel`

```java
@Configuration
public class BaseConfigLaurel {
  @Bean
  GeneradorLaurel generadorLaurel() {
    return new GeneradorLaurel();
  }
}
```

> **ERROR:** Caused by: java.lang.IllegalArgumentException: Unsupported
> class file major version 58
> **DESCRIPCION:** Al ejecutar `mvn jetty:run-war` saca este error
> **CAUSA:** Habia cambiado de java de la `version 8 a 14` y me saco
> este error
> **SOLUCION:** en mi caso el proyecto era para java8 asi que me cambie
> a esa

E. **COMPROBAMOS** iniciando nuestro proyecto, y en los logs deveriamos
de porder los siguientes mensajes

```log
[INFO] created Configuration - org.springframework.samples.petclinic.juegoPalabras.BaseConfig
[INFO] created Configuration - org.springframework.samples.petclinic.juegoPalabras.GeneradorLaurel
[INFO] created Component - org.springframework.samples.petclinic.juegoPalabras.GeneradorYanny
[INFO] created Service - org.springframework.samples.petclinic.juegoPalabras.Oyente
[INFO] ######## EJECT: loQueEscucheFue: Laurel
```

**Resultado:** Estamos creando 3 beans, dos que generan una palabra y
uno que es oyente esa palabra, y una configuracion para usarlos y asi
saber cual de los dos beans el oyente escucho.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 144. JUnit 4 Laurel Test, Crear Test JUnit4 para spring basico

**Objetivo:** Crear un test que pruebe que escucho `laurel` de nuestro
oyente, pero lo haremos de forma simple con JUnit4 el cual no usaremos
ni falsearemos el SpringContext **nosotros haremos el IoC**.

A. Creamos nuestro test para `Oyente`

```java
public class OyenteTest {

  Oyente oyente;

  @Before
  public void setup() {
    //asi no dependemos de beans y por ende el contexto
    oyente = new Oyente(new GeneradorLaurel());
  }

  @Test
  public void loQueEscucheFue() {
    assertEquals("Laurel", oyente.loQueEscucheFue());
  }
}
```

**Resultado:** Al ejecutar el test pasara correctamente, pero si nos
fijamos **ES IMPORTANTE NOTAR** esto es una prueba unitaria, ya que
no estamos dependiento del `SpringContext`.

**HACER PRUEBA DE INTEGRACION:** al involucrar el SpringContext esto
hace que nuestra prueba ya sea de integracion, y para hacer eso
vamos a refactorizar nuestra prueba.

1. `import org.junit.Bef...` Nos aseguramos de usar Junit4
2. `@RunWith(SpringRunner.class)` Por defecto JUnit no sabe nada de
   Spring y asi indicamos que corra los tests con Spring en este caso con
   SpringCore (no usamos SpringBoot) para poder tener disponible el
   springContext
3. `@ContextConfiguration:` Le indicamos el SpringContext de la prueba
   que traiga los siguientes beans.
4. `@Autowired`: Aqui ya estamos usando Spring y para poder usarlo debemos
5. de haver usado el `RunWith` ya que JUnit no sabe nada de Spting

```java
import static org.junit.Assert.assertEquals;

import org.junit.Before, Test, RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

+ @RunWith(SpringRunner.class)
+ @ContextConfiguration(classes = {BaseConfig.class, GeneradorLaurel.class} )
public class OyenteTest {

  Oyente oyente;

//@Before //Quitamos la IoC Que hacemos manualmente
//public void setup() {
//  oyente = new Oyente(new GeneradorLaurel());
//}

  @Test
  public void loQueEscucheFue() {
    assertEquals("Laurel", oyente.loQueEscucheFue());
  }
}
```

**Resultado:** Estamos usando el springContext para tests

#### Inyectar un bean u otro de la misma interface

**Objetivo:** Como vimos en el test de arriba tenemos 1 bean que puede
ocupar uno de dos Beans disponibles, y queremos tener la posibilidad
de cambiar entre uno u otro.

Si analizamos el test de abajo con el codigo de arriba, podremos ver que
`Oyente` puede usar los beans `GeneradorLaurel` o `GeneradorYanny` y esto
puede causarnos confuciones o el error `NoUniqueBeanDefinitionException`
a continuacion veremos como funciona y/o solucionarlo.

A. Este Bean de configuracion crea el bean `Oyente`

```java
@Configuration
public class BaseConfig {

  @Bean
  public Oyente oyente(IGeneradorpalabra generadorpalabra) {
    Oyente oyente = new Oyente(generadorpalabra);
    oyente.loQueEscucheFue();
    return oyente;
  }
}
```

B. Creamos nuestro test para ya sea que retorne a "Yanny" o "Laurel",
en otras palabras utilize un bean u otro.

```java
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {BaseConfig.class, GeneradorLaurel.class, GeneradorYanny.class} )
public class OyenteTest {

  @Autowired
  Oyente oyente;

  @Test
  public void loQueEscucheFue() {
  //assertEquals("Laurel", oyente.loQueEscucheFue());
    assertEquals("Yanny", oyente.loQueEscucheFue());
  }
  
}
```

C. Ejecutamos el codigo y nos da el siguiente **ERROR**

```log
NoUniqueBeanDefinitionException: No qualifying bean of type [xxx] is
defined: expected single matching bean but found 2: GeneradorLaurel,GeneradorYanny
```

**causa-solucion:** Esto es por que cuando se crea el Bean Spring no
sabe de las dos implementaciones condidato usar, por lo que debemos
de indicarselo nosotros, y si hacemos lo siguiente en la clase del test
el problema seguira siendo el mismo.

```java
@Autowired
@Qualifier("generadorYanny")
Oyente oyente;
```

**Esto es por que** el que esta creando el bean realmente es `BaseConfig`
no la clase del test, por lo que debemos de poner el Qualifier en esa parte.

```java
@Configuration
public class BaseConfig {
  ...
  @Bean
//public Oyente oyente(IGeneradorpalabra generadorpalabra) { AQUI ESTA EL PROBLEMA
  public Oyente oyente(@Qualifier("generadorYanny") IGeneradorpalabra generadorpalabra) {
```

D. Si ejecutamos de nuevo el test este pasara correctamente.

**Otra solucion** es ponerle a una de las clases la anotacion `@Primary`
sobre `GeneradorLaurel` o `GeneradorYanny` para indicar a cual darle
prioridad y asi Spring tome un Bean sobre el otro u otros Beans. Por
Ejemplo, habajo vemos que tomara `"yanny"` por que es el que tiene el
orincipal.

```java
@Component("yanny")
@Primary
public class GeneradorYanny implements IGeneradorpalabra {...

--------------------------

@Component("laurel")
//@Primary
public class GeneradorLaurel implements IGeneradorpalabra {...
```

**Otra Solucion en el test** es incluir solamente la clase que
requerimos, ya que estamos incluyendo las dos es por eso que nos da
este problema, pero al dejar solo una Spring no tendra de otra que
tomar la unica que ahi. **Por ejemplo** abajo vemos que solo tenemos
un generador, por lo que se tomara esa.

```java
  @RunWith(SpringRunner.class)
//@ContextConfiguration(classes = {BaseConfig.class, GeneradorLaurel.class, GeneradorYanny.class} )
  @ContextConfiguration(classes = {BaseConfig.class, GeneradorLaurel.class} )
```

**Resultado:** Ya sabemos donde se crea el Bean y por ende sabemos
como cambiarlo.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 144. JUnit 4 Laurel Test, Crear Test JUnit5 para spring basico

**Objetivo:** Vamos a hacer lo mismo solo que ahora con Junit5

A. Para hacerlo es lo mismo solo cambiamos las anotaciones.

1. `SpringJUnitConfig` Trae la anoracion de RunWith y contextConfigu
2. `Autowired` Sigue siendo importante para que Spring haga lo suyo.

```java
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig(classes = {BaseConfig.class, GeneradorLaurel.class})
public class OyenteJUnit5Test {

  @Autowired Oyente oyente;  

  @Test public void loQueEscucheFue() {
    assertEquals("Laurel", oyente.loQueEscucheFue());
  //assertEquals("Yanny", oyente.loQueEscucheFue());
  }
}
```

**Resultado:** Hicimos lo mismo que el titulo anterior pero con JUnit5
el cual necesitamos de menos anotaciones.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 150. Using Inner Class Configuration

**Teoria:** Ahi una forma de cambiar nuestra configuracion del test
con una clase interna. **Es comun** usar esta forma en los test de
Spring.

**Objetivo:** vamos a hacer lo mismo, solo que ahora nuestra configuracion
de que Bean usara Oyente (**Despreciamos a BaseConfig**), definiendolo
con una clase interna.

A. Creamos el test

1. No necesitamos a `BaseConfig` por que la clase interna es su sustituto
2. `SpringJUnitConfig` Agregamos la clase interna ya que ahi esta nuestra
   config

```java
@SpringJUnitConfig(classes = {OyenteJUnit5ClaseInternaTest.TestConfig.class})
public class OyenteJUnit5ClaseInternaTest {

  @Configuration
  static class TestConfig {
    @Bean Oyente oyente() {
      return new Oyente(new GeneradorLaurel());
    }
  }

  @Autowired
  Oyente oyente;

  @Test
  public void loQueEscucheFue() {
    assertEquals("Laurel", oyente.loQueEscucheFue());
  //assertEquals("Yanny", oyente.loQueEscucheFue());
  }
  
}
```

**Resultado:** Configuramos nuestro Bean con una clase interna, haciendo
que no dependamos del codigo existente, y el SpringContext segue
funcionando.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 151. Usando ComponentScans() es JUnit5

**Objetivo:** Podemos usar esta anotacion para configurar nuestras
clases, **DUDA** lo cual es mi mismo que agregarlas en SpringJUnitConfig
pero bueno.

```java
@SpringJUnitConfig(classes = {OyenteJUnit5ClaseInternaTest.TestConfig.class})
public class OyenteJUnit5UsarComponentScansTest {

  @Configuration
  @ComponentScan("org.springframework.samples.petclinic.juegoPalabras")
  static class TestConfig { }

  @Autowired Oyente oyente;
  
  @Test public void loQueEscucheFue() {
    assertEquals("Laurel", oyente.loQueEscucheFue());
  //assertEquals("Yanny", oyente.loQueEscucheFue());
  }
}
```

> **Error:** Por alguna razon no me roconocia la anotacion de primario,
> y me sacaba el error `NoUniqueBeanDefinitionException`.
> **Causa:** [DUDA] creo por las clases de `BaseConfigLaurel` y `BaseConfigYanny`
> estaban sobre-escribiendo el primary o crando a Yanny y Laurel de mas.
> **Solucion:** Le quite las 2 clases de configuracion y ya funcionaron
> las anotaciones de primary.

```java
/*
@Configuration
public class BaseConfigYanny {
  @Bean
  GeneradorYanny generadorYanny() {
    return new GeneradorYanny();
  }
}
---------------------------------------------------
@Configuration
public class BaseConfigLaurel {
  @Bean
  GeneradorLaurel generadorLaurel() {
    return new GeneradorLaurel();
  }
}
*/
```

**Resultado:** Estamos testeando segun la configuracion de nuestra app,
lo cual al parecer es mas comun, ya que estamos testeando como esta
funcionando actualmente nuestra app, no posibles escenarios alternos.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 152. Usando perfiles un beans y en tests

**Teria:** Cuando asignamos un perfil a un bean, este ya no se cargara
incluso que este como primario, solo se cargara si le ponemos de forma
explicita que carge el perfil, entonces prodremos tener el **problema**
de que ya no se carge un Bean en nuestros tests por este motivo, pero
para **solucionarlo** podemos usar perfiles en nuestros tests.

**Objetivo:** A un test que tiene un bean que se le asigna por ser el
primario, a este bean agregarle un **perfil**, entonces fallara, por
lo que usaremos perfiles en nuestro test para que pase de nuevo.

A. Agregamos la anotacion `@Profile` a `GeneradorLaurel`

```java
@Profile("Laurel")
@Component
@Primary
public class GeneradorLaurel implements IGeneradorpalabra {
```

B. Ejecutar nuestro test `OyenteTest` y veremos que **no pasara**.

C. Agregamos `ActiveProfiles` a nuestro/s test que fallaron y veremos
como estos **pasan correctamente**.

```java
  @RunWith(SpringRunner.class)
+ @ActiveProfiles("Laurel")
  @ContextConfiguration(classes = {BaseConfig.class, GeneradorLaurel.class, GeneradorYanny.class} )
  public class OyenteTest {...
```

**Resultado:** Vimos como usar perfiles en nuestros tests, y aprendimos
que cuando los usamos, para que se asigne el bean debemos de activar
el perfil, de otra maneara aun que tenga el primary el bean no se asignara.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 153. Testear el propierties de Spring

**Objetivo:** Crear un Bean que tome una propiedad del .properties `@Value`
y asegurar/testear que se este asignando dicha variable/valor al bean.

A. Creamos `resorurces/juegoPalabras.properties`

```properties
# CUIDADO: cualquier diferencia letra/mayuscula mas o menos no la asignara
generador.palabra=desdePropiedades
```

B. Creamos un nuevo generador de palabras, pero este regresara la palabra
que capture del properties que creamos apenas.

1. NOTA: el "404" es para percatarnos que no se asigno la variable

```java
@Component
@Profile("externo")
public class GeneradorPropiedad implements IGeneradorpalabra {

  //@Value("${generador.palabra:404}")
  //[DUDA] TAMBIEN SE PUEDE AQUI PERO NO SE CUAL ES LA DIFERENCIA
  private String palabra;

  @org.springframework.beans.factory.annotation.Value("${generador.palabra:404}")
  public void setPalabra(String palabra) {
    this.palabra = palabra;
  }

  @Override
  public String getPalabra() {
    return palabra;
  }
  
}
```

C. Creamos nuestro test

1. `TestPropertySource` para que de ahi asigne la variable que necesitamos

```java
@TestPropertySource("classpath:juegoPalabras.properties")
@ActiveProfiles("externo")
@SpringJUnitConfig(classes = {GeneradorPropiedadTest.TestConfig.class})
public class GeneradorPropiedadTest {

  @Autowired
  Oyente oyente;

  @Configuration
  @ComponentScan("org.springframework.samples.petclinic.juegoPalabras")
  static class TestConfig { }

  @Test
  public void loQueEscucheFueProperties() {
  //assertEquals("Laurel", oyente.loQueEscucheFue());
    assertEquals("desdePropiedades", oyente.loQueEscucheFue());
  }
  
}
```

**Resultado:** Aseguramos que el valor de una variable sea asignado
desde un .properties usando `@Value` de Spring.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, separacion seccion separacion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, separacion seccion separacion

.

-->

----

## Sección 14:Spring MVC Test

En esta seccion veremos **Spring MVC Test** componente importante ya
antes de tener el MVC de spring, un controlador hera algo complicado
ya que pasan muchas cosas aqui, por ejemplo, asegnacion de
**parametros y headers** se toman deciciones etc, y ahora con la nueva
implementacion tanto de MVC como de MVC-Test podremos testear de una
manera mas sencilla/efectiva nuestros controladores.

MVC-test tiene a `HttpServletRequest` y a `HttpServletResponse` para
falsear ambas implementaciones y como **YA SABEMOS :o** MVC dirige
todo esto a `DispacherServlet` que decidira como se rutean las cosas
al controlador.

**Configuraciones para MVC-test:** Tenemos varios tipos de configuraciones
entre ellas.

- Standalone Setup
  - El mas usado ya que es el mas ligero
  - Permite probar solo un controlador a la vez
  - Permite testear las peticiones y respuestas
- WebAppContext Setup
  - Carga un Contexto de spring mas amplio, permitiendo hacer cosas
    mas complejas, como SpringSecurity, Filtros, etc
  - Puede probar varios controlador a la vez

**Importaciones Estaticas:** este puede ser el tema mas complicado al
usar MVC-test que entre ellos estan.

- `MockMvcRequestBuilders.*`: Construlle la peticion
- `MockMvcResultMacher.*`: Crea assets para las respuestas
- `MockMvctBuilders.*`: Configura y construlle una instgancia de MockMvc

**CONSIDERACIONES IMPORTANTES:**

- MVC-Test no utiliza un "running Servlet container"
  - Por lo que No hay peticiones http ni puetos 80 u 8080 etc

- HTML no es generado, las plantillas no son ejecutadas (JSP, Thymeleaf, etc.)

- Se puede testear que vista/template fue llamado o redirigido.
  - pero no puedes esperar que se genere el HTML

- MVC-Test puede utilizar un "running Servlet container" pero eso ya son
  pruebas de integracion mucho mas lentas y ahora no es el momento paeso.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 159-A. Testear controlador Spring solo con Mockito

Podemos testear un controlador solamente usando mockito haciendo nuestro
test mas rapido e unitario.

**Por ejemplo:** Abajo podemos ver como estamos testeando un controlador
de en varios aspectos como son pero no limitando los que se señalan
**en los comentarios**.

```java
@ExtendWith(MockitoExtension.class)
public class VetControllerTest {

  @Mock private ClinicService clinicService;
  @Mock private Map<String, Object> model;
  @InjectMocks private VetController vetController;

  @Captor ArgumentCaptor<Vets> ListVetCaptor;
  @Captor ArgumentCaptor<String> stringCaptor;

  Vets vetsModel = new Vets();
  List<Vet> vetsService = new ArrayList<>();

  Vet vetFinded = new Vet();

  @BeforeEach public void setup() {
    vetsService.add(vetFinded);
  }

  @Test public void showVetList() {
    when(clinicService.findVets()).thenReturn(vetsService);
    when(model.put(stringCaptor.capture(), ListVetCaptor.capture())).thenReturn(vetsModel);

    String actual = vetController.showVetList(model);

    //verificamos que el "findVets()" de "service" sea llamado
    verify(clinicService).findVets();

    //aseguramos que "put()" de "model" sea llamado
    verify(model).put(anyString(), any());

    // aseguramos que regrese la vista correcta en forma de String
    assertEquals("vets/vetList", actual);

    // aseguramos que ponemos el nombre de porpiedad del modelo adecuado
    assertEquals("vets", stringCaptor.getValue());

    //aseguramos que la lista de Vets asignada al model contenga el Vet regresado por el Service
    assertTrue(ListVetCaptor.getValue().getVetList().contains(vetFinded));
  }
}
```

**NO PODEMOS:** Testear con mockito cosas como

- Ejecutar el controlador desde su ruta (sin dominio)
- Saber su tipo de respuesta 2xx, 4xx, etc
- Si se integra correctamente Spring-MVC con nuestra implementacion

**Resultado:** Creamos un test para un controlador solamente con Mockito,
y sus limitaciones ante MVC-Test.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 159-B. Primer tests con MVC-Test

**Objetivo:** Testear un controlador iniciandolo desde su ruta que
se le definio, y de ahi asegurar que regresa un codigo de respuesta
200, que el modelo tenga un X atributo y retorne la vista esperada.

A. Primero debemos de asegurarnos que tengamos la sig. dependencia.

```xml
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-test</artifactId>
  <version>${spring-framework.version}</version> <!-- En mi caso 5.1.4.RELEASE-->
  <scope>test</scope>
</dependency>
```

B. Este es el controlador que vamos a testear, (codigo [aqui](./img/08-tb2g-testing-spring.zip))

```java
@Controller
public class VetController {

  @Autowired  
  private final ClinicService clinicService;

  @RequestMapping(value = { "/vets.html"})
  public String showVetList(Map<String, Object> model) {
      // Here we are returning an object of type 'Vets' rather than a collection of Vet objects
      // so it is simpler for Object-Xml mapping
      Vets vets = new Vets();
      vets.getVetList().addAll(this.clinicService.findVets());
      model.put("vets", vets);
      return "vets/vetList";
  }
}
```

C. Asi quedaria nuestro test

1. `MockMvc`: Con este iniciaremos nuestro test, permitiendo ejecutar
   y testear nuestro controlador
2. `MockMvcBuilders:` Escojemos configuracion en este caso standalone
   ya que es una prueba sencilla este es suficiente y recomendado para
   unit-tests.
3. `test.web.servlet.request.MockMvcRequestBuilders.*`: De este paquete
   es donde estan las aserciones que que usamos sobre los controladores

```java
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(MockitoExtension.class)
public class VetControllerMockMvcTest {

  @Mock private ClinicService clinicService;
  @Mock private Map<String, Object> model;
  @InjectMocks private VetController vetController;
  
  org.springframework.test.web.servlet.MockMvc mocMvc;

  @BeforeEach
  public void setup() {
    mocMvc = MockMvcBuilders.standaloneSetup(vetController).build();
  }

  @Test public void showVetList() throws Exception {
  mocMvc.perform(get("/vets.html"))
      .andExpect(status().isOk())
      .andExpect(model().attributeExists("vets"))
      .andExpect(view().name("vets/vetList"));
  }
}
```

**Resultado:** Testeamos usando el propio MVC de spring pero falseado,
haciendo nuestras pruebas mas asertadas y faciles de hacer.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### [PENDIENTE] 160. Tomar configuracion de XML

**Objetivo:** Crear un test el cual use la configuracion de los xml
para los controladores, tambien crearemos un xml para crear los Mocks
del service como del controlador.

... aburrido/viejo

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 163. Peticion GET con argumentos

**Objetivo:** A un controlador el cual le entra un parametro, vamos a
simular esta entrada, y asi poder testear su salida adecuada segun la
entrada.

A. El controlador que vamos a testear es el siguiente, en el caso de,
el apellido que entre no se encuentre

1. Asegurar que nuestro tests entre a `no owners found`

```java
@Controller
public class OwnerController {

  @Autowired
  private final ClinicService clinicService;

  @RequestMapping(value = "/owners", method = RequestMethod.GET)
  public String processFindForm(Owner owner, BindingResult result, Map<String, Object> model) {

    if (owner.getLastName() == null) { owner.setLastName(""); }

    Collection<Owner> results = this.clinicService.findOwnerByLastName(owner.getLastName());
    if (results.isEmpty()) {                              // no owners found
        result.rejectValue("lastName", "notFound", "not found");
        return "owners/findOwners";
    } else if (results.size() == 1) {                     // 1 owner found
        owner = results.iterator().next();
        return "redirect:/owners/" + owner.getId();
    } else {                                              // multiple owners found
        model.put("selections", results);
        return "owners/ownersList";
    }
  }
}
```

B. Creamos nuestro test

1. `.param(` aqui le estamos simulando que entro el parametro "lastname"
  el cual se guardara en `Owner.lastname`

```java
@ExtendWith(MockitoExtension.class)
public class OwnerControllerTest {

  @Mock private ClinicService clinicService;
  //@Mock private Map<String, Object> model;
  @Mock private BindingResult bindingResult;
  @InjectMocks private OwnerController ownerController;
  
  org.springframework.test.web.servlet.MockMvc mocMvc;

  @BeforeEach
  public void setup() {
    mocMvc = MockMvcBuilders.standaloneSetup(ownerController).build();
  }

  @Test public void processFindForm_SinResultados() throws Exception {
    /**
     * Cuando: Se busca un apellido que no se encuenta
     * Debe: Retornar un 200 y la vista owners/findOwners
     */
    mocMvc.perform(
        get("/owners").param("lastName", "DoNotFindMe")
      )
      .andExpect(status().isOk())
      .andExpect(view().name("owners/findOwners"));
  }
}
```

C. Al ejecutar el test este debe de pasar, ya que al no indicarle que
retornar cuando se llame al `clinicService.findOwnerByLastName` este
retornara una lista vacia, por lo que entra el test a esa parte del
controlador.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 164. Diferentes respuestas de controlador con MockMvc

**objetivo:** Segun nuestro controlador `OwnerController` (buscar arriba)
tenemos que comprobar que

A. Metodo a testear

```java
@Controller
public class OwnerController {

  @Autowired
  private final ClinicService clinicService;

  @RequestMapping(value = "/owners", method = RequestMethod.GET)
  public String processFindForm(Owner owner, BindingResult result, Map<String, Object> model) {

    // allow parameterless GET request for /owners to return all records
    if (owner.getLastName() == null) {
        owner.setLastName(""); // empty string signifies broadest possible search
    }

    // find owners by last name
    Collection<Owner> results = this.clinicService.findOwnerByLastName(owner.getLastName());
    if (results.isEmpty()) {
        // no owners found
        result.rejectValue("lastName", "notFound", "not found");
        return "owners/findOwners";
    } else if (results.size() == 1) {
        // 1 owner found
        owner = results.iterator().next();
        return "redirect:/owners/" + owner.getId();
    } else {
        // multiple owners found
        results.add(new Owner());
        model.put("selections", results);
        return "owners/ownersList";
    }
  }
}
```

B. Nos tenemos que asrgurar de lo siguiente

B.1. Al pasar un lastname nulo, cambiarlo a un vacio=""

1. Aqui lo que vemos es que a causa de que el latsname se modifico de
   nulo a vacio, entonces el metodo `findOwnerByLastName()` debe de
   llamarse un un vacio, por lo que nos evitamos comprobar si el
   `setLastName` fue llamado que seria mas dificil.

```java
@ExtendWith(MockitoExtension.class)
public class OwnerControllerTest {

  @Mock private ClinicService clinicService;

  @InjectMocks private OwnerController ownerController;
  
  org.springframework.test.web.servlet.MockMvc mocMvc;

  @BeforeEach
  public void setup() {
    mocMvc = MockMvcBuilders.standaloneSetup(ownerController).build();
  }

  @Test public void processFindForm_lastnameNulo() throws Exception {
    mocMvc.perform(get("/owners"))
      .andExpect(status().isOk())
      .andExpect(view().name("owners/findOwners"))
    ;

    //verify(owner).setLastName("");
    verify(clinicService).findOwnerByLastName("");
  }
```

B.2. Cuando el sevice retorne uno con id 1, que retorne la vista "redirect:/owners/1"

```java
@Test public void processFindForm_unResultado() throws Exception {
  Owner ownerExpected = new Owner();
  ownerExpected.setId(1);
  Collection<Owner> ownersExpected = new ArrayList<>();
  ownersExpected.add(ownerExpected);

  String lastNameToSeach = "un_resultado";
  
  when(clinicService.findOwnerByLastName(lastNameToSeach)).thenReturn(ownersExpected);
  
  mocMvc.perform(get("/owners").param("lastName", lastNameToSeach))
    .andExpect(status().is3xxRedirection())
    .andExpect(view().name("redirect:/owners/1"));
}

```

B.3. Cuando varios, retorne la vista "owners/ownersList" y el modelo
tenga el valor de "selections".

```java
@Test public void processFindForm_multiplesResultados() throws Exception {
  //DADO
  Owner ownerExpected = new Owner(); ownerExpected.setId(3);
  Collection<Owner> ownersExpected = new ArrayList<>();
  ownersExpected.add(new Owner());
  ownersExpected.add(new Owner());
  ownersExpected.add(ownerExpected);
  String lastNameToSeach = "varios_resultados";
  
  //CUANDO
  when(clinicService.findOwnerByLastName(lastNameToSeach)).thenReturn(ownersExpected);
  //when(model.put(anyString(), collectionCaptor.capture())).thenReturn(any());
  
  //ASEGURA
  mocMvc.perform(get("/owners").param("lastName", lastNameToSeach))
    .andExpect(status().isOk())
    .andExpect(model().attributeExists("selections"))
    .andExpect(view().name("owners/ownersList"))
  ;

  //assertTrue(collectionCaptor.getValue().equals(ownerExpected)); [PENDIENTE]
}
```

[PENDIENTE] [DUDA]: Como hacer para capturar el argumento de un metodo
perteneciente a un argumento del que vamos a testear, porejemplo arriba
no puedo capturar que se le pasa al `model.put("selections", ESTE)` el
argumento `ESTE` quiero capturarlo para ver si es correcto.

**Resultado:** Testeamos nuestro controlador en diferentes desiciones
que este toma, con el uso de MockMvc.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 166. Petcion POST Valida

**Escenario:** testear un cotrolador/metodo que necesita una peticion
post, **por ejemplo**, en el codigo a testear `processCreationForm` vemos
que tenemos un `result.hasErrors()` el cual retornara verdadero, si
alguno de los campos NO cumple con lo establecido en el `@Entity`.

**Objetivo:** Que cuando se asigne correctamente a `owner`, el controlador
debe de retornar la vista `redirect:/owners/N` siendo N el valor que le
pasamos.

A. Codigo a testear

```java
@Controller
public class OwnerController {

  private static final String VIEWS_OWNER_CREATE_OR_UPDATE_FORM = "owners/createOrUpdateOwnerForm";
  private final ClinicService clinicService;
  
  @RequestMapping(value = "/owners/new", method = RequestMethod.POST)
  public String processCreationForm(@Valid Owner owner, BindingResult result) {
    if (result.hasErrors()) {
      return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
    } else {
      this.clinicService.saveOwner(owner);
      return "redirect:/owners/" + owner.getId();
    }
  }
}
```

B. Creamos nuestro test

1. `(post(`: Simulamos la peticion post
2. `.param(`: Le agregamos los parametros necesarios
3. `is3xxRedirection`: Como lo retornado es un `redirect:` entonces
    esperamos que el resultado sea una redireccion.

```java
public class OwnerControllerTest {
  @Mock private ClinicService clinicService;
  @InjectMocks private OwnerController ownerController;
  org.springframework.test.web.servlet.MockMvc mocMvc;
  @BeforeEach
  public void setup() {
    mocMvc = MockMvcBuilders.standaloneSetup(ownerController).build();
  }

  @Test public void processCreationForm_PostValido() throws Exception {
    mocMvc.perform(post("/owners/new")
          .param("firstName", "Mario")
          .param("lastName", "Marquez")
          .param("address", "Sanches Sanchiages #44")
          .param("city", "Mexico")
          .param("telephone", "2223411212")
      ).andExpect(status().is3xxRedirection())
    ;
  }
}
```

**Resultado:** Aprendimos a llamar un controlador con metodo http Post
y a pasarle los parametros.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 167. Peticion POST NO valida, manejo correcto del error

**Escenario:** Tenemos que testear una peticion post, en el caso de que
el `@Valid` no pase queremos que regrese la vista esperada.

**Objetivo:** Pasarle de forma incorrecta un modelo al controlador, de
manera que el `@Valid` falle, entonces asegurar que retorna la vista
que esperamos en ese caso.

A. Codigo a testear

```java
@MappedSuperclass
public class Person extends BaseEntity {

    @Column(name = "first_name")
    @NotEmpty
    protected String firstName;

    @Column(name = "last_name")
    @NotEmpty
    protected String lastName;

####################

@Entity
@Table(name = "owners")
public class Owner extends Person {
    @Column(name = "address")
    @NotEmpty
    private String address;

    @Column(name = "city")
    @NotEmpty
    private String city;

####################

@Controller
public class OwnerController {

  private static final String VIEWS_OWNER_CREATE_OR_UPDATE_FORM = "owners/createOrUpdateOwnerForm";
  private final ClinicService clinicService;
  
  @RequestMapping(value = "/owners/new", method = RequestMethod.POST)
  public String processCreationForm(@Valid Owner owner, BindingResult result) {
    if (result.hasErrors()) {
      return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
    } else {
      this.clinicService.saveOwner(owner);
      return "redirect:/owners/" + owner.getId();
    }
  }
}
```

B Creamos nuestro Test

1. `isOk` esperamos esto por que solo mandamos la vista del fomulario
   con los errores para que el usuario vea donde se equivoco.
2. `attributeHasErrors`: Esperamos que indique que atributo fallo.
3. `attributeHasFieldErrors`: Esperamos que espesifique por que capo
   fallo la @Valid
4. `view().name`: Podemos ver que el controlador retorna esa vista

```java
@Test public void processCreationForm_PostINValido() throws Exception {
  mocMvc.perform(post("/owners/new"))
      .andExpect(status().isOk())
      .andExpect(model().attributeHasErrors("owner"))
      .andExpect(model().attributeHasFieldErrors("owner", "address"))
      .andExpect(model().attributeHasFieldErrors("owner", "telephone"))
      .andExpect(model().attributeHasFieldErrors("owner", "city"))
      .andExpect(model().attributeHasFieldErrors("owner", "lastName"))
      .andExpect(model().attributeHasFieldErrors("owner", "firstName"))
      .andExpect(view().name("owners/createOrUpdateOwnerForm"))
  ;
}
```

**Resultado:** Aprendimos a testear que maneje correctamente los errores
por medio del `BindingResult` mandarle al usuario que fue lo que fallo.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### Argumentos de Path algo/{id}, correcto e incorrecto

**Objetivo:** Probar un controlador con un parametro en ruta

 ```java
   @Test public void processUpdateOwnerForm() throws Exception {
    //DADO
    Owner ownerExp = new Owner();
    ownerExp.setFirstName("Mario");
    ownerExp.setLastName("Marquez");
    ownerExp.setAddress("Sanches Sanchiages #44");
    ownerExp.setCity("Mexico");
    ownerExp.setTelephone("2223411212");

    //[DUDA] [PENDIENTE] COMO CAPTURAR ARGUMENTO DE UN METODO QUE RETORNA VOID
    //final ArgumentCaptor<Owner> captor = ArgumentCaptor.forClass(Owner.class);
    //when(clinicService.saveOwner(captor.capture()) )

    //CUANDO
    mocMvc.perform(post("/owners/{ownerId}/edit", 11)
        .param("firstName", ownerExp.getFirstName())
        .param("lastName", ownerExp.getLastName())
        .param("address", ownerExp.getAddress())
        .param("city", ownerExp.getCity())
        .param("telephone", ownerExp.getTelephone()))

    //ESPERA
      .andExpect(status().is3xxRedirection())
      //[DUDA] [PENDIENTE] COMO PROBAR EL "/owners/11" PARA ASEGURAR QUE REDIRIGE CON EL ID CORRECTO
      .andExpect(view().name("redirect:/owners/{ownerId}"))
    ;
  }
 ```

**Resultado:** Probamos una peticion donde su path tiene un argumento

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, separacion seccion separacion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, separacion seccion separacion

.

-->

----

## Sección 15: Introduction to Testing with Spring Boot

Hasta este punto hemos visto jUnit, mockito y tests con spring, para
darnos una idea que parte le pertenese a cual. En esta seccion veremos
SpringBoot lo cual cosas interesantes, pero para eso mesclamos las
herramientas que ya estudiamos.

Recordemos que SpringBoot trae dependencias para caracteristicas comunes
como lo son

- Spring JPA Started que incluye
  - Spring JPA Data, Hibernate, HikariCP, JDBC, etc
- Provee auto.configuracion por ejemplo
  - H2 para BD en memoria

El mismo caso ocurre para test donde tenemos **spring-boot-starter-test**
que provee las dependencias comunes para testing como.

- JUnit 2.1 en adelante
- SpringTest
- AssertJ
- Mockito
- JSONAssert
- JsonPath

Estructura recomendada para un proyecto Spring, principalmente porque
SpringBoot esta configurado para escanear los componentes que esten a
la altura y abajo de `Application.java` y los componenestes que no esten
a esas alturas no seran escaneado almenos que lo indiquemos explicitamente

![01-estructura-recomenda-para-proyecto-springboot](./img/06-estructura-recomenda-para-proyecto-springboot.PNG)

**Web Enviroment** Tenemos varias opciones con
`@SpringBootTest(WebEnviroment = {OPTION})`

- `MOCK`: Por default, Carga un ambiente web simulado
- `RANDOM_PORT`: Provee de un servidor envevido en un puerto aleatorio
- `DEFINED_PORT`: Provee de un servidor envevido en el pueto definido
  en application.properties o el default 8080
- `NONE`: No provee de un ambiente web.

**SpringBoot Test Slices:**

- `@SpringBootTest` por defecto va a traer todo el contexto de la aplicacion
  lo cual puede ser muy pesado, y para solucionarlo tenemos esto.
  - Por ejemplo: tenemos `@JsonTest` que crea un ambiente solo para
    JSON con Jackson

- Tenemos una lista de ejemplos [aqui](https://docs.spring.io/spring-boot/docs/2.1.6.RELEASE/reference/html/test-auto-configuration.html)
  que nos ayudaran a simular ambientes para diferentes caracteristicas
  como.

- Para abrir base de datos como MySQL, Postgress, Oracle, Etc.

![02-slice-para-base-de-datos](./img/07-slice-para-base-de-datos.PNG)

Y asi tenemos opciones como los siguiente ambientes.

- `@DataJpaTest` para BD con JPA
- `@DataMongoTest` para MongoDB
- `@RestClientTest` para probar consumo de APIs
- `@WebFluxTest` Para programacion reactiva
- `@WebMvcTest`: Para provar nuestros MVC

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 173. Crear proyecto Spring Boot con VSCode

<!-- Crear un proyecto spring boot con vs code, vs code crear un proyecto Spring boot-->

**Objetivo:** Vamos a crear un nuevo proyecto Spring Boot desde cero
con vscode.

A. Instalar extension precionando `Ctrl+Shift+X` y escribiendo
`vscode-spring-initializr`, debe de aparecernos la extencion hecha
por Microsoft

B. Ya instalada abrimos el la consola de comando `Ctrl+Shift+P` y
escribimos `Spring Initializr` y veremos si crear un gradle o maven
y de ahi seguir el `wizzard`.

C. Seleccionamos las siguientes dependencias

1. DevTools
2. Lombok
3. Web
4. JPA
5. Actuator
6. h2 <!-- en mi caso se me olvido agregarla-->

D. En mi caso me creo un proyecto con el siguiente pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.3.2.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
  </parent>
  <groupId>mario.testspringboot</groupId>
  <artifactId>testspringboot</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <name>demo</name>
  <description>Demo project for Spring Boot</description>

  <properties>
    <java.version>11</java.version>
  </properties>

  <dependencies>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-devtools</artifactId>
      <scope>runtime</scope>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <groupId>org.junit.vintage</groupId>
          <artifactId>junit-vintage-engine</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </build>

</project>
```

E. Tambien debe de crear un test como el siguinte

```java
@SpringBootTest
class DemoApplicationTests {

  @Test
  void contextLoads() {
  
  }
}
```

F. **Comprobar** Ejecutar los tests `mvn test`, y veremos que carga todo
el Spring Context y un Tomcat embebido (como emcionamos anteriormente)
lo cual lo hace lento.

> **Error:** DataSourceProperties$DataSourceBeanCreationException: Failed to determine a suitable driver class
> **Descripcion:** Creamos el proyecto y sin modificarle nada corrimos
> los test y saco este error, fallando los test
> **Causa:** Incluimos las dependencia de `jpa` la cual intenta conectar
> a una base de datos, pero al no haber ninguna, manda este error
> indicando que no sabe a que conectarse
> **Solucion:** Debemos de agregar algun dirver de base de datos y su
> configuracion por ejemplo mysql o etc, en mi caso agregaremos H2

```xml
<dependency>
  <groupId>com.h2database</groupId>
  <artifactId>h2</artifactId>
  <scope>runtime</scope>
</dependency>
```

**Resultado:** Tenemos un nuevo proyecto Spring boot con las dependencias
mas comunes de un proyecto web y para hacer nuestros tests.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 174. Updating Spring Boot 2.x to JUnit 5

De spring 2.2 para abajo por defecto trae JUnit4 por lo que es una buena
practiva usar JUnit5.

**Objetivo:** De un proyecto en el que tengamos SpringBoot menor a 2.2
ver que JUnit tiene y si tiene el cuatro pasarlo a JUnit5

A. Vamos a las dependencias en el pom.xml y agregamos una exclusion
para quitar a JUnit4, y agregamos la dependecnia de JUnit5, como
se ve abajo

<!-- Que significan las dependencias de test, diferencia entre 
dependencias para los tests Que significan las dependencias de los test-->

1. `junit-jupiter-api`: Para poder escribir las pruebas
2. `junit-jupiter-engine`: Para poder ejecutar las pruebas
3. `junit-vintage-engine`: Para retrocompativilidad con JUnit4
4. `org.mockito`: Para tener vercion compatible de mockito con JUnit5

```xml
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
    <exclusions>
      <exclusion> <!-- Este ya estaba por defecto-->
        <groupId>org.junit.vintage</groupId>
        <artifactId>junit-vintage-engine</artifactId>
      </exclusion>
+     <exclusion>
+       <groupId>junit</groupId>
+       <artifactId>junit</artifactId>
+     </exclusion>
    </exclusions>
  </dependency>
  <dependency>
+   <groupId>org.junit.jupiter</groupId>
+   <artifactId>junit-jupiter-api</artifactId>
  </dependency>
  <dependency>
+   <groupId>org.junit.jupiter</groupId>
+   <artifactId>junit-jupiter-engine</artifactId>
  </dependency>
  <dependency>
+   <groupId>org.mockito</groupId>
+   <artifactId>mockito-junit-jupiter</artifactId>
  </dependency>
```

B. Si estramos usando algunas de las clases de JUnit4, refrescar las
dependencias causaran error de no encontradas lo cual debemos sustituirlas
por sus equivalentes a JUnit5.

**Resultado:** Actuializamos JUnit4 a JUnit5 en un proyecto SpringBoot
antiguo

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

######################################### divicion titulo separacion titulo

-->

### 175. Integrando proyecto SpringBoot Maven con CircleCI

CircleCI es una herramienta web que nos permite hacer integracion
continua (CI), de forma sencilla, y otras cosas como agregar un icono
en github para indicar la salud de nuestros tests.

A. Crearse una cuenta en `circleci.com` y en `Github`

B. Subir nuestro proyecto creado a github

C. Entrar a `add project` de CircleCI y veremos los proyectos que tenemos
en Guthub listados dentro de CircleCI y le damos en `Set Up Project`

D. Nos pedira confirmar una configuracion, pero el problema que ahi tiene
es que quiere usar Java8 y nuestro proyecto es Java11, por lo que ponemos
la siguiente configuracion

```yml
# Java Maven CircleCI 2.0 configuration file
#
# Check https://circleci.com/docs/2.0/language-java/ for more details
#
version: 2
jobs:
    build:
        docker:
            # specify the version you desire here
            - image: circleci/openjdk:11-browsers-legacy

            # Specify service dependencies here if necessary
            # CircleCI maintains a library of pre-built images
            # documented at https://circleci.com/docs/2.0/circleci-images/
            # - image: circleci/postgres:9.4

        working_directory: ~/repo

        environment:
            # Customize the JVM maximum heap limit
            MAVEN_OPTS: -Xmx3200m

        steps:
            - checkout

            # Download and cache dependencies
            - restore_cache:
                  keys:
                      - v1-dependencies-{{ checksum "pom.xml" }}
                      # fallback to using the latest cache if no exact match is found
                      - v1-dependencies-

            - run: mvn dependency:go-offline

            - save_cache:
                  paths:
                      - ~/.m2
                  key: v1-dependencies-{{ checksum "pom.xml" }}

            # run tests!
            - run: mvn test
```

E. Al darle aceptar correra los test en la plataforma y nos dira el
resultado,

![09-integracion-con-circleci-con-proyecto-springboot](./img/09-integracion-con-circleci-con-proyecto-springboot.PNG)

<!-- Agregar icono de salud de test, ver salud de test en github,
ver estatus de tests en github, ver el estatus de los test en github -->

F. Agregamos `Status Badges` que es el icono que indica la salud a nuestro
readme.md que podemos ver desde github.com

1. En CircleCI en este [link](https://circleci.com/gh/MarioM51/testspringboot/edit)
   tenemos codigo para generar el icono
2. Se lo agregamos a nuestro readme.md

```r
[![CircleCI](https://circleci.com/gh/MarioM51/testspringboot/tree/circleci-project-setup.svg?style=svg)](https://circleci.com/gh/MarioM51/testspringboot/tree/circleci-project-setup)
```

![10-status-badges-de-circleci-en-github](./img/10-status-badges-de-circleci-en-github.PNG)

**Resultado:** Corrimos nuestras prubas JUnit5 en CircleCI el cual
automatiza que cada vez que hagamos un `push` podremos ver desde el
readme.md si pasaron o no las pruebas desde un icono.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

###################################### divicion titulo separacion titulo

-->

### 176. Proyecto de la seccion, uso de Lombok y mapstruct

**Lombok** es una extension para nuestro IDE y proyecto que nos ayuda
a generar codigo repetitivo, como los son getters/setters, constructores,
Builders, etc.

**mapstruct**: Ayuda a generar los parsers por ejemplo de Entity a DTO.

A. El proyecto usa Lombok asi que lo instalamos en nuestro IDE

1. [Fuente](https://stackoverflow.com/questions/22310414/how-to-configure-lombok-in-eclipse-luna)
2. Descargamos lombok de [aqui](https://mvnrepository.com/artifact/org.projectlombok/lombok)
3. Instalamos con el comando `java -jar lombok-1.16.18.jar` lo que nos
   abrira un wizard donde especificaremos el path raiz de nuestro IDE
   y le damos sobre `Install/Update`
4. A nuestro `STS.ini` o `eclipse.ini` cual sea el caso, le agregamos.

```r
-vmargs
...
-Xbootclasspath/a:lombok.jar
-javaagent:lombok.jar
```

- [DUDA] [PENDIENTE] Algo raro del "java compiler" y "annotation processing"
  - [Fuente](https://stackoverflow.com/questions/45518161/how-to-get-eclipse-to-generate-mapstruct-mappers-using-gradle)

B. Descargamos el codigo de este [link](https://github.com/springframeworkguru/tsbb2b-sfg-brewery/tree/testing-w-wiremock)

C. Ejecutamos el proyecto y **comprobamos** entrando `localhost:8080//api/v1/beer`

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

###################################### divicion titulo separacion titulo

-->

## Sección 16:Spring MVC Rest Controller Tests

### Jayway JsonPath

Cuando queremos testear un `RestController` este retorna un objeto JSON
y para navegar/recorrer en esta dependecia `json-path` nos ayuda a esta
navegacion lo cual podemos [estudiar esto](https://github.com/json-path/JsonPath)

Esta dependencia ya biene incluida dentro de `spring-boot-starter-test`
por lo que no es necesario que la inclullamos.

Ejemplo: Navegar en un json tienda que tiene una coleccion de libros
y queremos ver su tiulo, [herramienta visual](https://jsonpath.herokuapp.com/).

```json
{
    "store": {
        "book": [
            {
                "category": "reference",
                "author": "Nigel Rees",
                "title": "Sayings of the Century",
                "price": 8.95
            },
            {
                "category": "fiction",
                "author": "Evelyn Waugh",
                "title": "Sword of Honour",
                "price": 12.99
            },
            {
                "category": "fiction",
                "author": "Herman Melville",
                "title": "Moby Dick",
                "isbn": "0-553-21311-3",
                "price": 8.99
            },
            {
                "category": "fiction",
                "author": "J. R. R. Tolkien",
                "title": "The Lord of the Rings",
                "isbn": "0-395-19395-8",
                "price": 22.99
            }
        ],
        "bicycle": {
            "color": "red",
            "price": 19.95
        }
    },
    "expensive": 10
}

// CONSULTA/NAVEGACION
$.store.book[0].title

//RESULTADO
"Sayings of the Century"

// CONSULTA
$.store.book[*].author

//RESULTADO
[
   "Nigel Rees",
   "Evelyn Waugh",
   "Herman Melville",
   "J. R. R. Tolkien"
]
```

**Resultado:** Le hechamos un vistaso a `JsonPath` que nos ayudara a
seleccionar valores de los json y eso nos ayudara en los tests.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

###################################### divicion titulo separacion titulo

-->

### 181. Primer test a RestController con JsonPath (sin SpringBoot)

<!-- comprobar fecha -->

**Objetivo:** Vamos a realizar una prueba de una API-Rest tipo GET,
que nos retornara un json, y comproaremos que el json que nos retorna
es el qu esperando, comparando los valores del json con los que
esperamos.

A. Codigo a testear

```java
@RequestMapping("/api/v1/beer")
@RestController
public class BeerController {

    private final BeerService beerService;

    @GetMapping(path = {"/{beerId}"},produces = { "application/json" })
    public ResponseEntity<BeerDto>  getBeerById(@PathVariable("beerId") UUID beerId){

        return new ResponseEntity<>(beerService.findBeerById(beerId), HttpStatus.OK);
    }
}
```

B. Creamos nuestro test

  1. `@BeforeEach - BeerDto.builder`: Creamos el producto que queremos
     que retorne el Service y por ende el que esperamos en este caso.
  2. `@BeforeEach - MockMvcBuilders`: Crear Mock de controlador con su
     webContext igual falseado.
  3. `content().contentType(Med...`: Esperamos que tengamos el formato
     correcto.
  4. `andExpect(jsonPath(` estamos consultando el json que retorno el
    controlador y le damos cual es el que esperamos para que pase el
    test.
  5. `is(dateTimeFormatter`: Comprobar el formato de fecha

```java
import static org.mockito.ArgumentMatchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import guru.springframework.brewery.services.BeerService;
import guru.springframework.brewery.web.model.BeerDto;
import guru.springframework.brewery.web.model.BeerStyleEnum;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith(MockitoExtension.class)
class BeerControllerTest {

  BeerDto beerExpected;
  MockMvc mockMvc;

  @Mock private BeerService beerService;
  @InjectMocks private BeerController beerController;

  @BeforeEach
  void setUp() throws Exception {
    log.debug("########### PREARANDO TESTSS ########### ");
    beerExpected = BeerDto.builder()
        .version(1)
        .beerName("MyBeer1")
        .beerStyle(BeerStyleEnum.GOSE)
        .price(new BigDecimal(3))
        .quantityOnHand(5)
        .upc(123456789L)
        .id(new UUID(1L, 2L))
        .createdDate(OffsetDateTime.now().minusDays(2))
        .lastModifiedDate(OffsetDateTime.now())
        .build();
    mockMvc = MockMvcBuilders.standaloneSetup(beerController).build();
  }

  @Test
  void testGetBeerById() throws Exception {
    log.debug("########### EJECUTANDO TEST> testGetBeerById ########### ");

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    when(beerService.findBeerById( any() )).thenReturn(beerExpected);

    mockMvc.perform(get( "/api/v1/beer/" + beerExpected.getId()) )
          .andExpect(status().isOk())
          .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
          .andExpect(jsonPath("$.id", is(beerExpected.getId().toString())))
          .andExpect(jsonPath("$.beerName", is("MyBeer1")))
          .andExpect(jsonPath("$.createdDate", is(dateTimeFormatter.format(beerExpected.getCreatedDate()))))
    ;
  }
}
```

C. Nuestro test deberia de pasar correctamente

**Resultado:** Acabamos de testear un controlador y vimos que el json
retornado sea el que esperamos, y si solo usamos mockito y mockmvc el
cual hace la prueba ligera.

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

###################################### divicion titulo separacion titulo

-->

### 182. Usando JsonPath con listas (sin SpringBoot)

**Objetivo:** Lo mismo que el de antes pero ahora esperamos una lista
de objetos, el cual vamos a inspeccionar esta lista en json para ver
que es lo que esperamos.

A. Codigo a testear

```java
@RequestMapping("/api/v1/beer")
@RestController
public class BeerController {

  @GetMapping(produces = { "application/json" })
  public ResponseEntity<BeerPagedList> listBeers(@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                                  @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                                  @RequestParam(value = "beerName", required = false) String beerName,
                                                  @RequestParam(value = "beerStyle", required = false) BeerStyleEnum beerStyle){

    if (pageNumber == null || pageNumber < 0){
        pageNumber = DEFAULT_PAGE_NUMBER;
    }

    if (pageSize == null || pageSize < 1) {
        pageSize = DEFAULT_PAGE_SIZE;
    }

    BeerPagedList beerList = beerService.listBeers(beerName, beerStyle, PageRequest.of(pageNumber, pageSize));

    return new ResponseEntity<>(beerList, HttpStatus.OK);
  }
}
```

B. Escribimos nuestro test, el cual como vemos no cambia mucho, solo
por la manera de comprobar lo regrezado

1. `$.content", hasSize(2))`: Decimos que el arreglo debe tener un
   tamaño de dos
2. `ent[0].quantityOnHand", is(2)`: Comprobar que el valor del primer
   elemento del arreglo

```java
@Test
void testListBeers() throws Exception {
  log.debug("########### EJECUTANDO TEST: testListBeers ########### ");
  
  final ArgumentCaptor<String> nameStrigCaptor = ArgumentCaptor.forClass(String.class);
  final ArgumentCaptor<BeerStyleEnum> BeerStyleEnumCaptor = ArgumentCaptor.forClass(BeerStyleEnum.class);
  final ArgumentCaptor<PageRequest> pageRequestCaptor = ArgumentCaptor.forClass(PageRequest.class);
  
  when(beerService.listBeers(nameStrigCaptor.capture(), BeerStyleEnumCaptor.capture(),
      pageRequestCaptor.capture() )).thenReturn(beerPagedList) ;
  
  mockMvc.perform(get("/api/v1/beer").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is(200))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].quantityOnHand", is(2)))
        ;
}
```

**Resultado:** Inspeccionamos un arreglo del json, e igual solo usando
Mockito

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

###################################### divicion titulo separacion titulo

-->

### 183. Convertir prueba Mockito a SpringBoot Slices

Como vimos arriba solo estamos usando Mockito sin usar SpringBoot, lo
cual esta bien pero no nos traemos toda parte del contexto que nos
ayudaria con nuestra prueba, para hacerlo es sencillo.

**Objetivo:** La prueba que ya habiamos creado vamos a convertirla a
usar un **Slice de SpringBoot**, lo cual puede que la hago un poco mas
lenta pero tendremos una mayor capacidad ya que tenemos parte del
contexto de Spring boot disponible.

A. Nustra prueba queda igual que antes solo cambian pocas cosas como.

1. `@WebMvcTest`: Eliminamos Mockito y la sustituimos con esta para que
   se cree nuestro controlador como bean y asi poderlo inyectar despues
2. `@Autowired`: Inyecamos el controlador que se creo como bean.
3. `@MockBean`: Indicamos que cree el service como un bean pero falso.

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;


@Slf4j
//@ExtendWith(MockitoExtension.class) // quitamos mockito
@WebMvcTest(BeerController.class)     // con este estamos usando SpringBoot
  class BeerControllerTest {

  BeerDto beerExpected, beerExpected2;

+ @Autowired // Lo agregamos para que se inyecte el controlador
  MockMvc mockMvc;

  //@Mock
  @MockBean // sigue siendo un mock pero de tipo bean
  private BeerService beerService;

  // @InjectMocks // ya no lo necesitamos ya que "WebMvcTest" lo esta creando
  // private BeerController beerController;

  BeerPagedList beerPagedList;

  @BeforeEach
  void setUp() throws Exception { ...
    //mockMvc = MockMvcBuilders.standaloneSetup(beerController).build();
  }

  //nuetros test quedan exactamente igual
  @Test void testListBeers() throws Exception { ... }
  @Test void testGetBeerById() throws Exception { ... }
}
```

B. Nuestro test segura funcionando.

**Resultado:** Convertimos nuestra prueba de solo usar Mockito, a estar
usando el Spring-Context de Spring-Boot, de manera sencilla, esto nos
**permite** tener disponible el potencial de spring-boot, como son sus
auto-configuraciones

<!--

######################################### divicion titulo separacion titulo

-->

----

<!--

###################################### divicion titulo separacion titulo

-->

### 185. Crear test de Integracion, Spring-Context completo con RestTemplate

**RestTemplate** Recordar que este es el objeto que contiene las
implementaciones para conectar el codigo (comenmente ejecutar controladores),
a las peticiones HTTP, y lo que hace spring por nosotros es conectar
nuestros controladores al restemplate de forma auomatica, el cual en
realizad **podemos** ejecutar nuestros controladores desde este objeto
**IMPORTANTE:** este tiene un mode de funcionar `sincrono` (bloqueante)
por lo que va a quedar deprecado por `WebClient` que es asincrono.

El test anterior usamos solo una parte del Contexto, lo cual nos permite
hacer nuestras con el contexto necesario para nuestra prueba, pero si
no podremos usar otros elementos como Repositorios, Entidades Services
etc, ya que estos no existen, para poder usarlos tendremos que traernos
todo el spring context.

**Escenario:** Si vemos el proyecto tenemos a `DefaultBreweryLoader`
donde estamos creando a nuestra base de datos unos datos para probar,
el cual estamos usando los repositorios para hacer esto.

Si nos fijamos estamos agregando

1. 1 cerveceria
2. 3 cervezas
3. 3 inventarios
4. 1 Cliente
5. 1 Orden

```java
 @Component
public class DefaultBreweryLoader implements CommandLineRunner {

  private final BreweryRepository breweryRepository;
  private final BeerRepository beerRepository;
  private final BeerInventoryRepository beerInventoryRepository;
  private final BeerOrderRepository beerOrderRepository;
  private final CustomerRepository customerRepository;

  public DefaultBreweryLoader(BreweryRepository breweryRepository,
                            BeerRepository beerRepository, BeerInventoryRepository beerInventoryRepository,
                            BeerOrderRepository beerOrderRepository, CustomerRepository customerRepository) {
    this.breweryRepository = breweryRepository;
    this.beerRepository = beerRepository;
    this.beerInventoryRepository = beerInventoryRepository;
    this.beerOrderRepository = beerOrderRepository;
    this.customerRepository = customerRepository;
  }

  @Override public void run(String... args) throws Exception {
    loadBreweryData();
  }

  private void loadBreweryData() {
    if (breweryRepository.count() == 0){
      breweryRepository.save(Brewery
              .builder()
              .breweryName("Cage Brewing")
              .build());

      Beer mangoBobs = Beer.builder()
              .beerName("Mango Bobs")
              .beerStyle(BeerStyleEnum.IPA)
              .minOnHand(12)
              .quantityToBrew(200)
              .upc(337010000001L)
              .price(new BigDecimal(20))
              .build();

      beerRepository.save(mangoBobs);

      beerInventoryRepository.save(BeerInventory.builder()
              .beer(mangoBobs)
              .quantityOnHand(100)
              .build());

      Beer galaxyCat = Beer.builder()
              .beerName("Galaxy Cat")
              .beerStyle(BeerStyleEnum.PALE_ALE)
              .minOnHand(12)
              .quantityToBrew(200)
              .upc(337010000002L)
              .price(new BigDecimal(12.5))
              .build();

      beerRepository.save(galaxyCat);

      beerInventoryRepository.save(BeerInventory.builder()
              .beer(galaxyCat)
              .quantityOnHand(100)
              .build());

      Beer pinball = Beer.builder()
              .beerName("Pinball Porter")
              .beerStyle(BeerStyleEnum.PORTER)
              .minOnHand(12)
              .quantityToBrew(200)
              .upc(337010000003L)
              .price(new BigDecimal(7))
              .build();

      beerRepository.save(pinball);

      beerInventoryRepository.save(BeerInventory.builder()
              .beer(pinball)
              .quantityOnHand(100)
              .build());

      Customer testCustomer = customerRepository.save(Customer
              .builder()
              .customerName("Test 1").apiKey(UUID.randomUUID())
              .build());

      Set<BeerOrderLine> orderLines1 = new HashSet<>();
      orderLines1.add(BeerOrderLine.builder().beer(galaxyCat).orderQuantity(15).quantityAllocated(0).build());
      orderLines1.add(BeerOrderLine.builder().beer(pinball).orderQuantity(7).quantityAllocated(0).build());

      BeerOrder testOrder1 = beerOrderRepository.save(BeerOrder.builder()
              .orderStatus(OrderStatusEnum.NEW)
              .customer(testCustomer)
              .customerRef("testOrder1")
              .orderStatusCallbackUrl("http://example.com/post")
              .beerOrderLines(orderLines1)
              .build());

      orderLines1.forEach(line -> line.setBeerOrder(testOrder1));

      beerOrderRepository.save(testOrder1);
    }
  }
}
 ```

**Objetivo:** Usar la configuracion/datos que agregamos arriba dentro
de nuestras pruabas, en este caso, probar que tenemos 3 cervezas
reutilizando el codigo de arriba.

A. Codigo a testear

```java
@RequestMapping("/api/v1/beer")
@RestController
public class BeerController {

  @GetMapping(produces = { "application/json" })
  public ResponseEntity<BeerPagedList> listBeers(@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                                  @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                                  @RequestParam(value = "beerName", required = false) String beerName,
                                                  @RequestParam(value = "beerStyle", required = false) BeerStyleEnum beerStyle){

    if (pageNumber == null || pageNumber < 0){
        pageNumber = DEFAULT_PAGE_NUMBER;
    }

    if (pageSize == null || pageSize < 1) {
        pageSize = DEFAULT_PAGE_SIZE;
    }

    BeerPagedList beerList = beerService.listBeers(beerName, beerStyle, PageRequest.of(pageNumber, pageSize));

    return new ResponseEntity<>(beerList, HttpStatus.OK);
  }
}
```

B. Creamos nuestro test que utilize el contexto que tenemos en nuestra
aplicacion

1. `@SpringBootTest`: Estamos indicando que es una prueba de SpringBoot
   completa, el cual incluso creara un **servidor envevido** lo cual
   en este caso indicamos que le asigne un puerto aleatorio para crearlo.
2. `Autowired` En alguna parte del contexto ya tenemos creado el bean
   de `restTemplate` que contiene nuestro controlador

```java
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;

@Slf4j
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
class BeerControllerIntegracionTest {

  @Autowired private TestRestTemplate restTemplate;

  @Test void testListBeers() throws Exception {
    log.debug("########### EJECUTANDO TEST: testListBeers ########### ");
    BeerPagedList beerPagedList = restTemplate.getForObject("/api/v1/beer", BeerPagedList.class);
    assertThat(beerPagedList.getContent()).hasSize(3);
  }
```

**Resultado:** Tenemos una prueba exitosa, esto por que estamos usando
el contexto de nuestra aplicacion, en el cual se encuantra la creacion
de datos, lo cual nos permite esperar 3 cervezas, **cuidado** este test
es vastante pesado, por lo cual se concidera una prueba de integracion.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Sección 17:Testing with Wiremock

Hemos visto como testear el consumo de uno de nuestros servicios web API,
pero igual es muy utilizado el usar APIs de terceros, y para testear
estos usaremos esta herramienta.

**Wiremock** nos ayuda a simular una API u a replicar una existente,
para poder hacer sobre esta api simulada nuestras pruebas, es como un
Mockito el cual podemos indicar que respuesta queremos.

**Integrado:** Podemos meterlo dentro de nuestras pruebas, esta API
simulada donde vamos a tener exactamente lo que esperacmos de la API y
asi poder realizar pruebas de nuestro codigo que dependa de esta, su
origramacion es similar a la de mock.

Ejemplo

1. `stubFor` Estamos especificando como es que se va a llamar el servicio
   y que es lo que respondera.
2. `doSomething()`: Es el metodo que dentro de el llama al servicio que
   definimos como queremos que se llame y como contestara
3. `verify`: [DUDA] [PENDIENTE] NO ENTIENDO POR QUE ESPERA ESAS COSSAS.

![11-ejemplo-codigo-wiremock](./img/11-ejemplo-codigo-wiremock.PNG)

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 191. Primer uso de Wiremock en modo Standalone

- Para temas mas avanzadoz/extensos checar su [Documentacion.](http://wiremock.org/docs/)

Podemos usar Wiremock como un programa tipo Postman con la opcion de
crear APIs ficticias o copiadas a travez de una especie de proxy.  
Podemos [descargar](http://wiremock.org/docs/running-standalone/) y
ejecutarlo con el siguiente comando.

A. Ejecutamos el siguiente comando y nos dara la siguiante salida

1. Al ejecutarlo nos generara unas carpetas
   - `__files/`: Aqui pondremos archivos estaticos igual que una web
   - `mappings/`: Aqui podremos las configuraciones en .json para indicar
     como respondera a las peticiones
2. Podemos ver que esta activo en el puerto 8080

```r
java -jar wiremock-standalone-2.27.0.jar
#SALIDA
 /$$      /$$ /$$                     /$$      /$$                     /$$
| $$  /$ | $$|__/                    | $$$    /$$$                    | $$
| $$ /$$$| $$ /$$  /$$$$$$   /$$$$$$ | $$$$  /$$$$  /$$$$$$   /$$$$$$$| $$   /$$
| $$/$$ $$ $$| $$ /$$__  $$ /$$__  $$| $$ $$/$$ $$ /$$__  $$ /$$_____/| $$  /$$/
| $$$$_  $$$$| $$| $$  \__/| $$$$$$$$| $$  $$$| $$| $$  \ $$| $$      | $$$$$$/
| $$$/ \  $$$| $$| $$      | $$_____/| $$\  $ | $$| $$  | $$| $$      | $$_  $$
| $$/   \  $$| $$| $$      |  $$$$$$$| $$ \/  | $$|  $$$$$$/|  $$$$$$$| $$ \  $$
|__/     \__/|__/|__/       \_______/|__/     |__/ \______/  \_______/|__/  \__/

port:                         8080
enable-browser-proxying:      false
disable-banner:               false
no-request-journal:           false
verbose:                      false
```

B. Entramos a `localhost:8080/actuator/health` el cual nos dara un
mensaje como `no se pudo dar una respuesta por que no hay stub mappings`
pero nos permite saber que esta bien corrio correctamente.

C. Agregar una ruta con su respuesta

1. Este archivo por ejemplo `getPayorsById.json` lo guardamos en la
   carpeta `mappings/`
2. Reiniciamos wiremock

```json
{
  "id" : "b3685b8b-6085-4463-bdee-e5f5dbdaf48a",
  "request" : {
    "url" : "/v1/payors/0a818933-087d-47f2-ad83-2f986ed087eb",
    "method" : "GET"
  },
  "response" : {
    "status" : 200,
    "body" : "{\"payorId\":\"0a818933-087d-47f2-ad83-2f986ed087eb\"}",
    "transformers" : [ "response-template" ]
  },
  "uuid" : "b3685b8b-6085-4463-bdee-e5f5dbdaf48a"
}
```

D. Ya podemos hacer la peticion desde Postman o cualquier cliente http

```r
Request
  Method: GET
  URL: http://localhost:8080/v1/payors/0a818933-087d-47f2-ad83-2f986ed087eb

Answer
  Status: 200
  Body: { "payorId": "0a818933-087d-47f2-ad83-2f986ed087eb" }
```

**Resultado:** Creamos una API donde definimos su ruta y su respuesta,
el cual podemos consultar desde cualquier cliente http.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 192. Primer grabacion con Wiremock

Como vimos arriba podemos crear peticiones desde un archivo .json pero
habra veces que solo quedremos copiar una *API que ya existe* y hacerlo
de forma manual seria un proceso bastante tardado, por lo que podemos
**grabarlas por un proxy** de forma automatica, lo cual lo hace mucho
mas rapido.

A. iniciar wiremock: `java -jar wiremock-standalone.jar`

B. Entrar a `http://localhost:8080/__admin/recorder/`

![12-wiremock-recorder](./img/12-wiremock-recorder.PNG)

C. Poner la url de la API que queremos copiar en mi caso de el proyecto
de "Bares" el cual lo iniciamos en la ruta `localhost:8181`.

D. Al poner la url y ponerle en **Record** nos mostrara un mensaje
que esta grabando.

E. Mandamos nuestras peticiones a `localhost:8080` el cual es nuestro
wireMock y este ira a `localhost:8181` para hacer la misma peticion que
nosotros hicimos y en el proceso copiara la peticion.

F. Le damos `Stop` y nos dira cuantas peticiones capturo y ya podremos
ver los archivos en `mappings/`

G. Ahora detenemos nuestro proyecto que capturamos, en mi caso "Bares"
y damos las mismas peticiones a wiremock (localhost:8080) y lo que
pasara es que nos respondera de la misma manera, osea que ya tenemos
nuestra api copiada.

**Resultado:** Copiamos una API y ahora la podremos usar para nuestras
pruebas.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 193. Primer Test con wiremock y JUnit

Veremos como configurar una prueba JUnit y usar wiremock para usar un
cliente real, pero que en realidad este cliente esta falseado.

**Objetivo:** Teastear una parte donde usemos una API externa, queremos
que de lugar de que este vaya a consultar a la verdadera, vaya a
consultar a una que definamos con wiremock.

**Escenario** En el codigo de abajo tenemos un listener que escucha
cuando una orden es actualizada, y para hacerlo este envia una peticion
a una url que nosotros definimos para actualizarla.

```java
@Slf4j
@Component
public class BeerOrderStatusChangeEventListener {

    RestTemplate restTemplate;
    DateMapper dateMapper = new DateMapper();

    public BeerOrderStatusChangeEventListener(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Async
    @EventListener
    public void listen(BeerOrderStatusChangeEvent event){
        System.out.println("I got an order status change event");
        System.out.println(event);

        OrderStatusUpdate update = OrderStatusUpdate.builder()
                .id(event.getBeerOrder().getId())
                .orderId(event.getBeerOrder().getId())
                .version(event.getBeerOrder().getVersion() != null ? event.getBeerOrder().getVersion().intValue() : null)
                .createdDate(dateMapper.asOffsetDateTime(event.getBeerOrder().getCreatedDate()))
                .lastModifiedDate(dateMapper.asOffsetDateTime(event.getBeerOrder().getLastModifiedDate()))
                .orderStatus(event.getBeerOrder().getOrderStatus().toString())
                .customerRef(event.getBeerOrder().getCustomerRef())
                .build();

        try{
            log.debug("Posting to callback url");
            restTemplate.postForObject(event.getBeerOrder().getOrderStatusCallbackUrl(), update, String.class);
        } catch (Throwable t){
            log.error("Error Preforming callback for order: " + event.getBeerOrder().getId(), t);
        }
    }
}
```

pero NO queremos consumir la API/url real, ya que esta podria cambiar
su comportamiento, haciendo que nuestro test este contamido, entonces
lo que **haremos** es consumir a wiremock para que siempre nos de el
resultado esperado y asi concentrarnos en ver que funcione nuestro
codigo y no preocuparnos si funciona la API.

A. Agregamos dependencia y repositorio de jilpack donde esta wiremock

```xml
<dependency>
    <groupId>com.github.JensPiegsa</groupId>
    <artifactId>wiremock-extension</artifactId>
    <version>0.4.0</version>
</dependency>
</dependencies>
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

B. Creamos nuestra prueba sin nada, osea solo estamos ejecutando el
codigo que queremos comprobar, y ejecutamos.

```java
class BeerOrderStatusChangeEventListenerTest {

  private BeerOrderStatusChangeEventListener orderListener;

  @BeforeEach
  void setUp() throws Exception {
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    orderListener = new BeerOrderStatusChangeEventListener(restTemplateBuilder);
  }

  @Test
  void listen() {
    //CUANDO
    BeerOrder beerOrder = BeerOrder.builder()
        .orderStatus(OrderStatusEnum.READY)
        .orderStatusCallbackUrl("http://localhost/actualizar")
        .createdDate(Timestamp.valueOf(LocalDateTime.now()))
        .build();

    BeerOrderStatusChangeEvent event = new BeerOrderStatusChangeEvent(beerOrder, OrderStatusEnum.NEW);

    //ENTONCES
    orderListener.listen(event);
  }
}
```

**IMPORTANTE** Al ejecutar la prueba esta pasara pero si nos fijamos
en los logs y veremos que tenemos el siguiente error `onnection refused: connect`
lo que significa que la url que le pasamos realmente nunca le llego
la peticion ya qu esta ni existe.

Entonces lo que haremos es por medio de waremock hacer que cuando se
haga una peticion esta la haga a una API de waremock lo cual asi siempre
vamos a esperar una respuesta que nosotros queremos.

**NOTA** esta API tendra un puerto aleatorio por lo que igual vamos a
configurar eso para que el test le apunte el puerto correcto.

C. Agregar API/Servicio-Rest a nuestro test

```java
  import com.github.tomakehurst.wiremock.WireMockServer;
  import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
  import static com.github.jenspiegsa.wiremockextension.ManagedWireMockServer.with;

+ // NO OLVIDAR PONER EL "ExtendWith" PARA QUE FUNCIONE
+ @ExtendWith(com.github.jenspiegsa.wiremockextension.WireMockExtension.class)
  class BeerOrderStatusChangeEventListenerTest {

+   // CREAMOS EL SERVIDOR DE PRUEBA CON WIREMOCK PARACIDO AL STANDALONE QUE YA HICIMOS
+   @com.github.jenspiegsa.wiremockextension.Managed
+   WireMockServer wireMockServer = with(wireMockConfig().dynamicPort());

    private BeerOrderStatusChangeEventListener orderListener;

    @BeforeEach
    void setUp() throws Exception {
      RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
      orderListener = new BeerOrderStatusChangeEventListener(restTemplateBuilder);
    }

    @Test
    void listen() {
      //CUANDO
      BeerOrder beerOrder = BeerOrder.builder()
          .orderStatus(OrderStatusEnum.READY)
+          //LE PONOSMO QUE TOME EL PUERTO DEL SERVIDOR QUE CREAMOS
#         .orderStatusCallbackUrl("http://localhost:" + wireMockServer.port() + "/actualizar")
          .createdDate(Timestamp.valueOf(LocalDateTime.now()))
          .build();

      BeerOrderStatusChangeEvent event = new BeerOrderStatusChangeEvent(beerOrder, OrderStatusEnum.NEW);
      //ENTONCES
      orderListener.listen(event);
    }
  }
```

D. Al ejecutar la prueba nos dara los siguientes trazas/errores, lo que
significa que no hemos definido esa ruta y que es lo que contestara.

```log
nodename no servname provided //CREO ESTO ES CUANDO LA URL ESTA MAL ESCRITA
...
404 null HTTP/1.1            // CUANDO YA HICIMOS UNA PETICION EMPIEZA A RETORNAR 404
Content-Type: text/plain
...
A request was unmatched by any stub mapping //CREO LA PRIMERA VEZ SACA ESTO
```

E. Agregar ruta a WireMock

1. Solo agregamos la parte de `stubFor` donde definimos el comportamiento
   de la respuesta segun la consulta que se va a hacer
2. En este caso solo indicamos que la respuesta sea un `ok()`
3. `verify` que solo se haga una peticion, CUIDADO no el mismo verify
   que el de mockito

```java
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;

  @Test
  void listen() {
    //CUANDO
    BeerOrder beerOrder = BeerOrder.builder()
        .orderStatus(OrderStatusEnum.READY)
        .orderStatusCallbackUrl("http://localhost:" + wireMockServer.port() + "/actualizar")
        .createdDate(Timestamp.valueOf(LocalDateTime.now()))
        .build();
    BeerOrderStatusChangeEvent event = new BeerOrderStatusChangeEvent(beerOrder, OrderStatusEnum.NEW);

+   wireMockServer.stubFor(post("/actualizar").willReturn(ok()));

    //ENTONCES
    orderListener.listen(event);

    // VERIFIVCA
    verify(1, postRequestedFor(urlEqualTo("/update")));
  }
```

F. ejecutamos el test y tenemos la siguiente respuesta de la peticion
que nos dice las trazas.

```log
200 null HTTP/1.1
Matched-Stub-Id: 9cccdff5-6566-4ca4-8d42-8f7eaa5658f8
Vary: Accept-Encoding, User-Agent
Content-Encoding: gzip
```

**Resultado:** Creamos un servidor envevido dentro de nuestras pruebas
para simular peticiones web, en este caso solo al cambiar el puerto
ya le estamos pegando a este y no al real, lo hicimos de una forma
facil.

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Sección 18: Spring MVC REST Docs

- Documentacion: [Link](https://docs.spring.io/spring-restdocs/docs/current/reference/html5/)

Un problema comun de la documentacion es que cuando actualizamos el
codigo tambien debemos de actualizar la documentacion, algo que se va
dejando para el ultimo, lo cual genera que la documentacion sea inprecisa
en diferentes gravedades segun el equipo/persona.

**Spring MVC REST Docs** es una gran herramienta ya que genera la Doc
a partir de nuestras pruebas unitarias, lo cual hace un ahorro de trabajo
y que esta este al dia y sea precisa (segun hagamos nuestras pruebas).

![Spring-MVC-REST-Docs-diagrama](./img/13-Spring-MVC-REST-Docs-diagrama.PNG)

La forma de funcionar es por creacion de fragmentos que luego son
unidos con **asciidoctor**, estos fragmentos los saca de nuestras
pruebas de controladores.

- **Default Snippets**
  - `curl-request`: para consulta de la API desde linea de comandos
  - `http-request`: detalles de la peticion
  - `http-response`: detalles de la respuesta
  - `httpie-request`: Otra herramienta de comandos
  - `request-body`: detalles del cuerpo de la peticion
  - `response-body`: detalles del cuerpo de la respuesta

- **Opciones**
  - Generar documentacion con markdown
  - Generar la documentacion por una tarea de maven o gradle
  - Publicar la documentacion
  - Muchas opciones para personalizar la documentacion

- **Extenciones**
  - `restdocs-wiremock`: auto-generar stubs para wiremock
  - `restdocssext-jersey`: para generarlas con el marco de tests jersey
  - `spring-auto-restdocs`: Usa reflexion para auto-documentar parametros
    de entrada y salida
  - `restdocs-api-spec`: Genera especificaciones para OpenAPI2 o 3

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 199. Primer Documentacion con Spring-MVC-REST-Docs

**Nota:** En esta seccion usaremos el mismo proyecto que el anterior.

**Objetivo:** Generaremos documentacion de uso para las APIs en formato
html de forma auto. a partir de los test de los controladores, donde
en estos le agregaremos las decripciones de los datos de entrada y salida

A. Configuramos maven

1. Agregamos dependencia `spring-restdocs-mockmvc`
2. Agregamos plugin `asciidoctor-maven-plugin`

```xml
<dependency>
  <groupId>org.springframework.restdocs</groupId>
  <artifactId>spring-restdocs-mockmvc</artifactId>
  <scope>test</scope>
</dependency>

<plugin>
    <groupId>org.asciidoctor</groupId>
    <artifactId>asciidoctor-maven-plugin</artifactId>
    <version>1.5.3</version>
    <executions>
        <execution>
            <id>generate-docs</id>
            <phase>prepare-package</phase>
            <goals>
                <goal>process-asciidoc</goal>
            </goals>
            <configuration>
                <backend>html</backend>
                <doctype>book</doctype>
            </configuration>
        </execution>
    </executions>
    <dependencies>
        <dependency>
            <groupId>org.springframework.restdocs</groupId>
            <artifactId>spring-restdocs-asciidoctor</artifactId>
            <version>${spring-restdocs.version}</version>
        </dependency>
    </dependencies>
</plugin>
```

B. Creamos plnatilla par la documentacion

1. Creamos la carpeta y archivo main`/asciidoc/index.adoc` que es la
   ruta por defecto que buscara el plugin y agregara la documetnacion
   **cuando ejecutemos las pruebas**.
2. Agregamos el siguiente contenido a index.adoc

```adoc
= SFG Brewery Order Service Docs
John Thompson;
:doctype: book
:icons: font
:source-highlighter: highlightjs

Sample application demonstrating how to use Spring REST Docs with JUnit 5.

`BeerOrderControllerTest` makes a call to a very simple service and produces three
documentation snippets.

`GetById`

One showing how to make a request using cURL:

include::{snippets}/v1/beer-getById/curl-request.adoc[] // ERROR-COMUN: NO PONER LA RUTA BIEN

One showing the HTTP request:

include::{snippets}/v1/beer-getById/http-request.adoc[]

And one showing the HTTP response:

include::{snippets}/v1/beer-getById/http-response.adoc[]

Response Body:
include::{snippets}/v1/beer-getById/response-body.adoc[]

Response Fields:
include::{snippets}/v1/beer-getById/response-fields.adoc[]

`save`

One showing how to make a request using cURL:

include::{snippets}/v1/beer-save/curl-request.adoc[]

One showing the HTTP request:

include::{snippets}/v1/beer-save/http-request.adoc[]

And one showing the HTTP response:

include::{snippets}/v1/beer-save/http-response.adoc[]

Response Body:
include::{snippets}/v1/beer-save/response-body.adoc[]

Response Fields:
include::{snippets}/v1/beer-save/response-fields.adoc[]
```

C. Agregamos contenido a documentacion en nuestros tests, usando la
[Doc](https://docs.spring.io/spring-restdocs/docs/current/reference/html5/#getting-started-documentation-snippets-setup)
que tenemos para el uso de metodos y anotaciones.

1. `RestDocumentationExtension` Debemos de agregar la extension
2. `@AutoConfigureRestDocs`: Auto-configuramos nuestro rest-doc
3. `andDo(document(`: Aqui estamos indicando que agregarle a la documentacion
4. `pathParameters`: Agregamos descripciones para los parametros de la ruta
5. `requestParameters`: Agregamos descripciones para parametros de ruta
    pero con el "?nombre=valor"
6. `responseFields`: Agregamos descripciones para parametros del cuerpo
   de la respuesta en este caso el json que enviamos
7. `requestFields`: Agregamos descripciones para argumentos del cuarpo
   de la peticion, en este caso el json que responde
8. `uriScheme`: Estamos personalizando como se haran las urls para curl
   y tambien para httpie

```java
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get; //CUIDADO DE NO USAR ESTE
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import com.fasterxml.jackson.databind.ObjectMapper;

+ @ExtendWith(RestDocumentationExtension.class)
+ @AutoConfigureRestDocs(uriScheme="https", uriHost="midominio.com", uriPort=80 )
  @WebMvcTest(BeerController.class) @Slf4j
  class BeerControllerTest {

    BeerDto beerExpected, beerExpected2;
    @Autowired MockMvc mockMvc;
    @MockBean private BeerService beerService;
    @Autowired ObjectMapper objectMapper;
    BeerPagedList beerPagedList;

    @BeforeEach
    void setUp() throws Exception {...}

    @Test
    void testGetBeerById() throws Exception {
      log.debug("########### EJECUTANDO TEST> testGetBeerById ########### ");

      DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

      when(beerService.findBeerById( any() )).thenReturn(beerExpected);

      mockMvc.perform(get( "/api/v1/beer/{beerId}", beerExpected.getId())
                .param("miParam", "valorparametro") // no lo usa el controlador asi que lo ignorara
          )
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id", is(beerExpected.getId().toString())))
            .andExpect(jsonPath("$.beerName", is("MyBeer1")))
            .andExpect(jsonPath("$.createdDate", is(dateTimeFormatter.format(beerExpected.getCreatedDate()))))
+           .andDo(
              document("/v1/beer/",
                pathParameters(
+                 parameterWithName("beerId").description("UUID de la cerveza a obtener.")
                ),
                requestParameters(
+                   parameterWithName("miParam").description("miParam es solo un parametro de ejemplo para la doc")
                ),
                responseFields(
                    //CUIDADO: deben de estar todos los campos explicados si no mandara error que falta uno de describir
+                                   fieldWithPath("id").description("Id of Beer"),
                                    fieldWithPath("version").description("Version number"),
                                    fieldWithPath("createdDate").description("Date Created"),
                                    fieldWithPath("lastModifiedDate").description("Date Updated"),
                                    fieldWithPath("beerName").description("Beer Name"),
                                    fieldWithPath("beerStyle").description("Beer Style"),
                                    fieldWithPath("upc").description("UPC of Beer"),
                                    fieldWithPath("price").description("Price"),
                                    fieldWithPath("quantityOnHand").description("Quantity On hand")
                )
              )
            )
      ;
    }

    @Test
      void saveNewBeer() throws Exception {
          BeerDto beerDto =  getValidBeerDto();
          String beerDtoJson = objectMapper.writeValueAsString(beerDto);

          mockMvc.perform(post("/api/v1/beer/")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(beerDtoJson))
                  .andExpect(status().isCreated())
                  .andDo(document("v1/beer",
                          requestFields(
+                                 fieldWithPath("id").ignored(),
                                  fieldWithPath("version").ignored(),
                                  fieldWithPath("createdDate").ignored(),
                                  fieldWithPath("lastModifiedDate").ignored(),
                                  fieldWithPath("beerName").description("Name of the beer"),
                                  fieldWithPath("beerStyle").description("Style of Beer"),
                                  fieldWithPath("upc").description("Beer UPC").attributes(),
                                  fieldWithPath("price").description("Beer Price"),
                                  fieldWithPath("quantityOnHand").ignored()
                          )));
      }

    private BeerDto getValidBeerDto(){
          return BeerDto.builder()
                  .beerName("Nice Ale")
                  .beerStyle(BeerStyleEnum.ALE)
                  .price(new BigDecimal("9.99"))
                  .upc(123123123123L)
                  .build();
      }
  }
```

> **Error**:
>
> ```r
> java.lang.IllegalArgumentException: urlTemplate not found. If you are using MockMvc did you use RestDocumentationRequestBuilders to build the request?
> at org.springframework.util.Assert.notNull(Assert.java:198)
> a t org.springframework.restdocs.request.PathParametersSnippet.extractUrlTemplate(PathParametersSnippet.java:128)
> ```
>
> **Descripcion:** Al ejecutar las pruebas saca este error en las trasas.  
> **Causa:** Estamos usando el metodo get equivocado, ya que necesitamos
> el de `RestDocumentationRequestBuilders` para poder generar la
> documentacion
> **Solucion**: Checar el comentario/CUIDADO del codigo de arriba

D. Generamos documentacion con: `mvn clean package` (clean recomendado)

  1. los snippetsse generan  en `target/generated-snippets/`
  2. La documentacion se genera en `target/generated-docs/index.html`

> **Error:**
>
> ```log
> asciidoctor: WARNING: index.adoc: line 14: include file not found: ./target/generated-snippets/orders/curl-request.adoc
> asciidoctor: WARNING: index.adoc: line 18: include file not found: ./target/generated-snippets/orders/http-request.adoc
> ```
>
> **Descripcion:** El proceso termina bien pero si abrimos la
> documentacion no aparecera que no encontro unas cosas y realmente
> no esta incluyendo nada.  
> **Causa-1:** Si nos fijamos en `generated-snippets` tenemos todos los
> snnipted mesclados ya asi lo definimos en `andDo(document("XXX")` y
> si nos fijamos tienen el mismo.
> **Causa-2:** Tenemos mal la ruta en `index.adoc`
> **Solucion-1:** ver que en `document("XXX")` tenegamos uno por cada
> controlador, que no se repitan.
> **Solucion-2:** Definir bien la ruta en el `index.adoc`

**Resultado:** Generamos una documentacion con la informacion que def.
en los tests, que tiene caracteristicas que nos obliga a tenerla adecuada
como el que nos oblique a describir todos los campos si no falla el test.

![14-documentacion-Spring-MVC-REST-Docs](./img/14-documentacion-Spring-MVC-REST-Docs.PNG)

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

----

## Tips cosas que voy encontrando

### Construir reporte de tests

- Fuente: [maven-surefire](https://maven.apache.org/surefire/maven-surefire-report-plugin/usage.html)

Solo debemode agregar un pligin a nuestro pom y cuando se ejecuten
nuestros test se generaran de forma automatica.

A. Agregamos plugin

```xml
<project>
  ...
  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-report-plugin</artifactId>
        <version>3.0.0-M5</version>
      </plugin>
    </plugins>
  </reporting>
  ...
</project>
```

B. Ejecutamos tests `mvn test` o cualquier comando que tambien los
ejecute

C. Veremos en `./back/target/surefire-reports/` unos xmls que podremos
usar para mostrar los reportes.

**Resultado:** Aprendimos a como generar reportes que podriamos usar
por ejemplo en jenkins

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Testear Validaciones de datos de javax.validation.constraints

**Objetivo:** Aprender a como testear pojos/entidades que le agregamos
validaciones con estas anotaciones de este paquete

A. Debemos de tener las siguientes dependencias, el `validation-api` es
el que definira las validaciones y `hibernate-validator` sera el que
realizara las validaciones aunque podemos usar otro validador.

```xml
<dependency>
  <groupId>javax.validation</groupId>
  <artifactId>validation-api</artifactId>
  <version>2.0.0.Final</version>
</dependency>

<dependency>
  <groupId>org.hibernate</groupId>
  <version>5.2.4.Final</version>
  <artifactId>hibernate-validator</artifactId>
</dependency>
```

B, Creamos nuestro pojo/entidad, Cmo vemos tenemos res campos de los cuales
esamos validando que tengan un rango de caraceres y que estos solo tengan
caraceres alpha con espacios o puntos

```java
//sin anoaciones aqui arriba
public class UsuarioEntity {

  @Size(min = 3, max = 70, message = "El titulo debe estar ente 3 y 70 caracteres")
  @Pattern(regexp = "^[a-zA-Z .]{0,}$", message = "El titulo tiene caracteres incorrectos")
  private String titulo;

  @Size(min = 3, max = 70, message = "El nombre debe tener entre 3 y 70 catacteres")
  @Pattern(regexp = "^[a-zA-Z .]{0,}$", message = "El nombre tiene caracteres incorrectos")
  private String nombre;

  @Size(min = 3, max = 70, message = "El rol debe tener entre 3 y 70 catacteres")
  @Pattern(regexp = "^[a-zA-Z .]{0,}$", message = "El rol tiene caracteres incorrectos")
  private String roles;

  // GETTERS / SETTERS
```

C. Creamos nuestros test, el cual debemos de crear el validador

```java
import javax.validation.Validator;import javax.validation.ValidatorFactory;import javax.validation.ConstraintViolation;import javax.validation.Validation;
import org.junit.jupiter.api.BeforeEach;import org.junit.jupiter.api.Test;import org.slf4j.Logger;import org.slf4j.LoggerFactory;

public class UsuarioEntityTest {
  private static final Logger logger = LoggerFactory.getLogger(UsuarioEntityTest.class);

  ValidatorFactory factory;
  Validator validator;

  @BeforeEach
  public void setup() {
    factory = Validation.buildDefaultValidatorFactory(); // este dependera de hibernate u oro parecido
    validator = factory.getValidator();
  }

  @Test
  public void estos_usuarios_deben_tener_errores_en_titulo_nombre_y_roles() {
    List<UsuarioEntity> usuariosMalos = new ArrayList<>();
    usuariosMalos.add(new UsuarioEntity(-1, "f", "f", "f", true, new Date()));
    usuariosMalos.add(new UsuarioEntity(-2, "fo", "fo", "fo", false, new Date()));
    usuariosMalos.add(new UsuarioEntity(-3, "fo`", "fo\'", "fo\"", false, new Date()));
    usuariosMalos.add(new UsuarioEntity(-4, "fooo#", "foo\\", "fooo*", false, new Date()));
    usuariosMalos.add(new UsuarioEntity(-5, "fo o<", "fo 0)", "fo o&", false, new Date()));

    for (UsuarioEntity usuario : usuariosMalos) {
      logger.info("Validando: " + usuario.getIdUsuario());
      Set<ConstraintViolation<UsuarioEntity>> violations = validator.validate(usuario);
      if(violations.size() != 3) {
        assertTrue(false, "Debio de haber 3 errores en el usuario id: " + usuario.getIdUsuario());
      }
      violations = null;
    }
  }

  @Test
  public void estos_usuarios_deben_de_pasar() {
    List<UsuarioEntity> usuariosMalos = new ArrayList<>();
    usuariosMalos.add(new UsuarioEntity(1, "ing.", "Mario Marquez", "root, admin, editor", true, new Date()));
    usuariosMalos.add(new UsuarioEntity(2, "lic.", "Neey Lima", "ADMIN, editor", true, new Date()));
    usuariosMalos.add(new UsuarioEntity(3, "dr.", "Manjatan", "editOR", true, new Date()));
    usuariosMalos.add(new UsuarioEntity(4, "Ingeniero en ciencias de la informacion y comunicacion", "Jose San Juan Pedro de los matorrales de los no nacidos Gonzales Marquez de la luz", "root", true, new Date()));

    for (UsuarioEntity usuario : usuariosMalos) {
      logger.info("Validando: " + usuario.getIdUsuario());
      Set<ConstraintViolation<UsuarioEntity>> violations = validator.validate(usuario);
      if(!violations.isEmpty()) {
        assertTrue(false, "Debio de haber pasado el usuario con id: " + usuario.getIdUsuario());
      }
      violations = null;
    }
  }
}
```

**Resultado:** Vimos como testear el funcionamiento de las validaciones
por anotaciones del paquete `javax.validation.*`, y de forma concisa
la validacion de un rango de caracteres y que pase una exprecion regular.
