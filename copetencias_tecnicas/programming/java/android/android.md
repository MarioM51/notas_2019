- [Android](#android)
  - [1. Android Studio primeros pasos](#1-android-studio-primeros-pasos)
    - [Qué información podemos obtener del LogCat Desarrollo Androi](#qué-información-podemos-obtener-del-logcat-desarrollo-androi)
    - [Maneras de visualizar nuestro proyecto en Android Studio Des](#maneras-de-visualizar-nuestro-proyecto-en-android-studio-des)
    - [Fichero build.gradle](#fichero-buildgradle)
    - [Arquitectura de android, S02-CL04: Estructura de proyecto Android](#arquitectura-de-android-s02-cl04-estructura-de-proyecto-android)
    - [009 El Android manifets.xml](#009-el-android-manifetsxml)
    - [010 Primer activity e intent](#010-primer-activity-e-intent)
    - [9. La estructura de una actividad en Java](#9-la-estructura-de-una-actividad-en-java)
    - [10. La estructura de un Layout en XML 11. Mi primer app en Android, Hello World!](#10-la-estructura-de-un-layout-en-xml-11-mi-primer-app-en-android-hello-world)
  - [Sección 3: Día 2: Conociendo la herramienta y el diseño de interfícies gráficas](#sección-3-día-2-conociendo-la-herramienta-y-el-diseño-de-interfícies-gráficas)
    - [15. Un paseo por la interfaz de Android Studio](#15-un-paseo-por-la-interfaz-de-android-studio)
    - [16. El editor de Layouts de Android Studio](#16-el-editor-de-layouts-de-android-studio)
    - [17. El sistema de archivos de Android y Graddle](#17-el-sistema-de-archivos-de-android-y-graddle)
    - [19. Los layouts para el diseño de interfaces](#19-los-layouts-para-el-diseño-de-interfaces)
    - [27. Listas con Scrolls Views](#27-listas-con-scrolls-views)
    - [28. Creando un formulario de registro](#28-creando-un-formulario-de-registro)
    - [30. El ciclo de vida de una app](#30-el-ciclo-de-vida-de-una-app)
    - [32. El ciclo de vida a través de tostadas](#32-el-ciclo-de-vida-a-través-de-tostadas)
    - [33. Inflando menús de ajustes](#33-inflando-menús-de-ajustes)
    - [76. Instanciando widgets y layouts por código](#76-instanciando-widgets-y-layouts-por-código)
    - [78. Los radio groups y los radio buttons](#78-los-radio-groups-y-los-radio-buttons)
    - [79. Los switchs y los checks](#79-los-switchs-y-los-checks)
    - [80. Las web views y los selectores de fechas y horas](#80-las-web-views-y-los-selectores-de-fechas-y-horas)
  - [87. Día 8: Diálogos e inicio del primer proyecto](#87-día-8-diálogos-e-inicio-del-primer-proyecto)
  - [Sección 9: Día 8: Diálogos y constructores](#sección-9-día-8-diálogos-y-constructores)
    - [91. Las webs de recursos gratuitos de Android](#91-las-webs-de-recursos-gratuitos-de-android)
    - [92. Los recursos de String en Android](#92-los-recursos-de-string-en-android)
    - [94. El layout de creación de la nota](#94-el-layout-de-creación-de-la-nota)
    - [95. El layout de consultar una nota](#95-el-layout-de-consultar-una-nota)
    - [96. La lógica de creación de la nota](#96-la-lógica-de-creación-de-la-nota)
    - [97. La lógica de consulta de la nota](#97-la-lógica-de-consulta-de-la-nota)
    - [98-99. Abriendo y consultando la nota  Creando y consultando nuestra nota](#98-99-abriendo-y-consultando-la-nota--creando-y-consultando-nuestra-nota)
  - [Sección 10: Día 9: Arrays y colecciones de objetos](#sección-10-día-9-arrays-y-colecciones-de-objetos)
    - [Ejercicio 9: Arrays multidimensionales y preguntas aleatorias](#ejercicio-9-arrays-multidimensionales-y-preguntas-aleatorias)
    - [108. El layout de la lista de tareas](#108-el-layout-de-la-lista-de-tareas)
    - [109. El adapter de nuestra lista de tareas](#109-el-adapter-de-nuestra-lista-de-tareas)
  - [Sección 11: Día 10: La persistencia de datos en las apps de Android](#sección-11-día-10-la-persistencia-de-datos-en-las-apps-de-android)
    - [113. Los intents y la navegación entre actividade](#113-los-intents-y-la-navegación-entre-actividade)
    - [114. Diseñando el layout de ajustes](#114-diseñando-el-layout-de-ajustes)
    - [117. Persistir el entero de modo de animación](#117-persistir-el-entero-de-modo-de-animación)
    - [118. Errores y excepciones en Java](#118-errores-y-excepciones-en-java)
    - [120. El serializador y deserializador de JSON](#120-el-serializador-y-deserializador-de-json)
    - [121. Guardar y cargar notas con el serializador](#121-guardar-y-cargar-notas-con-el-serializador)
  - [123. Las animaciones de objetos de UI](#123-las-animaciones-de-objetos-de-ui)
    - [125. Creando las animaciones en Java](#125-creando-las-animaciones-en-java)
    - [126. Ejercicio 10: El layout de animaciones / 129. El uso de la seekbar](#126-ejercicio-10-el-layout-de-animaciones--129-el-uso-de-la-seekbar)
    - [130. Animaciones en la app de notas](#130-animaciones-en-la-app-de-notas)
  - [Sección 13: Día 12: Sonidos en las aplicaciones y buenas prácticas](#sección-13-día-12-sonidos-en-las-aplicaciones-y-buenas-prácticas)
    - [133. 134. El objeto Sound Pool para cargar efectos de sonido](#133-134-el-objeto-sound-pool-para-cargar-efectos-de-sonido)
    - [139. Eliminar una nota con un click largo](#139-eliminar-una-nota-con-un-click-largo)
    - [140. Acerca de la clase Media Player para crear un reproductor](#140-acerca-de-la-clase-media-player-para-crear-un-reproductor)
    - [141. Buenas prácticas: Modelo - Vista - Controlador](#141-buenas-prácticas-modelo---vista---controlador)
    - [143. La clase Display para obtener orientación y resolución 144. Los layouts para diferentes resoluciones en Android](#143-la-clase-display-para-obtener-orientación-y-resolución-144-los-layouts-para-diferentes-resoluciones-en-android)
    - [147. El concepto de fragmento](#147-el-concepto-de-fragmento)
    - [148. La lógica de un fragment](#148-la-lógica-de-un-fragment)
    - [149. Los síngletons y su uso](#149-los-síngletons-y-su-uso)
    - [150. Programando un síngleton en Java](#150-programando-un-síngleton-en-java)
    - [151. Interfaces y su papel en el desarrollo en Java](#151-interfaces-y-su-papel-en-el-desarrollo-en-java)
    - [152. Cómo crear un alias de un fichero en Android](#152-cómo-crear-un-alias-de-un-fichero-en-android)
    - [153. El modelo de datos de los contactos](#153-el-modelo-de-datos-de-los-contactos)
    - [155. Diseñando las dos vistas de la app](#155-diseñando-las-dos-vistas-de-la-app)
    - [154 156. Creando un Array List Adapter para mostrar los contactos en pantalla, Cargar los contactos con un fragment list](#154-156-creando-un-array-list-adapter-para-mostrar-los-contactos-en-pantalla-cargar-los-contactos-con-un-fragment-list)
    - [157. La lógica de los dos tipos de layout](#157-la-lógica-de-los-dos-tipos-de-layout)
    - [159. El layout del contacto completo en vertical](#159-el-layout-del-contacto-completo-en-vertical)
  - [Sección 15: Día 14: Swipes, Pager Views o cómo hacer un tutorial guiado](#sección-15-día-14-swipes-pager-views-o-cómo-hacer-un-tutorial-guiado)
    - [163-164-165-166 Swipes y resoluciones de imágenes - El layout del view pager - La lógica del view pager - La lógica del Main Activity](#163-164-165-166-swipes-y-resoluciones-de-imágenes---el-layout-del-view-pager---la-lógica-del-view-pager---la-lógica-del-main-activity)
    - [167.168. Definiendo un View Pager con Fragments - El View Pager Adapter](#167168-definiendo-un-view-pager-con-fragments---el-view-pager-adapter)
  - [Sección 16: Día 15: Drawer Layouts, Menús y toma de fotos](#sección-16-día-15-drawer-layouts-menús-y-toma-de-fotos)
    - [173. Creando los tres fragmentos de la app](#173-creando-los-tres-fragmentos-de-la-app)
    - [174. Diseñando el layout del Drawer](#174-diseñando-el-layout-del-drawer)
    - [176. Un switch para seleccionar el fragmento correcto](#176-un-switch-para-seleccionar-el-fragmento-correcto)
    - [177. El botón para abrir y cerrar el menú drawer](#177-el-botón-para-abrir-y-cerrar-el-menú-drawer)
    - [178. La lógica del botón atrás](#178-la-lógica-del-botón-atrás)
    - [181.183 Un intent para abrir la cámara de fotos - La lógica del intent con resultado final](#181183-un-intent-para-abrir-la-cámara-de-fotos---la-lógica-del-intent-con-resultado-final)
    - [184. El layout para la toma de fotos de nuestra app](#184-el-layout-para-la-toma-de-fotos-de-nuestra-app)
    - [185. La clase URI y su rol en la creación de un fichero en la memoria externa . Creando el fichero temporal externo para guardar la foto tomada](#185-la-clase-uri-y-su-rol-en-la-creación-de-un-fichero-en-la-memoria-externa--creando-el-fichero-temporal-externo-para-guardar-la-foto-tomada)
  - [Sección 17: Día 16: Bases de datos en SQLite](#sección-17-día-16-bases-de-datos-en-sqlite)
    - [192.193.194. Diseñando la interfaz de prueba, El data manager de SQLite, Implementando los métodos del data manager](#192193194-diseñando-la-interfaz-de-prueba-el-data-manager-de-sqlite-implementando-los-métodos-del-data-manager)
    - [195. La actividad principal de la base de datos](#195-la-actividad-principal-de-la-base-de-datos)
    - [196. La clase foto de nuestra app](#196-la-clase-foto-de-nuestra-app)
    - [197.198.199.200. El data manager de la app. Insertando la foto en la base de datos, Consultas varias al data manager, OnCreate para generar las tablas de la base de datos](#197198199200-el-data-manager-de-la-app-insertando-la-foto-en-la-base-de-datos-consultas-varias-al-data-manager-oncreate-para-generar-las-tablas-de-la-base-de-datos)
    - [201. Persistiendo la foto en la base de datos](#201-persistiendo-la-foto-en-la-base-de-datos)
    - [204. El layout para visualizar la foto y el mapa](#204-el-layout-para-visualizar-la-foto-y-el-mapa)
    - [206. El fragmento con la lista de títulos de fotos](#206-el-fragmento-con-la-lista-de-títulos-de-fotos)
    - [207. El fragmento con la lista de etiquetas de fotos](#207-el-fragmento-con-la-lista-de-etiquetas-de-fotos)
  - [Sección 18: Día 17: Mapas y GPS](#sección-18-día-17-mapas-y-gps)
    - [208.209. Los mapas y el GPS en móvil, El location manager de Android](#208209-los-mapas-y-el-gps-en-móvil-el-location-manager-de-android)
    - [210.211. Los Google Play Services - Integrando Google Maps en un proyecto de Android](#210211-los-google-play-services---integrando-google-maps-en-un-proyecto-de-android)
    - [214. Actualizar una base de datos ya existente cuando subimos de versión](#214-actualizar-una-base-de-datos-ya-existente-cuando-subimos-de-versión)
    - [215. Guardar las coordenadas GPS de la foto](#215-guardar-las-coordenadas-gps-de-la-foto)
    - [216. Mostrar el mapa de Google con un Intent](#216-mostrar-el-mapa-de-google-con-un-intent)
  - [<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />](#)
- [Master Desarrollo Android con Java y Kotlin [Octubre 2019]](#master-desarrollo-android-con-java-y-kotlin-octubre-2019)
    - [28. S03-CL13: RecyclerView en FragmentList](#28-s03-cl13-recyclerview-en-fragmentlist)
    - [29. S03-CL14: RecyclerView: Adapter personalizado (1ª parte)](#29-s03-cl14-recyclerview-adapter-personalizado-1ª-parte)
    - [31. S03-CL16: RecyclerView: mejorando el Look & Feel (Material Design)](#31-s03-cl16-recyclerview-mejorando-el-look--feel-material-design)
    - [32. S03-CL17: RecyclerView: haciendo uso de CardView](#32-s03-cl17-recyclerview-haciendo-uso-de-cardview)
  - [Sección  Sección 5: Android Jetpack: Responsive Design](#sección--sección-5-android-jetpack-responsive-design)
  - [Sección 8: MiniTwitter: Consumir API con Retrofit + Patrones de diseño (Java)](#sección-8-minitwitter-consumir-api-con-retrofit--patrones-de-diseño-java)
    - [69. S08-CL05: Creación app MiniTwitter y recursos](#69-s08-cl05-creación-app-minitwitter-y-recursos)
    - [70. S08-CL06: Diseño login app MiniTwitter](#70-s08-cl06-diseño-login-app-minitwitter)
    - [71. S08-CL07: Diseño Signup app MiniTwitte](#71-s08-cl07-diseño-signup-app-minitwitte)
    - [72. S08-CL08: Personalizando el Estilo de la app](#72-s08-cl08-personalizando-el-estilo-de-la-app)
    - [73. S08-CL09: Instalación Librería Retrofit](#73-s08-cl09-instalación-librería-retrofit)
    - [74. S08-CL10: Definición de clases POJO: Login y Registro](#74-s08-cl10-definición-de-clases-pojo-login-y-registro)
    - [75. S08-CL11: Definición Servicio Retrofit: login y registro](#75-s08-cl11-definición-servicio-retrofit-login-y-registro)
    - [76. S08-CL12: Conexión Retrofit (patrón Singleton)](#76-s08-cl12-conexión-retrofit-patrón-singleton)
    - [77. S08-CL13: Implementación petición POST Login](#77-s08-cl13-implementación-petición-post-login)
    - [78. S08-CL14: Implementación petición POST Registro](#78-s08-cl14-implementación-petición-post-registro)
    - [79. S08-CL15: SharedPreferences: almacenamiento del Auth Token](#79-s08-cl15-sharedpreferences-almacenamiento-del-auth-token)
    - [80. S08-CL16: SharedPreferences + Interceptor: obtención del Auth Token](#80-s08-cl16-sharedpreferences--interceptor-obtención-del-auth-token)
    - [81. S08-CL17: Toolbar personalizado](#81-s08-cl17-toolbar-personalizado)
    - [82.83 S08-CL18--CL19: TweeListFragment + Definición POJO Tweet + Diseño del TweetList Adapter](#8283-s08-cl18--cl19-tweelistfragment--definición-pojo-tweet--diseño-del-tweetlist-adapter)
    - [84. S08-CL20: Implementación de petición GET: listado de Tweets](#84-s08-cl20-implementación-de-petición-get-listado-de-tweets)
    - [85. S08-CL21: Patrón de diseño Android Jetpack: Repository](#85-s08-cl21-patrón-de-diseño-android-jetpack-repository)
    - [86. S08-CL22: Patrón de diseño Android Jetpack: ViewModel](#86-s08-cl22-patrón-de-diseño-android-jetpack-viewmodel)
    - [87. S08-CL23: DialogFragment: nuevo tweet](#87-s08-cl23-dialogfragment-nuevo-tweet)
    - [88.89. S08-CL24: Implementación petición POST Nuevo Tweet - CL25 DialogFragment: llamada a petición POST Nuevo Tweet](#8889-s08-cl24-implementación-petición-post-nuevo-tweet---cl25-dialogfragment-llamada-a-petición-post-nuevo-tweet)
    - [91. S08-CL27: Swipe-to-Refresh en la lista de Tweets](#91-s08-cl27-swipe-to-refresh-en-la-lista-de-tweets)
    - [92. S08-CL28: Petición POST: like en tweet](#92-s08-cl28-petición-post-like-en-tweet)
    - [93. S08-CL29: FragmentList Tweets favoritos](#93-s08-cl29-fragmentlist-tweets-favoritos)
    - [94. S08-CL30: Tweets Favoritos: Repository y ViewMode](#94-s08-cl30-tweets-favoritos-repository-y-viewmode)
    - [95. S08-CL31: Petición DELETE: eliminar tweet](#95-s08-cl31-petición-delete-eliminar-tweet)
    - [96. S08-CL32: Petición DELETE: Repository y ViewModel](#96-s08-cl32-petición-delete-repository-y-viewmodel)
    - [97. S08-CL33: Petición DELETE: BottomSheetDialogFragment](#97-s08-cl33-petición-delete-bottomsheetdialogfragment)
    - [98. S08-CL34: Petición DELETE: Rediseñando Adapter](#98-s08-cl34-petición-delete-rediseñando-adapter)
    - [99. S08-CL35: Fragment Perfil Usuario](#99-s08-cl35-fragment-perfil-usuario)
    - [100. S08-CL36: Petición GET y PUT: datos perfil](#100-s08-cl36-petición-get-y-put-datos-perfil)
    - [101. S08-CL37: Petición GET y PUT: Repository y ViewModel](#101-s08-cl37-petición-get-y-put-repository-y-viewmodel)
    - [102. S08-CL38: Petición PUT perfil: Repository y ViewModel](#102-s08-cl38-petición-put-perfil-repository-y-viewmodel)
    - [102. MIO: Manejo de errores MVVM en peticion y actuar sobre la vista](#102-mio-manejo-de-errores-mvvm-en-peticion-y-actuar-sobre-la-vista)
    - [103. S08-CL39: Petición POST: subir foto de perfil (MVVM)](#103-s08-cl39-petición-post-subir-foto-de-perfil-mvvm)
    - [104. S08-CL40: Petición POST: subir foto de perfil (MVVM)](#104-s08-cl40-petición-post-subir-foto-de-perfil-mvvm)
    - [105. S08-CL41: Seleccionar fotos: gestión de permisos](#105-s08-cl41-seleccionar-fotos-gestión-de-permisos)
    - [106. S08-CL42: Intent selección foto de galería](#106-s08-cl42-intent-selección-foto-de-galería)
- [Por el Camino](#por-el-camino)
  - [No detecta el celular](#no-detecta-el-celular)









# Android
Codigo completo: [**URL**](https://gitlab.com/MarioM51/hola_android) 


## 1. Android Studio primeros pasos
- ```Alt + Enter``` : Cuando tengamos un error, warning, etc esta cambinacion 
nos dara recomendaciones soluciones.   

### Qué información podemos obtener del LogCat Desarrollo Androi
- **Verbose** : Podemos ver todos los logs

- **Debu** : Todos aquellos catalogados como debug a Asserts, no podremos 
ver los catalogados como verbose.

- **Info** : No veremos Debubg ni verbose, pero si info a Assert

- **warn** : Lo mismo

- **Error** : Lo mismo

- **Assert** : Solo podremos ver Asserts

```java
Log.d(TAG, "onCreate: " + getString(R.string.hello_word));  // debug
Log.v                                                       // verbose
Log.i                                                       // info
Log.w                                                       // warn
Log.e                                                       // error
Log.i                                                       // info
```



### Maneras de visualizar nuestro proyecto en Android Studio Des
![1_vistas_de_proyecto](./img/1_vistas_de_proyecto.PNG) Por defecto 
la vista esta en **Android**, **Project** lo podemos ver igual que en un 
explorador de archivos, incluso ocultos **Package** Solo veremos los paquetes 
y lo contenido aqui, **





### Fichero build.gradle
Tenemos 2 tipos de archivos gradle uno que es a nivel de aplicacion, y otro por 
modulo, cada uno solo afecta a su respectivo nivel




### Arquitectura de android, S02-CL04: Estructura de proyecto Android
![2_arquitectura_android](./img/2_arquitectura_android.PNG)   

- Creamos el proyecto que tiene todos los recursos y luego pasa a apk

- El apk de desempaqueta y este corre sobre la Dalvik Virtual Machine que 
es el nucleo de android, y este corre sobre un kernel de linux, el cual mas 
abajo es modificado por los fabricantes.



### 009 El Android manifets.xml
Tiene toda la informacion del proyecto y configuracion como:
  - Todas nuestras activities deben de estar registrados en este archivo e indicamos 
  su jerarquia, cual es la principal
  - barra de navegacion
  - permisos para el hardware (camara, ubicacion, sensores) y datos (contactos) del usuario
  - Donde esta el icono de la app.  
  - El launcher
  - Podemos agregar mas de un LAUNCHER (pantalla de inicio), lo cual crearia mas de un icono 
  y cada icono abriria su pantalla correspondiente, un ejemplo de esto es Google Drive el 
  cual tiene un launcher para excel, pdf, Documents, etc, osea parece que fueran varias 
  aplicaciones pero en realidad es solo una.   



### 010 Primer activity e intent
1. Creamos nuevo activity con la ayuda de Android Studio.   
![2_crear_activity](./img/2_crear_activity.gif)   
Si nos fijamos en el **manifest** se crea el siguiente xml en el cual 
tenemos el ```android:parentActivityName``` el cual se usa para mostrar 
el header el boton para retroceder.   
```xml
<activity
	android:name=".OpcionDosActivity"
	android:label="@string/pagina_2"
	android:parentActivityName=".MainActivity">
</activity>
```

2. Agregamos un boton **layout/activity_main.xml** arrastrando 
un boton


3. En el **MainActivity.java** hacemos referencia al boton y le agregamos 
un listener para que nos muestre la pantalla dos con ```Intent```.
```java
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState); setContentView(R.layout.activity_main);

	Button btnOptA = (Button)findViewById(R.id.opt_a);
	btnOptA.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(MainActivity.this, OpcionDosActivity.class));
		}
	});
}
```

**Resultado:**   
![3_nav_basica.gif](./img/3_nav_basica.gif)   





<br /><br />
### 9. La estructura de una actividad en Java
<!-- agregar logs en android, tipos de logs-->
Para poner los logs en android lo hacemos atravez de Log, 
podemos trae recursos con R, y para obtener su valor usamos **getString**

- La clase Bundle nos sierve para pasar informacion entre activities

- **AppCompatActivity** : Esta tiene Compat de Compativilidad para que 
sea compatible con versiones anteriores de android.  




<br /><br />
### 10. La estructura de un Layout en XML 11. Mi primer app en Android, Hello World!
- **android:layout_width** : Obligatorio, indica que ancho debe 
de tener el elemento, tiene el **match** que cubre todo el espacio en el 
que esta contenido, y el **wrap** que se adapta segun el tamaño se su contenido.

- **android:layout_heigh** : Obligatorio, lo mismo pero para el alto.

- **tools:context** : la clase que lo gobierna, puede ser governada por mas de una clase.   

- Por defecto tenemos un layout de restricciones el cual tenemos 
cosas como conexiones y cantidad de gravedad, para posicionar.
![4_layout_constrains](./img/4_layout_constrains.PNG)

- **dp vs px** : Procurar usar el uso de **dp (densidad de pexeles)** ya que los 
telefonos de alta resolucion tiene los pixeles mas pegados.   

- **LinearLayout** : Tenemos dos diferentes tipos los que son 
horizontales y verticales, aqui no aplica lo que son las restricciones.   

- **RelativeLayout** : Cuando tenemos esto podemos hacer uso de **android:layout_below="@+id/button2"** 
y otros el cual indicamos cosas como colocate abajo, arriba, a la derecha o izq de X elemento.   

- **gravity** : Este es la posicion de sus elementos internos del componente.   
![5_gravity.gif](./img/5_gravity.gif)













<br /><br /><br /><br /><br />
## Sección 3: Día 2: Conociendo la herramienta y el diseño de interfícies gráficas
**Android Drawable Importer**: Es una herramienta que nos ayuda a crear las diferentes 
resoluciones de las imagenes.


### 15. Un paseo por la interfaz de Android Studio
Android Studio es una navaja suiza enorme el cual facilmente podemos perder una 
herramienta la cual puede ser bastante util.   

De la barra superior (hasta arriba) tenemos los siguintes menus

- **Build**   
  - **Clean Project** : Si alguna vez el IDE no encuentra un recurso que 
  nosotros estamos seguros que esta ahi, con este podemos hacer que lo reconosca.   
  
  - **Generate Signed APK...** : Firmamos para poder subirlo a la app store
  
  - **Build APK** : Generamos el apk para compartirla por correo o etc en forma de archivo.
  



### 16. El editor de Layouts de Android Studio
Tenemos opciones como poder.
  - Cambiar el idioma
  - Cambiar el dispositivo incluso al que estamos emulando
  - cambier orientacion
  


### 17. El sistema de archivos de Android y Graddle
- Si ponemos un archivo en un lugar que no le corresponde el pryecto va a causar 
error, esto es por que gradle el notar un nuevo recurso se lo notifica al IDE.   

- En la carpeta **drawable** (dibujable) tenemos los diferentes graficos y podemos 
dividir la misma imagen en diferentes resoluciones con **drawable-hdpi** y con las 
demas resoluciones que comunmente tiene los siguintes tamaños. Los cuales estan en 
**DPI = Density Pixels by Inche**    
  - **ldpi    Low Density** : 120dpi
  - **mdpi    medium Density** : 160dpi
  - **hdpi    High Density** : 240dpi
  - **xhdpi   Extra Density** : 320dpi
  - **xxhdpi  Extra Extra Density** : 480dpi
  - **xxxhdpi Extra Extra ExtraDensity** : 640dpi

Con esto podemos estimar los tamaños de pixeles que deberian de tener las imagenes.   


- **layout** : Es esta carpeta tenemos la misma idea donde por cada diferente pantalla 
podemos tener un diseño diferente, lar carpetas deben ser las siguintes segun la necesidad.   
  - **layout**      : layout por defecto
  - **layout-land** : layout por defecto landscape
  - **layout-ldpi** : layout dispo pequeños 
  - layout-ldpi-land, layout-xxxhdpi, layout-xxxhdp-land, etc


- **mipmap** : Solo guardamos los iconos

- **values** : Tenemos todos los datos que no queremos tener en duro en el codigo





<br /><br />
### 19. Los layouts para el diseño de interfaces
- ejes del planno carteciano, **X=horizontal Y=vertical**

- **sp (scalable pixel)** : Esta unidad es recomendable ponersela a los textos. el 
cual agrega calculos para saber que tama;o deberia tener un texto, segun la diferencia 
de dencidad de pixeles.   


**Uso de layout_weight:** Cuando tonemos un linearlayout (orientacion obligatorioa) 
digamos vertical y tenemos varios elemntos sobre el, podemos defininir su altura, en 
este caso por que pusimos vertical, en caso de horizontal asignariamos el ancho, 
el weight nos permitiria **dar el tamaño relativo a otro**, por ejemplo que un texto 
sea el doble que el otro.   
![6_weight.gif](./img/6_weight.gif)   
```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    ... android:orientation="vertical">
    
    <TextView android:id="@+id/textView1" android:layout_width="match_parent" android:layout_height="wrap_content"
+       android:layout_weight="1"
+       android:height="0dp"
+       android:text="@string/uno" />
    ...
```
**Nota**: Debemos de tener declarado *android:height="0dp"* para usarlo 


### 27. Listas con Scrolls Views
**Objetivo:** Vamos a agregar una vista horizontal para aplicacion tipo watssapp 
en la cual en la parte izq tenemos una lista de contactos y a la derecha tenemos 
una lista de mensajes.

1. Agregamos una estructura, padre linear layout horizontal y 2 hijos linear layouts 
verticales.   
2. Agregamos un scrollview al layout de la izq, y agregamos varios texts para poder ver 
el efecto del scroll

![7_layout_scrollview.PNG](./img/7_layout_scrollview.PNG)




### 28. Creando un formulario de registro
Nota: Por alguna razon no tengo el RelativeLayout por lo que voy a usar el 
Constrainlayout, y para padre un LinearLayout horizontal ya que el Constrainlayout 
no me sirve para proporciones, y asi asignar un mitad/midad.   
![8_layout_formulario_terminos](./img/8_layout_formulario_terminos.PNG)




### 30. El ciclo de vida de una app
Ahi una diferencia entre los SO de mobil y de Desklop que es basicamente que en 
movil el usuario normalmente no decide que app se cierra pero si cual esta al frente, 
por lo cual en movil a mitad de un formulario puede llegar una llamada y debemos de 
tener la capacidad de guardar el estado.    
Y para detectar este tipo de cambios sobre nuestra app tenemos los ciclos de vida, el 
cual tenemos los siguintes cilcos.   
![9_cliclo_de_vida](./img/9_cliclo_de_vida.PNG)   
 

- **onCreate()** : El método onCreate() es llamado justo cuando se lanza la actividad. 
Dentro de este método es donde se coloca el código que inicializa las principales funciones:   
  - Infla los widgets y los posiciona en la pantalla.
  - Obtiene las referencias de estos widgets.
  - Asigna listeners a los widgets para que el usuario pueda interactuar con ellas.


- **onStart()** : El método onStart() es llamado después de onCreate() y es el momento en 
el que la actividad está a punto de volverse visible al usuario.


- **onResume()** : El método onResume() es llamado después de onStart(). En este punto 
la actividad por fin está en primer plano y se vuelve visible al usuario, éste puede 
interactuar con los elementos o widgets que existan en la pantalla, **por ejemplo** 
cuando nos salimos de la app por que nos llego un mensaje y abrimos otra app por su 
notificacion y al terminar regresamos a la app.   


- **onPause()** : Es llamado cuando la actividad pierde el foco o deja de estar en primer 
plano. Esto puede ser causado por dos acciones:   
  1. El usuario presiona el botón Home   
  2. El usuario presiona el botón de aplicaciones recientes.   
  3. La actividad se vuelve parcialmente visible.   

En este punto el rumbo de la actividad podría tomar dos caminos:   
  1. onResume() cuando la actividad vuelve a estar en primer plano.   
  2. onStop() cuando la actividad deja de ser visible al usuario.   


- **onStop()** : Cuando la actividad deja de ser visible, puede ser causado porque 
la actividad ha sido destruida, una nueva actividad está siendo lanzada o una 
actividad existente vuelve a estar en primer plano. **Por ejemplo** se destruye la 
interface grafica, pero aun permanece algun hilo de ejecucion.   

Éste método puede tomar dos caminos:   
  - onRestart() si la actividad vuelve a ser visible para el usuario.   
  - onDestroy() si la actividad es destruida.   


  
- **onRstart()** El método onRestart() es llamado después de onStop() si es que el 
usuario eligió poner nuevamente la actividad en primer plano.   


- **onDestroy()** Cuando el usuario elige dar por finalizada su interacción con la 
actividad. Este método permite liberar recursos que se han estado utilizando por la 
actividad. **por ejemplo** cuando tenemos la app en segundo plano y el SO se empieza a 
quedar sin memoria o el usuario fuerza el cerrado, **entonces** se eliminan todas las 
variables que teniamos.   

![10_tabla_ciclos](./img/10_tabla_ciclos.PNG)   



### 32. El ciclo de vida a través de tostadas
Ciclos de vida codigo
<!-- crear un toast, uso de toast-->
```java
@Override protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(...); setContentView(...); Toast.makeText(this, "Estoy rn onCreate", Toast.LENGTH_SHORT).show(); 
}
@Override protected void onStart() {
  super.onStart(); Toast.makeText(this, "Estoy rn onStart", Toast.LENGTH_SHORT).show();
}
@Override protected void onResume() {
  super.onResume(); Toast.makeText(this, "Estoy rn onResume", Toast.LENGTH_SHORT).show();
}
@Override protected void onPause() {
  super.onPause(); Toast.makeText(this, "Estoy rn onPause", Toast.LENGTH_SHORT).show();
}
@Override protected void onStop() {
  super.onStop(); Toast.makeText(this, "Estoy rn onStop", Toast.LENGTH_SHORT).show();
}
@Override protected void onRestart() {
  super.onRestart(); Toast.makeText(this, "Estoy rn onRestart", Toast.LENGTH_SHORT).show();
}
@Override protected void onDestroy() {
  super.onDestroy(); Toast.makeText(this, "Estoy rn onDestroy", Toast.LENGTH_SHORT).show();
}
```


### 33. Inflando menús de ajustes
<!-- agregar menu de navegacion, añadir menu de navegacion, poner menu de navegacion, hacer menu de navegacion, menu desplegable, menu de arriba-->
1. Agregamos la carpeta/archivo *res/menu/menu_main.xml*
```xml
<menu xmlns:android="http://schemas.android.com/apk/res/android"
  xmlns:app="http://schemas.android.com/apk/res-auto">
  <item android:id="@+id/menu_opt_add"
      android:icon="@drawable/baseline_add_black"
      android:title="@string/action_add"
      app:showAsAction="ifRoom">          <!-- para que nos muestre directo (sin desplegar)-->
  </item>
  <item
    android:id="@+id/menu_opt_settings"
    android:title="@string/settings"
    app:showAsAction="never">           <!-- para que se muestre solo desplegando-->
  </item>
</menu>
```

2. en mainActivity sobre escribimos para inflar nuestro menu
```java
@Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
}
```
3. le aplicamos comportamiento cuando precionamos sobre algun boton
```java
@Override public boolean onOptionsItemSelected(@NonNull MenuItem item) {
  if(item.getItemId() == R.id.menu_opt_settings) {
      ...
      return true;
  }
  return false;
}
```

**Error:** Al navegar al la nueva activity no ahi una superior.   
**Causa:**: En el manifest tiene el siguiente tema, **Solucion:** borrar la sig.    
```
android:theme="@style/AppTheme.NoActionBar"
```



### 76. Instanciando widgets y layouts por código
No requerimos de un xml para la vista podemos crear los Layouts y widgets desde 
codigo, **por ejemplo** podemos crear un Constrainlayout y dentro de este agregarle 
un boton (ambos deben de tener un identificador).




### 78. Los radio groups y los radio buttons
Interface
```xml
<RadioGroup
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:id="@+id/rg_sex" >
    <RadioButton
        android:id="@+id/rb_hombre"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Hombre" />
    <RadioButton
        android:id="@+id/rb_mujer"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Mujer" />
    <RadioButton
        android:id="@+id/rb_indefinido"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Indefinido" />
</RadioGroup>
```
Codigo
```java
RadioGroup rg = findViewById(R.id.rg_sex);
rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
    @Override public void onCheckedChanged(RadioGroup group, int checkedId) {
        Log.d("Main", "Seleccionado: " + checkedId); //es una referencia de memoria
        RadioButton rd = findViewById(checkedId);
        switch (rd.getId()) {
            case R.id.rb_hombre:      Log.d("Main", "Accion hombre"); break;
            case R.id.rb_mujer:       Log.d("Main", "Accion Mujer"); break;
            case R.id.rb_indefinido:  Log.d("Main", "Accion Indefinido"); break;
        }
    }
});
```
Resultado   
![11_radio_button.gif](./img/11_radio_button.gif)   



### 79. Los switchs y los checks
interface
```xml
<Switch
  android:id="@+id/sw_uno"
  android:layout_width="match_parent"
  android:layout_height="wrap_content"
  android:checked="true"
  android:text="Switch" />
```
codigo
```java
findViewById(R.id.sw_uno).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
    @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
      if(isChecked) { Log.d("Main", "Switch Activo"); } 
      else          { Log.d("Main", "Switch Desactivado"); }
    }
});
```
Resultado   
![12_Switch](./img/12_Switch.PNG)   



<br /><br />
### 80. Las web views y los selectores de fechas y horas
interface
```xml
<WebView
  android:id="@+id/mi_webview"
  android:layout_width="match_parent"
  android:layout_height="match_parent" />
```
codigo
```java
WebView w = findViewById(R.id.mi_webview);
w.loadUrl("https://ytmp3.cc/");
```
<!-- uso de permisos, añadir permisos, agregar permiso, pedir permiso, permiso internet, permiso de internet, permisos de internet, permiso para internet, permisos para internet-->
Debemos de pedir permisos para acceder a internet en el manifest
```xml
<uses-permission android:name="android.permission.INTERNET" />
```
Resultado   
![13_webView.PNG](./img/13_webView.PNG)   



<br /><br />
## 87. Día 8: Diálogos e inicio del primer proyecto
<!-- como mostrar un dialog, uso de dialogs-->
Los Dialogs nos permiten cosas como informar y darle interactividad al usuario 
como **por ejemplo** sesion caduca vuelvete a loguear.   

mainActivity
```java
Button b = findViewById(R.id.btn_show_dialog);
b.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Log.d("Main", "onClick - btn_show_dialog");
        MyFirstDialog m = new MyFirstDialog();
        m.show(getSupportFragmentManager(), "Hola que hace");
    }
});
```

MyFirstDialog
```java
public class MyFirstDialog extends androidx.fragment.app.DialogFragment {
    @androidx.annotation.NonNull @Override
    public Dialog onCreateDialog(@androidx.annotation.Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Elige una opcion de las siguientes ")
               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   @Override public void onClick(DialogInterface dialog, int which) {
                       Log.d("MyFirstDialog", "Aceptada");
                   }
               })
               .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                   @Override public void onClick(DialogInterface dialog, int which) {
                       Log.d("MyFirstDialog", "Cancelado");
                   }
               });
        return builder.create();
    }
}
```
Resultado   
![14_dialog_fragment](./img/14_dialog_fragment.gif)   













<br /><br /><br /><br /><br /><br />
## Sección 9: Día 8: Diálogos y constructores
Crearemos una aplicacion de TODO que guardara actividades, ideas, uso de 
sonidos, animaciones.   

<br /><br />
### 91. Las webs de recursos gratuitos de Android
Descargamos las imagenes de https://material.io/resources/icons en la opcion de 
android en mi caso quedo de la sig manera.   
Sus relaciones por carpeta son respectivamente: **27x27, 24x24, 72x72, 144x144, 192x192**.   
<pre>
+-drawable-hdpi
|--check_box_black.png, check_box_outline_blank_black.png, warning_black.png, wb_incandescent_black.png

+---drawable-mdpi
|--check_box_black.png, check_box_outline_blank_black.png, warning_black.png, wb_incandescent_black.png

+---drawable-xhdpi
|--check_box_black.png, check_box_outline_blank_black.png, warning_black.png, wb_incandescent_black.png

+---drawable-xxhdpi
|--check_box_black.png, check_box_outline_blank_black.png, warning_black.png, wb_incandescent_black.png

+---drawable-xxxhdpi
|--check_box_black.png, check_box_outline_blank_black.png, warning_black.png, wb_incandescent_black.png
</pre>


### 92. Los recursos de String en Android
![15_todoapp_strings.PNG](./img/15_todoapp_strings.PNG)   




### 94. El layout de creación de la nota
**layout/layout_new_note.xml**   
![16_todoapp_view_add_note](./img/16_todoapp_view_add_note.PNG)



### 95. El layout de consultar una nota
**layout/layout_new_note.xml**   
![16_todoapp_view_add_note](./img/16_todoapp_view_add_note.PNG)




### 96. La lógica de creación de la nota
1. Creamos metodo en en mainActivity para recibir la nota creada.   
```java
public class MainActivity extends AppCompatActivity {
+    public void createNewNote(Nota newNote) { }//luago lo hacemos
```

2. creamos el constructor de la vista y la vista con el layout de nueva nota.   
2.1. tomamos el formulario para poder tomar sus datos.   
2.2. creamos la nota con los datos del formulario.   
<!-- Tomar texto de textedit, tomar datos de formulario, crear dialog con formulario, saber si un checkBox esta seleccionado -->
```java
@NonNull @Override
public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
    //CREAMOS EL CONSTRUCTOR DE LA VISTA Y LA VISTA CON EL LAYOUT DE NUEVA NOTA
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = getActivity().getLayoutInflater();
    View dialogView = inflater.inflate(R.layout.layout_new_note, null);

    //TOMAMOS EL FORMULARIO PARA PODER TOMAR SUS DATOS
    final EditText editTitle = dialogView.findViewById(R.id.txtv_title);
    final EditText editDescription = dialogView.findViewById(R.id.txtv_descripcion);
    final CheckBox checkBoxIdea = dialogView.findViewById(R.id.chbx_idea);
    final CheckBox checkBoxTodo = dialogView.findViewById(R.id.chbx_todo);
    final CheckBox checkBoxImportant = dialogView.findViewById(R.id.chbx_important);

    //BOTON CREAR
    Button btnOk = dialogView.findViewById(R.id.btn_new_note_ok);
    btnOk.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
            //CREAMOS LA NOTA CON LOS DATOS DEL FORMULARIO
            Nota newNote = new Nota();
            newNote.setTitle(editTitle.getText().toString());
            newNote.setDescripcion(editDescription.getText().toString());
            newNote.setIdea(checkBoxIdea.isChecked());
            newNote.setNote(checkBoxTodo.isChecked());
            newNote.setImportant(checkBoxImportant.isChecked());

            //LE MANDAMOS LA NOTA A MAINACTIVITY PARA QUE LA MUESTRE
            MainActivity mainActivity = (MainActivity)getActivity();
            mainActivity.createNewNote(newNote);
            dismiss();//cerramos el dialogo
        }
    });
    //BOTON CANCELAR
    Button btnCancel = dialogView.findViewById(R.id.btn_new_note_cancel);
    btnCancel.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) { dismiss(); }
    });
    //PONEMOS TITULO AL DIALOG
    builder.setView(dialogView).setMessage("Añadir una nueva nota");
    return builder.create();
}
```



### 97. La lógica de consulta de la nota
Crearemos la clase que muestra la nota, nota que le va a llegar por medio de 
un metodo y este pondra los valores en en el Dialog.   
```java
public class DialogShowNote extends DialogFragment {

private Nota mNote;

//PARA ASIGNAR LA NOTA DESDE FUERA
public void showNote(Nota inNote) { this.mNote = inNote; }

@NonNull @Override
public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
//creamos el constructor de la vista
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = getActivity().getLayoutInflater();
    View dialogView = inflater.inflate(R.layout.layout_show_note, null);
    final TextView txtViewTitle = dialogView.findViewById(R.id.txtv_title);
    final TextView txtViewDescription = dialogView.findViewById(R.id.txtv_description);
//MOSTRAMOS LOS DATOS
    txtViewTitle.setText(this.mNote.getTitle());
    txtViewDescription.setText(this.mNote.getDescripcion());
//DECIDIMOS QUE IMAGEN SE MUESTRA Y CUAL SE OCULTA
    ImageView imgImportant = dialogView.findViewById(R.id.img_important);
    ImageView imgTodo = dialogView.findViewById(R.id.img_check);
    ImageView imgideea = dialogView.findViewById(R.id.img_idea);
    if(!this.mNote.isIdea()) { imgideea.setVisibility(View.GONE); }
    if(!this.mNote.isImportant()) { imgImportant.setVisibility(View.GONE); }
    if(!this.mNote.isNote()) { imgTodo.setVisibility(View.GONE); }
//boton
    Button btnok = dialogView.findViewById(R.id.btn_show_note_ok);
    btnok.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) { dismiss(); }
    });    
    builder.setView(dialogView).setMessage("Detalles nota");
    return builder.create();
}
```


### 98-99. Abriendo y consultando la nota  Creando y consultando nuestra nota
<!-- mostrar boton en la barra superior-->

Uniremos agregar y mostrar nota en el main

1. Creamos un directorio en res llamado menu y dentro un Resource menu, en el cual 
le agregamos la opcion de agregar nota
```xml
<menu xmlns:android="http://schemas.android.com/apk/res/android"
  xmlns:app="http://schemas.android.com/apk/res-auto">
  <item android:id="@+id/menu_opt_add"
      android:icon="@drawable/baseline_add_black"
      android:title="@string/action_add"
      app:showAsAction="ifRoom">          <!-- para que nos muestre directo (sin desplegar)-->
  </item>
</menu>
```

2. Inflamos el menu en el main y como accion que muestre el dialog de mostrar
```java
//PARA QUE SE VEA EL MENU
@Override public boolean onCreateOptionsMenu(Menu menu) {
  getMenuInflater().inflate(R.menu.menu_name, menu);
  return super.onCreateOptionsMenu(menu);
}
//LE AGREGAMOS ACCION AL MENU (MOSTRAR EL FROM DE AGRGAR)
@Override public boolean onOptionsItemSelected(@NonNull MenuItem item) {
  if(item.getItemId() == R.id.menu_opt_add) {
      DialogNewNote dialogNewNote = new DialogNewNote();
      dialogNewNote.show(getSupportFragmentManager(), "add_note");
  }
  return false;
}
```

3. Agregamos un boton para mostrar el dialog con la nota creada.   
```java
@Override protected void onCreate(Bundle savedInstanceState) {
  ...
  Button btn = findViewById(R.id.button);
  btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
          DialogShowNote d = new DialogShowNote();
          d.showNote(mTempNote);
          d.show(getSupportFragmentManager(), "show_note");
}});}
```
**Resultado**.   
![18_appNotas_agregar_ver_uno](./img/18_appNotas_agregar_ver_uno.gif)   






















<br /><br /><br /><br /><br /><br />
## Sección 10: Día 9: Arrays y colecciones de objetos
Usaremos loa array para no mostrar una nota si no varias notas, la cuales 
seran mostradas por medio de un Adapter,     

### Ejercicio 9: Arrays multidimensionales y preguntas aleatorias
Primero crearemos una app de repaso de las capitales para... ensayar ¯\_(ツ)_/¯

1. **pantalla**: En este caso solo es un boton y un boton de siguiente.

2. Solo creamos una matriz donde una columna corresponde al estado y otra 
a la capital, y un metodo que tome estas de forma aleatoria.   
```java
public class MainActivity extends AppCompatActivity {
  private Random random = new Random();
  private String[][] paisCapital;
  private final byte state = 0;
  private final byte capital = 1;
  private byte custionNumber;
  private TextView txtPregunta;
  @Override protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      Toolbar toolbar = findViewById(R.id.toolbar);
      setSupportActionBar(toolbar);
      txtPregunta = findViewById(R.id.txt_question);
      paisCapital = new String[5][2];
      paisCapital[0][state] = "Baja California";
      paisCapital[0][capital] = "Mexicali";
      ...
  }

  public void nextQuestion(View v) {
      custionNumber = (byte)random.nextInt(paisCapital.length);
      String stateName = paisCapital[custionNumber][state];
      String capitalName = paisCapital[custionNumber][capital];
      String question = String.format("La capital de %s es %s.", stateName, capitalName);
      txtPregunta.setText(question);
  }
```

**Resultado:**
![19_repaso_estado_capital](./img/19_repaso_estado_capital.gif)   


### 108. El layout de la lista de tareas
1. En la vista del main agregamos una lista

2. Agregamos un nuevo layout el cual servira para pintar un item de la lista
```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android" xmlns:app="http://schemas.android.com/apk/res-auto" android:layout_width="match_parent" android:layout_height="wrap_content" android:orientation="vertical">
    <LinearLayout android:layout_width="match_parent" android:layout_height="wrap_content" android:orientation="horizontal">
        <ImageView android:id="@+id/img_important" android:layout_width="wrap_content" android:layout_height="wrap_content" android:layout_weight="1" android:visibility="visible" app:srcCompat="@drawable/baseline_warning_black" />
        <ImageView android:id="@+id/img_idea" android:layout_width="wrap_content" android:layout_height="wrap_content" android:layout_weight="1" android:visibility="visible" app:srcCompat="@drawable/baseline_wb_incandescent_black" />
        <ImageView android:id="@+id/img_nota" android:layout_width="wrap_content" android:layout_height="wrap_content" android:layout_weight="1" app:srcCompat="@drawable/baseline_check_box_outline_blank_black" />
        <Space android:layout_width="wrap_content" android:layout_height="wrap_content" android:layout_weight="3" />
    </LinearLayout>
    <LinearLayout android:layout_width="match_parent" android:layout_height="wrap_content" android:orientation="vertical">
        <TextView android:id="@+id/txt_title" android:layout_width="match_parent" android:layout_height="wrap_content" android:text="@string/title_hint" android:textAlignment="center" android:textSize="18sp" android:textStyle="bold" />
        <TextView android:id="@+id/txt_description" android:layout_width="match_parent" android:layout_height="wrap_content" android:maxLines="2" android:text="@string/description_hint" />
    </LinearLayout>
</LinearLayout>
```
![20_layout_item_lista.PNG](./img/20_layout_item_lista.PNG)


### 109. El adapter de nuestra lista de tareas
1. Creamos una clase que herede de BaseAdapter y le implementamos sus metodos.   
  1.1. Creamos coleccion de los datos a mostrar.   
  1.2. En el metodo ```getView()``` creamos la vista a partir del layout que agregamos apenas.   
  1.3. Agregamos ```addNote``` para meter un item y notificamos los cambios.   


2. Agregamos el Adapter a nuestra Activity.   
  2.1. Usamo el metodo ```addNote``` de nuestro adapter en el ```createNewNote``` de nuestro activity
  2.2. Le asignamos a nuestro list el adaptador que creamos.   
  2.3. Agregamos un click listener a cada item de la lista 

```java
public class MainActivity extends AppCompatActivity {
+   private NoteAdapter mNoteAdapter;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mNoteAdapter = new NoteAdapter();
        ListView listNotes = findViewById(R.id.list_todo);
        listNotes.setAdapter(this.mNoteAdapter);            //asigansmo el adapter a la lista
        //Agregamos un click listener a cada item de la lista
+       listNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int posPulsada, long id) {
                //mostramos detalle de la nota
+               Nota notaTemp = (Nota)mNoteAdapter.getItem(posPulsada);
                DialogShowNote dialogShowNote = new DialogShowNote();
+               dialogShowNote.showNote(notaTemp);
            }
        });
    }

    public void createNewNote(Nota newNote) {               //Agregamos la nueva nota a la lista
+       mNoteAdapter.addNote(newNote);
    }
    @Override public boolean onCreateOptionsMenu(Menu menu) { ...  }
    @Override public boolean onOptionsItemSelected(@NonNull MenuItem item) {...}
    
//----------------
    public class NoteAdapter extends BaseAdapter {
        List<Nota> notes = new ArrayList<Nota>();
        @Override public int getCount() { return notes.size(); }
        @Override public Object getItem(int position) { return notes.get(position); }
        @Override public long getItemId(int position) { return position; }

+       @Override public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                //la lista aun no ha sido inflada a si que lo primero es inflarla a partir del layout (.xml)
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
+               convertView = inflater.inflate(R.layout.note_item_list, parent, false);
            }
            //ya tenemos la vista creada ahora la seteamos
            TextView txtTitle = convertView.findViewById(R.id.txt_title);
            TextView txtDescription = convertView.findViewById(R.id.txt_description);
            ImageView imgImportant = convertView.findViewById(R.id.img_important);
            ImageView imgIdea = convertView.findViewById(R.id.img_idea);
            ImageView imgNote = convertView.findViewById(R.id.img_nota);
            Nota notaActual = notes.get(position);
            txtTitle.setText(notaActual.getTitle());
            txtDescription.setText(notaActual.getDescripcion());
            if(!notaActual.isIdea()) { imgIdea.setVisibility(View.GONE); }
            if(!notaActual.isImportant()) { imgImportant.setVisibility(View.GONE); }
            if(!notaActual.isNote()) { imgNote.setVisibility(View.GONE); }

            return convertView;
        }
        public void addNote(Nota notaIn) {
            notes.add(notaIn);
+           notifyDataSetChanged();  //para que pinte los cambios cuando agregemos una nota
        }
    }
}
```

**Resultado**.   
![21_lista_de_notas](./img/21_lista_de_notas.gif)























<br /><br /><br /><br /><br /><br />
## Sección 11: Día 10: La persistencia de datos en las apps de Android 
Tenemos 2 tipos de almacenamiento en android con la persistencia, lo que son los 
datos clave valor, por ejemplo para las opciones como ideoma:español, y el otro es 
el que guarda objetos complejos por ejemplo una nota con su titulo, descripcion y etc.



<!-- pasar entre pantallas, navegar entre pantallas, navegacion entre pantallas, 
pasar entre actividades, navegar entre actividades, navegacion entre actividades, 
pasar informacion entre pantallas, enviar informacion entre pantallas
pasar informacion entre actividades, enviar informacion entre actividades
abrir intent, abrir activity, usar intent, cambiar de activity
-->
### 113. Los intents y la navegación entre actividade
Los intents pide dos datos, la pantalla actual y la pantalla a la que se va.   
pantalla actual.   
```java
Intent intent = new Intent(this, SettingsActivity.class);
intent.putExtra("username", "Mario");   //igual le estamos pasando info
intent.putExtra("age", "26");
startActivity(intent);
```
La otra pantalla
```java
Intent recoveryIntent = new Intent();
String username = recoveryIntent.getExtras().getString("username");
String age = recoveryIntent.getExtras().getString("age");
```
**Nota**: No se puede enviar datos complejos.   





### 114. Diseñando el layout de ajustes
1. Cremoas un nuevo activity Settings y en el manifest cambiamos el titulo de la activity
con **android:label**.

2. En el menu.xml agregamos el boton para dirigir a la pantalla.   
```xml
<item android:id="@+id/menu_opt_add" android:icon="@drawable/baseline_add_black" 
android:title="@string/action_add" app:showAsAction="ifRoom"> </item>
<item
    android:id="@+id/menu_opt_settings"
    android:title="@string/settings"
    app:showAsAction="never">
</item>
```

3. Programamos el cambio de pantalla
```java
if(item.getItemId() == R.id.menu_opt_settings) {
  Intent intent = new Intent(this, SettingsActivity.class);
  startActivity(intent);
  return true;
}
```

4. **Resultado** Como podemos ver nevegamos entre pantallas **FALTA** que 
guarde las opciones que escojimos.    
![22_navegacion_entre_pantallas](./img/22_navegacion_entre_pantallas.gif)




### 117. Persistir el entero de modo de animación
Solo tenemos que guardar dos cosas, 1 si quiere o no sonido, 2 la velocidad de 
las animaciones.   

1. Creamos el cambio del checkbox de sonido y de animacion (Leer los comentarios)
```java
public class SettingsActivity extends AppCompatActivity {
//VARIABLES PARA LECTURA Y ESCRITURA DEL 
private SharedPreferences mPrefs;
private SharedPreferences.Editor mPrefsEditor;

//NOMBRE DE LAS PREFERENCIAS
public String preferenceSoundName = "sound";
public String preferenceAnimationName = "speed_animation";

//VALORES DE LAS PREFERENCIAS
private boolean mSound;
public static final byte FAST = 2, SLOW = 1, NONE = 0;
public static byte mAnimationPrefOptionSelected;

@Override protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  setContentView(R.layout.activity_settings);
  
  mPrefs = getSharedPreferences("todo_app_settings", MODE_PRIVATE);  //TOMAMOS O CREAMOS EL ARCHIVO DE PREFERENCIAS
  mPrefsEditor = mPrefs.edit();

//CAMBIO Y GUARDADO DE PREFERENCIA DE SONIDO
  this.mSound = mPrefs.getBoolean(preferenceSoundName, true);
  final CheckBox chBoxSound = findViewById(R.id.chb_sound);
  chBoxSound.setChecked(mSound);
  chBoxSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
    @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
      chBoxSound.setChecked(!mSound);
      mPrefsEditor.putBoolean(preferenceSoundName, !mSound);
      //mPrefsEditor.commit(); // lo pasamos al onPause para que sea mas eficiente
    }
  });

//CAMBIO Y GUARDADO DE PREFERENCIA DE VELOCIDAD DE ANIMACION
  this.mAnimationPrefOptionSelected = (byte)mPrefs.getInt(preferenceAnimationName, FAST);
  final RadioGroup rdGroup = findViewById(R.id.rb_group_animation);
  rdGroup.clearCheck(); //deseleccionamos todos
  switch (mAnimationPrefOptionSelected) {
    case(FAST): rdGroup.check(R.id.rb_sound_fast); break;
    case(SLOW): rdGroup.check(R.id.rb_sound_slow); break;
    case(NONE): rdGroup.check(R.id.rb_sound_none); break;
  }
  rdGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
    @Override public void onCheckedChanged(RadioGroup group, int checkedId) {
      RadioButton rbSelected = rdGroup.findViewById(checkedId); //RECUPERAMOS EL RADIO NUTON SELECCIONADO
      if(null != rbSelected) {
        switch (rbSelected.getId()) {
          case(R.id.rb_sound_fast): mAnimationPrefOptionSelected = FAST;  break;
          case(R.id.rb_sound_slow):  mAnimationPrefOptionSelected = SLOW;  break;
          case(R.id.rb_sound_none): mAnimationPrefOptionSelected = NONE;  break;
        }
      }
      mPrefsEditor.putInt(preferenceAnimationName, mAnimationPrefOptionSelected); //GUARDAMOS EL CAMBIO POR EL USUARIO DE PREFE DE ANIMACION 
      //LO PASAMOS AL ONPAUSE PARA QUE SEA MAS EFICIENTE mPrefsEditor.commit(); 
    }
});
}
@Override protected void onPause() { super.onPause(); mPrefsEditor.commit(); }
}
```


3. Recuperamos los cambios en el MainActivity, llamando a SharedPreferences en el 
onResume 
```java
@Override protected void onResume() {
  super.onResume();
  mPrefs = getSharedPreferences(SettingsActivity.nameSettingsSaved, MODE_PRIVATE);
  Log.i("Main", String.format("Sonido: %s - Velocidad Animacion: %s",
          mPrefs.getBoolean(SettingsActivity.preferenceSoundName, true),
          mPrefs.getInt(SettingsActivity.preferenceAnimationName, SettingsActivity.FAST)));
}
```


**Resultado:** Podemos ver que las preferencias se guardan si cambiamos de 
activity, y aun cuando salimos de la app.  
![23_guardado_de_datos_sharedpreferences_settings](./img/23_guardado_de_datos_sharedpreferences_settings.gif)   






<br /><br />
### 118. Errores y excepciones en Java
El shared Preferences se queda corte para guardar las notas que creo el usuario, para lo cual 
guardaremos esta informacion en formato json en un archivo. El lo seguimos el sig diagrama para hacerlo.   
![24_proceso_para_guardar_objeto_java_a_archivo_json](./img/24_proceso_para_guardar_objeto_java_a_archivo_json.PNG)   
Si nos fijamos para guardar pasamos de Objetos Notas Java a String JSON, este String lo 
guardamos en un fichero que es lo mismo que guardarlo en binario en un archivo. Y para 
cargarlo es el mismo proceso pero al revez.   

1. Al modelo de nota le agregamos el nombre de las propiedades del json.   
1.1. Agregamos un constructor de nota que construlla una instancia apartir de un JSONObject

```java
public class Nota {
    public Nota() { }
    private static final String JSON_TITLE = "title", JSON_DESCRIPTION = "description", JSON_IDEA = "idea", JSON_TODO = "todo", JSON_IMPORTANT = "important";
    
    public Nota(JSONObject jo) throws JSONException {
        mTitle = jo.getString(JSON_TITLE);
        mDescripcion = jo.getString(JSON_DESCRIPTION);
        mIdea = jo.getBoolean(JSON_IDEA);
        mImportant = jo.getBoolean(JSON_IMPORTANT);
        mNote = jo.getBoolean(JSON_TODO );
    }

    public JSONObject convertNoteToJSON() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(JSON_TITLE, mTitle);
        jo.put(JSON_DESCRIPTION, mDescripcion);
        jo.put(JSON_IDEA, mIdea);
        jo.put(JSON_IMPORTANT, mImportant);
        jo.put(JSON_TODO, mNote);
        return jo;
    }
```


### 120. El serializador y deserializador de JSON
1. Crearemos una clase que se encargara de estas acciones para poder guardar y leer un json.   
1.1. Creamos el metodo ```save``` para guardar a partir de una lista de notas.    
1.2. Creamos el metodo ```load``` para cargar el archivo json y pasarlo a una lista de notas.    
```java
public class JSONSerializer {
  private final String FILE_NAME;
  private final Context mContext; //PARA SABER DONDE SE GUARDARA EL FILE JSON

  public JSONSerializer(String InFILE_NAME, Context InmContext) {
    FILE_NAME = InFILE_NAME;
    mContext = InmContext;
  }

+ public void save(List<Nota> notas) throws IOException, JSONException {
    JSONArray jsonArray = new JSONArray();
    for(Nota n: notas){ jsonArray.put(n.convertNoteToJSON()); }
    Writer writer = null;
    try {
      OutputStream out = mContext.openFileOutput(FILE_NAME, mContext.MODE_PRIVATE);//ABRIMOS EL ARCHIVO
      writer = new OutputStreamWriter(out);                   //CREAMOS EL ESCRITOR DEL ARCHIVO
      writer.write(jsonArray.toString());                     //PASAMOS A STRING EL ARRAY Y LO ESCRIBIMOS EN DISCO
    } finally {
      if (null != writer) { writer.close(); }  //CERRAMOS EL ESCRITOR
    }
  }
  
+ public ArrayList<Nota> load() throws IOException, JSONException  {
    ArrayList<Nota> notas = new ArrayList<Nota>();
    BufferedReader bufferedReader = null;
    try {
        InputStream in = mContext.openFileInput(FILE_NAME);
        bufferedReader = new BufferedReader(new InputStreamReader(in));

        //OBTENEMOS DEL FILE ALL THE STRING CONTAINED IN THERE
        StringBuilder jsonString = new StringBuilder();
        String currentLine = null;
        while ((currentLine=bufferedReader.readLine()) != null) { jsonString.append(currentLine); }

        //DEL STRING LO PASAMOS A OBJETO JSON Y LO PASAMOS A UNA LISTA JAVA DE NOTAS
        JSONArray jsonArray = (JSONArray)new JSONTokener(jsonString.toString()).nextValue();
        for (int i=0; i<jsonArray.length(); i++) { notas.add(new Nota(jsonArray.getJSONObject(i))); }
    } catch(FileNotFoundException e) {
        //LA PRIMERA VEZ NO VA A HAVER FIVHERO LO POEMOS IGNORAR YA QUE SIGNIFICA QUE NO SE A CREADO NINGUNA NOTA
    } finally {
        if (bufferedReader != null) { bufferedReader.close(); }
    }
    return notas;
  }
}
```
ya tenemos creada la herramienta ahora nos falta usarla.


### 121. Guardar y cargar notas con el serializador
En donde necesitamos la carga es en el adaptar, por lo que llamamos 
al Serializador aqui.   
1. Desde el constructor hacemos la carga.   
2. Creamos un medodo de guardado para llamarlo el onPause de MainActivity.   

```java
public class NoteAdapter extends BaseAdapter {
  private JSONSerializer mJsonSerializer;
  public NoteAdapter() {
      mJsonSerializer = new JSONSerializer("tareas_app.json", MainActivity.this.getApplicationContext());
+     try { notes = mJsonSerializer.load();  }
      catch (Exception e) { e.printStackTrace(); }
  }
  
  public void saveNotas() {   //LO LLAMAMOS DESDE EL ONPAUSE DE MAIN ACTIVITY
    try { mJsonSerializer.save(notes); } 
    catch (Exception e) { e.printStackTrace(); }
  }
```
**Resultado:** persistencia de datos en archivo json apartir de objetos java.   
![25_persistencia_de_datos_en_archivo_json_apartir_de_objetos_java](./img/25_persistencia_de_datos_en_archivo_json_apartir_de_objetos_java.gif)      























<br /><br /><br /><br /><br /><br />
## 123. Las animaciones de objetos de UI
Las animaciones se guardan en xml en la carpeta res/anim las cuales 

1. Ejemplos de animaciones. en el caso de la de habajo no se apreciara la 
animacion ya que hara todas de golpe   
```xml
<set xmlns:android="http://schemas.android.com/apk/res/android">
    <!-- TRANSPARENCIA FADE IN-->
    <alpha android:fromAlpha="0.0" android:toAlpha="1.0" />
    <!-- TRANSPARENCIA FADE OUT-->
    <alpha android:fromAlpha="1.0" android:toAlpha="0.0" />

    <!-- TRANSACCIONES DEL INICIO AL FIN DE LA PANTALLA-->
    <translate android:fromXDelta="-100%" android:toXDelta="100%" />
    <!-- TRANSACCIONES DEL FIN AL INICIO DE LA PANTALLA-->
    <translate android:fromXDelta="100%" android:toXDelta="-100%" />

    <!-- ESCALADO APLASTAR HORIZANTAMENTE-->
    <scale android:fromXScale="1.0" android:toXScale="0.0" />
    <!-- ESCALADO AGRANDAR-->
    <scale android:fromXScale="1.0" android:toXScale="2.0" />

    <!-- ROTACION 3/4 DE VUELTA EL PIVOTE ESTA EN EL CENTRO 0.5 0.5-->
    <rotate android:fromDegrees="360" android:toDegrees="90"
        android:duration="500" android:startOffset="333" />
</set>
```
Para controlar los tiempos de la animacion a cada uno le podemos poner:   
- ```android:duration="500"``` la animacion durara 1/2 segundo.   
- ```android:startOffset="500"``` la animacion tardara 1 segundo en comenzar.   
- ```android:repeatMode="reverse"``` Se repetira la animacion haciendo lo contrario y lo contrario.   
- ```android:repeatCount="5"``` Veces que se reperitra la animacion, si queremos infinito "infinite".   



### 125. Creando las animaciones en Java
Para implementar animaciones solo traemos la animacion, o aplicar la animacion sobre 
algun view lo hecemos de la siguiente manera.   
```java
TextView txtvTitle = findViewById(R.id.txtv_title);
Animation ani = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.hello_anim);
txtvTitle.startAnimation(ani);
```
Para saber el estado de las animaciones lo hacemos con una interface sobre una clase.
```java
public class MainActivity extends AppCompatActivity implements Animation.AnimationListener  {...
  @Override public void onAnimationStart(Animation animation) {...}
  @Override public void onAnimationEnd(Animation animation) {...}
  @Override public void onAnimationRepeat(Animation animation) {...}
```
implementado debemos de asignar el listener a la animacion
```java
ani.setAnimationListener(MiImpl_AnimationListener);
```
**Los listener nos pueden servir** para cosas como no iniciar la aplicacion 
hasta que termine la animacion, del logo, reproducir una animacion mientras 
se escucha musica, etc.



### 126. Ejercicio 10: El layout de animaciones / 129. El uso de la seekbar
Objetivo: Vamos a crear 12 animaciones y 12 botones y una imagen para 
aplicar esta animaciones. 

Interface: [interface xml](./img/26_UI_animaciones.xml).   
Codigo: [java](./img/27_animaciones.java).   

**Resultado:**   
![java](./img/28_animaciones.gif).   



### 130. Animaciones en la app de notas
**Objetivo:** Vamos a crear una animacion de flash a las notas importantes
en nuestra app de notas.  

1. Creamos el efecto de fadeIn y de flash.   

2. Cargamos las animaciones en la parte de onResume del MainActivity por si 
el usuario cambia la velocidad de las animaciones, al volver se cargen estas.   
  2.1. Igual debemos de notificar al NoteAdapter que la velocidad de animacion a cambiado.   
```java
@Override protected void onResume() { ...
  this.mAnimFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
  this.mAnimFlash = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.flash_notas);
  if(mPrefSpeedAnimation == SettingsActivity.FAST     ) { mAnimFadeIn.setDuration(100); } 
  else if(mPrefSpeedAnimation == SettingsActivity.SLOW) { mAnimFadeIn.setDuration(1000); }
+ mNoteAdapter.notifyDataSetChanged();
}
```

3. En el getView del NoteAdapter aplicacamos las animaciones
```java
= Nota notaActual = notes.get(position);
+ if(notaActual.isImportant() && mPrefSpeedAnimation != SettingsActivity.NONE ) {
+     convertView.setAnimation(mAnimFlash);
+ } else {
+     convertView.setAnimation(mAnimFadeIn);
+ }
```

**Resultado:**   
![java](./img/29_animacion_nota_flash.gif).   
























<br /><br /><br /><br /><br /><br />
## Sección 13: Día 12: Sonidos en las aplicaciones y buenas prácticas
Veremos temas de como distinguir entre 2 diferentes versiones de android, donde 
en una funcionalidad funcionara en una API mas reciente pero en la mas antigua no, 
y para esto nos ayudara distinguir entre APIs para poder hacer que funcione an ambos.   
**Por ejemplo** La API de sonido cambio mucho cuando salio Loli Pop el cual deberemos 
de tener una diferente implementacion para versiones anteriores y posteriores.   


### 133. 134. El objeto Sound Pool para cargar efectos de sonido
**Teoria**: Creamos el SoundPool el cual aqui agregaremos desde la carpeta de assets una pila 
de audios los cuales podremos reproducir señalando su id. Este SoundPool se crea de dos diferentes 
formas por lo que deberemos de diferenciar en que api estamos para crearlo de forma diferente.   

1. Creamos la [interface.xml](./img/33_interface_reproducir_audios.xml)


2. Para diferencias sobre que api esta nuestra aplicacion y para crear el SoundPool
lo hacemos de la siguietne manera, donde la mas reciente usa un Fabric, y el otro un constructor.   
```java
if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
  //CODIGO PARA LOLI POP (22) EN ADELANTE
  AudioAttributes attr = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION).setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();
  soundPool = new SoundPool.Builder().setMaxStreams(3) // cuantas reproduccione simultaneas permitimos.setAudioAttributes(attr).build();
}  else {
  //CODIGO PARA LOLI POP HACIA ATRAS
  soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
}
```

3. Para cargar los [audios](./img/32_mis_sonidos.zip)
```java
try{
  AssetManager mng = this.getAssets(); //cargamos la carpeta de los assets
  AssetFileDescriptor descriptor = mng.openFd("sound_1.ogg");
  idFX1 = soundPool.load(descriptor, 0);
  descriptor = mng.openFd("sound_2.ogg"); idFX2 = soundPool.load(descriptor, 0);
} catch (IOException e) {
  e.printStackTrace();
}
```

4. Para repoducir y parar el audio lo hacemos con
```java
int volumenIzq=1, volumenDer=1, prioridad=0, velocidadSonido=1;
nowPlaying = soundPool.play(idFX1, volumenIzq, volumenDer, prioridad, mRepeats, velocidadSonido);
soundPool.stop(nowPlaying);
```

5. [Java Completo](./img/34_reproducir_audios.java)

**Resultado:** Al pulsar un boton este suena, con el volumen que queramos
(a partir del que tenga el usuario), y se repetira las veces que indiquemos.   
![35_reproducir_audios](./img/35_reproducir_audios.png)



### 139. Eliminar una nota con un click largo
1. En el **MainActivity** creamos un metodo para eliminarlo de la coleccion.  
```java
public class NoteAdapter extends BaseAdapter {
  List<Nota> notes = new ArrayList<Nota>();
  ...
  public void removeNote(int n) {
    notes.remove(n);
    notifyDataSetChanged();
  }
```

2. Relacionamos el click largo con la eliminacion
```java
listNotes.setLongClickable(true);
listNotes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        mNoteAdapter.removeNote(position);
        return true;
    }
});
```

**Resultado:** Con un click prolongado o una pulsacion larga podemos aliminar 
la nota: **Falta** Crear un dialogo de ¿seguro que quieres eliminar?.   
![36_click_largo_eliminar_nota](./img/36_click_largo_eliminar_nota.gif)




### 140. Acerca de la clase Media Player para crear un reproductor
Si quremos reproducir mp3 en audio no es con SoundPool este se realiza con 
MediaPlayer que es mas adecuado para reproducciones largas, lo que ahi que 
tenener **cuidado** es que este es bastante pesado.
```java
MediaPlayer mediaPlayer = new MediaPlayer();
try {
    mediaPlayer.prepare();
    mediaPlayer.setDataSource(this.getAssets().openFd("mi.mp3"));
} catch (IOException e) {
    e.printStackTrace();
}
mediaPlayer.start();
```


### 141. Buenas prácticas: Modelo - Vista - Controlador
link de documentacion de android, url documentacion.    
- https://developer.android.com/guide#essential-documentation   

- https://developer.android.com/design




### 143. La clase Display para obtener orientación y resolución 144. Los layouts para diferentes resoluciones en Android
<!-- - detectar en que dispositivo esta corriendo la app, saber en que dispositivo  esta corriendo nuestro app,
- detectar en que resolucion esta corriendo la app, saber en que resolucion  esta corriendo nuestro app,
- detectar en que orientacion esta corriendo la app, saber en que orientacion  esta corriendo nuestro app,
-->

<!-- 
- uso de las distintas pantallas, tipos de pantallas, variedad de pantallas, tamaños de pantallas, 
- uso de las distintas resoluciones, tipos de resoluciones, variedad de resoluciones, tamaños de resoluciones, 
-->
```java
private void detectDevice() {
    Display display = getWindowManager().getDefaultDisplay();
    txtorientation.setText(display.getRotation()+"");
    Point xy = new Point();
    display.getSize(xy);
    txtResolution.setText(String.format("X: %s Y: %s", xy.x+"", xy.y+""));
}
```



### 147. El concepto de fragmento
Son iguales que los Activities pero tenemos diferentes ciclos de vida como:
- **onAttach()** : Es para referenciarlo a un Activity ya que estos necesitan a fuerza de 
un activity.  
- **onDetach()** : Se lo llama cuando se desasocia el fragmento de la actividad.    

**FragmentManager** Este nos ayudara a gestionar los fragemntos en una activiti como 
inicialisar, agregar, finalizar.


### 148. La lógica de un fragment
1. En nuestro activity a nuestro layout principal le damos un ID ya que heremos referencia este.  

2. Cremos un xml layout para la vista de nuestro fragmento.   

3. Creamos una clase de nuestro fragmento
  3.1. ```savedInstanceState``` esta variable dentro sirve para que llegue info 
  del Activity en el uqe esta contenido, ya que en el Fragmento **no deberiamos** 
  de trear datos ni nada solo mostrarlos e interactuar con el usr.   

```java
public class MyFragment extends androidx.fragment.app.Fragment {
    private String mAlgunString; private Button btnMybutton;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAlgunString = "Texto dentro de nun fragmento";
    }
    
    @Override @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_fragment_layout, container, false);
        btnMybutton = v.findViewById(R.id.btn_dentro_fragmento);
        btnMybutton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) { Toast.makeText(getActivity(), "hola desde Fragment", Toast.LENGTH_SHORT).show(); }
        });
        return v;
    }
}
```


<!-- Como crear fragmentos simple, uso de fragmentos simple, como usar fragmentos simple, uso fragmentos simple-->
4. INSTANCIAR EL FRAGMENT, ya lo creamos ahora debemos de llamarlo desde algun (el que sea) Activity
```java
public class FragmentosActivity extends AppCompatActivity {
  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_fragmentos);
    
    FragmentManager fm = getSupportFragmentManager();
    Fragment myFragment = fm.findFragmentById(R.id.fragment_holder);
    if(myFragment == null) {
        FragmentTransaction transaction = fm.beginTransaction();
        myFragment = new MyFragment();
        transaction.replace(R.id.fragment_holder, myFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
  }
}
```
**IMPORTANTE:** El proceso para crear el fragmento siempre es el mismo, 
1) Inflar fragmento, 2) Recuperar los elementos de su layout, 3) Configurar/Settear elementos visuales.   

**Resultado:** nuestro Activity en realidad no hace nada mas que mostrar el activity 
y el fragment ase todo lo demas como tomar su layout.xml y definir el comportamiento de 
sus componentes en este caso un boton que saca un mensaje Toast.   



### 149. Los síngletons y su uso
¿Que es un síngleton?...

### 150. Programando un síngleton en Java
¿Como crear un singleton?...


### 151. Interfaces y su papel en el desarrollo en Java
¿Como usar las interfaces en fragments?...

En este caso usamos las interfaces para comunicarnos entre fragmentos 

```java
public interface IActivityAlgo {
  void hasAlgo();
}

public class MainActivity extends Activity implements IActivityAlgo {
  ...
  @Override void hasAlgo() {
    //codigo que se ejecutara desde afuera
  }
}

public class someFragment extends Fragment {
  private IActivityAlgo mActivityAlgo;
  
  //por aqui nos llegara el fragment con el que nos querramos comunicar
  @Override public void onAttach(Context context) {
    this.mActivityAlgo = (IActivityAlgo)context;
  }
  
  private void algunMetodo() {
    mActivityAlgo.hasAlgo();
  }
}
```









<!-- inicio app de contactos -->
<br /><br /><br />
### 152. Cómo crear un alias de un fichero en Android
Vamos a hacer una aplicacion de contactos donde tendremos dos vistas diferentes 
donde cuando este en forma **vertical** al hacer click en un contacto nos mostrara 
los detalles en otra pantalla, mientras que si estamos en forma **horizontal** 
en la parte izq. mostraremos la lista de contatactos misnetras en la der. mostraremos 
el detalle de los contactos.   

1. En la parte de los archivos de nuestro proyecto dentro de Android Studio pasamos a 
la vista de "Project"

2. En values creamos un nuevo archivo de recursos "refs.xml" y hacemos referencia al 
layout de main
```xml
<resources>
    <item name="dual_main_activity" type="layout">
        @layout/activity_main
    </item>
</resources>
```

3. Creamos un layout para la version horizontal de nuestro mainActivity.   
```
la misma vista pero uno dice horizontal y otro vertical
```

4. Creamos una carpeta values para la version en horizontal.   
4.1. Sobre la carpeta de res->new resource directory-> name: "values-land".   
4.2. dentro de la carpeta nueva creamos "refs.xml" y hacemos la referencia a la vercion 
land del mainActivity.   
```xml
<resources>
    <item name="dual_main_activity" type="layout">
        @layout/activity_main_land
    </item>
</resources>
```

5. Cambiamos en el MainActivity.java el activity_main por **dual_main_activity**.   
```java
public class MainActivity extends AppCompatActivity {
    @Override protected void onCreate(Bundle savedInstanceState) {
+       setContentView(R.layout.dual_main_activity);
```

- **Nota:** Si nos fijamos tanto la vercion potrait como la land tienen el mismo alias, 
lo que nos permite cargar dos archivos diferentes segun el caso con el mismo codigo.   

- **Resultado**   
![37_alias_pa_cargar_un_layout_u_otro_segun_verti_u_hori](./img/37_alias_pa_cargar_un_layout_u_otro_segun_verti_u_hori.gif)   




### 153. El modelo de datos de los contactos
1. Creamos modelo de contactos 
```java
public class Contact implements Serializable {
    private String mName;
    private String mAlias;
    private String mNumber;
    private String mEmail;
    public Contact(String mName, String mAlias, String mNumber, String mEmail) {...
```

<!-- crear singleton, ejemplo singleton -->
2. Creamos singleton, 
```java
public class ContactManagger {
  //CREAMOS REFERENCIA INSTANCIA UNICA
  private static ContactManagger contactList;
  private ArrayList<Contact> contacts;//LO QUE GESTIONARA
  //OBTENEMOS UNICA INSTANCIA
  public static ContactManagger getInstance() {
      if(contactList == null) { return new ContactManagger(); } 
      else { return contactList; }
  }
  //CONSTRUCTOR PRIVADO
  private ContactManagger() {
      contacts = new ArrayList<Contact>();
      createData();
  }

  public ArrayList<Contact> getContacts() { return contacts; }

  private void createData() {
    Contact c1 = new Contact("mario", "Tatemas", "2412345467", "mario@gmail.com");
    ... contacts.add(c1); contacts.add(c2); contacts.add(c3); contacts.add(c4);
  }
}
```


### 155. Diseñando las dos vistas de la app
1. En *activity_main.xml* Creamos un FrameLayout (que nos sirven para estos escenarios de
para ser rellenados) que sera solamente la lista de contactos.    
```xml
<androidx.constraintlayout.widget.ConstraintLayout
    android:id="@+id/main_layout"
    ...>
    <FrameLayout
+       android:id="@+id/fragment_holder_contacts_list"
        android:layout_width="match_parent"
        android:layout_height="match_parent">
    </FrameLayout>
</androidx.constraintlayout.widget.ConstraintLayout>
```
2. En *activity_main_land.xml* creamos dos FrameLayout uno para lista de contactos y otro 
para Detalles de contacto con un weight 40% y 60% usando un LinearLayout.    
2.1. **Nota:** los holders deben de tener el mismo id para que en cualquier orientacion 
el fragment se acomode aqui.   
```java
<androidx.appcompat.widget.LinearLayoutCompat ... android:orientation="horizontal" >
    <FrameLayout
        android:id="@+id/fragment_holder_contacts_list"...
        android:layout_weight="40">
    </FrameLayout>

    <FrameLayout
        android:id="@+id/fragment_holder_contat_details"...
        android:layout_weight="60">
    </FrameLayout>
</androidx.appcompat.widget.LinearLayoutCompat>y
```





### 154 156. Creando un Array List Adapter para mostrar los contactos en pantalla, Cargar los contactos con un fragment list
2. Creamos una clase que extienda de ListFragment 
2.1. Creamos una clase ArrayAdapter que se encargara de mostrar/inflar el fragmento
```java
public class ContactsListFragment extends ListFragment {
    private ArrayList<Contact> contacts;
    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contacts = ContactManagger.getInstance().getContacts(); //QUE TENGO QUE MOSTRAR
        ContactsListAdapter adapter = new ContactsListAdapter(contacts); // QUIEN SE ENCARGA DE MOSTRARLO
        setListAdapter(adapter);    // UNION
    }
//---------------adapter
    public class ContactsListAdapter extends ArrayAdapter<Contact> {
        public ContactsListAdapter(ArrayList<Contact> inContacts) {  //DESDE EL CONTRUCTOR NOS LLEGA LA LISTA A MOSTRAR
            super(getActivity(), R.layout.adapter_list_contacts_item, inContacts);
        }
        @NonNull @Override public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getLayoutInflater();
            if(convertView == null) {//SI NO SE A LLAMADO ANTES EL ADAPTER ESTE NO SE A LLENADO
                convertView = inflater.inflate(R.layout.adapter_list_contacts_item, null);
            }
            Contact currentContact = getItem(position);//COMO EXTIENDO DE ArrayAdapter PUEDO TENER EL ACTUAL ASI
            //ASIGNAMOS VALORES A MOSTRAR
            TextView txtName = convertView.findViewById(R.id.txt_item_contact_name);
            txtName.setText(currentContact.getName() +", " + currentContact.getAlias());
            return convertView;
}   } }
```

3. Relacionamos el Fragmento con el mainActivity.   
```java
FragmentManager fm = getSupportFragmentManager();
//si nos fijamos fragment_holder_contacts_list esta tanto en la vista hori como verti
Fragment contactsListFragment = fm.findFragmentById(R.id.fragment_holder_contacts_list);
if(contactsListFragment == null) {
    FragmentTransaction transaction = fm.beginTransaction();
    contactsListFragment = new ContactsListFragment();
    transaction.replace(R.id.fragment_holder_contacts_list, contactsListFragment);
    transaction.addToBackStack(null);
    transaction.commit();
}
```
**Resultado:**
![38_contactos_cambio_hori_verti.gif](./img/38_contactos_cambio_hori_verti.gif)   



### 157. La lógica de los dos tipos de layout
Objetivo: Vamos a crear el fragment de la derecha y crear la comunicacion entre 
activity y fragment para pasar la posicion entre estos, y asi el fragment saber 
que Contacto mostrar.    

1. Cremos una interface 
```java
public interface IActivityComs {
  void onListItemSelected(int position);
}
```


<!-- comunicar entre fragment y activity, pasar datos entre fragment y activity, pasar informacion entre fragment y activity comunicar entre activity y fragment, pasar datos entre activity y fragment, pasar informacion entre activity y fragment -->
2. Creamos comunicacion entre Activity y Fragment   
2.1. A nuestro ListFragment le agregamos una variable de la interface creada    
2.2. Sobre escribimos los metodos onAttach y onDetach para crear la relacion   
2.3. Creamos el comportamiento al hacer click   
```java
public class ContactsListFragment extends ListFragment {
    private IActivityComs mActivityComs;
    
    //POR AQUI NOS LLEGARA EL MAIN_ACTIVITY PERO LA USAREMOS COMO ActivityCons
    @Override public void onAttach(Context context) { super.onAttach(context);
        mActivityComs = (IActivityComs)context;
    }

    @Override public void onDetach() { super.onDetach();
        mActivityComs = null;//YA NO NECESITAMOS RELACION
    }
    
    @Override public void onListItemClick(ListView l, View v, int position, long id) {
        mActivityComs.onListItemSelected(position);
    }
```

4. Creamos **fragment_contact_details.xml.xml** la vista del fragment para mostrar los detalles del contacto.   
```
<LinearLayout ...vertical match_parent match_parent>
    <TextView android:id="@+id/frag_contact_details_name"... />
    <TextView android:id="@+id/frag_contact_details_alias" .../>
    <TextView android:id="@+id/frag_contact_details_number" .../>
    <TextView android:id="@+id/frag_contact_details_email" .../>
</LinearLayout>
```


5. Vamos a necesitar instanciar un fragment pasandole la posicion del contacto seleccionado.    
  5.1. este ```instantiate``` personalizado **pasara la posicion** preparando el Bundle con 
  este argumento.   
  5.2. el ```onCreate``` se llamara indirectamente, el cual nos dara el "currentContact".    
  5.2. ```onCreateView```  Ya con el "currentContact" creamos la vista.    
```java
public class ContactsDetailsFragment extends Fragment {
    private ArrayList<Contact> mContacts;
    private Contact currentContact;

    public static ContactsDetailsFragment instantiate(int position){
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        ContactsDetailsFragment frag = new ContactsDetailsFragment();
        frag.setArguments(bundle);
        return frag;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContacts = ContactManagger.getInstance().getContacts();
        int position = getArguments().getInt("position");
        this.currentContact = mContacts.get(position);
    }
    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contact_details, container, false);
        TextView txtName = v.findViewById(R.id.frag_contact_details_name);
        ...
        txtnEmail.setText(this.currentContact.getEmail());
        return v;
    }
}
```

6. Implementamos IActivityComs en MainActivity y en el metodo llamamos a detalles.   
  6.1. Si estamos en portrait entonces mostramos los detalles con un Activity.   
  6.2. Si estamos en landscape entonces mostramos los detalles con un Fragment.   
    6.2.1. Debemos de recuperar el fragment por su ID si ya ahi uno instanciado 
    lo eliminamos por que es el viejo.   
    6.2.2. Usamos el ```instantiate``` personalizado para obetener el framento 
    con la posicion del contacto seleccionado.    
```java
public class MainActivity extends AppCompatActivity implements IActivityComs {
...
@Override
public void onListItemSelected(int position) {
    if(findViewById(R.id.fragment_holder_contat_details) == null) {
        //ESTAMOS EN VERTICAL
        Intent intent = new Intent(this, PortraitDetailsContactActivity.class);
        intent.putExtra("position", position);
        startActivity(intent);
    } else {
        //ESTAMOS EN HORIZONTAL
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        Fragment fragmentContactDetailsOld = fm.findFragmentById(R.id.fragment_holder_contat_details); //recuperar el fragment por su ID
        Fragment fragmentContactDetailsNew = ContactsDetailsFragment.instantiate(position);

        if(fragmentContactDetailsOld != null) {
            //PUEDE DARSE EL CASO QUE YA SE MOSTRO UN DETALLE ANTERIOR ASI QUE AHI QUE QUITARLO
            transaction.remove(fragmentContactDetailsOld);
        }
        transaction.add(R.id.fragment_holder_contat_details, fragmentContactDetailsNew);
        transaction.commit();
    }
}
```

**Resultado:** Ya deberiamos de tener la vista horizontal/landscape que 
al pulsar un contacto del lado derecho se muestran los detalles.   
**Falta** hacer que funciona en vertical/portrait


### 159. El layout del contacto completo en vertical
**Objetivo:** Vamos a crear el activity para mostrar los datelles de contacto cuando 
estamos en forma portrait/vertical, **pero** reutilizando el fragment que ya creamos 
para la vista horizintal.    
<!-- Como crear fragmentos con comunicacion, uso de fragmentos con comunicacion, como usar fragmentos con comunicacion, uso fragmentos con comunicacion Como crear fragmentos medio, uso de fragmentos medio, como usar fragmentos medio, uso fragmentos medio-->
```java
public class PortraitDetailsContactActivity extends AppCompatActivity {
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portrait_details_contact);
        Bundle bundle = getIntent().getExtras();
        if(bundle == null || bundle.getInt(Constans.POSITION, -1) == -1) { throw new RuntimeException("No se encontro posicion"); }
        
        int posicion = bundle.getInt(Constans.POSITION);
        FragmentManager fm = getSupportFragmentManager();
        //REUTILIZAMOS FRAGMENT
        Fragment contactsDetailsFragment = fm.findFragmentById(R.id.fragment_holder_contat_details); 
        if(contactsDetailsFragment == null) {
            FragmentTransaction transaction = fm.beginTransaction();
            contactsDetailsFragment = ContactsDetailsFragment.instantiate(posicion);
            transaction.replace(R.id.fragment_holder_contat_details, contactsDetailsFragment);
            //transaction.addToBackStack(null);
            transaction.commit();
}   }   }
```

[**Commit:**](https://gitlab.com/MarioM51/hola_android) Git Init, -Mis Tareas- Terminada y -App contactos- vista cambiante horizontal 2 fragments, vertical 1 fragment   
**Resultado:** Tenemos Comunicacion entre MainActivity y Fragment el cual reutilizamos el fragment de details en la segunda actividad.   
![39_contactos_cambio_hori_verti_pase_info_acti_frag](./img/39_contactos_cambio_hori_verti_pase_info_acti_frag.gif)   


























<br /><br /><br /><br /><br /><br />
## Sección 15: Día 14: Swipes, Pager Views o cómo hacer un tutorial guiado
Vamos a Crear el swipe para mostrar mayor contenido deslizando el dedo.    
![40_ejemplo_swipe](./img/40_ejemplo_swipe.gif)   

### 163-164-165-166 Swipes y resoluciones de imágenes - El layout del view pager - La lógica del view pager - La lógica del Main Activity
Teoria.    
- El **view pager** es un View/Widget que nos sirve para mostrar un arreglo de imagenes.    

1. Descagamos las [imagenes](https://a2.udemycdn.com/2018-01-22_18-35-25-1d1caf70eb40b2f114ab59d0f9917d95/original.zip?nva=20191024105532&download=True&filename=imagenes-slide.zip&token=005a4284518aef353385a)

2. Las pegamos en drawable-xhdpi

3. En nuestro root layout lo cambiamos su altura y ancho a fill_parent
```xml
<androidx.coordinatorlayout.widget.CoordinatorLayout 
    ... android:layout_width="fill_parent" android:layout_height="fill_parent" >
    <androidx.viewpager.widget.ViewPager
        android:id="@+id/pager"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content" />
</androidx.coordinatorlayout.widget.CoordinatorLayout>
```

4. Creamos un nuevo layout **pager_item** que sera el que llevara dentro el pager
```xml
<LinearLayout...
    android:gravity="center"
    android:padding="10dp">

    <androidx.appcompat.widget.AppCompatImageView
        android:id="@+id/page_item_image"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />
```

5. Agregamos una clase PagerAdapter para que infle el ViewPager
```java
public class ImagePagerAdapter extends PagerAdapter {
    private Context context; private int[] images; private LayoutInflater inflater;

    public ImagePagerAdapter(Context context, int[] images) {
        this.context = context; this.images = images;
    }

    
    @Override public int getCount() {//PARA QUE SEPA CUANTAS MOSTRAR
        return this.images.length;
    }

    @Override //POR SI ES EL MISMO QUE CREE UNO NUEVO SI NO REUTILIZAR
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;//CASI SIEMPRE SERA ASI
    }

    @NonNull @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);//INFLADOR ESTANDART
        View itemView = inflater.inflate(R.layout.pager_item, container, false);
        ImageView currenImage = itemView.findViewById(R.id.page_item_image);
        currenImage.setImageResource(images[position]);
        ((ViewPager)container).addView(itemView);
        return itemView;
    }

    @Override//ESTE NOS DA LA POS Y ITEM=LAYOUT QUE YA SE PUEDE ELIMINAR
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ((ViewPager)container).removeView((LinearLayout) object);
    }
}
```

6. Ya solo indicamos que el adapter muestre las imagenes
```java
public class SwipeActivity extends AppCompatActivity {
    ViewPager viewPager;
    PagerAdapter pagerAdapter;
    int[] images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe);
        images = new int[] { R.drawable.im1, R.drawable.im2, R.drawable.im3,
                             R.drawable.im4, R.drawable.im5, R.drawable.im6 };
        viewPager = findViewById(R.id.pager);
        pagerAdapter = new ImagePagerAdapter(SwipeActivity.this, images);
        viewPager.setAdapter(pagerAdapter);
    }
```

**Resultado:**    
![41_swipe_horizontal_mio.gif](./img/41_swipe_horizontal_mio.gif)   




### 167.168. Definiendo un View Pager con Fragments - El View Pager Adapter
Es mejor hacerlo con fragmentos ya que como en la lista de contactos podemos reutilizar y tener 
un diseño mas apropiado para cada orientacion op dispositivo.   

1. Creamos una nueva activity y en su layout solo ponemos un root de tipo FrameLayout, **activity_swipe_with_fragmet.xml**.    

2. Creamos una clase que exteienda de fragmento.    
2.1. Creamos un metodo newInstance para que reciba lo que se va a mostrar.   
2.2. En el onCreate recuperamos lo que preparamoes en el newInstance.    
<!-- pasar objeto por el bundle, pasar objetos por el bundle, usar bundle para pasar objetos, usar bundle para enviar objetos, uso bundle para objetos, uso bundle con objetos-->
```java
public class SwipeFragment extends Fragment {

    public static final String MESSAGE = "properties";

    public static SwipeFragment newInstance(SwipeFragmentProperties properties){
        SwipeFragment swipeFragment = new SwipeFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(MESSAGE, properties);
        swipeFragment.setArguments(bundle);
        return swipeFragment;
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SwipeFragmentProperties props = (SwipeFragmentProperties) getArguments().getSerializable(MESSAGE);
        View v = inflater.inflate(R.layout.fragment_swipe, container, false);
        ConstraintLayout root = v.findViewById(R.id.fragment_swipe_root);
        root.setBackgroundColor(props.getRootbackgroud());
        TextView txtMessage = v.findViewById(R.id.txt_fragment_message);
        txtMessage.setText(props.getMessage());
        ImageView imgUpBox = v.findViewById(R.id.img_box_up);
        imgUpBox.setBackgroundColor(props.getBoxUo());
        ImageView imgDownBox = v.findViewById(R.id.img_box_down);
        imgDownBox.setBackgroundColor(props.getBoxDown());
        ImageView imgRigth = v.findViewById(R.id.img_rigth);
        imgRigth.setImageResource(props.getImageResource());
        return v;
    }
}
```

3. En el Activity donde tendremos el PagerAdapter en el cual inflara cada fragemento con la info que le demos.   
3.1. Creamos los paneles a mostrar.    
3.1. Creamos el adapter que es de tipo FragmentPagerAdapter.    
```java
public class SwipeWithFragmetActivity extends AppCompatActivity {
    private SwipeFragmentPagerAdapter swipeFragmentPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_with_fragmet);
        //elementos a mostrar
        List<Fragment> fragmentList = new ArrayList<Fragment>();
        
        fragmentList.add(SwipeFragment.newInstance(
                new SwipeFragmentProperties("Primero mensaje", Color.rgb(0, 210, 190), Color.rgb(100, 27, 72), Color.rgb(200, 131, 72),R.drawable.im1) )
        );
        fragmentList.add(...); fragmentList.add(...); fragmentList.add(...); fragmentList.add(...);
        swipeFragmentPagerAdapter = new SwipeFragmentPagerAdapter(getSupportFragmentManager(), fragmentList);
        ViewPager pager = findViewById(R.id.pager_fragmets);
        pager.setAdapter(swipeFragmentPagerAdapter);
    }

    private class SwipeFragmentPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> fragments;
        public SwipeFragmentPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm); this.fragments = fragments;
        }
        @Override public int getCount() { return fragments.size(); }
        @Override public Fragment getItem(int position) { return fragments.get(position); }
    }
}
```

**Resultado:** Podemos agregar tantos swipes personalizados como querramos con 
el ```fragmentList.add```.    
![42_swipe_horizontal_fragments.gif](./img/42_swipe_horizontal_fragments.gif)   































<br /><br /><br /><br /><br /><br />
## Sección 16: Día 15: Drawer Layouts, Menús y toma de fotos
Vamos a trabajar con geolocalisacion, base de datos, imagenes/fotos, un 
tipo de snapchat donde se toma la foto se guarda la geolocalizacion en 
una BD.    

<!-- drawer menu -->
**Navigation Drawer** este es un tipo de menu de navegacion que sale de la 
derecha.

![43_navigation_drawer_ejemplo.PNG](./img/43_navigation_drawer_ejemplo.PNG)   

1. Creamos un nuevo proyecto snapMap.    
**NOTA:** Procurar tener el ideoma por default el ingles, ya que si nuestra app 
no cubre el idioma del usuario tomara el default, y lo mas probable es que entienda 
el ingles o nuestro idioma nativo.   



### 173. Creando los tres fragmentos de la app
Estos 3 fragmentos son los que seran accesibles desde el Navigation Drawer, 1 para 
tomar la foto, 2 para la lista de titulos, 3 para la lista de etiquetas.   

1. Creamos los fragmentos de cada uno.   
  1.1. Creamos **TitlesFragment** el encargado de que titulo tendran las fotos.   
  1.2. Creamos **TagsFragment** encargado de las etiquetas de las fotos.   
  1.3. Creamos **CaptureFragment** encargado de tomar foto y poner titulo y el layout **fragment_capture**.   
  1.4. Creamos el onCreate en los 3   
- **Nota:** Solo creamos el onCreateView en CaptureFragment porque los otros son extendidos de ListFragment 
  y el encargado de mostrar la vista es el adapter.    
```java
public class TitlesFragment extends ListFragment {
    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
```
----
```java
public class TagsFragment extends ListFragment {
    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
```
----
```java
public class CaptureFragment extends Fragment {
    @Override public void onCreate(@Nullable Bundle savedInstanceState) { super.onCreate(savedInstanceState); }
    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_capture, container, false);
        //TODO: falta configurar
        return v;
    }
}
```


### 174. Diseñando el layout del Drawer
1. Creamos un Drawer Layout dentro de un Relative (root), en este esta tanto 
el navigate drawer como el contenido.   
- [44_navigation_drawer_base.xml](./img/44_navigation_drawer_base.xml)   




### 176. Un switch para seleccionar el fragmento correcto
1. En *MainActivity* crearemos un metodo que cambiara los fragments en este caso 
para cambiar el contenido del cuerpo de la app.   
```java
private void switchFragment(int position) {
  Fragment fragment = null;
  String fragmentID = null;
  switch (position) { //decidimos que Fragment mostrar
      case 1:
        fragmentID = "TITLES";
        Bundle bundle = new Bundle();//nos servira para pasarle tags
        bundle.putString("Tag", "NO_TAG");
        fragment = new TitlesFragment();
        fragment.setArguments(bundle);
      break;
      case 2:
        fragmentID = "TAGS";
        fragment = new TagsFragment()
      break;
      case 3:
        fragmentID = "CAPTURE";
        fragment = new CaptureFragment();
      break;
      default: Log.e(TAG, "Entro en el switch default que no deberia de entrar"); break;
  }
  //mostramos fragment
  FragmentManager fragmentManager = getSupportFragmentManager();
  fragmentManager.beginTransaction()
          .replace(R.id.main_fragment_holder, fragment, fragmentID)
          .commit();
  //cerramos menu de navegacion
  mDrawerLayout.closeDrawer(mNavDrawerListView);
}
```


### 177. El botón para abrir y cerrar el menú drawer
1. Configuramos el boton de abrir/cerrar menu
```java
private void setupDrawer() {//creamos el boton para abrir y cerrar el nav menu
  mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
          R.string.drawer_open, R.string.drawer_close){
      @Override
      public void onDrawerOpened(View drawerView) {
          super.onDrawerOpened(drawerView);
          getSupportActionBar().setTitle(getString(R.string.menu_choice));//CAMBIARA EL TEXTO DE ARRIBA
          invalidateOptionsMenu();//LLAMA AUTO. AL METODO onPrepareOptionMenu PARA QUE REFRESQUE EL TEXTO
      }

      @Override
      public void onDrawerClosed(View drawerView) {
          super.onDrawerClosed(drawerView);
          getSupportActionBar().setTitle(mActivityTitle);
          invalidateOptionsMenu();
      }
  };
  mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);//LO ACTIVAMOS
  mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);//TODO: ver cual es la nueva forma
}
```

<!-- guardar el estado de navagacion, evitar que se pierda la navagacion-->
2. Cremos los metodos para que la app recuerde el estado de la aplicacion 
y en caso de rotarla o regressar a la app, se siga mostrando la opcion seleccionada 
y no la de por defecto/inicio.   
```java
@Override
public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
    super.onPostCreate(savedInstanceState, persistentState);
    mActionBarDrawerToggle.syncState();
}

@Override
public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    mActionBarDrawerToggle.onConfigurationChanged(newConfig);
}
```


### 178. La lógica del botón atrás
**Problema** Cuando el usuario pulsa el boton atras del movil, queriendo que 
se cierre el menu de navegacion este no se hara y en cambio hara otra cosa como 
cerrar la app o retornar de fragmento&actividad.    
**Teoria:** El metodo *onBackPress* se llama cuando pulsamos el boton de atras del movil y aqui 
podemos cambiar el comportamiento por default.   
**Solucion:** debemos de comprobar si el nav menu esta abierto y si lo esta debemos de cerrarlo.   
**Tomar en cuenta** que si el nav menu esta cerrado pero no estamos en la pantalla inicial debemos de 
cerrar regresar en esta **Y** que si esta cerrado el nav menu y estamos en la pantalla inicial debemos 
de cerrar la app.   

```java
@Override
public void onBackPressed() {
  //super.onBackPressed(); SI NO LO QUITAMOS HACE LO DICHO POR DEFAULT
  if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) { //EL NAV MENU ESTA ABIERTO
      mDrawerLayout.closeDrawer(mNavDrawerListView);
  } else {// EL NAV MENU ESTA CERRADO
    Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment_holder);
    if(currentFragment instanceof TitlesFragment) {//ESTOY EN TITLE DEBO CERRAR LA APP
      finish();
      System.exit(0);//TODO: DEBERIAMOS DE PREGUNTAR ANTES DE SALIR
    } else { //NO ESTOY EN TITLE (PANTALLA INICIAL) DEBO IR A ELLA
      this.switchFragment(0);
    }
  }
}
```

nose bien que pex, creo es para que actue correctamente.   
```java
@Override
public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
        return true;//PARA QUE ACTUE CORRECTAMENTE EL MENU DAWER
    }
    return false;
}
```

**Checar:** Tener cuidado con la creacion del layout ya que me estuvo 
fallando, al parecer le puse el **tools:context del fragment_holder** 
a otro.   

**Resultado:** Nos muestra una pantalla como si estubiera cargando 
cuando estamos en title o en tags, porque no hemos inflado estas vistas, 
pero si en capture se puede ver un texto. Tambien podemos ver que cuando 
pulsamos atras nos lleva a title(en teoria aun no se aprecia) y al darle 
otra vez ya cierra la app.   
![45_navigation_drawer_menu_deslisable_lateral](./img/45_navigation_drawer_menu_deslisable_lateral.gif)   





### 181.183 Un intent para abrir la cámara de fotos - La lógica del intent con resultado final
Podriamos desarrollar una app desde cero para tomar fotos pero esto nos 
llevaria mucho tiempo y no es el obj de nuestra app, en cambio vamos a 
usar la app por defecto de fotos que nos basta para nuestro desarrollo.   


<!-- lanzar una actividad externa, abrir app externa, abrir otra app, lanzar app externa, EN ESTE CASO DE FOTOS -->
1. Pedimos permisos de tomar fotos y agregamos la caracteristica en el 
manifest.xml abajo o arriba del \<application\> da igual.   
  1.1. Agregamos permiso
```xml
<uses-permission android:name="android.permission.CAMERA" />
```

  1.2. agregamos feature
```xml
<uses-feature android:name="android.hardware.camera2" />
```

2. Creamos el llamado
2.1. Necesitamos un ID de nuestro request el que sea.  
2.1. Creamos un ImageView para mostrar la foto que tomo/tomara el usr dentro de nuestra app.   
```java
public class MainActivity extends AppCompatActivity {
  private static final int CAMERA_REQUEST = 12345;
  
  protected void onCreate(Bundle savedInstanceState) {
  ImageView imgFot = findViewById(R.id.img_fot);
        Button btnTakePhoto = findViewById(R.id.btn_take_photo);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                //AQUI EL CONTROL PASA A LA APP DE FOTOS Y QUEDAMOS ESPERANDO RESULTADO
            }
        });
  }
```
2.3. Usamo el metodo para que nos llegue el resultado de la otra app/activity, 
**en data** tenemos toda la info en este caso para obtener la img. que nos llega 
en forma de bitMap ya que es el mas lijero.   
```java
@Override
protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
  if(requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
    //EL REQUEST_REQUEST ES EL MISMO QUE EL QUE MANDAMOS
    //Y EL RESULTADO FUE OK, YA QUE EL USUARIO PUDO NO TOMAR LA FOTO O OCURRIO UN ERROR
    Bitmap photo = (Bitmap) data.getExtras().get("data");
    imgFot.setImageBitmap(photo);
  }
}
```
**Cuidado:** Cuidado con los permisos, ya que pueda que no nos pida permiso 
y nos niegue el acceso a la camara.   
![46_tomar_foto_y_tenerla_en_nuestra_app.gif](./img/46_tomar_foto_y_tenerla_en_nuestra_app.gif)   



### 184. El layout para la toma de fotos de nuestra app
- Creamos diseño de la pantalla de captura,

### 185. La clase URI y su rol en la creación de un fichero en la memoria externa . Creando el fichero temporal externo para guardar la foto tomada
1. Pedimos permisos, para tomar fotos, para guardar fotos en el external storage
```xml
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

<uses-feature android:name="android.hardware.camera" />
```


2. EN *CaptureFragment* le 
  2.1. agregamos el evento del boton para que lance el intent el cual creara un archivo **en la URI que le indicamos**
```java
public class CaptureFragment extends Fragment {
  private static final int CAMERA_REQUEST = 12345;
  private ImageView mImgFot;
  private Uri mImageUri = Uri.EMPTY;
  private String mCurrentPhotoPath;

  @Nullable @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    ...
    btnTakePhoto = v.findViewById(R.id.btn_capture);
    btnTakePhoto.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (photoFile != null) {
                mImageUri = Uri.fromFile(photoFile);
+               cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        }
    });
```
  2.2. En la misma clase agregamos un metodo para crear un archivo
```
private File createImageFile() throws IOException {
    //Creamos el nombre de la foto basado en la fecha en que ha sido tomada
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    String imageFileName = "IMG_"+timeStamp+"_";

    //@deprecated File storeageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    File storeageDirectory =  getActivity().getBaseContext().getExternalFilesDir(null);

    File image = File.createTempFile(
            imageFileName, //nombre del fichero a crear
            ".jpg", //extensión del fichero
            storeageDirectory //directorio o carpeta donde guardamos el fichero
    );

    //Guardamos para utilizarlo con el intent de Action View
    mCurrentPhotoPath = "file: "+image.getAbsolutePath();

    return image;
}
```

  2.3. Al regresar de la app de fotos y guardarla la mostramos **haciendo referencia a la URI**
  donde la guardo esta app.   
```java
@Override
public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    if(requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
        try {
            mImgFot.setImageURI(Uri.parse(mImageUri.toString()));
            turnOnSave();
        } catch (Exception e) {
            e.printStackTrace();
        }
    } else {
        mImageUri = null; //LIBERAMOS MEMORIA YA QUE ALGO NO SALIO BIEN
        turnOnCapture();
    }
}
```


**Resultado**: Estamos gaurdando una imagen en la memoria externa y usando su 
URI trabajar con ella dentro de nuestra app, **a diferencia** del uso del Bitmap 
directamente NO usmos memoria interna del cel y podemos guardar la URI en una base 
de datos.   
**Commit**: "gaurdando una imagen en la memoria externa y usando su".    
![47_tomar_foto_guardar_en_mem_externa_usar_uri_en_mi_app](./img/47_tomar_foto_guardar_en_mem_externa_usar_uri_en_mi_app.gif)   
































<br /><br /><br /><br /><br /><br />
## Sección 17: Día 16: Bases de datos en SQLite
La diferencia entre SQLite y otros motores de DB este que esta como dice su nombre 
es una version reducida.

**Teoria:** Se usa un Cursor para obtener datos del cual a partir de este podemos 
recorrer todo el clasico if(getNext()!=null)

### 192.193.194. Diseñando la interfaz de prueba, El data manager de SQLite, Implementando los métodos del data manager 
Vamos a crear una aplicacion de prueba para tener las bases de SQLite en Android.   

1. Creamos nuevo proyecto y su [interface.xml]()

2. Creamos una clase que es la que gestionara nuestra BD (un Helper).   
  2.1. Agregamos variables publicas con os nombres de la info a consultar.   
  2.2. Agregamos variables privadas con la estructura de la BD.   
  2.3. Agregamos una clase extencion de SQLiteOpenHelper para gestionar la BD.   
  2.4. Creamos los metodos CRUD.   
  2.3. Creamos clase interna que tendra las helpers de android y pondremos aqui la creacion de tablas.
<!-- uso de sqlite, crear bd, crear base de datos, crear consulats sql, uso sqlite, uso base de datos, uso db-->
```java
public class DataManager {
  private static final String TAG = "DataManager";
  private SQLiteDatabase database;

  public static final String TABLE_ROW_ID = "_id";
  public static final String TABLE_ROW_NAME = "name";
  public static final String TABLE_ROW_AGE = "age";

  private static final String DATABASE_NAME = "my_db";
  private static final String TABLE_NAME = "persons";
  private static final int DATABASE_VERSION = 1;

  public DataManager(Context context) {
      CustomSQLiteopenHelper helper = new CustomSQLiteopenHelper(context, DATABASE_NAME, DATABASE_VERSION);
      database = helper.getWritableDatabase();//OBTENEMOS LA BD
  }

  public void insert(String name, String age) { //insertar
      String queryInsert = String.format(
       "INSERT INTO %s (%s, %s) VALUES('%s', '%s');",
       TABLE_NAME, TABLE_ROW_NAME, TABLE_ROW_AGE, name, age
      );
      Log.i(TAG, "insert query: " + queryInsert);
      database.execSQL(queryInsert);
  }

  public void delete(String name) { //eliminar
      String queryDelete = String.format(
              "DELETE FROM %s WHERE %s='%s';",
              TABLE_NAME, TABLE_ROW_NAME, name
      );
      Log.i(TAG, "delete query: " + queryDelete);
      database.execSQL(queryDelete);
  }

  public Cursor getUser(String name) { //obtener un usuario
      String queryGetUser = String.format(
              "SELECT %s, %s, %s FROM %s WHERE %s='%s';",
              TABLE_ROW_ID, TABLE_ROW_NAME, TABLE_ROW_AGE, TABLE_NAME, TABLE_ROW_NAME, name);
      Log.i(TAG, "getUser query: " + queryGetUser);
      Cursor c = database.rawQuery(queryGetUser, null);
      return c;
  }

  //obtener todos los usuarios
  public Cursor getAll() {
      String queryGetAll = String.format("SELECT * FROM %s;", TABLE_NAME);
      Log.i(TAG, "getAll query: " + queryGetAll);
      Cursor c = database.rawQuery(queryGetAll, null);
      return c;
  }

//####################################################
  private class CustomSQLiteopenHelper extends SQLiteOpenHelper {

      public CustomSQLiteopenHelper(@Nullable Context context, @Nullable String name, int version) {
          super(context, name, null, version);
      }

      @Override
      public void onCreate(SQLiteDatabase db) {
          final String create_table_persons = String.format(
              "CREATE TABLE %s(" +
                      "%s integer primary key autoincrement not null, " +
                      "%s text not null, " +
                      "%s text not null" +
               ");", TABLE_NAME, TABLE_ROW_ID, TABLE_ROW_NAME, TABLE_ROW_AGE
          );
          Log.i(TAG, "onCreate table persons: " + create_table_persons);
          //ERROR database.execSQL(create_table_persons); CUIDADO NO CONFUNDIR CON LA VARIABLE DEL DATAMANAGER
          db.execSQL(create_table_persons);
      }

      @Override
      public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }
  }
}
```


### 195. La actividad principal de la base de datos
Ya tenemos la gestion de la BD ahora bamos a vincular estos con la UI (los botones)
```java
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    Button btnInsert, btnDelete, btnSearch, btnAll;
    EditText inTxtNameToInsert, inTxtAgeToInsert, inTxtNameToDelete, inTxtNameToSearch;
    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      dataManager = new DataManager(this);
      //TRAEMOS 4 BOTONES Y LE PONEMOS CLICKLISTENERS Y 4 EDITTEXT
      btnInsert = findViewById(R.id.btn_insert); ... btnAll = findViewById(R.id.btn_select_all);
      btnInsert.setOnClickListener(this); .... btnAll.setOnClickListener(this);
      inTxtNameToInsert = findViewById(R.id.intxt_nombre); ... findViewById(R.id.intxt_edad);
    }

    @Override public void onClick(View v) {
      switch (v.getId()) {
        case R.id.btn_insert:
          dataManager.insert(inTxtNameToInsert.getText().toString(), inTxtAgeToInsert.getText().toString());
        break;
        case R.id.btn_delete:
          dataManager.delete(inTxtNameToDelete.getText().toString());
        break;
        case R.id.btn_search:
          Cursor cUser = dataManager.getUser(inTxtNameToSearch.getText().toString()); showData(cUser);
        break;
        case R.id.btn_select_all:
          Cursor cAll = dataManager.getAll(); showData(cAll);
        break;
        default:Log.d(TAG, "Callo en switch dafault"); break;
      }
    }

    private void showData(Cursor c) {
      String persontemp = null;
      while(c.moveToNext()) {
        persontemp = String.format("person: id: %s, name: %s, age: %s", c.getString(0), c.getString(1), c.getString(2));
        Log.i(TAG, "showData: " + persontemp);
      }
    }
}
```
**Resultado:** Podemoa ver un CRUD con SQlite funcionando de una tabla 
personas con campos nombre y edad.    
![48_uso_sqlite_crud_basico.gif](./img/48_uso_sqlite_crud_basico.gif)   
**Commit:** "Agrego ejemplo basico de CRUD con Sqlite de tabla personas(nombre, edad)".   





### 196. La clase foto de nuestra app
Creamos nuestro modelo de datos en este caso manejaremos fotos.
```java
public class Photo {
    private String title;
    private String tag1,tag2,tag3;
    private android.net.Uri starageLocation;
    
    //getters/setters
```


### 197.198.199.200. El data manager de la app. Insertando la foto en la base de datos, Consultas varias al data manager, OnCreate para generar las tablas de la base de datos
**Nota** No necesariamente estricto que el DataManager sea de tipo **Singleton** ya 
que todas las instancias chuparian de la misma Base de datos, asi que no habria 
problemas de sincronisacion.    

**Nota** Tendremos una relacion **uno a muchos**  en tags donde un tag lo podran 
tener muchas fotos.   

1. Creamos nuestro DataManager con su OpenHelper personalizado.
1.1. del OpenHelper añadimos la creacion de las tablas de photos y de tags.   

2. Creamos el metodo de insertar photo y de insertar tag si no existe.   

3. Consultamos todos los titulos, todas las etiquetas, o todos los datos de una foto.   

<!-- sql para insertar registro si no existe, sql para insertar si no existe,  insertar tag si no existe -->
<!-- sql para insertar registro en tabla solo si no existe, sql para insertar solo si no existe,  insertar tag solo si no existe -->

```java
import android.database.Cursor; import android.database.sqlite.SQLiteDatabase; import android.database.sqlite.SQLiteOpenHelper;
public class DataManager {
    public static final String TAG = "DataManager";

    private SQLiteDatabase db;

    public static final String TABLE_ROW_ID = "_id";
    public static final String TABLE_ROW_TITLE = "image_title";
    public static final String TABLE_ROW_URI = "image_uri";

    public static final String TABLE_ROW_TAG = "tag";
    private static final String TABLE_ROW_TAG1 = "tag1";
    private static final String TABLE_ROW_TAG2 = "tag2";
    private static final String TABLE_ROW_TAG3 = "tag3";

    private static final String DB_NAME = "snapmap_db";
    private static final int DB_VERSION = 1;
    private static final String TABLE_PHOTOS = "snapmap_table_photos";
    private static final String TABLE_TAGS = "snapmap_table_tags";

    public DataManager(Context context){
        SnapMapSQLiteOpenHelper helper = new SnapMapSQLiteOpenHelper(context);
        db = helper.getWritableDatabase();
    }

    /*METODOS DEL DATA MANAGER*/
    public void addPhoto(Photo photo){

        String query = "INSERT INTO "+
                TABLE_PHOTOS + " ("+ TABLE_ROW_TITLE+ ", "+ TABLE_ROW_URI + ", "+ TABLE_ROW_TAG1 + ", "+ TABLE_ROW_TAG2 + ", "+ TABLE_ROW_TAG3 + ") " +
                "VALUES ("+ "'"+photo.getTitle()+"', "+ "'"+photo.getStorageLocation()+"', "+ "'"+photo.getTag1()+"', "+ "'"+photo.getTag2()+"', "+ "'"+photo.getTag3()+"'" + ");";
        Log.i(TAG, query);
        db.execSQL(query);
        addTag(photo.getTag1()); addTag(photo.getTag2()); addTag(photo.getTag3());
    }


    private void addTag(String tag){
        String query = "INSERT INTO "+ TABLE_TAGS+ " ("+ TABLE_ROW_TAG + ") "+
                "SELECT '"+tag+"' "+ "WHERE NOT EXISTS ("+ "SELECT 1 FROM "+ TABLE_TAGS+ " WHERE "+TABLE_ROW_TAG+" = '"+tag+"'"+ ");";
        Log.i(TAG, query); db.execSQL(query);
    }


    public Cursor getTitles(){
        String query = "SELECT "+ TABLE_ROW_ID+", "+TABLE_ROW_TITLE+ " FROM "+TABLE_PHOTOS; Log.i(TAG, query);
        Cursor c = db.rawQuery(query,null);
        c.moveToFirst(); return c;
    }

    public Cursor getTitlesWithTag(String tag){
        String query = "SELECT "+
                TABLE_ROW_ID+ ", "+TABLE_ROW_TITLE + " FROM "+TABLE_PHOTOS+ " WHERE "+
                TABLE_ROW_TAG1 + " = '"+tag+"' OR "+
                TABLE_ROW_TAG2 + " = '"+tag+"' OR "+
                TABLE_ROW_TAG3 + " = '"+tag+"';"; Log.i(TAG, query);
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst(); return c;

    }

    public Cursor getPhoto(int id){
        String query = "SELECT * FROM "+TABLE_PHOTOS+" WHERE "+TABLE_ROW_ID+" = "+id; Log.i(TAG, query);
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst(); return c;
    }

    public Cursor getTags(){
        String query = "SELECT "+TABLE_ROW_ID+ ", "+TABLE_ROW_TAG +" FROM "+TABLE_TAGS; Log.i(TAG, query);
        Cursor c = db.rawQuery(query,null);
        c.moveToFirst(); return c;
    }

    private class SnapMapSQLiteOpenHelper extends SQLiteOpenHelper{
        public SnapMapSQLiteOpenHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }
        @Override public void onCreate(SQLiteDatabase db) {
            Log.i(TAG, "##################################");
            String newTableQuery = "CREATE TABLE "+ TABLE_PHOTOS + " ("+
                    TABLE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+
                    TABLE_ROW_TITLE + " TEXT NOT NULL, "+
                    TABLE_ROW_URI + " TEXT NOT NULL, "+
                    TABLE_ROW_TAG1 + " TEXT NOT NULL, "+
                    TABLE_ROW_TAG2 + " TEXT NOT NULL, "+
                    TABLE_ROW_TAG3 + " TEXT NOT NULL"+ ");";
            Log.i(TAG, newTableQuery);
            db.execSQL(newTableQuery);

            newTableQuery = "CREATE TABLE "+
                    TABLE_TAGS + " ("+
                    TABLE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+
                    TABLE_ROW_TAG + " TEXT NOT NULL"+ ");";
            Log.i(TAG, newTableQuery);
            db.execSQL(newTableQuery);
            Log.i(TAG, "##################################");
        }
        @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { //COMING SOON... }
    }
}
```

Da como resultado los siquientes SQLs
```sql
    -PENDIENTE-
-- insertar foto
INSERT INTO snapmap_table_photos (image_title, image_uri, tag1, tag2, tag3) 
    VALUES ('123', 'file:///...', '123', '123', '123');

-- insertar tag
INSERT INTO snapmap_table_tags (tag) SELECT '123' 
    WHERE NOT EXISTS (SELECT 1 FROM snapmap_table_tags WHERE tag = '123');

-- obtener todas las fotos
SELECT _id, image_title FROM snapmap_table_photos

-- obtener una foto
SELECT * FROM snapmap_table_photos WHERE _id = 1

```


### 201. Persistiendo la foto en la base de datos
**Objetivo:** Vamos a usar nuestro DataManager en **CaptureFragment** para guardar 
la foto en la DB.

1. Creamos instancia de DataManager y lo insanciamos en onCreate

2. Cundo se el usr da click sobre el click listener usamos dataManager para guardar la foto

```java
public class CaptureFragment extends Fragment {
    ...
+   private DataManager mDataManager; //lo inicializamos en onCreate
    ...
    @Override public void onCreate(@Nullable Bundle savedInstanceState) {...
        mDataManager = new DataManager(getActivity().getApplicationContext());
    }
    
    public View onCreateView(... {
      ...
      btnSavePhoto.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          if(mImageUri != null ) { //hay fichero
            if (!mImageUri.equals(Uri.EMPTY)) { //el fichero no esta vacio talvez por que cancelo el usuario
            Photo photo = new Photo();
            photo.setTitle(inTxtTitle.getText().toString());
            photo.setStorageLocation(mImageUri);
            photo.setTag1(inTxtTag1.getText().toString());
            photo.setTag2(inTxtTag2.getText().toString());
            photo.setTag3(inTxtTag3.getText().toString());
+           mDataManager.addPhoto(photo);
            Toast.makeText(getActivity(), "Se guardo la foto en BD", Toast.LENGTH_SHORT).show();
          } else {
            Toast.makeText(getActivity(), "No ahi imagen a guardar", Toast.LENGTH_LONG).show();
          }
        } else {
          Log.e("CaptureFragment", "onClick() returned: " + "Error Tecnico no ahi Uri que guardar para guardar la photo");
        }
      }
      });
   }
```

**Resultado:** Creamos un datamanager y lo usamos para guardar una foto (el path donde se guardo) 
con su titulo, Uri, tag123 **Falta** mostrar que el usuario pueda var las fotos guardadas.   

**Commit:** "App SnapMap - agrego modelo foto, dataManager y uso de guardado de foto".   










<br /><br />
### 204. El layout para visualizar la foto y el mapa
Para poder mostrar la foto lo primero que haresmo es el layout

**Al recuperar los datos** NO usar obtener datos por posiciones ya que 
cualquier cambio haria que tronara por lo que obtenemos el dato por **getColumnIndex**.   

1. Usamos el data manager para obtener a travez de la posicion=idPhoto de la imagen.   
<!-- liberar memoria de una imagen, uso de memoria en imagenes, reciclar imagenes, borrar imagen de la memoria, -> onDestroy -->
```java
public class ShowPhotoFragment extends Fragment {
  private Cursor mCursor;
  private ImageView mImg;
  
  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    int position = getArguments().getInt("position", -1);
    if(position == -1) { Toast.makeText(getActivity(), "ERROR, no se encontro posicion", Toast.LENGTH_LONG).show(); }
    DataManager dataManager = new DataManager(getActivity().getApplicationContext());
    mCursor = dataManager.getPhoto(position);
  }

  @Nullable @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragmet_show_photo, container, false);
    TextView txtTitle = v.findViewById(R.id.show_photo_txt_title);
    mImg = v.findViewById(R.id.show_photo_img);
    Button btnShowMap = v.findViewById(R.id.show_photo_btn_show_map);
    
    txtTitle.setText(mCursor.getString(mCursor.getColumnIndex(DataManager.TABLE_ROW_TITLE)));
    mImg.setImageURI(Uri.parse(mCursor.getString(mCursor.getColumnIndex(DataManager.TABLE_ROW_URI))));

    return v;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    //objetemos referencia del bitmap
    BitmapDrawable bitmapDrawable = (BitmapDrawable)mImg.getDrawable();
    bitmapDrawable.getBitmap().recycle();
    mImg.setImageBitmap(null);
  }
}
```


### 206. El fragmento con la lista de títulos de fotos
<!-- Como funciona la comunicacion, como se comunican los fragmentos con el Activity, ejemplo comunicacion entre fragmento y actividad -->
<!-- Como SnapMap, ejemplo como se comunican la Activity con los fragmentos, ejemplo comunicacion entre actividad y fragmento, -->
<!-- enviar informacion de fragmento a actividad, enviar datos de fragmento a actividad, enviar info de fragmento a actividad-->
**IMPORTANTE : Estructura/logica:** Tenemos dos Fragments uno que mostrara todos los titulos o filtrados por tag, 
y el fragment de tags mostrara todos los tags, por lo que son muy parecidos, (muestran una lista a 
partir de algun parametro), y dicho parametro nos llegara por el **MainActivity**.   

Y al interactuar con los fragmentos de tags o titulos mandaran el parametro a MainActivity 
para que este se encarge de; 1) Mostrar una sola foto con toda su info, 2) mostrar los titulos 
con un X tag, por lo que creamos **ActivityComs** para esta comunicacion Fragment a Activity.    

![49_comunicacion_frament-activity_bilateral](./img/49_comunicacion_frament-activity_bilateral.PNG)   

1. Creamos una interface que nos sirva para comunicar,  
```java
public interface ActivityComs {
    
    String ID_PHOTO = "idPhoto";
    String TAG_NAME = "tag";

    //SE LLAMARA CUANDO SE PRECIONE SOBRE UN TITULO DE LA LISTA DE TITULOS
    void onTitleListItemSelected(int idPhoto);
    
    //SE LLAMARA CUANDO SE PRECIONE SOBRE UN TAG DE LA LISTA DE TAGS
    void onTagListItemSelected(String tagToSearch);
}
```


**Objetivo:** Recordar que **TitlesFragment** mostrara todos los titulos de las fotos que tengamos 
en la bd o **filtrara** aquellos titulos con X tag lo que significa que tenemos DOS escenarios 
1) el usuario quiere ver todos los titulo o 2) solo los titulos con X tag.   

1. En **TitlesFragment** recuperaremos todas las fotos y/o las fotos con X Tag 
y por medio de un Adapter mostraremos solo el titulo usando un Layout propio de android.    

2. Cuando pulsemos sobre un titulo significara le enviamos el ID a **MainActivity** para 
que este se encargue de mostrar el fragmento con toda la info de ducha foto.   
```java
public class TitlesFragment extends ListFragment {
    private Cursor mCursor;
    private ActivityComs mActivityComs;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String tag = getArguments().getString("tag");

        DataManager dataManager = new DataManager(getActivity().getApplicationContext());
        if(tag == "NO_TAG"){
            //SI EL USUARIO NO FILTRO POR NINGUNA TAG SIGNIFICA QUE QUIERE VER TODAS LAS FOTOS
            mCursor = dataManager.getTitles();
        } else {
            //SI AHI UN TAG EL USR SOLO QUIERE LAS FOTOS CON DICHO TAG.
            mCursor = dataManager.getTags();
        }

        //ESTE ADAPTER HARA EL TRABAJO SUCIO CON EL CURSOR
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(getActivity(),
            android.R.layout.simple_list_item_1,       // APARIENCIA DEL ITEM
            mCursor,                                   //LA PRIMERA POSICION DE NUESTRA COLECCION DE DATOS
            new String[]{DataManager.TABLE_ROW_TITLE}, //QUE DATO VAMOS A MOSTRAR
            new int[]{android.R.id.text1},             //NO_SE
            0                                          //NO_SE
        );
        setListAdapter(simpleCursorAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mCursor.moveToPosition(position);//vamos a la posicion de la lista seleccionada
        // del seleccionado extramemos su id
        int idPhoto = mCursor.getInt(mCursor.getColumnIndex(DataManager.TABLE_ROW_ID));
        mActivityComs.onTitleListItemSelected(idPhoto);
    }

    //SE MANDA A LLAMAR CUANDO CREAMOS EL FRAGMENTO Y NO LO PONEMOS EN EL ON_CREATE POR QUE NO 
    //NECESARIAMENTE SE CREA Y DESTRULLE CADA VEZ QUE LE CAMBIAMOS EL CONTENIDO AL FRAGMENT
    @Override public void onAttach(Context context) { super.onAttach(context); mActivityComs = (ActivityComs)context; }
    @Override public void onDetach() { super.onDetach(); mActivityComs = null; }
}
```


### 207. El fragmento con la lista de etiquetas de fotos
1. Hacemos que **TagsFragment** muestre todos los tags y al pulsar uno tag se lo envie a MainActivity.   

```java
public class TagsFragment extends ListFragment {
    public static final String TAG = "TagsFragment";

    private Cursor mCursor;
    private ActivityComs mActivityComs;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataManager dataManager = new DataManager(getActivity().getApplicationContext());

        mCursor = dataManager.getTags();

        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                getActivity(),//contexto
                android.R.layout.simple_list_item_1,
                mCursor,                  //la primera posicion de nuestra coleccion de datos
                new String[]{DataManager.TABLE_ROW_TAG}, //QUE DATO VAMOS A MOSTRAR
                new int[]{android.R.id.text1},           //TEXTO BASICO
                0
        );
        setListAdapter(simpleCursorAdapter);
    }

    @Override
    public void onListItemClick(ListView list, View v, int position, long id) {
        //CUAL A SIDO LA ETIQUETA QUE SE A SELECCIONADO
        Cursor c = ((SimpleCursorAdapter)list.getAdapter()).getCursor();//obtengo todos
        c.moveToPosition(position); //apunto a la laseleccionada
        String tag = c.getString(c.getColumnIndex(DataManager.TABLE_ROW_TAG));
        Log.i(TAG, "Etiqueta seleccionada: " + tag);

        //ENVIAMOS TAG A MAIN_ACTIVITY
        mActivityComs.onTagListItemSelected(tag);
    }

    //SE MANDA A LLAMAR CUANDO CREAMOS EL FRAGMENTO Y NO LO PONEMOS EN EL ON_CREATE POR QUE NO 
    //NECESARIAMENTE SE CREA Y DESTRULLE CADA VEZ QUE LE CAMBIAMOS EL CONTENIDO AL FRAGMENT
    @Override public void onAttach(Context context) { super.onAttach(context); mActivityComs = (ActivityComs)context; }
    @Override public void onDetach() { super.onDetach(); mActivityComs = null; }
}
```


2. **CONECTAMOS AL MAIN ACTIVITY CON SUS FRAGMENTS**
2.1. implementamos ActivityComs.    
2.2. Agrgamos el DataManager.    
2.3. Implementamos los metodos y ussamos el dataManager para enviar la info a mostrar.    
```java
public class MainActivity extends AppCompatActivity implements ActivityComs {

  @Override
  protected void onCreate(Bundle savedInstanceState) {...
    dataManager = new DataManager(getApplicationContext());
    ....
  }

  //ESTE METODO SE LLAMARA CUANDO SELECCIONEMOS UN TITULO
  @Override public void onTitleListItemSelected(int idPhoto) {
      Bundle bundle = new Bundle();
      bundle.putInt(ActivityComs.ID_PHOTO, idPhoto);

      ShowPhotoFragment showPhotoFragment = new ShowPhotoFragment();
      showPhotoFragment.setArguments(bundle);

      if(showPhotoFragment != null) {
          // si da nulo significa que talvez ocurrio un error,
          // lo mas seguro por la carga de la foto
          FragmentManager fragmentManager = getSupportFragmentManager();
          fragmentManager.beginTransaction()
                  .replace(R.id.fragment_holder, showPhotoFragment, "SHOW_PHOTO")
                  .commit();

          //ACTUALIZACMO EL TITULO Y LA OPCION SELECCIONADA, EN ESTE CASO
          mNavDrawerListView.setItemChecked(1, true);
          mNavDrawerListView.setSelection(1);
          mDrawerLayout.closeDrawer(mNavDrawerListView);//POR SI ESTABA AVIERTO EL MENU LO CIERRO

      }

  }

  //ESTE METODO SE LLAMARA CUANDO SELECCIONEMOS UN TAG
  @Override public void onTagListItemSelected(String tagToSearch) {
      Bundle bundle = new Bundle();
      bundle.putString(ActivityComs.TAG_NAME, tagToSearch);

      TitlesFragment titlesFragment = new TitlesFragment();
      titlesFragment.setArguments(bundle);

      FragmentManager fragmentManager = getSupportFragmentManager();
      fragmentManager.beginTransaction()
              .replace(R.id.fragment_holder, titlesFragment, "TAGS")
              .commit();

      //ACTUALIZACMO EL TITULO Y LA OPCION SELECCIONADA, EN ESTE CASO
      mNavDrawerListView.setItemChecked(1, true);
      mNavDrawerListView.setSelection(1);
      mDrawerLayout.closeDrawer(mNavDrawerListView);//POR SI ESTABA AVIERTO EL MENU LO CIERRO
  }
```

**Resultado:** Podemos agregar fotos con sus tag, ver todas y filtrarlas por 
tag seleccionando uno desde la lista de tags como ver una sola al seleccionarla 
desde la lista titulos.   

**Commit:** fotos_listar_todas_filtrar_por_tag_mostrar_una_commit_bueno.   

![50_fotos_listar_todas_filtrar_por_tag_mostrar_una.gif](./img/50_fotos_listar_todas_filtrar_por_tag_mostrar_una.gif)   


<b style="color:red;">Error</b>: Canvas: trying to use a recycled bitmap android.graphics.Bitmap
































<br /><br /><br /><br /><br /><br />
## Sección 18: Día 17: Mapas y GPS


### 208.209. Los mapas y el GPS en móvil, El location manager de Android
Android funciona con el GNSS que es un sistema de 24 satelites para siempre tener 3 
satelites disponibles para un usr.    

1. Creamos un **nuevo proyecto** y agreamos el permiso de acceso al GPS, el cual 
usuamos la posicion fina y aproximada del usuario, e intenrnet ya casi todos los 
telefonos usan internet para acceder al GPS.    

<!-- agregar permisos de localizacion, agregar permiso de gps permisos, agregar permisos de gps, agregar permisos de geolocalisacion permisos-->
```xml
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.INTERNET" />
```

2. En el mainActivity implementamos LocationListener para que nos llege 
la posicion a travez de sus metodos.   
2.1. Traemos el location mananager y checamos si tiene los permisos
```java
Location location = null;
boolean fineLocationGranted = ContextCompat.checkSelfPermission( getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED;
boolean fineLocationCoarse = ContextCompat.checkSelfPermission( getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
if ( Build.VERSION.SDK_INT >= 23 && fineLocationGranted && fineLocationCoarse) {
    Toast.makeText(this, "Agrega permisos de acceso porfa", Toast.LENGTH_LONG);
} else {
    location = locationMng.getLastKnownLocation(provider);
}
if(location != null) {
    txtSource.setText("Source: " + provider);
    onLocationChanged(location);
}
```

2.3. Implementamos los metodos para cuando se actualize los 
```java
@Override
public void onLocationChanged(Location location) {
    double lat = location.getLatitude();
    double longi = location.getLongitude();
    txtLatitud.setText("Latitud: " + String.valueOf(lat));
    txtLogitud.setText("Longitud: " + String.valueOf(longi));
    txtSource.setText("Source: " + provider);
}
@Override public void onStatusChanged(String inProvider, int status, Bundle extras) { txtSource.setText("Source: " + inProvider); }
@Override public void onProviderEnabled(String inProvider) { txtSource.setText("Source: " + inProvider); }
@Override public void onProviderDisabled(String inProvider) { txtSource.setText("Source: " + inProvider); }
```


3.5. Para que no chupe pila la actualizacion cuando se pause y 
reanude la app pausamos/activamos la captura de datos
```java
@Override protected void onPause() {
    super.onPause();
    locationMng.removeUpdates(this);
}

@Override protected void onResume() {
    super.onResume();
    //cada 1/2seg notificame o si el usr se mueve un metro notifica a esta clase
    boolean fineLocationGranted = ContextCompat.checkSelfPermission( getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED;
    boolean fineLocationCoarse = ContextCompat.checkSelfPermission( getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    if ( Build.VERSION.SDK_INT >= 23 && fineLocationGranted && fineLocationCoarse) {
        Toast.makeText(this, "Agrega permisos de acceso porfa", Toast.LENGTH_LONG);
    } else {
        locationMng.requestLocationUpdates(provider, 500, 1, this);
    }
}
```


**Resultado**: Podmeos generar archivos GPX (http://www.gpxeditor.co.uk/map o en http://gpx-poi.com/) simular 
el movimiento en el emulador.   
![51_hola_GPS.PNG](./img/51_hola_GPS.PNG)




### 210.211. Los Google Play Services - Integrando Google Maps en un proyecto de Android
Agregar google Maps, usar google maps.




### 214. Actualizar una base de datos ya existente cuando subimos de versión
Ahora para agregar a nuestra base de datos para poder guardar la posicion 

1. En nuestro proyecto de SnapMap en nuestro DataManager pasamos la variable de 
VERSION a 1 al 2.   
```java
public class DataManager {
    private static final int DB_VERSION = 2;
```
2. Agregamos lo que vamos a agregar en este caso 2 columnas lo que necesitaremos 
sus dos nombres, pero en caso de necesitar otra tabla se agregaria el nombre de la tabla.   
```java
//campos de la version 2
private static final String TABLE_ROW_LOCATION_LAT ="gps_location_lat";
private static final String TABLE_ROW_LOCATION_LONG ="gps_location_long";
...
@Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
  if(oldVersion < 2) {
      String addColLong = "ALTER TABLE " + TABLE_PHOTOS + " ADD " + TABLE_ROW_LOCATION_LONG + " REAL;";
      String addColLat = "ALTER TABLE " + TABLE_PHOTOS + " ADD " + TABLE_ROW_LOCATION_LAT + " REAL;";
      db.execSQL(addColLong);
      db.execSQL(addColLat);
  }
}
```

3. **IMPORTANTE:** Debemos de considerar los usuarios que ya tienen funcionando la version 1 
de la BD y los usuarios que instalan por primera vez la app, la cual se debe de crear la BD 
con los nuevos datos.   
```java
private class SnapMapSQLiteOpenHelper extends SQLiteOpenHelper{
@Override 
public void onCreate(SQLiteDatabase db) {
  Log.i(TAG, "##################################");
  String newTableQuery = "CREATE TABLE "+ TABLE_PHOTOS + " ("+
          ...
          TABLE_ROW_LOCATION_LAT + "REAL, " +     //cambio para la v2
          TABLE_ROW_LOCATION_LONG + "REAL, " +    //cambio para la v2
```



4. Agregamos los nuevos campos a nuestra consulta de insertar nueva foto.   
```java
public void addPhoto(Photo photo){
  String query = "INSERT INTO "+ TABLE_PHOTOS + " (... "+ TABLE_ROW_LOCATION_LAT + ", "+ TABLE_ROW_LOCATION_LONG + ") " +
      "VALUES (... "+ ""+photo.getLocation().getLatitude()+"" + ", " + ""+photo.getLocation().getLongitude()+"" + ");";
```



### 215. Guardar las coordenadas GPS de la foto
1. Agregamos los permisos para acceder a la geolocalizacion como vimos arriba.   


2. En **CaptureFragment** Implementamos el LocationListener para capturar la posicion.   
2.1. ```mLocation``` : Nos servira para en el medodo onChangeLocation tener la posicion del usr actualizada.   
2.1. 

<!-- pedir pemismos al usuario java, pedir pemismos al usuario en java, pedir pemismos al usuario en tiempo de ejecucion pedir pemismos java, pedir pemismos en java, pedir pemismos en tiempo de ejecucion 
 https://developer.android.com/training/permissions/requesting -->
```java
+public class CaptureFragment extends Fragment implements LocationListener {
    ...
+   private Location mLocation;
    private LocationManager mLocationManager;
    private String mProvider;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        ...
        //PARA LA CAPTURA DE LA LOCALIZACION EL EL METODO ONLOCATIONCHANGE
+       mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        mProvider = mLocationManager.getBestProvider(criteria, false);
    }


    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      btnSavePhoto.setOnClickListener(new View.OnClickListener() {
            ...
              Photo photo = new Photo();
+             photo.setLocation(mLocation); // OBTENEMOS UBICACION YA ACTUALIZADA EN ON_CHANGE_LICATION
              ...
      }
    }

    @Override public void onLocationChanged(Location location) {
+       mLocation = location;
    }

        
    @Override public void onResume() {
      super.onResume();
      boolean fineLocationGranted = ContextCompat.checkSelfPermission( getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED;
      boolean fineLocationCoarse = ContextCompat.checkSelfPermission( getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
      if ( Build.VERSION.SDK_INT >= 23 && fineLocationGranted && fineLocationCoarse) {
//PEDIMOS PERMISOS DE ACCESO DE GEOLOCATION
          ActivityCompat.requestPermissions(getActivity(),
                  new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                  GEOLACATION_REQUEST_PERMISION);
      } else {
          mProvider = mLocationManager.getBestProvider(mCriteria, false);
          mLocationManager.requestLocationUpdates(mProvider, 500, 1, this);
      }
    }
    
    //SABEMOS SI EL USUARIO DIO O NO PERMISOS DE ACCESO A LA GEO-LOCALIZACION
    @Override public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    switch (requestCode) {
        case GEOLACATION_REQUEST_PERMISION: {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the contacts-related task you need to do.
            } else {
                // permission denied, boo! Disable the functionality that depends on this permission.
                Toast.makeText(getActivity(), "Las fotos se guardaran sin geo-posicionamiento", Toast.LENGTH_LONG).show();
            }
            return;
        } // other 'case' lines to check for other permissions this app might request.
    }

+    @Override public void onPause() { super.onPause(); mLocationManager.removeUpdates(this); }
```



### 216. Mostrar el mapa de Google con un Intent
En **ShowPhotoFragment** (donde monstramos una foto) tenemos el boton de 
mostrar mapa en el cual abriremos la app de mapas con la posicion de la foto.   
<!-- lanzar google maps, abrir google maps desde intent google maps, pasar latitud y longitud a mapa-->
```java
btnShowMap.setOnClickListener(new View.OnClickListener() {
  @Override public void onClick(View v) {
      //SACAMOS DE LA BD LA POSICION DE LA FOTO QUE ESTAMOS MOSTRANDO
      double latitude = Double.valueOf(mCursor.getString(mCursor.getColumnIndex(DataManager.TABLE_ROW_LOCATION_LAT)));
      double longitud = Double.valueOf(mCursor.getString(mCursor.getColumnIndex(DataManager.TABLE_ROW_LOCATION_LONG)));

      //LANZAMOS INTENT Y LE PASAMOS LaT Y LONG POT URI
      String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitud);
      Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
      getActivity().startActivity(intent);
  }
});
```

**Resultado:** Podemos guardar una foto y se guardara la geo-posicion y 
podremos ver la ubicacion desde el mapa.   
**Commit:** Uso basico de Mapas y guardar foto con localizacion y mostrar la pos en google maps.   
![52_guardar_foto_con_geoposi_mostrar_en_gmap_pedir_permisos_en_ejecucion](./img/52_guardar_foto_con_geoposi_mostrar_en_gmap_pedir_permisos_en_ejecucion.gif)   
























































<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
----
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

# Master Desarrollo Android con Java y Kotlin [Octubre 2019]
Codigo completo: [**URL**](https://gitlab.com/MarioM51/hola_android) 

### 28. S03-CL13: RecyclerView en FragmentList
**Doc:** https://developer.android.com/guide/topics/ui/layout/recyclerview   
Este componente 

1. En nuestro proyecto de MisTareas en la carpeta app -> clickDer -> new -> Fragment -> Fragment List 
quitamos el "fragment factory" 


2. En nuestro activity layout le agregamos un fragment desde arrastrandolo desde la vista de diseño.   

3. En Activity.java agregamos la implementacion que se creo dentro de nuestro fragment.   

**Resultado:** Con puro codigo auto-generado tenemos la siguiente 
lista usando un RecyclerView.   
![53_recycler_view_lista_autogenerado.gif](./img/53_recycler_view_lista_autogenerado.gif)   




### 29. S03-CL14: RecyclerView: Adapter personalizado (1ª parte)
1. De la lista que creamos arriba vamos a eliminar el DummyContent y lo sustituimos 
por uno persionalizado en mi caso Producto.   

- Si nos fijamos se agregaron dos layouts "fragment_producto.xml" y "fragment_producto_lisr.xml" 
los cuales son la representacion visual de un producto de la lista y la lista en si respectivamtne.   

2. Editamos el layout de item de la lista.


3. Descargamos la libreria de Picaso (https://square.github.io/picasso/#download) que 
nos servira para descargar y mostrar imagenes de internet.   
<!-- uso de picaso, uso de picasso, mostrar imagen de internet, mostrar imagen de la web -->
```java
implementation ('com.squareup.picasso:picasso:2.71828') {
    exclude group: 'com.android.support'
    exclude module: ['exifinterface', 'support-annotations']
}
```

4. Editamos el Adaptador que si nos fijamos en el codigo auto-generado veremos que 
usa el metodo **onBindViewHolder para inflar** la vista la cual le entra una clase 
interna la cual enlaza los elementos de la vista.   
```java
@Override public void onBindViewHolder(final ViewHolder holder, int position) {
  holder.mProducto = mProductos.get(position);
  holder.mNombre.setText(holder.mProducto.getNombre());
  holder.mPrecio.setText(String.valueOf(holder.mProducto.getPrecio()));
  Picasso.get()
    .load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmeCrtkoFuGHkWx4a28zHzFt1HjMZvN7xHrY1CYohR-hmPMQaq&s")
    .resize(400, 100)
    .centerCrop()
    .into(holder.mFoto);
  holder.mValoracion.setRating(holder.mProducto.getValoracion());
  holder.mView.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
          if (null != mListener) { mListener.onListFragmentInteraction(holder.mProducto); }
      }
  });
}

public class ViewHolder extends RecyclerView.ViewHolder {
  public final View mView;
  public final TextView mNombre;
  public final TextView mPrecio;
  public final ImageView mFoto;
  public final RatingBar mValoracion;
  public Producto mProducto;
  public ViewHolder(View view) {
      super(view);
      mView = view;
      mNombre = mView.findViewById(R.id.item_list_poducto_nombre);
      mPrecio = mView.findViewById(R.id.item_list_poducto_precio);
      mFoto = mView.findViewById(R.id.item_list_poducto_img);
      mValoracion = mView.findViewById(R.id.item_list_poducto_valoracion);
  }
}
```

**Resultado:** Podemos ver una lista de items con una vista personalizada 
incluyendo imagen que usamos directamente de internet (gif mas abajo).   
**Commit:** "uso de recyclerview y mostar imagen de internet con Picasso"


### 31. S03-CL16: RecyclerView: mejorando el Look & Feel (Material Design)
<!-- uso de colores, paleta de colores, -->
- Uso de colores segun la guia de material design: https://material.io/design/color/the-color-system.html#color-theme-creation   
  - A partir de que tan oscuro o claro el texto que va ensima del color debe de ser blanco o negro.   

- Heramienta para seleccionar colores: https://material.io/tools/color


1. Al seleccionar los coleres que queremos en tools/color copiamos el color 
primario y lo pegamos en nuestro colors.xml
```xml
<resources>
    <color name="colorPrimary">#b71c1c</color>
    <color name="colorPrimaryDark">#7f0000</color>
    <color name="colorAccent">#1a237e</color>
</resources>
```



### 32. S03-CL17: RecyclerView: haciendo uso de CardView
Doc: https://developer.android.com/guide/topics/ui/layout/cardview   

Usaremos el CardView para pintar cada uno de nuestros elementos y que se distingan 
entre ellos mostrando una sombra.

1. Agregamos un elemento CardView como root, pero conservando la estructura del 
ContrainLayout, como se ve abajo.
```java
<androidx.cardview.widget.CardView >
    <androidx.constraintlayout.widget.ConstraintLayout >
      <ImageView />
      <TextView />
      <TextView />
      <RatingBar />
```

2. Para la aparicion de las sombrar hacemos uso de Elevation.   


**Commit:** "recycler_view_con_cardview_mostrar_img_de_internet_con_picasso"
![54_recycler_view_con_cardview_mostrar_img_de_internet_con_picasso.gif](./img/54_recycler_view_con_cardview_mostrar_img_de_internet_con_picasso.gif)   





























<br /><br /><br /><br /><br /><br />
## Sección  Sección 5: Android Jetpack: Responsive Design
Diseño Responsivo: https://developer.android.com/training/constraint-layout/

Android Jetpack es una nueva coleccion de componentes que a agregado Google 
entre ellos esta el ContrainLayout.







































<br /><br /><br /><br /><br /><br />
## Sección 8: MiniTwitter: Consumir API con Retrofit + Patrones de diseño (Java)
<!-- api minitwitter api, api mini twitter api, api mini-twiter api, api de minitwiter, api para minitwiter  -->
- Usaremos esta api https://www.minitwitter.com:3001/api/#/ que ya nos trae 
el login con JWT y todo lo necesario.   

- codigo minitwitter: https://github.com/Android-Avanzado/androidapp/tree/master/S08-api-minitwitter   


### 69. S08-CL05: Creación app MiniTwitter y recursos
<!-- poner icono a aplicacion poner icono de aplicacion poner icono de la aplicacion, cambiar icono a aplicacion, cambiar icono de aplicacion, cambiar icono de la aplicacion, asignar icono a aplicacion, asignar icono de aplicacion, asignar icono de la aplicacion, poner icono a app, poner icono de app, oner icono de la app,  cambiar icono a app, cambiar icono de app, cambiar icono de la app, asignar icono a app, asignar icono de app asignar icono de la app -->
1. Creamos un proyecto tipo vacio, agregamos un color azul "#0F7EDD" y estos recursos.   


### 70. S08-CL06: Diseño login app MiniTwitter
Creamos la interface de login 

### 71. S08-CL07: Diseño Signup app MiniTwitte
Creamos la pantalla de creacion de cuenta.   


### 72. S08-CL08: Personalizando el Estilo de la app
<!-- ocultar barra superior, ocultar toolbar,  quitar barra superior, quitar toolbar -->
Quitamos/ocultamos el toolbar de arriba cambiando el estilo en "styles.xml" y le damos 
un ```name="AppTheme" parent="Theme.AppCompat.DayNight"``` y en ambos activities ocultamos 
la barra con ```getSupportActionBar().hide();``` y para que el color de los iconos de 
notificaciones propios de android en colors.xml pasamos el "colorPrimaryDark" aun "DDDDDD".    


![1_disenio_mini_twitter](./img/curso2/1_disenio_mini_twitter.gif)   



### 73. S08-CL09: Instalación Librería Retrofit
- Sitio Retrofit: https://square.github.io/retrofit/ (codigo dependencia)   
- Guía de arquitectura de apps:  https://developer.android.com/jetpack/docs/guide?hl=es-419   <!-- arquitectura android -->
- Sitio gson:  https://github.com/google/gson


Es una libreria muy popular incluso mensionada en la documentacion oficial de Android 

En nuestro gradle.app
```
implementation 'com.squareup.retrofit2:retrofit:2.5.0'
implementation 'com.google.code.gson:gson:2.8.6'
implementation 'com.squareup.retrofit2:converter-gson:2.5.0'
```
Sincronizamos el proyecto deberia de aparecernos un dialogo/boton en la parte superior del 
editor de texto   


### 74. S08-CL10: Definición de clases POJO: Login y Registro
extrapolaremos el json que requiere la api a clases de java   

<!-- generar codigo java de json a java, generar pojo de json, generar pojo apartir de json, generar de pojos apartir de json, json a java, json a pojo, Generar el pojo a partir de json-->
1. Vamos a **jsonschema2pojo.org** y pegamos el json que nos pide el login, y para 
que lo genere en la ui-web pondremos Source type: "json", Annotation style "Gson".   


json Para peticion de registro
```json
{
  "username": "mario_marquez",
  "email": "mario@mario.com",
  "password": "mario",
  "code": "UDEMYANDROID"
}
```

java generado para registro
```java
package com.mariocorp.mariotwitter.model.dto;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class SingupRequest {
    @SerializedName("username") @Expose private String username;
    @SerializedName("email") @Expose private String email;
    @SerializedName("password") @Expose private String password;
    @SerializedName("code") @Expose private String code;
    public SingupRequest() { }
    public SingupRequest(String username, String email, String password, String code) {...}
    public String get...set... {...}
}
```

json para login
```json
{
  "email": "mario@mario.com",
  "password": "mario"
}
```

java generado para login
```java
public class LoginRequest {
    @SerializedName("email") @Expose private String email;
    @SerializedName("password") @Expose private String password;
    public LoginRequest() { }
    public LoginRequest(String email, String password) {...}

    public String get...set...
}

```


### 75. S08-CL11: Definición Servicio Retrofit: login y registro
**Objetivo** crear clase que consume la API, notar que al hacer una peticion 
esta se hace de forma asincrona lo cual se abrira un nuevo hilo de ejecucion
```java
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MiniTwitterService {

    @POST("/auth/login")
    Call<LoginResponse> doLogin(@Body LoginRequest loginRequest);

    @POST("/auth/signup")
    Call<LoginResponse> doSingup(@Body SingupRequest singupRequest);

}
```


### 76. S08-CL12: Conexión Retrofit (patrón Singleton)
Crearemos la clase la cual nos conectaremos a traves de esta, lo haremos 
con singleton para estabilidad y rendimiento.   
```java
public class MiniTwitterClient {
    private static MiniTwitterClient instance = null;
    private MiniTwitterService miniTwitterService = null;
    private Retrofit retrofit;

    private MiniTwitterClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Const.API_MINI_TWITTER_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        miniTwitterService = retrofit.create(MiniTwitterService.class);
    }

    public static MiniTwitterClient getInstance() {
        if(instance == null) { instance = new MiniTwitterClient(); }
        return instance;
    }

    public MiniTwitterService getMiniTwitterService() {
        return miniTwitterService;
    }
}
```

2. Igual podemos crear un singleto para el context, para obtenerlo de forma facil
```java
public class MyApp extends Application {
    private static MyApp instance;
    private MyApp() { }
    @Override public void onCreate() { instance = this; super.onCreate(); }
    public MyApp getInstance() { return instance; }
}
```

2.1. En el manifest agregamos lo siguiente para tener el context en esta
```xml
<application
  android:name=".common.MyApp"
```

3. Agregar logs retrofit <!-- agregar logs de retrofit, agregar logs retrofit mostrar logs de retrofit, mostrar logs retrofit logs, vey los logs de retrofit, ver logs retrofit logs, retrofit ver logs -->
<!-- fuente: https://futurestud.io/tutorials/retrofit-2-log-requests-and-responses -->
3.1. agregamos la dependencia que los capturara
```
implementation 'com.squareup.okhttp3:logging-interceptor:3.8.0'
```

2.2. En el contructor de nuestra conexion agregamos
```
HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
logging.setLevel(HttpLoggingInterceptor.Level.BODY);
OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
httpClient.addInterceptor(logging);// REMEMBER: add logging as last interceptor

retrofit = new Retrofit.Builder()...
	.client(httpClient.build())
```

2.3. DEberia de mostrar los siguientes logs en mi caso, talvez algo mas
```
D/OkHttp: --> POST https://www.minitwitter.com:3001/apiv1/auth/login http/1.1
D/OkHttp: Content-Type: application/json; charset=UTF-8
D/OkHttp: Content-Length: 39
D/OkHttp: {"email":"algo@algo","password":"algo"}
D/OkHttp: --> END POST (39-byte body)
D/OkHttp: <-- 400 Bad Request https://www.minitwitter.com:3001/apiv1/auth/login (1164ms)
D/OkHttp: X-Powered-By: Express
D/OkHttp: Access-Control-Allow-Origin: *
D/OkHttp: Content-Type: application/json; charset=utf-8
D/OkHttp: Content-Length: 35
D/OkHttp: ETag: W/"23-QerYfq8HhRpHWq3rg95O9+cJ6Yg"
D/OkHttp: Date: Fri, 22 Nov 2019 06:57:25 GMT
D/OkHttp: Connection: keep-alive
D/OkHttp: {"messsage":"API_AUTH_LOGIN_ERROR"}
D/OkHttp: <-- END HTTP (35-byte body)
```


### 77. S08-CL13: Implementación petición POST Login
**Objetivo** implementaremos la llamada al servicio de login

1. agregar permisos para acceder a internet, agregar permisos internet permisos
```xml
<uses-permission android:name="android.permission.INTERNET" />
```

2. Creamos Activity del tipo "Botton Navigation Activity", para que si es 
exitoso el login nos mande 

3. Implementamos logica de peticion y pase si es exitosa o informar error.   
  2.1. En LoginActivity vinculamos los EditText de email y pass.   
  2.2. Traemos el Cliente y el Service para poder hacer la peticion.   
  2.3. Al pulsar el boton se capturara la pass y email y se enviara la peticion.   

<!-- file://E:\cursos\_2019\img\android\curso2\2_peticion_login_mainActivity.java -->
- [2_peticion_login_mainActivity.java](./img/curso2/2_peticion_login_mainActivity.java)   
```
// CODIGO COMPLETO EN ARCHIVO
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView btToSingup;
    private EditText editTextEmail, editTextPassword;
    private Button btnDoLogin;
    private MiniTwitterClient miniTwitterClient;
    private MiniTwitterService miniTwitterService;
	
	private void retrofitInit() {
        miniTwitterClient = MiniTwitterClient.getInstance();
        miniTwitterService = miniTwitterClient.getMiniTwitterService();
    }
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        this.retrofitInit();
        setuptUI();
    }
	
	@Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin: this.doLogin(); break;
        }
    }
	
	private void doLogin() {
        String inputEmail =  editTextEmail.getText().toString();
        String inputPass =  editTextPassword.getText().toString();
        Log.i(TAG, String.format("email: %s | pass: %s", inputEmail, inputPass));

        if(inputEmail.isEmpty()) { editTextEmail.setError("El email es requerido");       return ; }
        if(inputPass.isEmpty()) {  editTextPassword.setError("El password es requerido"); return ; }

        LoginRequest loginRequest = new LoginRequest(inputEmail, inputPass);
        Call<LoginResponse> call = miniTwitterService.doLogin(loginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {//esta en el rango del 200
                    Intent i = new Intent(MainActivity.this, DashnoardActivity.class);
                    startActivity(i);
                    finish(); // lo terminamos ya que el usuario no requerira volver al login
// NO OLVIDAR QUE LOS TOAST SE CREAN Y SE MUSTRAN CON SHOW
				} else { Toast.makeText(MainActivity.this, "Reviza los datos", Toast.LENGTH_LONG).show(); }
            }
            @Override public void onFailure(Call<LoginResponse> call, Throwable t) { Toast.makeText(MainActivity.this, "Problemas de conexion", Toast.LENGTH_LONG).show(); }
        });
    }
```


**Resultado:** Al poner los datos y darle en enviar, si esta correcto 
nos deberia de mandar al dashboar, de lo contrario un mensaje de verifica 
tus datos.   
![3_login_basico](./img/curso2/3_login_basico.gif)   



### 78. S08-CL14: Implementación petición POST Registro
Lo hacemos igual que el login, solo que mandando la peticion con el 
cuerpo correspondiente de registro






### 79. S08-CL15: SharedPreferences: almacenamiento del Auth Token
<!-- guardar token guardar, guardado de token guardado, jwt guardar jwt, guardado de jwt guardado en SharedPreferences -->
- Dodumentacion shared preference: https://developer.android.com/training/data-storage/shared-preferences   

1. Creamos un archivo para guardar y recuperar de sharedpreferences
```
public class SharedPreferencesManager {
    private static final String APP_SETTINGS_FILE = "APP_SETTINGS";
    
    private static SharedPreferences getSharedPreferences() {
        return MyApp.getContext().getSharedPreferences(APP_SETTINGS_FILE, Context.MODE_PRIVATE);
    }

    public static void saveString(String property, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(property, value);
        editor.commit();
    }
	
	public static String getsomeString(final String property) {
        return getSharedPreferences().getString(property, null);
    }
}
```

2. En MainActivity cuando el login es exitoso guardamos el token y los datos del usuario.   
```
if(response.isSuccessful()) {//esta en el rango del 200
  SharedPreferencesManager.saveString(Const.TOKEN, response.body().getToken());
```


3. Comprobamos en nuestro DashnoardActivity que podemos recuperar 
los datos guardados
```
String token = SharedPreferencesManager.getsomeString(Const.TOKEN);
Log.i(TAG, "Token: " + token);
```


**RESULTADO:** Al iniciar secion y pasar el DashnoardActivity nos mostrara el 
token que guardamos en sharedpreferences, el cual usaremos en las peticiones 
privadas de la API.   



### 80. S08-CL16: SharedPreferences + Interceptor: obtención del Auth Token
**PROBLEMA:** Para todas aquellas peticiones que requieran del token seria lioso 
andar agregando una por una de las peticiones privadas el token, **SOLUCION** haremos 
un interceptor que detectara las peticiones que requieran token y de los agregara, esto 
en un mismo sitio, lo cual esta mejor.   


1. Crear interceptor para agregar la cabecera con el token.   
```
import java.io.IOException; import okhttp3.Interceptor; import okhttp3.Request; import okhttp3.Response;
public class AuthInterceptor implements Interceptor {
    @Override public Response intercept(Chain chain) throws IOException {
        String token = SharedPreferencesManager.getsomeString(Const.TOKEN);
        String propertie = "Authorization";
        String value = "Bearer " + token;
        Request request = chain.request().newBuilder().addHeader(propertie, value).build();
        return chain.proceed(request);
    }
}
```
En futuro usaremos esta para agregarlo



### 81. S08-CL17: Toolbar personalizado
**Objetivo:** Para imitar a twitter este tiene un toolbar un icono con 
la foto del usuario que inicio sesion, para esto crearemos un toolbar 
personalizado.   

Para rapido crearemos un "basic activity" el cual ya cuenta con un toolbar 
personalizado y un boton flotante, el cual queremos la misma extructura, con el 
plus del BottonNavigationView, podemos sustituir el layout del "basic activity" 
dentro del "dashboard" dejando el BottonNavigation, lo que nos quedaria algo asi.   

- [4_dashboard_style.xml](./img/curso2/4_dashboard_style.xml)   <!-- file://E:\cursos\_2019\img\android\curso2\4_dashboard_style.xml -->



### 82.83 S08-CL18--CL19: TweeListFragment + Definición POJO Tweet + Diseño del TweetList Adapter 
**Objetivo** Crearemos la lista de tweets

<!-- usuario con info, usuario con tweets, usuarios con info, usuarios con tweets, { "email": "info@minitwitter.com", "password": "1234" } -->

1. Con ayuda del json generamos los pojos necesarios
```java
public class Like {
    @SerializedName("id") @Expose private Integer         id;
    @SerializedName("username") @Expose private String    username;
    @SerializedName("descripcion") @Expose private String descripcion;
    @SerializedName("website") @Expose private String     website;
    @SerializedName("photoUrl") @Expose private String    photoUrl;
    @SerializedName("created") @Expose private String     created;
```

```java
public class Tweet {
    @SerializedName("id") @Expose private Integer        id;
    @SerializedName("mensaje")@Expose private String     mensaje;
    @SerializedName("likes") @Expose private List<Like>  likes = null;
    @SerializedName("user") @Expose private User         user;
```

```java
public class User {
    @SerializedName("id") @Expose private Integer         id;
    @SerializedName("username") @Expose private String    username;
    @SerializedName("descripcion") @Expose private String descripcion;
    @SerializedName("website") @Expose private String     website;
    @SerializedName("photoUrl") @Expose private String    photoUrl;
    @SerializedName("created") @Expose private String     created;
```

1. Creamos un FragmentList, el cual crea un *fragment_tweets.xml* (diseño de un item), 
*fragment_tweets_list.xml* diseño de la lista, *MyTweetRecyclerViewAdapter.java* para 
obtener y manipular los datos y *TweetsFragment* para inflar.

<!-- crear imagenes redondas, hacer imagenes redondas, mostrar imagenes redondas, añadir imagenes redondas, pasar imagenr cuadrada a redonda, convertir imagen cuadrada a redonda -->
3. Añadimos libreria que nos ayudara a hacer nuestrar imagenes redondas.
```
//fuente: https://github.com/hdodenhof/CircleImageView
implementation 'de.hdodenhof:circleimageview:3.0.1'
```

4. Añadimos libreria que nos ayudara a cargar una imagen u otra dependiendo si 
nosotros le hemos dado like a esta imagen.   
```
// en el otro gradle le agregamos el repo de maven con "mavenCentral()"
implementation 'com.github.bumptech.glide:glide:4.10.0'
annotationProcessor 'com.github.bumptech.glide:compiler:4.10.0'
```


2. en *fragment_tweets.xml* diseñamos como queremos que se visualicen nuestros tweets
2.1. A nuestra imagen la pasamos de ImageView a **de.hdodenhof.circleimageview.CircleImageView**
![4_dashboard_style.xml](./img/curso2/5_disenio_tweets_item.PNG)   <!-- file://E:\cursos\_2019\img\android\curso2\5_disenio_tweets_item.PNG -->



4. En *MyTweetRecyclerViewAdapter* unimos el disenio con nuestro adapter.   
	4.1. En *ViewHolder* Agregando los elementos que creamos a este
	4.2. En *onBindViewHolder* 
```java
public class MyTweetRecyclerViewAdapter extends RecyclerView.Adapter<MyTweetRecyclerViewAdapter.ViewHolder> {
    private List<Tweet> mValues;
    private Context ctx;
    private final String username = SharedPreferencesManager.getsomeString(Const.USER_USERNAME);

    public MyTweetRecyclerViewAdapter(Context context, List<Tweet> items) {
        mValues = items;
        ctx = context;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_tweets, parent, false);
        return new ViewHolder(view);
    }

    @Override public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
	//RELLENAMOS ELEMENTOS DE LA VISTA
        holder.userName.setText(holder.mItem.getUser().getUsername());
        holder.text.setText(holder.mItem.getMensaje());
        holder.numLikss.setText(holder.mItem.getLikes().size());
    //CARGAMOS LA FOTO DE USUARIO DE INTERNET
        String photo = holder.mItem.getUser().getPhotoUrl();
        if(!photo.equals("")) {
            String url = Const.API_MINI_TWITTER_BASE + "uploads/photos/" + photo;
            Glide.with(ctx).load(url).into(holder.userPhoto);
        }
    //BUSCAMOS SI EL USUARIO LE DIO LIKE, PARA PINTAR EL ICONO DE LIKE Y EL NUMERO DE LIKES VACIO Y/O ROSA
        for(Like like: holder.mItem.getLikes()) {
            if(like.getUsername().equals(username)) {
                Glide.with(ctx).load(R.drawable.ic_like_pink).into(holder.icLike);
                holder.numLikss.setTextColor(ctx.getResources().getColor(R.color.colorRosa));
                holder.numLikss.setTypeface(null, Typeface.BOLD);
                break;
            }
        }
    }

    @Override public int getItemCount() { return mValues.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public TextView text, userName, numLikss; public ImageView userPhoto, icLike;
        public Tweet mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
		//LIGAMOS ELEMENTOS DE LA VISTA
            userPhoto = view.findViewById(R.id.imageView_tweet_photo_user);
            icLike = view.findViewById(R.id.imageView_tweet_like); userName = view.findViewById(R.id.textView_tweet_user_name); text = view.findViewById(R.id.textView_tweet_text); numLikss = view.findViewById(R.id.textView_tweet_num_likes);
        }
    }
}
```


### 84. S08-CL20: Implementación de petición GET: listado de Tweets
Ya tenemos el disenio y el adapter ahora vamos por detalles y la peticion

1. Creamos *MiniTwitterClientAuth* que es lo mismo que MiniTwitterClient solo que este 
tendra un interseptor para agregar la cabecera que a;adimos arriba.   
  1.1. Creamos *MiniTwitterServiceAuth* donde tenemos la peticion ``` @GET("tweets/all") Call<List<Tweet>> getAllTweets();```
```java
public class MiniTwitterClientAuth {
    public MiniTwitterClientAuth() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        //INTERCEPTOR PARA AGREGAR EL TOKEN EN HEADER
        httpClient.addInterceptor(new AuthInterceptor());
```

2. En *TweetsFragment* hacemos la peticion al servidor y la pasamos al adaptador, lo 

```java
public class TweetsFragment extends Fragment {
    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private RecyclerView recyclerView;
    private MyTweetRecyclerViewAdapter adapter;
    private List<Tweet> tweetList = new ArrayList<Tweet>();
    private MiniTwitterClientAuth miniTwitterClientAuth;
    private MiniTwitterServiceAuth miniTwitterServiceAuth;
	
    public TweetsFragment() { }
    public static TweetsFragment newInstance(int columnCount) {... }
    @Override public void onCreate(Bundle savedInstanceState) {...}
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {...}
    private void retrofitInit() { miniTwitterClientAuth = MiniTwitterClientAuth.getInstance(); miniTwitterServiceAuth = miniTwitterClientAuth.getMiniTwitterServiceAuth(); }

    private void loadTweets() {
        retrofitInit();
+       Call<List<Tweet>> call = miniTwitterServiceAuth.getAllTweets();
        call.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                if(response.isSuccessful()) {
        //PASAMOS LOS TWEETS QUE LLGARON DEL SERVIDOR AL ADAPTADOR
+                   tweetList = response.body();
+                   adapter = new MyTweetRecyclerViewAdapter(getActivity(), tweetList);
+                   recyclerView.setAdapter(adapter);

                } else {
                    Toast.makeText(getActivity(), "Error de peticion", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<List<Tweet>> call, Throwable t) {
                Toast.makeText(getActivity(), "Error de conexion", Toast.LENGTH_LONG).show();
            }
        });
    }
}
```
3. Cargamos el fragmento en el dashboar
```java
public class DashnoardActivity extends AppCompatActivity {
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCre...
+       getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new TweetsFragment()).commit();
    }
```


**RESULTADO:** Al hacer login pasaremos al dashboar se cargara el 
Fragment List que muestra todos los tweets, y este los obtendra por 
una peticion, y se los mandara al adapter que los pintara.   
**Commit:** *commit init y mostrar todos los tweets* [enlace](https://gitlab.com/MarioM51/hola_android/tree/master/MarioTwitter)   
![6_lista_all_tweets](./img/curso2/6_lista_all_tweets.gif)   







### 85. S08-CL21: Patrón de diseño Android Jetpack: Repository
Segun la documentacion de *arquitectura android* oficial, nos indica que accedamos a los 
datos por medio de repositorio.

![7_arquitectura_android_peticiones_servidor](./img/curso2/7_arquitectura_android_peticiones_servidor.PNG)   


1. Creamos **TweetRepository** el cual cargara todos los tweets pero en un wrapper 
Live data que nos ayudara a mostrar los cambios en tanto ocurran.
```java
public class TweetRepository {
    private MiniTwitterClientAuth miniTwitterClientAuth;
    private MiniTwitterServiceAuth miniTwitterServiceAuth;
    LiveData<List<Tweet>> allTeetsLiveData;

    public TweetRepository() {
        retrofitInit();
        allTeetsLiveData = getAllTeetsLiveData();
    }

    private void retrofitInit() {
        miniTwitterClientAuth = MiniTwitterClientAuth.getInstance();
        miniTwitterServiceAuth = miniTwitterClientAuth.getMiniTwitterServiceAuth();
    }

    public LiveData<List<Tweet>> getAllTeetsLiveData() {
        final MutableLiveData<List<Tweet>> data = new MutableLiveData<>();
        Call<List<Tweet>> call = miniTwitterServiceAuth.getAllTweets();
        call.enqueue(new Callback<List<Tweet>>() {
            @Override public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                if(response.isSuccessful()) {
                    //PASAMOS LOS TWEETS QUE LLGARON DEL SERVIDOR AL ADAPTADOR
                    data.setValue(response.body());
                } else {
                    Log.e(TAG, "Error de peticion");
                }
            }

            @Override public void onFailure(Call<List<Tweet>> call, Throwable t) {
                Log.e(TAG, "Error de conexion");
            }
        });
        return data;
    }
}
```


### 86. S08-CL22: Patrón de diseño Android Jetpack: ViewModel
Agregamos dependencias en mi caso no los use pero por las moscas los dejo
```
implementation "android.arch.lifecycle:extensions:$archLifecycleVersion"
annotationProcessor "android.arch.lifecycle:compiler:$archLifecycleVersion"
```


1. Creamos el intermediario entre la vista y el repositorio.   
```java
public class TweetViewModel extends AndroidViewModel {
    private TweetRepository tweetRepository;
    private LiveData<List<Tweet>> tweetsLiveData;

    public TweetViewModel(@NonNull Application application) {
        super(application);
		tweetRepository = new TweetRepository();
        tweetsLiveData = tweetRepository.getAllTeetsLiveData();
    }

    public LiveData<List<Tweet>> getTweetsLiveData() {
        return tweetsLiveData;
    }
}
```

3. Agregamos un metodo en *MyTweetRecyclerViewAdapter* para que se entere que 
ubo cambios.  
```java

@Override
public void onBindViewHolder(final ViewHolder holder, int position) {
if(mValues == null) {
	return ; //si NO hay datos no tendremos por que mostrar algo }
...
}

@Override public int getItemCount() { if(mValues == null) { return 0; } return mValues.size(); }

public void setData(List<Tweet> tweets) {
	mValues = tweets;
	notifyDataSetChanged();
}
```


2. En **TweetsFragment** quitamos la peticion y la sustituimos por el TweetViewModel.   
  2.1. Agregamos un condicional para que si no ahi tweets no truene la app.   
```java
private void loadTweets() {
	tweetViewModel.getTweetsLiveData().observe(getActivity(), new Observer<List<Tweet>>() {
		@Override
		public void onChanged(List<Tweet> tweetListUpdate) {
			tweetList = tweetListUpdate;
			//indicamos al adapter que ahi un cambio
			adapter.setData(tweetList);
		}
	});
	adapter = new MyTweetRecyclerViewAdapter(getActivity(), tweetList);
	recyclerView.setAdapter(adapter);
}
```

**Resultado:** El mismo solo que haora tenemos mejor arquitectura.   
**Commit:** "cambio de arquitectura para obtener todos los tweets".    



### 87. S08-CL23: DialogFragment: nuevo tweet
<!--  http://shapes.softartstudio.com/ generar estylo de botones, generar botones personalizados, crear estylo de botones, crear botones personalizados,  cambiar el estilo de un boton, modificar apariencia de un boton, personalizar boton, crear boton personalizado -->
1. Generamos el dialogo que mostrara el formulario de agrgar tweet, el cual 
tendra un boton personalizado y un estilo del dialog personalizado.   
	1.1. Creamos el estilo del dialog en *v23styles.xml*
```xml
...</style>

<style name="FullScreenDialogStyle" parent="Theme.AppCompat.Dialog">
	<item name="colorPrimary">@color/colorPrimary</item>
	<item name="colorPrimaryDark">@color/colorPrimaryDark</item>
	<item name="colorAccent">@color/colorPrimary</item>
	<!-- estilos para pantalla completa -->
	<item name="android:windowNoTitle">true</item>
	<item name="android:windowFullscreen">false</item> <!-- para que no desaparesca la barra de bateria, red etc-->
	<item name="android:windowIsFloating">false</item> <!-- para que ocupe all el alto y cho de la pantallla-->
	<item name="android:windowBackground">@android:color/white</item>
</style>
```

2. Creamos el formulario que se compone de su xml y de su DialogFragment

3. mostrar dialog al pulsar boton
```java
btnToAddTweet = findViewById(R.id.btn_to_add_tweet);
btnToAddTweet.setOnClickListener(new View.OnClickListener() {
	@Override public void onClick(View v) {
		AddTweetFormDialogFragment dialog = new AddTweetFormDialogFragment();
		dialog.show(getSupportFragmentManager(), "AddTweetFormDialogFragment");
	}
});
```
**Resultado:** Solo se muestra el formulario falta agregarle la logica para que 
lo agrege por medio de la api.   
![8_dialog_pantalla_completa_sin_ocultar_barra_bateria_form_agregar_tweet](./img/curso2/8_dialog_pantalla_completa_sin_ocultar_barra_bateria_form_agregar_tweet.gif)   


### 88.89. S08-CL24: Implementación petición POST Nuevo Tweet - CL25 DialogFragment: llamada a petición POST Nuevo Tweet
1. Creamos dto para enviar la peticion
```java
public class AddTweetRequest {
    @SerializedName("mensaje") @Expose private String mensaje;
	...
```

2. Agregamos el metodo de la peticion
```java
public interface MiniTwitterServiceAuth {
	...
    @POST("tweets/create")
    Call<Tweet> createTweet(@Body AddTweetRequest addTweetRequest);
```

3. En **TweetRepository** agregamos el metodo que hara la peticion
```java
public void createTweet(String msj) {
	CreateTweetRequest createTweetRequest = new CreateTweetRequest(msj);
	Call<Tweet> tweetCall = miniTwitterServiceAuth.createTweet(createTweetRequest);
	tweetCall.enqueue(new Callback<Tweet>() {
		@Override public void onResponse(Call<Tweet> call, Response<Tweet> response) {
			if(response.isSuccessful()){
				Tweet tweetCreated = response.body();
				List<Tweet> listaClonada = new ArrayList<>();
				listaClonada.add(tweetCreated);
				for (int i = 0; i < allTeetsLiveData.getValue().size(); i++) {
					listaClonada.add(new Tweet(allTeetsLiveData.getValue().get(i)));
				}
				allTeetsLiveData.setValue(listaClonada);
			} else { Toast.makeText(MyApp.getContext(), "Error, Checa tu peticion", Toast.LENGTH_LONG).show(); }
		}
		@Override public void onFailure(Call<Tweet> call, Throwable t) { Toast.makeText(MyApp.getContext(), "Error de conexion", Toast.LENGTH_LONG).show(); }
	});
}
```

4. En **TweetViewModel** Agregamos el metodo anteriror para que lo llame la vista
```java
public void doCreateTweet(String mensaje) {
	tweetRepository.createTweet(mensaje);
}
```

<!--       CL25       -->

5. En **AddTweetFormDialogFragment**   
  5.1. Relacionamos el boton y el metodo de crear tweet.   
  5.1. Creamos Dialogo de confirmacion de ya no tweetear y de cerrarlo directamente si el mensaje esta vacio.   
```java
public class AddTweetFormDialogFragment extends DialogFragment implements View.OnClickListener {
    ImageView btnClose, imgPhotoUser;
    Button btnDoCreateTweet;
    EditText tweetMesaje;
	
    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Nullable @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.add_tweet_form_dialog_fragment, container, false);
        btnClose = v.findViewById(R.id.imageView_close_form_add_tweet);
        btnClose.setOnClickListener(this);
        btnDoCreateTweet = v.findViewById(R.id.btn_do_twittear);
        btnDoCreateTweet.setOnClickListener(this);
        imgPhotoUser = v.findViewById(R.id.imageView_form_add_tweet_photo_user);
        String photoUrl = SharedPreferencesManager.getsomeString(Const.USER_PHOTOURL);
        if(!photoUrl.isEmpty()) { Glide.with(getActivity()).load(Const.API_GET_PHOTO_USER + photoUrl).into(imgPhotoUser); }
        tweetMesaje = v.findViewById(R.id.editText_add_tweet_msj);
        return v;
    }

    @Override public void onClick(View v) {
        String msj = tweetMesaje.getText().toString();
        switch (v.getId()) {
            case R.id.btn_do_twittear:
                if(msj.isEmpty()) { tweetMesaje.setError("El Tweet no puede estar vacio"); return ; }
                TweetViewModel tweetViewModel = ViewModelProviders.of(getActivity()).get(TweetViewModel.class);
                tweetViewModel.doCreateTweet(msj);
                getDialog().dismiss();
            break;

            case R.id.imageView_close_form_add_tweet:
                if(!msj.isEmpty()) { showDialogCloseConfirm(); }
                else {               getDialog().dismiss();    }
            break;
        }
    }

    private void showDialogCloseConfirm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Realmente quieres cancelar el tweet?").setTitle("No publicar Tweet");
        builder.setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
    //CERRAMOS EL DILOGO DE CONFIRMACION Y DE CREAR TWEET
            dialog.dismiss();
            getDialog().dismiss();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // CUIDADO getDialog().dismiss();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

```

**ERROR:** Tuve descuido a la hora de clonar el tweet, donde 
tome el getUser de la clase y no del objeto que me llegaba por el 
constructor.   
```java
public class Tweet { ...
  public Tweet(Tweet tweet) { ...
    //CUIDADO this.user = getUser();
    this.user = tweet.getUser();
  }
```

**ERROR** Tuve un error en **TweetRepository.getAllTeetsLiveData()** el que 
estaba creando una instancia local y no de clase, lo cual hacia que no dectectara 
el cambio, ya que al clonarla usaba la de clase y el observer estaba en el la local.   
```java
public class TweetRepository {
  private MutableLiveData<List<Tweet>> allTeetsLiveData;

	public MutableLiveData<List<Tweet>> getAllTeetsLiveData() {
//  CUIDADO final MutableLiveData<List<Tweet>> allTeetsLiveData = new MutableLiveData<>();
		Call<List<Tweet>> call = miniTwitterServiceAuth.getAllTweets();
		call.enqueue(new Callback<List<Tweet>>() {...
```

**RESULTADO:** Podemos agregar un nuevo tweet al servidor y este se mostrara al inicio de 
la lista de todos los tweets, y un correcto manejo a la hora de cancelar el tweet para que 
el usuario mo lo cierre por error.   
![9_creacion_de_tweet_en_api_y_mostrada_en_ui](./img/curso2/9_creacion_de_tweet_en_api_y_mostrada_en_ui.gif)   
Se puede ver como **1)** al darle cerrar la crecion del tweet y el mensaje esta vacio se cierra 
sin preguntar, **2)** y cuando el mensaje tiene algo pregunta por confirmacion, **3)** que cuando se 
crea el tweet este se muestra al inicio de la lista, **4)** cuando volvemos a entrar a la app nuestro 
nuevo tweet aun esta al incio de la lista, ya que consulto de nuevo al serviror.   



<br /><br /><br />
### 91. S08-CL27: Swipe-to-Refresh en la lista de Tweets
- Doc: https://developer.android.com/training/swipe/respond-refresh-request

**Objetivo:** Vamos a implementar que cuando hagamos scroll hacia arriba estando 
arriba se refresquen haga otra peticion para refrescar los tweets, tal como lo 
implementan otras aplicaciones como twitter. En la documentacion de android nos dice como hacerlo.      


1. En **fragment_tweets_list.xml** que el el diseño de la lista de tweets vamos a 
envolver nuestro el ResiclerView por SwipeRefreshLayout
```xml
<androidx.swiperefreshlayout.widget.SwipeRefreshLayout
    android:id="@+id/list_swiper_refresh_layout"
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:name="com.mariocorp.mariotwitter.TweetsFragment"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".ui.fragment.TweetsFragment" >
	
    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/list"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_marginLeft="16dp"
        android:layout_marginRight="16dp"
        app:layoutManager="LinearLayoutManager"
        tools:listitem="@layout/fragment_tweets"
    />
</androidx.swiperefreshlayout.widget.SwipeRefreshLayout>
```

2. En **TweetViewModel** Agregamos metodo que hara la peticion al 
servidor, ya que actualmente peticionamos por medio de este constructor.   
```java
public LiveData<List<Tweet>> getNewTweetsLiveData() {
	tweetsLiveData = tweetRepository.getAllTeetsLiveData();
	return tweetsLiveData;
}
```


3. En **TweetsFragment** en onCreateView ya no se cumplira nuestra 
condicion de "view instanceof RecyclerView" por lo que la quitamos.   
	3.1. Agregamos la referencia y el evento de este gesto.   
	3.2. Agregamos un metodo que hara la peticion ya que sera muy parecido al de 
	obtener todos los tweets que obtubimos en el constructor.   
	3.3. Usamos el **setRefreshing** para idicar cuando se empezo/termino de refrescar.   
	3.4. cambiar icono swipe-refresh con **setOnRefreshListener**   
```java
public class TweetsFragment extends Fragment {
    private SwipeRefreshLayout swipeRefreshLayout;
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tweets_list, container, false);
        //if (view instanceof RecyclerView) {
        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.list);  //recyclerView = (RecyclerView) view;
        swipeRefreshLayout = view.findViewById(R.id.list_swiper_refresh_layout);
+		    swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAzul));
+       swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
+               loadTweets();
            } });
		... return view;
    }
	
	private void doRequestTweets() {
+       tweetViewModel.getNewTweetsLiveData().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweetListUpdate) {
                tweetList = tweetListUpdate;
                //indicamos al adapter que ahi un cambio
                adapter.setData(tweetList);
+               swipeRefreshLayout.setRefreshing(false);
                //EVITAMOS QUE SE DISPAREN 2 OBSERVERS QUITANDO ESTE OBSERVER PARA QUE CUANDO EJECUTEMOS EL NEW TWEET NO PASE ESTO
+               tweetViewModel.getNewTweetsLiveData().removeObserver(this);
            }
        });
    }
```

**Resultado:** Si agregamos un tweet desde afuera por ejemplo desde swagger y 
le damos refrescar deberiamos de poder ver el nuevo tweet.   
**Commit:** "formulario y peticion de agregar tweet y refrescar lista de tweets al agregar tweet o hacer swipe-refresh".   
![10_swipe_refresh_de-tweets.gif](./img/curso2/10_swipe_refresh_de-tweets.gif)   
Podemos ver, 1) estamos dentro de la app y esta el el tweet "3", 2) creamos un nuevo tweet "el 4" en 
Swagger, 3) hacemos swipe-refresh y se mos muestra el el tweet "4".   




<br /><br /><br />
### 92. S08-CL28: Petición POST: like en tweet
**Objetivo** Agregaremos la peticion de poner me gusta en un tweet el cual 
podremos hacerlo con la api "/tweets/like/{id}" el cual nos servira para agregar 
el tweet señalado como me gusta, o si ya esta marcado quitarlo.   


1. Agregamos la peticion en **MiniTwitterServiceAuth** 
<!-- enviar parametro por url, enviar variable por url, enviar argumento por url con retrofit-->
```java
@POST("tweets/like/{tweet_id}")
Call<Tweet> likeTweet(@Path("tweet_id") int tweetId);
```

2. En **TweetRepository** agregamos metodo de like
```java
public void likeTweet(final int idTweet) {
+	Call<Tweet> tweetCall = miniTwitterServiceAuth.likeTweet(idTweet);
	tweetCall.enqueue(new Callback<Tweet>() {
		@Override public void onResponse(Call<Tweet> call, Response<Tweet> response) {
			if(response.isSuccessful()){
				List<Tweet> listaClonada = new ArrayList<>();
				for (int i = 0; i < allTeetsLiveData.getValue().size(); i++) {
					if(allTeetsLiveData.getValue().get(i).getId() == idTweet)  {
						Tweet tweetLiked = response.body();
+						listaClonada.add(tweetLiked);
					} else {
						Tweet tweetClon = allTeetsLiveData.getValue().get(i);
						listaClonada.add(tweetClon);
					}
				}
				allTeetsLiveData.setValue(listaClonada);//AVISAMOS DE CAMBIO AL UI
	...});
}
```

3. En **TweetViewModel** consumimos el metodo que acabamos de crear para 
que funcione de intermediario.   
```java
public void doLikeTweet(int idTweet) {
	tweetRepository.likeTweet(idTweet);
}
```

4. En **MyTweetRecyclerViewAdapter** detectamos cuando se haga like 
```java
public class MyTweetRecyclerViewAdapter extends RecyclerView.Adapter<MyTweetRecyclerViewAdapter.ViewHolder> {
    private TweetViewModel tweetViewModel;
	  public MyTweetRecyclerViewAdapter(Context context, List<Tweet> items) { 
		mValues = items; ctx = context;
+		tweetViewModel = ViewModelProviders.of((FragmentActivity) ctx).get(TweetViewModel.class);
	}
	
	@Override public void onBindViewHolder(final ViewHolder holder, int position) {
		...
		holder.icLike.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v) {
+				tweetViewModel.doLikeTweet(holder.mItem.getId());
			}
		});
	}
}
```

**Resultado**   
![11_like_tweet](./img/curso2/11_like_tweet.gif)   




### 93. S08-CL29: FragmentList Tweets favoritos
<!-- aplicar mvvm arquitectura mvvm -->
**Objetivo:** Crear la vista de tweet favoritos y como usamos fragments 
para mostrar la lista de todos los tweets podremos reutilizar esta lista 
ya que es lo mismo, solo filtramos los tweets.   


1. Comunicacion entre fragments
  1.1. En **Const** agregamos el argumento, que nos servira para saber si 
  mostrar la lista con todos los tweets o solo la de tweets favoritos.   
```java
//argumnetos
public static final String TWEET_LIST_TYPE = "TWEET_LIST_TYPE";
public static final int TWEET_LIST_ALL = 1;
public static final int TWEET_LIST_FAVS = 2;
```

1.2. En **TweetsFragment** quitamos ARG_COLUMN_COUNT que se creo automaticamente 
ya que estaba planteada para mostrar la lista de forma vertical u horizontal y la 
cambiamos por las que creamos.
```java
public class TweetsFragment extends Fragment {...
    private int mTweetListType = 1;
    
    public static TweetsFragment newInstance(int tweetListType) {...
        args.putInt(Const.TWEET_LIST_TYPE, tweetListType);...
    }
    
    @Override public View onCreateView(Lay...
-      //if (mColumnCount <= 1) { recyclerView.setLayoutManager(new LinearLayoutManager(context)); }
-      //else { recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount)); }
+     recyclerView.setLayoutManager(new LinearLayoutManager(context));
```

1.3. En **DashnoardActivity** Sustituimos la carga directa de todos los tweets por la de newinstance 
indicando que queremos la de todos
1.4. y agregamos el evento del menu inferior de navegacion
```
public class DashnoardActivity extends AppCompatActivity {
private BottomNavigationView navView;
    
@Override protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  getSupportActionBar().hide();
  setContentView(R.layout.activity_dashnoard);
  navView = findViewById(R.id.nav_view);
  navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
  //AGREGAMOS EVENTO DEL MENU DE NAVEGACION INFERIOR
  btnToAddTweet = findViewById(R.id.btn_to_add_tweet);
  btnToAddTweet.setOnClickListener(new View.OnClickListener() {
    @Override public void onClick(View v) {
      AddTweetFormDialogFragment dialog = new AddTweetFormDialogFragment();
      dialog.show(getSupportFragmentManager(), "AddTweetFormDialogFragment");
    }
  });
  getSupportFragmentManager().beginTransaction()
    .add(R.id.fragment_container, TweetsFragment.newInstance(Const.TWEET_LIST_ALL))
    .commit();
}

private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
  @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    Fragment f = null;
    switch (item.getItemId()) {
      case R.id.navigation_home:
        f = TweetsFragment.newInstance(Const.TWEET_LIST_ALL);
      break;
      case R.id.navigation_favs:
        f = TweetsFragment.newInstance(Const.TWEET_LIST_FAVS);
      break;
      case R.id.navigation_account:
      break;
      default:
        Log.e(TAG, "Algo a ido mal en el switch");
      break;
    }
    if(f != null) {
      getSupportFragmentManager()
          .beginTransaction()
          .replace(R.id.fragment_container, f)
          .commit();
      return true;
    }
    Log.e(TAG, "Algo a ido mal creando el fragment de dashboard");
    return false;
  }
};  
  
  
```

En **TweetsFragment** Agregamos metodo para cargar uno u otra lista y lo ponemos 
en la recarga de swipe-refresh y cuando se carga por primera ver la 
lista
```
private void loadTweetList() {
  if( mTweetListType == Const.TWEET_LIST_ALL) {
    loadLocalTweets();
  } else if ( mTweetListType == Const.TWEET_LIST_FAVS) {
    loadFavsTweets();
  }
}

@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
  ...
  swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
-       //doRequestTweets();
+       if( mTweetListType == Const.TWEET_LIST_ALL) {
            doRequestTweets();
        } else if ( mTweetListType == Const.TWEET_LIST_FAVS) {
            doRequestFavsTweets();
        }
      }
  });
  ...
- //loadLocalTweets();
+ if( mTweetListType == Const.TWEET_LIST_ALL) {
      loadLocalTweets();
  } else if ( mTweetListType == Const.TWEET_LIST_FAVS) {
      loadLocalFavsTweets();
  }
  return view;
}

private void doRequestFavsTweets() { CHECAR ABAJO }

private void loadLocalFavsTweets() { CHECAR ABAJO  }
    
```



### 94. S08-CL30: Tweets Favoritos: Repository y ViewMode

1. En **TweetRepository** 
  1.1. Agregamos una nueva lista donde guardaremos los tweets favoritos y una var 
  donde guardaremos el nombre del usuario para compararlo con el nombre de los 
  likes de los tweets y asi identificarlos.   
  1.2. agregamos metodo que filtrara de la lista completa de tweets, por aquellos 
  al que el usuario logeado le dio like.   
  1.3. agregamos la actualizacion de los tweets en caso de que el usuario likeo 
  un tweet.   
```java
public class TweetRepository {
  private MutableLiveData<List<Tweet>> favTeetsLiveData;
  String username;
  
  public TweetRepository() {...
+   username = SharedPreferencesManager.getsomeString(Const.USER_USERNAME);
  }

  public MutableLiveData<List<Tweet>> getFavTeetsLiveData() {
      if (favTeetsLiveData == null) {
          favTeetsLiveData = new MutableLiveData<>();
      }

      //BUSCAMOS LOS TWEETS QUE EL USUARIO ACTUAL LE DIO LIKE
      List<Tweet> newFavList = new ArrayList<>();
      Iterator itTweets  = allTeetsLiveData.getValue().iterator();
      while(itTweets.hasNext()) {
          Tweet curretTweet = (Tweet)itTweets.next();
          Iterator itLikes = curretTweet.getLikes().iterator();
          boolean finded = false;
          while (itLikes.hasNext() && !finded) {//UN TWEET TIENE VARIOS LIKES
              Like currentLike = (Like)itLikes.next();
+             if(currentLike.getUsername().equals(username)) {
                  // DENTRO DE LOS LIKES AHI UNO QUE TIENE EL MISMO NOMBRE DE USUARIO QUE EL LOGEADO
                  finded = true;
                  newFavList.add(curretTweet);
              }
          }
      }
+     favTeetsLiveData.setValue(newFavList);//NOTIFICAMOS DE CAMBIO A UI
      return favTeetsLiveData;
  }

  public void likeTweet(final int idTweet) {
    Call<Tweet> tweetCall = miniTwitterServiceAuth.likeTweet(idTweet);
    tweetCall.enqueue(new Callback<Tweet>() {
      @Override public void onResponse(Call<Tweet> call, Response<Tweet> response) {
          if(response.isSuccessful()){
              for (...}
              allTeetsLiveData.setValue(listaClonada);//AVISAMOS DE CAMBIO AL UI
+             getFavTeetsLiveData();//REFRESCAMOS LA LISTA DE FAVORITOS POR SI LE DIO ME GUSTA A UN TWEET PROPIP
              Log.i(TAG, "Lista clonada por like a tweet");
          } else {
     ...     
  }}}
```

2. En **TweetViewModel** creamos la lista de favoritos, y dos metodos uno para obtener los ya cargados, y otro 
 para cargar del servidor y obtener de nuevo los favoritos
```java
public class TweetViewModel extends AndroidViewModel {
    private LiveData<List<Tweet>> favTweetsLiveData;
    
    public LiveData<List<Tweet>> getFavTweets() {
        favTweetsLiveData = tweetRepository.getFavTeetsLiveData();
        return favTweetsLiveData;
    }

    public LiveData<List<Tweet>> getNewFavTweets() {
        getNewTweetsLiveData(); //OBTENEMOS DEL SERVIDOR TODOS LOS TWEETS
        return getFavTweets();  //APARTE DE OBTENER LOS TWEETS FAVORITOS LOS FILTRARA DE NUEVO
    }
```

3. En **TweetsFragment** llenamos los metodos que nos faltaban
```
private void doRequestFavsTweets() {
  tweetViewModel.getNewFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
    @Override public void onChanged(List<Tweet> tweets) {
        tweetList = tweets;
        swipeRefreshLayout.setRefreshing(false);
        adapter.setData(tweetList);
        tweetViewModel.getNewFavTweets().removeObserver(this);
    }
  });
}

private void loadLocalFavsTweets() {
  tweetViewModel.getFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
    @Override public void onChanged(List<Tweet> tweets) {
        tweetList = tweets;
        adapter.setData(tweetList);
    }
  });
}
```


**Resultado:** Podemos ver los los tweets que tenemos como favoritos y estos se 
agregaran o quitaran de la vista aotomaticamente.   
**Commit:** "like tweed y lista de tweets favoritos".   
![12_lista_favoritos_agregar_quitar.gif](./img/curso2/12_lista_favoritos_agregar_quitar.gif)   



### 95. S08-CL31: Petición DELETE: eliminar tweet
1. Haremos una peticion de eliminar un tweet para poder generar el pojo de 
respuesta de este servicio.   
```java
public class DeleteTweetResponse {
    @SerializedName("mensaje") @Expose private String mensaje;
    @SerializedName("user") @Expose private User      user;
```

2. En **MiniTwitterService** agregamos la peticion de eliminar
```java
@DELETE("tweets/{tweet_id}")
Call<DeleteTweetResponse> deleteTweet(@Path("tweet_id") int tweetId);
```


### 96. S08-CL32: Petición DELETE: Repository y ViewModel
1. En **TweetRepository** usamos la peticion de eliminar
```java
public void deleteTweet(final int idTweet) {
    Call<DeleteTweetResponse> deleteTweetResponseCall = miniTwitterServiceAuth.deleteTweet(idTweet);
    deleteTweetResponseCall.enqueue(new Callback<DeleteTweetResponse>() {
        @Override public void onResponse(Call<DeleteTweetResponse> call, Response<DeleteTweetResponse> response) {
            if(response.isSuccessful()){
                List<Tweet> listaClonada = new ArrayList<>();
                for (int i = 0; i < allTeetsLiveData.getValue().size(); i++) {
                    if(allTeetsLiveData.getValue().get(i).getId() != idTweet)  {
        //SI EL ID DEL TWEET ELIMINADO ES EL MISMO AL RECODRRIDO
        //DE TODA LA LISTA NO LO INCLUIMOS EN LA LISTA CLONADA
                        listaClonada.add(allTeetsLiveData.getValue().get(i));
                    }
                }
                allTeetsLiveData.setValue(listaClonada);//AVISAMOS DE CAMBIO AL UI
                getFavTeetsLiveData();//REFRESCAMOS LA LISTA DE FAVORITOS POR SI LE DIO ME GUSTA A UN TWEET PROPIP
                Log.i(TAG, "Lista clonada por like eliminado");
            } else {
                Toast.makeText(MyApp.getContext(), "Error, Checa tu peticion", Toast.LENGTH_LONG).show();
            }
        }
        @Override public void onFailure(Call<DeleteTweetResponse> call, Throwable t) {
            Toast.makeText(MyApp.getContext(), "Error de conexion", Toast.LENGTH_LONG).show();
        }
    });
}
```


2. En **TweetViewModel** consumimos el metodo creado de arriba
```java
public void deleteTweet(int id) {
    tweetRepository.deleteTweet(id);
}
```
falta: lanzar el evento de eliminar


### 97. S08-CL33: Petición DELETE: BottomSheetDialogFragment
**Objetivo** Creamos un menu de opciones inferior que aparecera al 
mantener precionado un tweet



1. Creamos un Fragment whit viewModel **BottonModalTweetFragment** y 
sustituimos el BottonModalTweetViewModel que nos crea dentro de BottonModalTweetFragment 
por nuestro TweetViewModel

2. Creamos las vistas del menu inferior.   

botton_modal_tweet_fragment.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".ui.fragment.BottonModalTweetFragment">
    <com.google.android.material.navigation.NavigationView
        android:id="@+id/navigation_view_bottom_tweet"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginBottom="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:menu="@menu/navigation_view_dialog_tweet" />
</androidx.constraintlayout.widget.ConstraintLayout>
```

menu/navigation_view_dialog_tweet
```
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <group android:checkableBehavior="none">
        <item
            android:id="@+id/action_delete_tweet"
            android:icon="@drawable/ic_delete_forever_black_24dp"
            android:title="Eliminar tweet"/>
    </group>
</menu>
```

3. agregamos listener al boton de eliminar, el cual obtendra el id del 
tweet a eliminar como argumento del getinstance
```java
public class BottonModalTweetFragment extends BottomSheetDialogFragment {
  private TweetViewModel tweetViewModel;
  private int idTweetDelete;

  public static BottonModalTweetFragment newInstance(int idTweet) {
    BottonModalTweetFragment bottonModalTweetFragment = new BottonModalTweetFragment();
    Bundle bundle = new Bundle();
    bundle.putInt(Const.ARG_TWEET_ID, idTweet);
    bottonModalTweetFragment.setArguments(bundle); /* ######### CUIDADO  ########## */
    return bottonModalTweetFragment;
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if(getArguments() != null) {
+     idTweetDelete = getArguments().getInt(Const.ARG_TWEET_ID);
    }
  }

  @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.botton_modal_tweet_fragment, container, false);
    final NavigationView navigationView =  v.findViewById(R.id.navigation_view_bottom_tweet);
+   navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
      @Override public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int idOptionSelected = menuItem.getItemId();
        if(idOptionSelected == R.id.action_delete_tweet) {
          getDialog().dismiss();
+         tweetViewModel.deleteTweet(idTweetDelete);
          return true;
        }
        return false;
      }
    });
    return v;
  }
  
  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
+   tweetViewModel = ViewModelProviders.of(getActivity()).get(TweetViewModel.class);
  }
}
```

4. En **TweetViewModel** agregamos metodo para poder abrirlo 
```java
public void openDialogTweetMenu(Context context, int idTweet) {
  BottonModalTweetFragment bottonModalTweetFragment = BottonModalTweetFragment.newInstance(idTweet);
  bottonModalTweetFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), "bottonModalTweetFragment");
}
```


### 98. S08-CL34: Petición DELETE: Rediseñando Adapter

1. Creamos un vector 
```xml
<vector xmlns:android="http://schemas.android.com/apk/res/android" android:width="24dp" android:height="24dp" android:viewportWidth="24.0"  android:viewportHeight="24.0">
  <path android:fillColor="#FF000000" android:pathData="M7.41,7.84L12,12.42l4.59,-4.58L18,9.25l-6,6 -6,-6z"/>
</vector>
```

2. En **fragment_tweets.xml** añadimos el boton para mostrar el menu 
de eliminar 


3. En **MyTweetRecyclerViewAdapter**, agregamos evento abrir menu
```java
@Override public void onBindViewHolder(final ViewHolder holder, int position) {...
  //SOLO MOSTRAMOS EL ICONO SI ES EL DUEÑO DEL TWEET
+ holder.icSHowMenu.setVisibility(View.GONE);
  if(holder.mItem.getUser().getUsername().equals(username)) {
      holder.icSHowMenu.setVisibility(View.VISIBLE);
  }
  holder.icSHowMenu.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
+       tweetViewModel.openDialogTweetMenu(ctx, holder.mItem.getId());
      }
  });
  
  public class ViewHolder extends RecyclerView.ViewHolder {...
    public ImageView icSHowMenu;
    public ViewHolder(View view) {
        ...
+       icSHowMenu = view.findViewById(R.id.ic_show_menu);
    }
  }
```


**Resultado** Podemos eliminar los tweets que creamos, no se ve pero 
los tweets que no son nuestros no muestra el boton para mostrar menu 
inferiror,   
**Commit:** "Eliminar tweets mostrando menu inferior -BottomSheetDialogFragment-"
![13_eliminar_solo_nuestros_tweets](./img/curso2/13_eliminar_solo_nuestros_tweets.gif)   





### 99. S08-CL35: Fragment Perfil Usuario
<!-- aplicar mvvm -->
1. Creamos un nuevo fragment with view-model y pasamos el view-model que se 
genera auto. 

2. Editamos la vista, por el tema de tamaños ñe agregamos un scroll <!-- agregar scroll, añadir scroll, poner scroll,  agregar scroll vertical, añadir scroll vertical, poner scroll vertical-->
[14_vista_profile_scroll](./img/curso2/14_vista_profile_scroll.xml)   <!-- file://E:\cursos\_2019\img\android\curso2\14_vista_profile_scroll.xml -->


3. Vinculamos elementos al fragmen


4. En **DashnoardActivity** Incluimos en el fragment que creamos en 
la opcion del barra de navegacion inferior.   
```
private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
@Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    Log.i(TAG, "Entro evento mOnNavigationItemSelectedListener");
    Fragment fragment = null;
    switch (item.getItemId()) {
        case R.id.navigation_home: ... break;
        case R.id.navigation_favs: ... break;
        case R.id.navigation_account:
+           fragment = ProfileFragment.newInstance();
            btnToAddTweet.hide();
        break; default: Log.e(TAG, "Algo a hido mal en el switch"); break;
    }
```

### 100. S08-CL36: Petición GET y PUT: datos perfil
1. Generar el pojo a partir de json de la peticion de obtener perfil " GET: /users/profile"   
json
```json
{
  "id": 760,
  "username": "mario_marquez",
  "email": "mario@mario.com",
  "descripcion": "",
  "website": "",
  "photoUrl": "",
  "created": "2019-11-20T03:07:00.000Z"
}
```
java
```java
import com.google.gson.annotations.Expose; import com.google.gson.annotations.SerializedName;
public class GetProfileResponse {
    @SerializedName("id") @Expose private Integer           id;
    @SerializedName("username") @Expose private String      username;
    @SerializedName("email") @Expose private String         email;
    @SerializedName("descripcion") @Expose private String   descripcion;
    @SerializedName("website") @Expose private String       website;
    @SerializedName("photoUrl") @Expose private String      photoUrl;
    @SerializedName("created") @Expose private String       created;
```

2. En **MiniTwitterServiceAuth** Añadmos peticion
```
  @GET("users/profile")
  Call<GetProfileResponse> getProfile();
```


3. agregar pojo la peticion de modificar usuario "PUT: users/profile/"
```json
{
  "username": "mario_marquez",
  "email": "mario@mario.com",
  "descripcion": "",
  "website": "",
}
```

4. En **MiniTwitterServiceAuth** agregar peticion
```java
@PUT("users/profile")
Call<GetProfileResponse> changeProfile(@Body ChangeProfileRequest changeProfileRequest);
```


### 101. S08-CL37: Petición GET y PUT: Repository y ViewModel
1. implementar peticion y crear repository para profile
```java
public class ProfileRepository {
    private final String TAG = this.getClass().getSimpleName();

    private MiniTwitterClientAuth miniTwitterClientAuth;
    private MiniTwitterServiceAuth miniTwitterServiceAuth;
    private MutableLiveData<GetProfileResponse> userprofile;

    public ProfileRepository() {
        retrofitInit();
        userprofile = getUserprofile();
    }

    private void retrofitInit() {
        miniTwitterClientAuth = MiniTwitterClientAuth.getInstance();
        miniTwitterServiceAuth = miniTwitterClientAuth.getMiniTwitterServiceAuth();
    }

    public MutableLiveData<GetProfileResponse> getUserprofile() {
        if(userprofile == null) {
            userprofile = new MutableLiveData<>();
        }

        Call<GetProfileResponse> call = miniTwitterServiceAuth.getProfile();
        call.enqueue(new Callback<GetProfileResponse>() {
            @Override public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
                if(response.isSuccessful()) {
                    userprofile.setValue(response.body());
                } else {
                    Toast.makeText(MyApp.getContext(), "Error de peticion", Toast.LENGTH_LONG).show();
                }
            }
            @Override public void onFailure(Call<GetProfileResponse> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error de conexion", Toast.LENGTH_LONG).show();
            }
        });
        return userprofile;
    }
}
```

2. En **ProfileViewModel**
2.1. Iniciamos nuestro repositorio de ProfileRepository
```java
public class ProfileViewModel extends AndroidViewModel {
    private ProfileRepository profileRepository;
    private MutableLiveData<GetProfileResponse> userprofile;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
        profileRepository = new ProfileRepository();
        userprofile = profileRepository.getUserprofile();
    }
}
```

3. En **ProfileFragment** usamos nuestro ProfileViewModel para hacer 
la peticion antes de devolver la vista
```java
public class ProfileFragment extends Fragment {
    private ProfileViewModel profileViewModel;
    private ImageView imgPhotoProfile; private EditText inUsername, inEmil, inCueerentPasword, inWebsite, inDescription; private Button btnDaveProfile, btnChangePassword;
    public static ProfileFragment newInstance() { return new ProfileFragment(); }
    
    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
+       profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class); //CUIDADO saber como se inicia un ViewModel
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_fragment, container, false);
        //images
        imgPhotoProfile = v.findViewById(R.id.img_profile_photo);
        //editText
        inUsername = v.findViewById(R.id.inttxt_username); inEmil = v.findViewById(R.id.intxt_profile_email); inCueerentPasword = v.findViewById(R.id.intext_current_pass); inWebsite = v.findViewById(R.id.intext_wetsite); inDescription = v.findViewById(R.id.intext_profile_description);
        //btn
        btnDaveProfile = v.findViewById(R.id.btn_save_profile); btnDaveProfile.setOnClickListener(new View.OnClickListener() {...});
        btnChangePassword = v.findViewById(R.id.btn_change_pass); btnChangePassword.setOnClickListener(new View.OnClickListener() {...});
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
+       profileViewModel.getUserprofile().observe(getActivity(), new Observer<GetProfileResponse>() {
            @Override public void onChanged(GetProfileResponse getProfileResponse) {
                inUsername.setText(getProfileResponse.getUsername());
                inEmil.setText(getProfileResponse.getEmail());
                inWebsite.setText(getProfileResponse.getWebsite());
                inDescription.setText(getProfileResponse.getDescripcion());
+               if(!getProfileResponse.getPhotoUrl().isEmpty()) {
                    Glide.with(getActivity()).load(Const.API_GET_PHOTO_USER+getProfileResponse.getPhotoUrl())
                            .into(imgPhotoProfile);
                }
            }
        });
    }
}

```

**Resultado:** Tenemos implementado el ver los datos del usuario consultandoloe en el servidor.   
![14_peticion_mostrar_datos_usuario](./img/curso2/14_peticion_mostrar_datos_usuario.gif)   



### 102. S08-CL38: Petición PUT perfil: Repository y ViewModel
1. Vamos al repositorio **ProfileRepository** para implementar la peticion de 
actualizar el perfil, que es casi igual que la de obtener.   
```java
public MutableLiveData<GetProfileResponse> chnageUserprofile(ChangeProfileRequest changeProfileRequest) {
    if(userprofile == null) { userprofile = new MutableLiveData<>(); }
    Call<GetProfileResponse> call = miniTwitterServiceAuth.changeProfile(changeProfileRequest);
    call.enqueue(new Callback<GetProfileResponse>() {
        @Override public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
            if(response.isSuccessful()) {
                userprofile.setValue(response.body());
            } else {
                Toast.makeText(MyApp.getContext(), "Error de peticion", Toast.LENGTH_LONG).show();
            }
        }
        @Override public void onFailure(Call<GetProfileResponse> call, Throwable t) {
            Toast.makeText(MyApp.getContext(), "Error de conexion", Toast.LENGTH_LONG).show();
        }
    });
    return userprofile;
}
```

2. En **ProfileViewModel** intermediamos la peticion del repositorio.   
```java
public MutableLiveData<GetProfileResponse> changeUserprofile(ChangeProfileRequest changeProfileRequest) {
    return profileRepository.chnageUserprofile(changeProfileRequest);;
}
```

3. En **ProfileFragment** en el evento del boton de guardar agregamos 
la peticion PUT para actualizar el usuario, la cual ahara que cambie el objeto de 
la info del usuario lo que lanzara el observador


**Resultado:** Podemos actualizar la informacion del usuario en el servidor.   
**Commit:** "Minitwitter carga y modificacion de la info del usuario, falta modificar imagen".   
![15_actualizar_info_usuario](./img/curso2/15_actualizar_info_usuario.gif)   








### 102. MIO: Manejo de errores MVVM en peticion y actuar sobre la vista
<!-- manejo de errores en mvvm manejo de errores, manejo de errores en mvvm manejo de errores manejar errores en arquitectura mvvm manejar errores, manejar errores en arquitectura mvvm manejo de errores manejar errores en patron mvvm manejar errores, manejar errores en patron mvvm manejo de errores manejo de errores en patron mvvm manejo de errores, manejo de errores en patron mvvm manejo de errores -->
**PROBLEMA:**  Tenemos el escenario donde desabilitamos el boton mientras se hace la 
peticion y cuando sale bien lo habilitamos, ya que se hace un cambio sobre el objeto 
de informacion de usuario lo que dispara su observador, pero cuando falla la API (regresa algo diferente a 200) 
no cambia el objeto de informacion de usuario por lo que no podia mostrar el mensaje 
y habilitar de nuevo nuevo el boton, por la separacion de la arquitectura MVVM, por 
lo que la hice la siguiente **SOLUCION** agrege un objeto error en el repositorio que 
lo capturo en la vista por el view-model y le agrego un observador y cualquier cambio 
sobre este objeto error se ejecutara en el observador que esta en la vista, lo cual ya 
podre activar el boton.   

1. En **ProfileRepository**.   
  1.1. agregamos un objeto error con su get.   
  1.1. En la peticion que queremos mandar el error le ponemos un setValue para cuando 
  observemos el objeto este se pueda disparar.     
```java
public class ProfileRepository {
  ...
+ final private MutableLiveData<Throwable> error = new MutableLiveData<>();
  ...
  public MutableLiveData<GetProfileResponse> chnageUserprofile(ChangeProfileRequest changeProfileRequest) {
    if(userprofile == null) { userprofile = new MutableLiveData<>();  }
    Call<GetProfileResponse> call = miniTwitterServiceAuth.changeProfile(changeProfileRequest);
    call.enqueue(new Callback<GetProfileResponse>() {
      @Override public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
        if(response.isSuccessful()) { userprofile.setValue(response.body()); }  else {  error.setValue(new Throwable("Error de peticion")); }
      }
      @Override public void onFailure(Call<GetProfileResponse> call, Throwable t) {
+       error.setValue(t);
      }
    });
    return userprofile;
  }
  
+ public MutableLiveData<Throwable> getError() {
    return error;
  }
}
```

2. En **ProfileViewModel** agregamos un metodo para obtener el 
objeto error.   
```java
public MutableLiveData<Throwable> getErrorUserProfile() {
  return profileRepository.getError();
}
```


3. En **ProfileFragment** agregamos un observador al objeto error 
y le ponemos la logica de vista que requiramos.   
```java
public class ProfileFragment extends Fragment {
  private ProfileViewModel profileViewModel;

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
//OBTENDREMOS EL OBJ ERROR ATRAVEZ DE "profileViewModel"
+   profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
  }
  
  @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    ...
    btnSaveProfile = v.findViewById(R.id.btn_save_profile); btnSaveProfile.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        ...
//ESTA ES LA PETICION QUE CAMBIARA EL OBJETO ERROR SI ES QUE OCURRE ALGUNO
+       profileViewModel.changeUserprofile(changeProfileRequest);
      }
    });
    ... return v;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//CUALQUIER CAMBIO DE ERROR GATILLARA ESTE OBSERVADOR
+   profileViewModel.getErrorUserProfile().observe(getActivity(), new Observer<Throwable>() {
      @Override
      public void onChanged(Throwable throwable) {
+       enableButton(btnSaveProfile); //LA ACCION SOBRE VISTA QUE QUEREMOS HACER
      }
    });
  }
}
```


### 103. S08-CL39: Petición POST: subir foto de perfil (MVVM)
Para eso usamos la API *POST users/uploadprofilephoto* la cual subimos una imagen, pero 
vamos a tener problemas con la cache por lo que ahi que indicar a Glide que no use la cache 
para la carga de lafoto de perfil.   

1. En **ProfileFragment** desactivar la cache <!-- no usar cache en glide no usar cache, glide desactivar cache en glide -->
```java
Glide.with(getActivity())
      .load(...)
      .dontAnimate()
      .diskCacheStrategy(DiskCacheStrategy.NONE)
      .skipMemoryCache(true)
```


2. En **MiniTwitterServiceAuth** Agregamos la peticion retrofit
<!--  subir una foto al servidor, subir foto sercidor, subir foto a servidor, incluir foto en peticion, peticion con foto mandar foto a peticion, 
subir foto servidor, subir foto por http subir foto  subir una archivo al servidor, subir archivo sercidor, subir archivo a servidor, incluir archivo en peticion, peticion con archivo mandar archivo a peticion, subir archivo servidor, subir archivo por http subir archivo   subir una imagen al servidor, subir imagen sercidor, subir imagen a servidor, incluir imagen en peticion, peticion con imagen mandar imagen a peticion, subir imagen servidor, subir imagen por http subir imagen   -->
```java
@POST("users/uploadprofilephoto")
Call<UploadPhotoResponse> uploadPhotoProfile(@Part("file\"; filename=\"photo.jpeg\"  ")RequestBody requestBody);
```

### 104. S08-CL40: Petición POST: subir foto de perfil (MVVM)
1. En **ProfileRepository** agregamos un mutable con el nombre de la foto 
para poderlo observar.   
1.1. Implementamos subida de photo

```java
public class ProfileRepository {...
  private MutableLiveData<String> photoProfile;

  public ProfileRepository() {...
+     photoProfile = new MutableLiveData<String>();
  }
    
  public MutableLiveData<String> uploadPhoto(String photoPath) {
    File file = new File(photoPath);
    RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
+   Call<UploadPhotoResponse> uploadPhotoResponse = miniTwitterServiceAuth.uploadPhotoProfile(requestBody);
    uploadPhotoResponse.enqueue(new Callback<UploadPhotoResponse>() {
        @Override public void onResponse(Call<UploadPhotoResponse> call, Response<UploadPhotoResponse> response) {
          if(response.isSuccessful()) {
            String filename = response.body().getFilename();
            SharedPreferencesManager.saveString(Const.USER_PHOTOURL, filename);
+           photoProfile.setValue(filename);
          } else {
            error.setValue(new Throwable("Error en la peticion"));
          }
        }
        @Override
        public void onFailure(Call<UploadPhotoResponse> call, Throwable t) {
          error.setValue(new Throwable(t));
        }
    });
    return photoProfile;
  } 
```

2. En **ProfileViewModel** agregamos el consumo intermedio
```java
public MutableLiveData<String> uploadPhoto(String photo) {
    return profileRepository.uploadPhoto(photo);
}
```

### 105. S08-CL41: Seleccionar fotos: gestión de permisos
**Objetivo:** Vamos a hacer un manejo de permisos para poder leer archivos 
externos, para que el usuario pueda seleccionarlo, y lo haremos con la libreria 
de Dexter para mas facil.   

<!-- libreria para manejo de permisos, libreria para control de permisos, libreria para uso de permisos -->
Libreria Dexter: https://github.com/Karumi/Dexter


0. Agregamos en el manifest el permisos
```xml
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
```

1. Instalar Dexter
```
implementation 'com.karumi:dexter:6.0.0'
```

2. En **DashnoardActivity** implemetamos PermissionListener ya que este sera nuestro 
activity que usara Dexter para trabajar
```java
public class DashnoardActivity extends AppCompatActivity implements PermissionListener {
```

2. En **ProfileFragment** 
2.1. agregamos un PermissionListener  propia de la libreria

```java
private PermissionListener permissionListener;
private void checkPermission() {
  PermissionListener dialogOnPermisionDenied =
      DialogOnDeniedPermissionListener.Builder.withContext(getActivity())
      .withTitle("Permisos")
      .withMessage("Los permisos solicitados son necesarios para poder seleccionar foto de perfil")
      .withButtonText("Aceptar")
      .withIcon(R.mipmap.ic_launcher)
      .build();

  permissionListener = new CompositePermissionListener(
      (PermissionListener)getActivity(),
      dialogOnPermisionDenied
  );

  Dexter.withActivity(getActivity()).withPermission(Manifest
      .permission.READ_EXTERNAL_STORAGE)
      .withListener(permissionListener)
      .check();
}
```
**Resultado:** A la hora de pulsar sobre la imagen de perfil saldra 
un dialogo pidiendo los permisos de lectura de archivos externos, y 
en caso de negarlos saldra una ventana indicando que son necesario para 
x cosa los permisos.   

### 106. S08-CL42: Intent selección foto de galería
<!-- capturar imagen de intent, seleccionar imagen con intent, intent seleccionar imagen intent, usar intent para seleccionar imagen, intent para seleccionar foto -->
<!-- capturar foto de intent, seleccionar foto con intent, intent seleccionar foto intent, usar intent para seleccionar foto, intent para seleccionar imagen -->
1. En **DashnoardActivity** agregamos la captura de la imagen por medio de un intent.   
```java
@Override
public void onPermissionGranted(PermissionGrantedResponse response) {
  Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
  startActivityForResult(intent, Const.SELECT_PHOTO);
}

@Override protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
  super.onActivityResult(requestCode, resultCode, data);
  if (resultCode != RESULT_CANCELED) {
    if (requestCode == Const.SELECT_PHOTO) {
      if (data != null) {
        Uri imgSelected = data.getData();
        String[] partsPath = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(imgSelected, partsPath, null, null, null);
        if (cursor != null) {
          cursor.moveToFirst();
          int imageIndex = cursor.getColumnIndex(partsPath[0]);
          String photopath = cursor.getString(imageIndex);
          Log.i(TAG, "Archivo a subir: " + photopath);
          profileViewModel.uploadPhoto(photopath);
          cursor.close();
} } } } }
```


**Error:** "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).".   
**Descripcion:** A la hora de subir imagen y de ir y volver del fregment de perfil terminaba la aplicacion con 
este error.   
**Solucion:** Cambiamos el contexto de glide with() de getActivity() por MyApp.getContext()".   
```java
  profileViewModel.getUploadPhotoUserprofile().observe(getActivity(), new Observer<String>() {
            @Override public void onChanged(String photoName) {
                if(photoName.isEmpty()) { return ;}
      //ERROR:  Glide.with(getActivity())...
                Glide.with(MyApp.getContext())...
```

**Resultado:** Podemos subir una imagen para subir la imagen de 
perfil y por medio de observadores cambiar la imagen en distintos 
lugares de la ui.   
**Commit:** "subir imagen cambio de imagen de perfil, manejo de cache y observadores para cambiar imagen en ui".   
![16_subir_imagen_cambio_de_imagen_de_perfil](./img/curso2/16_subir_imagen_cambio_de_imagen_de_perfil.gif)   


























































<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

----

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />


# Por el Camino

## No detecta el celular

<!-- no reconoce el celular, reconocer el ceular, no etecta el telefono -->

Recordar que tenemos no basta con activar el modo desarrollador, tambien
deberemos de activar la `Depuracion USB` y si aun no funciona tambien
activar `Desbloqueo de OEM` y si aun no lo detecta checar los drivers,
los cuales podremos seguir las instrucciones en este [Link](https://developer.android.com/studio/run/oem-usb)





