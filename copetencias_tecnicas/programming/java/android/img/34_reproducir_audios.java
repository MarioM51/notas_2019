package com.mariocorp.mistareas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;

import java.io.IOException;

public class SoundsActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnSound1, btnSound2, btnSound3, btnStop;
    int mRepeats = 2, idFX1 = 1, idFX2 = 1, idFX3 = 1, volumne = 2, nowPlaying = -1;
    SoundPool soundPool;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sounds);

        btnSound1 = findViewById(R.id.btn_sound1);
        btnSound2 = findViewById(R.id.btn_sound2);
        btnSound3 = findViewById(R.id.btn_sound3);
        btnStop = findViewById(R.id.btn_stop);
        btnSound1.setOnClickListener(this);
        btnSound2.setOnClickListener(this);
        btnSound3.setOnClickListener(this);
        btnStop.setOnClickListener(this);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //CODIGO PARA LOLI POP (22) EN ADELANTE
            AudioAttributes attr = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(3) // cuantas reproduccione simultaneas permitimos
                    .setAudioAttributes(attr)
                    .build();
        }  else {
            //CODIGO PARA LOLI POP HACIA ATRAS
            soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }
        try{
            //CARGAMOS LOS AUDIOS
            AssetManager mng = this.getAssets(); //cargamos la carpeta de los assets
            AssetFileDescriptor descriptor = mng.openFd("sound_1.ogg");
            idFX1 = soundPool.load(descriptor, 0);

            descriptor = mng.openFd("sound_2.ogg");
            idFX2 = soundPool.load(descriptor, 0);

            descriptor = mng.openFd("sound_3.ogg");
            idFX3 = soundPool.load(descriptor, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        SeekBar seekBar = findViewById(R.id.sbar_volume);
        seekBar.setProgress(volumne);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                volumne = (progress != 0) ? volumne=10/progress : 0;
                soundPool.setVolume(volumne, 1,1);
            }

            @Override public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        final Spinner spinner = findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String temp = String.valueOf(spinner.getSelectedItem());
                Log.i("SounfActivity", "Repeticiones: " + temp);
                mRepeats = Integer.valueOf(temp);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        int prioridad=0, velocidadSonido=1;
        switch (v.getId()) {
            case(R.id.btn_sound1):
                nowPlaying = soundPool.play(idFX1, volumne, volumne, prioridad, mRepeats, velocidadSonido);
            break;
            case(R.id.btn_sound2):
                nowPlaying = soundPool.play(idFX2, volumne, volumne, prioridad, mRepeats, velocidadSonido);
            break;
            case(R.id.btn_sound3):
                nowPlaying = soundPool.play(idFX3, volumne, volumne, prioridad, mRepeats, velocidadSonido);
            break;
            case(R.id.btn_stop):
                soundPool.stop(nowPlaying);
            break;
        }
    }
}
