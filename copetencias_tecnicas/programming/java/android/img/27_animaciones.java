package com.mariocorp.mistareas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

public class AnimActivity extends AppCompatActivity implements Animation.AnimationListener, View.OnClickListener {

    ImageView imageView;
    TextView txtvStatusAnimation, txtvSpreed;
    SeekBar seekBarSpeed;

    Button   btnFadeOut, btnFadeIn, btnFadeInOut,
                btnZoomIn, btnZoomOut,
                btnLeftRight, btnTopBotton, btnRightLeft,
                btnBounce, btnFlash,
                btnRotateLeft, btnRotateRight;

    Animation   animFadeOut, animFadeIn, animFadeInOut,
            animZoomIn, animZoomOut,
            animLeftRight, animTopBotton, animRightLeft,
            animBounce, animFlash,
            animRotateLeft, animRotateRight;

    int seekBarValue = 2000;

    private void loadUI() {
        btnFadeOut = findViewById(R.id.btn_fade_out);
        btnFadeOut.setOnClickListener(this);

        btnFadeIn = findViewById(R.id.btn_fade_in);
        btnFadeIn.setOnClickListener(this);

        btnFadeInOut = findViewById(R.id.btn_fade_in_out);
        btnFadeInOut.setOnClickListener(this);

        btnZoomIn = findViewById(R.id.btn_zoom_in);
        btnZoomIn.setOnClickListener(this);

        btnZoomOut = findViewById(R.id.btn_zoom_out);
        btnZoomOut.setOnClickListener(this);

        btnLeftRight = findViewById(R.id.btn_left_right);
        btnLeftRight.setOnClickListener(this);

        btnTopBotton = findViewById(R.id.btn_top_botton);
        btnTopBotton.setOnClickListener(this);

        btnRightLeft = findViewById(R.id.btn_right_left);
        btnRightLeft.setOnClickListener(this);

        btnBounce = findViewById(R.id.btn_bounce);
        btnBounce.setOnClickListener(this);

        btnFlash = findViewById(R.id.btn_flash);
        btnFlash.setOnClickListener(this);

        btnRotateLeft = findViewById(R.id.btn_rotate_left);
        btnRotateLeft.setOnClickListener(this);

        btnRotateRight = findViewById(R.id.btn_rotate_right);
        btnRotateRight.setOnClickListener(this);

        imageView = findViewById(R.id.img_2anim);
        txtvSpreed = findViewById(R.id.txt_speed);
        txtvStatusAnimation = findViewById(R.id.txt_status_anim);

        seekBarSpeed = findViewById(R.id.seekbar_speed);
        seekBarSpeed.setProgress(2000);
        seekBarSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarValue = progress;
                txtvSpreed.setText(String.format("%s/%s", seekBarValue, seekBarSpeed.getMax()));
            }

            @Override public void onStartTrackingTouch(SeekBar seekBar) {
                // detecta que empieza a mover el seekbak
            }

            @Override public void onStopTrackingTouch(SeekBar seekBar) {
                // detecta cuando termina de mover la seekbar
            }
        });
    }

    private void loadAninimations() {
        animFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        animFadeOut.setAnimationListener(this);
        animFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        animFadeIn.setAnimationListener(this);
        animFadeInOut = AnimationUtils.loadAnimation(this, R.anim.fade_in_out);
        animFadeInOut.setAnimationListener(this);
        animZoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
        animZoomIn.setAnimationListener(this);
        animZoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
        animZoomOut.setAnimationListener(this);
        animLeftRight = AnimationUtils.loadAnimation(this, R.anim.left_right);
        animLeftRight.setAnimationListener(this);
        animTopBotton = AnimationUtils.loadAnimation(this, R.anim.top_bottom);
        animTopBotton.setAnimationListener(this);
        animRightLeft = AnimationUtils.loadAnimation(this, R.anim.right_left);
        animRightLeft.setAnimationListener(this);
        animBounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        animBounce.setAnimationListener(this);
        animFlash = AnimationUtils.loadAnimation(this, R.anim.flash);
        animFlash.setAnimationListener(this);
        animRotateLeft = AnimationUtils.loadAnimation(this, R.anim.rotate_left);
        animRotateLeft.setAnimationListener(this);
        animRotateRight = AnimationUtils.loadAnimation(this, R.anim.rotate_right);
        animRotateRight.setAnimationListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anim);
        this.loadUI();
        this.loadAninimations();
    }


    @Override
    public void onAnimationStart(Animation animation) {
        txtvStatusAnimation.setText("Start...");
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        txtvStatusAnimation.setText("Stop...");
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        txtvStatusAnimation.setText("Repeat...");
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case( R.id.btn_fade_out): animFadeOut.setDuration(seekBarValue); imageView.startAnimation(animFadeOut);  break;
            case( R.id.btn_fade_in): animFadeIn.setDuration(seekBarValue); imageView.startAnimation(animFadeIn);  break;
            case( R.id.btn_fade_in_out): animFadeInOut.setDuration(seekBarValue); imageView.startAnimation(animFadeInOut);  break;
            case( R.id.btn_zoom_in): animZoomIn.setDuration(seekBarValue); imageView.startAnimation(animZoomIn);  break;
            case( R.id.btn_zoom_out): animZoomOut.setDuration(seekBarValue); imageView.startAnimation(animZoomOut);  break;
            case( R.id.btn_left_right): animLeftRight.setDuration(seekBarValue); imageView.startAnimation(animLeftRight);  break;
            case( R.id.btn_top_botton): animTopBotton.setDuration(seekBarValue); imageView.startAnimation(animTopBotton);  break;
            case( R.id.btn_right_left): animRightLeft.setDuration(seekBarValue); imageView.startAnimation(animRightLeft);  break;
            case( R.id.btn_bounce): animBounce.setDuration(seekBarValue); imageView.startAnimation(animBounce);  break;
            case( R.id.btn_flash): animFlash.setDuration(seekBarValue); imageView.startAnimation(animFlash);  break;
            case( R.id.btn_rotate_left): animRotateLeft.setDuration(seekBarValue); imageView.startAnimation(animRotateLeft);  break;
            case( R.id.btn_rotate_right): animRotateRight.setDuration(seekBarValue); imageView.startAnimation(animRotateRight);  break;
        }
    }
}
