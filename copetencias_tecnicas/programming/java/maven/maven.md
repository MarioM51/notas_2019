# Maven

- [Maven](#maven)
  - [Crear proyecto](#crear-proyecto)
    - [Ejecutar main](#ejecutar-main)
    - [Ejecutar tests](#ejecutar-tests)
    - [Ver arbol de dependencias](#ver-arbol-de-dependencias)
    - [Actualizar directorio de resources](#actualizar-directorio-de-resources)
    - [Actualizar dependencia dentro de otra (un parent creo)](#actualizar-dependencia-dentro-de-otra-un-parent-creo)
    - [La ruta .m2](#la-ruta-m2)

## Crear proyecto

Crear un proyecto con maven 1: ``

```r
mvn -B archetype:generate \
  -DarchetypeGroupId=org.apache.maven.archetypes \
  -DgroupId=com.mycompany.app \
  -DartifactId=my-app
```

### Ejecutar main

A. Agregamos en el pon lo siguiente

```xml
<properties>
  <exec.mainClass>com.mario.MainClass</exec.mainClass>
</properties>
```

B. Nos haceguramos que exista la clase Main con su metodo main en el
paquete correcto

```java
package com.mario;
public class MainClass  {
    public static void main( String[] args ) {
        System.out.println( "Hello World!" );
    }
}
```

C. Ejecutamos el comando> `mvn clean compile exec:java` para limpiar
compilar y ejecutar o solamente `mvn exec:java` para solo ejecutar.

### Ejecutar tests

Ver "Iniciando proyecto" en "copetencias_tecnicas\spring\tests\testing-spring-boot.md"

### Ver arbol de dependencias

A la altura de maven ejecutamos: `mvn dependency:tree`

Actualizar dependencias (algo asi como refresh): `mvn clean install -U -DskipTests`

### Actualizar directorio de resources

`mvn process-resources`

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Actualizar dependencia dentro de otra (un parent creo)

**Objetivo:** Actualizar una dependencia que esta dentro de otra, por
ejemplo, actualizar las anotaciones de fasterxml ya que esta trae nueva
funcionalidad que quiero, pero esta dependencia viene dentro de otra
dependencia en mi caso spring-boot-starter-data-rest.

A. Dependencia inicial, de esta forma estamos trayendo varias dependencias

```xml
<!-- Esta dependencia es un conjunto de dependencias entre ella jackson-annotations v2.11-->
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-rest</artifactId>
</dependency>
```

B. Para agregar el que nosotros queremos debemos de ignorar el que trae
este paquete y agregarlo nosotros.

```xml
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-rest</artifactId>
+   <exclusions>
+         <exclusion>
+             <artifactId>jackson-annotations</artifactId>
+             <groupId>com.fasterxml.jackson.core</groupId>
+         </exclusion>
+     </exclusions>
  </dependency>

+ <dependency>
+   <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
+   <groupId>com.fasterxml.jackson.core</groupId>
+   <artifactId>jackson-annotations</artifactId>
+   <version>2.12.0</version>
+ </dependency>
```

**Resultado** Actualizamos un paquete en especifico que viene dentro
de otro.

### La ruta .m2

Ver ruta actual del .m2: `mvn help:evaluate -Dexpression=settings.localRepository`

Cambiar ruta [fuente](https://stackoverflow.com/questions/16649420/how-to-specify-an-alternate-location-for-the-m2-folder-or-settings-xml-permanen)
