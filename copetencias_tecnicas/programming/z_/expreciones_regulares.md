# Expreciones regulares

## Patrones regulares

Para detectar un parametro y valor en json `palabra: [0-9]{1,5},`
detecta por ejemplo "palabra: 1," o "palabra: 123," etc.

____

Para detectar que empiezen por prod... les continue 0 a 2 digitos y
termine con .png o .jpg: `producto-cargador-[1-9]{0,2}.((jpg)|(png))`

```json
"producto-cargador-1.jpg",
"producto-cargador-2.jpg",
"producto-cargador-41.png"
```

____

Detecta una lista de numeros de 1 o 2 digitos por ejemplo `([0-9]){1,2}.`

```r
1. algo
3. algo
9. algo
10. algo
10.algo # le quitariamos el espacio para que lo detecte bien al patron
100. algo #necesitaria para que lo detecte bien `([0-9]){1,3}. `
```
