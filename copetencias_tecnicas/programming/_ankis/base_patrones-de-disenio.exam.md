# Preguntas Patrones de diseño

## Introduccion

Cuantos son los patrones de diseño fueron propuestos por "The Gang Of Four (GoF)"

- [ ] 19
- [ ] 6
- [ ] 13
- [ ] 23<!-- Correcto-->

----

Cuales son las catregorias de los patrones de diseño

- [ ] Cobertura
- [ ] Creacionales<!-- Correcto-->
- [ ] De diseño
- [ ] Estructurales<!-- Correcto-->
- [ ] Comportamiento <!-- Correcto-->
- [ ] SOLID
- [ ] Usabilidad

----

Cuales son los patrones creacionales

- [ ] Facade
- [ ] Flyweight
- [ ] Factory Method <!-- Correcto-->
- [ ] Abstract Factory <!-- Correcto-->
- [ ] Interactor
- [ ] Builder <!-- Correcto-->
- [ ] Prototype <!-- Correcto-->
- [ ] Interpreter
- [ ] Singleton <!-- Correcto-->
- [ ] Proxy

----

Cuales son los patrones estructurales

- [ ] Command
- [ ] Adapter<!-- Correcto-->
- [ ] Bridge<!-- Correcto-->
- [ ] Factory Method
- [ ] Composite<!-- Correcto-->
- [ ] Decorador<!-- Correcto-->
- [ ] Builder
- [ ] Facade<!-- Correcto-->
- [ ] Flyweight<!-- Correcto-->
- [ ] Abstract Factory
- [ ] Chain of responsability
- [ ] Proxy<!-- Correcto-->
- [ ] Interpreter
- [ ] Interactor
- [ ] Mediator

----

Cuales son los patrones de comportamiento

- [ ] Facade
- [ ] Chain of responsability <!-- Correcto-->
- [ ] Builder
- [ ] Decorador
- [ ] Command <!-- Correcto-->
- [ ] Interpreter <!-- Correcto-->
- [ ] Interactor <!-- Correcto-->
- [ ] Mediator <!-- Correcto-->
- [ ] Momento <!-- Correcto-->
- [ ] Prototype
- [ ] Observer <!-- Correcto-->
- [ ] State <!-- Correcto-->
- [ ] Flyweight
- [ ] Proxy
- [ ] Strategy <!-- Correcto-->
- [ ] Template method <!-- Correcto-->
- [ ] visitor <!-- Correcto-->

## Nomenclatura UML

- [ ] Este sombolo es de <input type="text" />

![02_simbolo_herencia](./img/02_simbolo_herencia.PNG)

- [ ] Este sombolo es de <input type="text" />

![03_simbolo_asociacion](./img/03_simbolo_asociacion.PNG)

- [ ] Este sombolo es de <input type="text" />

![04_simbolo_dependencia](./img/04_simbolo_dependencia.PNG)

- [ ] Este sombolo es de <input type="text" />

![05_simbolo_agregacion.PNG](./img/05_simbolo_agregacion.PNG)

- [ ] Este sombolo es de <input type="text" />

![06_simbolo_composicion](./img/06_simbolo_composicion.PNG)

----

Que significa Asociacion:

- [ ] Que la claseA conoce a la claseB, pero la claseB desconoce a ClaseA <!--Correcto-->

----

Que Significa dependencia:

- [ ] Que la claseA depende de la claseB <!--Correcto-->

----

Que Significa agregacion:

- [ ] Que la ClaseA esta contenida en la ClaseB, y si se elimina ClaseB <!--Correcto-->
      seguira existiendo la clase ClaseA

----

Que significa Composicion:

- [ ] Que la ClaseA esta contenida en la clase ClaseB y si se elimina <!--Correcto-->
      la clase ClaseB se elimina la clase ClaseA

----

## Factory

En que consiste el patron "Factory"

- [ ] En tomar la responsabilidad de crear los objetos de una familia <!-- Correcto-->
      de objetos, nos ayuda cuando queremos instanciar un objeto que
      solo en tiempo de ejecucion sabemos cual instanciar.
- [ ] 
- [ ] 
- [ ] PENDIENTE// //TODO: Que esta pasando

----

Cual es el diagrama del patron "Factory"

- [ ] <br /> ![nope](./img/07_diagrama_factory.PNG)

## Abstract Factory

En que consiste el patron "Abstract Factory"

- [ ] Se podria definir como fabrica de fabricas, donde podremos <!--Correcto-->
      instanciar varios objetos de varias familias

----

Es un escenario donde es comunmente usado el "Abstract Factory"

- [ ] En la creacion de graficos, por ejemplo para crear un boton o <!--Correcto-->
      input, ya sea para linux o windows.

----

Cual es el diagrama del patron "Factory"

- [ ] <br /> ![nope](./img/08_diagrama_abstract_factory.PNG)

## Bulder

En que consiste el patron builder

- [ ] Permite crear objetos con varios campos de una forma desacoplada. <!--Correcto-->

----

Cual es el diagrama del del patron Builder

- [ ] <br /> ![09_diagrama_builder](./img/09_diagrama_builder.PNG)

----

Los campos del objeto pueden ser modificados directamente?

- [ ] Si
- [ ] No <!-- Correcto-->

----

Codigo resultante del patron "Builder"

```java
Conexion c = new Conexion.ConexionBuilder("miUrl", "MiUsuario", "MiPass")
      .puerto(80)
      .seguro(false)
      .build();
```

## Prototype

PENDIENTE DE ENTENDER / TODO....

## Singlenton

En que consiste el patron "Singlenton"

- [ ] Nos sirve para crear un solo objeto de una clase <!--Correcto-->
- [ ] 
- [ ] 

----

Es un escenario donde es comunmente usado el patron "Singlenton"

- [ ] Objetos de configuracion o/y conenexion donde queremos que el <!--Correcto-->
      cambio lo note toda la aplicacion, evitandonos problemas, ej. que
      en una parte de la aplicacion este configurado de una manera y
      en otra parte de otra.
- [ ] 
- [ ] 

----

Cual es el diagrama del del patron "Singlenton"

- [ ] <br /> ![]()

## Chain of Responsibility

En que consiste el patron "Chain of Responsibility"

- [ ] Este es un patron de comportamiento que nos evita acoplar una <!--Correcto-->
      peticion, de su receptor a su emisor, dando la posibilidad de
      dar respueta a dicha peticion a mas de un objeto. Su uso ideal
      es cuando la respuesta puede ser atendida por varios objetos y
      no estamos seguros, de cual de esos objetos va a resolverlo ya
      que lo sabemos es tiempo de ejecucion.

----

Es un escenario/ejemplo donde es comunmente usado el patron "Chain of Responsibility"

- [ ] Por ejemplo, Al abrir una targeta de credito dependiendo del la <!--Correcto-->
      cantidad, se usara un algoritmo u otro, y dichos algoritmos estan
      contenidos en su respectivo objeto.

----

Cual es el diagrama del del patron "Chain of Responsibility"

- [ ] ![nope](./img/11_diagrama_chain_of_responsibility.PNG)

## Command

En que consiste el patron "Command"

- [ ] Es muy util cuando para extraer la logica de negocio en unidades que <!--Correcto-->
      llamamos comandos, ayudando a encampsular toda la informacion necesaria
      para realizar una accion/evento. Nos ayuda a separar la logica de
      negocio de la aplicacion, trabajando con parametros.

----

Cual es el diagrama del del patron "Command"

- [ ] ![12_diagrama_command](./img/12_diagrama_command.PNG)

----

Es un escenario donde es comunmente usado el patron "Command"

- [ ] Ejemplo, Al contratar una targeta de credito tiene procesos <!--Correcto-->
      definidos, pero varia entre que tipo de targeta se quiere sacar,
      entonces con este patron podemos unir el proceso de contratar
      una targeta con sus diferentes algoritmos. Y dichos algoritmos
      requieren de n parametros para realizarse.

----

Es el codigo resultante del patron "Command"

- [ ] A

```java
EstudianteInvoquer estudianteInvoquer = new EstudianteInvoquer();
InscripcionEstudianteCommand insComand = new InscripcionEstudianteCommand(new InscripcionEstudiantePrimaria());
estudianteInvoquer.setCommand(insComand);
estudianteInvoquer.run();
```

## Mediator

En que consiste el patron "Mediator"

- [ ] Nos permite desacoplar los objetos, agregando una capa intermedia, <!--Correcto-->
      para que la interaccion ocurra por medio de esta, en dicha capa
      se encapsula el comportamiento en un objeto. simplificando la
      comunicacion entre objetos de tipo muchos a muchos.

----

Cual es el diagrama del del patron "Mediator"

- [ ] <br /> ![14_diagrama_mediator](./img/14_diagrama_mediator.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Mediator"

- [ ] Ejemplo, construir un chat en los cuales seran varios usuarios <!--Correcto-->
      que se podran comunicar entre todos uno a uno ellos, por ejemplo
      10 usuarios, podra el user1 chatear con el user2, o user4 y
      viceversa.

----

Es el codigo resultante del patron "Mediator"

- [ ] A

```java
public static void main(String[] args) {
  ComunicadorImpl comunicador = new ComunicadorImpl();
  ConcreteCollage1 user1 = new ConcreteCollage1(comunicador, "Usuario1");
  ConcreteCollage2 user2 = new ConcreteCollage2(comunicador, "Usuario2");
  comunicador.setUser1(user1);
  comunicador.setUser2(user2);

  user1.send("Hola soy el usuario1");
  user2.send("Que tal usuario1, yo soy usuario2");
}
/* SALIDA ############
Usuario2 recibio: Hola soy el usuario1
Usuario1 recibio: Que tal usuario1, yo soy usuario2
```

## Memento

En que consiste el patron "Memento"

- [ ] Se utiliza para guardar y restaurar el estado de un objeto en un <!--Correcto-->
      determinado momento

----

Cual es el diagrama del del patron "Memento"

- [ ] <br /> !![15_diagrama_memeto](./img/15_diagrama_memeto.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Memento"

- [ ] Imaginar, que tenemos un documento que tiene su contenido, autor, <!--Correcto-->
      titulo, etc, el cual queremos a travez del tiempo lo modificamos
      y por X razones queremos recuperar estados anteriores.

----

Es el codigo resultante del patron "Memento"

- [ ] A

```java
public static void main(String[] args) {
  ArticuloCaretaker articuloCaretaker = new ArticuloCaretaker();

  Articulo articulo = new Articulo("Mario M.", "primer cambio,");
  articuloCaretaker.addEstado(articulo.createEstado());

  articulo.setText(articulo.getText() + " SEGUNDO CAMBIO,");
  articuloCaretaker.addEstado(articulo.createEstado());

  articulo.setText(articulo.getText() + " cambio tercero,");
  articuloCaretaker.addEstado(articulo.createEstado());

  articulo.setText(articulo.getText() + " CUARTO CAMBIO,");
  articuloCaretaker.addEstado(articulo.createEstado());

  System.out.println("ESTADO ACTUAL: - " + articulo.getText() + "\n");
  for (int i=articuloCaretaker.getEstado().size()-1; i>=0; i--) {
    articulo.restoreEstado(articuloCaretaker.getMemento(i));
    System.out.println("Ctrl+Z:  " + articulo.getText());
  }
}

/* SALIDA
ESTADO ACTUAL:   -   primer cambio, SEGUNDO CAMBIO, cambio tercero, CUARTO CAMBIO,
Ctrl+Z:  primer cambio, SEGUNDO CAMBIO, cambio tercero, CUARTO CAMBIO,
Ctrl+Z:  primer cambio, SEGUNDO CAMBIO, cambio tercero,
Ctrl+Z:  primer cambio, SEGUNDO CAMBIO,
Ctrl+Z:  primer cambio,
```

## Observer

En que consiste el patron "Observer"

- [ ] Este nos permite reaccionar a los cambios de estado en el objeto que <!--Correcto-->
      estemos observando

----

Cual es el diagrama del del patron "Observer"

- [ ] <br /> !![16_diagrama_observer](./img/16_diagrama_observer.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Observer"

- [ ] Ejemplo, Cuando un objeto tenga un cambio nosotros queremos ejecutar <!--Correcto-->
      algo, en otro objeto.

----

Es el codigo resultante del patron "Observer"

- [ ] A

```java
public static void main(String[] args) {
  Coche coche = new Coche();
  Peaton peaton = new Peaton();
  MessagePublisher combioEnSemaforo = new MessagePublisher();
  combioEnSemaforo.attach(coche);
  combioEnSemaforo.attach(peaton);

  Semaforo semaforo = new Semaforo(Color.ROJO);
  combioEnSemaforo.notifyUpdate(semaforo);
  try { Thread.sleep(2000); } catch (Exception e) { e.printStackTrace(); }
  semaforo.status = Color.VERDE;
  combioEnSemaforo.notifyUpdate(semaforo);
}
/* SALIDA
Semaforo cambia a: ROJO
carro se detiene
EL PEATON AVANZA
Semaforo cambia a: VERDE
carro avanza
EL PEATON SE DETIENE
```

## State

En que consiste el patron "State"

- [ ] Su objetivo es modificar el comportamiento del objeto, dependiendo en <!--Correcto-->
      el estado interno que se encuenta, un problema comun al NO usarlo es
      que se crea una cantidad muy grande de if-else.

----

Cual es el diagrama del del patron "State"

- [ ] <br /> ![17_diagrama_state](./img/17_diagrama_state.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "State"

- [ ] Ejemplo, controlar el comportamiento de una alarma de un <!--Correcto-->
      movil, en donde puede tener los estados silencio, vibrador,
      sonido. Y dependiendo del estado cambiara la logica de la alarma.

----

Es el codigo resultante del patron "State"

- [ ] A

```java
MovileAlertStateContext alertStateContext = new MovileAlertStateContext();
alertStateContext.alert();

alertStateContext.setCurrentState(new Vibration());
alertStateContext.alert();

alertStateContext.setCurrentState(new Silencio());
alertStateContext.alert();
/* SALIDA
...((Sonido))...((Sonido))
Vibrando...Vibrando...
---Prender luces solamente---
```

## Interpreter

En que consiste el patron "Interpreter"

- [ ] Dado un lenguaje define una reprecentacion para su gramatica, este patron <!--Correcto-->
      se utiliza para definir un lenguaje, como reprecentar expreciones
      regulares

## Strategy

En que consiste el patron "Strategy"

- [ ] Este nos permite escoger una implementacion especifica en tiempo de <!--Correcto-->
      ejecucion para la misma tarea, y estas estrategias van a ser facilmente
      reutilizados.

----

Cual es la diferencia entre los patrones "Command" y "Strategy"

- [ ] COMMAND es para un conjunto de pasos/algoritmos que tiene varios <!--Correcto-->
      parametros de entrada, y es construido para ser mas encapsulado,
      pudiendo asi enviar este objeto a otro lado como una unidad.
      Mientras que STRATEGY es como debe de hacerce una tarea, ahi un
      acoplamento estre la tarea y el modo que se hace.

----

Cual es el diagrama del del patron "Strategy"

- [ ] <br /> !![19_diagrama_strategy](./img/19_diagrama_strategy.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Strategy"

- [ ] Ejemplo, tenemos que cifrar algo y podemos hacerlo ya sea con un <!--Correcto-->
      algorimoA o un algoritmoB.

----

Es el codigo resultante del patron "Strategy"

- [ ] A

```java
public static void main(String[] args) {
  ITextFormat ftextFormat = (false) ? new UpperText() : new LowerText();
  Context context = new Context(ftextFormat);
  context.publicText("TEXTO Con DIFERENTES formatos a TOMAR");
}
/*SALIDA: dependiendo del boleano
MAYUS: TEXTO CON DIFERENTES FORMATOS A TOMAR
minus: texto con diferentes formatos a tomar
```

## Template Method

En que consiste el patron "Template Method"

- [ ] Define los pasos secuenciales para ejecutar un algoritmo de varios <!--Correcto-->
      pasos, es decir define la escructura del algoritmo en general y los
      sub/algoritmos seran implementada por las clases hija.

----

Cual es el diagrama del del patron "Template Method"

- [ ] <br /> ![20_diagrama_template_method](./img/20_diagrama_template_method.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Template Method"

- [ ] Ejemplo, Tenemos unos requisitos/pasos de registro para nuestros <!--Correcto-->
      usuarios, como validacion, guardado y notificacion no importa de
      que tipo ya sean Alumnos, Maestros, Trabajadores, pero dependiendo
      de que tipo sean estos requisitos/pasos variaran.

----

Es el codigo resultante del patron "Template Method"

- [ ] A

```java
AbstractPayment paymentMng = new VisaPayment();
paymentMng.doPayment();
System.out.println("####################");
paymentMng = new PaypalPayment();
paymentMng.doPayment();
/*
Viendo que este activo Visa
Visa: realiando pago
Notificar al usuario por SMS
####################
Viendo que paypal este activo
Realizar pago por paypal
Notificar al usuario por Email
```

## XXXX

En que consiste el patron "Visitor"

- [ ] Nos permite separar la logica de la estructura de un objeto y nos <!--Correcto-->
      permite agregar o intercambiar logica, dependiendo del visitante
      sin tener que modificar la estructura del objeto.

----

Cual es el diagrama del del patron "Visitor"

- [ ] <br /> ![21_diagrama_visitor](./img/21_diagrama_visitor.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Visitor"

- [ ] Ejemplo. Tenemos un proceso de fabricacion de objetos comunes pero <!--Correcto-->
      estos varian por mediano y grande, por lo que el proceso de empacar
      es distinto, por lo que solo con meter el objeto a paqueteria ellos
      ya saben si lo empacan en carton o madera.

----

Es el codigo resultante del patron "Visitor"

- [ ] A

```java
public static void main(String[] args) {
  IOfertaElement ofertaBoletos = new OfertaBoletos();
  IOfertaElement ofertaTienda = new OfertaBoletos();
  ofertaBoletos.aceptar(new UserCuriosoImpl());
  ofertaTienda.aceptar(new UserCuriosoImpl());

  ofertaBoletos.aceptar(new UserRegularImpl());
  ofertaTienda.aceptar(new UserRegularImpl());

  ofertaBoletos.aceptar(new UserFanImpl());
  ofertaTienda.aceptar(new UserFanImpl());
}
/* SALIDA
usuario CURIOSO, descuento en BOLETOS 10%
usuario CURIOSO, descuento en TIENDA 15%
usuario -- REGULAR, descuento en BOLETOS -- 30%
usuario -- REGULAR, descuento en TIENDA -- 35%
usuario -- -- FAN, descuento en BOLETOS -- -- 50%
usuario -- -- FAN, descuento en TIENDA -- -- 55%
```

## Bridge

En que consiste el patron "Bridge"

- [ ] la usamos cuando queremos desacoplar una abstraccion de su implementacion <!--Correcto-->
      para que los dos puedan variar independientemente, el cual usamos una
      interface que tendra referencia a la implementacion y la abstraccion.

----

Cual es la diferencia entre con "Strategy" y "Bridge"

- [ ] Bridge estamos usando una tarea especifica con una generica que <!--Correcto-->
      bien no tendrian porque trabajar juntas pero pueden hacerlo, y
      STRATEGY la tarea depende forsosamente de una estrategia, EJEMPLO
      podemos cifrar un texto sin enviarlo y viceversa. En cambio no
      podemos realizar un cifrado sin una estrategia.

----

Cual es el diagrama del del patron "Bridge"

- [ ] <br /> ![23_diagrama_adapter](./img/23_diagrama_adapter.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Bridge"

- [ ] Tenemos una abstraccion de enviar una respueta a un cliente, y <!--Correcto-->
      su diferentes implementaciones generar la respuesta, y ya sea que
      querramos modificar como se envia o genera la respuesta, al
      hacerlo, no afectamos a una u otra.

----

Es el codigo resultante del patron "Bridge"

- [ ] A

```java
//Main
new MensajeroCifrador(new CifradoEstandart()).enviarMensaje();
new MensajeroCifrador(new CifradoPersonalizado()).enviarMensaje();

/*
NOTA: aqui no estamos indicando como hacer una tarea, mas bien una tarea
generica (EnviarMensaje) y una especifica (Cifrado) queremos que cuando
modifiquemos una no afectemos a la otra y viceversa.
SALIDA
Mensaje enviado con cifrado ESTANDART
Cifrado PERSONALIZADO aplicado para enviar mensaje
```

## Decorator

En que consiste el patron "Decorator"

- [ ] Se usa para agregar comportamientos o caracteristicas adicionales, a <!--Correcto-->
      una INSTANCIA particular sin afectar a las demas instancias, lo cual
      nos agrega mayor flexibilidad. Osea nos permite agregar mas responsa-
      bilidad/caracteristicas a los objetos en tiempo de ejecucion.

----

Cual es el diagrama del del patron "Decorator"

- [ ] <br /> ![25_diagrama_decorator](./img/25_diagrama_decorator.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Decorator"

- [ ] Ejemplo, Tenemos targetas Visa y MasterCard, y ambas pueden tener <!--Correcto-->
      los extras de "pago internacional" y "pagos digital", los cuales
      al crear la targeta no sabemos si van a usar esta funcionalidad,
      si no hasta que se realize el pago, y por ello no queremos agregar
      esta funcionalidad a todas las targetas, entonces creamos esta
      funcionalidad como extra, y a la hora de realizar el pago optaremos
      por agregar esta funcionalidad u otra, a una targeta u otra.
      EJEMPLO2, fabricamos hamburguesas y Hot-Dogs que comparten
      ingredientes pero no queremos que todos estos ingredientes a estos,
      porque no sabemos si el cliente quiere estos ingredientes o cuales,
      por lo que estos ingredientes los etiquetamos como extras.

----

Es el codigo resultante del patron "Decorator"

- [ ] A

```java
public static void main(String[] args) {
  INavaja navajaBasica = new NavajaBasica();
  System.out.println("==== Navaja Basica Original ====");
  navajaBasica.cuchillos();
  navajaBasica.desarmadores();
  System.out.println("==== Navaja Basica EXTRAS ====");
  navajaBasica = new CortaCarne(navajaBasica);
  navajaBasica.cuchillos();

  System.out.println("\n ==== Navaja CAMPO Original ====");
  INavaja navajaCampo = new NavajaCampo();
  navajaCampo.cuchillos();
  navajaCampo.desarmadores();
  System.out.println("==== Navaja CAMPO CON EXTRAS ====");
  navajaCampo = new DesarmaTuercas(navajaCampo);
  navajaCampo.desarmadores();
  navajaCampo = new PicaHielo(navajaCampo);
  ((IPicarHielo)navajaCampo).picarhielo();
}/** SALIDA
==== Navaja Basica Original ====
Cuchillos: liso y cierra
Desarmadores: cruz y plano
==== Navaja Basica EXTRAS ====
Cuchillos: liso y cierra
                         y cortar-carne

 ==== Navaja CAMPO Original ====
Cuchillos: cierra grueza, cierra delgada
Desarmadores: cruz-G, plano-G
==== Navaja CAMPO CON EXTRAS ====
Desarmadores: cruz-G, plano-G
                         y desarma-tuercas-ajustable
Picos: pica-hielo
```

## Facade

En que consiste el patron "Facade"

- [ ] Busca simplificar el sistema para el cliente, precentando una interface <!--Correcto-->
      unidficada, para un subconjunto de subsistema definiendo una interface
      de alto nivel, lo usamos cuando tenemos un sistema complejo y queremos
      darsela al cliente de forma simplificada.

----

Cual es el diagrama del del patron "Facade"

- [ ] <br /> ![26_diagrama_facade](./img/26_diagrama_facade.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Facade"

- [ ] Tenemos una empresa con 3 diferentes fabricas, cada fabrica <!--Correcto-->
      es un systema complejo pero desde un solo punto queremos controlar
      toda la empresa, queremos evitar que el cliente maneje cosas
      complicadas.

----

## Flyweight

En que consiste el patron "Flyweight"

- [ ] Permite el uso de objetos compartidos para soportar una gran cantidad <!--Correcto-->
      de objetos que tienen la misma informacion de manera eficiente, este
      es un objeto copartido actuando independiente en cada contexto.

----

Cual es el diagrama del del patron "Flyweight"

- [ ] <br /> ![27_diagrama_flyweight](./img/27_diagrama_flyweight.PNG)

----

Es un escenario/ejemplo donde es comunmente usado el patron "Flyweight"

- [ ] Crear una lista de 500 computadoras dell, pero vemos que en la <!--Correcto-->
      informacion (ram, procesador) solo tenemos un maximo de 20 combinaciones
      diferentes, entonces implementariasmos este patron pàra realmente crear
      solo estas 20 veriantes en memoria, y de algun modo mostrarle las 500
      computadoras al usuario.

----

Es el codigo resultante del patron "Flyweight"

- [ ] A

```java
public class EnemyFactory {
  public static  enum EnemyType { TANK, ANTI_TANK }
  private static Random random = new Random();

  public static int antitanques = 0;
  public static int tanques = 0;

  public Map<EnemyType, IEnemy> enemies = new HashMap<>();

  public IEnemy getEnemy(EnemyType type) {
    IEnemy enemy = null;
/*
PARA CREAR LOS TANQUES CON MAYOR DIFERENCIA QUE EL TIPO, POR EJEMPLO
EN LA CANTIDAD DE ATAQUE, VELOCIDAD, RECISTENCIA ETC, MODIFICARIAMOS
ESTE CONDICIONAL, PARA SABER SI YA EXISTE UNO IGUAL EN LA LISTA, SI
AHI UNO IGUAL REGRESAMOS EL QUE SEA IGUAL, SI NO CREAMOS UNO NUEVO.
*/
    if(enemies.containsKey(type)) {
      enemy = enemies.get(type);
    } else {
      switch (type) {
        case TANK:
          //enemy = new Tank(random.nextInt(1)+1);
          enemy = new Tank(5);
        break;
        case ANTI_TANK:
          //enemy = new AntiTank(random.nextInt(3)+1);
          enemy = new AntiTank(10);
        break;
      }
      System.out.println("Se crea un: " + type);
      enemies.put(type, enemy);
    }
    switch (type) {
      case TANK: tanques++; break;
      case ANTI_TANK: antitanques++; break;
    }
    return enemy;
  }

  public static EnemyType randomEnum(){
    int x = random.nextInt(EnemyType.class.getEnumConstants().length);
    return EnemyType.class.getEnumConstants()[x];
  }
}
```

<!-- #########################  PLANTILLA  ############################ -->
<!-- ##################################################### -->

## PLANTILLA

En que consiste el patron "XXX"

- [ ]

----

Cual es el diagrama del del patron "XXXX"

- [ ] <br /> ![#######################](./img/#######################)

----

Es un escenario/ejemplo donde es comunmente usado el patron "XXX"

- [ ]

----

Es el codigo resultante del patron "XXX"

- [ ] A

```java

```
