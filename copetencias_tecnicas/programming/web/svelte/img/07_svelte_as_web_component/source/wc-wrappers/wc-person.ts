import MyComponent from '../src/person/App.svelte'
import type { SvelteComponent } from 'svelte'

customElements.define(
  // I recommend prefixing your custom elements, but for this example
  // I'm keeping it simple.
  'wc-person',
  class extends HTMLElement {
    _element: SvelteComponent;
    //_items = ["item 1","Item 2","Item 3"]
    
    constructor() {
      super()
  
      // Create the shadow root.
      const shadowRoot = this.attachShadow({ mode: 'open' })
  
      // Instantiate the Svelte Component
      this._element = new MyComponent({
        // Tell it that it lives in the shadow root
        target: shadowRoot,
        // Pass any props
        props: {
          // This is the place where you do any conversion between
          // the native string attributes and the types you expect
          // in your svelte components
          // items: this.getAttribute('items').split(','),
        },
      })
    }
    disconnectedCallback(): void {
      // Destroy the Svelte component when this web component gets
      // disconnected. If this web component is expected to be moved
      // in the DOM, then you need to use `connectedCallback()` and
      // set it up again if necessary.
      this._element?.$destroy();
    }
  }
)