export default class Product {
  constructor(
    public name:string = ""
  ){}

  public static fromArrayJsonLD():Product[] {
    const elem: any = document.querySelector(
      'script[type="application/ld+json"]'
    );
    const jsonld = JSON.parse(elem.innerText).products;
    const products:Product[] = jsonld.map((rawP: any) => new Product(rawP.name));
    return products
  }

}