export default class DataModel {
  constructor(
    public prop1: string = "",
    public prop2: number = 0,
    public prop3: boolean = false,
    public prop4: string[] = [],
    public prop5: any = ""
  ) {}

  public static fromJsonLD(): DataModel {
    let elem: any = document.querySelector(
      'script[type="application/ld+json"]'
    );
    let jsonld = JSON.parse(elem.innerText);
    jsonld = jsonld.person;
    let someData = new DataModel(
      jsonld.name,
      jsonld.age,
      jsonld.alive,
      jsonld.friends,
      jsonld.car
    );
    return someData;
  }
}