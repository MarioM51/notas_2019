# SvelteKit

- [SvelteKit](#sveltekit)
  - [1. Documentacion](#1-documentacion)
    - [1.1. Basicos](#11-basicos)
      - [Svelte CLI](#svelte-cli)
      - [Routing](#routing)
        - [Teoria](#teoria)
        - [Practica](#practica)
          - [Crear ruta simple](#crear-ruta-simple)
          - [Crear una ruta dinamica](#crear-una-ruta-dinamica)
          - [Crear endpoint dinamico anidado](#crear-endpoint-dinamico-anidado)
      - [Layouts](#layouts)
        - [Teoria Layouts](#teoria-layouts)
        - [Practica Layouts](#practica-layouts)
          - [Crear layout simple](#crear-layout-simple)
          - [Crear layout anidado](#crear-layout-anidado)
          - [Resetear layout](#resetear-layout)
          - [layouts error pages](#layouts-error-pages)

## 1. Documentacion

Fuente [kit.svelte.dev/docs](https://kit.svelte.dev/docs)

### 1.1. Basicos

#### Svelte CLI

Crear nuevo proyecto: `npm init svelte@next my-app`

- al ejecutarlo nos hara preguntas la configuracion, ej, si queremos
  usar typescript, ESLint, etc.

Iniciar proyecto: `npm run dev`

#### Routing

- [Fuente](https://kit.svelte.dev/docs#routing)
- [Codigo](./img/04_routing.zip)

##### Teoria

El routing funciona en base al filesystem de la carpeta **src\routes**
donde `src\routes\docs.svelte` se ejecutara en la ruta `/docs` y el
archivo `src\routes\about\index.svelte` se ejecutara en `/about`.  

Tenemos dos tipos de rutas

- pages: son los arhivos **.svelte** que entregan html
- endpoints: Son archivos **.js o .ts** que entregan comunmente json

**Parametros dinamicos**: tambien conocidos como **slugs** son las rutas
dinamicas las cuales puede procesar un solo archivo que usa la informacion
de la ruta. se declaran con **[property_name].svelte** o **[property_name].js/ts**

##### Practica

###### Crear ruta simple

1,Crear ruta simple: Solo creamos el archivo `src\routes\docs.svelte`
y accedemos a el desde `/docs`.

```svelte
<!-- src\routes\docs.svelte -->
<svelte:head>
  <title>Docs</title>
</svelte:head>
<main>
  <h1>Documentation</h1>
</main>
```

> lo mismo aplica para rutas mas profundas, ej. `src\routes\blog\rules.svelte`
> podriamos entrar a `blog/rules`

Y podremos ver

![01_ruta_simple](./img/01_ruta_simple.PNG)

###### Crear una ruta dinamica

createmos `src\routes\blog\[name].svelte` la cual resolvera rutas como

- /docs/intro
- /docs/resume

```html
<!-- src\routes\blog\[name].svelte -->
<script>
  import { page } from '$app/stores';
</script>

<main>
  <h1>Documentation</h1>
  <h2>Section: {$page.params.section}</h2>
</main>
```

podremos ver

![02_rutas_dinamicas](./img/02_rutas_dinamicas.PNG)

###### Crear endpoint dinamico anidado

Conmunmente usaremos los endpoints para regresar json, los cuales funcionan
igual solo que los archivos tienen una terminacion .ts o .js

```js
//src\routes\products\[category]\[product].json.ts
export async function get({ params }) {
  return {
    body: {
      params
    }
  };
}
```

> Lo mismo aplica para las pages (los que terminan en .svelte)

![02_endpoint_dinamico_anidado](./img/03_endpoint_dinamico_anidado.PNG)

----

#### Layouts

- [Fuente](https://kit.svelte.dev/docs#layouts)
- [Codigo](./img/09_layout_code.zip)

##### Teoria Layouts

Comunmente tenemos contenido repetible en nustra vista, como el header
y el footer a esto se le llama el layout. Para crear un layout solo
tenemos que crear el archivo `src/routes/__layout.svelte` el cual usara
el `<slot></slot>` que equivale al contenido de nuestros archivos .svelte

##### Practica Layouts

###### Crear layout simple

1, Creamos src/routes/__layout.svelte

```html
<!-- src/routes/__layout.svelte -->
<nav>
  <a href="/">Home</a>
  <a href="/about">About</a>
  <a href="/settings">Settings</a>
</nav>

<slot></slot>

<footer>
Copyright @Mario
</footer>
```

2, En automatico se anidaran nuestros archivos, y podremos ver

![05_layout](./img/05_layout.PNG)

###### Crear layout anidado

Suponiendo que tenemos una pagina /settings y tenemos `/settings/profile`
y `/settings/notifications` y queremos que tengan un submenu lo que este
dentro de `/settings` entonces podemos meter `src/routes/settings/__layout.svelte`
y este automaticamente se anidara con el de `src/routes/__layout.svelte`

```svelte
<nav>
  <h3>Menu Docs</h3>
  <ul style="background: gray;">
    <li><a href="/docs/Doc1">Doc 1</a></li>
    <li><a href="/docs/Doc2">Doc 2</a></li>
    <li><a href="/docs/Doc3">Doc 3</a></li>
  </ul>
</nav>

<slot></slot>

```

![05_layout_code](./img/06_layout_anidados_1.PNG)

Podemos ver que tenemos dos menus, los cuales uno es el layout principal
y otro es el layout anidado.

![05_layout_print](./img/06_layout_anidados_2.PNG)

###### Resetear layout

- [Fuente](https://kit.svelte.dev/docs#layouts-resets)

Igual podemos quitar/sustituir el layout si no queremos el que esta
arriba de la ruta, por ejemplo, si ponemos un layout en el root y luego
creamos una ruta `/login` puede que querramos un layout diferente, podemos
qitarlo con un archivo `__layout.reset.svelte` a la misma altura del que
quemremos quitar NOTA> tambien quitara el layout rutas mas adentro.

```svelte
<!-- src\routes\login\__layout.reset.svelte -->
<div style="background-color: bisque;">
  algun estilo bonito
</div>
<slot></slot>
```

**Resultado:** Quitamos el layout que no nesecitamos.

![07_layout_reset](./img/07_layout_reset.jpg)

###### layouts error pages

- [Fuente](https://kit.svelte.dev/docs#layouts-error-pages)

Si ocurre un error de loading se mostrara el `__error.svelte` mas cercano
ya sea por ejemplo `src/routes/settings/notifications/index.svelte` o
`src/routes/__error.svelte`

```html
<!-- src\routes\about\index.svelte -->
<script context="module">
import Error from '../__error.svelte';
  export function load({ error, status }) {
    return {
      props: {
// Causara error
        msg: status.XXX
      }
    };
  }
</script>
<script>
  export let title;
</script>
<h1>{title}</h1>
```

Creamos el sig. archivo para cambiar el error que se muestra por default,
mostrar error 404

```svelte
<!-- src\routes\__error.svelte -->

<script context="module">
  export function load({ error, status }) {
    return {
      props: { title: `${status}: ${error.message}`, code: status }
    };
  }
</script>

<script>
  export let title;
  export let code;
</script>

<h1>Ocurrio un error</h1>
<div>
{#if code == 404}
  <h2>Pagina no encontrada</h2>
{:else if code >= 500}
  <h2>Error intenta mas tarde</h2>
{:else}
  <h2>Ocurrio un error desco</h2>
{/if}
</div>
```

**Resultado:** Creamos una vista para error personalizada.

![08_custom_loading_error](./img/08_custom_loading_error.jpg)
