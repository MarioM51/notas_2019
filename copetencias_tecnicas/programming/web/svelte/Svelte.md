# Svelte

- [Svelte](#svelte)
  - [Accesos Rapidos](#accesos-rapidos)
    - [Resultados](#resultados)
  - [Ejemplo Basicos](#ejemplo-basicos)
  - [Uso TypeScript](#uso-typescript)
    - [Codigo Contador typescript](#codigo-contador-typescript)
  - [Svelte Components](#svelte-components)
  - [Controles de flujo, promesas y dobleBinding](#controles-de-flujo-promesas-y-doblebinding)
  - [Ejemplos Practicos](#ejemplos-practicos)
    - [CRUD MVVM: Routing y Stores](#crud-mvvm-routing-y-stores)
    - [Setup svelte con vite, tailwind, daysyUI, typescript, y tema personalizado](#setup-svelte-con-vite-tailwind-daysyui-typescript-y-tema-personalizado)
    - [Ligar elementos html generados dinamicamente (each) a un arreglo](#ligar-elementos-html-generados-dinamicamente-each-a-un-arreglo)
    - [Uso de libreria js nativa en Svelte: USANDO QUILL](#uso-de-libreria-js-nativa-en-svelte-usando-quill)
    - [Convertir componente svelte a componente web y usarlo en html puro](#convertir-componente-svelte-a-componente-web-y-usarlo-en-html-puro)
      - [Scripts que investigue para pasar de componente svelte a web](#scripts-que-investigue-para-pasar-de-componente-svelte-a-web)

## Accesos Rapidos

### Resultados

- Contador typescript, se vio como usar svelte con ts [.zip](./01_hello.zip)
- CRUD MVVM, navegacion, Modales, stores/observadores, con MVVM para
  crear alta interactividad: [.zip](./img/04_crud-mvvm.zip) [.Gif](./img/04_crud-mvvm.gif)

## Ejemplo Basicos

**Objetivo:** Vamos a crear un contador, veremos el uso de variables,
eventos, UI reactiva y estilos dinamicos.

0, Crear proyecto

```r
> npx degit sveltejs/template NOMBRE_PROYECTO # clona proyecto de git
> yarn install # instalar dependencias
```

A. En `src\main.js` usamos nuestro componente contador, donde podemos
ver que usa nuestro componente dentro de la etiqueta body, de nuestro
index.

```js
import App from './App.svelte';

const app = new App({
  target: document.body,
});

export default app;
```

B. En `src\App.svelte`, tenemos nuestro compoente, el cual pinta el
contador.

```html
<script>
  export let counter = 0;
  const add = () => { counter++; }
  const remove = () => { counter--; }
</script>

<main>
  <h1 class={ counter == 0 ? "en-cero" : counter < 0 ? "en-negativo" : "en-positivo"}>{counter}</h1>
  <button on:click={add} class="verde">+1</button>
  <button on:click={remove} class="rojo">-1</button>
</main>

<style>
  main {
    text-align: center;
    padding: 1em;
  }

  .verde { color: green; }
  .rojo { color: red; }

  .en-cero { color: rgb(199, 199, 199); }
  .en-negativo { color: red; }
  .en-positivo { color: rgb(0, 201, 0); }
</style>
```

Resultado: Tenemos un contador con una UI reactiva a una variable.

![Contador](./img/01_contador.gif)

<!--

######################################################################

-->

## Uso TypeScript

Fuente: [Svelte <3 TypeScript](https://svelte.dev/blog/svelte-and-typescript)

A. Instalar las siguientes dependencias, Checar que solo se agregen las
dependencias en `devDependencies`

```r
yarn add --dev @tsconfig/svelte typescript svelte-preprocess svelte-check @rollup/plugin-typescript
```

B. Agregar las siguintes lineas en `rollup.config.js`

```js
+   import autoPreprocess from 'svelte-preprocess';
+   import typescript from '@rollup/plugin-typescript';

  export default {
  export default {
// ESTA COMO  'src/main.js' CAMBIARLO A 'src/main.ts' COMO EL NOMBRE DEL ARCHIVO
+ input: 'src/main.js',
    output: {
    ...,
    plugins: [
      svelte({
+         preprocess: autoPreprocess()
      }),
+     typescript({ sourceMap: !production })
    ]
  }
```

C. Agregar el archivo './tsconfig.json' con el sig. contenido, cambiar
la comilla sinple por la doble. CUIDADO: El ide me subrallo un error,
pero al correrlo todo fue bien.

```json
  {
    "extends": "@tsconfig/svelte/tsconfig.json",

    "include": ["src/**/*", "src/node_modules"],
    "exclude": ["node_modules/*", "__sapper__/*", "public/*"],
  }
```

D. En nuestro `App.svelte` agregamos en script que queremos usar ts
de la siguiente forma. Y como **Resultado**: Podemos usar typescript
en nustro proyecto, por ejemplo.

```html
<script lang="ts">
let contador: number = 0;

const cambiarContador = (incremento: number): void => {
  contador += incremento;
}
</script>

<main>
<h1>{contador}</h1>
<button on:click={() => cambiarContador(1) }>+1</button>
<button on:click={() => cambiarContador(-1) }>-1</button>
</main>

<style>

</style>
```

### Codigo Contador typescript

**CODIGO:** [Contador typescript](./01_hello.zip)

<!-- 

######################################################################

.

######################################################################

.

######################################################################

-->

----

## Svelte Components

A. Crear componente svelte

```html
  <script lang='ts'>
// INSICAMOS QUE QUEREMOS ESTOS PROPS, 
+   export let title: string;
// VALOR POR DEFECTO, QUE PODREMOS CAMBIAR SI LO DEFINIMOS
+   export let titleColor: string = 'blue';

    let mostrar: boolean = true;
    const switchShow = ():void => { mostrar = !mostrar }
  </script>

  <article>
    <h1 style='color: {titleColor};'>{title}</h1>
    <div class={mostrar ? 'mostrar' : 'esconder'}>
      Lorem ipsum ...
    </div>
    <button on:click={switchShow} >{mostrar ? 'Esconder' : 'Mostrar'}</button>
  </article>
```

b. Usar componente Svelte

```html
  <script lang='ts'>
    import Card from './common/Card.svelte';

    let title='Mismo nombre'
    let titleColor='red'
  </script>

  <main>
    <h1>Hello {name}!</h1>
<!-- ESTAMOS USANDO EL TITLECOLOR POR DEFAULT -->
+   <Card title='Paso de props' />

<!--
  COMO TIENEN EL MISMO NOMBRE PODEMOS HACER ESTO, YA QUE IMPLISITAMENTE
  ESTAMOS DICIENDO "{title: title} {titleColor: titleColor}"
-->
+   <Card {title} {titleColor} />

  </main>
```

Resultado:

![02_basic_svelte_components](./img/02_basic_svelte_components.PNG)

<!-- 

######################################################################

.

######################################################################

.

######################################################################

-->

----

## Controles de flujo, promesas y dobleBinding

Objetivo: Vamos a crear un componente que

- Tenga controles de flujo condicional y loop
- manejo de promesas con await, then y catch, para mostrar al usuario
  4 estados, inicial, cargando, exito o fallo.
- Data-binding, para sincronizar el cambio de una var con la UI en tiempo
  real

Resultado

![03_control_de_flujo_promesas_y_dobleBinding](./img/03_control_de_flujo_promesas_y_dobleBinding.gif)

```html
<script lang='ts'>

// lOS DOS ARREGLOS SON POSIBLES RESPUESTAS DE LA PROMESA, PARA DEMOSTRAR
// QUE EL BLOQUE SE REPINTARA CON LA NUEVA RESPUESTA, Y NO SOLO SE MUESTRA Y ESCONDE
+ const normales = ["Perro","Gato"];
+ const peligrosos = ["Cocodrilo","Leon","Lobo"]

// TIEMPO EN QUE TARDARA EN RESPONDER LA PROMESA
+ const T = 2000

  let miRequest: Promise<string[]> = null;
  let inputValue = "Inicial"
  let bandera = false;
  let inputError:string = null;

  const miMetodo = ()=> {
  inputError = null
  if(inputValue.trim() === "") {
  // Si la variable es diferente a null se mostrara mensaje y no se hara peticion
    inputError = "No debe ser vacio" return
  }
  //La promesa (miRequest) tendra 4 estados null, cargando, exito, error.
  //Mostrando un bloque diferente por cada uno.
  miRequest = doRequest( (inputValue == 'falla') )
  }

  // Promesa que simula una peticion que tarda un tiempo en resulverse
  const doRequest = async (falla: boolean):Promise<string[]> => {
  bandera = !bandera;
  return new Promise<string[]>(function(resolve, reject){
    setTimeout(()=>{ let resp = (bandera) ? normales : peligrosos; resolve(resp); }, (!falla) ? T : T+100 );
    setTimeout(()=>{ reject("Cataplum");}, (falla) ? T : T+100 );
  });
  }
</script>

<main>
  <h1>Manejo de Promesas</h1>
  <!-- 'preventDefault' evitara que recarge la pagina-->
  + <form on:submit|preventDefault={miMetodo}>

  <!-- El bind:XXX hara que esten sincronizados en tiempo real, 'contenteditable' obligatorio para definir algo como notsafe-->
  +   <input type="text" bind:value={inputValue} />
  +   <span bind:innerHTML={inputValue} contenteditable></span>
  </form>

<!-- Esta parte de abajo, nos permitira imprimir un bloque segun el
estado que pueden ser, bienvenida, cargando, Exito, Error-Promesa, Error-Input, -->
  <div>
  {#await miRequest}
    <span>Cargando...</span>
  {:then miResponse } 
    {#if  miResponse === null }
      Bienbenida
    {:else}
      <span>Resultado:</span>
      <ul>
        {#each miResponse as mascota }
        <li>{mascota}</li>
        {/each}
      </ul>
    {/if}
    {:catch error}
    <div >Error de la promesa: {error}</div>
  {/await}
  {#if inputError != null }
  <div>{inputError}</div>
  {/if}
  </div>
</main>
```

<!-- 

######################################################################
######################################################################

-->

----

<!-- 

######################################################################
######################################################################

-->

## Ejemplos Practicos

### CRUD MVVM: Routing y Stores

**Objetivo:** Crear un crud interactiva con mvvm, como vemos abajo

![.Gif](./img/04_crud-mvvm.gif)

A, Creamos el ViewModel, el cual tendra.

- `Writable`; Para crear variables que pueda escuchar la vista.
- Dentro de los "Writable" tendremos `Promises` ya que son objetos que
  se obtienen de forma asincrona, y de esta forma podemos mostrar
  loadings, resultados o errores de forma mas facil.
- Leer comentarios

```ts
  import { writable, Writable, Readable, get } from "svelte/store";
  import Pagination from "../../common/src/Pagination";
  import ProductIoC from "./ProductIoC";
  import ProductModel from "./ProductModel";
  import type ProductService from "./ProductService";

  export default class ProductMV {
    private readonly ITEMS_PER_PAGE = 5;


// PARA EVITARNOS TEMAS DE MULTIPLES INSTANCIAS, SE APLICO UN SINGLETON
+   private static instance:ProductMV = null;

    private _productService:ProductService;
    private _oldProduct:ProductModel;
    
//PODEMOS VER QUE NUESTRA VISTA ESTA CONFORMADA DE ESTAS 3 VARIABLES
+   private _productListRequest:Writable<Promise<ProductModel[]>> = writable(new Promise(()=>[]));
+   private _pagination:Writable<Pagination> = writable(null);

//CUANDO `_selected` TENGA UN VALOR SE ABRIRA UN MODAL, Y CON `_isSelectedToDel`
//  ABRIREMOS UN MODAL U OTRO, Y CON `_isUpdate` SABREMOS SI ES PARA
//  ACTUALIZAR O CREAR.
//TAMBIEN ESTE ES UN 'Promise' PARA ABRIR LA POSIBILIDAD DE QUE EL USUARIO
//  AL SELECCIONAR UN PRODUCTO, PODAMOS CARGARLO DESDE EL BACK-END DE
//  SER NECESARIO.
+   private _selected:Writable<Promise<ProductModel>> = writable(null);

//PARA PODER MOSTRAR DISTINTOS MODALES Y BLOQUEAR/AVTIVAR BOTONES
    private _isUpdate:Writable<boolean> = writable(false);
    private _isLoading:Writable<boolean> = writable(false);
    private _isSelectedToDel:Writable<boolean> = writable(false);

    private constructor() {
      this._productService = ProductIoC.getService();
      this._watchActionsToSetLoading();
    }

  // PARA NO ASIGNAR EN CADA METODO EL '_isLoading', OBSERVAMOS LOS
  // PROMISES Y SE ASIGNARA DE FORMA AUTOMATICA.
+   private _watchActionsToSetLoading() {
      this._productListRequest.subscribe((w1)=>{
        this._isLoading.set(true);
        w1.then(_ => {
          this._isLoading.set(false);
        })
      });

      this._selected.subscribe((w2)=>{
        this._isLoading.set(true);
        w2?.then(_ => {
          this._isLoading.set(false);
        })
      });
    }
    
    public static getInstance():ProductMV {
      if (ProductMV.instance === null) {
        ProductMV.instance = new ProductMV();
      }
      return ProductMV.instance;
    }

//EN LA VISTA EJECUTAMOS ESTE, PARA QUE EMPIEZE A CARGAR PRODUCTOS Y A
//MOSTRAR LA PAGINACION
+   public onInit():void {

//'firstpage' ES UNA PROMESA Y LA ASIGNAMOS A LA '_productListRequest' QUE
//ES LO QUE ESTARA ESCUCHANDO LA VISTA, DE ESTE MODO PODEMOS CONTROLAR
//LOS ESTADOS WAITING, SUCCESS O ERROR MAS FACILMENTE.
+     const firstpage = this._productService.findPagination(new Pagination(this.ITEMS_PER_PAGE, 5));
+     this._productListRequest.set(firstpage);
      
      this._productService.getSize().then((totalProducts) => {
        this._pagination.set(new Pagination(this.ITEMS_PER_PAGE, totalProducts))
      });
    }

    public findPagination(isFoward: boolean):void {
      let pag = get(this._pagination);
      (isFoward) ? pag.moveNext() : pag.movePrevious();
      this._pagination.set(pag)

      const productsFinded = this._productService.findPagination(pag);
      this._productListRequest.set(productsFinded);
    }

    public findPaginationN(page: number):void {    
      let pag = get(this._pagination);
      pag.moveToPage(page);
      this._pagination.set(pag);

      const finded = this._productService.findPagination(pag);
      this._productListRequest.set(finded);
    }

    public onOpenAddForm(): void {
      this._isSelectedToDel.set(false);
      this._isUpdate.set(false);
      this._selected.set(new Promise((r, _) => { r(new ProductModel()) }));
    }

    public async onSubmitAddForm(PInForm:ProductModel): Promise<void> {
      const saveReq = this._productService.save(PInForm);
      this._selected.set(saveReq);
      const saved = await saveReq;

//DE ESTA AGREGAMOS EL PRODUCTO A LA LISTA PARA QUE SE VEA EL AMBIO EN LA UI
+     this._productListRequest.update(async (listProm) => {
        const pList = await listProm;
        pList.unshift(saved);
        if(pList.length > 5) {
          pList.pop();
        }
        this._selected.set(null);
        return listProm;
      });

//EN UI EN TIEMPO REAL SE MODIFICARA LA PAGINACION, EN CASO DE QUE AHIGA
//MAS PAGINAS ACCESIBLES
+     this._pagination.update((pag) => {
        pag.increseTotalItemsBy(1)
        return pag;
      });
    }



    public onOpenUpdateForm(toUpdate: ProductModel):void {
      this._isSelectedToDel.set(false)
      this._isUpdate.set(true)
      this._oldProduct = ProductModel.copy(toUpdate);
      this._selected.set(new Promise((r, _) => { r( toUpdate ) } ));
    }

    public async onSubmitUpdateForm(newInfo:ProductModel): Promise<void> {
      const updateReq = this._productService.update(newInfo, this._oldProduct);
      this._selected.set(updateReq);
      const updated = await updateReq;

      this._productListRequest.update(async (list) => {
        let i = (await list).findIndex((item) => { item.name == this._oldProduct.name} )
        list[i] = updated;
        return list
      });

      this._selected.set(null)
      this._oldProduct.name= null;
    }



    public onClickDelete(toDel: ProductModel):void {
      this._isSelectedToDel.set(true)
      this._selected.set(new Promise((r)=>r(toDel)))
    }

    public async delete(): Promise<void> {
//ESTE BLOQUE SIRVE PARA QUE LA PAGINACION SIEMPRE TENGA 5 ITEMS,  TRAYENDO
//EL ITEM QUE SIGUE DE LA SIG PAGINACION.
+     const list = await get(this._productListRequest)
+     let nextPageReq:Promise<ProductModel[]> = null;
+     if(list.length >= 5) {
+       const nextPag = get(this._pagination);
+       nextPag.moveNext();
+       nextPageReq = this._productService.findPagination(nextPag);
+       nextPag.movePrevious();
+     }
      
      const toDel = await get(this._selected);
      const deleteReq = this._productService.delete(toDel);

      this._selected.set(deleteReq)
      const deleted = await deleteReq;
      const nextPage = await nextPageReq;

      this._productListRequest.update(async (Plist) => {
        const PlistInfo = await Plist;
        if(nextPage != null) {
          PlistInfo.push(nextPage[0]);
        }
        return PlistInfo.filter((p) => p.name !== deleted.name)
      })

      this.pagination.update((pag)=>{
        pag.increseTotalItemsBy(-1)
        return pag;
      });

      this._selected.set(null);
    }

//NOTAR QUE NUESTROS 'Writable' LOS PASAMOS A 'Readable' ESTO PARA EVITAR
//MODIFICAR DESDE LA UI, Y DE ESTE TENER ORDEN CONTROL.
    public get selected():Readable<Promise<ProductModel>> { return this._selected; }
    public get productList():Readable<Promise<ProductModel[]>> { return this._productListRequest; }
    public get isUpdate():Readable<boolean> { return this._isUpdate; }
    public get isLoading():Readable<boolean> { return this._isLoading }
    public get isSelectedToDel():Readable<boolean> { return this._isSelectedToDel }
    public get pagination():Writable<Pagination> { return this._pagination }
  }
```

B. Modal para la eliminacion (leer comentarios)

```html
  <script lang="ts">
    import type { Readable } from "svelte/store";
    import type ProductModel from "../../src/ProductModel";
// EL VIEW-MODEL QUE CREAMOS ARRIBA
  + import ProductMV from "../../src/ProductMV";
    import Modal from "./../../../common/components/Modal.svelte";

// ES UN SINGLETON ASI QUE SOLO PODEMOS OBTENERLO ASI.
    const mv = ProductMV.getInstance();

//COMO `selected` ES UNA PROMESA Y ESTE COMPONENTE SE MONTA DESDE LA
//PRIMER CARGA, NO PODEMOS OBTENERLO DIRECTAMENTE, SI NO CUANDO ESTE
//TIENE UN VALOR, PARA ESO VER LA LINEA 'pLoaded = await $pSelected;'
+   const pSelected = mv.selected;
+   let pLoaded: ProductModel = null;

    const isSelectedToDel: Readable<boolean> = mv.isSelectedToDel;
    const isLoading: Readable<boolean> = mv.isLoading;

    let isSubmit = false;

//EQUIVALENTE AL 'getElementById' YA QUE NECESITAMOS LOS BOTONES PARA
//SIMULAR EL CLICK Y ASI ABRIRLO O CERRARLO
  + let btnOpen, btnClose: HTMLElement;

// ESCUCHAMOS POR 'pSelected' QUE CUANDO TENGA VALOR Y "isSelectedToDel"
// SEA TRUE, ABRIMOS ESTE MODAL, Y CUANDO NO TENGA VALOR LO CERRAMOS
  + pSelected.subscribe(async (p) => {
  +   if (p != null && $isSelectedToDel) {
  +     pLoaded = await $pSelected;
  +     btnOpen?.click();
  +   } else {
  +     btnClose?.click();
  +   }
  + });

    const onSubmit = () => {
      isSubmit = true;
      mv.delete();
    };
  </script>

  <Modal>
    <div slot="trigger" let:open>
<!--
ESTE MODAL SOLO LO ABRIREMOS CUANDO EL USR SELECCIONE UN ITEM
PARA ELIMINAR, Y NO DIRECTAMENTE, ASI QUE IMPOSIVILITAMOS AL USR DE HACERLO
-->
+     <input type="hidden" on:click={open} bind:this={btnOpen} />
    </div>
    <div slot="header">
      <h2>Delete Confirmation</h2>
    </div>
    <div slot="content">
<!--
AQUI PODEMOS VER COMO ESCUCHAMOS UNA PROMESA, ASI QUE EN TANTO ESTA
SEA ACTUALIZADA/ASIGNADA PODREMOS DETECTARLO AQUI, Y TAMBIEN MANEJAR
SUS 3 ESTADOS POSIBLES (WAIT, SUCCESS, ERROR)
-->
      {#await $pSelected}
        {!isSubmit ? "Loading..." : "Deleting..."}
      {:then _ }
        <form on:submit|preventDefault={() => {}}>
          Are you sure that you want to delete the product <br />
          <b>{pLoaded.name}</b>?
        </form>
      {:catch _}
        {!isSubmit ? "Error Loading" : "Error Deleting"}
      {/await}
    </div>

    <div slot="footer" style="text-align: right;" let:store={{ close }}>
      <button on:click={close} bind:this={btnClose}>Close</button>
<!--
PODEMOS DES-HABILITAR O HABILITAR UN BOTON EN TIEMPO REAL ESCUCHANDO
EL STORE.
-->
      <button on:click={onSubmit} disabled={$isLoading}>Delete</button>
    </div>
  </Modal>
```

C. Modal de formulario producto.

```html
<!--
  ES BASTANTE SIMILAR AL MODAL DE ELIMINACION
-->
<script lang="ts">
  import type { Readable } from "svelte/store";
  import type ProductModel from "../../src/ProductModel";
  import ProductMV from "../../src/ProductMV";
  import Modal from "./../../../common/components/Modal.svelte";

  const mv = ProductMV.getInstance();

  // como no se puede recuperar 'pSelected' al iniciar el componente,
  // entonces creamos 'productOnForm' para que se use mientras.
  const pSelected = mv.selected;
  let productOnForm: ProductModel = null;

  let btnOpen, btnClose: HTMLElement;

  const isSelectedToDel: Readable<boolean> = mv.isSelectedToDel;
  const isUpdate: Readable<boolean> = mv.isUpdate;
  const isLoading: Readable<boolean> = mv.isLoading;
  let isSubmit = false;

  pSelected.subscribe((p) => {
    if (p != null && !$isSelectedToDel) {
      btnOpen?.click();

      $pSelected?.then((loaded) => {
        productOnForm = loaded;
      });
    } else {
      btnClose?.click();
    }
  });

  const onSubmit = () => {
    console.log("aa");
    isSubmit = true;
    if ($isUpdate) {
      mv.onSubmitUpdateForm(productOnForm);
    } else {
      mv.onSubmitAddForm(productOnForm);
    }
  };
</script>

<Modal>
  <div slot="trigger" let:open>
    <button
      on:click={() => {
        mv.onOpenAddForm();
      }}
      disabled={$isLoading}>Add Product</button
    >
    <input type="hidden" on:click={open} bind:this={btnOpen} />
  </div>
  <div slot="header">
    <h2>{$isUpdate ? "Update Product" : " Add Product"}</h2>
  </div>
  <div slot="content">
    <form on:submit|preventDefault={() => {}}>
      {#await $pSelected}
        {!isSubmit ? "Loading..." : "Saving..."}
      {:then _}
        <input type="text" bind:value={productOnForm.name} placeholder="Name" />
        <br />
        <input
          type="number"
          bind:value={productOnForm.price}
          placeholder="Price"
        />
        <br />
      {:catch error}
        {!isSubmit ? "Error loading product" : "Error saving changes."}
      {/await}
    </form>
  </div>

  <div slot="footer" style="text-align: right;" let:store={{ close }}>
    <button on:click={close} bind:this={btnClose}>Close</button>
    <button on:click={onSubmit} disabled={$isLoading}
      >{$isUpdate ? "Update" : "Add"}</button
    >
  </div>
</Modal>
```

D. Pagina que mostrara la lista y usara ambos modales, para manejar el
crud,

```html
  <script lang="ts">
    import ProductFormModal from "./components/ProductFormModal.svelte";
    import ProductConfirmDeleteModal from "./components/ProductConfirmDeleteModal.svelte";
    import ProductMV from "../src/ProductMV";
    import { onMount } from "svelte";
    import { Writable, writable } from "svelte/store";

    const productMV: ProductMV = ProductMV.getInstance();
    const listRequest = productMV.productList;
    const isLoading = productMV.isLoading;

// LOS MANEJAMOS SEPARADOS YA QUE NECESITAMOS EL ARREGLO PARA PODER
// PINTAR LOS BOTONES QUE LLEBAN A ALGUNA PAGINA
+   const pagination = productMV.pagination;
+   const pages: Writable<number[]> = writable([]);

    pagination.subscribe((pag) => {
      let temp: number[] = [];
      for (let i = 1; i <= pag?.totalPages; i++) {
        temp.push(i);
      }
      pages.set(temp);
    });

    onMount(() => {
+     productMV.onInit();
    });
  </script>

  <div>
    <h1>Products</h1>
    <div id="products-table-container">
<!--
LOS MODALES POR DEFAULT ESTAN ESCONDIDOS, DESDE EL VIEW-MODEL Y ESCUCHAS
LOS PODREMOS ABRIR DESDE EL VIEW-MODEL.
-->
      <ProductFormModal />
      <table id="products-table" class="table table-striped">
        <thead>
          <th>Name</th>
          <th>Price</th>
          <th>Delete</th>
          <th>Edit</th>
        </thead>

        {#await $listRequest}
          <tr><td colspan="4" style="text-align: center;">Cargando...</td></tr>
        {:then list}
          {#each list as p}
            <tr>
              <td>{p.name}</td>
              <td>{p.price}</td>
              <td
                ><button on:click={() => productMV.onClickDelete(p)}>X</button
                ></td
              >
              <td
                ><button on:click={() => productMV.onOpenUpdateForm(p)}>/</button
                ></td
              >
            </tr>
          {/each}
        {:catch error}
          <tr
            ><td colspan="4" style="text-align: center; color:red;"
              >{error.message}</td
            ></tr
          >
        {/await}
      </table>

      <div id="pagination-container">
        <button
          disabled={!$pagination?.existPrevius() || $isLoading}
          on:click={() => {
            productMV.findPagination(false);
          }}>Previus</button
        >
        {#each $pages as page}
          <button
            disabled={page == $pagination.currentPage || $isLoading}
            on:click={() => {
              productMV.findPaginationN(page);
            }}
            class="btn-page">{page}</button
          >
        {/each}
        <button
          disabled={!$pagination?.existNext() || $isLoading}
          on:click={() => {
            productMV.findPagination(true);
          }}>Next</button
        >
      </div>
    </div>
    <ProductConfirmDeleteModal />
  </div>

  <style>
    #products-table-container { width: 90%; margin: auto; }
    #products-table thead th:nth-of-type(1) { width: 40%; }
    #pagination-container { text-align: center; }
    #pagination-container .btn-page:active { font-weight: bolder; }
  </style>
```

**Resultado:** Tenemos un crud como el del gif, podemos ver el codigo
completo en [.zip](./img/04_crud-mvvm.zip).

![img](./img/04_crud-mvvm.gif)

<!-- 

######################################################################
######################################################################

-->

----

<!-- 

######################################################################
######################################################################

-->

### Setup svelte con vite, tailwind, daysyUI, typescript, y tema personalizado

Fuente: [youtube](https://www.youtube.com/watch?v=5lF5PxBJoso)

- Enlaces utiles
  
  1. [Vite doc scaffolding](https://vitejs.dev/guide/#scaffolding-your-first-vite-project)
  2. [Tailwindcss install](https://tailwindcss.com/docs/installation/using-postcss)
  3. [DaisyUI install](https://daisyui.com/docs/install/)

1, Iniciar proyecto con vite, crear proyecto con vite

```r
> npm init vite@latest NOMBRE
# seleccionamos svelte, y vatiante svelte ts
> npm install
```

2, Agregar tailwind

```r
# instalar dependencia
> npm install -D tailwindcss postcss autoprefixer

#Configuracion
> npx tailwindcss init
```

2,1, Agregar tailwind a nuestro postcss, agregando *./postcss.config.js*

```js
module.exports = {
  plugins: {
    tailwindcss: {},
    autoprefixer: {},
  }
}
```

2,2, Agregar que tailwind vigile los archivos .ts y .svelte en *./tailwind.config.cjs*

```js
module.exports = {
  content: ["./src/**/*.{html,js,ts,svelte}"],
  theme: {
    extend: {},
  },
  plugins: [],
}
```

2,3, Agregamos en `src\App.svelte` el base, components y utils de tailwind

```html
  <script lang="ts">...</script>
  <main>....</main>
  <style global lang="postcss">
+   @tailwind base;
+   @tailwind components;
+   @tailwind utilities;
  </style>
```

2,4 Comprobar que funciona tailwind

```r
<main>
  <h1 class="text-3xl font-bold underline">Hello Typescript!</h1>
  <button class="px-10 py-3 bg-red-500">Hello Tailwind</button>
```

![05_svelte_con_tailwindcss](./img/05_svelte_con_tailwindcss.PNG)

3, Agregar daisyUI

3,1, Instalar daisyUI `npm i daisyui`

3,2, En *tailwind.config.js* agregar daisyUI

```js
module.exports = {
    plugins: [
      require('daisyui'),
    ],
  }
```

3,3, Comprobar que dunciona

```html
<button class="btn btn-outline btn-accent">accent</button>
```

![06_svelte_con_tailwindcss_y_daisyUI](./img/06_svelte_con_tailwindcss_y_daisyUI.PNG)

4, Agregar Tema Personalizado

TODO: No agarra el tema custom solo los por default, ver despues    

4,1, En ** agregamos un tema personalizado

```js
module.exports = {
  content: ["./src/**/*.{html,js,ts,svelte}"],
  theme: {
    extend: {},
+   daisyui: {
      themes: [{
        'mytheme': {                          /* your theme name */
            'primary' : '#a991f7',           /* Primary color */
            'primary-focus' : '#8462f4',     /* Primary color - focused */
            'primary-content' : '#ffffff',   /* Foreground content color to use on primary color */

            'secondary' : '#f6d860',         /* Secondary color */
            'secondary-focus' : '#f3cc30',   /* Secondary color - focused */
            'secondary-content' : '#ffffff', /* Foreground content color to use on secondary color */

            'accent' : '#37cdbe',            /* Accent color */
            'accent-focus' : '#2aa79b',      /* Accent color - focused */
            'accent-content' : '#ffffff',    /* Foreground content color to use on accent color */

            'neutral' : '#3d4451',           /* Neutral color */
            'neutral-focus' : '#2a2e37',     /* Neutral color - focused */
            'neutral-content' : '#ffffff',   /* Foreground content color to use on neutral color */

            'base-100' : '#ffffff',          /* Base color of page, used for blank backgrounds */
            'base-200' : '#f9fafb',          /* Base color, a little darker */
            'base-300' : '#d1d5db',          /* Base color, even more darker */
            'base-content' : '#1f2937',      /* Foreground content color to use on base color */

            'info' : '#2094f3',              /* Info */
            'success' : '#009485',           /* Success */
            'warning' : '#ff9900',           /* Warning */
            'error' : '#ff5724',             /* Error */
          },
        },
      ],
    },
    
  },
  plugins: [
    require('daisyui'),
  ],
}
```

5, Usar tema

```html
<html data-theme="NOMBRE">
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Ligar elementos html generados dinamicamente (each) a un arreglo

TODO: https://stackoverflow.com/questions/64545260/svelte-using-bindthis-inside-each-block  


<!--

#########################################

-->

----

<!--

#########################################

-->

### Uso de libreria js nativa en Svelte: USANDO QUILL


TODO: https://svelte.dev/repl/e2bbe94abb19419892442729752ee308?version=3.19.1


```js
<script>
  import { onMount } from "svelte";

  let editor;

  export let toolbarOptions = [
    [{ header: 1 }, { header: 2 }, "blockquote", "link", "image", "video"],
    ["bold", "italic", "underline", "strike"],
    [{ list: "ordered" }, { list: "ordered" }],
    [{ align: [] }],
    ["clean"]
  ];

  onMount(async () => {
    const { default: Quill } = await import("quill");

    let quill = new Quill(editor, {
      modules: {
        toolbar: toolbarOptions
      },
      theme: "snow",
      placeholder: "Write your story..."
    });
  });
</script>

<style>
  @import 'https://cdn.quilljs.com/1.3.6/quill.snow.css';
</style>

<div class="editor-wrapper">
  <div bind:this={editor} />
</div>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Convertir componente svelte a componente web y usarlo en html puro

TODO: componentes y estilos globales https://www.colorglare.com/css-resets-and-global-styles-in-web-components-c71fcea86dbd

Fuentes

- [colorglare.com(1)](https://www.colorglare.com/svelte-components-as-web-components-b400d1253504)
- [gist.github.com/nanodeath(2)](https://gist.github.com/nanodeath/2b2c233501570d68b644c1f3e21ab7ce)

Proyecto

- [.zip](./img/07_svelte_as_web_component.zip)

**Que hace el proyecto**: Compila los componentes svelte a archivos
individuales que contienen el componente web, el cual podemos usar
con html puro, el cual podrimos hacer una combinacion mas clara entre
ssr y csr.

```r
public/
  build/  # Contiene los componentes web
    wc-person.js 
    wc-products.js # cada componente tiene su archivo propio
  # html puro donde podemos importar los componentes svelte convertidos
  products/
    index.html # ej. de que las rutas se manejan igual
  index.html

source/ # Nuestro codigo de svelte
  src/
    person/ # Definimos los componentes de svelte como siempre
      App.svelte
      Doman.ts
    product/
      Product.svelte
      p_domain.ts

  wc-wrappers/ # contiene los wrappers, cada uno de estos significa un componente que se pasara al su archivo propiio en build/
    wc-person.ts
    wc-products.ts

tsconfig.json #No cambia nada
package.json #No cambia nada
rollup.config.js #Debemos de cambiar la configuracion
```

Nuestro wrapper es el mismo que en la fuentes que investique (ver titulo
de abajo), y la configuracion solo le hice unos cambios ya que estaba
levantando varios servidores en desarrollo

```js
//rollup.config.js
import svelte from 'rollup-plugin-svelte';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import sveltePreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';
import css from 'rollup-plugin-css-only';
import { readdirSync } from 'fs';

const production = !process.env.ROLLUP_WATCH;

function serve() {
  let server;

  function toExit() {
    if (server) server.kill(0);
  }

  return {
    writeBundle() {
      if (server) return;
      server = require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
        stdio: ['ignore', 'inherit', 'inherit'],
        shell: true
      });

      process.on('SIGTERM', toExit);
      process.on('exit', toExit);
    }
  };
}

let entryPoints = readFiles()

function readFiles() {
  return  readdirSync('source/wc-wrappers')
    .filter(f => f.endsWith('.ts'))
    .filter(f => !f.endsWith('.d.ts'))
    .map(f => f.substring(0, f.indexOf('.ts')));
}

export default entryPoints.map((name, i) => {

  entryPoints = readFiles();
  let isLastFile = entryPoints.length-1 <= i;
  
  return {
    input: `source/wc-wrappers/${name}.ts`,
    output: {
      sourcemap: !production,
      format: 'iife',
      name: 'app',
      file: `public/build/${name}.js`
    },
    plugins: [
      typescript({
        sourceMap: !production,
        inlineSources: !production
      }),

      svelte({
        preprocess: sveltePreprocess({
          sourceMap: !production,
          postcss: true,
        }),
        compilerOptions: {
          // enable run-time checks when not in production
          dev: !production
        }
      }),
      // we'll extract any component CSS out into
      // a separate file - better for performance
      css({ output: `${name}.css` }),
  
      // If you have external dependencies installed from
      // npm, you'll most likely need these plugins. In
      // some cases you'll need additional configuration -
      // consult the documentation for details:
      // https://github.com/rollup/plugins/tree/master/packages/commonjs
      resolve({
        browser: true,
        dedupe: ['svelte']
      }),
      commonjs(),
      typescript({
        sourceMap: !production,
        inlineSources: !production
      }),
  
      // In dev mode, call `npm run start` once
      // the bundle has been generated
      (!production && isLastFile) && serve(),
  
      // Watch the `public` directory and refresh the
      // browser on changes when not in production
      (!production && isLastFile) && livereload('public'),
  
      // If we're building for production (npm run build
      // instead of npm run dev), minify
      production  && terser(),
    ],
    watch: {
      clearScreen: false
    }
  };
});
```

<!-- importar web component, importar componente web -->
Para importar el componenete web, lo hacemos de la sig. manera

```html
<script defer src='build/name.js'></script>
```

#### Scripts que investigue para pasar de componente svelte a web

<!-- de svelte a web component, compilar a web component -->

Para pasar de componente svelte a web, se debe hacer uso de rollup que
hace el proceso de compilacion y un wrapper para definirlo en javascript
y/o html.

En la fuente (1) tenemos el seguiente wrapper el cual nos sirve para
definir el componente compilado que crea svelte, a registrarlo como
componente web, el cual definimos que etiqueta vamos a usar.

```js
import Products from '../src/product/Product.svelte'
import type { SvelteComponent } from 'svelte'

customElements.define(
  'wc-products',
  class extends HTMLElement {
    _element: SvelteComponent;
    //_items = ["item 1","Item 2","Item 3"]
    
    constructor() {
      super()
  
      const shadowRoot = this.attachShadow({ mode: 'open' })
  
      this._element = new Products({
        target: shadowRoot,
        props: {
          // items: this.getAttribute('items').split(','),
        },
      })
    }
    disconnectedCallback(): void {
      this._element?.$destroy();
    }
  }
)
```

Tambien en la fuente (1) tenemos la conf de rollup para pasar el wrapper
que contiene el componente svelte a componente web dentro de nuestra
carpeta build.

```js
import commonjs from '@rollup/plugin-commonjs'
import resolve from '@rollup/plugin-node-resolve'
import typescript from '@rollup/plugin-typescript'
import svelte from 'rollup-plugin-svelte'
import { terser } from 'rollup-plugin-terser'
import sveltePreprocess from 'svelte-preprocess'

const production = !process.env.ROLLUP_WATCH

export default {
  // The file we created with our web component wrapper.
  input: 'web-components.ts',
  output: {
    sourcemap: !production,
    format: 'iife',
    name: 'app',
    // We output it to public. This way, our svelte kit
    // app will also host the web components.
    file: 'public/web-components.js',
  },
  // Normal rollup svelte configuration. Nothing fancy
  // happening here.
  plugins: [
    typescript(),
    svelte({
      preprocess: sveltePreprocess({
        sourceMap: !production,
        postcss: true,
      }),
      // We just make sure that no global CSS is injeced
      // into the page that imports this script.
      emitCss: false,
      compilerOptions: {
        dev: !production,

        // customElement: true,
      },
    }),
    resolve(),
    commonjs(),
    // Minify the production build (npm run build)
    production && terser(),
  ],
}
```

En la fuente (2) tenemos el script para compilar todos los archivos .ts
con el componente svelte, a su respectivo archivo .js con el componente
web.

```js
import svelte from 'rollup-plugin-svelte';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import sveltePreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';
import css from 'rollup-plugin-css-only';
import { readdirSync } from 'fs';

const production = !process.env.ROLLUP_WATCH;

function serve() {
  let server;

  function toExit() {
    if (server) server.kill(0);
  }

  return {
    writeBundle() {
      if (server) return;
      server = require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
        stdio: ['ignore', 'inherit', 'inherit'],
        shell: true
      });

      process.on('SIGTERM', toExit);
      process.on('exit', toExit);
    }
  };
}

const entryPoints = readdirSync('src')
  .filter(f => f.endsWith('.ts'))
  .filter(f => !f.endsWith('.d.ts'))
  .map(f => f.substring(0, f.indexOf('.ts')));

export default entryPoints.map(name => {
  return {
    input: `src/${name}.ts`,
    output: {
      sourcemap: !production,
      format: 'iife',
      name: 'app',
      file: `../build/resources/main/public/${name}.js`
    },
    plugins: [
      svelte({
        preprocess: sveltePreprocess({ sourceMap: !production }),
        compilerOptions: {
          // enable run-time checks when not in production
          dev: !production
        }
      }),
      // we'll extract any component CSS out into
      // a separate file - better for performance
      css({ output: `${name}.css` }),
  
      // If you have external dependencies installed from
      // npm, you'll most likely need these plugins. In
      // some cases you'll need additional configuration -
      // consult the documentation for details:
      // https://github.com/rollup/plugins/tree/master/packages/commonjs
      resolve({
        browser: true,
        dedupe: ['svelte']
      }),
      commonjs(),
      typescript({
        sourceMap: !production,
        inlineSources: !production
      }),
  
      // In dev mode, call `npm run start` once
      // the bundle has been generated
      !production && serve(),
  
      // Watch the `public` directory and refresh the
      // browser on changes when not in production
      !production && livereload('public'),
  
      // If we're building for production (npm run build
      // instead of npm run dev), minify
      production && terser()
    ],
    watch: {
      clearScreen: false
    }
  };
});
```
