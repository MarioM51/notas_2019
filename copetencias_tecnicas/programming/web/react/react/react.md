# React

- [React](#react)
  - [Accesos rapidos](#accesos-rapidos)
  - [Proyectos codigo y desplieges](#proyectos-codigo-y-desplieges)
  - [Documentacion basica](#documentacion-basica)
    - [Instalar React](#instalar-react)
    - [Hola mundo de React](#hola-mundo-de-react)
    - [Introduccion a JSX](#introduccion-a-jsx)
      - [Por que JSX](#por-que-jsx)
  - [Tutorial Inicial Gato = Tic-Tac-Toe](#tutorial-inicial-gato--tic-tac-toe)
    - [Que es React](#que-es-react)
    - [Que vamos a construir](#que-vamos-a-construir)
    - [Pase de informacion](#pase-de-informacion)
    - [Haciendo componente interactivo](#haciendo-componente-interactivo)
    - [Levantando estados](#levantando-estados)
    - [Por que la inmutabilidad es importante](#por-que-la-inmutabilidad-es-importante)
    - [Componentes funcion (function component)](#componentes-funcion-function-component)
    - [Escojiendo turnos](#escojiendo-turnos)
    - [Declarar Ganador](#declarar-ganador)
    - [Agregando viaje en el tiempo](#agregando-viaje-en-el-tiempo)
      - [Guardando historial de movimientos](#guardando-historial-de-movimientos)
      - [Mostrar Historial](#mostrar-historial)
      - [Teoria Uso de Key](#teoria-uso-de-key)
      - [Implementar Viaje en el tiempo](#implementar-viaje-en-el-tiempo)
  - [Mis notas](#mis-notas)
    - [Uso de recorsos estaticos como imagenes, fuentes, etc](#uso-de-recorsos-estaticos-como-imagenes-fuentes-etc)
    - [Concatenar string](#concatenar-string)
  - [ReactJS Hooks: De 0 a Experto Creando Aplicaciones Reales](#reactjs-hooks-de-0-a-experto-creando-aplicaciones-reales)
    - [Intorduccion](#intorduccion)
      - [Por que escojer React.js](#por-que-escojer-reactjs)
      - [5. Instalando Editor de Codigo y Extensione](#5-instalando-editor-de-codigo-y-extensione)
      - [14. Conceptos basicos de React JS](#14-conceptos-basicos-de-react-js)
      - [16. Primer componente, Componente Basico](#16-primer-componente-componente-basico)
      - [6. Instalacion de React Developer Tools para Google Chrome](#6-instalacion-de-react-developer-tools-para-google-chrome)
      - [25 Como se utiliza React Developer Tools](#25-como-se-utiliza-react-developer-tools)
      - [31. Añadir SASS en un fichero externo por componentes](#31-añadir-sass-en-un-fichero-externo-por-componentes)
      - [33. ¿Que son los PropTypes y para que sirven](#33-que-son-los-proptypes-y-para-que-sirven)
      - [35. Props con Doble Destructuring](#35-props-con-doble-destructuring)
      - [36. Componentes internos Componentes dentro de Componentes](#36-componentes-internos-componentes-dentro-de-componentes)
      - [37. Children Layout](#37-children-layout)
    - [Sección 6: Los Hooks de React](#sección-6-los-hooks-de-react)
      - [Que es un Hook](#que-es-un-hook)
      - [41. useState - Creando estados en nuestros componentes](#41-usestate---creando-estados-en-nuestros-componentes)
      - [42. useState - Creando estados de tipo objetos](#42-usestate---creando-estados-de-tipo-objetos)
      - [43. useEffect - Que es y para que se usa](#43-useeffect---que-es-y-para-que-se-usa)
    - [Seccion 7: PROYECTO: Simulador de Tweets = PioPio](#seccion-7-proyecto-simulador-de-tweets--piopio)
      - [Inicio de proyecto](#inicio-de-proyecto)
      - [49. Creando el componente SendTweet y posicionando el botón de abrir el moda](#49-creando-el-componente-sendtweet-y-posicionando-el-botón-de-abrir-el-moda)
      - [50. Crearemos el componente ModalContainer y le pasaremos el contenido del modal](#50-crearemos-el-componente-modalcontainer-y-le-pasaremos-el-contenido-del-modal)
      - [51. Creando FormSendTweet para renderizar el formulario de envío de tweets](#51-creando-formsendtweet-para-renderizar-el-formulario-de-envío-de-tweets)
      - [52. Escribiendo logica para enviar el Tweet y guardarlo en el localStorage](#52-escribiendo-logica-para-enviar-el-tweet-y-guardarlo-en-el-localstorage)
      - [53. Añadiendo Toast para notificar al usuario si el Tweet se ha enviado](#53-añadiendo-toast-para-notificar-al-usuario-si-el-tweet-se-ha-enviado)
      - [54. Recuperando Tweets del localStorage y actualizándolos con el nuevo tweet](#54-recuperando-tweets-del-localstorage-y-actualizándolos-con-el-nuevo-tweet)
      - [55. Componente para mostrar todos la lista de Tweets enviados](#55-componente-para-mostrar-todos-la-lista-de-tweets-enviados)
      - [56. Componente Tweet que pintara el tweet](#56-componente-tweet-que-pintara-el-tweet)
      - [57. Función para borrar Tweets](#57-función-para-borrar-tweets)
      - [Codigo](#codigo)
    - [Seccion 8, PROYECTO, Web de Peliculas](#seccion-8-proyecto-web-de-peliculas)
      - [63.  Creando todas las paginas de nuestra aplicacion](#63--creando-todas-las-paginas-de-nuestra-aplicacion)
      - [65. Creando el Menú y enlazando las páginas](#65-creando-el-menú-y-enlazando-las-páginas)
      - [66. Creando un Hooks personsonalizado useFetch para hace peticiones http](#66-creando-un-hooks-personsonalizado-usefetch-para-hace-peticiones-http)
      - [67.69 Creando el componente SliderMovies para mostrar un slider de películas](#6769-creando-el-componente-slidermovies-para-mostrar-un-slider-de-películas)
      - [69. Componente Loading para mostrar un spinner mientras cargar](#69-componente-loading-para-mostrar-un-spinner-mientras-cargar)
      - [70.71 Componente MovieList para mostrar una lista de película](#7071-componente-movielist-para-mostrar-una-lista-de-película)
      - [72. Reutilizando el Componente MovieList para mostrar otro tipo de peliculas](#72-reutilizando-el-componente-movielist-para-mostrar-otro-tipo-de-peliculas)
      - [73. Componente Footer](#73-componente-footer)
      - [74.76 Detalles pelicula](#7476-detalles-pelicula)
      - [77. ModalVideo reproducir el trailer de la pelicula](#77-modalvideo-reproducir-el-trailer-de-la-pelicula)
      - [79. Estructura de la página Ultimos lanzamientos](#79-estructura-de-la-página-ultimos-lanzamientos)
      - [82. Añadiendo paginacion en ultimos lanzamiento](#82-añadiendo-paginacion-en-ultimos-lanzamiento)
      - [83. Reutilizando componentes para montar la página de Películas Populares](#83-reutilizando-componentes-para-montar-la-página-de-películas-populares)
      - [84. 1/3 - Creando la página para buscar películas](#84-13---creando-la-página-para-buscar-películas)
      - [87. Codigo Completo](#87-codigo-completo)
    - [Sección 9: PROYECTO: Carrito de un E-Commerce](#sección-9-proyecto-carrito-de-un-e-commerce)
      - [92. Creando el componente TopMenu](#92-creando-el-componente-topmenu)
      - [94. Creando base de datos de productos y subiéndola a api.myjson](#94-creando-base-de-datos-de-productos-y-subiéndola-a-apimyjson)
      - [96.  Creando el componente Products mostrando un loading o los productos por pantalla](#96--creando-el-componente-products-mostrando-un-loading-o-los-productos-por-pantalla)
      - [98. Componente Product para mostrar el productos en la web](#98-componente-product-para-mostrar-el-productos-en-la-web)
      - [99. Logica para añadir productos al carrito](#99-logica-para-añadir-productos-al-carrito)
      - [100. Personalizar Toast](#100-personalizar-toast)
      - [101. Recuperando productos del carrito al recargar la web](#101-recuperando-productos-del-carrito-al-recargar-la-web)
      - [102.106. Agregar Carrito, abrir-cerrar, cambio icono al agregar-vaciar, bloquer scroll](#102106-agregar-carrito-abrir-cerrar-cambio-icono-al-agregar-vaciar-bloquer-scroll)
      - [107. Creando funciones reutilizables para tratar arrays](#107-creando-funciones-reutilizables-para-tratar-arrays)
      - [108.109 listar productos del carrito](#108109-listar-productos-del-carrito)
      - [110. funcionalidad boton de incrementar y decrementar producto](#110-funcionalidad-boton-de-incrementar-y-decrementar-producto)
      - [111.112 Mostrar y calcular total de carrito en su Footer](#111112-mostrar-y-calcular-total-de-carrito-en-su-footer)
      - [Codigo Completo](#codigo-completo)
    - [Sección 10: PROYECTO: Lista de tareas con React y Firebase](#sección-10-proyecto-lista-de-tareas-con-react-y-firebase)
    - [Sección 12: Proyecto Redux y React](#sección-12-proyecto-redux-y-react)
    - [149. Configurando REDUX en el proyecto e instalaremos Redux DevTool](#149-configurando-redux-en-el-proyecto-e-instalaremos-redux-devtool)
      - [150. Abriendo y cerrando el modal y actualizando el estado en el store](#150-abriendo-y-cerrando-el-modal-y-actualizando-el-estado-en-el-store)
      - [151. Formulario agregar pio y si error guardar en el store](#151-formulario-agregar-pio-y-si-error-guardar-en-el-store)
      - [153. Guardando el tweet en el store de Redux](#153-guardando-el-tweet-en-el-store-de-redux)

## Accesos rapidos

Construir para produccion: `yarn build`

<!--

#########################################

-->

----

<!--

#########################################

-->

## Proyectos codigo y desplieges

- [Repo](https://gitlab.com/MarioM51/hala-react)
- Para sitas en el mismo doc uso `%{xxxxxx}%`

## Documentacion basica

- Fuente: [Main Concepts](https://reactjs.org/docs/hello-world.html)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Instalar React

A. Debemos de tener node instalado

<!-- Instalar react instalacion, Instalar reactjs instalacion, instalar cli instalacion -->
B. Instalamos de forma global el CLI de react `npm install -g create-react-app`

C. Recomendable, instalar la extencion de Babel en nuestro IDE

<!--

#########################################

-->

----

<!--

#########################################

-->

### Hola mundo de React

La forma mas simple de aplicacion es la siguiente

```js
import React from 'react';
import ReactDOM from 'react-dom';
ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
);
```

Al parecer constrira un index.html el cual ya hace el una comprovacion de
que el navegador ejecute javascript y un llamado al elemento root como se
ve abajo.

```html
<div id="root"></div>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Introduccion a JSX

Considerando la siguiente asignacion

```js
const element = <h1>Hello, world!</h1>;
```

Esto no es una cadena ni html, esto es llamado JSX lo cual recomendamos
usar con React, ya que de esta forma declaramos las "plantillas".

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Por que JSX

React abraza el concepto de que la logica de renderizado esta acoplado con
la logica de UI, como lo son eventos, cambios de estados sonbre el tiempo,
y como la informacion se prepara para ser mostrada.

En lugar de poner en archivos separados parte de la logica, React hace la
preocupaciones de separacion, por medio de componentes poco acoplados

## Tutorial Inicial Gato = Tic-Tac-Toe

Fuente: [Documentacion oficial](https://reactjs.org/tutorial/tutorial.html)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Que es React

Es una libreria declarativa, eficiente y flexible para construir interfaces de
usuario, haciendo uso de componentes, hacemos uso de `React.Component` del
cual usamos `this.props` para las propiedades y `render` para fabricar la vista
que queremos mostrar que llamaremos descripcion, ejemplo.

```js
class ShoppingList extends React.Component {
  render() {
    return (
      <div className="shopping-list">
        <h1>Shopping List for {this.props.name}</h1>
        <ul>
          <li>Instagram</li>
          <li>WhatsApp</li>
          <li>Oculus</li>
        </ul>
      </div>
    );
  }
}
```

Tambien estamos usando  una sintaxis llamada **JSX** que hace mas facil escribir
los componentes, ya que sin esta deberiamos de hacerlo de esta manera

```js
return React.createElement('div', {className: 'shopping-list'},
  React.createElement('h1', /* ... h1 children ... */),
  React.createElement('ul', /* ... ul children ... */)
);
```

Ahora podemos llamar al componente solamente con `<ShoppingList />` el cual funcionara
de forma independiente.

<!--

#########################################

-->

----

<!--

#########################################

-->

### Que vamos a construir

Vamos a hacer un juego de gato o tic tac, el cual tendra una lista a la derecha,
que registrara los movimientos.

<!-- iniciar proyecto -->

A. crear proyecto react `npx create-react-app my-app`

B. Eliminamos todo lo que esta dentro de `./src` y agregamos los archivos `./src/index.css`
y `./src/index.js`, en este ultimo agregamos el codigo base que nos servira para nuestro tutorial

```css
body { font: 14px "Century Gothic", Futura, sans-serif; margin: 20px; }
ol, ul { padding-left: 30px; }
.board-row:after { clear: both; content: ""; display: table; }
.status { margin-bottom: 10px; }
.square { background: #fff; border: 1px solid #999; float: left; font-size: 24px; font-weight: bold; line-height: 34px; height: 34px; margin-right: -1px; margin-top: -1px; padding: 0; text-align: center; width: 34px;}
.square:focus { outline: none; }
.kbd-navigation .square:focus { background: #ddd; }
.game { display: flex; flex-direction: row; }
.game-info { margin-left: 20px; }

```

```js
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Square extends React.Component {
  render() {
    return (
      <button className="square">
        {/* TODO */}
      </button>
    );
  }
}

class Board extends React.Component {
  renderSquare(i) {
    return <Square />;
  }

  render() {
    const status = 'Next player: X';

    return (
      <div>
        <div className="status">{status}</div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <Board />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

```

Si nos fijamos tenemos 3 componentes Square (muestra un boton), Board (muestra 9 botones),
Game (tiene un padding y usaremos este para interactuar), el cual deberemos de tener lo
siguiente

<!-- iniciar react, ejecutar proyecto react, correr react, correr proyecto react-->
Iniciar proyecto react `yarn start` en la carpeta raiz.

![01_base_tutorial](./img/01_base_tutorial.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Pase de informacion
<!-- pasar argumentos padre a hijo, pasar variables padre a hijo, pasar informacion padre a hijo, enviar argumentos padre a hijo, enviar variables padre a hijo, enviar informacion padre a hijo, pase de argumentos padre a hijo, pase de variables padre a hijo, pase de informacion padre a hijo, envio de argumentos padre a hijo, envio de variables padre a hijo, envio de informacion padre a hijo-->

**Objetivo** Vamos a enviar informacion de nuestro componente Board a nuestro
componente Squeare

```js
class Board extends React.Component {
  renderSquare(i) {
    return <Square value={i}/>;
  }...
```

Para obtenerlo usamos `prop` propio del componente de react

```js
class Square extends React.Component {
  render() {
    return (
      <button className="square">
        {this.props.value}
      </button>
    );
} }
```

**Resultado** podemos ver dentro de los cuadrados el numero que enviamos
en Board y este lo envia a Square  

![02_pase_argumentos](./img/02_pase_argumentos.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Haciendo componente interactivo

En Square agregamos un evento clic

```js
 class Square extends React.Component {
  render() {
    return (
     <button className="square" onClick={() => alert('click') }>
        {this.props.value}
      </button>
    );
  }
}
```

**Resultado** Ahora si damos clic sobre uno veremos la palabra clic en una alerta  

**Objetivo** Vamos a hacer que el Square recuerde que fue clikedo esto lo hacemos
con `state` de React.Component , esto lo hacemos configurandolo desde el constructor
de la siguinte manera

```js
class Square extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: null, };
  }

  render() {...
}
```

Setteamos el valor cambiando la alerta por la asignacion de state y tambien
mostramos la x al usuario.

```js
class Square extends React.Component {
  constructor(props) {...}

  render() {
    return (
      <button
        className="square" onClick={() => this.setState({value: 'X'})} >
        {this.state.value}
      </button>
    );
  }
}
```

**Resultado** Al darle click sobre un cuadro este mostrara una X.

![03-click-and-state](./img/03-click-and-state.gif)

**Nota:** Cuando llamamos a `setState` la inteface se actualizara auto.

<!--

#########################################

-->

----

<!--

#########################################

-->

### Levantando estados

Ahora cada Square guarda su propio estado, pero para poder intercalar entre "X" y "O",
tambien debemos de saber el estado de los 9 cuadrados para saber si hay un ganador.

Podremos pensar que el Board le pregunte a cada Square su estado, esto es posible pero
no es lo optimo, lo mejor seria guardar los estados en Board y este le diga a cada Square
que es lo que debe de mostrar.

Para esto debemos de declarar un estado compartido en board

A. Creamos un estado en Board con un arreglo de 9 reprecentando cada cuadrado

```js
class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      squares: Array(9).fill(null),
    };
  }...
```

B. En pasos antes nosotros le pasamos al Square un numerpo para mostrar, pero ahora
le debemos de pasar lo que hay en el arreglo

C. Ahora para cambiar el array le pasamos el evento clic que actualmente tiene el Square
al Board

C.1. Agregamos la funcion de `handleClick` en donde usamos `slice` para crear un clon
del arreglo y no modificarlo directamente, veremos el porque mas adelante. Lo que esta
pasando es que asignamos el evento de Square desde Board.

```js
class Board extends React.Component {
  constructor(props) {...
  renderSquare(i) {
    return <Square value={this.state.squares[i]} onClick={() => this.handleClick(i)} />;
  }

  handleClick(i) {
    const squares = this.state.squares.slice();
    squares[i] = 'X';
    this.setState({squares: squares});
  }

```

D. Vamos a hacer que el Square dibuje lo que le le enviamos por el Board, que es
su vamor correspondiente del arreglo.

E. cambiamos el onClick por ejecutar la funcion que nos llega por el board

F. Eliminamos su constructor ya que este ya no guarda un estado

```js
class Square extends React.Component {
  render() {
    return (
      <button className="square" onClick={() => this.props.onClick()} >
        {this.props.value}
      </button>
    );
  }
}
```

**Resultado:** Tenemos el mismo comportamiento, pero ahora el que controla
los cuadros es Board a travez su estado (el Array), lo cual nos da mayor control y
separacion.

![04-asignar-funcion-desde-parent](./img/04-asignar-funcion-desde-parent.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Por que la inmutabilidad es importante

Como vimos es `handleClick` creamos clon del arreglo para modificarlo y no
modificamos el original, esto crea inmutabilidad lo cual da siertas ventajas
como.

- Reduce la complejidad para caracteristicas de deteccion de cambios, por ejemplo,
  vamos a agregar un "viaje de tiempo" el cual podremos recuperar un determinado estado
  para mostrar una jugada.
- Detectar cambios en un mutable implica tener que comparar este con una copia del mismo,
  y esto implica recorrer el objeto
- Ayuda a detectar cambios lo cual facilita el uso decir cuando se debe renderizar el
  componente

Ejemplo cambio mutable

```js
var player = {score: 1, name: 'Jeff'};
player.score = 2;
// Now player is {score: 2, name: 'Jeff'}
```

Ejemplo cambio inmutable

```js
var player = {score: 1, name: 'Jeff'};

var newPlayer = Object.assign({}, player, {score: 2});
// Now player is unchanged, but newPlayer is {score: 2, name: 'Jeff'}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Componentes funcion (function component)

Estos compoentes son los que tienen unicamente el metodo `render` lo
cual es la forma mas simple de crear un componente, lo que significa
que ya no tenemos que extender de `React.Component` ya que no tienen
un estado propio, y solo debemos de pasarle un parametro con los datos
a mostrar.

**Objetivo** Vamos a hacer que Square sea un componente funcion

A. Remplazamos la clase de Square con la siguiente funcion

```js
function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}
```

**Resultado** debemos de tener el mismo comportamiento, pero el codigo
es mas simple, [Codigo Completo](./img/05_codigo_1.js)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Escojiendo turnos

Vamos a intercalar entre X y O para esto necesitamos un estado en Square el
cual sera un boleano `xIsNext` que cambiaremos en `handleClick`, tambien en
el `render` cambiaremos lo que se muestra.

```js
class Board extends React.Component {
  constructor(props) { // ``%{nnj56rtfg}%``
    super(props);
    this.state = {
      squares: Array(9).fill(null),
+     xIsNext: true,
    };
  }

   handleClick(i) {
    const squares = this.state.squares.slice();
+   squares[i] = (this.state.isXTurn) ? 'X' : 'O';
+   this.setState({
      squares: squares,
      isXTurn: !this.state.isXTurn
    });
  }

  render() {
    const status = 'Next player: ' + ((this.state.isXTurn) ? 'X' : 'O');
    ...
```

**Resultado:** Ya se debe de poder intercalar los turnos.

![06_cambio_x_o](./img/06_cambio_x_o.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Declarar Ganador

Debemos de daber cuando alguien a ganado, e indicar que ya no se pueden hacer
mas jugadas.

A. agregamos la funcion `calculateWinner` que lo calculara,
que entrara un arreglo de 9 y retornara "X", "O" ò "null".

B. Camboiamos el `render` para que muestre el ganador

C. En `` ignoramos el click en caso de que ya este marcado o al calcular
un ganador regrese algo que no sea null,

```js
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

handleClick(i) {
  const squares = this.state.squares.slice();
  if (calculateWinner(squares) || squares[i]) {
    return;
  }
  squares[i] = (th...
}

render() {
  const winner = calculateWinner(this.state.squares);
  let status;
  if (winner) {
    status = 'Winner: ' + winner;
  } else {
    status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
  }
    ...
```

**Resultado** Ya mostramos si hay un ganador y en tanto alguien gane
ignorar los demas clicks. Commit "mostrar ganador"

![07-mostrar-ganador](./img/07-mostrar-ganador.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Agregando viaje en el tiempo

Si tratamos el array de movimientos como inmutable lo que hablamos en
"Por que la inmutabilidad es importante", hacer esto sera mas facil.

#### Guardando historial de movimientos

Lo que hacemos es crear un arreglo de arreglos (una matriz) donde
cada vez que hagamos un movimiento guardaremos el array de `squares`
en un estado de `Game`.

Pero al hacer esto vamos a tener el tambien el movimiento actual de
por lo que ya no necesitaremos el arreglo en el componente `Board` asi
que.

A. Agregamos `history` el arreglo de movimientos en Game

```js
class Game extends React.Component {
   constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      xIsNext: true,
    };
  }
```

B. Eliminamos el constructor de Board

C. En el **Board** ya no tomamos los cuadros de state propio, lo tomamos
del props que nos pasa Game. por lo que sustituimos cuando llamamos a Square
lo que le pasamos a este, `this.state.squares[i]`con `this.props.squares[i]` y
`this.handleClick(i)` con `this.props.onClick(i)`

```js
class Board extends React.Component {
  renderSquare(i) {
#   return <Square value={this.props.squares[i]} onClick={() => this.props.onClick(i)} />;
  }
```

D. En el **Game** donde creamos el Board a travez de la ultima posicion del history y
tambien desde aqui vamos a calcular quien fue el ganador en `status`, y mostrarlo
en el componente del Game por lo que ya no lo necesitamos mostrar el el Board

```js
class Game extends React.Component {
  constructor(props) {...

 render() {
#   const history = this.state.history;
#   const current = history[history.length - 1];
#   const winner = calculateWinner(current.squares);
#   let status;
#   if (winner) {
#     status = 'Winner: ' + winner;
#   } else {
#     status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
#   }

    return (
      <div className="game">
        <div className="game-board">
#         <Board squares={current.squares} onClick={(i) => this.handleClick(i)} />
        </div>
        <div className="game-info">
#         <div>{status}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
```

E. Como ya estamos mstrando el ganador en game ya no lo necesitamos
ni calcularlo ni mostrarlo, asi que eliminamos de  `Board`

```js
  render() {
//  const winner = calculateWinner(this.prop.squares); let status;
//  if (winner) { status = 'Winner: ' + winner; }
//  else { status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O'); }
    return (
      <div>
<!--    <div className="status">{status}</div> --> QUITARLA AL PARECER EL COMENT NO FUNCIONA
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}...
```

F. Movemos `handleClick` del Board a Game y tambien lo modificamos ya
que estamos accediendo a los cuadros desde el history

```js
handleClick(i) {
    const history = this.state.history;
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{ //LO USAMOS EN LUGAR DE PUSH YA QUE ESTE LO HACE MUTABLE Y CONCAT HACE SU CLON
        squares: squares,
      }]),
      xIsNext: !this.state.xIsNext,
    });
  }
```

**Resultado:** Debemo de tener el mismo comportamiento, pero ahora
tenemos guardados todos los movimientos en history [Codigo completo 2](./img/08_codigo_2.js)

![Guardar History](./img/09-guardar-history.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Mostrar Historial

Como mensionammos los componentes no dejan de ser clases/objetos javascript
lo cual podemos pasar estos objetos a travez de nuestra aplicacion para
mostrarlos

Podemos usar `map()` para manejar el history por ejemplo

```js
const numbers = [1, 2, 3];
const doubled = numbers.map(x => x * 2); // [2, 4, 6]
```

Podemos usar esto para pasar el history a botones y mostrarselo al usuario.

A. En `Game` en `render` agregamos un map para crear un boton por cada
movimiento, y le pasamos el metodo `jumpTo` para cambiar la vista,
**importante** notar que le pasamos move a `jumpTo` para que este
funcione como nuestro id y asi saber a que punto volver.

```js
render() {
    ...const winner = calculateWinner(current.squares);

+   const moves = history.map((step, move) => {
      const desc = move ? 'Go to move #' + move : 'Go to game start';
      return (
        <li>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status; ...
    return (
      ...
          <div>{status}</div>
+         <ol>{moves}</ol>
        </div>
      </div>
    );
```

**Resultado:** Podemos ver el historial en forma de botones,
mas adelante le vamos a agregar funcionalidad a estos.

![Mostrar History](./img/10-mostrar-history.gif)

#### Teoria Uso de Key

Cuando hacemos un cambio React guarda cierta informacion de lo renderizado
pasa saber que fue lo que cambio, ya sea elimino, agrego, actualizo, por ejemplo,

Imagina si pasamos se este cambio

```html
<li>Alexa: 7 tasks left</li>
<li>Ben: 5 tasks left</li>
```

a esto

```html
<li>Ben: 9 tasks left</li>
<li>Claudia: 8 tasks left</li>
<li>Alexa: 5 tasks left</li>
```

Un humano podria imaginarse que paso, pero React no sabe nuestras intenciones,
por lo que debemos de definir una llave para cada elemento, en el caso del ejemplo
podriamos usar los nombres

```html
<li key={user.id}>{user.name}: {user.taskCount} tasks left</li>
```

Ahora React por medio del `key` (propiedad propia de react) haciendo una comparacion,
sabe si se elimino agrego, o modifico algo, y esta compatacion entre llaves hace que
React pueda guardar un estado entre renders.

**Un componente no puede indagar sobre su llave** por lo que se recomienda fuertmente que
cada elemento que construllas de forma dinamica le indiques una key, en caso de no hacerlo
React nos mostrara una advertencia. Solo necesitan ser unicas entre sus hermanos.

Por el metodo `jumpTo` estamos pasando el key (posicion del historial) que
vamos a mostrar

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Implementar Viaje en el tiempo

A. En nuestro caso el movimiento no se modifica, reordena y es secuensial por lo que
podemos usar `key={move}` para definirlo como key.

B. agregamos un estado `stepNumber` que guardara el movimiento.

C. Agregamos el metodo `jumpTo` el cual modificara el movimiento actual
por el movimiento que es nuestro key que le pasamos en el onclick de los
botones que representan nuestro historial, como tambien el `xIsNext` lo
pasamos X si este es par.

D. En nuestro `handleClick` cambiamos el history a un recorte (`slice`) que va
del movimiento actual al primero para que se elimine el "futuro", y tambien pasamos
el `stepNumber` al ultimo del historial con el futuro eliminado disponible, para que
cuando "retrosedamos en el tiempo" y hagamos otro movimiento no nos salgamos de lo
disponible en el historial, ya que de no cambiarlo se quedaria el stepNumber en el futuro.

E. En el `render` al asignar `current` cambiamos que hagarre el utlimo en el historial
a la pocicion de `stepNumber`

```js
class Game extends React.Component {
  constructor(props) { super(props);
    this.state = {
      ...
+     stepNumber: 0,
    };
  ...

  handleClick(i) {
#   const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares
      }]),
+     stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

+ jumpTo(step) {
    this.setState({
      stepNumber: step,
+     xIsNext: (step % 2) === 0,
    });
  }
  
  render() {
    const history = this.state.history;
#   const current = history[this.state.stepNumber];
    ...
    const moves = history.map((step, move) => {
      const desc = move ? 'Go to move #' + move : 'Go to game start';
      return (
#       <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });
    return (
      ...
    );
```

**Resultado** Al hacer un click sobre los botones de historial, regresamos
a esa jugada, [Codigo completo 3](./img/12_codigo_3_completo.js)

![Uso de historial](./img/11-uso-de-historial.gif)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Mis notas

### Uso de recorsos estaticos como imagenes, fuentes, etc

Ya que se usa webpack se construye un folder con los estaticos, y para que
webpack copie los archivos que queremos usar en nuestro peoyecto, debemos de
crear una carpeta en la raiz de nuestro proyecto llamada `./static`. Ejemplo

1. Creamos carpeta y archivo `mipriyecto/static/images/amarillo.png`

2. Ya deberiamos ser capaces de mostrar la imagen con lo siguiente

```html
<img src={`images/`+props.image} />
```

### Concatenar string

<!-- concatenar string y variable, concatenacion jsx concatenacion, concatenar jsx concatenar, concatenar concatenar jsx concatenar string y variable, concatenacion en jsx, concatenar en jsx, concatenar en jsx-->

Para concatenar 2 variables o dos valores en jsx lo hacemos de la siguiente manera

```html
<img src={`images/`+props.image} />
```

<!--

#########################################

-->

----

<!--

#########################################

-->

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## ReactJS Hooks: De 0 a Experto Creando Aplicaciones Reales

### Intorduccion

#### Por que escojer React.js

Podemos crear aplicaciones web CSR/SPA/PWA con react y SSR con Next o Gatsby,
aplicaciones de escritorio con Electron, apps de Moviles con React-Native y apps
de VR o AR con React-360

#### 5. Instalando Editor de Codigo y Extensione

<!-- instalar plugins visual studio para javascript, vs plugins React -->
Para mas comodo y rapido agregamos los siguientes plugins

- Babel Pair Colorizer, para colorizar pares de llaves y parentesis
- ES7 React/Redux/GraphQL/React-Native snippets
- IntelliSense for CSS class names in HTML, auto completa las clases css
- JavaScript (ES6) code snippets
- Path Intellisense
- Prettier - Code formatter, al guardar un fichero formatea el codigo

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 14. Conceptos basicos de React JS

Una app-web con codigo vainilla solo maneja un **Real DOM** mientras que React usa
un **Virtual DOM** este es un programa que siempre esta vijilando por cambios y
renderizarlos en tienpo real, lo cual hace que ya no sea necesario que se recarge
todos los componentes como se hace siempre.

#### 16. Primer componente, Componente Basico

A. crear un proyecto react `npx create-react-app my-app`

B. Iniciar aplicacion react `yarn start`

<!-- Crear componente. estructura componente-->
B. Creamos el componente `./src/App.js` mayuscula al inicio y debe
de tener la ultima linea, para indicar que se exporte por default, y
solo debe de retornar un elemento, en este ejemplo regresamos `<>`
denominado fragment, el cual lo ponemos cuando no queremos retornar
un div o elemento parecido.

Si el usuario no nos un dato de los que usamos en los props podemos
asignarle un valor por defecto.

+. Index

```js
import React from 'react';import ReactDOM from 'react-dom';import './index.css';import * as serviceWorker from './serviceWorker';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
serviceWorker.unregister();
```

A. Componente principal

```js
import React from 'react'; import './App.css'; import Saludar from './components/Saludar'

function App(name) {
  const usuario = { nombre: "mario", edad: 27, casado: false, apodo: "Tatemas" } //descomentar apodo para ver el default
  
  const saludar = (nombre) => { console.log("Hola " + nombre); }

  return (
    <>
      Aplicacion
      <Saludar user={usuario} saludar={saludar} />
    </>
  );
}
export default App;
```

B. Componente secundario

```js
import React from 'react';

function Saludar(props) {
  const { user } = props;
  const { apodo = "sin-apodo" } = user;

  const saludar = (nombre) => {
    console.log("Hola " + nombre);
  }
  return (//estilo en linea, estilo directo, uso de style en JSX style, estilos en JSX
    <div style={{border: `1px solid black`, width:'50%', margin: '1rem'}}>
      Usuario: {user.nombre} <br />
      Edad: {user.edad}, <br />
      Cadado { (user.casado ? "Si" : "No") } <br />
      Apodo: {apodo},
      <br />
      <button onClick={() => saludar(apodo)}>Hola</button>
    </div>
  );
}

export default Saludar;
```

**Resultado** Podemos ver el objeto usuario como ejecutar la funcion
que le pasamos por argumento desde App.js

![13_resultado_componente_basico](./img/13_resultado_componente_basico.PNG)

#### 6. Instalacion de React Developer Tools para Google Chrome

Vamos a aplicaciones, buscamos "React Developer Tools" ofrecido por Facebook

#### 25 Como se utiliza React Developer Tools

En Chrome vamos a  F12 -> pestaña components.

![14_React_developer_tools](./img/14_React_developer_tools.PNG)

Aqui podemeos ver nuestros componentes, sus variables, donde se renderiza

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 31. Añadir SASS en un fichero externo por componentes
<!-- agregar sass a proyecto, react con sass, usar sass, dependencias sass, dependencia para compilar sass, dependencia sass, dependencia scss,  -->

A. instalar dependencias `yarn add node-sass`

<!-- error al instalar node-sass error de instalason, error instalar node-sass error instalason -->
> **Error**: not found: python2 - Python not found exception due to node-sass and node-gyp.  
> **Descripcion**: AL querer instalar la dependencia no termina por sacar este error.  
> **Causa**: Al parecer una dependencia de node-sass no es compatible con windows.  
> **Solucion**: Instalar `npm install --global --production windows-build-tools`  
> **Fuente**: [link](https://stackoverflow.com/questions/45801457/node-js-python-not-found-exception-due-to-node-sass-and-node-gyp)

#### 33. ¿Que son los PropTypes y para que sirven

- Documentacion: www.reactjs.org/docs/typechecking-with-proptypes.html

Podmeos definir los tipos de variables que nos pueden llegar, por ejemplo, si
queremos que entre un string, podemos añadir la regla de que solo entre un string
en caso de que nos dara una advertensia

```js
import React from 'react'; import PropTypes from 'prop-types'; import './Saludar.css';

function Saludar(props) {
  const { user {}, saludo="Este es un saludo", saludar } = props;
  const { apodo = "sin-apodo" } = user;
  
  return (
    <div className="saludar">
      <span className="nombre">Usuario: {user.nombre} <br /></span>
      Edad: {user.edad}, <br />
      Cadado { (user.casado ? "Si" : "No") } <br />
      Apodo: {user.apodo},
      <br />
      <button onClick={() => saludar(apodo)}>Hola</button>
      <br />
      {saludo}
    </div>
  );
}

Saludar.propTypes = {
  saludo: PropTypes.string,
  saludar: PropTypes.func.isRequired
}

export default Saludar;
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 35. Props con Doble Destructuring

Antes creabamos dos lineas para para acceder a apodo lo cual debemos de acceder promero a props,
luego a user, y ya podemos acceder a apodo, esto lo podemos simplificar

```js
// OBJETO QUE ENTRA, ES DECIR, MANDAMOS DESDE APP
//const usuario = { nombre: "mario", edad: 27, casado: false, apodo: "Tatemas" }

// ANTES
// function Saludar(props) {
//   const { user, saludo="Este es un saludo", saludar } = props;
//   const { apodo = "sin-apodo" } = user;

// DOBLE Destructuring

function Saludar(props) {
  const { user:{nombre, edad, casado, apodo="sin-apodo"}, saludo="Este es un saludo", saludar } = props;
  return (
    <div className="saludar"><span className="nombre">Usuario: {nombre} <br /></span>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 36. Componentes internos Componentes dentro de Componentes

Creamos las funciones dentro del mismo archivo, aunque igual puede ser fuera de este y
los importamos, tambien los podemos exportar, pero no podremos hacerlo como default.

```js
import React from 'react'; import '../App.css';

function Formulario(props) {
  return (
    <form className="formulario">
      <MyInput label="Nombre" name="name" tipo="password" placeholder="Pon tu primer nombre" />
      <br />
      <MySubmit />
    </form>
  );
}

function MyInput(props) {
  const {label, placeholder, name, tipo="text"} = props;
  return (
    <><label>{label}</label>: <input name={name} placeholder={placeholder} type={tipo} /></>
  )
}

function MySubmit() { return 'Soy un Submit'; }
export default Formulario;
```

**Resultado:** Podemos ver como se construlle nuestro formulario que
esta conformado de dos componentes internos.

![15_Componentes_internos](./img/15_Componentes_internos.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 37. Children Layout

Con esto podemos pasar el cuerpo de algo dentro de un componente, por
ejemplo para crear un cuerpo con cabezera y pie

A. Componente Layout

```js
import React from 'react'; import '../App.css';

function Articulo(props) {
  return (
    <div className="contenedor" >
      <hr />
      Header
      <hr />
      {props.children}
      <hr />
      Footer
      <hr />
    </div>
  );
}
export default Articulo;
```

B. importamos y usamos

```js
<Articulo>
  Este texto esta en App.js
</Articulo>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

### Sección 6: Los Hooks de React

- Documentacion: [Link](https://www.reactjs.org/docs/hooks-intro.html)

#### Que es un Hook
<!-- uso de estados, como usar los estados, crear estados, uso de useState, usar useState, uso useState -->

Los Hooks son funciones con estado, antes debiamos hacer uso de clases
y de ahi usar un constructor para definir un objeto estado, que tiene
nuestras variables, como lo vimos arriba en `%{nnj56rtfg}%`.

Ahora solo demos hacer uso de `useState`, como vemos en el ejemplo de
abajo tenemos un estado `count` el cual cambiamos su valos con `setCount`
e iniciamos el valor en `cero`, y **al modificarse se re-renderiza** el
componente

```js
import React, { useState } from 'react';
function Example() {
  const [count, setCount] = useState(0); // Declara una nueva variable de estado, la cual llamaremos “count”
  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}> Click me</button>
    </div>
  );
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 41. useState - Creando estados en nuestros componentes

**Objetivo:** Vamos a crear un componente Carro el cual tenga como
estados si esta prendido y distancia, y solo podra aumentar su
distancia cuando esta encendido.

A. Cremos un componente `./components/Car.js`

```js
import React, { useState } from 'react';
function Car(props) {
  const [prendido, setPrendido] = useState(false);
  const [distancia, setDistancia] = useState(0);

  const sumDistancia = (aumento) => {
    if(prendido) { setDistancia(distancia+aumento); }
    else { alert("El carro esta apagado"); }
  }
  // cambio de clase, cambiar clase con boleano, cambiar clase usando boleano, cambiar estilo usando boleano, cambiar estilos usando boleano, cambiar estilo con boleano, intercambiar estilos con boleano, intercambiar clases con boleano
  return (
    <div className="contenedor">
      <p>
        El carro esta <b className={ prendido ? 'verde' : "rojo" }>{prendido ? 'Prendido' : "Apagado"}</b><br />
        Distancia Recorrida <b>{distancia}</b><br />
      </p>
      <button onClick={() => setPrendido(!prendido) }>Prender / Apagar</button><br />
      <button onClick={() => sumDistancia(3) }>Aumentar distancia</button>
    </div>
  );
}
export default Car;
```

**Resultado:** ver objetivo.

![16-uso-de-estados-carro-prendido-distancia](./img/16-uso-de-estados-carro-prendido-distancia.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 42. useState - Creando estados de tipo objetos

Podemos tener como estados cualquier tipo de variable, como tambien
objetos

**Objetivo** El mismo objetivo de arriba pero ahora vamos a tener
distancia y prendido dentro de un objeto.

A. Para cambiar prendido debemos de pasarle el objeto con los campos
que le queremos asignar

B. Para cambiar distancia usamos el mismo boton y evento, cambiamos el
cuerpo de la funcion

```js
import React, { useState } from 'react';

function Car(props) {
  const [carState, setCarState] = useState({prendido: true, distancia: 0});

  const sumDistancia = (aumento) => {
    if(carState.prendido) { setCarState({...carState, distancia: carState.distancia+aumento}); }
    else { alert("El carro esta apagado"); }
  }

  return (
    <div className="contenedor">
      <p>
        El carro esta <b className={ carState.prendido ? 'verde' : "rojo" }>{carState.prendido ? 'Prendido' : "Apagado"}</b><br />
        Distancia Recorrida <b>{carState.distancia}</b><br />
      </p>
      <button onClick={() => setCarState({
        ...carState, //extraemos los datos de carState
        prendido: !carState.prendido
        })}>Prender / Apagar</button><br />
      <button onClick={() => sumDistancia(3) }>Aumentar distancia</button>
    </div>
  );
}

export default Car;
```

**Resultado** Hace lo mismo que el gif de arriba, solo que ahora el
estado es un objeto.

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 43. useEffect - Que es y para que se usa

**Problematica** A veces cuando cambia un estado debemos hacer una
accion secundaria como es petcion http, re-renderizar un tercer
componente, etc. para esto asemos uso de *useEffect*

**Que es?** es una funcion que se ejecuta cuando termina el renderizado
y cuando cualquier variable que estemos usando en el componente se
actualize

```js
import React, { useState, useEffect } from 'react';
function Car(props) {
  const [carState, setCarState] = useState({prendido: true, distancia: 0});
  useEffect(() => {
    document.title = "Carro " + (carState.prendido ? 'Prendido' : "Apagado");
  })
  return (
    <div className="contenedor">
      <p>El carro esta <b className={ carState.prendido ? 'verde' : "rojo" }>{carState.prendido ? 'Prendido' : "Apagado"}</b><br /></p>
      <button onClick={() => setCarState({...carState, prendido: !carState.prendido })}>Prender / Apagar</button><br />
    </div>
  );
}
export default Car;
```

**Resultado:** Cuando inicia y cambia prendido se ejecuta useEffect

![17-uso-de-useEffect](./img/17-uso-de-useEffect.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

### Seccion 7: PROYECTO: Simulador de Tweets = PioPio

Codigo completo: [tweets.zip](./img/17-)
Demo: [Link](ada)

#### Inicio de proyecto

A. Crear Proyecto

<!-- instalar moment, instalar dependencias, dependencia moment-->
B. instalar dependencias `yarn add @material-ui/core @material-ui/icons moment`

C. descargamos [imagen](https://att-c.udemycdn.com/2019-10-14_20-31-28-f9e4f9105baadd1afe28d1a707826cba/original.png?e=1587469327&h=14768b7692887d949f3131d319c4007f)
y la ponemos en ./src/assets/img/

D. Creamos estilos para el header

```css
.header {
  position: fixed; top: 0; left: 0; width: 100%; display: flex;
  align-items: center; justify-content: center; background-color: rgb(67, 102, 136);
}
.header img { width: 60px; margin-right: 1rem; }

.header h1 { color: darkgrey; }
```

E. Creamos Header con imagen que descargamos

```js
import React from 'react';import './Header.css';import Logo from '../../assets/img/logo.png';
function Header(props) {
  return (
    <div className="header" alt="logo">
      <img src={Logo} />
      <h1>PioPio-Simulador</h1>
    </div>
  );
}
export default Header;
```

F. Creamos index.js para importar señalando solo la carpeta.

<!-- exportar por default, index para importar, index importacion, index.js para importar, index para exportar
index.js importacion index.js para componente, index.js componente, index exportacion, index.js exportacion-->

```js
export { default } from './Header';
```

J. Importamos y usamos en App.js

```js
import Header from './components/header'
```

![18_piopio_header](./img/18_piopio_header.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 49. Creando el componente SendTweet y posicionando el botón de abrir el moda

**Objetivo** Vamos a crear el boton abajo a la derecha para crear pios
por medio de una ventana modal.

A. Creamos componente en su carpeta con su estilos, Nota creamos un
index parecido al de arriba

```js
import React from 'react';
import { Fab } from '@material-ui/core'
//CUIDADO: import { AddIcon } from '@material-ui/icons/HomeIcon'
import AddIcon from '@material-ui/icons/Add';
import { moment } from 'moment'
import './AddPio.css'

function AddPio(props) {
  return (
    <div className="add-pio" >
      <Fab className="open-modal" color="primary" aria-label="add">
        <AddIcon />
      </Fab>
    </div>
  )
}
export default AddPio;
```

B. Estilos

```css
.add-pio { position: fixed !important; bottom: 1rem; right: 1rem; }

.add-pio .open-modal {
  background-color: #0093ce !important;
}
```

**Resultado** Tenemos un boton redondo al final abajo

![19-piopio-btn-flotante-abajo](./img/19-piopio-btn-flotante-abajo.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 50. Crearemos el componente ModalContainer y le pasaremos el contenido del modal

A. Creamos un nuevo componente `ModalContainer.js`, con sus estilos

```js
import React from 'react';
import { Modal } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add';

import './ModalContainer.css'

function ModalContainer(props) {
  const {isOpenModal, closeModal, children} = props;
  return (
    <Modal className="modal-container" open={isOpenModal} onClose={closeModal} closeAfterTransition>
      <div>{children}</div>
    </Modal>
  )
}
export default ModalContainer;
```

Estilos

```css
.modal-container {
  display: flex;
  align-items: center;
  justify-content: center;
}
```

B. Lo importamos nuestro componente boton que creamos arriba en `AddPio.js`

1. creamos variable de estado para saber si esta abierto o cerrado.  
2. el evento para cambier el estado.  
3. le pasamos la variable al Modal.  
4. al modal le agregamos evento para cerrarlo.

```js
import React, { useState } from 'react';import { Fab } from '@material-ui/core'import AddIcon from '@material-ui/icons/Add';import { moment } from 'moment'

+ import './AddPio.css'
+ import ModalContainer from "../ModalContainer";


function AddPio(props) {
+  const [isOpenModal, setOpenModal] = useState(false);
+  const openModal = () => { setOpenModal(true); }
+  const closeModal = () => { setOpenModal(false); }

  return (
    <div className="add-pio" >
+     <Fab onClick={openModal} className="open-modal" color="primary" aria-label="add">
        <AddIcon />
      </Fab>

+     <ModalContainer isOpenModal={isOpenModal} closeModal={closeModal}>
+       <p>Contenido del modal</p>
+     </ModalContainer>
    </div>
  )
}
export default AddPio;
```

**Resultado:** Tenemos un modal que podemos abrir click en el boton y
cerrarlo cuando hacemos click fuera de el.

![20-pio-pip-abrir-cerrar-modal](./img/20-pio-pip-abrir-cerrar-modal.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 51. Creando FormSendTweet para renderizar el formulario de envío de tweets

**Objetivo:** Ya tenemos nuestro modal que se cierra y abre pero ahora
nos falta agregarle el formulario para agregar los pios.

A. Creamos Componente que conendra nuestro formulario `FormAddPio.js`

1. Usamos el formulario de material para Formularios

```js
import React, { useState } from 'react';import { FormControl, FormGroup, TextField, Button } from '@material-ui/core'import './FormAddPio.css'
function FormAddPio(props) {  
  return (
    <div className="form-add-pio-container" >
      <h2>Crear Pio</h2>
      <form>
        <FormControl className="form-add-pio">
          <FormGroup className="name-user" ><TextField type="text" name="name" placeholder="Nombre de Usuario" margin="normal" /></FormGroup>
          <FormGroup><TextField multiline rows="6" name="pio" placeholder="Escribe tu pio" margin="normal" /></FormGroup>
          <FormGroup className="btn-crear"><Button type="submit">Crear</Button></FormGroup>
        </FormControl>
      </form>
    </div>
  )
}
export default FormAddPio;
```

Estilos

```css
.form-add-pio-container { background-color: white; border: 2px solid steelblue; padding: 2rem; }
.form-add-pio { width: 600px; }
.form-add-pio .name-user { width: 50%; }
.form-add-pio .btn-crear { background-color: lightgrey; }
```

B. Importamos a nuestro `AddPio.js` el formulario y lo ponemos dontro
de ModalContainer

```js
//ANTES
// <ModalContainer ...><p>Contenido del modal</p></ModalContainer>
<ModalContainer isOpenModal={isOpenModal} closeModal={closeModal}>
  <FormAddPio />
</ModalContainer>
```

![21-piopio-formulario](./img/21-piopio-formulario.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 52. Escribiendo logica para enviar el Tweet y guardarlo en el localStorage

**LocalStorage** Lo que haremos es guardar los Pios en `LocalStorage`

A. En `AddPio.js` creamos el metodo para guardarlo para luego pasarlo
al componente de formulario para que el lo ejecute.

1. En el metodo evitamos la accion del formulario para evitar que se
   recarge la pagina
2. Resivimos la info de formulario por `formValue` del metodo `agregarPio`

```js
import React, { useState } from 'react';import { Fab } from '@material-ui/core'import AddIcon from '@material-ui/icons/Add';import { moment } from 'moment'
import './AddPio.css';import ModalContainer from "../ModalContainer";import FormAddPio from "../FormAddPio";

function AddPio(props) {
  const [isOpenModal, setOpenModal] = useState(false);
  const openModal = () => { setOpenModal(true); }
  const closeModal = () => { setOpenModal(false); }

+ const agregarPio = (event, formValue) => {  //RECIBIMOS INFO DEL FORMULARIO
    event.preventDefault();                   //EVITAR QUE RECARGE
    const {nombre, pio} = formValue;
    let allPios = [];
    if(!nombre || !pio) {
      alert("Nombre y/o Pio vacio");
    } else {
      formValue.time = new Date();
      allPios.push(formValue);
      localStorage.setItem("pios", JSON.stringify(allPios)); //GUARDAMOS
      closeModal();
    }
    allPios = [];
  }

  return (...)//el mismo de arriba
}
export default AddPio;
```

B. En `FormAddPio.js` Agregamos el metodo en el evento onSuibmit
del formulario
<!-- Enviar valores de formulario por variable,  -->
1. Enviamos lo capturado en el formulario
2. Creamos estado que tenga los datos del formulario
3. Creamos metodo que cuando cambie la info del form la guarde en
   nuestro estado

```js
import React, { useState } from 'react';  import { moment } from 'moment'
import { FormControl, FormGroup, TextField, Button } from '@material-ui/core' import './FormAddPio.css'

function FormAddPio(props) {
+ const { agregarPio } = props;

+ const[formValue, setFormValue] = useState({ nombre: '', pio: '' });

+ const onFormChange = (event) => {
    setFormValue({
      ...formValue,
      [event.target.name]: event.target.value // equivalente a name: "mi name" y a pio: "mi Pio"
    })
  }

  return (
    <div className="form-add-pio-container" >
      <h2>Crear Pio</h2>
+     <form onSubmit={event => agregarPio(event, formValue)}  onChange={onFormChange}>
      ...
        )
}
export default FormAddPio;
```

**Resultado:** Podemos ver como al enviar nuestro formulario, el pio
se guarda en nuestro localstorage, Nota aun falta que se vayan
acumulando, ya que al guardar uno nuevo se borra el anterior.

![22-gardar-pio-en-localstorage](./img/22-gardar-pio-en-localstorage.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 53. Añadiendo Toast para notificar al usuario si el Tweet se ha enviado

**Objetivo:** Vamos a agregar un componente que se encargara de mostrar
los errores, lo haremos con `Snackbar` el cual aparecera en la parte
duperior derecha.

A. Vamos a `App.js`  

1. creamos el componente Snackbar
2. creamos `snackBarProps` un estado de tipo objeto para saber si esta
   abierto y el texto del toast
3. Le enviamo el metodo `setSnackBarProps` a `AddPio`

```js
import React,  {useState} from 'react';import './App.css';import Header from './components/header';import { Container, Snackbar } from '@material-ui/core';import AddPio from './components/AddPio'

function App(name) {
+ const [snackBarProps, setSnackBarProps] = useState({open: false, text: '' });
  return (
    <Container className="tweet-simulator" maxWidth={false} >
      <Header />
#     <AddPio setSnackBarProps={setSnackBarProps} />
+      <Snackbar
+       anchorOrigin = { {vertical: 'top', horizontal: 'right'} }
+       open={snackBarProps.open}
+       autoHideDuration={1000}
+       message={<span id="message-id">{snackBarProps.text}</span>}
      />
    </Container>
  );}
  export default App;
```

B. Vamos a `AddPio`

1. hacemos des-estruct de props para recibir `snackBarProps`
2. En el condicional cuando un Pio tiene un campo vacio cambiamos
   el texto del SnackBar osea usamos el metodo `setSnackBarProps`
3. Lo mismo cuando guardamos un Pio mostramos un Snack

```js
import React, { useState } from 'react';import { Fab } from '@material-ui/core';import AddIcon from '@material-ui/icons/Add';import './AddPio.css';import ModalContainer from "../ModalContainer";import FormAddPio from "../FormAddPio";
function AddPio(props) {
+ const { setSnackBarProps } = props
  const [isOpenModal, setOpenModal] = useState(false); const openModal = () => { setOpenModal(true); } const closeModal = () => { setOpenModal(false); }

  const agregarPio = (event, formValue) => {
    event.preventDefault(); const {nombre, pio} = formValue; let allPios = [];
    if(!nombre || !pio) {
+     setSnackBarProps({open: true, text: 'ADVERTENCIA: Nombre y/o Pio vacio'});
    } else {
      formValue.time = new Date(); allPios.push(formValue); localStorage.setItem("pios", JSON.stringify(allPios));
+     setSnackBarProps({open: true, text: 'MENSAJE: Pio Creado correctamente'});
      closeModal();
    } allPios = [];
  }
  return (... )
}
export default AddPio;
```

**Resultado:** Podemos ver un mensaje tipo Toat en arriba derecha.

![23-mostrar-mensaje-snackbar-tipo-toast](./img/23-mostrar-mensaje-snackbar-tipo-toast.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 54. Recuperando Tweets del localStorage y actualizándolos con el nuevo tweet

**Objetivo**: Podemos guardar un Pio pero no mas, ya que se borran al
agregar mas de uno, vamos a solucionar este error recuperando los Pios
que tengamos en el localStorage

A. Vamos a `App.js`

1. Creamos un estado `allPios` un arreglo que tendra todos los tweets
   con un arreglo vacio por si hay recursividad.
2. Crearemos un `useEffect` que consultara los Pios en el localStorage
3. lo recuperado lo pasamos de string a objeto con `JSON.parse` y lo
   guardamos en `allPios`
4. `allPios` y su setter se lo mandamos al componente `AddPio`

```js
import './App.css'; import Header from './components/header' import { Container, Snackbar } from '@material-ui/core' import AddPio from './components/AddPio'
+import React,  {useState, useEffect} from 'react';

function App(name) {const [snackBarProps, setSnackBarProps] = useState({ open: false, text: '' });
+ const [allPios, setAllPios] = useState([]);

+ useEffect(() => {
    const allPiosStorage = localStorage.getItem('pios');
    const allPiosArray = JSON.parse(allPiosStorage);
    setAllPios(allPiosArray);
+ }, []); // CUIDADO: si no ponemos el  [] entra en loop

  return (
    <Container className="tweet-simulator" maxWidth={false} ><Header /> cuerpo
+     <AddPio setSnackBarProps={setSnackBarProps} setAllPios={setAllPios} allPios={allPios} />
      <Snackbar anchorOrigin = { {vertical: 'top', horizontal: 'right'} } open={snackBarProps.open} autoHideDuration={1000} message={<span id="message-id">{snackBarProps.text}</span>} />
    </Container>
  );
}
export default App;
```

B. Vamos a `AddPio.js`

1. Traemos a `allPios` y su setter
2. Si localStorag tenia nada le ponemos un arreglo vacio a allPiosTemp
3. y si trajo algo lo dejamos como esta
4. Usamos el `setAllPios` ya que se agrego el Pio nuevo

```js
import React, { useState } from 'react';import { Fab } from '@material-ui/core';import AddIcon from '@material-ui/icons/Add';
import './AddPio.css'import ModalContainer from "../ModalContainer";import FormAddPio from "../FormAddPio";
function AddPio(props) {
  const [isOpenModal, setOpenModal] = useState(false); const openModal = () => { setOpenModal(true); } const closeModal = () => { setOpenModal(false); }

# const { setSnackBarProps, setAllPios, allPios } = props;

  const agregarPio = (event, formValue) => {
    event.preventDefault();
    const {nombre, pio} = formValue;
#   let allPiosTemp = allPios;
+   if(!allPiosTemp) {
      allPiosTemp = [];
    }
    if(!nombre || !pio) {
     setSnackBarProps({open: true, text: 'ADVERTENCIA: Nombre y/o Pio vacio'});
    } else {
      formValue.time = new Date();
      allPiosTemp.push(formValue);
      localStorage.setItem("pios", JSON.stringify(allPiosTemp));
+     setAllPios(allPiosTemp);
      setSnackBarProps({open: true, text: 'MENSAJE: Pio Creado correctamente'});
      closeModal();
    }
  }
  return (...)
}
export default AddPio;
```

**Resultado** Podemos crear los pios desde la primera vez que abrimos
la aplicacion.

![24-guardar-mas-de-un-pio-en-localstorage](./img/24-guardar-mas-de-un-pio-en-localstorage.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 55. Componente para mostrar todos la lista de Tweets enviados

**Objetivo:** Ya guardamos los Pios ahora vamos a hacer que se muestren

A. Creamos componente `ListPios.js`

1. Traemos todos los Pios que van a entrar por `props`
2. Agregamos condicional que si no hay Pios indicarselo al usuario
3. Mostramos los Pios usando Map

```js
//+ archivo nuevo
import React, { useState } from 'react';import './ListPios.css';
import { Grid } from '@material-ui/core'

function ListPios(props) {
  const { allPios } = props;

  if(!allPios || allPios.length===0 ) {
    return (
      <div className="pio-list-emply">No Ningun Pio agregado</div>
    )
  }

  return (  
    <Grid container spacing="3" className="" className="pio-list">
      {allPios.map(({nombre, pio, fecha}, key) => (
        <Grid key={key} item xs="4">
          <p>NOmbre: {nombre}</p>
        </Grid>
      ))}
    </Grid>
  )
}
export default ListPios;
```

B. Agregamos Pios a App.js pasandole el arreglo con todos los Pios son
solo 2 lineas (no vale poner el codigo).

**Resultado** Podemos ver la lista de Pios como aquellos nuesvos que
vamos agregando.

![25-mostrar-lista-de-pios](./img/25-mostrar-lista-de-pios.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 56. Componente Tweet que pintara el tweet

**Objetivo:**  Vamos mostrar los Pios adecuadamente, haciendo uso de
un componente nuevo.

A. Creamos componente `Pio.js`

```js
import React from 'react'; import { Card, CardContent } from '@material-ui/core' import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone' import moment from 'moment'; import './Pio.css'
function Pio(props) {
  const { pio:{nombre, pio, time} } = props;
  return (  
    <Card className="pio-container">
      <CardContent className="pio">
        <header>
          <h5>{nombre}</h5>
          <DeleteTwoToneIcon onClick={() => {console.log("Eliminando")} } />
        </header>
        <p>{pio}</p>
        <div className="pio-footer">
          {moment(time).format("YYYY/MM/DD HH:mm")}
        </div>
      </CardContent>
    </Card>
  )
}
export default Pio;
```

B. Le asignamos estilos

```css
.pio-container .pio { position: relative; }
.pio-container .pio header { display: flex; align-items: center; justify-content: space-between; }
.pio-container .pio header h5 { margin: 0; }
.pio-container .pio header svg:hover { cursor: pointer; fill: red; }
.pio-container .pio .pio-footer { position: absolute; bottom: 5px; right: 5px; font-size: 0.8em; }
```

C. Lo utilizamos en `ListPios.js` importamos y lo ponemos donte de Grid
para conservar estructura

```js
import React from 'react';import { Grid } from '@material-ui/core';import Pio from '../Pio';
+ import './ListPios.css'

function ListPios(props) {
  const { allPios } = props;
  if(!allPios || allPios.length===0 ) { return ( <div className="pio-list-emply" > No Ningun Pio agregado </div> ) }
  return (  
    <Grid container spacing={3} className="pio-list"> {allPios.map((pio, key) => (
        <Grid key={key} item xs={4} >
+         <Pio pio={pio} />
        </Grid>
      ))} </Grid>
  )
}
export default ListPios;
```

**Resultado:** Podemos ver nustros Pios de mejor manera

![26-piopio-mostrar-lista-pios-en-su-componente](./img/26-piopio-mostrar-lista-pios-en-su-componente.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 57. Función para borrar Tweets

A. En `App.js`
2. Agregamos funcion de eliminar `deletePio`
3. Pasamos la funcion a `ListPios`

```js
import React,  {useState, useEffect} from 'react';import './App.css';import Header from './components/header'import { Container, Snackbar } from '@material-ui/core'import AddPio from './components/AddPio'import ListPios from './components/ListPios'
function App(name) {
  const [allPios, setAllPios] = useState([]); const [snackBarProps, setSnackBarProps] = useState({open: false, text: '' });

  useEffect(() => { const allPiosStorage = localStorage.getItem('pios'); const allPiosArray = JSON.parse(allPiosStorage); setAllPios(allPiosArray); }, []);

+ const deletePio = (index) => {
    allPios.splice(index, 1);
    setAllPios(allPios);
    localStorage.setItem('pios', JSON.stringify(allPios));
  }

  return (
    <Container className="tweet-simulator" maxWidth={false} > <Header />

+     <ListPios allPios={allPios} deletePio={deletePio} />

      <AddPio setSnackBarProps={setSnackBarProps} setAllPios={setAllPios} allPios={allPios} /> <Snackbar anchorOrigin = { {vertical: 'top', horizontal: 'right'} } open={snackBarProps.open} autoHideDuration={1000} message={<span id="message-id">{snackBarProps.text}</span>} />
    </Container>
  );
}
export default App;
```

B. En `ListPios` le pasamos la funcion a Pios como el index que nos da
al usar el map, (nada complejo)

C. En `Pio.js` asemos que se ejecute la funcion de eliminar, y le
pasamos el index que resivimos por props.

```js
import React from 'react';import { Card, CardContent } from '@material-ui/core'import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone'import moment from 'moment';import './Pio.css'
function Pio(props) {
+ const { pio:{nombre, pio, time}, deletePio, index } = props;
  return (  
    <Card className="pio-container">
      <CardContent className="pio">
        <header> <h5>{nombre}</h5>
+         <DeleteTwoToneIcon onClick={() => deletePio(index) } />
        </header>
        <p>{pio}</p>
        <div className="pio-footer"> {moment(time).format("YYYY/MM/DD HH:mm")} </div>
        </CardContent>
    </Card>
  )
}
export default Pio;
```

<!-- no se actualiza ui, actualizar ui al cambiar estado, re-renderizar al hacer cambio, no se actualiza interface, actualizar interface al cambiar estado, rerenderizar al hacer cambio-->
<!-- cambio no se refleja en la interface, cambio no se refleja en la ui, no cambia la ui, no cambia la interface, no se actualiza la interface -->
**Error-Problematica**: Hasta este punto si vemos si se eliminan los
Pio de localStoerage pero no se ve reflejado el cambio en la UI.

**Objetivo**: Vamos a hacer que el cambio se vea en la interface.

A. En `App`

1. Creamos estado `reloadPios` como falso
2. En el `useEffect` que se ejecutara cuando este pase a true, lo
   pasamos a falso, y ponemos `,[setReloadPios]` para no caer en
   recusividad.
3. En el metodo `deletePio` agregamos el `setReloadPios(true)` para
   que se renderize al eliminar un Pio, entrara al `useEffect` y
   pasara a falso

```js
import React,  {useState, useEffect} from 'react';import './App.css';import Header from './components/header';import { Container, Snackbar } from '@material-ui/core';import AddPio from './components/AddPio';import ListPios from './components/ListPios';

function App(name) {
  const [allPios, setAllPios] = useState([]);
  const [snackBarProps, setSnackBarProps] = useState({open: false, text: '' });

+ const [reloadPios, setReloadPios] = useState(false);

  useEffect(() => { const allPiosStorage = localStorage.getItem('pios'); const allPiosArray = JSON.parse(allPiosStorage); setAllPios(allPiosArray);
+   setReloadPios(false);
+ }, [reloadPios]); // CUIDADO: si no ponemos el  [] entra en loop

  const deletePio = (index) => { allPios.splice(index, 1); setAllPios(allPios); localStorage.setItem('pios', JSON.stringify(allPios));
+   setReloadPios(true);
  }
  return (... );
}
export default App;
```

**Resultado:** Podemos eliminar los Pios del localstorage y ver el
cambio en la ui, fijarse que al eliminar todos los mensajes se muestra
el mensaje de no hay pios

![27-eliminar-pios-del-local-storage-y-mostrar-cambio-en-ui](./img/27-eliminar-pios-del-local-storage-y-mostrar-cambio-en-ui.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Codigo

- comprimido Codigo completo  [.zip](./img/28-piopio-CODIGO-COMPLETO.zip)
- Carpeta: [carpeta](E:\cursos\0activ\web\react\piopio)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

### Seccion 8, PROYECTO, Web de Peliculas

A. Nos creamos una cuenta en www.themoviedb.org para consultar su
pagina

1. Vamos a (logo de mi usuario) -> Settings -> API -> Create, aceptamos
   y le damos los datos que nos piden, (los que sean) solo confirman
   correo

B. Instalamos dependencias `yarn add node-sass react-router-dom antd moment`

1. `react-router-dom` - Para la navegacion entre paginas
2. `antd` - tipo material design

<!-- como usar antd, uso de antd, implementar antd, importar antd - como usar ant Design , uso de ant Design, implementar ant Design, importar ant Design, usar iconos antd iconos, usar iconos de antd, uso de iconos antd  -->
C. Como usar ant Design = antd en react

En nuestro index importamos (que quede arriba de index.css)

```js
+ import 'antd/dist/antd.css'
  import './index.css';
```

En nuestro componente importamos el componente y su logo [fuente](https://ant.design/components/button/#header)

```js
+ import { SearchOutlined } from '@ant-design/icons';
+ import { Button } from 'antd'
  function App() {
    return (
+     <Button type="primary" shape="circle" icon={<SearchOutlined />} />
```

**Resultado** Podemos ver el siguinte boton.

![29-uso-de-ant-design-o-antd](./img/29-uso-de-ant-design-o-antd.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 63.  Creando todas las paginas de nuestra aplicacion

Crearemos las paginas para luego hacer la configuracion del routing

A. Creamos carpeta pages y ponemos todas la paginas

B. Creamos la configuracion de rutas en `App.js`

Nota: el Switch sirve para que solo carge una, si ahi mas iguales no las cargara

```js
import React from 'react';import './App.css';import { Layout } from 'antd'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';import Movie from './pages/Movie';import NewMovies from './pages/NewMovies';import PopularMovies from './pages/PopularMovies';import SearchMovies from './pages/SearchMovies';import Error404 from './pages/error404';

function App() {
  const { header, content } = Layout;
  return (
    <Layout>
      <Router>
        <header>
          Mi Header
        </header>
        <content>
          <Switch>
            <Route path="/" exact={true} >
              <Home />
            </Route>
            <Route path="/movie/:id" exact={true} >
              <Movie />
            </Route>
            <Route path="/new-movies" exact={true} >
              <NewMovies />
            </Route>
            <Route path="/populars" exact={true} >
              <PopularMovies />
            </Route>
            <Route path="/search" exact={true} >
              <SearchMovies />
            </Route>
            <Route path="*">
              <Error404 />
            </Route>
          </Switch>
        </content>

      </Router>
    </Layout>
  );
}
export default App;
```

**Resultado:** Si ponemos cada una de las rutas indicadas en `path`
veremos el componente que esta dentro de aquel que coinside.

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 65. Creando el Menú y enlazando las páginas

A. crear menu de navegacion `MenuTop`

```js
import React from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';
import { ReactComponent as Logo } from '../assets/images/original.svg';
import './MenuTop.scss';

function MenuTop() {

  return (
    <div className="menu-top">
      <div className="logo">
        <Logo />
      </div>
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}
         style={{ lineheight: '64px'}}>
        <Menu.Item key="1">
          <Link to="/">Home</Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/new-movies">Estrenos</Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/populars">Populares</Link>
        </Menu.Item>
        <Menu.Item key="4">
          <Link to="/search">Buscador</Link>
        </Menu.Item>
      </Menu>
    </div>
  );
}
export default MenuTop;
```

B. Importamos nuestro menu

```js
  import React from 'react';import './App.css';import { SearchOutlined } from '@ant-design/icons';import { Button } from 'antd'
  import { Layout } from 'antd';import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
+ import MenuTop from './components/MenuTop';
function App() {
  const { header, content } = Layout;
  return (
    <Layout>
      <Router>
        <header>
+         <MenuTop />
        </header>
        <content>
          <Switch> ... </Switch>
        </content>
      </Router>
    </Layout>
  );
}
export default App;
```

**Resultado:** Tenemos nuestro menu de navegacion.  
![30-peliculas-menu-de-navegacion](./img/30-peliculas-menu-de-navegacion.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 66. Creando un Hooks personsonalizado useFetch para hace peticiones http
<!-- hacer peticion http, realizar peticion http, consulta http, consumo http - hacer peticion get, realizar peticion get, consulta get - hacer peticion a api, realizar peticion a api, consultar api, consumir api - hacer peticion a api, realizar peticion a api, consulta a api, consumo de api-->
A. Creamos nuestro Hook `useFetch.js`

1. Lo metenemos en su propia carpeta
2. creamos los estado boleano `isLoading` para saber si esta cargando,
   `error` para saber hubo un error y `result` para guardar la peticion
3. crear `useEffect` en donde mandamos la peticion, es una sincrona

<!-- %{tfcvghn}% -->
```js
import { useEffect, useState } from 'react';

export default function useFetch(url, options) {

  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [result, setResult] = useState(null);

  useEffect(() => {
    (async () => {
      try {
        const res = await fetch(url, options); // mandamos la peticion
        const resJson = await res.json(); // la respuesta la pasamos a json
        setResult(resJson); // guardamos el resultado
        setIsLoading(false); // indicamos que ya acabo de cargar
      } catch (error) {
        setIsLoading(false);
        setError(error);
      }
    })(); // CUIDADO CON LOS ULTIMOS "()" SI LOS OLVIDAMOS HAY PROBLEMAS CON LA IMPORTACION
  }, [url, options]);

  return {isLoading, result, error}; //CUIDADO SON LLAVES NO PARENTESIS
}

```

B. Lo usamos en nuestro `Home`

1. Vemos la [documentacion de la api](https://developers.themoviedb.org/3/movies/get-now-playing)
   generamos la peticion=url-get en "try it out" ahi mismo.
2. Importamos y usamos la peticion

```js
  import React from 'react';
+ import useFetch from "../hooks/useFetch";
  export default function Home() {
+   const movies = useFetch('https://api.themoviedb.org/3/movie/now_playing?api_key=78785c5aa3ef8df607e2b32524f6fa42&language=es-Es&page=1');
    console.log(movies);
    return (...);
}
```

**Resultado:** Podemos hacer una peticion http, la cual mostramos en la
consola, y nos indica si esta cargando

![31-peliculas-peticion-get-a-api](./img/31-peliculas-peticion-get-a-api.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 67.69 Creando el componente SliderMovies para mostrar un slider de películas

A. Creamos constantes para nuestras peticiones `Constants.js`

```js
export const API_URL = "https://api.themoviedb.org/3";
export const API_TOKEN = "api_key=78785c5aa3ef8df607e2b32524f6fa42";
export const API_LANG = "language=es-Es";
```

B. En `Home`

1. Hacemos la peticion a de peliculas y el resultado se lo pasamos a SliderMovies

```js
  import React from 'react';from '../utils/Constants';import SliderMovies from "../components/SliderMovies";
+ import {API_URL, API_TOKEN, API_LANG} import useFetch from "../hooks/useFetch";
export default function Home() {
  const urlMovies = `${API_URL}/movie/now_playing?${API_TOKEN}&${API_LANG}&page=1`;
  const movies = useFetch(urlMovies);
  return (
+   <><SliderMovies movies={movies} /></>
  );
}
```

C. Creamos un nuevo componente SliderMovies

1. dentro de `movies` que le pasamos desde Home, las pelicualas estan
   dentro de result, asi que este lo iteramos con un `map` para hacer
   otro componente que haremos mas abajo
2. Creamos componente Movie que hara significara cada elemento del
   slider.

```js
import React from 'react';import {Button, Carousel } from 'antd';import './SliderMovies.scss';import { Link } from 'react-router-dom';
function SliderMovies(props) {
  const { movies } = props;
  if(movies.isLoading || !movies.result ) { return "loading..."; }
  return (
    <Carousel autoplay className="slider-movies">
      {movies.result.results.map(movie => (
        <Movie key={movie.id} movie={movie} />)
      )}
    </Carousel>
  );
}

export default SliderMovies;
function Movie(props) {
  const {movie: {id, backdrop_path, title, overview} } = props;
  const backdrop_url = `https://image.tmdb.org/t/p/original${backdrop_path}`;
  return (
    <div className="slider-movies__movie" style={{ backgroundImage: `url('${backdrop_url}')`}}>
      <div className="slider-movies__movie-info">
        <div>
          <h2>{title}</h2>
          <p>{overview}</p>
          <Link to={`movie/${id}`}>
            <Button type="primary">Ver Mas</Button>
          </Link>
        </div>
      </div>
    </div>
  );
}
```

D. Agregamos estilos

```scss

.slider-movies {
  height: 500px;
  &__movie { height: 500px; background-size: cover; background-repeat: no-repeat; background-position: center top; }
  &__movie-info { display: flex; align-items: flex-end; height: 100%; padding-bottom: 40px;
    > div { background-color: #a7a7a7cf; margin: 0 auto; text-align: center; width: 50%; padding: 20px; }
  }
}
```

**Resultado:** Podemos ver nuestro Carrucel que se mueve mostrando
diferentes peliculas.

![32-peliculas-Carucel-slider](./img/32-peliculas-Carucel-slider.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 69. Componente Loading para mostrar un spinner mientras cargar

Para las personas que tengan una red lenta crearemos un componente a
mostrar, para que no crea que esta atorada la pagina sin hacer nada.

A. Creamos componente

```js
import React from 'react';import './Loading.scss';import { Spin } from 'antd';
function Loading(props) {
  return (
    <div className="loading">
      <Spin size="large" />
    </div>
  );
}
export default Loading;
```

B. Ponemos estilos

```css
.loading {
  height: 300px;
  display: flex;
  align-items: center;
  justify-content: center;
}
```

c. Lo mostramos en `Home`

```js
function SliderMovies(props) { const { movies } = props;
  if(movies.isLoading || !movies.result ) {
+   return  <Loading />;
  }
  ...
```

**Resultado:** Podemos ver el spiner cuando esta cargando.

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 70.71 Componente MovieList para mostrar una lista de película

A. Creamos un componente MovieList

1. Como es una peticion http pude que tarde algo, asi que mostramos
   el componente Loading por un momento
2. Creamos nuestra lista que es un elemento de antd le podemos pasar
   el datasource y el item que es un componente que creamos abajo

```js
import React from 'react';import { List, Button, Avatar } from 'antd';import { RightOutlined } from '@ant-design/icons';import Loading from '../Loading';import { API_IMAGES } from '../../utils/Constants';import { Link } from 'react-router-dom';
import './MovieList.scss';
function MovieList(props) {
  const { title, movies } = props;

  if(movies.isLoading || !movies.result ) { return  <Loading />; }

  return (
    <List dataSource={movies.result.results} className="movie-list"
          renderItem={movie => <RenderMovie movie={movie} />}
          size="default" header={<h2>{title}</h2>} bordered >
    </List>
  );
}

function RenderMovie(props) {
  const { movie:{id, title, poster_path} } = props;
  const posterPath = API_IMAGES + poster_path;

  return (
    <List.Item className="movie-list??movie">
      <List.Item.Meta avatar={<Avatar src={posterPath} />} title ={<Link to={`/movie/${id}`}>{title}</Link>} />
      <Link to={`/movie/${id}`}>
        <Button type="primary" icon={<RightOutlined />}></Button>
      </Link>
    </List.Item>
  );
}
export default MovieList;
```

B. Creamos estilos

```css
.movie-list {
  width: 80%; margin: 0 auto !important; margin-top: 2rem !important; border-color: #001529 !important;
  .ant-list-header {
    background-color: #001529;
    h2 { text-align: center; margin: 0; color: white; }
  }
  .ant-list-items { height: 500px; overflow: scroll; }
  &__movie {
    .ant-list-items-meta { align-items: center; }
    .ant-list-items-meta-title { margin: 0; }
  }
}
```

B. En Home

1. Lo importamos a home dentro de un row y col para dividir el espacio
   a la mitad

```js
import React from 'react';import useFetch from "../hooks/useFetch";import {API_URL, API_TOKEN, API_LANG} from '../utils/Constants';import SliderMovies from "../components/SliderMovies";import MovieList from "../components/MovieList";import { Row, Col } from 'antd';
export default function Home() {
  const urlMovies = `${API_URL}/movie/now_playing?${API_TOKEN}&${API_LANG}&page=1`; const movies = useFetch(urlMovies);
+ const urlPopulars = `${API_URL}/movie/popular?${API_TOKEN}&${API_LANG}&page=1`;
+ const popularsResp = useFetch(urlMovies);
  
  return (
    <>
      <SliderMovies movies={movies} />
+     <Row>
+       <Col span="12">
+         <MovieList title="Peliculas populares" movies={popularsResp} />
+       </Col>
+     </Row>
    </>
  );
}

```

**Resultado:** Mostramos la lista de peliculas que nos llego de la api
y con un scroll para que no ocupen tanto espacio.

![33-lista-de-peliculas-con-scroll](./img/33-lista-de-peliculas-con-scroll.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 72. Reutilizando el Componente MovieList para mostrar otro tipo de peliculas

En Home

1. Creamos otra peticion para las peliculas Top
2. Reutilizamos el componente

```js
import React from 'react';import useFetch from "../hooks/useFetch";import {API_URL, API_TOKEN, API_LANG} from '../utils/Constants';import SliderMovies from "../components/SliderMovies";import MovieList from "../components/MovieList";import { Row, Col } from 'antd';

export default function Home() {
  const urlMovies = `${API_URL}/movie/now_playing?${API_TOKEN}&${API_LANG}&page=1`; const movies = useFetch(urlMovies); const urlPopulars = `${API_URL}/movie/popular?${API_TOKEN}&${API_LANG}&page=1`; const popularsResp = useFetch(urlPopulars);
+ const urlTop = `${API_URL}/movie/top_rated?${API_TOKEN}&${API_LANG}&page=1`;
+ const topResp = useFetch(urlTop);
  
  return (
    <><SliderMovies movies={movies} />
      <Row>
        <Col span="12"> <MovieList title="Peliculas populares" movies={popularsResp} /> </Col>
+       <Col span="12">
+         <MovieList title="Top peliculas mas puntuadas" movies={topResp} />
+       </Col>
      </Row>
    </> );
}
```

![34-peliculas-rutilizar-peliculas-lista-peliculas](./img/34-peliculas-rutilizar-peliculas-lista-peliculas.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 73. Componente Footer

A. Creamos componente, con Footer de antd

```js
import React from 'react';import './Footer.scss';import { Layout } from 'antd';

function Loading(props) {
  return (
    <Layout.Footer className="footer">
      <p>Hola Mario Desarrollador de esta pagina</p>
    </Layout.Footer>
  );
}
export default Loading;
```

B. Agregamos estilos

```scss
.footer {
  display: flex; align-items: center; justify-content: center; background-color: #001529 !important; margin-top: 4rem; padding: 1.5rem 0 !important;
  p { color: white; margin: 0; }
}
```

C. Lo usamos en nuestro Home

```js
+ import Footer from "../components/Footer";...
export default function Home() {
  ...  
  return ( <>
      ...
+     <Footer />
  </>);
}
```

**Resultado**: Tenemos nuestro footer
![35-peliculas-footer](./img/35-peliculas-footer.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 74.76 Detalles pelicula

<!-- capturar variable de url, capturar argumento de url, capturar parametro de url - capturar variable de la url, capturar argumento de la url, capturar parametro de la url -->
<!-- tomar variable de url, tomar argumento de url, tomar parametro de url, tomar parametro url - tomar variable de la url, tomar argumento de la url, tomar parametro de la url -->
<!-- usar variable de url, usar argumento de url, usar parametro de url - usar variable de la url, usar argumento de la url, usar parametro de la url -->
<!-- variable url, argumento url, parametro url -->

Tenemos una pagina paramostrar toda la informacion completa de una
pelicula.

A. En Movie

1. capturar parametro que llega por url
2. mandamos peticion a api que nos trae esta informacion

```js
import React from 'react';import { useParams } from 'react-router-dom';import Loading from '../../components/Loading';import useFetch from '../../hooks/useFetch';import { API_URL, API_TOKEN, API_LANG, API_IMAGES } from '../../utils/Constants';import { Row, Col, Button } from 'antd';import moment from 'moment';import './Movie.scss';

function Movie() {
  const { id } = useParams(); //TOMAMOS EL PARAMETRO DE LA URL
  const urlInfo = `${API_URL}/movie/${id}?${API_TOKEN}&${API_LANG}&page=1`; //CONSULTAMOS INFO DE LA PELI
  const movieInfo = useFetch(urlInfo);
  if (movieInfo.isLoading || !movieInfo.result) { return <Loading /> }   //MOSTRAMOS QUE ESTA CARGANDO
  return <RenderMovie movie={movieInfo.result} />
}

function RenderMovie(props) {
  const { movie:{poster_path, backdrop_path} } = props;
  const backdropPath = API_IMAGES + backdrop_path;       // HACEMOS URL PARA EL BACKGROUND

  return (
    <div className="movie-details" style={{backgroundImage: `url(${backdropPath})`}}>
      <div className="movie-details__dark" />
      <Row>
        <Col span="8" offset="3" className="movie-details__poster">
            <PosterMovie image={poster_path} />
        </Col>
        <Col span="10" className="movie-details__info">
          <MovieInfo movieInfo={props.movie} />
        </Col>
      </Row>
    </div>
  );
}

function PosterMovie(props) {
  const {image} = props;
  const posterPath = API_IMAGES + image;          // HACEMOS URL PARA EL POSTER
  return <div style={{backgroundImage: `url('${posterPath}')`}}></div>;
}

function MovieInfo(props) {// MOSTRAMOS INFORMACION DE LA PELICULA
  const {movieInfo: {id, title, release_date, overview, genres} } = props;
  
  return (
  <>
    <div className="movie-details__info-header">
      <h1>
        {title}
        <span>{ moment(release_date, 'YYYY-MM-dd').format('YYYY') }</span>
      </h1>
      <Button>Ver Trailer</Button>
    </div>
    <div className="movie-details__info-content">
      <h3>General</h3>
      <p>{overview}</p>

      <h3>Generos</h3>
      <ul>
        {genres.map(genre => (
          <li key={genre.id}>{genre.name}</li>
        ))}
      </ul>
    </div>
  </>);
}
export default Movie;
```

B. Creamos estilos

```scss
.movie-details {
  height: calc(100vh - 64px); background-repeat: no-repeat; background-position: center; background-size: cover;
  &__dark { position: absolute; top: 0; left: 0; height: 100%; width: 100%; background-color: black; opacity: 0.5; }
  > .ant-row { height: 100%; display: flex; align-items: center; }
  &__poster {
    height: 70%;
    div { height: 100%; background-repeat: no-repeat; background-position: center; background-size: contain; }
  }
  &__info {
    height: 70%;
    &-header {
      display: flex; align-items: center; justify-content: space-between; font-size: 30px;
      h1 { color: white !important; margin-left: 10px;
        span { font-size: 20px; margin-left: 10px; opacity: 0.6; }
      }
      button {
        height: 100%; background-color: transparent; color: white; border: 0; font-size: 25px;
        &:hover &:focus &:active { background-color: transparent; color: turquoise; }
      }
    }
    &-content { margin-left: 1rem; margin-top: 5rem;
      h3 { color: white; font-weight: bold; font-size: 24px; }
      p { color: white; }
      ul { li { color: white; } }
    }
  }
}
```

**Resultado:** Mostramos la informacion de la pelicula que api nos dio

![36-peliculas-detalles](./img/36-peliculas-detalles.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 77. ModalVideo reproducir el trailer de la pelicula

Vamos a agregar la posivilidad de que el usuario pueda ver el trailer
desde nuestra pagina

A. instalar `yarn add react-player`

B. Agregamos nuevo componente `VideoModal`

```js
import React, { useState, useEffect } from 'react';import { Modal } from 'antd';import ReactPlayer from 'react-player';import './ModalVideo.scss';
function ModalVideo(props) {
  const {videoKey, videoPlataform, isOpen, close} = props;
  const [ urlVideo, setUrlVideo] = useState(null);
  useEffect(() => {
    switch (videoPlataform) {
      case 'YouTube': setUrlVideo('https://youtu.be/' + videoKey); break;
      case 'Vimeo': setUrlVideo('https://vimeo.com/' + videoKey); break;
      default: alert("No plataforma de video no soportada"); break;
    }
  }, [videoPlataform, videoKey])
  return (
    <Modal className="modal-video" visible={isOpen} centered onCancel={close} footer={false} >
      <ReactPlayer url={urlVideo} controls />
    </Modal>
  )
}
export default ModalVideo;
```

C. Agregamos estilos

```js
.modal-video { width: 100% !important; height: 100%; padding: 0 !important;
  .ant-modal-content { height: 100%; background-color: black !important;
    .ant-modal-close { color: white;
      span { font-size: 40px; margin-right: 20px; margin-top: 20px; }
    }
    .ant-modal-body { height: 100%; display: flex; align-items: center; justify-content: center;
      > div { width: 90% !important; height: 90% !important; }
} } }
```

D. En `Movie` agregamos el nuevo componente

1. Agregamos peticion para consegir el id-path del video
2. condicional de pintar boton solo si existen trailers para la peli

3. En la funcion/componente `MovieInfo`
   1. agregamos el estado

```js
...
+ import './Movie.scss';
+ import ModalVideo from '../../components/ModalVideo/ModalVideo';

function Movie() {...}
function RenderMovie(props) {...}
function PosterMovie(props) {...}

function MovieInfo(props) {
  const {movieInfo: {id, title, release_date, overview, genres} } = props;
  
+ const [ isVisible, setVIsVisible ] = useState(false);
+ const keyVideoMovie = useFetch(`${API_URL}/movie/${id}/videos?${API_TOKEN}&${API_LANG}`);  //PETICION
+ const openModal = () => setVIsVisible(true);
+ const closeModal = () => setVIsVisible(false);

+ const renderButton = () => {
    if(keyVideoMovie.result) {
      if(keyVideoMovie.result.results.length > 0) {
        const info = keyVideoMovie.result.results[0];                             //CAPTURA INFO DE TRAILER
        return  (
          <>  
+           <Button onClick={openModal} >Ver Trailer</Button>
+           <ModalVideo videoKey={info.key} videoPlataform={info.site} isOpen={isVisible} close={closeModal} />
          </> );
      }
    }
+ };

  return (
  <>
    <div className="movie-details__info-header">
      <h1>{title} <span>{ moment(release_date, 'YYYY-MM-dd').format('YYYY') }</span> </h1>
#     { renderButton() }
    </div>
    <div className="movie-details__info-content">... </div>
  </>);
}
export default Movie;
```

**Resultado** Tenemos un modal el cual si el sitio del trailer es
youtube o vimeo lo podremos ver

![37-peliculas-reproducir-trailer-youtube](./img/37-peliculas-reproducir-trailer-youtube.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 79. Estructura de la página Ultimos lanzamientos

B. Creamos archivo=componente `MovieCatalog` para listar las peliculas
en `NewMovies`

1. resivimos la el resultado de peticion y des-estructuramos a
   `results` son nuestras peliculas
2. Iteramos con `map` y le pasamos la pelicula a un componente
   interno `MovieCard`

```js
import React from 'react';import './MovieCatalog.scss';import { Col, Card } from 'antd';import { EyeFilled } from '@ant-design/icons';
import { API_IMAGES } from '../../utils/Constants';import { Link } from 'react-router-dom';

function MovieCatalog(props) {
  const {listMovies:{ results} } = props;
  return results.map(movie => (
    <Col key={movie.id} xs={4} className="movie-catalog">
      <MovieCard movie={movie} />
    </Col>
  ));
}

function MovieCard(props) {
  const { movie: {id, title, poster_path} } = props;
  const { Meta } = Card;
  const poster_url = API_IMAGES + poster_path;
  //hoverable: cuando pase el raton ensima muestre la mano
  return (
    <Link to={`/movie/${id}`} >
      <Card hoverable style={{width: '200px'}} cover={<img src={poster_url}/>} actions={[<EyeFilled />]}>
        <Meta title={title}>
          <h3>{title}</h3>

        </Meta>
      </Card>
    </Link>
  );
  
}

export default MovieCatalog;
```

Estilos de MovieCatalog

```scss
.movie-catalog { display: flex !important; justify-content: center !important; margin-top: 25px;
  .ant-card-cover {
    img{ height: 300px !important; }
  }
  .ant-card-meta-title { text-align: center; }
}
```

A. En `NewMovies`

1. Creamos estado `listMovies` para guardar las peliculas que nos
   lleguen por la peticion
2. Creamos estado `page` ya que manejaremos paginacion
3. En el `useEffect` hacemos peticion para las peliculas populares
4. Creamos condicional `listMovies.results ? (...` para mostrar el
   Loading o las peliculas cuando estas esten disponibles
5. Importamos `NewMovies` y lo mostramos cuando las pelicualas ya
   aigan cargado.

```js
import React, { useState, useEffect } from 'react';import { Row, Col } from 'antd';import { API_URL, API_TOKEN, API_LANG } from '../utils/Constants';import Footer from '../components/Footer';import Loading from '../components/Loading';import MovieCatalog from '../components/MovieCatalog/MovieCatalog';

function NewMovies() {
  const [listMovies, setListMovies] = useState([])
  const [page, setPage] = useState(1);

  useEffect(() => {
    (async () => {
      const response = await fetch(`${API_URL}/movie/now_playing?${API_TOKEN}&${API_LANG}&page=${page}`);
      const movies = await response.json();
      setListMovies(movies);
    })();
  }, [page]);

  return (
    <Row>
      <Col span="24" style={{ textAlign: 'center', marginRight: 25 }}>
        <h1 style={{fontSize: '30px', fontWeight: 'bold'}}>
          Ultimos Lanzamientos
        </h1>
      </Col>
      {listMovies.results ? ( // ternario para mostrar contenido con ternario
      <Col span="24" >
        <Row>
          <MovieCatalog listMovies={listMovies} />
        </Row>
      </Col>
      ) : (
        <Col span="24" >
          <Loading />
        </Col>
      ) }
      <Col span="24" >
        <Footer />
      </Col>
    </Row>
  );
}
export default NewMovies;
```

**Resultado**: Podemos ver una lista en forma de cartas verticales que
muestran las pelicualas con su respectivo poster, y cuando aun no ha
cargado se muestra un spinner.

![38-lista-cartas-peliculas-populares](./img/38-lista-cartas-peliculas-populares.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 82. Añadiendo paginacion en ultimos lanzamiento

**Objetivo** Vamos a hacer la paginacion, vamos a usar rc-paginacion

A. Instalamos en mi caso la version `yarn add rc-pagination`

1. Agregamos a nuestro index.js arriba del ./index.css

```js
+ import 'rc-pagination/assets/index.css'
= import './index.css';
```

B. Creamos nuevo archivo=componente `PaginationMovies`

```js
import React from 'react';
import Pagination from 'rc-pagination';
import './PaginationMovies.scss';
function PaginationMovies(props) {
  const { currentPage, totalItems, onChargePage } = props;
  return (
    <Pagination className="pagination" current={currentPage} total={totalItems} pageSize={20} onChange={onChargePage} />
  );
}
export default PaginationMovies;
```

Estilos

```css
.pagination {
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 50px !important;
}
```

C. Importamos nuevo componente en `NewMovies`

```js
  import React, { useState, useEffect } from 'react';import { Row, Col } from 'antd';import { API_URL, API_TOKEN, API_LANG } from '../utils/Constants';import Footer from '../components/Footer';import Loading from '../components/Loading';import MovieCatalog from '../components/MovieCatalog/MovieCatalog';
+ import PaginationMovies from '../components/Pagination/PaginationMovies';

function NewMovies() {
  const [listMovies, setListMovies] = useState([]) const [page, setPage] = useState(1);

  useEffect(() => { (async () => {
      const response = await fetch(`${API_URL}/movie/now_playing?${API_TOKEN}&${API_LANG}&page=${page}`); const movies = await response.json();
      setListMovies(movies);
    })();
  }, [page]);

+ const onChargePage = (pageIn) => {
+   setPage(pageIn);
+ }

  return (
    <Row>
      <Col span="24" style={{ textAlign: 'center', marginRight: 25 }}> <h1 style={{fontSize: '30px', fontWeight: 'bold'}}> Ultimos Lanzamientos </h1> </Col>

      {listMovies.results ? ( // ternario para mostrar contenido con ternario
    <>
#     <MovieCatalog listMovies={listMovies} />
+     <Col span="24">
+       <PaginationMovies currentPage={listMovies.page} totalItems={listMovies.total_results} onChargePage={onChargePage} />
+     </Col>
    </>
      ) : ( <Col span="24" > <Loading /> </Col> ) }
      <Col span="24" >
        <Footer />
      </Col>
    </Row>
  );
}
export default NewMovies;
```

**Resultado:** Tenemos funcionando un sistema de paginacion usando
la api.

![39-peliculas-paginacion](./img/39-peliculas-paginacion.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 83. Reutilizando componentes para montar la página de Películas Populares
<!-- Reutilizar componentes, reutilizacion de componentes, componentes reutilizables-->
A. copiamos lo mismo que tiene `NewMovies` en popularMovies

B. Le cambiamos la url que consume por

```js
const response = await fetch(`${API_URL}/movie/now_playing?${API_TOKEN}&${API_LANG}&page=${page}`);
```

**Resultado:** Tenemos nuestra pagina de populares funcionando, y lo
hizimos reutilizando componentes y copiando y pegando un codigo el cual
dejamos casi igual, igual podriamos crear otro componente para evitar
replicar el codigo.

![40-peliculas-paginacion-pupulares-reutilizacion](./img/40-peliculas-paginacion-pupulares-reutilizacion.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 84. 1/3 - Creando la página para buscar películas

**Objetivo** Vamos a crear la forma de que si queremos pasarle una x
informacion a otro usuario, vaste con pasarle la url

A. Instalar dependencia para extraer informacion de la url query-string
`yarn add query-string`

B. En `SearchMovies`

1. wrapper en `export default withRouter(SearchMovies);` para tener
   disponible a location y history en props
2. Creamos estados `movieList` y `searchValue` para guardar las pelis
   encontradas y el texto que se busca
3. capturamos datos de la url
4. Con los datos del url extraemos s para hacer la peticion de busqueda
5. `onChangeSearch` es necesario para poder cambiar el input

```js
import React, { useEffect, useState } from 'react';import { Row, Col, Input } from 'antd';import { withRouter } from 'react-router-dom';import queryString  from 'query-string';import { API_URL, API_TOKEN, API_LANG } from '../../utils/Constants';import MovieCatalog from '../../components/MovieCatalog/MovieCatalog';import Footer from '../../components/Footer';
import './SearchMovies.scss';

function SearchMovies(props) {
  //console.log(props); //al hacer wrapper podemos ver el historial y otras cosas
  const { location, history } = props;
  const [ movieList, setMovieList ] = useState([]);
  const [ searchValue, setSearchValue ] = useState("");
  useEffect(() => {
    (async () => {
      const searchQValue = queryString.parseUrl(location.search); // capturamos datos de la url
      const { s } = searchQValue.query; //capturamos parametro s ej. dom.com?s=algo en este caso "algo"
      if(s) {
        const response = await fetch(
          `${API_URL}/search/movie?${API_TOKEN}&${API_LANG}&query=${s}&page=1`
        );
        const movies = await response.json();
        setMovieList(movies); // PENDIENTE: NO SE QUE PASA imprime null el log pero al final si no asigna
      }
      else {
        setSearchValue("");
      }
    })();
  }, [location.search]);

  const onChangeSearch = (e) => {
    const urlParams = queryString.parse(location.search); //consegimos params de la url
    urlParams.s = e.target.value;                         //Los actualizamos
    const params = queryString.stringify(urlParams);      //actualizamos en la url
    history.push(`?${params}`);
    setSearchValue(urlParams.s);                        //cambiamos el valor en nuestro input=buscador
  }
  
  return (
    <Row>
      <Col span="12" offset="6" className="search">
        <h1>Busca tu pelicula</h1>
        <Input value={searchValue} onChange={onChangeSearch} />
      </Col>
      {movieList.results ? (
        <Row span="24">
          <MovieCatalog listMovies={movieList} />
        </Row>
      ) : (
        <Col span="24"> </Col>
      ) }
      <Col span={24} > <Footer /> </Col>
    </Row>
  );
}
export default withRouter(SearchMovies); //wrapper a SearchMovies
```

**Resultado:** Podemos hacer busqueda de peliculas y cambiamos el texto
el el input tambien se actualiza en la url

![41-peliculas-buscador-actualizar-url-dinamicamente](./img/41-peliculas-buscador-actualizar-url-dinamicamente.gif)

#### 87. Codigo Completo

- [Codigo Completo](./img/41-z-peliculas-CODIGO-COMPLETO.zip)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

### Sección 9: PROYECTO: Carrito de un E-Commerce

A. Iniciar proyecto react `npx create-react-app carrito`

B. Abrir carpera con visual estudio `code .`

C. Instalamos dependencias:
`yarn add react-bootstrap bootstrap node-sass react-toastify`

1. react-bootstrap [Documentacion](https://react-bootstrap.github.io/components/buttons/)
2. react-toastify [Documentacion](https://www.npmjs.com/package/react-toastify)

D. En ./index.js

1. agregar estilos de bootstrap `import 'bootstrap/dist/css/bootstrap.min.css';`  
2. agregar estilos de react-toastify `import 'react-toastify/dist/ReactToastify.css';`

E. Comprobamos que funcionen bien las dependencias

```js
import React from 'react'; import './App.css';
import Button from 'react-bootstrap/Button';
import { ToastContainer, toast } from 'react-toastify';
function App() {//CUIDADO: SI NO PONEMOS EL ToastContainer NO SE MOSTRARAN
  const openToast = () => { toast("Este es mi toast"); }
  return (
    <div className="App">
      <Button variant="primary" onClick={openToast} >Mostrar Toast</Button>
      <ToastContainer />  
    </div>
  );
}
export default App;
```

**Resultado:** Al repcionar el boton debe de salirnos un toast

![42-carrito-bootstrap-toastify](./img/42-carrito-bootstrap-toastify.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 92. Creando el componente TopMenu

A. En su propia carpeta creamos

```js
import React from 'react';import { Container, Navbar, Nav } from 'react-bootstrap';import './TopMenu.scss';import { ReactComponent as Logo } from '../../assets/svg/logo-carrito.svg';

function TopMenu() {
  return (
    <Navbar bg="dark" variant="dark" className="top-menu">
      <Container>
        <BrandNav />
        { /*  ------------------ MENU ---------------- */ }
        <MenuNav />
      </Container>
    </Navbar>
  );
}

function BrandNav() {
  return (
    <Navbar.Brand>
      <Logo />
      <h2>La casa de los helados</h2>
    </Navbar.Brand>
  );
}

function MenuNav() {
  return (
    <Nav className="mr-auto">
      <Nav.Link href="#">Aperitivos</Nav.Link>
      <Nav.Link href="#">Helados</Nav.Link>
      <Nav.Link href="#">Mascotas</Nav.Link>
    </Nav>
  );
}
export default TopMenu;
```

B. Agregamos estilos

```scss
.top-menu {
  .navbar-brand {
    display: flex; align-items: center; justify-content: center;
    svg { width: 2.5rem; height: 2.5rem; }
    h2 { margin: 0; font-size: 22px; }
  }
}
```

**Resultado:** Yenemos nuestra barra superior maquetada de la sig. m.

![43-carrito-topmenu](./img/43-carrito-topmenu.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 94. Creando base de datos de productos y subiéndola a api.myjson

A. Creamos ./bd/dbProductos.json

```json
[
  { "id": 1, "name": "Helado sándwich de nata", "image": "img/sandwich-nata.jpg", "extraInfo": "Caja 6 ud. (600 ml", "price": 1.2 },
  { "id": 2, "name": "Helado cucurucho de vainilla", "image": "img/cucuruchos-vainilla.jpg", "extraInfo": "Caja 6 ud. (720 ml", "price": 1.55 },
  { "id": 3, "name": "Mini surtido", "image": "img/mini-surtido.jpg", "extraInfo": "Caja 6 ud. (720 ml)", "price": 1.55 },
  { "id": 4, "name": "Helado mini doble sándwich nata", "image": "img/sandwich-doble.jpg", "extraInfo": "Caja 6 ud. (480 ml)", "price": 1.99 },
  { "id": 5, "name": "Helado de fresa", "image": "img/fresa-hielo.jpg", "extraInfo": "Caja 10 ud. (750 ml)", "price": 1.65 },
  { "id": 6, "name": "Helado yogoice Mr. Toddy", "image": "img/yogoice.jpg", "extraInfo": "Caja 8 ud. (450 ml)", "price": 1.7 },
  { "id": 7, "name": "Helado sabor huevo de chocolate", "image": "img/huevo.jpg", "extraInfo": "Tarrina 1 L.", "price": 2.55 },
  { "id": 8, "name": "Medios cocos helados", "image": "img/medio-coco-helado.jpg", "extraInfo": "Caja 2 ud. (350 ml)", "price": 2.05 }
]
```

B. Consultamos el mismo

```js
export const urlApiProducts = "https://demo2562028.mockable.io/products";
```

C. Ponemos las imagenes de nuestros productos en `./public/img`

D. Creamos funcion para hacer peticiones a la url en `./src/hooks/useFetch.js`
que tiene el mismo contenido que en `%{tfcvghn}%`

E. En App.js

1. Importamos useFetch y hacemos consulta

```js
  import React, { useEffect } from 'react';import TopMenu from './components/TopMenu';
+ import useFetch from './hooks/useFetch'; import { API_PRODUCTS } from './utils/Constants';

function App() {
+ const consult = useFetch(API_PRODUCTS, null);
+ console.log(consult);
  return ( <div className="App"> <TopMenu /> </div> );
}
export default App;
```

**Resultado:** Debemos de poder ver el la peticion funcionando

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 96.  Creando el componente Products mostrando un loading o los productos por pantalla

A. Creamos Loading.js

```js
import React from 'react';
import { Spinner } from 'react-bootstrap';

import './Loading.scss';

function Loading() {
  return (
    <div className="loading">
      <Spinner animation="border"  role="status" />
      <h5>Cargando...</h5>
    </div>
  );
}
export default Loading;
```

Estilos

```scss
.loading { width: 100%; text-align: center; margin-top: 100px;
  h5 { font-weight: normal; padding-top: 20px; }
}
```

B. Creamos Products.js en su carpeta

1. Importamos Loading
2. De props nos va a llegar el resultado de la peticion http
3. Dependiendo de esta mostramos el Loading o iteramos productos

```js
import React from 'react';import './Products.scss';import { Container, Row } from 'react-bootstrap';import Loading from '../Loading/Loading';
function Products(props) {
  const { products: {result, loading, error} } = props;
  return (
    <Container className="products">
      <Row>
        { loading || !result ? ( //MOSTRAMOS CARGA
          <Loading />
        ) : (
          result.map((product, index) => ( //ITERAMOS PRODUCTOS
            <div key={product.id}> <p>{product.name}</p> </div>
          ))
        )}
      </Row>
    </Container>
  );
}
export default Products;
```

C. Agregamos Products a `App`

```js
  import React from 'react';import TopMenu from './components/TopMenu';import useFetch from './hooks/useFetch';import { API_PRODUCTS } from './utils/Constants';
+ import Products from './components/Products/Products';
function App() { const consult = useFetch(API_PRODUCTS, null); //CONSULTA
  return (
    <div className="App"> <TopMenu />
+     <Products products={consult} />  { /* ITERAMOS-MOSTRAMOS PRODUCTOS */ }
    </div>
);}export default App;
```

**Resultado** Mostramos los productos consultados de la api de una
forma basica y se muestra un sppiner de carga.

![44-carrito-consulta-http-cargando](./img/44-carrito-consulta-http-cargando.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 98. Componente Product para mostrar el productos en la web

a. Agregamos ruta a Constants para consultar nuestras images

```js
export const BASE_PATH = "http://localhost:3000";
```

A. Creamos Product.js

```js
import React from 'react'; import { Col, Card, Button } from 'react-bootstrap'; import { BASE_PATH } from '../../utils/Constants';
import './Product.scss';
function Product(props) {
  const { product: { image, name, extraInfo, price } } = props;
  return (
    <Col className="product">
      <Card.Img variant="top" src={`${BASE_PATH}/${image}`}></Card.Img>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{extraInfo}</Card.Text>
        <Card.Text>{price} $ / unidad</Card.Text>
        <Button>Añadir al carrito</Button>
      </Card.Body>
    </Col>
  );
}
export default Product;
```

B. Añadimos estilos a Product

```scss
.product { padding: 0; min-width: 15rem;
  .card-body { margin: 15px;
    .card-title { height: 4.5rem; }
    .btn { width: 100%; background-color: transparent; color: #0098d3; border-color: #0098d3; border-radius: 50px;
      &:hover, &:active, &:focus { background-color: #0098d3 !important; border-color: #0098d3; color: white; box-shadow: none;
} } } }
```

C. En `Products` importamos a Product y lo usamos en la iteracion

```js
+ import React from 'react';import './Products.scss';import { Container, Row } from 'react-bootstrap';import Loading from '../Loading/Loading';
+ import Product from '../Product/Product';

function Products(props) {
  const { products: {result, loading, error} } = props;
  return (
    <Container className="products"> <Row>
        { loading || !result ? ( <Loading /> ) : (
          result.map((product, index) => (
+           <Product product={product} />
          ))
        )}
      </Row></Container>
  );
}
export default Products;
```

**Reultado:** Mostramos un catalogo de productos maqutados

![45-carrito-componente-productos-maquetacion](./img/45-carrito-componente-productos-maquetacion.PNG)

- Commit: "proyecto carrito, mostrar catalogo falta agregar carrito"

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 99. Logica para añadir productos al carrito

**Objetivo:** hasta ahora el al darle click sobre agregar producto no
pasa nada, lo que vamos a hacer es agregar el id del producto al
localstorage, y cuando ahiga un cambio en el localstorage se mostrara
dicho producto en el carrito.

A. En App

1. Agregamos funcion para agregar al localstorage
2. La funcion se la pasamos a Products
3. Agregamos ToastContainer y toast para mostrar toast

```js
import React, { useState } from 'react';import TopMenu from './components/TopMenu';import useFetch from './hooks/useFetch';import Products from './components/Products/Products';
+ import { API_PRODUCTS, STORAGE_PRODUCT_CAR } from './utils/Constants';
+ import { ToastContainer, toast } from 'react-toastify';

function App() {
  const consult = useFetch(API_PRODUCTS, null);
  const [ productsCart, setProductsCart ] = useState([]);

+ const addproductToCart = (id, name) => {
+   const idsProducts = productsCart;
+   idsProducts.push(id);
+   setProductsCart(idsProducts);
+   localStorage.setItem(STORAGE_PRODUCT_CAR, idsProducts);
+   toast(`Agregado ${name}`);
+ }

  return (
    <div className="App">
      <TopMenu />
#     <Products products={consult} addproductToCart={addproductToCart} />
+     <ToastContainer />
    </div>
  );
}
export default App;
```

B. En Products le pasamos la funcion a Product y en products
ejecutamos la funcion cuando se haga click.

```js
import React from 'react';
import { Col, Card, Button } from 'react-bootstrap';
import { BASE_PATH } from '../../utils/Constants';
import './Product.scss';

function Product(props) {
# const { product: { id, image, name, extraInfo, price }, addproductToCart } = props;
  return (
    <Col className="product"><Card.Img variant="top" src={`${BASE_PATH}/${image}`}></Card.Img>
      <Card.Body>
        <Card.Title>{name}</Card.Title><Card.Text>{extraInfo}</Card.Text><Card.Text>{price} $ / unidad</Card.Text>
#       <Button onClick={() => { addproductToCart(id, name) } }>Añadir al carrito</Button>
      </Card.Body>
    </Col>
  );
}
export default Product;
```

**Resultado:** Al darle click sobre agregar al carrito, el id de este
se agrega al localstorage

![46-carrito-agregar-id-producto-a-localstorage](./img/46-carrito-agregar-id-producto-a-localstorage.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 100. Personalizar Toast

Podemos ver un demo de nuestra personalizacion [aqui](https://fkhadra.github.io/react-toastify/introduction)

Despues de haber visto las opciones en la documentacion en App
pegamos la personalizacion

```js
import React, { useState } from 'react';import TopMenu from './components/TopMenu';import useFetch from './hooks/useFetch';import { API_PRODUCTS, STORAGE_PRODUCT_CAR } from './utils/Constants';
import Products from './components/Products/Products'; import { ToastContainer, toast } from 'react-toastify';

function App() {
  const consult = useFetch(API_PRODUCTS, null); const [ productsCart, setProductsCart ] = useState([]);
  const addproductToCart = (id, name) => { const idsProducts = productsCart; idsProducts.push(id); setProductsCart(idsProducts); localStorage.setItem(STORAGE_PRODUCT_CAR, idsProducts);
#  toast.success(`Agregado ${name}`);
  }

  return (
    <div className="App">
      <TopMenu />
      <Products products={consult} addproductToCart={addproductToCart} />
      <ToastContainer
        position="bottom-left" autoClose={4000} hideProgressBar
        newestOnTop closeOnClick rtl={false} pauseOnHover={false}
        pauseOnVisibilityChange={false} draggable
      />
    </div>
  );
}
export default App;
```

**Resultado:** Aparece abajo y de color verde sin la barra

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 101. Recuperando productos del carrito al recargar la web
<!-- se borran el localstorage se borra-->
**Problematica** al agregar productos al carrito y recargar la pagina
y agregar nuevamente productos se eliminan

A. En `App` agregamos una funcion

```js
import ...
function App() {
  ...
+ const getInitialProductsCar = () => {
    const idsProducts = localStorage.getItem(STORAGE_PRODUCT_CAR);
    if(idsProducts) {
      const idsTemp = idsProducts.split(","); //recuperamos string (ej '1,2,3') lo convertimos a array
      setProductsCart(idsTemp);
    } else {
      setProductsCart([]);
    }
  }

+ useEffect(() => { // SE EJECUTA AL INICIAR LA PAGINA
    getInitialProductsCar();
  }, [])

  const addproductToCart = (id, name) => {... }

  return (... );
}
export default App;
```

**Resultado:** Ya podemos agregar ids al localstorage salir y agregar
de nuevo y no se borraran los agregados antes.

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 102.106. Agregar Carrito, abrir-cerrar, cambio icono al agregar-vaciar, bloquer scroll

A. Descargamos los iconos en `./src/assets/svg`

B. Creamos componente Car.js

1. Importamos iconos con un alias
2. Lo creamos como dos elementos html diferentes usando `<>`
3. **Mostrar un icono u otro si ahi productos en carrito** Ponemos
   condicional para mostrar un icono u otro segun el tamaño de productos
   resividos desde `App`
4. **Mostrar header de contenido de carrito** Agregamos funcion=Componente `CarContentHeader`
5. **Boton de cerrar** Le pasamos la funcion `closeCar` a `CarContentHeader`
   y que se ejecute en el onclic de `CarClose`
6. **Vaciar carrito**: Resivimos `emptyCar` de `TopMenu` que se la da
   `App` y la ejecutamos en el onClick de `CarGarbage`
7. **Cambiar icono a carrito vacio cuando vaciamos carrito** le de
   `App` pasamos la funcion `getInitialProductsCar` a travez de `TopMenu` a la funcion de `emptyCar`

```js
import React, { useState } from 'react';
import './Car.scss';
import { Button } from 'react-bootstrap';

import { ReactComponent as CarEmpty } from '../../assets/svg/cart-empty.svg';
import { ReactComponent as CarFull } from '../../assets/svg/cart-full.svg';
import { ReactComponent as CarClose } from '../../assets/svg/close.svg';
import { ReactComponent as CarGarbage } from '../../assets/svg/garbage.svg';

function Car(props) {
  const { productsCart, emptyCar } = props;
  const [isCarOpen, setIsCarOpen] = useState(false);
  const widthCarContent = isCarOpen ? 400 : 0; //Variable que hace que se habra y cierre el carrito

  const openCar = () => {
    setIsCarOpen(true);
    document.body.style.overflow = "hidden"; // ocultamos el scroll para que no suba y baje
  }

  const closeCar = () => {
    setIsCarOpen(false);
    document.body.style.overflow = "scroll"; // mostramos  el scroll para que no suba y baje
  }

  return (
    <>
      <Button variant="link" className="car">
        {productsCart.length>0 ? ( //si hay productos mustra carrito lleno
          <CarFull onClick={openCar} />
        ) : (                       // mustra carrito vacio por que no hay productos
          <CarEmpty onClick={openCar} />
        )}
      </Button>
      {/* cambiara el ancho segun sea verdadero o falso la variable 'isCarOpen' */}
      <div className="car-content" style={{width: widthCarContent}}>
        <CarContentHeader closeCar={closeCar} emptyCar={emptyCar} />
      </div>
    </>
  );
}


function CarContentHeader(props) {
  const { closeCar, emptyCar } = props
  return (
    <div className="car-content__header">
      <div>
        <CarClose onClick={closeCar} />     { /* para cerrar carrito*/ }
        <h2>Carrito</h2>
      </div>
      <Button variant="link">
        Vaciar <CarGarbage onClick={emptyCar} /> { /* funcion que llega desde App*/ }
      </Button>
    </div>
  )
}
export default Car;
```

D. Agregamos estilos

```scss
.car { svg { width: 30px; fill: white; } } //icono carrito para abrir carrito que cambia a lleno o vacio
.car-content {
  overflow: hidden; position: absolute; top: 0; right: 0; background-color: #343a40; text-align: left;
  height: 100vh; /* para que ocupe todo el alto de la pantalla */
  white-space: nowrap; /* para que las lineas de texto no caigan */ transition: width 1s; z-index: 9;
  .car-content__header { display: flex; align-items: center;
    justify-content: space-between; //el (boton cerrar, titulo ) y (botonvacias) se vayan a los extremos izq-der
    padding: 20px; border-bottom: 1px solid #4f5256; height: 76px;
    > div { // los div de primer nivel
      &:first-of-type {
        display: flex;
        svg { /* icono de cerrar */ width: 15px; fill: white; margin-right: 1rem; &:hover { cursor: pointer; } }
        h2 { color: white; margin: 0; font-size: 24px; }
      }
    }
    button { /* boton de vaciar */ margin: 0; color: white;
      svg { /*icono de vaciar*/ height: 20px; width: 20px; fill: white; margin-left: 10px; }
      &:hover { text-decoration: none; color: #0098d3; svg { fill: #0098d3; } }
} } }

```

X. En TopMenu

1. Agregamos el componente Car
2. A Car le pasamos los ids de productos que resiviremos desde App

```js
  import React from 'react';import { Container, Navbar, Nav } from 'react-bootstrap';import './TopMenu.scss';import { ReactComponent as Logo } from '../../assets/svg/logo-carrito.svg';
+ import Car from '../Car/Car';
function TopMenu(props) {
+ const { productsCart, emptyCar } = props;
  return ( <Navbar bg="dark" variant="dark" className="top-menu">
      <Container> <BrandNav /> <MenuNav />
+        <Car productsCart={productsCart} emptyCar={emptyCar} />
      </Container>
    </Navbar>
  );
}
function BrandNav() {...} function MenuNav() {...} export default TopMenu;
```

X. En App

1. le pasamos `productsCart` al carrito desde TopMenu y de este a Car
2. En `addproductToCart` ejecutamos `getInitialProductsCar()` para que cuando se agrege un producto se actualize el icono del carrito

```js
import ...
function App() {
  const consult = useFetch(API_PRODUCTS, null);
  const [ productsCart, setProductsCart ] = useState([]);

+ const getInitialProductsCar = () => {
    const idsProducts = localStorage.getItem(STORAGE_PRODUCT_CAR);
    if(idsProducts) {
      const idsTemp = idsProducts.split(","); //recuperamos string (ej '1,2,3') lo convertimos a array
      setProductsCart(idsTemp);
    } else {
      setProductsCart([]);
    }
  }

+ useEffect(() => {
    getInitialProductsCar();
  }, [])

+ const addproductToCart = (id, name) => {
    const idsProducts = productsCart;
    idsProducts.push(id);
    setProductsCart(idsProducts);
    localStorage.setItem(STORAGE_PRODUCT_CAR, idsProducts);
    getInitialProductsCar(); // cuando se agrega un producto cambia el icono de carrito
    toast.success(`Agregado ${name}`);
  }

+ const emptyCar = () => {
    localStorage.removeItem(STORAGE_PRODUCT_CAR);
    getInitialProductsCar();
  }

  return (
    <div className="App">
 #    <TopMenu productsCart={productsCart} emptyCar={emptyCar} />
      <Products products={consult} addproductToCart={addproductToCart} />
      <ToastContainer ... />
    </div>
  );
}
export default App;
```

**Resultado:**

1. Podemos abrir y cerrar el carrito
2. Al abrir el carrito se bloque el scroll vertical para que se concentre
   el usuario en el carrito.
3. Podemos vaciar el carrito y al hacerlo cambia el icono
4. Al agregar un producto cambie el icono

![47-carrito-abrir-cerrar-vaciar-cambio-icono-ocultar-scroll](./img/47-carrito-abrir-cerrar-vaciar-cambio-icono-ocultar-scroll.gif)

- Commit: Abrir cerrar carrito

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 107. Creando funciones reutilizables para tratar arrays

A. Agregamos ./src/utils/arrayFunc.js para peparnos para eliminar un
   X producto, mostrar conjunto de un solo producto.

```js
//Para contar cuantos elementos dplicados ahi en un array
export const countDuplicatesItemArray = (value, array) => {
  let count = 0;
  array.forEach(arrayValue => {
    if(arrayValue == value) {
      count++;
    }
  });
  return count;
}

//Eliminar elementos repetidos
export const removeArrayDuplicates = array => {
  return Array.from(new Set(array));
}

//eliminar un elemento del array
export const removeItemArray = (array, itemToDelete) => {
  const index = array.indexOf(itemToDelete);
  if(index > -1) { // si encontro elemento a eliminar
    array.splice(index, 1);
  }
  return array;
}
```

**Resultado:** Nada visual solo tenemos disponible las funciones.

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 108.109 listar productos del carrito

<!-- 
, Como funciona el carrito, funcionamiento carrito funcionamiento, 
como hacer un carrito, como crear un carrito, como funciona un carrito, 
como mostrar los productos de un carrito, como listar los productos de un carrito,
mostrar productos del carrito.
-->

Algoritmo carrito: **Pre-requisito** debemos de guardar el id de los
productos agregados por el usuario al carrito, de forma repetida en
este caso en el localstorage.

1. De el conjunto=array de `productos-agregados-en-carrito`=`productsCart`
   que pueden estar repetidos `creamos` un nuevo cojunto `productos-en-carrito`
   =`singleProductsCar` el cual es productos sin repetir.
2. Iteramos `singleProductsCar` para pintar solo un producto en la lista
   y le pasamos `productsCart` a `CarContentProducts` para que pueda
   calcular la cantidad de un mismo producto
3. En `CarContentProducts` comparamos el `singleProductsCar` y `productsCart`
   para saber que existe en la lista que tiene toda la informacion de
   los productos, y si, si existe, contamos los productos repetidos en
   `productsCart` para mostrarle al usuario cuantos veces tiene agregado
   el mismo producto
4. En `RenderProduct` solo nos dedicamos a mostrar el producto.

En Car

```js
import React, { useState, useEffect } from 'react';
import './Car.scss';
import { Button } from 'react-bootstrap';

import { ReactComponent as CarEmpty } from '../../assets/svg/cart-empty.svg';
import { ReactComponent as CarFull } from '../../assets/svg/cart-full.svg';
import { ReactComponent as CarClose } from '../../assets/svg/close.svg';
import { ReactComponent as CarGarbage } from '../../assets/svg/garbage.svg';
+ import { removeArrayDuplicates, countDuplicatesItemArray } from '../../utils/arrayFuncs';
+ import { BASE_PATH } from '../../utils/Constants'; // para mostrar la imagen

function Car(props) {
# const { productsCart, emptyCar, products } = props; // resivimos todos los productos
  const [isCarOpen, setIsCarOpen] = useState(false); const widthCarContent = isCarOpen ? 400 : 0;
+ const [ singleProductsCar, setSingleProductsCar ] = useState([]); // guardamos los-productos sin repetir

+ useEffect(() => {
+    // de todos los produtos agregados que pueden repetirse, lo dejamos sin repetirce
+   const allProductsId = removeArrayDuplicates(productsCart);  
+   setSingleProductsCar(allProductsId);
+ }, [productsCart])

  const openCar = () => {...}

  const closeCar = () => { ... }

  return (
    <>
      <Button variant="link" className="car">...</Button>
      <div className="car-content" style={{width: widthCarContent}}>
        <CarContentHeader closeCar={closeCar} emptyCar={emptyCar} />
+       <div className="car-content__products">
+         {singleProductsCar.map((idProductCar, index) => ( // iteramos los productos agregados sin repetir
+             <CarContentProducts key={index} products={products} productsCart={productsCart} idProductCar={idProductCar} />
+         ))}
+       </div>
      </div>
    </>
  );
}


  function CarContentHeader(props) {....}

+ function CarContentProducts(props) {
+   const {products: { loading, result }, productsCart, idProductCar} = props;
+   if(!loading && result) {
+     return result.map((product, index) => { // en todos los productos buscamos el productoId agregado en el carrito
+       if(idProductCar == product.id) {     //se encontro un id del carrito en la lista de todos los productos, 1 si existe, 2 sabemos que debemos de pintar este producto
+         //contamos cuantas veces esta el mismo producto en el carrito
+         const quantity = countDuplicatesItemArray(product.id, productsCart);
+         return (
+           <RenderProduct key={index} product={product} quantity={quantity} />
+         )
+       }
+     });
+   }
+   return null;
+ }

+ function RenderProduct(props) { // sin ciencia solo pintamos, lo complicado son los estilos
    const { product, quantity } = props
    return (
      <div className="car-content__product" style={{color: 'white'}}>
        <img src={`${BASE_PATH}/${product.image} `} alt={product.name} style={{width: '50px'}} />
        <div className="car-content__product-info">
          <div><h3>{product.name.substr(0, 25)}</h3></div>
          <p>{product.price.toFixed(2)} $ / ud.</p>
          <div>
            <p>En Carrito: {quantity} ud.</p>
            <div>
              <button>+</button>
              <button>-</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
export default Car;
```

Estilos de lista productos en carrito

```scss
.car-content {
  .../* estilos del header y del container (lo que se muestra-oculta) se mueestra arriba */
  .car-content__products { overflow-y: scroll; height: calc(100vh - 76px - 19px); /* todo el alto menos 76 arriba menos 19 abajo */ }
  .car-content__product { display: flex; align-items: center; padding: 15px; border-bottom: 1px solid #4f5256;
    img { width: 100px; margin-right: 10px; }
    .car-content__product-info { color: white; width: 100%;
      h3 { font-size: 18px; }
      p { font-size: 16px; }
      >div {
        &:first-of-type { border-bottom: 1px solid #4f5256; }
        &:last-of-type { margin-top: 15px; display: flex; justify-content: space-between;
          p { margin: 0; }
          button { background-color: #0098d3; border-color: #0098d3; color: white; border-radius: 5px; margin: 0 5px; width: 30px; height: 30px; }
        }
} } } }
```

**Resultado:** Vemos en el carrito la lista de productos seleccionados
y la cantidad de cada uno de estos en el carrito, Nota: deveriamos de
porder ver como se agrega un nuevo producto mas al carrito en tiempo
real al igual que su cantidad actializandoze.

![48-carrito-mostrar-prodcutos-del-carrito](./img/48-carrito-mostrar-prodcutos-del-carrito.gif)

- Commit: "mostrar productos de carrito"

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 110. funcionalidad boton de incrementar y decrementar producto

A. En Car. Añadismo funcion `increseQuantity` y `decreseQuantity` y a
travez de propiedades se lo pasamos al onclic de los botones

```js
function Car(props) {
  ...
  const increseQuantity = (id) => {
      const arrayItemCar = productsCart;
      arrayItemCar.push(id);
      localStorage.setItem(STORAGE_PRODUCT_CAR, arrayItemCar);
      getInitialProductsCar();
    }

  const decreseQuantity = (id) => {
    console.log("decrementando");
    const arrayItemCar = productsCart;
    localStorage.setItem(STORAGE_PRODUCT_CAR, removeItemArray(arrayItemCar, id.toString()));
    getInitialProductsCar();
  }...
}
  ...
  <button onClick={() => increseQuantity(product.id) } >+</button>
  <button onClick={() => decreseQuantity(product.id) } >-</button>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 111.112 Mostrar y calcular total de carrito en su Footer

A. En Car

1. Agregamos Compoente para renderizar footer que resive el total
2. En el render de Car agregamos el componente nuevo
3. Agregamos estado para guardar el precio
4. Agregamos useEffect para calcular el total que se ejecutara cada vez
   que se agrege un producto

Algoritmo para calculat total de precio **PENDIENTE**

```js
function Car(props) {
  ...
+ const [ totalPrice, setTotalPrice ] = useState(0);

+ useEffect(() => { //calcular el total del carrito
    const productData = [];
    let total = 0;
    const allProductsId = removeArrayDuplicates(productsCart);
    allProductsId.forEach((productId) => {
      const quantity = countDuplicatesItemArray(productId, productsCart);
      const productValue = {
        id: productId,
        quantity: quantity
      };
      productData.push(productValue);
    });
    if(!products.loading && products.result) {
      products.result.forEach(product => {
        productData.forEach(item => {
          if(product.id == item.id) {
            const totalByProduct = product.price * item.quantity;
            total = total + totalByProduct
          }
        });
      });
    }
    setTotalPrice(total);
  }, [productsCart, products]);

  return (
    <>
      <Button variant="link" className="car"> ...</Button>
      <div className="car-content__products">...</div>
+       <CarContentFooter totalPrice={totalPrice} />
      </div>
    </>
  );
}

+ function CarContentFooter(props) {
    const { totalPrice } = props
    return (
      <div className="car-content__footer">
        <div>
          <p>Total Aproximado: </p>
          <p>{totalPrice.toFixed(2)} $</p>
        </div>
        <Button>Tramitar Pedido</Button>
      </div>
    )
  }
```

Estilos

```scss
  .car-content__footer {
    position: absolute;bottom: 0;left: 0;width: 100%;padding: 15px;padding-top: 0;background-color: #4f5256;
    > div { display: flex;align-items: center;justify-content: space-between;color: white;
      p { &:last-of-type { font-weight: bold;font-size: 20px; } }
    }
    button { background-color: #0098d3;width: 100%;border: 0;font-size: 20px;color: white;padding: 0.5rem; }
  }
```

**Resultado**: Al agregar o quitar un producto del carrito se actualiza
el total.

![49-mostrar-actializar-total-carrito](./img/49-mostrar-actializar-total-carrito.gif)

- Commit: "Proyecto carrito, calcular y actualizar total"

#### Codigo Completo

- [50-carrito-CODIGO-COMPLETO](./img/50-carrito-CODIGO-COMPLETO.zip)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

### Sección 10: PROYECTO: Lista de tareas con React y Firebase

PENDIENTE...

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

### Sección 12: Proyecto Redux y React

**Que es Redux**: Es un estado global de la aplicacion, en la cual
podremos acceder a estados en cualquier parte de nuestra web.

A. Crear proyecto como siempre

B. agregar dependencias  
`yarn add moment react-bootstrap bootstrap uuid redux react-redux`

- uuid: es para generar ids unicos

y nuestro package.json deberia de quedar asi

```json
"dependencies": {
  "@testing-library/...": "^...", "react-dom": "^16.13.1", "react": "^16.13.1","react-scripts": "3.4.1",
  "bootstrap": "^4.4.1",
  "moment": "^2.24.0",
  "react-bootstrap": "^1.0.1",
  "react-redux": "^7.2.0",
  "redux": "^4.0.5",
  "uuid": "^7.0.3"
},
```

C. agregar estilos de bootstrap a index.js  
`import 'bootstrap/dist/css/bootstrap.min.css';`

D. Creamos Menu.js

```js
import React from 'react'; import { Container, Navbar, Button } from "react-bootstrap";
import LogoRedux  from "../assets/img/logo-redux.png";
function Menu() {
  //mt-5 para darle un matgin-top de 5
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand>
          <img alt="PioPio Redux" src={LogoRedux} width="30" height="30" className="d-inline-block align-top mr-4" />
          Pio-Pio Simulator
        </Navbar.Brand>
        <Button variant="outline-success">Nuevo Tweet</Button>
      </Container>
    </Navbar>
  );
}
export default Menu;
```

Nuestro App inicial

```js
import React from 'react'; import { Container } from "react-bootstrap";
import Menu from './components/Menu';
function App() {
  //mt-5 para darle un matgin-top de 5
  return (
    <>
    <Menu />
    <Container className="mt-5">
      <h1 className="text-center">PioPio</h1>
    </Container>
    </>
  );
}
export default App;
```

**Resultado:** Tenemos nuestra cabecera

![51-redux-inicio](./img/51-redux-inicio.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 149. Configurando REDUX en el proyecto e instalaremos Redux DevTool

A. Instalamos en Crhome Redux DevTools de "Ofrecido por: remotedevio"

X. Creamos `./redux/reducers/ModalsReducers.js` que sera el que cambiara los
estados de los modales.

```js
const initialProps = {
  isOpen_ModalAddPio: false
}
//al iniciar la app no le pasaremos nada por eso le ponemos como default el initialProps
export default function(state = initialProps, action) {
  switch (action.type) {
    // A que estado queremos cambiar
    case "STATE_ADD_MODAL": // en este caso servira para abrir y cerrar el modal
      return {
        ...state,
        isOpen_ModalAddPio: action.payload //cambiamos el estado por lo que nos llegue
      };
    break;
    default:
      return state;
    break;
  }
}
```

X. Creamos nuestras acciones en `./src/redux/actions/ModalsActions.js` para
que ejecute los reducers que definimos arriba

```js
export const openCloseAddPioModalAction = (state) => {
  return {
    type: 'STATE_ADD_MODAL',
    payload: state
  }
};
```

X. Creamos `./reducers/index.js` para importar nuestro reducers facilmente

```js
import { combineReducers } from 'redux';
import ModalsReducer from "./ModalsReducer";
export default combineReducers({
  modals: ModalsReducer
});
```

X. Creamos `./src/store.js` que configurara nuestro redux y la linea de
`window.__REDUX_DEVTOOLS_EXT...` es para que la extencion que agregamos
funcione con nuestra app.

```js
import { createStore } from 'redux'
import reducer from "./reducers";
const store = createStore(
  reducer, /* preloadedState, */
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
export default store;
```

**Resultado:** Podemos ver los estados que agregamos en la pestaña de
redux en herramientas de desarrollador.

![51-redux-configuracion-inicial](./img/51-redux-configuracion-inicial.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 150. Abriendo y cerrando el modal y actualizando el estado en el store

A. Creamos ModalContainer.js

1. Importar useDispatch para poder cambiar el estado a cerrado
2. Importar useSelector para acceder al estado y asi que este se habra
   o se cierre.

```js
import React from 'react';
import { Modal } from 'react-bootstrap';
import { useDispatch, useSelector } from "react-redux";
import { openCloseAddPioModalAction } from '../redux/actions/ModalsActions';

function ModalContainer(props) {
  const {children} = props;

  // useDispatch: es para acceder a nuestras acciones
  const dispatch = useDispatch();
  const setOpenModal = state => { dispatch(openCloseAddPioModalAction(state)); }

  // useSelector: para acceder a un valor en el store
  const isOpenModal = useSelector(state => state.modals.isOpen_ModalAddPio);

  return (
    <Modal show={isOpenModal} onHide={()=>setOpenModal(false)} size="large" centered>
      {children}
    </Modal>
  )
}
export default ModalContainer;
```

**Resultado** Podemos abrir/cerrar nuestro modal cambiando/consultando
el estado en redux, y este estara grabando el cambio de estados.

![52-redux-estados-de-react-y-grabado-d-estados](./img/52-redux-estados-de-react-y-grabado-d-estados.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 151. Formulario agregar pio y si error guardar en el store

A. Añadimos nuevo estado en el store `isError_FormAddPio` para lo cual

A-1. Creamos Action en `./src/redux/actions/ValidationsActions.js`

```js
export const validationFormAddPioActions = (state) => {
  return {
    type: 'VALIDATION_FORM_ADD_PIO',
    payload: state
  }
};
```

A-2. Creamos reducer `./src/redux/reducers/validationsReducer.js`

```js
const initialProps = {
  isError_FormAddPio: false
}
export default function(state = initialProps, action) {
  switch (action.type) {
    case "VALIDATION_FORM_ADD_PIO":
      return {
        ...state,
        isError_FormAddPio: action.payload
      };
    //break;
    default:
      return state;
    //break;
  }
}
```

A-3. Lo agregamos en `./src/redux/reducers/index.js`

```js
import { combineReducers } from 'redux';
import ModalsReducer from "./ModalsReducer";
import ValidationsReducer from "./validationsReducer";
export default combineReducers({
  modals: ModalsReducer,
  validations: ValidationsReducer,
});
```

B. Creamos `src/components/PioAddForm.js`

1. `formValue` para guardar los datos del formulario
2. `useDispatch` para poder cambiar el estado
3. `useSelector` para consultar el estado
4. `onChange` para guardar los cambios en formValue.
5. `onSubmit` para hacer la validacion y cambiar el estado isError_FormAddPio
   a true si no pasa la validacion o false si si la pasa.

```js
import React, { useState } from 'react';import { Form, Button, Alert } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';import { validationFormAddPioActions } from '../redux/actions/ValidationsActions';

function FormAddPio(props) {  
  const [ formValue, setFormValue] = useState({ name: "", pio: "", });

  const dispatch = useDispatch();
  const errorForm = state => { dispatch(validationFormAddPioActions(state)) }

  const isErrorFormAddPio = useSelector(state => state.validations.isError_FormAddPio);

  const onChange = (e) => {
    setFormValue({
      ...formValue,
      [e.target.name]: e.target.value
    });
  }

  const onSubmit = (e) => {
    e.preventDefault();
    const {name, pio} = formValue;
    if(!name && !pio) {
      errorForm(true);
    } else {
      errorForm(false);
    }
  }

  return (
    <form className="form-add-pio-container" onChange={onChange} onSubmit={onSubmit}>
      <Form.Group className="text-center">
        <h2>Crear Pio</h2>
      </Form.Group>
      <Form.Group className="text-center">
        <Form.Control type="text" name="name" placeholder="Escribe tu nombre" />
      </Form.Group>
      <Form.Group className="text-center">
        <Form.Control as="textarea" name="pio" row="3" placeholder="Escribe el pio..." />
      </Form.Group>
      <Button variant="primary" type="submit">Enviar Pio</Button>
      {isErrorFormAddPio && ( <Alert variant="danger" className="mt-4" >Todos los campos son obligatorios</Alert> )}
    </form>
  )
}
export default FormAddPio;
```

**Resultado:** Al querer mandar un pio vacio cambie el estado de error
a true y se muestra el error, y al llenar la informacion pasa a false
y con este cambio se muestra o oculta el error

![53-mostrar-ocultar-error-con-redux](./img/53-mostrar-ocultar-error-con-redux.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### 153. Guardando el tweet en el store de Redux

A. Agregamos nuevo estado redux `pios` para guardar nuestros pios.

A-1. Creamos Action en `./src/redux/actions/ValidationsActions.js`

```js
export const addPioAction = (state) => {
  return { type: 'ADD_PIO', payload: state }
};
```

A-2. Creamos reducer `./src/redux/reducers/validationsReducer.js`

```js
const initialProps = { pios: [] }
export default function(state = initialProps, action) {
  switch (action.type) {
    case "ADD_PIO":
      return {
        ...state,
        pios: [...state.pios, action.payload]
      };
    //break;
    default:
      return state;
    //break;
  }
}
```

A-3. Lo agregamos en `./src/redux/reducers/index.js`

```js
  import { combineReducers } from 'redux'; import ModalsReducer from "./ModalsReducer"; import ValidationsReducer from "./validationsReducer";
+ import PiosReducer from "./PiosReducer";

export default combineReducers({
  modals: ModalsReducer,
  validations: ValidationsReducer,
+ pios: PiosReducer,
});
```

B. En FormAddPios

1. importamos addPioAction y lo usamos para crear `addPio`
2. Usamos addPio cuando el pio no causa error
3. importamos moment para poner fecha a nuestro tweet y uuid para su id

```js
import React, { useState } from 'react'; import { Form, Button, Alert } from 'react-bootstrap'; import { useDispatch, useSelector } from 'react-redux'; import { validationFormAddPioActions } from '../redux/actions/ValidationsActions';
+ import { addPioAction } from '../redux/actions/PiosActios';
+ import moment from "moment";
+ import uuid from "uuid/v4";

function FormAddPio(props) {  
  const [ formValue, setFormValue] = useState({ name: "", pio: "", });
  const dispatch = useDispatch(); const errorForm = state => { dispatch(validationFormAddPioActions(state)) } const closeModal = () => { dispatch( openCloseAddPioModalAction(false)); }
+ const addPio = state => { dispatch( addPioAction(state)); }
  const isErrorFormAddPio = useSelector(state => state.validations.isError_FormAddPio);
  const onChange = (e) => { setFormValue({ ...formValue, [e.target.name]: e.target.value }); }
  const onSubmit = (e) => {
    e.preventDefault(); const {name, pio} = formValue;
    if(!name && !pio) {
      errorForm(true);
    } else {
      errorForm(false);
+     addPio({id: uuid(), name, pio, date: moment()});
      closeModal();
    }
  }
  return (... )
}
export default FormAddPio;
```

**Resultado:** Podemos guardar los pios en redux, pero si recargamos
la aplicacion se borran los tweets ya que estos no se guardan.

![54-guardar-pios-en-estado-redux](./img/54-guardar-pios-en-estado-redux.gif)
