# React Native: Creando un TripAdvisor de restaurantes

- [React Native: Creando un TripAdvisor de restaurantes](#react-native-creando-un-tripadvisor-de-restaurantes)
  - [Accesos Rapidos](#accesos-rapidos)
    - [Documentacion](#documentacion)
  - [Codigos guadados](#codigos-guadados)
  - [Por el camino](#por-el-camino)
  - [Manejo de navegacion centralizado y/o fuera de componentes](#manejo-de-navegacion-centralizado-yo-fuera-de-componentes)
    - [Cambiar de input al terminar de editar](#cambiar-de-input-al-terminar-de-editar)
  - [Introduccion](#introduccion)
    - [Crear proyecto](#crear-proyecto)
    - [28. Estructura de un proyecto](#28-estructura-de-un-proyecto)
    - [29. Ejecutando la Aplicación en Android y iOS](#29-ejecutando-la-aplicación-en-android-y-ios)
    - [31. Creando las 5 páginas principales de nuestra aplicación](#31-creando-las-5-páginas-principales-de-nuestra-aplicación)
    - [32. Instalando React Navigation](#32-instalando-react-navigation)
    - [33. Montando el Sistema de Navegación](#33-montando-el-sistema-de-navegación)
    - [34. Creando Stacks de Navegación](#34-creando-stacks-de-navegación)
    - [35.36. Instalando React Native Elements. Añadiendo iconos al Tab Menu](#3536-instalando-react-native-elements-añadiendo-iconos-al-tab-menu)
  - [Sección 7:Sistema de registro y login de usuarios](#sección-7sistema-de-registro-y-login-de-usuarios)
    - [39. Mostrando Screen diferente si el usuario esta logeado o no](#39-mostrando-screen-diferente-si-el-usuario-esta-logeado-o-no)
    - [40.41 Componente de Loading](#4041-componente-de-loading)
    - [42. Screen de usuario no logeados](#42-screen-de-usuario-no-logeados)
    - [43.44. Creando Screen login y añadiéndola al sistema de rutas y agregar Enlace a este](#4344-creando-screen-login-y-añadiéndola-al-sistema-de-rutas-y-agregar-enlace-a-este)
    - [45. Estructura de la Screen de login](#45-estructura-de-la-screen-de-login)
    - [46.47.48. Creando Screen Registro y añadiendo al sistema de rutas y navegacion](#464748-creando-screen-registro-y-añadiendo-al-sistema-de-rutas-y-navegacion)
    - [49. Creando el formulario de registro de usuarios](#49-creando-el-formulario-de-registro-de-usuarios)
    - [50. Evitando que el teclado oculte el input](#50-evitando-que-el-teclado-oculte-el-input)
    - [51. Añadiendo iconos a los inputs del formulario de registro](#51-añadiendo-iconos-a-los-inputs-del-formulario-de-registro)
    - [52. Mostrando y ocultando contraseña con el icono](#52-mostrando-y-ocultando-contraseña-con-el-icono)
    - [53. Guardando los datos del formulario de registro un estado](#53-guardando-los-datos-del-formulario-de-registro-un-estado)
    - [54.55 Validar formulario de registro](#5455-validar-formulario-de-registro)
    - [56. Mostrando los mensajes de la validación en un toast](#56-mostrando-los-mensajes-de-la-validación-en-un-toast)
    - [57. Crear PETICION para crear registrar usuario en BACKEND](#57-crear-peticion-para-crear-registrar-usuario-en-backend)
    - [58.59 Agregar navegacion cuando el registro de usuario es exitoso](#5859-agregar-navegacion-cuando-el-registro-de-usuario-es-exitoso)
    - [60. Formulario de login](#60-formulario-de-login)
    - [Agregar toast a Login](#agregar-toast-a-login)
    - [65. Realizando el login con el Backend](#65-realizando-el-login-con-el-backend)
    - [65. Guardar token y usarlo para saber si esta logueado el usuario](#65-guardar-token-y-usarlo-para-saber-si-esta-logueado-el-usuario)
    - [IsFocused: Ejecutar codigo cuando entramos o salimos de screen (navegamos)](#isfocused-ejecutar-codigo-cuando-entramos-o-salimos-de-screen-navegamos)
    - [Cerrar Session y extraer informacion del token](#cerrar-session-y-extraer-informacion-del-token)
  - [Sección 8:Panel de usuario](#sección-8panel-de-usuario)
    - [Crear Usuario Info mostrando datos de usuario de la API](#crear-usuario-info-mostrando-datos-de-usuario-de-la-api)
    - [70. Instalando Packages para poder acceder a la galería del teléfono](#70-instalando-packages-para-poder-acceder-a-la-galería-del-teléfono)
    - [71. Abriendo la galeria de imágenes](#71-abriendo-la-galeria-de-imágenes)
    - [76. Actualizando la imagen sin tener que recargar la app](#76-actualizando-la-imagen-sin-tener-que-recargar-la-app)
    - [77. Lista de opciones disponibles en la cuenta del usuario](#77-lista-de-opciones-disponibles-en-la-cuenta-del-usuario)
    - [79. Componente Modal](#79-componente-modal)
    - [80. Cambiando el contenido del modal dependiendo el menú haga click el usuario](#80-cambiando-el-contenido-del-modal-dependiendo-el-menú-haga-click-el-usuario)
    - [81. Creando formulario para actualizar nombre de usuario](#81-creando-formulario-para-actualizar-nombre-de-usuario)
    - [85. Formulario para cambiar el email](#85-formulario-para-cambiar-el-email)
    - [85. Formulario para cambiar el contraseña](#85-formulario-para-cambiar-el-contraseña)
    - [RESULTADO: Codigo](#resultado-codigo)
  - [Sección 9:Creación de productos](#sección-9creación-de-productos)
    - [96. Botón para crear producto solo para logueados](#96-botón-para-crear-producto-solo-para-logueados)
    - [98. Creando y navegando a la Screen para crear restaurantes](#98-creando-y-navegando-a-la-screen-para-crear-restaurantes)
    - [99. Formulario de alta de restaurantes](#99-formulario-de-alta-de-restaurantes)
    - [100. Guardando los datos del producto en el estado](#100-guardando-los-datos-del-producto-en-el-estado)
    - [101. Componente para subir imágenes del restaurante](#101-componente-para-subir-imágenes-del-restaurante)
    - [102. Selecionando imágenes de la galeria](#102-selecionando-imágenes-de-la-galeria)
    - [103. Mostrando las imágenes seleccionados](#103-mostrando-las-imágenes-seleccionados)
    - [104. Eliminar cualquier imagen seleccionada](#104-eliminar-cualquier-imagen-seleccionada)
    - [105. Mostrando la imagen principal del restaurante](#105-mostrando-la-imagen-principal-del-restaurante)
    - [106. Icono para abrir el mapa y seleccionar la localización del restaurante](#106-icono-para-abrir-el-mapa-y-seleccionar-la-localización-del-restaurante)
    - [107. Instalando y explicando Expo MapView y Location](#107-instalando-y-explicando-expo-mapview-y-location)
    - [108. Obteniendo la localización del dispositivo](#108-obteniendo-la-localización-del-dispositivo)
    - [109. Abriendo Google Map con la localización del usuario](#109-abriendo-google-map-con-la-localización-del-usuario)
    - [110. Guardando localización del restaurante](#110-guardando-localización-del-restaurante)
    - [111. Validando formulario para subir productos](#111-validando-formulario-para-subir-productos)
    - [112. Activando icono, cambiar de color a icono](#112-activando-icono-cambiar-de-color-a-icono)
    - [113. Subiendo las imágenes del restaurante al Storage](#113-subiendo-las-imágenes-del-restaurante-al-storage)
    - [116. Configuran API Google Maps](#116-configuran-api-google-maps)

## Accesos Rapidos

### Documentacion

- Documentacion Oficial: [reactnative.dev](https://reactnative.dev/docs/)
- Equivalente a bootstrap: [reactnativeelements.com](https://reactnativeelements.com/docs/)
- Buscar Iconos: [materialdesignicons.com](http://materialdesignicons.com/)
- Lodash Libreria utilerias popular (colecciones, math, etc.): [npmjs.com](https://www.npmjs.com/package/lodash)
- Mostrar toasts: [react-native-easy-toast](https://www.npmjs.com/package/react-native-easy-toast)
- Debbug: [react-native-debugger](https://github.com/jhen0409/react-native-debugger/blob/master/docs/getting-started.md)
- Guardar info persistente (llave-valor): [react-native-async-storage](https://react-native-async-storage.github.io/async-storage/docs/install)

## Codigos guadados

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

----

----

<!--
. separacion capitulo, divicion capitulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

## Por el camino

## Manejo de navegacion centralizado y/o fuera de componentes

- Fuente: [stackoverflow](https://stackoverflow.com/questions/61170112/how-to-use-navigation-navigate-from-a-component-outside-the-stack-navigation)

**Objetivo:** Crear un Util que nos sirva para navegar, el cual este lo
usaremos fuera de los componentes, por ejemplo, para manejo de errores
por peticiones al servidor.

A. Create a new file named: `RootNavigation.js`

```js
// RootNavigation.js
import * as React from 'react';
export const navigationRef = React.createRef();
export function navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}
```

B. En donde tenemos declarada nuestra navegacion en mi caso agrgamos
la referecnia: `App\navigation\Navigation.js`

```js
// file where you have the navigation
import {navigationRef} from './path/to/RootNavigation';
  <NavigationContainer ref={navigationRef}>
  .....
    <Footer />
  </NavigationContainer>
```

C. Lo usamos, por ejemplo `App\utils\RequestUtils.js`

```js
import * as RootNavigation from './RootNavigation';

export function handleRequestError(errorResponse) {

  if(errorResponse.error == "NO_TOKEN" ) {
    RootNavigation.navigate('login');
    return ;
  }

  return "Error de servidor";
}
```

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Cambiar de input al terminar de editar

<!-- cambiar al siguiente input, enfocar siguiente input, proximo input -->

- Fuente:  [stackoverflow](https://stackoverflow.com/questions/32748718/react-native-how-to-select-the-next-textinput-after-pressing-the-next-keyboar)

```js
  export default function LoginForm(props) {
+   let inputPass = null;
    
    return (
    <View style={styles.formContainer}>
      <Input placeholder="Correo electronico"
+       onSubmitEditing={() => { inputPass.focus(); }}
      />
      <Input placeholder="Contraseña"
+       ref={(input) => { inputPass = input; }}
        onSubmitEditing={() => { onSubmit() }}
      />
      <Button title="Iniciar sesión" onPress={() => { onSubmit()  } } />
```

**Resultado:** Cuando le demos enter al campo de email este pasara al
de contraseña automaticamente.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

----

----

<!--
. separacion capitulo, divicion capitulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

## Introduccion

- Documentacion Oficial: [reactnative.dev](https://reactnative.dev/docs/)

A. Instalacion del cli: `yarn global add expo-cli`

En mi caso estoy trabajando con y se me instalo la version

- Yarn: 1.21.1
- expo-cli: 3.28.5

B.OpcionA Podemos abrir nuestros emuladores en android-studio->tools->abd manager

B2.OpcionB Abrir abd: En mi caso lo tengo en `E:\Progamas\Java\android_sdk\tools\bin`

> Error: ClassNotFoundException: javax.xml.bind.annotation.XmlSchema
> Solucion: `set JAVA_OPTS=-XX:+IgnoreUnrecognizedVMOptions --add-modules java.se.ee`

B2.1. Vamos a la carpeta de arruba y ejecutamos: `avdmanager list avd`

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Crear proyecto

A. Desde la terminal vamos a una carpeta donde queremos nuestro proyecto
en mi caso: `E:\cursos\0activ\react-native`

B. Crear proyecto react-native: `expo init nombre-proyecto`

- Nos dara una serie de opciones
- Seleccionamos la `blank`

**Resultado:** Al terminar se nos el codigo de la aplicacion, mas
aelante veremos como iniciar la aplicacion

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 28. Estructura de un proyecto

Tenemos los siguientes carpetas/archivos

- `assets`: Son los archivos estaticos como imagenes, svg, csv, etc
- `node_modules`: depenencias
- `App.js` Nuestro componente root
- `app.json`: Configuracion de nuestra aplicacion, como el nombre, plataforma,
  orientacion, icono, splash, conf para android, conf para ios
- `package.json`: Declaracion de las ddependencias y comandos como el
  de iniciar en nuestro local en emulador android/ios/web

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 29. Ejecutando la Aplicación en Android y iOS

<!-- iniciar aplicacion react, iniciar app, correr aplicacion, ejecutar aplicacion, ejecutar app -->
A. Ejecutamos el comando: `expo start`

- La consola nos indicara que podemos abrir `http://localhost:19002` y
  si entramos veremos la opcion de...
- `Run in web Browser`: Tambien podemos correrlo en el navegador, el cual
  funcionara de la misma manera
- `Run on Android device/emulator`, le damos click y despues de unos minutos
  terminara e construir y nos abrira la aplicacion
  - Nota: Lo que hace es instalar una app en nustro emulador llamada
    expo, la cual pondra dentro de esta nuestra aplicacion

![ejecutar-app](./img/01-ejecutar-app.PNG)

- `Hot-reload`: Solo con modificar nustro codigo se vera reflejado el
  cambio en nuestro emulador

**Resultado:** Corrimos nuestra aplicacion en nustro emulador y en web.

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 31. Creando las 5 páginas principales de nuestra aplicación

**Objetivo:** Crear las 5 pantallas de nuestra por las que navegara nuestra
aplicacion.

A. En nustra raiz creamos una carpeta con otra carpeta dentro que tendra
   nustras vistas

```r
-App/
 -screens/
   -Account.js
   -Buscador.js
   -Favorites.js
   -Products.js
   -TopProducts.js
```

B. A todos les ponemos el mismo contenido solo cambiara el nombre

```js
import React from 'react'
import { View, Text } from 'react-native'

export default function Favorites() {
  return (
    <View>
      <Text></Text>
      <Text></Text>
      <Text>Favorites</Text>
    </View>
  );
}

```

**Resultado:** Tenemos los componentes que funcionaran como las diferentes
pantallas de nuestro proyecto.

![02-paginas.PNG](./img/02-paginas.PNG)

### 32. Instalando React Navigation

- Fuente: [reactnavigation.org](https://reactnavigation.org/docs/getting-started)

**Introduccion** Ya tenemos nuestras pantallas ahora para poder navegar
entre ellas, necesitaremos de `React Navigation` el cual tendremos
instalar.

<!-- instalar dependencia, agregar dependencias -->

A. instalar componente: `yarn add @react-navigation/native@5.1.5`

B. Tambien instalaremos otras dependencias para que funcione con expo.io
que es el que gestiona nuestra aplicacion (como dice la docu.)

```r
yarn add @react-navigation/native@^5.8.10

expo install react-native-gesture-handler@~1.6.0
expo install react-native-reanimated@~1.13.0
expo install react-native-screens@~2.10.1
expo install react-native-safe-area-context@3.1.4
expo install @react-native-community/masked-view@~0.1.10

yarn add @react-navigation/stack@^5.12.8
yarn add @react-navigation/bottom-tabs@^5.11.2

yarn add modal-react-native-web

```

C. Instalamos: `yarn add @react-navigation/stack`, este paquete nos
permitira crear **stacks** que son conjuntos de elementos que conforman
un screen, por ejemplo, la parte e comentarios, detalles de producto, etc.

D. Instalamos: `yarn add @react-navigation/bottom-tabs` que es el que
nos servira para crear el menu de navegacion

**Resultado:** En mi caso se instalaron en mi `package.json`

```json
  "dependencies": {
+   "@react-native-community/masked-view": "0.1.10",
+   "@react-navigation/bottom-tabs": "~5.2.6",
+   "@react-navigation/native": "~5.1.5",
+   "@react-navigation/stack": "~5.2.10",
    "expo": "~39.0.2",
    "expo-status-bar": "~1.0.2",
    "react": "16.13.1",
    "react-dom": "16.13.1",
    "react-native": "https://github.com/expo/react-native/archive/sdk-39.0.4.tar.gz",
+   "react-native-gesture-handler": "~1.7.0",
+   "react-native-reanimated": "~1.13.0",
+   "react-native-safe-area-context": "3.1.4",
+   "react-native-screens": "~2.10.1",
    "react-native-web": "~0.13.12"
  },
  "devDependencies": {
    "@babel/core": "~7.9.0"
  },
```

**Resultao:** Estamos listos para empezar a realizar nuestra navegacion

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 33. Montando el Sistema de Navegación

A. Cremos en la raiz `Navigation/Navigation.js`

- `initialRouteName`: Indicamos con que se mostrara primero

```js
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Account from "../screens/Account";
import Buscador from "../screens/Buscador";
import Favorites from "../screens/Favorites";
import Products from "../screens/Products";
import TopProducts from "../screens/TopProducts";

const Tab = createBottomTabNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="products" >
        <Tab.Screen name="cuenta" component={Account} options={{ title: "Cuenta" }} />
        <Tab.Screen name="buscador" component={Buscador} options={{ title: "Buscador" }} />
        <Tab.Screen name="favorites" component={Favorites} options={{ title: "Favoritos" }} />
        <Tab.Screen name="products" component={Products} options={{ title: "Productos" }} />
        <Tab.Screen name="topProducts" component={TopProducts} options={{ title: "Prouctos Top" }} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
```

**Resultado:** Podemos navegar entre pantallas

![Navegacion](./img/03-navegacion.gif)

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 34. Creando Stacks de Navegación

**Introduccion:** Como mencionamos vamos a tener mas screens de las que
agregamos apenas arriba, por ejemplo, el screen de cuenta, va a tener
la pantalla de registro, detalles, configuracion y lo mismo para la
pantalla de productos tendra detalles, comentario etc. Cada uno de estos
sub-conjuntos estaran dentro de `stacks`.

**Objetivo:** Crear los stacks de cada seccion de nuestra aplicacion, esto
igual nos servira para `agregar cabezera` a nustras pantallas

A. Creamos un archivo Stack para cada screen, por ejemplo.

```js
import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import Products from "./../screens/Products";

const Stack = createStackNavigator();

export default function ProductsStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Products" component={Products} options={{ title: "Productos" }} ></Stack.Screen>
    </Stack.Navigator>
  )
}
```

B. Actualizamos Por todos los screens que creamos

```js
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import AccountStack from "./AccountStack";
import SearchsStack from "./SearchsStack";
import FavoritesStack from "./FavoritesStack";
import ProductsStack from "./ProductsStack";
import TopProductsStack from "./TopProductsStack";

const Tab = createBottomTabNavigator();


export default function Navigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="products">
#       <Tab.Screen name="cuenta" component={AccountStack} options={{ title: "Cuenta" }} />
#       <Tab.Screen name="buscador" component={SearchsStack} options={{ title: "Buscador" }} />
#       <Tab.Screen name="favorites" component={FavoritesStack} options={{ title: "Favoritos" }} />
#       <Tab.Screen name="products" component={ProductsStack} options={{ title: "Productos" }} />
#       <Tab.Screen name="topProducts" component={TopProductsStack} options={{ title: "Prouctos Top" }} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
```

**Resultado:** Cada uno de nuestras pantallas esta dentro de un stack,
en los cuales podremos meterle mas pantallas, y tambien se nos
agrego un header con el titulo en el que esta nuestro stack.

![04-meter-screen-en-stack](./img/04-meter-screen-en-stack.PNG)

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 35.36. Instalando React Native Elements. Añadiendo iconos al Tab Menu

<!-- uso de iconos, usar iconos, agregar icono, poner icono-->

- Fuente: [reactnativeelements.com](https://reactnativeelements.com/docs/)
- Fuente: [Uso de iconos](https://reactnativeelements.com/docs/icon)

**Introduccion**: Esta es una libreria con elementos ya realizados por
terceros que nos ayudaran, equivalente a instalar un bootrap para un
entorno web, .

A. Instalamos dependencia: `yarn add react-native-elements`

B. `App\navigation\Navigation.js` Importamos iconos y los usamos

```js
import { Icon } from "react-native-elements";
```

C. MOdificamos el `App\navigation\Navigation.js`

- `tabBarOptions={{`: Cambiar el color del icono activo y desactivo
- `screenOptions={({`: asi pasamos la ruta y el color que tiene actualmente
  el boton a la fucion que creamos mas abajo.
- `function screenOptions(route, color)`: asignar icono dinamico junto
  con su color.

```js
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon } from "react-native-elements";
import AccountStack from "./AccountStack";
import SearchsStack from "./SearchsStack";
import FavoritesStack from "./FavoritesStack";
import ProductsStack from "./ProductsStack";
import TopProductsStack from "./TopProductsStack";

const Tab = createBottomTabNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="products"
+       tabBarOptions={{ inactiveTintColor: "#646465", activeTintColor: "#00a680" }}
+       screenOptions={({ route }) => ({ tabBarIcon: ({ color }) => screenOptions(route, color) })}
      >
        <Tab.Screen name="cuenta" component={AccountStack} options={{ title: "Cuenta" }} />
        <Tab.Screen name="buscador" component={SearchsStack} options={{ title: "Buscador" }} />
        <Tab.Screen name="favorites" component={FavoritesStack} options={{ title: "Favoritos" }} />
        <Tab.Screen name="products" component={ProductsStack} options={{ title: "Productos" }} />
        <Tab.Screen name="topProducts" component={TopProductsStack} options={{ title: "Prouctos Top" }} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

+function screenOptions(route, color) {
  let iconName = "";
  switch (route.name) {
    case "cuenta": iconName = "account-circle-outline" break;
    case "buscador": iconName = "magnify" break;
    case "favorites": iconName = "heart-outline" break;
    case "products": iconName = "compass-outline" break;
    case "topProducts": iconName = "star-outline" break;
    default: console.log("Navitation.screenOptions() switch default option"); break;
  }
  return (
    <Icon type="material-community" name={iconName} size={22} color={color} />
  )
}
```

**Resultado:** Tenemos los iconos del menu de navegacion, y estos iconos
igual cambiaran con el color de activo o desactivo.

![05-poner-iconos-a-nav-bar](./img/05-poner-iconos-a-nav-bar.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

----

----

<!--
. separacion capitulo, divicion capitulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

## Sección 7:Sistema de registro y login de usuarios

### 39. Mostrando Screen diferente si el usuario esta logeado o no

**Objetivo:** Mostrar en la pantalla de usuario ya sea una pantalla para
un usuario loguedo o una de un usuario que no se ha logueado.

A. Cremos las pantallas para usuario NO-logueado y Si-logueado, por lo
mientras ambos tendran lo mismo, solo cambia el texto a mostrar

- Creamos: `App\screens\Account\AccountGuess.js`
- Creamos: `App\screens\Account\AccountLogged.js`

```js
import React from 'react'
import { View, Text } from 'react-native'

export default function AccountGuess() {
  return (
    <View>
      <Text></Text>
      <Text></Text>
      <Text>AccountGuess</Text>
    </View>
  );
}
```

B. Creamos el cosumo de API (en mi caso spring) para saber si el usuario
esta logueado o no: `App\Service\AuthService.js`

- `createRequestOptions`: Funcion que nos ayudara a crear las cabezeras
  y otras cosas para las peticiones
- `getData`: metodo para hacer las peticiones GET genericas
- `isAuth()`: Nos dira si esta logueado o no por medio de un false o un
  objeto con la info del usuario logueado
- `response.ok`: Existen varios tipos de errores, que no responda, que responda
  pero con un error, o que responda correctamente, y esto no ayuda a saberlo.

```js
function createRequestOptions(token="xxx") {
  let myHeaders = new Headers();
  //myHeaders.append("Authorization", "Bearer XXX");  
  let requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };
  return requestOptions;
}

async function getData(url) {
  const response = await fetch(
    url,
    createRequestOptions()
  );
  return response;
}

export function isAuth() {
  return new Promise(function(resolve, reject) {
    getData("http://127.0.0.1:8080/api/users/49")
    .then(response => {
      if(response.ok) {
        resolve(response.json());
      } else {
        reject(false);
      }
    })
    //.then(result => console.log("Result: ", result))
    .catch(error => {
      reject(false);
    });
  });
}
```

B. Movemos nuestro componente Account (`App\screens\Account\Account.js`)
y le agregamos.

- `setlogin] = useState`: Aqui guardaremos al usuario y lo ponemos como
  estado, ya que con el tiempo cambiara su valor, y esto nos permitira
  darle seguimiento a estos cambios.
- `useEffect(()`: cono no definimos nada en `}, [])` este se ejecutara
  la primera vez que se muestre el componente nadamas.
- `isAuth()`: es el metodo que nosotros creamos, el cual con este sabremos
  si el usuario esta logueado y obtener sus datos.
- `if(login`: Asi vamos a decidir si mostramos cargando, visita o registrado.

```js
import React, { useState, useEffect } from 'react'
import { View, Text } from 'react-native'
import AccountGuess from "./AccountGuess";
import AccountLogged from "./AccountLogged";
import { isAuth } from "../../Service/AuthService";

export default function Account() {
  const [login, setlogin] = useState(null);
  
  useEffect(() => {
    isAuth()
      .then(result => {
        console.log("Result: ", result)
        setlogin(login);
      })
      .catch(error => {
        console.log('error', error)
        setlogin(error);
      } );
  }, [])

  if(login === null) { return <Text>Cargando...</Text> }

  return (login === true) ? <AccountLogged /> : <AccountGuess />;
}
```

**Resultado:** Tenemos el uso de 3 vistas, ya sea para mostrar cargando,
visita o registrado, aunque solo emos visto las primeras dos.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 40.41 Componente de Loading

A. Creamos nuevo componente `App\component\Loading.js`

- `import { Overlay`: Usaremos de reactnativeelements el compoente
  [overlay](https://reactnativeelements.com/docs/overlay) que nos
  ayudara a crear la pantalla
- `import { stylesheet`: este stylesheet nos ayudara a crear los estilos
  ya que no podemos hacerlo con css
- `import { ActivityIndicator`: Es la animacion de loading.

```js
import React from "react";
import { StyleSheet, View, Text, ActivityIndicator } from "react-native";
import { Overlay } from "react-native-elements";
//import Modal from 'modal-react-native-web';

export default function Loading(props) {
  const { isVisible, text } = props;

  return (
    <Overlay
      //ModalComponent={Modal} // usarlo para que se vea bien en el navegador, pero dara error en Android
      isVisible={isVisible}
      windowBackgroundColor="rgba(0, 0, 0, 0.5)"
      overlayBackgroundColor="transparent"
      overlayStyle={styles.overlay}
    >
      <View style={styles.view}>
        <ActivityIndicator size="large" color="#00a680" />
        {text && <Text style={styles.text}>{text}</Text>}
      </View>
    </Overlay>
  );
}

const styles = StyleSheet.create({
  overlay: {
    height: 100,
    width: 200,
    backgroundColor: "#fff",
    borderColor: "#00a680",
    borderWidth: 2,
    borderRadius: 10,
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#00a680",
    textTransform: "uppercase",
    marginTop: 10,
  },
});
```

B. Agregamos el nuevo compoenete en `App\screens\Account\Account.js`

```js
import React, { useState, useEffect } from 'react'
import AccountGuess from "./AccountGuess";
import AccountLogged from "./AccountLogged";
import { isAuth } from "../../Service/AuthService";
import Loading from "../../component/Loading";

export default function Account() {

  const [login, setlogin] = useState(null);

  useEffect(() => {
    isAuth()
      .then(result => {
        console.log("Result: ", result)
        setlogin(login);
      })
      .catch(error => {
        console.log('error', error)
        setlogin(error);
      } );
  }, [])

+ if(login === null) { return <Loading isVisible={true} text="Cargando..." /> }

  return (login === true) ? <AccountLogged /> : <AccountGuess />;
}
```

**Resultado:** Podremos ver la pantalla de carga por un momento, y luego
como la peticion al servidor indico que el usr no esta logueado, nos
muestra al final la pantalla de invitado,

![alt](./img/06-consulta-get-y-screen-loading.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 42. Screen de usuario no logeados

**Objetivo:** Vamos a crear/completar la vista para los usuarios no
logueados,

A. Descargamos la [imagen](xxx)

B. En nuestro `App\screens\Account\AccountGuess.js` agregamos

- `ScrollView`: para poder hacer scroll, agregar scroll
- `source={require("../...")`: agregar imagen local
- `buttonStyle`: Es el estilo del boton
- `containerStyle`: Estilo del contenedor del boton

```js
import React from 'react'
// CUIDADO: react-native tiene su propio boton pero nosotros usaremos el de "react-native-elements"
// me paso que importe el otro y no ne aseptaba los estilos
import { View, Text, StyleSheet, ScrollView, Image, /* Button */ } from 'react-native'
import { Button } from "react-native-elements";

export default function AccountGuess() {
  return (
    <ScrollView centerContent={true} style={styles.myViewBody} >
      <Image source={require("../../../assets/user-guest.jpg") } resizeMode="contain" style={styles.myImage} />
      <Text style={styles.myTitle}>Entra o crea una cuenta</Text>
      <Text style={styles.description}>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam illo, adipisci dignissimos ex nostrum tempora voluptatibus saepe voluptate! Ipsa voluptate enim possimus quaerat natus perferendis ratione sit. Nisi, delectus possimus!</Text>
      <View style={styles.btnEntrarView}>
        <Button title="Entrar"
          onPress={() => { console.log("Mua CLick!!"); }}
          buttonStyle={styles.btnEntrar}
          containerStyle={styles.btnEntrarCo}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  myViewBody: {
    marginLeft: 30,
    marginRight: 30
  },
  myImage: {
    height: 300,
    width: "90%",
    marginBottom: 40,
  },
  myTitle: {
    fontWeight: "bold",
    fontSize: 19,
    marginBottom: 10,
    textAlign: "center",
  },
  description: {
    textAlign: "center",
    marginBottom: 20,
  },
  btnEntrarView: {
    flex: 1,
    alignItems: "center"
  },
  btnEntrar: {
    backgroundColor: "#00a680",
  },
  btnEntrarCo: {
    width: "70%",
  },
});
```

**Resultado:** tenemos nuestra vista formateada para los usuarios anonimos

![07-vista-usuario-anonimo](./img/07-vista-usuario-anonimo.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 43.44. Creando Screen login y añadiéndola al sistema de rutas y agregar Enlace a este

**Objetivo:** Crear el formulario de login la cual accederemos desde
la pantalla de usuario no-loguado.

A. **Agrgando login al sistema de rutas**

A.1 Creamos: `App\screens\Account\Login.js`

```js
import React from 'react'
import { View, Text } from 'react-native'
export default function Login() {
  return (
    <View><Text>Login Form</Text></View>
  );
}
```

A.2 Lo importamos al stack de Account: `App\navigation\AccountStack.js`

```js
import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import Account from "../screens/Account/Account";
+ import Login from "../screens/Account/Login";

const Stack = createStackNavigator();

export default function AccountStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="account"
        component={Account}
        options={{ title: "Cuenta" }}>
      </Stack.Screen>

+     <Stack.Screen
+       name="login"
+       component={Login}
+       options={{ title: "Login" }} >
+     </Stack.Screen>

    </Stack.Navigator>
  )
}
```

B. **Agregar enlace a login**: Vamos a hacer que cuando se le de click
sobre un boton nos habra la bista de login

B.1. En `App\screens\Account\AccountGuess.js` agregamos el enlace

```js
import React from 'react'
import { View, Text, StyleSheet, ScrollView, Image, /* Button */ } from 'react-native'
import { Button } from "react-native-elements";
+ import { useNavigation } from "@react-navigation/native";

export default function AccountGuess() {

+ // nos traera nuestro sistema de navegacion, con funciones como push, pop, conGoBack etc.
+ const navigation = useNavigation();

  return (
    <ScrollView centerContent={true} style={styles.myViewBody} >
      <Image source={require("../../../assets/user-guest.jpg") } resizeMode="contain" style={styles.myImage} />
      <Text style={styles.myTitle}>Entra o crea una cuenta</Text>
      <Text style={styles.description}>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam illo, adipisci dignissimos ex nostrum tempora voluptatibus saepe voluptate! Ipsa voluptate enim possimus quaerat natus perferendis ratione sit. Nisi, delectus possimus!</Text>
      <View style={styles.btnEntrarView}>
        <Button title="Entrar"
+         // el texto debe ser el mismo que pusimos en App\navigation\AccountStack.js en (name="xxx")
+         onPress={() => { navigation.navigate("login") }}
          buttonStyle={styles.btnEntrar}
          containerStyle={styles.btnEntrarCo}
        />
      </View>
    </ScrollView>
  );
}

= const styles = StyleSheet.create({...});
```

**Resultado:** Podemos navegar de screen no-logueado a login desde
un boton comun.

![08-boton-enlace-a-login](./img/08-boton-enlace-a-login.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 45. Estructura de la Screen de login

**Objetivo:** Vamos a crear la primera parte del login, solo lo suficiente
para pasarnos al screen de registro ya que necesitamos primero esta
funcionalidad.

```js
import React from 'react'
import { StyleSheet, View, ScrollView, Text, Image } from "react-native";
import { Divider } from "react-native-elements"; // nos servira para un <hr />
import { log } from 'react-native-reanimated';

export default function Login() {
  return (
    <ScrollView>
      <Image
        source={require("../../../assets/img/logo-login.png")}
        resizeMode="contain"
        style={styles.logo}
      />
      <View style={styles.viewContainer}>
        <Text>Login</Text>
        <CreateAccount />
      </View>
      <Divider style={styles.divider} />
      <Text>Social Login</Text>
    </ScrollView>
  );
}

function CreateAccount() {
  return (
    <Text style={styles.textRegister}>
      Aun no tienes una cuenta?{" "}
      <Text onPress={()=>{ console.log("Registro..."); }} style={styles.btnRegister}>Registrate</Text>
    </Text>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: "100%",
    height: 150,
    marginTop: 20,
  },
  viewContainer: {
    marginRight: 40,
    marginLeft: 40,
  },
  textRegister: {
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10,
  },
  btnRegister: {
    color: "#00a680",
    fontWeight: "bold",
  },
  divider: {
    backgroundColor: "#00a680",
    margin: 40,
  },
});
```

**Resutaldo:** Tenemos el screen con un enlace en el cual le pondremos
que vaya al registro desde este.

![09-vista-login-1](./img/09-vista-login-1.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 46.47.48. Creando Screen Registro y añadiendo al sistema de rutas y navegacion

**Objetivo:** Vamos a crear nuestra pantalla de registro, que podamos
entrar a ella desde la pantalla d login.

A. Creamos `App\screens\Account\Register.js`

```js
import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

export default function Register() {
  return (
    <View>
      <Image
        source={require("../../../assets/img/logo-login.png")}
        resizeMode="contain"
        style={styles.logo}
      />
      <View style={styles.viewForm}>
        <Text>Register</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: "100%",
    height: 150,
    marginTop: 20,
  },
  viewForm: {
    marginRight: 40,
    marginLeft: 40,
  },
});
```

B. Añadimos nuestra nueva screen al sistema de rutas

```js
import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import Account from "../screens/Account/Account";
import Login from "../screens/Account/Login";
+ import Register from "../screens/Account/Register";

const Stack = createStackNavigator();

export default function AccountStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="account"
        component={Account}
        options={{ title: "Cuenta" }}>
      </Stack.Screen>

      <Stack.Screen
        name="login"
        component={Login}
        options={{ title: "Login" }} >
      </Stack.Screen>

+     <Stack.Screen
+       name="register"
+       component={Register}
+       options={{ title: "Registro" }} >
+     </Stack.Screen>

    </Stack.Navigator>
  )
}
```

C. En `App\screens\Account\Login.js` agregamos el la navegacion/enlace
a la pantalla de Register

**Resultad:** Tenemos la navegacion a nuestra pantalla de registro, nos
faltaria agregarle estilos.

![10-navegacion-a-login](./img/10-navegacion-a-login.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 49. Creando el formulario de registro de usuarios

**Objetivo:** Ya tenemos la navegacion y estructura de nuestra screen
de registro, vamos a agregar lo que es el fomulario

A. Creamos nuevo componente `App\component\Account\RegisterForm.js`

```js
import React  from 'react';
import { StyleSheet, View, ScrollView } from "react-native";
import { Input, Icon, Button } from "react-native-elements";

export default function RegisterForm() {
  return (
    <ScrollView>
      <Input
        placeholder="Correo electronico"
        containerStyle={styles.inputForm}
      />
      <Input
        placeholder="Contraseña"
        containerStyle={styles.inputForm}
        password={true}
        secureTextEntry={true}
      />
      <Input
        placeholder="Confirma contraseña"
        containerStyle={styles.inputForm}
        password={true}
        secureTextEntry={true}
      />
      <Button
        title="Registrarse"
        containerStyle={styles.btnContainerRegister}
        buttonStyle={styles.btnRegister}
        onPress={ console.log("Registro...") }
      />
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  inputForm: {
    width: "100%",
    marginTop: 20,
  },
  btnContainerRegister: {
    marginTop: 20,
    width: "95%",
  },
  btnRegister: {
    backgroundColor: "#00a680",
  },
  iconRight: {
    color: "#c1c1c1",
  },
});
```

B. Modificamos `App\screens\Account\Register.js` agregando el componente
que recien creamos de formulario a la pantalla registro.

```js
import React from 'react' import { View, Text, StyleSheet, Image } from 'react-native'
+ import RegisterForm from "../../component/Account/RegisterForm";

export default function Register() {
  return (
    <View>
      <Image source={require("../../../assets/img/logo-login.png")} resizeMode="contain" style={styles.logo} />
      <View style={styles.viewForm}>
+       <RegisterForm />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  logo: { width: "100%", height: 150, marginTop: 20, },
  viewForm: { marginRight: 40, marginLeft: 40, },
});
```

**Resultado:** Podemos ver el formulario en nustra pantalla de registro.
**Problema** tenemos que cuando escribimos un registro se muestra el
teclado el cual oculta el input, el cual vamos a solucionar mas abajo.

![11-formulario-registro-problema-teclado-oculta-input](./img/11-formulario-registro-problema-teclado-oculta-input.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 50. Evitando que el teclado oculte el input

- Enlace libreria: [react-native-keyboard-aware-scroll-view](https://classic.yarnpkg.com/es-ES/package/react-native-keyboard-aware-scroll-view)

**Objetivo** Vamos a instalar "react-native-keyboard-aware-scroll-view"
el cual nos permitira enfocar el campo en el que estamos escribiendo,
para que no se nos oculte.

A. Instalamos: `yarn add react-native-keyboard-aware-scroll-view`

- En mi caso me instalo la

```json
"react-native-keyboard-aware-scroll-view": "^0.9.3",
```

B. Agregamos el compoente a nuestro formulario, **CUIDADO** se lo debemos
de agregar al screen no al componente

```js
import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import RegisterForm from "../../component/Account/RegisterForm";
+ import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

export default function Register() {
  return (
    <KeyboardAwareScrollView>
      <Image source={require("../../../assets/img/logo-login.png")} resizeMode="contain" style={styles.logo} />
      <View style={styles.viewForm}>
        <RegisterForm /> // AQUI TENEMOS EL FORMULARIO QUE TIENE LOS INPUTS QUE SE LLEGAN A OCULTAR
      </View>
    </KeyboardAwareScrollView>
  );
}
const styles = StyleSheet.create({
  logo: { width: "100%", height: 150, marginTop: 20, },
  viewForm: { marginRight: 40, marginLeft: 40, },
});
```

**Resultado:** Cuando se abre el teclado este no oculta el campo donde
estamos escribiendo, este hace un scroll para que se pueda ver el campo.

![ejecutar-app](./img/12-evitar-que-teclado-oculte-input.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 51. Añadiendo iconos a los inputs del formulario de registro

<!-- input con icono, agregar icono a input, input icono input -->

A. Modificamos `App\component\Account\RegisterForm.js` para agregar los
iconos dentro del input

- Buscar Iconos: [materialdesignicons.com](http://materialdesignicons.com/)

```js
import React  from 'react';
import { StyleSheet, View, ScrollView } from "react-native";
import { Input, Icon, Button } from "react-native-elements";

export default function RegisterForm() {
  return (
    <View>
      <Input placeholder="Correo electronico" containerStyle={styles.inputForm}
+       rightIcon={
+         <Icon
+           type="material-community"
+           name="at"
+           iconStyle={styles.iconRight} // lo aclarecemos para que se vea como parte del placeholder
+          />
+       }
      />
      <Input placeholder="Contraseña" containerStyle={styles.inputForm} password={true} secureTextEntry={true}
+       rightIcon={
+         <Icon
+           type="material-community"
+           name="eye-outline"
+           iconStyle={styles.iconRight}
+          />
+       }
      />
      <Input placeholder="Confirma contraseña" containerStyle={styles.inputForm} password={true} secureTextEntry={true}
+       rightIcon={
+         <Icon
+           type="material-community"
+           name="eye-outline"
+           iconStyle={styles.iconRight}
+          />
        }
      />
      <Button title="Registrarse" containerStyle={styles.btnContainerRegister} buttonStyle={styles.btnRegister} onPress={ console.log("Registro...") } />
    </View>
  )
}

const styles = StyleSheet.create({
  formContainer: { flex: 1, alignItems: "center", justifyContent: "center", marginTop: 30, },
  inputForm: { width: "100%", marginTop: 20, },
  btnContainerRegister: { marginTop: 20, width: "95%", },
  btnRegister: { backgroundColor: "#00a680", },
+ iconRight: {
+   color: "#c1c1c1",
+ },
});
```

**Resultado:** Agregamos unos icono al lado de los inputs

![13-agregar-icono-a-input](./img/13-agregar-icono-a-input.jpg)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 52. Mostrando y ocultando contraseña con el icono

**Objetivo:** Cuando precionemos sobre el icono del ojo se nos mostrara
la contraseña oculta, para esto necesitaremos utilizar un estado para
activar/desactivar lo que oculta la contraseña.

A. Implementar

- Importamos el `useState`
- Cremos un nuevo estado
- `name={showPass1 ?...`: Con esto podremos cambiar de icono
- `setShowPass1(!showPass1)` Asignar el contrario de lo que actualmetne tiene

```js
# import React, { useState, set } from 'react';
import { StyleSheet, View, ScrollView } from "react-native";
import { Input, Icon, Button } from "react-native-elements";

export default function RegisterForm() {

+ const [showPass1, setShowPass1] = useState(true);
+ const [showPass2, setShowPass2] = useState(true);

  return (
    <View>
      <Input placeholder="Correo electronico" containerStyle={styles.inputForm} rightIcon={
          <Icon type="material-community" name="at" iconStyle={styles.iconRight}  />
        }
      />
      <Input placeholder="Contraseña" containerStyle={styles.inputForm}
#       password={showPass1}
#       secureTextEntry={showPass1}
        rightIcon={
          <Icon type="material-community" iconStyle={styles.iconRight}
#           name={showPass1 ? "eye-outline" : "eye-off-outline" }
#           onPress={() => setShowPass1(!showPass1) }
           />
        }
      />
      <Input placeholder="Confirma contraseña" containerStyle={styles.inputForm}
#       password={showPass2}
#       secureTextEntry={showPass2}
        rightIcon={
          <Icon type="material-community" iconStyle={styles.iconRight}
#           name={showPass2 ? "eye-outline" : "eye-off-outline" }
#           onPress={() => setShowPass2(!showPass2) }
           />
        }
      />
      <Button title="Registrarse" containerStyle={styles.btnContainerRegister} buttonStyle={styles.btnRegister} onPress={ () => console.log("Registro...") } />
    </View>
  )
}

const styles = StyleSheet.create({
  formContainer: { flex: 1, alignItems: "center", justifyContent: "center", marginTop: 30, },
  inputForm: { width: "100%", marginTop: 20, },
  btnContainerRegister: { marginTop: 20, width: "95%", },
  btnRegister: { backgroundColor: "#00a680", },
  iconRight: { color: "#c1c1c1", },
});
```

**Resultado:** Tenemos la opcion de mostrar&ocultar contraseña, e igual
se cambia el icono para dar a entender que hara cualdo lo pulsemos

![14-ocultar-contrasenia](./img/14-ocultar-contrasenia.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 53. Guardando los datos del formulario de registro un estado

**Objetivo:** Para poder enviar la informacion al backend, primero vamos
a guardarlo en un estado, para poder hacer validaciones.

A. Implementamos en: `App\component\Account\RegisterForm.js`

<!--
manejar varios campos en un formulario, multiples campos en formulario,
-->

- `defaultFormData` Creamos una nueva funcion que cree el objeto de
  inicio
- Creamos un nuevo estado `formData` y le metemos el lo de la fincion
  que creamos
- `onChangeFormData`: Esta funcion le meteremos el evento en el cual
  por medio de este sabremos el texto que tiene el input, y el tipo
  para saber el campo que se modifico.
- `setFormData({...formData`: asi podemos agregar nuevos campos sin
  que se ensimen
- `const onSubmit` En esta funcion vamos a hacer la logica de registro

```js
import React, { useState, set } from 'react';
import { StyleSheet, View, } from "react-native";
import { Input, Icon, Button } from "react-native-elements";

+ const IN_EMAIL = "email";
+ const IN_PASS = "pass";
+ const IN_PASSC_ONFIRM = "passConfirm";

+ function defaultFormData() {
+   return {
+     [IN_EMAIL]: "",
+     [IN_PASS]: "",
+     [IN_PASSC_ONFIRM]: "",
+   }
+ }

export default function RegisterForm() {
  const [showPass1, setShowPass1] = useState(true);
  const [showPass2, setShowPass2] = useState(true);
+ const [formData, setFormData] = useState(defaultFormData());

+ const onChangeFormData = (event, field) => {
+   let value = event.nativeEvent.text;
+   setFormData({...formData, [field]: value } );
+ }

+ const onSubmit = (formDataIn) => {
+   console.log(formDataIn);
+ }

  return (
    <View>
      <Input placeholder="Correo electronico" containerStyle={styles.inputForm}
+       onChange={ (e) => { onChangeFormData(e, IN_EMAIL) } }
        rightIcon={ <Icon type="material-community" name="at" iconStyle={styles.iconRight} /> }
      />
      <Input placeholder="Contraseña" containerStyle={styles.inputForm} password={showPass1} secureTextEntry={showPass1}
+       onChange={ (e) => { onChangeFormData(e, IN_PASS) } }
        rightIcon={
          <Icon type="material-community" name={showPass1 ? "eye-outline" : "eye-off-outline" } iconStyle={styles.iconRight} onPress={() => setShowPass1(!showPass1) } />
        }
      />
      <Input placeholder="Confirma contraseña" containerStyle={styles.inputForm} password={showPass2} secureTextEntry={showPass2}
+       onChange={ (e) => { onChangeFormData(e, IN_PASSC_ONFIRM) } }
        rightIcon={
          <Icon type="material-community" name={showPass2 ? "eye-outline" : "eye-off-outline" } iconStyle={styles.iconRight} onPress={() => setShowPass2(!showPass2) } />
        }
      />
      <Button title="Registrarse" containerStyle={styles.btnContainerRegister} buttonStyle={styles.btnRegister}
+       onPress={ () => onSubmit(formData) }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  formContainer: { flex: 1, alignItems: "center", justifyContent: "center", marginTop: 30, },
  inputForm: { width: "100%", marginTop: 20, },
  btnContainerRegister: { marginTop: 20, width: "95%", },
  btnRegister: { backgroundColor: "#00a680", },
  iconRight: { color: "#c1c1c1", },
});
```

**Resultado:** Tenemos la informacion del formulario dentro de un objeto,
y cada vez que de damos el boton registrar nos muestra dicho objeto

```js
Object {
  "email": "Hggjbj",
  "pass": "gbjygc",
  "passConfirm": "hyuuuu",
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 54.55 Validar formulario de registro

**Objetivo:** Para que el usuario no pueda enviar un info. que no sea
valida crearemos las siguientes validaciones.

- El email debe ser valido
- la contraseña debe de tener mas de x xatacteres

A. Instalamos: [Lodash](https://www.npmjs.com/package/lodash)

```r
yarn add lodash
```

B. Creamos: `App\utils\Validations.js`

- Importamos el Lodash

<!-- usar exprecion regular, uso de regexs, usar regex, expreciones regulares -->

```js
import { isEmpty, size } from "lodash";

export function isValidEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function isValidRegisterForm(formDataIn) {
  const {email, pass, passConfirm} = formDataIn;

  let someFieldIsEmpty = (isEmpty(email) || isEmpty(pass) || isEmpty(passConfirm))
  if(someFieldIsEmpty) {
    console.warn("Todos los campos son obligatorios");
  } else {
    if(!isValidEmail(email)) {
      console.warn("El email no es valido");
    } else {
      let arePaswordsHaveCorrectLength = ( size(pass) >= 6 && size(passConfirm) >= 6 );
      if(!arePaswordsHaveCorrectLength) {
        console.warn("Las contrase;as deben tener minimo 6 caracteres");
      } else {
        let arePaswordsEquals = (pass == passConfirm);
        if(!arePaswordsEquals) {
          console.warn("Las contraseñas deben ser iguales");
        } else {
          return true;
        }
      }
    }
  }
  return false;
}
```

B. En `App\component\Account\RegisterForm.js` usamos nuestra nueva
funcion

```js
...
+ import { isValidRegisterForm } from "../../utils/Validations";
...
export default function RegisterForm() {
...
  const onSubmit = (formDataIn) => {
+   let validForm = isValidRegisterForm(formDataIn);
    console.log(formDataIn, "valido:", validForm);
  }
  return ( ... )
}
const styles = StyleSheet.create({ ... });
```

**Resultado:** Podemos hacer validacion de nuestro formulario, ya solo
nos falta mostrar un toast al usuario para que sepa en que esta mal.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 56. Mostrando los mensajes de la validación en un toast

- Mostrar toasts: [react-native-easy-toast](https://www.npmjs.com/package/react-native-easy-toast)

**Objetivo:** ya tenemos la validacion de los datos, ahora nos falta
indicarle al usuario de cualquier error, usando la libreria de aqui arriba.

A. Instalar dependencia: `react-native-easy-toast`

> **Error:** react Animated: `useNativeDriver` was not specified.  
> **Descripcion** NOs sale una advertencia cuando se muestra un toast.  
> **Causa:** Creo temas de la dependencia necesitan actualizarla.  
> **Solucion:** Ignoramos la advertencia con `LogBox.ignoreLogs(["xxx","xxx"])`
> ejemplo abajo

B. Importamos la dependencia en: `App\screens\Account\Register.js`

- `const toastResf = useRef();` De esta forma hacemos que sea compatible
  pasar los hooks
- `RegisterForm toastResf={toastResf}` pasamos el toas a mostrar al formulario
  que es este que decidira cuando mostrarlo.
- `<Toast` Creamos el toast

```js
import React, { useRef } from 'react' import { View, Text, StyleSheet, Image, LogBox } from 'react-native' import RegisterForm from "../../component/Account/RegisterForm"; import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
+ import Toast from "react-native-easy-toast";

+ LogBox.ignoreLogs([
+   'Animated: `useNativeDriver` was not specified. This is a required option and must be explicitly set to `true` or `false`',
+ ])

export default function Register() {

+ const toastResf = useRef();

  return (
    <KeyboardAwareScrollView>
      <Image source={require("../../../assets/img/logo-login.png")} resizeMode="contain" style={styles.logo} />
      <View style={styles.viewForm}>
#       <RegisterForm toastResf={toastResf} />
      </View>
+     <Toast ref={toastResf} position="top" opacity={0.9}  />
    </KeyboardAwareScrollView>
  );
}
const styles = StyleSheet.create({ logo: { width: "100%", height: 150, marginTop: 20, }, viewForm: { marginRight: 40, marginLeft: 40, }, });
```

B. Usamos el Toast en que le pasamos al `App\component\Account\RegisterForm.js`

```js
...
# export default function RegisterForm(props) {
+   const { toastResf } = props;
...
    const onSubmit = (formDataIn) => {
+     let validForm = isValidRegisterForm(formDataIn, toastResf);
+     if(typeof validForm === 'string') {
+       console.log(validForm);
+       toastResf.current.show(validForm, 2000);
+     } else if( validForm === true ) {
+       console.log("OK");
+     }
    }
```

C. Modificamos el `X` para que regrese el mensaje o verdadero si es que
la validacion es correcta

```js
  export function isValidRegisterForm(formDataIn) {
    const {email, pass, passConfirm} = formDataIn;
    let someFieldIsEmpty = (isEmpty(email) || isEmpty(pass) || isEmpty(passConfirm))
    if(someFieldIsEmpty) {
#   return "Todos los campos son obligatorios";
    } else {
      if(!isValidEmail(email)) {
#       return "El email no es valido";
      } else {
        let arePaswordsHaveCorrectLength = ( size(pass) >= 6 && size(passConfirm) >= 6 );
        if(!arePaswordsHaveCorrectLength) {
#         return "Las contraseñas deben tener minimo 6 caracteres";
        } else {
          let arePaswordsEquals = (pass == passConfirm);
          if(!arePaswordsEquals) {
#           return "Las contraseñas deben ser iguales";
          } else {
            return true;
          }
        }
      }
    }
  }
```

**Resultado:** Los mensajes de la validacion las estamos mostrando por
un toast, **Nota:** en este caso tengo el toast en la parte superior.

![15-mostrar-toast-para-validacion](./img/15-mostrar-toast-para-validacion.jpg)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 57. Crear PETICION para crear registrar usuario en BACKEND

**Objetivo:** Crearemos la capa service para manejar lo que es el consumo
del backend, y asi solo llamar una funcion que retornara el usuario o
el error, para lo cual...

A. Creamos `App\Service\RequestUtils.js` para crear las funciones que
para haran las peticiones de forma generica, en este caso para peticiones
get y post.

```js
const TIMEOUT = 5000;


// CUIDADO ESTA FUNCION TIENE ERROR PARA PETICIONES LA DEJAMOS ASI POR QUE
// NOS SIRVE PARA SIMULAR QUE IDENTIFICO QUE EL USUARIO NO ESTA LOGUEADO
export async function get(url) {
  //agregar parametros en url
  //const params = { action: "query", generator: "random", rnlimit: "1" };
  //url = url + "?origin=*";
  //Object.keys(params).forEach(function(key){ url += "&" + key + "=" + params[key]; });

  let myHeaders = new Headers();
  //myHeaders.append("Authorization", "Bearer XXX");
  
  let requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };

  const response = await fetch(url, requestOptions);
  return response;
}


export function post(url, data) {
  return new Promise(async function(resolve, reject){
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify(data);

    const controller = new AbortController()
    const timeoutId = setTimeout(() => { controller.abort(); reject({ cause: "timeout" }) }, TIMEOUT)

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      signal: controller.signal,
    };

    try {
      const response = await fetch(url, requestOptions);
      const data = await response.json();
      clearTimeout(timeoutId)
      if(response.ok) {
        resolve(data);
      } else {
        reject(data);
      }
    } catch (error) {
      reject(error);
    }
  })
}
```

B. Creamos los metodos que hara una tarea especifica en el backend, en
mi caso para

- checar si el usuario esta logueado
- para registrar un usuario.

```js
import { get, post } from "./RequestUtils";

export function isAuth( ) {
  return new Promise(function(resolve, reject) {
    get("http://127.0.0.1:8080/api/users/49")  // CUIDADO HASTA ESTE PUNTO SOLO SIMULA QUE IDENTIFIQUE NO LOGUEADO
    .then(response => {
      if(response.ok) {
        resolve(response.json());
      } else {
        reject(false);
      }
    })
    .catch(error => {
      reject(false);
    });
  });
}

export function registerUser(email, password) {
  return new Promise(function(resolve, reject){
    post("http://192.168.1.76:8080/api/users", {"email":email,"password":password})
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        reject(error)
      });
  })
}
```

C. Consumimos el metodo, en mi caso el de registrar un usuario

```js
  ...
+ import { registerUser } from "../../Service/AuthService";
  ...
  function defaultFormData() {...}
  export default function RegisterForm(props) {
    ....
    const onChangeFormData = (event, field) => {...}
    const onSubmit = (formDataIn) => {
      let validForm = isValidRegisterForm(formDataIn, toastResf);
      if(typeof validForm === 'string') { // formulario no valido
        toastResf.current.show(validForm, 2000);
      } else if( validForm === true ) { // formulario correcto
+       registerUser(formDataIn.email, formDataIn.pass)
+         .then(userCreated => {
+           toastResf.current.show("Usuario Creado", 2000);
+         })
+         .catch(error => {
+           toastResf.current.show(error.cause, 2000);
+         });
      }
    }
    return ( <View>...</View> )
  }
  const styles = StyleSheet.create({...});
```

**Resultado:** Podemos hacer una peticion al servidor y en nuestra
vista/controlador solo llamamos una linea, mas las lineas de que pasa
si esta falla o tiene exito.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 58.59 Agregar navegacion cuando el registro de usuario es exitoso

**Objetivo** Ya estamos registrando al usuario pero no hay una respuesta,
por parte de la aplicacion, por lo que le agregaremos la navegacion al
login cuando el registro del usuario sea exitoso..

A. En `App\component\Account\RegisterForm.js`

- Importamos la navegacion
- Importamos la ventana de Loading
- Creamos un estado `loading` para mostrar y ocultar la carga
- `<Loading...` Lo agregamos para que se muestre

```js
  ...
+ import { useNavigation } from "@react-navigation/native";
+ import Loading from "../../component/Loading";

  ...

  function defaultFormData() {...}

  export default function RegisterForm(props) {
    const { toastResf } = props;

+   const navigation = useNavigation();

    const [..., ...] = useState(...);
+   const [loading, setLoading] = useState(false);

    const onChangeFormData = (event, field) => {...}

    const onSubmit = (formDataIn) => {
      let validForm = isValidRegisterForm(formDataIn, toastResf);
      if(typeof validForm === 'string') {
        toastResf.current.show(validForm, 2000);
      } else if( validForm === true ) {
+       setLoading(true);  // MOSTRAMOS LA PANTALLA DE CARGA
        registerUser(formDataIn.email, formDataIn.pass)
          .then(userCreated => {
+           navigation.navigate("login"); // NAVEGAMOS AL LOGIN
+           setLoading(false);            // OCULTAMOS LA PANTALLA DE CARGA
          })
          .catch(error => {
            toastResf.current.show(error.cause, 2000);
+           setLoading(false);// OCULTAMOS LA PANTALLA DE CARGA
          });
      }
    }

    return (
    <View>
      ...
+     <Loading isVisible={loading} text="Cargando..." />
    </View>
  )
  }
  const styles = StyleSheet.create({...});
```

**Resultado:** Ya mostramos un modal que esta cargando y si es correcto
el inicio entonces navegaremos al login para que este pueda loguearse.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 60. Formulario de login

**Objetivo:** Vamos a crear un formulario parecido al del registro pero
este sera para que el usuario se identifique.

A. Creamos: `App\component\Account\LoginForm.js`

- `showPassword`: para mostrar o ocultar la contraseña
- `formData`: Guardaremos la informacion del formulario aqui
- `loading`: Para mostrar o ocultar el modal de loading
- `setFormData({ ...` Agregar informacion del formulario sin eliminar
  otros datos
- `onPress={onSubmit}` Le agregamos una accion a nuestro formulario.

```js
import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Icon, Button } from "react-native-elements";
import { isEmpty } from "lodash";
import { useNavigation } from "@react-navigation/native";
import { isValidEmail } from "../../utils/Validations";
import Loading from "../Loading";

export default function LoginForm() {
  const [showPassword, setShowPassword] = useState(false);
  const [formData, setFormData] = useState(defaultFormValue());
  const [loading, setLoading] = useState(false);

  const onChange = (e, type) => {
    setFormData({ ...formData, [type]: e.nativeEvent.text });
  };

  const onSubmit = () => {
    if (isEmpty(formData.email) || isEmpty(formData.password)) {
      //toastRef.current.show("Todos los campos son obligatorios");
      console.log("Todos los campos son obligatorios");
    } else if (!isValidEmail(formData.email)) {
      //toastRef.current.show("El email no es correcto");
      console.log("El email no es correcto");
    } else {
      setLoading(true);
      /// CONSULTAMOS EL SERVIDOR
      setTimeout(() => { setLoading(false); }, 1000);
      console.log("Todo Ok mandando datos a servidor");
    }
  };

  return (
    <View style={styles.formContainer}>
      <Input
        placeholder="Correo electronico"
        containerStyle={styles.inputForm}
        onChange={(e) => onChange(e, "email")}
        rightIcon={
          <Icon
            type="material-community"
            name="at"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Contraseña"
        containerStyle={styles.inputForm}
        password={true}
        secureTextEntry={showPassword ? false : true}
        onChange={(e) => onChange(e, "password")}
        rightIcon={
          <Icon
            type="material-community"
            name={showPassword ? "eye-off-outline" : "eye-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setShowPassword(!showPassword)}
          />
        }
      />
      <Button
        title="Iniciar sesión"
        containerStyle={styles.btnContainerLogin}
        buttonStyle={styles.btnLogin}
        onPress={onSubmit}
      />
      <Loading isVisible={loading} text="Iniciando sesión" />
    </View>
  );
}

function defaultFormValue() {
  return {
    email: "",
    password: "",
  };
}

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  inputForm: {
    width: "100%",
    marginTop: 20,
  },
  btnContainerLogin: {
    marginTop: 20,
    width: "95%",
  },
  btnLogin: {
    backgroundColor: "#00a680",
  },
  iconRight: {
    color: "#c1c1c1",
  },
});
```

B. Agregamos el formulario al screen `App\screens\Account\Login.js`

```js
  ...
+ import LoginForm from '../../component/Account/LoginForm';

  export default function Login() {
    return (
      <ScrollView>
        <Image .../>
        <View style={styles.viewContainer}>
+         <LoginForm />
          <CreateAccount />
        </View>
        <Divider style={styles.divider} />
        <Text>Social Login</Text>
      </ScrollView>
    );
  }
  function CreateAccount() {...}
  const styles = StyleSheet.create({...});
```

**Resultado:** Tenemos la vista de nuestro login, mas la funcionalidad
de validar, mostrar pantalla de carga y guardar los datos del formulario
en un objeto.

- Consola

```r
Todos los campos son obligatorios
El email no es correcto
La contraseña debe tener mas de 6 caracteres
Todo Ok mandando datos a servidor Object {
  "email": "Hdfd@umail.com",
  "passwo
}
```

![16-pantalla-de-login.gif](./img/16-pantalla-de-login.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Agregar toast a Login

**Objetivo:** Estamos mostrandi los mensajes por consola, pero debemos
de mostrarselos al usuario, para esto usaremos agregaremos el toast

A. Modificamos `App\screens\Account\Login.js`

- `useRef` para crear la ref para podersela pasar a LoginForm
- `import Toast` Importamos el toast de la libraria que instalamos
- `<Toast ref` Creamos el Toast
- `<LoginForm toast` Le pasamos el toast a LoginForm

```js
# import React, { useRef } from 'react'
  ...
+ import Toast from "react-native-easy-toast";

  export default function Login() {

+   const toastRef = useRef();

    return (
      <ScrollView>
        <Image source={require("../../../assets/img/logo-login.png")} resizeMode="contain" style={styles.logo} />
        <View style={styles.viewContainer}>
#         <LoginForm toastRef={toastRef} />
          <CreateAccount />
        </View>
        <Divider style={styles.divider} />
        <Text>Social Login</Text>
+       <Toast ref={toastRef} position="top" opacity={0.9}  />
      </ScrollView>
    );
  }
  function CreateAccount() {...}
  const styles = StyleSheet.create({...});
```

B. Lo usamos en `App\component\Account\LoginForm.js`

- `function LoginForm(props)`> Traemos las propiedades que le pasamos por Login
- `const showToast` Creamos metodo para mostrarlo por si un dia queremos modificarlos
- `showToast("...")` Mostramos los mensajes que queremos

```js
  import ...
# export default function LoginForm(props) {
+   const { toastRef } = props;
    ...
    const onChange = (e, type) => { };

+   const showToast = (msg) => {
+     toastRef.current.show(msg, 2000);
+   }

    const onSubmit = () => {
      if (isEmpty(formData.email) || isEmpty(formData.password)) {
#       showToast("Todos los campos son obligatorios");
      } else if (size(formData.password) < 6) {
#       showToast("La contraseña debe tener mas de 6 caracteres");
      } else if (!isValidEmail(formData.email)) {
#       showToast("El email no es correcto");
      } else {
        setLoading(true);
        setTimeout(() => { setLoading(false); }, 1000);
        /// CONSULTAMOS EL BACKEND
      }
    };

    return (...);
  }
  function defaultFormValue() {...}
  const styles = StyleSheet.create({...});
```

**Resultado:** Ya mostramos los mensajes de validacion por un toast

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

<!-- no cambiar titulo ya esta referenciado -->
### 65. Realizando el login con el Backend

- fuente: [stackoverflow](https://stackoverflow.com/questions/35325370/post-a-x-www-form-urlencoded-request-from-react-native)

**Objetivo:** Ya estamos listos para hacer login en el backend, por lo
que vamos a agregarlo

A. Modificamos `App\Service\RequestUtils.js` para que acepte peticiones
post de tipo **x-www-form-urlencoded** ya que asi vamos a enviar nuestro
formulario, ya que el back esta implementado con este tipo de peticion
para el login

```js
// AGREGAMOS LA OPCION DE agregar datos planos
# export function post(url, data, headers=null, isRawData=false) {
    return new Promise(async function(resolve, reject){

// AGREGAMOS LA OPCION DE AGREGAR HEADERS DESDE AFUERA O DEJAR EL DEFAULT
#     let myHeaders = null;
#     if(headers === null) {
#       myHeaders = new Headers();
#       myHeaders.append("Content-Type", "application/json");
#     } else {
#       myHeaders = new Headers(headers);
#     }

#     let raw = null;
#     if(data instanceof URLSearchParams || isRawData ) {
#       raw = data;
#     } else {
#       raw = JSON.stringify(data);
#     }

      const controller = new AbortController()
      const timeoutId = setTimeout(() => { controller.abort(); reject({ cause: "timeout" }) }, TIMEOUT)

      let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        signal: controller.signal,
      };

      try {
        const response = await fetch(url, requestOptions);
        const data = await response.json();
        clearTimeout(timeoutId)
        if(response.ok) {
          resolve(data);
        } else {
          reject(data);
        }
      } catch (error) {
        reject(error);
      }
    })
  }
```

B. Creamos nueva funcion para hacer el login

```js
export function doLogin(email, password) {
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": "Basic bml0ZXRsYXhfZnJvbnQ6MTIzNDU=",
  };

  let details = {
    'username': email,
    'password': password,
    'grant_type': 'password'
  };

  let data = [];
  for (let property in details) {
    let encodedKey = encodeURIComponent(property);
    let encodedValue = encodeURIComponent(details[property]);
    data.push(encodedKey + "=" + encodedValue);
  }
  data = data.join("&");

  return new Promise(function(resolve, reject) {
    post("http://192.168.1.76:8080/oauth/token", data, headers, true)
      .then(response => {
        resolve(response);
      })
      //.then(result => console.log("Result: ", result))
      .catch(error => {
        reject(error)
      });
  })
}
```

C. Agregamos la funcion de login a nuestro Formulario

```js
  import { doLogin } from "../../Service/AuthService";
  export default function LoginForm(props) {
    ...
    const onSubmit = () => {
      if (isEmpty(formData.email) || isEmpty(formData.password)) { showToast("Todos los campos son obligatorios");
      } else if (size(formData.password) < 6) { showToast("La contraseña debe tener mas de 6 caracteres");
      } else if (!isValidEmail(formData.email)) { showToast("El email no es correcto");
      } else {
        setLoading(true);
+       doLogin(formData.email, formData.password)
+         .then(response => {
+           console.log(response);
+           setLoading(false);
+         })
+         .catch((error) => {
+           console.log("Error", error);
+           setLoading(false);
+         })
+       ;
      }
    };
    return (...);
  }
  function defaultFormValue() { ... }
  const styles = StyleSheet.create({...});
```

**Resultado:** Estamos haciendo la peticion al servidor para que nos
retorne el token de autenticacion o si ubo algun fallo, lo que nos
faltaria, es guardar este token y darle feddback al usuario

- consola: ambas respuestas que llegan del servidor.

```r
Object {
  "error": "credenciales incorrectas",
}

Object {
  "access_token": "xxxx",
  "refresh_token": "xx",
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 65. Guardar token y usarlo para saber si esta logueado el usuario

- Fuente: [react-native-async-storage](https://react-native-async-storage.github.io/async-storage/docs/install)

**Objetivo** Para poderle dar feedback de cuando se logue este cambie a
la pantalla, o muestre un letrero indicando algun error.

A. Instalamos dependencia para guardar/leer info localmente

```r
yarn add @react-native-async-storage/async-storage
```

B. Creamos `App\utils\LocalDataUtils.js` para manejar el guardado y
lectura de informacion local del telefono de forma general.

```js
import AsyncStorage from '@react-native-async-storage/async-storage';

export class Storage {

  static async storeString(key, value) {
    try {
      await AsyncStorage.setItem(key, value)
      return true;
    } catch (e) {
      console.warn(e);
      return false;
    }
  }
  
  static async storeObject(key, object) {
    try {
      const jsonValue = JSON.stringify(object)
      await AsyncStorage.setItem(key, jsonValue)
      return true;
    } catch (e) {
      console.warn(e);
      return false;
    }
  }
  
  
  static async getString(key) {
    try {
      const value = await AsyncStorage.getItem(key)
      if(value !== null) {
        return value;
      }
    } catch(e) {
      console.warn(e);
      return null;
    }
  }
  
  static async getObject() {
    try {
      const jsonValue = await AsyncStorage.getItem(key)
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.warn(e);
      return null;
    }
  }
  static async remove(key) {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    } catch (e) {
      console.warn(e);
      return false;
    }
  }

}
```

C. Creamos `App\Service\StorageService.js` Para manejar cosas mas
especificas del Storage

```js
import { Storage } from "./../utils/LocalDataUtils";
const TOKEN = "token";
export class StorageService {

  static saveToken(token) {
    Storage.storeString(TOKEN, token);
  }

  static getToken() {
    return new Promise(function (resolve, reject) {
      let token = Storage.getString(TOKEN);
      if(token != null) {
        resolve(token);
      } else {
        reject("error al recuperar el toquen")
      }
    });
  }

  static deleteToken() {
    Storage.remove(TOKEN);
  }
}
```

D. Modificamos en `App\utils\RequestUtils.js` el metodo de get para
que pueda enviar el token

```js
export async function get(url, token) {
  return new Promise(function(resolve, reject) {
    //agregar parametros en url
    //const params = { action: "query", generator: "random", rnlimit: "1" };
    //url = url + "?origin=*";
    //Object.keys(params).forEach(function(key){ url += "&" + key + "=" + params[key]; });

+   var myHeaders = new Headers();
+   if(token != null && !isEmpty(token)) {
+     myHeaders.append("Authorization", "Bearer "+token);
+   }

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

+   // CUIDADO: manejar bien los promises por que si no causa errores
    // raros como el de no regresar body en la peticion solo header
    // no se por que ingados luego ase eso
#   fetch(url, requestOptions)
#     .then(response => {
#       resolve(response.text());
#     })
#     .catch(error => {
#       reject(error);
#     });
  })  
}
```

F. Modificamos en `App\Service\AuthService.js` para recuperar el token
guardado y enviarlo por la peticion get, al servicio que nos sirva para
saber si el usuario esta logueado o no.

- tambien agregamos que se guarde el token cuando el login sea exitoso

```js
import { get, post } from "./../utils/RequestUtils";
import { StorageService } from "./StorageService";

export function isAuth() {
  return new Promise(async function(resolve, reject) {
+   let token = await StorageService.getToken();
    let url = "http://192.168.1.76:8080/api/users/49" // ID TEMPORAL LUEGO SE LO VAMOS A CAMBIAR

+   if(isEmpty(token)) {//Al recuperar token no se recupero lo que significa que es la primera vez p lo eliminamos en algun momento
+     return false;
+   }

#   get(url, token)
      .then((resp) => {
        resolve(resp);
      })
      .catch((error)=>{
        StorageService.deleteToken(); // significa que no nos sirvico el token
        reject(error);
      })
    ;
  });
}

export function doLogin(email, password) {
  const headers = { "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Basic bml0ZXRsYXhfZnJvbnQ6MTIzNDU=", };
  let details = { 'username': email, 'password': password, 'grant_type': 'password' };
  let data = [];
  for (let property in details) { let encodedKey = encodeURIComponent(property); let encodedValue = encodeURIComponent(details[property]); data.push(encodedKey + "=" + encodedValue); }
  data = data.join("&");
  return new Promise(function(resolve, reject){
    post("http://192.168.1.76:8080/oauth/token", data, headers, true)
      .then(response => {
+       StorageService.saveToken(response.access_token);
        resolve(response);
      })
      .catch(error => {
        console.log(error);
        if(isEqual(error.error, "invalid_grant") ) {
          reject("Credenciales incorrectas")
        }
        reject("Error al hacer login")
      });
  })
}
```

F. Modificamos en `App\component\Account\LoginForm.js` el metodo `onSubmit`
para que haga la navegacion si es que la peticion y la recuperacion del
token guardado en el dispositivo son correctos.

```js
const onSubmit = () => {
    if (isEmpty(formData.email) || isEmpty(formData.password)) {
      showToast("Todos los campos son obligatorios");
    } else if (size(formData.password) < 6) {
      showToast("La contraseña debe tener mas de 6 caracteres");
    } else if (!isValidEmail(formData.email)) {
      showToast("El email no es correcto");
    } else {
      setLoading(true);
      doLogin(formData.email, formData.password)
        .then(response => {
          StorageService.getToken()
            .then(token => {
              navigation.navigate('account');
            })
            .catch(error => {
              showToast("Error al guardar sesion");
            })
          ;
          setLoading(false);
        })
        .catch((error) => {
          showToast(error);
          setLoading(false);
        })
      ;
    }
  };
```

G. Lo mismo pasa con el `App\screens\Account\Account.js` donde debemos
de indicar que pantalla mostrar modificando la bandera login

```js
export default function Account() {

  const [login, setlogin] = useState(null);

  useEffect(() => {
    isAuth()
      .then(result => {
        console.log("Result: ", result);
#       setlogin(true);
      })
      .catch(error => {
        console.log('error', error);
#       setlogin(false);
      } );
  }, [])

  if(login === null) { return <Loading isVisible={true} text="Cargando..." /> }

  return (login === true) ? <AccountLogged /> : <AccountGuess />;
}
```

**Resultado:** Si nos logueamos y lueego entramos y salimos de la
aplicacion, podemos ver que se carga la pantalla de Usuario, para solucionar
esto ver el resultado de abajo.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### IsFocused: Ejecutar codigo cuando entramos o salimos de screen (navegamos)

**Problema:** En el resultado anterior tenemos una UX pobre ya que debemos
de entrar y salir de la aplicacion para notar los cambios en la pantalla,
esto debido a que cuando entramos a la pantalla solo se ejecuta el
`useEffect` que tenemos en `App\screens\Account\Account.js` una sola vez.

**Solucion**: Necesitamos algo que se ejecute cada vez que entramos a
la panatalla de cuenta para ssaber si el usuario sigue logueado o no.

A. Modificamos `App\screens\Account\Account.js` para agregarle la bandera
`useIsFocused` que nos indicara si el usuario entra o sale de la pantalla

```js
  import React, { useState, useEffect } from 'react' import AccountGuess from "./AccountGuess"; import AccountLogged from "./AccountLogged"; import { isAuth } from "../../Service/AuthService"; import Loading from "../../component/Loading";
+ import { useIsFocused } from "@react-navigation/native";

  export default function Account() {
+   const isFocused = useIsFocused();
    const [login, setlogin] = useState(null);

    useEffect(() => {
+     if(isFocused) {
        isAuth()
          .then(result => {
            setlogin(true);
          })
          .catch(error => {
            setlogin(false);
          })
        ;
+     }
#   }, [isFocused])
    if(login === null) { return <Loading isVisible={true} text="Cargando..." /> }
    return (login === true) ? <AccountLogged /> : <AccountGuess />;
  }

```

**Resultado:** Ahora ca vez que entramos a la pantalla de Account se
ejecutara el metodo que checara si el usuario esta logueado o no.

![17-login-y-check-si-esta-logueado-en-navegacion](./img/17-login-y-check-si-esta-logueado-en-navegacion.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Cerrar Session y extraer informacion del token

<!-- sacar info del token, scar informacion del token, token info-->

**Objetivo:** Vamos a cerrar sesion eliminando solamente el token, ya
que nuestro navegador es stateles no requiere que le evisemos, o no hay
forma de indicarle a este que ya no acepte el token actual.

A. Extraer info del token

A.1 En `App\Service\AuthService.js` Creamos el metodo

```js
+ export function getIdFormToken(token) {
+   let tokenDescrypt = decode(token.split(".")[1]);
+   let infoToken = JSON.parse(tokenDescrypt);
+   return infoToken.user_name;
+ }

  export function isAuth() {
    return new Promise(async function(resolve, reject) {
      let token = await StorageService.getToken();

      if(isEmpty(token)) { reject('Sin token'); return ; }

      let idUser = getIdFormToken(token);
      let url = "http://192.168.1.76:8080/api/users/"+idUser
      get(url, token)
        .then((resp) => { resolve(resp); })
        .catch((error)=>{
+         StorageService.deleteToken();
          reject(error);
        })
      ;
    });
  }
```

B. Cerrar sesion

B.1. En `App\screens\Account\AccountLogged.js` agregamos el metodo que
elimina el token en nuestra accion de precionar el token

```js
+ import { StorageService } from '../../Service/StorageService';
  import { useNavigation } from "@react-navigation/native";

  export default function AccountLogged() {

    const navigation = useNavigation();

+   const onClickLogout = () => {
+     StorageService.deleteToken();
+     navigation.navigate('login');
+   }

    return (
      <View style={styles.btnEntrarView} >
        <Text>AccountLogged</Text>
        <Button title='Cerrar session' buttonStyle={styles.btnEntrar} containerStyle={styles.btnEntrarCo}
+         onPress={() => onClickLogout() }
        />
      </View>
    );
  }
```

**Resultado:** Podemos hacer logout de nuestra cuenta y tambien extraimos
informacion del token para mandar a traer el usuario que nos interesa.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

----

----

<!--
. separacion capitulo, divicion capitulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

## Sección 8:Panel de usuario

**Objetivo:** Ya tenemos el registro y login del usuario ahora vamos a
hacer mostrar toda su infirmacion en una screen

### Crear Usuario Info mostrando datos de usuario de la API

A. Pasamos el usuario que tenemos en `App\screens\Account\Account.js`
a `AccountLogged.js`

- `const [user, setUser]...` Creamos un estado para enviarselo
- `setUser(result);` Cuando carge el usuario le pasamos el usuario
- `<AccountLogged user={user}` Se lo pasamos como propiedad

```js
  import React, { useState, useEffect } from 'react'
  import AccountGuess from "./AccountGuess";
  import AccountLogged from "./AccountLogged";
  import { isAuth } from "../../Service/AuthService";
  import Loading from "../../component/Loading";
  import { useIsFocused } from "@react-navigation/native";

  export default function Account() {
    const isFocused = useIsFocused();

    const [login, setlogin] = useState(null);

+   const [user, setUser] = useState(null);

    useEffect(() => {
      console.log("isFocused", isFocused);
      if(isFocused) {
        isAuth()
        .then(result => { 
          console.log("Result: ", result);
+         setUser(result);
          setlogin(true);
        })
        .catch(error => {
          console.log('error en Account', error);
          setlogin(false);
        } );
      }
    }, [isFocused])

    if(login === null) { return <Loading isVisible={true} text="Cargando..." /> }

#   return (login === true) ? <AccountLogged user={user} /> : <AccountGuess />;
  }

```

B. Creamos Estructura del screen del `App\screens\Account\AccountLogged.js`
para prepararnos y agregar el user info que estara en un componente.

- `const { user } = props;` Recibimos la info de usuario desde Account
- `const toastResf = useRef();` Nos preparamos para usar Toast
- `<UserInfo user={user} />` Es el componente que crearemos para mostar
  los datos del usuario

```js
# import React, { useRef, useState } from 'react'
  import { StyleSheet, View, Text } from 'react-native'
  import { Button } from 'react-native-elements'
  import { StorageService } from '../../Service/StorageService';
  import { useNavigation } from "@react-navigation/native";
+ import Toast from "react-native-easy-toast";
+ import { Loading } from "../../component/Loading";
+ import UserInfo from '../../component/Account/UserInfo';

# export default function AccountLogged(props) {
+   const { user } = props;

    const navigation = useNavigation();
+   const toastResf = useRef();

+   // lo usaremos para mostrar u ocultar la ventana de cargando
+   const [isLoading, setIsLoading] = useState(true);
+   // la ventana de carga indicara que esta cargando ej. cambiamdo pass, subiendo foto, etc
+   const [textLoading, settextLoading] = useState("")

    const onClickLogout = () => {
      StorageService.deleteToken();
      console.log('Logout: Token eliminado');
      navigation.navigate('login');
    }

    return (
      <View style={styles.btnEntrarView} >
#       <UserInfo user={user} />
#       <Text>Option User</Text>
        <Button title='Cerrar session'
          onPress={() => onClickLogout() }
          buttonStyle={styles.btnEntrar}
          containerStyle={styles.btnEntrarCo}
+         titleStyle={styles.btnLogoutTitle}
        />
+       <Toast ref={toastResf} position="top" opacity={0.9}  />
      </View>
    );
  }


  const styles = StyleSheet.create({
    btnEntrarView: { flex: 1, alignItems: "center" },
    btnEntrar: { backgroundColor: "#00a680", },
    btnEntrarCo: { margin: "auto", width: "70%", },
+   viewUserInfo: {
+     minHeight: "100%",
+     backgroundColor: '#f2f2f2',
+   },
+   btnLogoutTitle: { // cambiar estilo a texto de boton, boton texto
+     color: '#f2f2f2',
+   }
  });
```

B. Creamos componente `App\component\Account\UserInfo.js`

```js
import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Avatar } from 'react-native-elements';

export default function UserInfo(props) {
  const { user } = props;
  const { email, name, img } = user

  return (
    <View style={styles.view}>
      <Avatar rounded size="large" showAccessory={true} containerStyle={styles.avatar}
        source={ (img != null) ? img : require("../../../assets/img/avatar-default.jpg") }
      />
      <View>
        <Text style={styles.txtName}>{(name !=null) ? name : "Sin nombre"}</Text>
        <Text>{email}</Text>
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection:"row", // para que se ponga uno al lado del otro
    padding: 30,
    paddingBottom: 30,
  },
  avatar: {
    marginRight: 20,
    backgroundColor: "#e1e1e1",
  },
  txtName: {
    fontWeight: "bold",
    marginBottom: 5
  }
});
```

**Resultado:** Tenemos la pantalla lista para mostrar los datos del
usuario, solo nos falta mostrar la imagen que nos llegue por el Backend

![ejecutar-app](./img/18-user-info-sin-imagen.jpg)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 70. Instalando Packages para poder acceder a la galería del teléfono

**Objetivo:** Nececitamos poder acceder a la galeria y para eso usaremos
dos plugins

- [permissions](https://docs.expo.io/versions/latest/sdk/permissions/)
  nos servira para poder pedir permiso para acceder a losrecursos del
  telefono.

- [imagepicker](https://docs.expo.io/versions/latest/sdk/imagepicker/)
  nos servira para mostrar la UI para poder seleccionar una imagen.

- Nota: Las versiones deambos plugins estan relacionados, por lo que
  en mi caso me pidio que tuviera esta convinacion de versiones.

A. instalar permissions: `yarn add expo-permissions@~9.1.1`

B. Instalar imagepicker: `yarn add expo-image-picker@~9.3.0`

**Resultado:** Tenemos instalados los plugins que necesitaresmos para
poder subir una imagen a nuestro back

```json
"dependencies": {
    ...
    "expo-image-picker": "~9.1.1",
    "expo-permissions": "~9.3.0",
    ...
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 71. Abriendo la galeria de imágenes

**CUIDADO:** Tenemos dos tipos de permisos los de nuestra aplicacion
"expo" que es el contenedor web donde probamos las aplicaciones, otra
es nuestra aplicacion cuando tengamos la .apk generada, esta equivalencia
podemos ver en.

**CUIDADO:** Tuve el error de no poner bien la IP puse la localhost de
en lugar del la red LAN, por lo que me atrazo algo.

A. Vamos a `app.json` para agregar los permisos en android

```json
  "android": {
    "adaptiveIcon": {...},
+   "permissions": [
+     "CAMERA_ROLL"
+     "READ_EXTERNAL_STORAGE",
+     "WRITE_EXTERNAL_STORAGE"
+   ]
  },
```

B. Creamos metodo que subira nuestra imagen

```js
export function uploadUserPicture(file) {
  return new Promise(async function(resolve, reject){
    let token = await StorageService.getToken();

    if(isEmpty(token)) {
      reject('Sin token');
      return ;
    }
    let idUser = getIdFormToken(token);

    const formData = new FormData();
    formData.append('file', file);

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer "+token);

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: formData
    };

    const url = "http://192.168.1.76:8080/api/users/"+idUser+"/image";

    fetch(url, requestOptions)
      .then(async (response) => {
        const data = await response.json();
        if(response.ok) {
          resolve(data);
        } else {
          reject(data);
        }
        resolve(response)
      })
      .catch(error => {
        reject(error);
      })
    ;
  })//fin return promesa
}
```

C. En `App\component\Account\UserInfo.js` agregaremos el evento de cuando
el usuario precione sobre el boton del avatar

- `const changeAvatar` funcion que se ejecutara al precionar el lapiz
  del avatar
- `onAccessoryPress={` Agregamos el metodo al boton
- `Permissions.askAsync(` Si no tiene los permisos los pedira
- `ImagePicker.launchIm` Mostramos la galeria para que el usr escoja una
  imagen
- `const newImageUri = "file:///" + result.uri.split(` CUIDADO, si no
  esta bien esta URI la peticion no servira
- `uploadUserPicture(file)`: Mandamos la imagen al backend para que
  se guarde.

```js
  import React from 'react';
  import { View, StyleSheet, Text } from 'react-native';
  import { Avatar } from 'react-native-elements';
+ import * as Permissions from "expo-permissions";
+ import * as ImagePicker from "expo-image-picker";
+ import { uploadUserPicture } from "../../Service/AuthService";
+ import mime from "mime";

  export default function UserInfo(props) {
#   const { user, toastResf } = props; // agregamos el toastResf que nos llega de AccountLogged
#   const { email, name, img } = user; // agregamos la imagen del usuario
    
+   const changeAvatar = async () => {
+     const resultPermision = await Permissions.askAsync(Permissions.CAMERA_ROLL);
+     const resultPermisionCamera = resultPermision.permissions.cameraRoll.status;
+     if(resultPermisionCamera === 'denied') {
+       toastResf.current.show('Es necesario aceptar los permisos de la galeria');
+     } else {
+       const result = await ImagePicker.launchImageLibraryAsync({
+         allowsEditing: true,
+         aspect: [4,3],
+         //base64: true por si queremos obtener el base64
+       });
+       if(result.cancelled) {
+         toastResf.current.show('Seleccion de imagen cancelada');
+       } else {
+         // Subir imagen
+         const newImageUri = "file:///" + result.uri.split("file:/").join(""); // NOTA: Dejar como viene la uri para ios
+         const nameX = newImageUri.split("/").pop();
+         const typeX = mime.getType(newImageUri);
+         const file = { uri : newImageUri, type: typeX, name: nameX }

+         uploadUserPicture(file)
+           .then(resp => {
+             toastResf.current.show('Imagen guardada:' + resp);
+           })
+           .catch(error => {
+             console.log("ErrorSubIma:", error);
+           });
+         //---------------------------
+      }       
+     }
+   }

    return (
      <View style={styles.view}>
        <Avatar rounded size="large" showAccessory={true} containerStyle={styles.avatar} source={ (img != null) ? img : require("../../../assets/img/avatar-default.jpg") }
+         onAccessoryPress={ changeAvatar }
        />
        <View> <Text style={styles.txtName}>{(name !=null) ? name : "Sin nombre"}</Text> <Text>{email}</Text> </View>
      </View>
    );
  }
  const styles = StyleSheet.create({...);
```

**Resultado:** Podemos enviar la peticion para guardar la imagen, pero
aun no la podemos ver en el componente de avatar.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 76. Actualizando la imagen sin tener que recargar la app

**IMPORTANTE:** Si hacemos una peticion usando la misma url, al parecer
react usara el cache y la imagen no se actualizara, por lo que cambiaremos
la url con un parametro del nombre de la imagen lo cual cambia cada vez
que cambiamos de imagen, **es decir** cargaremos la imagen solo cuando
cambie la imagen, si no cambia se cargara el cache.

- `const [imageR,` Con este estado se recargara la url

```js
...
  export default function UserInfo(props) {
    const { email, name, image, idUser } = user;

+   const [imageR, setImageR] = useState(image);
    
    const changeAvatar = async () => {
      ...
        if(result.cancelled) { toastResf.current.show('Seleccion de imagen cancelada'); } else {
          // Subir imagen ...
          uploadUserPicture(file)
            .then(uploadResp => {
              setImageR(uploadResp.image);
            })
            .catch(error => {
              console.log("ErrorSubIma:", error);
            });
          //---------------------------
        }
        
      }
    }

    return (
      <View style={styles.view}><Avatar rounded size="large" showAccessory={true} containerStyle={styles.avatar}
#         source={ (imageR != null) ? {uri: "http://192.168.1.76:8080/api/users/"+idUser+"/image?a="+imageR } : require("../../../assets/img/avatar-default.jpg") }
...
```

**Resultado:** Podemos escojer una imagen, esta se subira al servidor y
al terminar cargara la imagen del avatar del servidor, cuidando el hecho
de usar el cache correctamente.

![19-escojer-avatar-recarga-reactiva.gif](./img/19-escojer-avatar-recarga-reactiva.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 77. Lista de opciones disponibles en la cuenta del usuario

<!-- agregar evento a una lista de botones, crear botones y agregarle evento cada uno,
añadir metodo a cada boton, agregar metodo a cada boton -->

**Objetivo:** En la pantalla de usuario logueado (donde mostramos la
info del usuario) podremos hacer diferentes acciones, como cambio de
pass, nombre etc.

> **Error:** el ancho no es correcto del listItem.  
> **Descripcion:** Al poner el listitem el ancho no era el correcto
> mostrandolo de una manera erronea.  
> **Causa:** El componente padre estaba mal, por alguna razon no estaba
> tomando todo el ancho.  
> **Solucion**: Hacer a mano mas ancho el componente padre, en mi caso
> con `<View style={styles.box}>`.  

A. Creamos `App\component\Account\AccountOptions.js` donde mostraremos
los diferentes botones

- `generateOptions(onClickListItem)` Creamos array donde ponemos la info
  de cada boton
- `const onClickListItem = (name) =>` El evento que cada boton de la lista
  ejecutara y cambiara por medio de su parametro que entre
- `const menuOptions = generateOptions(onClickListItem);` le pasamos
  el metodo, el cual funcionara como tipo en java, para luego perzonalizar
  cada uno de estos.
- `menuOptions.map((btn, i) => (`: Iteramos el array para crear los
  botones
- `onPress={btn.onPress}`: Personalizamos lo que se ejecutara en cada boton.

```js
import { map } from 'lodash';
import React, { useState } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { ListItem, Icon } from 'react-native-elements';

function generateOptions(onClickListItem) {
  return [
    {
      title: "Cambiar nombre y titulo",
      iconType: "material-community",
      iconLeft: "account-circle",
      iconLeftColor: "#ccc",
      onPress: () => onClickListItem("1"),
    }, {
      title: "Cambiar email",
      iconType: "material-community",
      iconLeft: "at",
      iconLeftColor: "#ccc",
      onPress: () => onClickListItem("2"),
    }, {
      title: "Cambiar contraseña",
      iconType: "material-community",
      iconLeft: "lock-reset",
      iconLeftColor: "#ccc",
      onPress: () => onClickListItem("3"),
    }
  ]
}

export default function AccountOptions(props) {
  const { user, toastResf } = props;
  
  // CUIDADO: Debemos de estar usando este, ya que funciona como una especie de tipo en java
  const onClickListItem = (name) => {
    console.log(name); // mas adelante implementaremos
    // por medio del name sabremos que boton se preciono y que ejecutar
  }

  const menuOptions = generateOptions(onClickListItem);

  // Chevron, tiene las mismas propiedades de icono
  return (
    <View style={styles.box}>
      {
        menuOptions.map((btn, i) => (
          <ListItem key={i} containerStyle={styles.listItem} onPress={btn.onPress}>
            <Icon name={btn.iconLeft} type={btn.iconType} color={btn.iconLeftColor} />
              <ListItem.Content>
                <ListItem.Title>{btn.title}</ListItem.Title>
              </ListItem.Content>
              <ListItem.Chevron color="#000" />
          </ListItem>
        ))
      }
    </View>
  );
}

const styles = StyleSheet.create({
  box: {
    width: '100%',
    marginBottom: 20,
  },
  listItem: {
    borderBottomColor: "#e3e3e3",
    borderBottomWidth: 1
  }
});
```

B. Agregamos el componente en `App\screens\Account\AccountLogged.js`
para mostrar el menu de botones que creamos.

```js
  export default function AccountLogged(props) {
    ...
    return (
      <View style={styles.btnEntrarView}>
        ...
+       <AccountOptions user={user} toastResf={toastResf} />
```

**Resultado** Mostramos el boton y si precionamos un boton, cada uno de
estos imprimira un valor diferente.

```r
consola
1
2
3
```

![20-iterar-para-crear-botones-con-su-metodo](./img/20-iterar-para-crear-botones-con-su-metodo.png)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 79. Componente Modal

**Objetivo:** Queremos que cuando precionemos un boton del menu que
apenas arriba creamos, se habra un modal con distinto contenido/formulario
para poder interactuar con el, y asi poder hacer una cosa distinta.

A. Creamos `App\component\Modal.js`

```js
import React from "react";
import { StyleSheet, View, Text, ActivityIndicator } from "react-native";
import { Overlay } from "react-native-elements";

export default function Modal(props) {
  const { 
    isVisible, // bandera para mostrarlo o ocultarlo
    setIsVisible, // para poderlo ocultar desde este mismo componente
    children // el contenido que mostrara el modal
  } = props;

  const closeModal = () => setIsVisible(false);

  return (
    <Overlay
      isVisible={isVisible}
      onBackdropPress={closeModal} // cuando el usuario precione fuera del modal o le de atras
      windowBackgroundColor="rgba(0, 0, 0, 0.5)"
      overlayBackgroundColor="transparent"
      overlayStyle={styles.overlay}
    >
      {children} // contenido que esta dentro de nuestro componente cuando lo usemos
    </Overlay>
  );
}

const styles = StyleSheet.create({
  overlay: {
    height: 'auto', // ocupara solo el alto necesario
    width: '85%',
    backgroundColor: "#fff",
    borderColor: "#00a680",
    borderWidth: 2,
    borderRadius: 10,
  },
});
```

B. Agregamos el modal a nuestro menu de opciones

- `import Modal` Traemos el modal que creamos nosotros
- `const [showModal, setShowModal] = useState(false)`: Creamos estado
  para abrir y cerrar el modal
- `const onClickListItem = (name) => { setShowModal(true);` Sea cual
  sea el boton que precionemos abrira el modal, luego le cambiaremos
  el contenido que este mostrara
- `<Modal isVis` usamos el modal

```js
  import { map } from 'lodash';
  import React, { useState } from 'react';
  import { View, StyleSheet, Text } from 'react-native';
  import { ListItem, Icon } from 'react-native-elements';
+ import Modal from "../Modal";

  function generateOptions(onClickListItem) { return [ ... ] }

  export default function AccountOptions(props) {
    const { user, toastResf } = props;
    
+   const [showModal, setShowModal] = useState(false)
    
    // CUIDADO: Debemos de estar usando este, ya que funciona como una especie de tipo en java
    const onClickListItem = (name) => {
      console.log(name);
+     setShowModal(true);
    }

    const menuOptions = generateOptions(onClickListItem);

    // Chevron, tiene las mismas propiedades de icono
    return (
      <View style={styles.box}>
        {
          menuOptions.map((btn, i) => (
            <ListItem key={i} containerStyle={styles.listItem} onPress={btn.onPress}>
              <Icon name={btn.iconLeft} type={btn.iconType} color={btn.iconLeftColor} />
                <ListItem.Content>
                  <ListItem.Title>{btn.title}</ListItem.Title>
                </ListItem.Content>
                <ListItem.Chevron color="#000" />
            </ListItem>
          ))
        }
+       <Modal isVisible={showModal} setIsVisible={setShowModal} >
+         <Text>Hola dezde modal</Text>
+       </Modal>
      </View>
    );
  }

  const styles = StyleSheet.create({
    box: { width: '100%', marginBottom: 20, },
+   listItem: {
+     borderBottomColor: "#e3e3e3",
+     borderBottomWidth: 1
+   }
  });

```

**Resultado:** Al darle click a un boton se mostrara un modal y podremos
cerrarlo cuando precionemos fuera de este, **falta** agregarle contenido
perzonalizado, mas abajo vemos como

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 80. Cambiando el contenido del modal dependiendo el menú haga click el usuario

A. En `App\component\Account\AccountOptions.js`

> **Error:** Failed prop type: The prop `children` is marked as required in `Overlay`, but its value is `null`.  
> **Descripcion:** Al querer mostrar pasar un componente como children
> dentreo de una variable (en este caso `modalContent`) marca este error.  
> **Causa:** El componente se inicializa como nulo y le estamos passando
> un nulo lo cual eso causa el error.  
> **Soluccion:** Usar un condicional que solo si este tiene valor este
> se muestre, en caso contrario que no lo muestre, en mi caso con un
> `{AMostrar && ( <AMostrar /> ) }`.

- `const [modalContent,` Creamos estado el cual contendra el contenido
  a mostrar dentro del model
- `{modalContent && (` Indicamos qie si existe el modal lo renderizamos
- `switch (name) {` Asignamos el contenido segun el boton que se precione

```js
  export default function AccountOptions(props) {
    const { user, toastResf } = props; const [showModal, setShowModal] = useState(false)
    
+   const [modalContent, setModalContent] = useState(null);
    
    // CUIDADO: Debemos de estar usando este, ya que funciona como una especie de tipo en java
    const onClickListItem = (name) => {
+     switch (name) {
+       case "basic":
+         setModalContent(
+           <Text>Cambiar nombre y titulo</Text>
+         );
+       break;
+       case "email":
+         setModalContent(
+           <Text>Cambiar email</Text>
+         );
+       break;
+       case "pass":
+         setModalContent(
+           <Text>Cambiar contraseña</Text>
+         );
+       break;     
+       default:
+         setShowModal(false);  
+         setModalContent(null);
+       break;
+     }
+     setShowModal(true);
    }

    const menuOptions = generateOptions(onClickListItem);

    // Chevron, tiene las mismas propiedades de icono
    return (
      <View style={styles.box}>
        { menuOptions.map((btn, i) => (<ListItem key={i} containerStyle={styles.listItem} onPress={btn.onPress}>...</ListItem>))}
+       {modalContent && (
          <Modal isVisible={showModal} setIsVisible={setShowModal} >
#           {modalContent}
+         </Modal>
        )}
      </View>
    );
  }
```

**Resultado:** Por cada boton que precionemos se mostrara un contenido
diferente, **falta** que este contenido sea un formulario.

![21-mostrar-modal-con-contenido-diferente-por-cada-boton](./img/21-mostrar-modal-con-contenido-diferente-por-cada-boton.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 81. Creando formulario para actualizar nombre de usuario

**Objetivo:** Este se mostrara al pulsar el la opcion de actualziar
nombre, y cuando actualizemos podremos ver un loading y cuando termine
de actualizar, se cerrara el modal y se actualizara el nombre.

<!-- actualizar variable en un componente superior, actualizar variable en un componente arriba-->

A. Para actualizar el nombre en la pantalla vamos a hacer uso de un
state y el useEffect en `App\screens\Account\Account.js`

- Le pasamos `setReloadUser` a AccountLogged

```js
  export default function Account() {
    ...
+   const [reloadUser, setReloadUser] = useState(false);

    useEffect(() => {
      if(isFocused) {
        isAuth().then(result => { console.log("Result: ", result); setUser(result); setlogin(true); })
        .catch(error => { console.log('error en Account', error); setlogin(false); } );
      }
+     setReloadUser(false);
#   }, [isFocused, reloadUser])
    if(login === null) { return <Loading isVisible={true} text="Cargando..." /> }

#   return (login === true) ? <AccountLogged user={user} setReloadUser={setReloadUser} /> : <AccountGuess />;
  }
```

B. Pasamos el xx de AccountLogged a AccountOptions

```js
# export default function AccountLogged(props) {
  const { user, setReloadUser } = props;
    ...
    return (
      <View style={styles.btnEntrarView} ><UserInfo user={user} toastResf={toastResf} />
#       <AccountOptions user={user} toastResf={toastResf} setReloadUser={setReloadUser} />
```

C. Pasamos el xx de AccountOptions a ChangeBasicInfo

```js
# export default function AccountOptions(props) {
    const { user, toastResf, setReloadUser } = props;
    
    const onClickListItem = (name) => {
      switch (name) {
        case "basic":
          setModalContent(
#           <ChangeBasicInfo name={user.name} setShowModal={setShowModal} setReloadUser={setReloadUser} />
          );
```

D. Creamos `App\component\Account\ChangeBasicInfo.js`

<!-- texto de un imput, usar el texto de input, usar input texto-->

```js
import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, Input } from 'react-native-elements';
import { updateUser } from '../../Service/AuthService';
import Loading from "../Loading";

export default function ChangeBasicInfo(props) {
  const { name, setShowModal, setReloadUser } = props;
  const [newName, setNewName] = useState(name);
  const [error, setError] = useState(null);
  const [isLoading, setisLoading] = useState(false);

  const onSubmit = () => {
    setError(null);
    if(!newName) {
      setError("El nombre no puede estar vacio.");
    } else if(newName === name) {
      setError("El nombre no puede se el mismo al actual.");
    } else {
      setisLoading(true);
      updateUser({name: newName})
        .then(resp => {
          console.log("cambiar nombre resp", resp);
          setisLoading(false);
          setShowModal(false);
          setReloadUser(true);
        })
        .catch(error => {
          console.log("cambiar nombre error", error);
          setisLoading(false);
        });
      ;
    }
  }
  
  return (
    <View style={styles.view}>
      <Input
        placeholder="Nombre y apellidos"
        containerStyle={styles.input}
        rightIcon={{
          type: "material-community",
          name: "account-circle-outline",
          color: "#c2c2c2",
        }}
        defaultValue={name || ""}
        onChange={(e) => setNewName(e.nativeEvent.text) }
        errorMessage={error}
      />
      <Button
        title="Cambiar nombre"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={onSubmit}
      />
      <Loading isVisible={isLoading} text="Actuaslizando..." />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  input: {
    marginBottom: 10,
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});
```

**Resultado:** Checar objetivo.

![22-actualizar-nombre-reactivo](./img/22-actualizar-nombre-reactivo.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 85. Formulario para cambiar el email

Lo mismo que cambiar nombre checar codigo

### 85. Formulario para cambiar el contraseña

Lo misco checar codigo

### RESULTADO: Codigo

TODO: Poner gif del manejo de usuarios

![ejecutar-app](./img/23-react-native-usuarios.zip)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

----

----

<!--
. separacion capitulo, divicion capitulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

## Sección 9:Creación de productos

### 96. Botón para crear producto solo para logueados

**Objetivo** Solo los usuarios registrados podran crear productos, y
como tendremos varias pantallas para esta seccion crearemos una carpeta
donde guardaremos estas pantallas

A. Creamos carpeta `App\screens\products` y dentro de esta metemos a
`App\screens\Products.js` y cambiamos

- Cambiamos `App\navigation\ProductsStack.js`

```js
  import React from 'react'; import { createStackNavigator } from "@react-navigation/stack";
# import Products from "./../screens/products/Products";
  const Stack = createStackNavigator();
  export default function ProductsStack() {
    return (<Stack.Navigator><Stack.Screen name="Products" component={Products} options={{ title: "Productos" }} ></Stack.Screen></Stack.Navigator>)
  }
```

B. Modificamos `App\screens\products\Products.js` para agregar el
boton

- `useIsFocused();` Para saber cuando el usuario entra y sale del screen
- `useEffect(() => {` Para ejecutar codigo cadavez que el usuario entra
- `isAuth()` Con este sabemos si el usuario esta o no logueado.
- `{user &&` Mostramos el boton solo si esta loguado el usuario.

```js
  import React, { useState, useEffect } from 'react'
  import { useIsFocused } from '@react-navigation/native';
  import { Button } from 'react-native';
  import { View, Text, StyleSheet } from 'react-native'
  import { Icon } from 'react-native-elements'
  import { isAuth } from "../../Service/AuthService";

  export default function Products() {
    const [user, setUser] = useState(null);
    const isFocused = useIsFocused();

    useEffect(() => {
      if(isFocused) {
        isAuth()
          .then(user => {
            setUser(user);
          })
          .catch(error => {
            console.log("Products, usuaruio no loguado");
            setUser(null);
          })
        ;
      }
    }, [isFocused])

    return (
      <View style={styles.viewBody}>
        <Text></Text>
        <Text>Products</Text>
        {user &&
          <Icon
            reverse // invertir colores
            type="material-community"
            name="plus"
            color="#00a680"
            containerStyle={styles.btnAddCtr}
          />
        }
      </View>
    );
  }

  const styles = StyleSheet.create({
    viewBody: {
      flex: 1,
      backgroundColor: "#fff",
    },
    btnAddCtr: {
      position: "absolute",
      bottom: 10,
      right: 10,
      shadowColor: "#000",
      // width (negativo=izq) height: (negativo=arriba)
      shadowOffset: { width: 2, height: 2 },
      shadowOpacity: 0.5,
    }
  });
```

**Resultado:** Tenemos el sig. boton solo cuando estamos logueados

![ejecutar-app](./img/24-boton-agregar-redondo.png)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 98. Creando y navegando a la Screen para crear restaurantes

**Objetivo:** Hacer que cuando se precione el boton que agregamos, se
habra la screen de agregar producto.

A. Agregamos `App\screens\products\AddProduct.js`

- Preparamos de una vez el loading y el toast

```js
import React, { useState, useEffect, useRef } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Toast from "react-native-easy-toast";

import Loading from "../../component/Loading";

export default function AddProduct() {
  const toast = useRef()

  return (
    <View style={styles.viewBody}>
      <Text></Text>
      <Text>Agregar Producto</Text>
      <Toast ref={toast} position="top" opasity={0.9} />
      <Loading isVisible={false} text="Agregando..." />
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#fff",
  },
  btnAddCtr: {
    position: "absolute",
    bottom: 10,
    right: 10,
    shadowColor: "#000",
    // width (negativo=izq) height: (negativo=arriba)
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.5,
  }
});

```

B. agregar screen en `App\navigation\ProductsStack.js`

```js
  import React from 'react'; import { createStackNavigator } from "@react-navigation/stack"; import Products from "./../screens/products/Products";
+ import AddProduct from "./../screens/products/AddProduct";
  const Stack = createStackNavigator();
  export default function ProductsStack() {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Products" component={Products} options={{ title: "Productos" }} ></Stack.Screen>
+       <Stack.Screen name="AddProduct" component={AddProduct} options={{ title: "Agregar Producto" }} ></Stack.Screen>
      </Stack.Navigator>
    )
  }
```

C. Agregamos navegacion en `App\screens\products\Products.js`

```js
  export default function Products(props) {
+   const { navigation } = props; // nos llega "navigation" por que nos lo pasa ProductsStack de forma implisita
    ...
    return (
      <View style={styles.viewBody}>
        <Text>Products</Text>
        {user && 
          <Icon reverse type="material-community" name="plus" color="#00a680" containerStyle={styles.btnAddCtr}
+           onPress={()=>{ navigation.navigate("add-product"); }}
          />
        }
    )
  ...
```

**Resultado:** Al precionar el boton de mas navegamos de la pantalla
de productos a agregarProducto

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 99. Formulario de alta de restaurantes

**Objetivo:** Crear el componente que contendra nuestro formulario

A. Agregamos `App\component\products\AddProductForm.js`

```js
import React from 'react'
import { ScrollView } from 'react-native';
import { View, Text, StyleSheet } from 'react-native'
import { Input } from 'react-native-elements';

export default function AddProductForm(props) {
  const { toastRef, setIsLoading, navigation } = props;

  return (
    <ScrollView style={styles.viewBody}>
      <FormAdd />
    </ScrollView>
  );
}

function FormAdd(props) {
  return (
    <View style={styles.viewForm}>
      <Input
        placeholder="Nombre Producto"
        containerStyle={styles.inputCtnr}
      />

      <Input
        placeholder="Precio"
        keyboardType="numeric"
        containerStyle={styles.inputCtnr}
      />

      <Input
        placeholder="Cantidad"
        keyboardType="numeric"
        containerStyle={styles.inputCtnr}
      />

      <Input
        placeholder="Descripcion"
        multiline={true}
        inputContainerStyle={styles.textArea}
      />

    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    height: "100%",
  },
  viewForm: {
    marginLeft: 10,
    marginRight: 10,
  },
  inputCtnr: {
    marginBottom: 10,
  },
  textArea: {
    height: 100,
    width: "100%",
    padding: 0,
    margin: 0,
  }
});
```

B. Lo incluimos en `App\screens\products\AddProduct.js`

```js
  import React, { useState, useEffect, useRef } from 'react' import { View, Text, StyleSheet } from 'react-native' import Toast from "react-native-easy-toast"; import Loading from "../../component/Loading";
+ import AddProductForm from '../../component/products/AddProductForm';
  export default function AddProduct(props) {
    const { navigation } = props; const toastRef = useRef(); const [isLoading, setIsLoading] = useState(false);
    return (
      <View style={styles.viewBody}> <Text></Text> <Text>Agregar Producto</Text>
 +      <AddProductForm toastRef={toastRef} setIsLoading={setIsLoading} navigation={navigation} />
      <Toast ref={toastRef} position="top" opasity={0.9} /> <Loading isVisible={isLoading} text="Agregando..." /> </View>
    );
  }
  const styles = StyleSheet.create({...});
```

**Resultado:** Tenemos la vista del formulario  para agregar un producto

![09-vista-login-1](./img/25-formulario-agregar-producto.png)

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 100. Guardando los datos del producto en el estado

**Objetivo:**: Vamos a guardar el formulario en un objeto para luego
enviarlo a nuestro backend

A. Modificamos `App\component\products\AddProductForm.js` para agregar
dicho objeto y su rellanado.

```js
  import React, { useState } from 'react'
  import { ScrollView } from 'react-native';
  import { View, Text, StyleSheet, Button } from 'react-native'
  import { Input } from 'react-native-elements';


+ function productSchema() {
+   return {
+     name: null,
+     price: 0,
+     amount: 0,
+     description: null
+   }
+ }

  export default function AddProductForm(props) {
    const { toastRef, setIsLoading, navigation } = props;
+   const [ newProduct, setNewProduct ] = useState(productSchema());

+   const addProduct = () => {
+     console.log("Guardando...", newProduct);
+   }

    return (
      <ScrollView style={styles.viewBody}>
        <FormAdd newProduct={newProduct} setNewProduct={setNewProduct} />
        <Button
          title="Crear Producto"
+         onPress={addProduct}
          buttonStyle={styles.btnAdd}
        />
      </ScrollView>
    );
  }

  function FormAdd(props) {
    const { newProduct, setNewProduct } = props;

+   const onChangeNewProduct = (e, field) => {
+     setNewProduct({...newProduct, [field]: e.nativeEvent.text} );
+   }

    return (
      <View style={styles.viewForm}>
        <Input
          placeholder="Nombre o titulo del producto"
          label="Nombre Producto"
          containerStyle={styles.inputCtnr}
+         value={newProduct.name}
+         onChange={e => onChangeNewProduct(e, "name")}
        />

        <Input
          label="Precio"
          placeholder="Ejemplo: 1200.50"
          keyboardType="numeric"
          containerStyle={styles.inputCtnr}
+         value={newProduct.price}
+         onChange={e => onChangeNewProduct(e, "price")}
        />

        <Input
          label="Cantidad"
          placeholder="cantidad en almacen"
          keyboardType="numeric"
          containerStyle={styles.inputCtnr}
+         value={newProduct.amount}
+         onChange={e => onChangeNewProduct(e, "amount")}
        />

        <Input
          label="Descripcion"
          placeholder="Descripcion del producto"
          multiline={true}
          inputContainerStyle={styles.textArea}
+         value={newProduct.description}
+         onChange={e => onChangeNewProduct(e, "description")}
        />

      </View>
    );
  }
  const styles = StyleSheet.create({
    viewBody: { height: "100%", },
    viewForm: { marginLeft: 10, marginRight: 10, },
    inputCtnr: { marginBottom: 10, },
    textArea: { height: 100, width: "100%", padding: 0, margin: 0, },
+   btnAdd: {
+     backgroundColor: "#00a680",
+     margin: 20,
+   }
+ });
```

**Resultado:** Al precionar el boton de crear producto, podremos ver en
la consola los datos del formulario

```r
{ 
  "amount": "86",
  "price": "15",
  "description": "t hoy jueve",
  "name": "Olvi",
}
```

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 101. Componente para subir imágenes del restaurante

**Objetivo:** Crear el botn

A. En `App\component\products\AddProductForm.js` agregamos el componente

```js
  export default function AddProductForm(props) {
    ...
    return (
      <ScrollView style={styles.viewBody}>
        <FormAdd newProduct={newProduct} setNewProduct={setNewProduct} />
+       <AddImages />
        ...
    )
  }

+ function AddImages() {
    const selectImage = () => {
      console.log("select image...");
    }

    return (
      <View style={styles.viewAddImage}>
        <Icon
          type="material-community"
          name="camera"
          color="#7a7a7a"
          containerStyle={styles.btnAddImage}
          onPress={selectImage}
        />
      </View>
    );
  }

  const styles = StyleSheet.create({
    viewBody: { height: "100%", },
    viewForm: { marginLeft: 10, marginRight: 10, },
    inputCtnr: { marginBottom: 10, },
    textArea: { height: 100, width: "100%", padding: 0, margin: 0, },
    btnAdd: { backgroundColor: "#00a680", margin: 20, },
+   viewAddImage: {
      flexDirection: "row",
      marginLeft: 20,
      marginRight: 20,
      marginTop: 30,
    },
+   btnAddImage: {
      alignItems: "center",
      justifyContent: "center",
      marginRight: 10,
      height: 70,
      width: 70,
      backgroundColor: "#e3e3e3"
    },
  });
```

**Resultado:** Tenemos el icono de la camara que funcionara como boton,
mas el componente que nos acomodara las imagenes en horizontal

![26-icono-agregar-imagen-producto](./img/26-icono-agregar-imagen-producto.png)

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 102. Selecionando imágenes de la galeria

A. En `App\component\products\AddProductForm.js` agregamos la funcionalida
de seleccionar una imagen y guardar la uri en un estado de tipo array
para luego poder mostrar/subir la imagen.

```js
  import React, { useState } from 'react'
  import { ScrollView, View, Text, StyleSheet } from 'react-native';
  import { Input, Button, Icon } from 'react-native-elements';
+ import * as Permissions from "expo-permissions";
+ import * as ImagePicker from "expo-image-picker";
+ import mime from "mime";

  function productSchema() {
    return {
      name: null,
      price: 0,
      amount: 0,
      description: null
    }
  }

  export default function AddProductForm(props) {
    const { toastRef, setIsLoading, navigation } = props;
    const [ newProduct, setNewProduct ] = useState(productSchema());
+   const [ images, setImages ] = useState([]);

    const addProduct = () => {
      console.log("Guardando...", newProduct);
+     console.log("Imagenes...", images);
    }

    return (
      <ScrollView style={styles.viewBody}>
        <FormAdd newProduct={newProduct} setNewProduct={setNewProduct} />
+       <AddImages toastRef={toastRef} images={images} setImages={setImages} />
        <Button
          title="Crear Producto"
          onPress={addProduct}
          buttonStyle={styles.btnAdd}
        />
      </ScrollView>
    );
  }

  function FormAdd(props) {...}

+ function AddImages(props) {
    const { toastRef, images, setImages } = props;

+   const selectImage = async () => {
      const resultPermision = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      const resultPermisionCamera = resultPermision.permissions.cameraRoll.status;
      if(resultPermisionCamera === 'denied') {
        toastRef.current.show('Es necesario aceptar los permisos de la galeria');
      } else {
        const result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4,3],
          //base64: true por si queremos obtener el base64
        });
        if(result.cancelled) {
          toastRef.current.show('Seleccion de imagen cancelada');
        } else {
          // Subir imagen
          const newImageUri = "file:///" + result.uri.split("file:/").join(""); // NOTA: Dejar como viene la uri para ios
          const nameX = newImageUri.split("/").pop();
          const typeX = mime.getType(newImageUri);
          const file = { uri : newImageUri, type: typeX, name: nameX }
          //console.log("A_subir", file);
+         setImages([...images, file.uri]);

          //uploadUserPicture(file)
          //  .then(resp => { toastRef.current.show('Imagen guardada:' + resp); })
          //  .catch(error => { console.log("ErrorSubIma:", error); })
          //;
          //---------------------------
        }
      }
    }

    return (
      <View style={styles.viewAddImage}>
        <Icon
          type="material-community"
          name="camera"
          color="#7a7a7a"
          containerStyle={styles.btnAddImage}
          onPress={selectImage}
        />
      </View>
    );
  }
  const styles = StyleSheet.create({...});
```

**Resultado:** Si seleccionamos una imagen y seleccionamos guardar veremos
las uris en la consola lo que significan las imagenes.

```js
Guardando... Object {
  "amount": 0,
  "description": null,
  "name": null,
  "price": 0,
}
Imagenes... Array [
  "file:///data/user/0/host.exp.exponent/cache/ExperienceData/%2540anonymous%252Fmis-comer-11fddc82-100d-4576-bfb6-c59f6f79f2c5/ImagePicker/040251a1-0c3f-49aa-a18d-ad65d878f16d.jpg",
  "file:///data/user/0/host.exp.exponent/cache/ExperienceData/%2540anonymous%252Fmis-comer-11fddc82-100d-4576-bfb6-c59f6f79f2c5/ImagePicker/de780371-feba-46a4-adc1-707c97afb9e9.jpg",
]
```

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 103. Mostrando las imágenes seleccionados

**Objetivo:** Vamos a mostrar las imagenes que estemos seleccionando, y
cuando llegue a 5 imagenes el boton de agregar imagenes se ocultara,
para ya no permitir subir mas de estas.

A. En `App\component\products\AddProductForm.js` Agregamos

```js
  function AddImages(props) {
    const { toastRef, images, setImages } = props;
    const selectImage = async () => {...}

    return (
      <View style={styles.viewAddImage}>
+       {(images.length < 5) && (
          <Icon
            type="material-community"
            name="camera"
            color="#7a7a7a"
            containerStyle={styles.btnAddImage}
            onPress={selectImage}
          />
+       )}
+       {images.map((imageUri, i) => (
+         <Avatar
+           key={i}
+           style={styles.imageProd}
+           source={{uri: imageUri}}
+         />
+       ))}
      </View>
    );
  }

  const styles = StyleSheet.create({
    ...
+   imageProd: {
+     width: 70,
+     height: 70,
+     marginRight: 10
+   },
  });
```

**Resultado:** Logramos el objetivo de este titulo.

![27-imagenes-agregadas-mostradas-dinamicamente](./img/27-imagenes-agregadas-mostradas-dinamicamente.png)

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 104. Eliminar cualquier imagen seleccionada

**Objetivo:** Cuando el usuario pulse sobre una imagen, vamos a mostrar
un modal preguntando si quiere eliminar la imagen o no.

En `App\component\products\AddProductForm.js`

```js
  // traemos Alert
# import { ScrollView, View, Text, StyleSheet, Alert } from 'react-native';

  function productSchema() {...}
  export default function AddProductForm(props) {...}
  function FormAdd(props) {...}
  
  function AddImages(props) {
    const { toastRef, images, setImages } = props;
+   const removeImage = (imageUri) => {
      console.log("imageUri to delete: ", imageUri);
+     Alert.alert("Eliminar Imagen", "¿Estas seguro que quieres eliminar la imagen?", // mostramos alerta
        [ 
          { text: "Cancel", style: "cancel" },
          { text: "Eliminar", onPress: () => { 
            console.log("Eliminar");
+           let imagesFiltered = images.filter(image => image!=imageUri);// eliminamos la imagen seleccionada
            setImages(imagesFiltered);
          }}
        ],
        { cancelable: false } // para que ¿?
      );
+   }

    return (...);
  }
```

**Resultado:** Podemos eliminar la imagen dandole click encima.

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 105. Mostrando la imagen principal del restaurante

**Objetivo:** Mostraremos en la parte superior la imagen de nuestro
producto, y cuando no ahiga ninguna mostraremos una por defecto.

A. En `App\component\products\AddProductForm.js` Agregamos un nuevo compo

```js
# import { ... , Image } from 'react-native';
  ...
  export default function AddProductForm(props) {
    ...

    return (
      <ScrollView>
+       <MainImageProduct image={images[0]} />
        <FormAdd newProduct={newProduct} setNewProduct={setNewProduct} />
        ...
    )
  }

+ function MainImageProduct(props) {
    const { image } = props;
    return (
      <View style={styles.mainImage}>
        <Image 
          source={ image ? { uri: image } : require("../../../assets/img/no-image.png") }
          style={{ width: "100%", height: 200 }}
        />
      </View>
    );
+ }
```

**Resultado:** Podemos ver la primera imagen como la principal.

![28-seleccionar-imagen-y-mostrar-principal](./img/28-seleccionar-imagen-y-mostrar-principal.gif)

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 106. Icono para abrir el mapa y seleccionar la localización del restaurante

En `App\component\products\AddProductForm.js` agregamos

<!-- agregar icono a la izquierda -->

```js
+ import Modal from "../Modal";

  function productSchema() {
    return {
      name: null, price: null, amount: null, description: null,
+     address: null,
    }
  }

  export default function AddProductForm(props) {
    const [ isVisibleMap, setIsVisibleMap ] = useState(false);
    ...
    return (
      <ScrollView>
#       <FormAdd newProduct={newProduct} setNewProduct={setNewProduct} setIsVisibleMap={setIsVisibleMap} />
        ...
+       <Map isVisibleMap={isVisibleMap} setIsVisibleMap={setIsVisibleMap} />
      </ScrollView>
    )
  }

  function FormAdd(props) {
    const { newProduct, setNewProduct, setIsVisibleMap } = props;
    
+   const selectAddress = () => {
+     setIsVisibleMap(true);
+   }

    return (
      ...
      <Input
          label="Direccion"
          placeholder="Ubicacion del producto"
          containerStyle={styles.inputCtnr}
          onChange={e => onChangeNewProduct(e, "address")}
          rightIcon={{
            type: "material-community",
            name: "google-maps",
            color: "#c2c2c2",
+           onPress: selectAddress,
          }}
        />
      ...
    )
  }

+ function Map(prop) {
    const { isVisibleMap, setIsVisibleMap } = prop;
    return (
      <Modal isVisible={isVisibleMap} setIsVisible={setIsVisibleMap}>
        <Text>Mapa</Text>
      </Modal>
    )
+ }
```

**Resultado:** Tenemos un nuevo icono el cual abrira el modal en el
cual tendremos el mapa.

![29-input-icono-izq](./img/29-input-icono-izq.png)

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 107. Instalando y explicando Expo MapView y Location

**Cuidado:** Como dice en la [documentacion](https://docs.expo.io/versions/latest/sdk/map-view/#deploying-to-a-standalone-app-on-android)
tenemos que generar una `API-Key` para que NO FALLE cuando tengamos
el APK creado.

```r
# para mostrar el mapa
yarn add react-native-maps 
# para obtener la ubicacion del dispo
yarn add expo-location@~9.0.0
```

**Resultado:** Se instalaron los sig. paquetes

```json
"dependencies": {
    "expo-location": "^10.0.0",
    "react-native-maps": "^0.27.1",
}
```

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 108. Obteniendo la localización del dispositivo

<!-- obtener cordenadas, ver cordenadas -->

A. En `App\component\products\AddProductForm.js` Agregamos el metodo
para que se capturar la localizacion

```js
import * as Location from "expo-location";

function Map(prop) {
  const { isVisibleMap, setIsVisibleMap, toastRef } = prop;
  const [ myLocation, setMyLocation ] = useState(null);
  //Location
  useEffect(() => {
    (async () => {
      const resultPermision = await Permissions.askAsync(Permissions.LOCATION);
      const isLocationGranted = resultPermision.permissions.location.status === "granted";
      if(isLocationGranted) {
        const actualLocation = await Location.getCurrentPositionAsync({});
        setMyLocation({
          latitude: actualLocation.coords.latitude,
          longitude: actualLocation.coords.longitude,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001,
        })
        
      } else {
        toastRef.current.show("Los permisos a Localizacion son necesarios", 3000);
      }
    })();
  }, [])

  return (
    <Modal isVisible={isVisibleMap} setIsVisible={setIsVisibleMap}>
      <Text>Mapa</Text>
    </Modal>
  )
}
```

**Resultado:** Al entrar al formulario de agregar producto nos pedira
permisos para la localizacion, para posterior capturar las cordenadas
actuales y mostrarlas en pantalla

```js
myLocation:  Object {
  "coords": Object {
    "accuracy": xxx,
    "altitude": xxx,
    "altitudeAccuracy": xx,
    "heading": xx,
    "latitude": xxx,
    "longitude": xxx,
    "speed": xx,
  },
  "mocked": false,
  "timestamp": xxx,
}
```

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 109. Abriendo Google Map con la localización del usuario

En `App\component\products\AddProductForm.js`

```js
+ import MapView from "react-native-maps";
  ...
  function Map(prop) {
    const { isVisibleMap, setIsVisibleMap, toastRef } = prop;
    const [ myLocation, setMyLocation ] = useState(null);
    useEffect(() => {...}, [])
    return (
      <Modal isVisible={isVisibleMap} setIsVisible={setIsVisibleMap}>
+       <View>
+         {myLocation && (
+           <MapView 
+             style={styles.map}
+             initialRegion={myLocation}
+             showsUserLocation={true}
+             onRegionChange={region => setMyLocation(region) }
+           >
+             <MapView.Marker coordinate={{ latitude: myLocation.latitude, longitude: myLocation.longitude }} draggable />
+           </MapView>
+         )}
+       </View>
      </Modal>
    )
  }
  const styles = StyleSheet.create({...,
+   map: {
      width: "100%",
      height: 550,
    },
  });
```

**Resultado:** Se muestra un mapa el cual tiene un marker que se mueve
cuando nosotros movemos el mapa

![30-mostrar-mapa-con-marker-siempre-enmedio](./img/30-mostrar-mapa-con-marker-siempre-enmedio.png)

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 110. Guardando localización del restaurante

En `App\component\products\AddProductForm.js`

```js
  export default function AddProductForm(props) {
    ...
    return (
      <ScrollView>
        ...
// Pasamos el estado newProduct con su set a Map
#       <Map isVisibleMap={isVisibleMap} setIsVisibleMap={setIsVisibleMap} toastRef={toastRef} newProduct={newProduct} setNewProduct={setNewProduct} />
      </ScrollView>
    );
  }

  function Map(prop) {
// recibimos el newProduct
#  const { isVisibleMap, setIsVisibleMap, toastRef, setNewProduct, newProduct } = prop;
    useEffect(() => {...}, [])

// Agregamos metodo que guardara la ubicacion
+   const confirmLoc = (loc) => {
+     setNewProduct({...newProduct, address: loc });
+     setIsVisibleMap(false);
+   }


    return (
      <Modal isVisible={isVisibleMap} setIsVisible={setIsVisibleMap}>
        <View> {myLocation && (...)} </View>
// Agregamos los botones de aceptar y cancelar
+       <View style={styles.btnsMap}>
+         <Button 
+           title="Guardar Ubicacion"
+           containerStyle={styles.btnSaveLocCnt} buttonStyle={styles.btnSaveLoc}
+           onPress={ () => confirmLoc(myLocation) }
+         />
+         <Button 
+           title="Cancelar"
+           containerStyle={styles.btnCancelLocCnt} buttonStyle={styles.btnCancelLoc}
+           onPress={ () => setIsVisibleMap(false) }
+         />
+       </View>
      </Modal>
    )
  }

// estilos para los botones
  const styles = StyleSheet.create({
    ...,
    btnsMap: {
      flexDirection: 'row',
      justifyContent: "center",
      marginTop: 10,
    },
    btnCancelLocCnt: { paddingLeft: 5, },
    btnCancelLoc: { backgroundColor: '#a60d0d', },
    btnSaveLocCnt: { paddingRight: 5, },
    btnSaveLoc: { backgroundColor: "#00a680", },
  });
```

**Resultado:** Tenemos el boton de aceptar y cancelar el cual si damos
aceptar se agregara la ubicacion al objeto de producto.

![31-botones-guardar-cancelar-ubicacion](./img/31-botones-guardar-cancelar-ubicacion.png)

```r
{
  "address": {
    "latitude": 19.6165049,
    "latitudeDelta": 0.001,
    "longitude": -98.1210413,
    "longitudeDelta": 0.001,
  },
  "amount": null, "description": null, "name": null, "price": null,
}
```

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 111. Validando formulario para subir productos

```js
function validateNewProduct(newProd, images) {
  if(!newProd) {
    return "El producto no existe";
  } else if(isEmpty(newProd.name) || newProd.name.length <= 3 ) {
    return "El nombre es obligatorio y debe tener mas de 3 caracteres";
  } else if(isEmpty(newProd.price) || newProd.price <= 0 ) {
    return "El precio es obligatorio y mayor a cero";
  } else if(isEmpty(newProd.amount) || newProd.amount <= 0 ) {
    return "La cantidad es obligatoria y mayor a cero";
  } else if(isEmpty(newProd.description) || newProd.description.length <= 3 ) {
    return "La descripcion es obligatoria debe terner mas de 3 caracteres";
  } else if(isEmpty(newProd.address) ) {
    return "La direccion es obligatoria";
  } else if(!images || images.length <= 0) {
    return "Al menos una imagen es obligatoria";
  } else {
    return true;
  }
}

export default function AddProductForm(props) {
  const { toastRef, setIsLoading, navigation } = props; const [ isVisibleMap, setIsVisibleMap ] = useState(false);
  const [ newProduct, setNewProduct ] = useState(productSchema()); const [ images, setImages ] = useState([]);

  const addProduct = () => {
+   const validationResp = validateNewProduct(newProduct, images);
+   if(validationResp === true) {
+     console.log("producto Ok");
+   } else {
+     toastRef.current.show(validationResp, 3000);
+   }
  }
  return (<ScrollView>...)
}
```

**Resultado:** Tenemos la validacion del formulario

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 112. Activando icono, cambiar de color a icono

```js
function FormAdd(props) {

+ let coors = null;
+ if(newProduct.address && newProduct.address.latitude) {
+   coors = newProduct.address.latitude + ", " + newProduct.address.longitude;
+ }
  
  return (
    <View style={styles.viewForm}>
      ...
      <Input
        label="Direccion" placeholder="Ubicacion del producto" containerStyle={styles.inputCtnr}
        onChange={e => onChangeNewProduct(e, "address")}
 +      editable={false}
 +      value={coors}
        rightIcon={{
          type: "material-community",
          name: "google-maps",
 +        color: coors ? "#00a680" : "#c2c2c2",
          onPress: selectAddress,
        }}
      />
      ---
  )
}
```

**Resultado:** Cuando seleccionamos una direccion el icono cambia de
color y el valor se le agrega

![32-cambiar-color-a-icono.png](./img/32-cambiar-color-a-icono.png)

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--
. separacion titulo, divicion titulo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 113. Subiendo las imágenes del restaurante al Storage

Al final tuve resulto un codigo como el de abajo para subir la imagen

```js
  import React, { useState, useEffect } from 'react'
  import { ScrollView, View, Text, StyleSheet, Alert, Image, ToastAndroid, Platform, AlertIOS } from 'react-native';
  import { Input, Button, Icon, Avatar } from 'react-native-elements';
  import * as Permissions from "expo-permissions";
  import * as ImagePicker from "expo-image-picker";
  import mime from "mime";
  import Modal from "../Modal";
  import * as Location from "expo-location";
  import MapView from "react-native-maps";
  import { isEmpty } from 'lodash';
  import { addProduct, uploadProductPicture  } from "../../Service/ProductService";

  function productSchema() {
    return {
      name: null,
      price: null,
      amount: null,
      description: null,
      address: null,
    }
  }

  function validateNewProduct(newProd, images) {
    if(!newProd) {
      return "El producto no existe";
    } else if(isEmpty(newProd.name) || newProd.name.length <= 3 ) {
      return "El nombre es obligatorio y debe tener mas de 3 caracteres";
    } else if(isEmpty(newProd.price) || newProd.price <= 0 ) {
      return "El precio es obligatorio y mayor a cero";
    } else if(isEmpty(newProd.amount) || newProd.amount <= 0 ) {
      return "La cantidad es obligatoria y mayor a cero";
    } else if(isEmpty(newProd.description) || newProd.description.length <= 3 ) {
      return "La descripcion es obligatoria debe terner mas de 3 caracteres";
    } else if(isEmpty(newProd.address) ) {
      return "La direccion es obligatoria";
    } else if(!images || images.length <= 0) {
      return "Al menos una imagen es obligatoria";
    } else {
      return true;
    }
  }

  async function uploadImages(imagesIn, idProductAdded) {
    for (const i in imagesIn) {
      console.log("enviando imagen " + imagesIn[i].name + "...");
      let resp = await uploadProductPicture(imagesIn[i], idProductAdded);
      if(resp.idProductImage) {
        console.log("  - Respuesta: Imagen guardada en servidor: ", resp.idProductImage, resp.name);
      } else {
        console.log("  - Error:", resp);
      }
    }
  }

  function showToast(msg) {
    if (Platform.OS === 'android') {
      ToastAndroid.show(msg, ToastAndroid.LONG);
    } else {
      AlertIOS.alert(msg);
    }
  }

  export default function AddProductForm(props) {
    const { toastRef, setIsLoading, navigation } = props;
    const [ newProduct, setNewProduct ] = useState(productSchema());
    const [ images, setImages ] = useState([]);
    const [ isVisibleMap, setIsVisibleMap ] = useState(false);

    const addProductEvent = () => {
      setIsLoading(true);

      const validationResp = validateNewProduct(newProduct, images);
      if(validationResp === true) {
        console.log("producto Ok");
        //primero crear producto
        addProduct(newProduct)
          .then(productAdded => {
            if(productAdded.idProduct) {
              // segundo subir imagenes (ya que antes no contamos con el id del producto)
              uploadImages(images, productAdded.idProduct)
                .then((resp) => {
                  setNewProduct(productSchema());
                  setImages([]);
                  setIsLoading(false);
                  showToast("Producto agregado con exito");
                  navigation.goBack(null);
                })
                .catch((error) => {
                  showToast("Producto guardado, pero ocurrio un error al subir las imagenes", 3000);
                  setIsLoading(false);
                })
              ;
            } else {
              console.log("Error, peticion agregar producto exitosa, pero no llego el id");
              showToast("Error, intente mas tarde");
              setIsLoading(false);
            }
          })
          .catch(error => {
            showToast(error);
            setIsLoading(false);
          })
        ;
      } else { // fallo la validacion del formulario
        setIsLoading(false);
        showToast(validationResp)
      }
    }

    return (
      <ScrollView>
        <MainImageProduct image={images[0] ? images[0].uri : null} />
        <FormAdd newProduct={newProduct} setNewProduct={setNewProduct} setIsVisibleMap={setIsVisibleMap} />
        <AddImages toastRef={toastRef} images={images} setImages={setImages} />
        <Button
          title="Crear Producto"
          onPress={addProductEvent}
          buttonStyle={styles.btnAdd}
        />
        <Map isVisibleMap={isVisibleMap} setIsVisibleMap={setIsVisibleMap} toastRef={toastRef} newProduct={newProduct} setNewProduct={setNewProduct} />
      </ScrollView>
    );
  }

  function FormAdd(props) {
    const { newProduct, setNewProduct, setIsVisibleMap } = props;

    const onChangeNewProduct = (e, field) => {
      setNewProduct({...newProduct, [field]: e.nativeEvent.text} );
    }

    const selectAddress = () => {
      setIsVisibleMap(true);
    }

    let coors = null;
    if(newProduct.address && newProduct.address.latitude) {
      coors = newProduct.address.latitude + ", " + newProduct.address.longitude;
    }
    
    return (
      <View style={styles.viewForm}>
        <Input
          placeholder="Nombre o titulo del producto"
          label="Nombre Producto"
          containerStyle={styles.inputCtnr}
          onChange={e => onChangeNewProduct(e, "name")}
        />

        <Input
          label="Precio"
          placeholder="Ejemplo: 1200.50"
          keyboardType="numeric"
          containerStyle={styles.inputCtnr}
          onChange={e => onChangeNewProduct(e, "price")}
        />

        <Input
          label="Cantidad"
          placeholder="cantidad en almacen"
          keyboardType="numeric"
          containerStyle={styles.inputCtnr}
          onChange={e => onChangeNewProduct(e, "amount")}
        />

        <Input
          label="Direccion"
          placeholder="Ubicacion del producto"
          containerStyle={styles.inputCtnr}
          onChange={e => onChangeNewProduct(e, "address")}
          editable={false}
          value={coors ? coors : "" }
          rightIcon={{
            type: "material-community",
            name: "google-maps",
            color: coors ? "#00a680" : "#c2c2c2",
            onPress: selectAddress,
          }}
        />

        <Input
          label="Descripcion"
          placeholder="Descripcion del producto"
          multiline={true}
          textAlignVertical="top"
          inputContainerStyle={styles.textArea}
          style={{ height: "100%" }}
          onChange={e => onChangeNewProduct(e, "description")}
        />

      </View>
    );
  }

  function AddImages(props) {
    const { toastRef, images, setImages } = props;

    const selectImage = async () => {
      const resultPermision = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      const resultPermisionCamera = resultPermision.permissions.cameraRoll.status;
      if(resultPermisionCamera === 'denied') {
        showToast('Es necesario aceptar los permisos de la galeria');
      } else {
        const result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4,3],
          //base64: true por si queremos obtener el base64
        });
        if(result.cancelled) {
          showToast('Seleccion de imagen cancelada');
        } else {
          // Subir imagen
          const newImageUri = "file:///" + result.uri.split("file:/").join(""); // NOTA: Dejar como viene la uri para ios
          const nameX = newImageUri.split("/").pop();
          const typeX = mime.getType(newImageUri);
          const file = { uri : newImageUri, type: typeX, name: nameX }
          //console.log("A_subir", file);
          setImages([...images, file]);

          //uploadUserPicture(file)
          //  .then(resp => { showToast('Imagen guardada:' + resp); })
          //  .catch(error => { console.log("ErrorSubIma:", error); })
          //;
          //---------------------------
        }
      }
    }

    const removeImage = (imageUri) => {
      console.log("imageUri to delete: ", imageUri);
      Alert.alert("Eliminar Imagen", "¿Estas seguro que quieres eliminar la imagen?",
        [ 
          { text: "Cancel", style: "cancel" },
          { text: "Eliminar", onPress: () => { 
            console.log("Eliminar");
            let imagesFiltered = images.filter(image => image!=imageUri);
            setImages(imagesFiltered);
          }}
        ],
        { cancelable: false }
      );
    }

    return (
      <View style={styles.viewAddImage}>
        {(images.length < 4) && (
          <Icon
            type="material-community"
            name="camera"
            color="#7a7a7a"
            containerStyle={styles.btnAddImage}
            onPress={selectImage}
          />
        )}
        {images && images.map((image, i) => (
          <Avatar
            key={i}
            style={styles.imageProd}
            source={{uri: image.uri}}
            onPress={() => removeImage(image) }
          />
        ))}
      </View>
    );
  }

  function MainImageProduct(props) {
    const { image } = props;
    return (
      <View style={styles.mainImage}>
        <Image 
          source={ image ? { uri: image } : require("../../../assets/img/no-image.png") }
          style={{ width: "100%", height: 200 }}
        />
      </View>
    );
  }

  function Map(prop) {
    const { isVisibleMap, setIsVisibleMap, toastRef, setNewProduct, newProduct } = prop;
    const [ myLocation, setMyLocation ] = useState(null);
    //Location
    useEffect(() => {
      (async () => {
        const resultPermision = await Permissions.askAsync(Permissions.LOCATION);
        const isLocationGranted = resultPermision.permissions.location.status === "granted";
        if(isLocationGranted) {
          const locationTemp = await Location.getCurrentPositionAsync({});
          setMyLocation({
            latitude: locationTemp.coords.latitude,
            longitude: locationTemp.coords.longitude,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001,
          })
          
        } else {
          showToast("Los permisos a Localizacion son necesarios", 3000);
        }
      })();
    }, [])

    const confirmLoc = (loc) => {
      setNewProduct({...newProduct, address: loc });
      setIsVisibleMap(false);
    }

    return (
      <Modal isVisible={isVisibleMap} setIsVisible={setIsVisibleMap}>
        <View>
          {myLocation && (
            <MapView 
              style={styles.map}
              initialRegion={myLocation}
              showsUserLocation={true}
              onRegionChange={region => setMyLocation(region) }
            >
              <MapView.Marker coordinate={{ latitude: myLocation.latitude, longitude: myLocation.longitude }} draggable />
            </MapView>
          )}
        </View>
        <View style={styles.btnsMap}>
          <Button 
            title="Guardar Ubicacion"
            containerStyle={styles.btnSaveLocCnt} buttonStyle={styles.btnSaveLoc}
            onPress={ () => confirmLoc(myLocation) }
          />
          <Button 
            title="Cancelar"
            containerStyle={styles.btnCancelLocCnt} buttonStyle={styles.btnCancelLoc}
            onPress={ () => setIsVisibleMap(false) }
          />
        </View>
      </Modal>
    )
  }

  const styles = StyleSheet.create({
    viewForm: {
      marginLeft: 10,
      marginRight: 10,
    },
    inputCtnr: {
      marginBottom: 10,
    },
    textArea: {
      height: 100,
      width: "100%",
      padding: 0,
      margin: 0,
    },
    btnAdd: {
      backgroundColor: "#00a680",
      margin: 20,
    },
    viewAddImage: {
      flexDirection: "row",
      marginLeft: 20,
      marginRight: 20,
      marginTop: 30,
    },
    btnAddImage: {
      alignItems: "center",
      justifyContent: "center",
      marginRight: 10,
      height: 70,
      width: 70,
      backgroundColor: "#e3e3e3"
    },
    imageProd: {
      width: 70,
      height: 70,
      marginRight: 10
    },
    mainImage: {
      alignItems: "center",
      height: 200,
      marginBottom: 20,
    },
    map: {
      width: "100%",
      height: "90%",
    },
    btnsMap: {
      flexDirection: 'row',
      justifyContent: "center",
      marginTop: 10,
    },
    btnCancelLocCnt: {
      paddingLeft: 5,
    },
    btnCancelLoc: {
      backgroundColor: '#a60d0d',
    },
    btnSaveLocCnt: {
      paddingRight: 5,
    },
    btnSaveLoc: {
      backgroundColor: "#00a680",
    },
  });

```


### 116. Configuran API Google Maps

expo build:android