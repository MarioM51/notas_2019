# Gatsby de Cero a Experto

## indice

- [Gatsby de Cero a Experto](#gatsby-de-cero-a-experto)
  - [indice](#indice)
  - [Accesos Rapidos](#accesos-rapidos)
  - [Pendientes](#pendientes)
  - [Resultados Finales](#resultados-finales)
    - [Proyecto: Introduccion](#proyecto-introduccion)
    - [Proyecto: Portafolio](#proyecto-portafolio)
  - [Introduccion](#introduccion)
    - [29. Uso de Layouts, Enlaces, estilos, SEO](#29-uso-de-layouts-enlaces-estilos-seo)
    - [33. Añadiendo CSS Global, Sass, uso de Imagenes](#33-añadiendo-css-global-sass-uso-de-imagenes)
  - [Sección 5:PROYECTO: Curriculum web](#sección-5proyecto-curriculum-web)
    - [Inicio](#inicio)
    - [47. Componente Profile (imagen de perfil mas info basica)](#47-componente-profile-imagen-de-perfil-mas-info-basica)
    - [50. Componente Social Media](#50-componente-social-media)
    - [55. Pagina de Habilidades](#55-pagina-de-habilidades)
    - [56. Pagina de Proyectos](#56-pagina-de-proyectos)
    - [57. Compilando aplicacion](#57-compilando-aplicacion)
  - [PROYECTO: Blog + Strapi + GraphQL](#proyecto-blog--strapi--graphql)
    - [67. Prerar Strapi como Backend](#67-prerar-strapi-como-backend)
    - [68. Conectando Strapi con Gatsby](#68-conectando-strapi-con-gatsby)
    - [70. Uso de Sass global](#70-uso-de-sass-global)
    - [71. Agregar SemanticUI](#71-agregar-semanticui)
    - [72. CODIGO: Crear codigo base de nuestro proyecto](#72-codigo-crear-codigo-base-de-nuestro-proyecto)
    - [77. Obteniendo todos los post del blog](#77-obteniendo-todos-los-post-del-blog)
    - [78. Creacion de paginas dinamicas: PAGINACION](#78-creacion-de-paginas-dinamicas-paginacion)
    - [81. Agregando estilos a la paginacion y formatear fecha](#81-agregando-estilos-a-la-paginacion-y-formatear-fecha)
    - [83. Componente para movernos entre las páginas](#83-componente-para-movernos-entre-las-páginas)
    - [84. Generando una pagina por entrada en el blog](#84-generando-una-pagina-por-entrada-en-el-blog)
    - [85. Agregar estilos y pintando toda la info en PostDetails](#85-agregar-estilos-y-pintando-toda-la-info-en-postdetails)
  - [Por el Camino](#por-el-camino)
    - [gatsby-node](#gatsby-node)
      - [Usar archivo json (en filesystem) para generar paginas](#usar-archivo-json-en-filesystem-para-generar-paginas)

<!--

#########################################

-->

----

<!--

#########################################

-->

## Accesos Rapidos

- [Doc Gatsby](https://www.gatsbyjs.com/docs/)
- [Doc Bootstrap](https://react-bootstrap.github.io)
- [Plugin usar sass](https://www.gatsbyjs.com/plugins/gatsby-plugin-sass/)
- [Plugin usar svg](https://www.gatsbyjs.com/plugins/gatsby-plugin-svgr/)
- [Plugin evitar problemas con variable window](https://yarnpkg.com/package/@loadable/component)
- [Conectar Strapi con Gatsby](https://www.gatsbyjs.com/plugins/gatsby-source-strapi)
- [en Strapi mejorar editor de texto enrriquesido](https://strapi.io/documentation/developer-docs/latest/guides/registering-a-field-in-admin.html#setup)
- [Libreria semantic-ui](https://react.semantic-ui.com/)
- [Manejo/formateo de fechas](https://momentjs.com/)

<!--

#########################################

-->

----

<!--

#########################################

-->

## Pendientes

- [TODO: Leer para saber llamar servicio en CSR](https://www.gatsbyjs.com/docs/data-fetching/)

<!--

#########################################

-->

----

<!--

#########################################

-->

## Resultados Finales

### Proyecto: Introduccion

Vimos lo basico de gatsby como el uso de SEO, enlaces, imágenes optimizadas,
uso de graphQL, estilos locales y globales, agregar sass, librerías,
plugins, etc

- [Codigo](./img/_1_intro.zip)
- [Imagen](./img/_1_intro.PNG)

### Proyecto: Portafolio

Vimos que no ahi casi ninguna diferencia entre trabajar con gatsby y
trabajar con react, ademas de. 1) sea que usemos una variable javascript
para renderizar algo, `al final esta variable estara presente en nuestro html`,
2) Que gatsby `no puede usar window` pero podemos solucionarlo renderizando
el componente del lado del cliente para que funcione correctamente.

- [Codigo](./img/_2_portafolio.zip)
- [Imagen](./img/_2_portafolio.gif)

<!--

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, division de seccion 2

-->

## Introduccion

A, Instalar gatsby, instalar el CLI de gatsby

```r
npm install -g gatsby-cli
 o 
yarn add global gatsby-cli
```

B, Comprobar instalacion de Gatsby: `gatsby --version`

C, Crear proyecto

```r
gatsby new nombre
```

C, Iniciar proyecto; `gatsby develop`

**NOTA:** Podemos modificar el `package.json` para usar `yarn dev`

```json
"scripts": {
- "develop": "gatsby develop",
+ "dev": "gatsby develop",
```

<!-- construir para produccion, crear version de produccion, compilar -->

D, Construir proyecto: `yarn build`

**Estructura de Carpetas** Cuando recien creamos un proyecto tenemos
la siguiente estructura, lo cual es lo mismo que un proyecto react mas
otras cosillas como.

**Nota** Cada uno de los archivos `gatsby-xxx` tienen un enlace donde
extienden su explicacion.

- `gatsby-browser.js`: Permite responder a acciones dentro del navegador,
  y agregar/envolver a nuestra aplicaciones componentes, por ejemplo,
  agregar estilos globales

- `gatsby-config.js`: define los metadatos de nuestra aplicacion, y agrega
  complementos como son los plugins

- `gatsby-node.js`: Este script se ejecuta solo cuando levantamos el
  proyecto o hacemos un build, y se encagara de cosas como, crear las
  paginas a partir de un conjunto de datos.

- `gatsby-ssr.js`: Aqui esta la configuracion del servidor que se levanta
  cuando estamos en desarrollo

<!-- como funciona la navegacion -->
- `src\pages`: Aqui tenemos los archivos que igual son equivalentes a
  las rutas que son accesibles desde el navegador, por ejemplo:
  - `src\pages\index.js` sera igual a `ej.com/`
  - `src\pages\page-2.js` sera igual a `ej.com/page-2`
  - `src\pages\productos\index.js` sera igual a `ej.com/productos`

<!--

#########################################

-->

----

<!--

#########################################

-->

### 29. Uso de Layouts, Enlaces, estilos, SEO

<!-- Crear layout -->

**Objetivo**: Vamos a la base de nuestro proyecto empezando por el estos
elementos.

**Layout:** Funiona igual que en react.

**SEO:** Para el SEO ya tenemos instalado en `gatsby-config.js` el
plugin de `gatsby-plugin-react-helmet` que nos sirve para esto.

<!-- uso de graphql -->

A, Creamos nuestro componente que agregara nuestro SEO `src\components\utils\SEO.js`

```js
import React from "react";
// el que se encargara de agregar info en el <head>
import { Helmet } from 'react-helmet';
import { PropTypes } from 'prop-types'
import { useStaticQuery, graphql } from "gatsby";

export default function SEO(props) {
  const { pageTitle, lang, description, meta } = props;
// realizamos peticion
  const data = useStaticQuery(graphql`query basicSiteQuery1 {
    site { siteMetadata {
      siteTitle, description, author
    } } }`
  ).site.siteMetadata;
  const pageDescription = description || data.description;
  const title = `${pageTitle} | ${data.siteTitle} `;

//uso de seo
  return (
    <Helmet
      htmlAttributes={{
        lang: lang
      }}
      title={title}
      meta={[
// todas las meta-etiquetas que querramos, agregar seo etiquetas meta
        {name: 'description', content: pageDescription},
        {name: 'og:title', content: title},
        {name: 'og:description', content: pageDescription},
        {name: 'og:tyoe', content: 'website'},
        {name: 'twitter:creator', content: data.author},
        ...meta
      ]}
    />
  )
}
// definimos la estructura de los props de nuestro componente, para obligarnos
// a poner x datos o como documentacion, para los que no son obligatorios
// definir los props de componente
SEO.propTypes = {
  pageTitle: PropTypes.string.isRequired,
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
}

// datos por default si es que no definimos los opcionales
SEO.defaultProps = { description: '', lang: 'es', meta: [], }
```

B, Creamos layout.

**Nota**: La query de graphql podemos sacarla de `http://localhost:8000/__graphql`
como se ve abajo, los cuales en este caso corresponde con la info que
tenemos en `gatsby-config.js`

![01-de-ui-graphql](./img/01-de-ui-graphql.PNG)

```js
import React from "react" import { useStaticQuery, graphql, Link } from "gatsby";
//uso de css
import './LayoutBasic.css'

export default function LayoutBasic(props) {
  const { children } = props;
  const data = useStaticQuery(graphql`query basicSiteQuery2 { site { siteMetadata { siteTitle author } } }`).site.siteMetadata;
  const { siteTitle, author } = data;
  return (
    <>
      <header className="header">
        <h1><Link to="/">{siteTitle}</Link></h1>
{/* uso de links */}
        <nav><Link to="/productos">Productos</Link> <Link to="/about">About</Link> </nav>
      </header>
      <main>{children}</main>
      <footer style={{position: 'fixed', bottom: '0'}}>autor: {author}</footer>
    </>
  )
}

```

C, crear estilos

```css
.header { display: flex; justify-content: space-between; align-items: center; }
.header h1 a { text-decoration: none; color: black !important; }
.header nav { display: flex; list-style: none; }
.header nav a { padding-right: 1em; }
```

D, usar layout

```js
import React from "react"
import SEO from "../components/utils/SEO";
import LayoutBasic from "./../components/layouts/LayoutBasic";
export default function Index() {
  return (
<LayoutBasic>
  <SEO pageTitle="Home" />
  <h2>Home</h2>
</LayoutBasic>
  )
}
```

E, Podemos usar campos opcionales de nuestro SEO, por ejemplo.

```js
import React from "react"
import SEO from "../components/utils/SEO";
import LayoutBasic from "./../components/layouts/LayoutBasic";
export default function Index() {
  return (
<LayoutBasic>
  <SEO pageTitle="Productos" description="lista de prodcutos de madera" 
    meta={[ {name: 'keywords', content: 'madera, trompo, artesania'}, ]}
  />
  <h2>Productos</h2>
</LayoutBasic>
  )
}
```

**Resultado:** Tenemos listo un sitio muy basico, con uso de layout con
header y footer, enlaces, seo con metadados dinamicos que se agregan en
head, y estlos locales.

![02_sitio_basico](./img/02_sitio_basico.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 33. Añadiendo CSS Global, Sass, uso de Imagenes

**Objetivo:** Agregar estilos globales, en este caso vamos a agregar
estilos globales de bootstrap.

A, Uso de bootstrap

A,1, instalar bootstrap: `yarn add react-bootstrap bootstrap`

A,2, importamos en `gatsby-browser.js`: `import 'bootstrap/dist/css/bootstrap.min.css';`

A,3, Ya podemos usar lo en nuestros componetes.

```js
import { Button } from "react-bootstrap";
...
  <Button variant="primary">Primario</Button>
```

B, Uso de sass

<!-- instalar plugin, agregar sass -->

B,1, instalar sass `yarn add sass gatsby-plugin-sass`

B,1, Importamos en `gatsby-config.js`

```js
  plugins: [
+   `gatsby-plugin-sass`
  ],
```

B,2 Ya podemos agregar nuestros compoentes el scss

```js
import "./index.scss";

export default function Index() {
```

C, Uso de imagenes

**Nota:** Tenemos dos formas la forma nativa de react, y la forma de
gatsby, ambos nos van a funcionar, pero la de gatsby ya biene optimizada,
por ejemplo, varios tamaños para el responsive o la carga.

C,1, Forma de React

```js
import React from "react" import { Button } from "react-bootstrap"; import SEO from "../components/utils/SEO";
import LayoutBasic from "./../components/layouts/LayoutBasic";import "./index.scss";
//uso de imagenes
import ImgLogoGatsby from "../images/gatsby-icon.png";

export default function Index() {
  return (
<LayoutBasic>
  <SEO pageTitle="Home" />
  <h2>Home</h2>
  <Button variant="primary">Primario</Button>
{ /* forma de react */}
  <img src={ImgLogoGatsby} />
</LayoutBasic>
  )
}
```

<!-- Uso de imagenes optimizado por gatsby -->

C,2, Forma de Gatsby: **Nota** para esta notamos que ya tenemos 3 plugins
que nos ayudaran a hacer esto, que son `gatsby-source-filesystem` (para
tomar archivos del filesystem), `gatsby-transformer-sharp`, `gatsby-plugin-sharp`,

C,2,1, Instalamos `yarn add gatsby-image` la cual nos ayudara a usar
lo que nos llega de graphQl en una imagen.

C,2,2, Vamos a `http://localhost:8000/` y conseguimos la peticion graphql,
para conseguir las imagenes.

- Lo que hara la peticion es traer/generar las imagenes en diferentes
  tamaños para los diferentes dispositivos

```r
# traer todas las imagenes
query findAllFluidImages {
  allFile {
    edges { node { relativePath childImageSharp { fluid { srcSet } } } }
  }
}

# traer solo una imagen, pero creo no se puede
query findByNameFluidImages {
  allFile(filter: {relativePath: {eq: "gatsby-icon.png"}}) {
    edges { node { relativePath childImageSharp { fluid { srcSet } } } }
  }
}
```

C,2,3, Creamos el siguiente componente que nos ayudara a crear la imagen

```js
  import React from "react"
  import { StaticQuery, graphql } from "gatsby";
+ import Img from "gatsby-image"

  export default function Image(props) {
    const { imageName, alt } = props;
    
// preparamos peticion, que nos trasera todas las imagenes, ya que una peticion dinamica
// es un rollo
+   const query = graphql`
      query allFluidImages {
        allFile {
          edges { node { relativePath childImageSharp { fluid { ...GatsbyImageSharpFluid } } } } 
        }
      }
    `;

// este metodo filtrara todas las imagenes dejando solo la imagen que el usuario definio
// para luego mostrarla
    const renderImage = (respQuery) => {
        const image = respQuery.allFile.edges.find(image => {
!         return image.node.relativePath.includes(imageName)
        });
        console.log('image finded', image);
        if(!image) { return null; }
// notar que usamos componente que recien instalamos
!       return <Img alt={alt} fluid={image.node.childImageSharp.fluid} />
    }
    
// el StaticQuery nos permitira renderizar a partir de una peticion grafphQl
    return (
!     <StaticQuery query={query} render={renderImage} />
    );
  }
```

C,2,4, Mostramos la imagen a trabes de nuestro componente

```js
...
+ import Image from "../components/utils/Image";
  export default function Index() {
    return (
  <LayoutBasic>
    ...
+   <Image imageName="gatsby-icon.png" alt="logo de gatsby" />
  </LayoutBasic>
    )
  }
```

**Resultado:** Podemos hacer uso de imagenes de una forma facil y
optimizada.

![_1_intro](./img/_1_intro.PNG)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

-->

## Sección 5:PROYECTO: Curriculum web

En este proyecto presentaremos informacion guardada en json los cuales
al agregar info. en este json, podremos verlo reflejado en nuestra pagina,
en nuestro caso seran la sig. informacion.

- Datos de persona
  - edad
  - direccion
  - email
  - descripcion
  - C.V.
  - etc

- Redes sociales
  - Logo
  - URL

- Habilidades
  - categoria
  - nombre
  - dominio

- Proyectos
  - imagen de preview
  - nombre
  - descripcion
  - enlace

### Inicio

A, Crear proyecto: `gatsby new portafolio`

B, Instalar bootstrap y sass: `yarn add react-bootstrap bootstrap sass gatsby-plugin-sass`

- Importar Bootstrap eb gatsby-browser.js `import 'bootstrap/dist/css/bootstrap.min.css';`
- Importamos en gatsby-config.js: `plugins: [ ... 'gatsby-plugin-sass' ... ],`

C, Creamos La siguiente estrucura de carpetas

```r
# dentro de carpeta src/
components/
  layouts/
    BasicLayout/
      BasicLayout.js
      BasicLayout.scss
      index.js
  Menu/
    index.js
    Menu.js
    Menu.scss
images/
  gatsby-icon.png
pages/
  index.js
  proyects.js
  skills.js
```

D, crear menu de navegacion `src\components\Menu`

D,1, estilos

```scss
nav {
  z-index: 4; position: relative; display: flex; justify-content: center;
  a { color: white; margin: 0; padding: 0.4em 2em; font-weight: bold;
    &:hover { text-decoration: none; color: lightblue; }
  }
}
```

D,2, Componente

```js
import React from "react" import { Link } from "gatsby" import './Menu.scss'; import { Container } from "react-bootstrap";
const Menu = () => (
  <Container as="nav">
    <Link to="/">Sobre Mi</Link> <Link to="/skills">Skills</Link> <Link to="/proyects">Proyectos</Link>
  </Container>
)
export default Menu;
```

E, Crear Layout `src\components\layouts\BasicLayout`

E,1, Estilos

```css
header { background-color: darkgray; }
```

E,1, Compoenete

```js
import React from "react"import Menu from "../../Menu";import './BasicLayout.scss';
const BasicLayout = ({children}) => (
 <>
  <header><Menu /></header>
  <main>{children}</main>
  <footer>Pie</footer>
 </>
)
export default BasicLayout;
```

F, Crear paginas (el mismo contenido para los 3)

- `src\pages\index.js`
- `src\pages\proyects.js`
- `src\pages\skills.js`

```js
import React from "react"
import BasicLayout from '../components/layouts/BasicLayout';
const IndexPage = () => (
  <BasicLayout><h2>Sobre mi</h2></BasicLayout>
)
export default IndexPage;
```

**Resultado:** Tenemos una estructura basica para empezar nuestro proyecto

![03_inicio_portafolio](./img/03_inicio_portafolio.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 47. Componente Profile (imagen de perfil mas info basica)

**Objetivo:** Vamos a crear el siguiente diseño, el cual tendra la info
basica, mas la foto de perfil.

![04_imagen_de_perfil_mas_info_basica](./img/04_imagen_de_perfil_mas_info_basica.PNG)

A, Creamos el componente Profile

- Estilos

```css
.profile { position: relative; top: -5em; z-index: 0;
  .backgroundImage { background-image: url("../../images/code.jpg"); background-position: top; background-size: cover; width: 100%; height: 20em; }
  .dark { width: 100%; height: 20em; background-color: #000; position: absolute; top: 0; left: 0; opacity: 0.7; }
  .box { position: relative; margin-top: -13em; padding: 0; background-color: white; box-shadow:  0px 0px 15px 0px rgba(0,0,0,0.75); -webkit...; -moz...;
    .info { padding: 2em;
      .photo-profile { display: flex; justify-content: center; }
      &__data {
        > span:first-of-type { // ">" = seleccionar primer elemento display: inline-block; // usar solo el ancho del contenido background-color: cadetblue; font-size: 12px; font-weight: bold; color: white; padding: 5px 10px; border-radius: 4px; }
        .more_info {
          .item {
            span {
              &:first-of-type { text-transform: uppercase; font-weight: bold; max-width: 150px; display: inline-block; }
              &:last-of-type { color: gray; }
} } } } } } }
```

- Componente

```js
import React from "react"
//import { Link } from "gatsby"
import { Container, Row, Col, Image } from "react-bootstrap";
import './Profile.scss';
import profilePhoto from "./../../images/profile-photo.png"

const data = [
  {property: 'Telefono', value: '+52 241-234-56-78'}, {property: 'Email', value: 'mario@mail.com'},
  {property: 'Ubicacion', value: 'Tlaxco, Tlaxcala, Mexico'}, {property: 'Experiencia', value: '4 años'},
];

const Profile = () => (
  <div className="profile">
    <div className="backgroundImage" />
    <div className="dark" />
    <Container className="box">
      <Row as="article" className="info">
        <Col xs={12} md={4} className="photo-profile" ><Image src={profilePhoto} fluid /> </Col>
        <Col xs={12} md={8} className="info__data">
          <span>Desarrollador</span> <h2>Mario Marquez</h2> <span>Ing. En tecnologias de la Informacion y Comunicacion</span>
          <hr />
          <div className="more_info">
            {data
             ? data.map((item, index) => (
                <Row as="div" key={index} className="item">
                  <Col xs={12} sm={6} as="span">{item.property}: </Col>
                  <Col xs={12} sm={6} as="span">{item.value}</Col>
                </Row>
              ))
            : <p>Sin Informacion</p>
            }
          </div>
        </Col>
      </Row>
    </Container>
  </div>
)
export default Profile;
```

**Resultado:** Creamos el diseño del de este objetivo.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 50. Componente Social Media

<!-- uso de svg -->

Fuente

- [Plugin usar svg](https://www.gatsbyjs.com/plugins/gatsby-plugin-svgr/)

**Objetivo:** Vamos a agregar los iconos de redes sociales, para que al
darle click sobre ellos lleve al perfil de dicha red social.

A, Agregar soporte de SVG en gatsby

A,1, Instalar plugin: `yarn add @svgr/webpack gatsby-plugin-svgr`

A,2, Importamos plugin e le indicamos donde tenemos nuestros iconos

```r
# ./gatsby-config.js
'gatsby-plugin-svgr',
```

A,2, Usamo el svr

- Estilos

```css
.social { 
  svg {
    height: 1.5em; width: 1.5em; fill: black; margin: 10px;
    &:hover { opacity: 0.7; }
  }
}
```

- Componente

```js
import React from "react" import './SocialIcon.scss'; import { ReactComponent as IconWeb } from './../../../images/icons/web.svg';
// TAMBIEN SE PUEDE
// import twitterUrl from './../../../images/icons/twitter.svg'
import { ReactComponent as IconTwitter } from './../../../images/icons/twitter.svg'; import { ReactComponent as IconInstagram } from './../../../images/icons/instagram.svg';
import { ReactComponent as IconLinkedin } from './../../../images/icons/linkedin.svg'; import { ReactComponent as IconFacebook } from './../../../images/icons/facebook.svg';
export default function SocialIcon({name, url}) {
  let selectedIcon;
  switch (name) {
    case 'twitter':   selectedIcon = <IconTwitter />;   break;
    case 'instagram': selectedIcon = <IconInstagram />; break;
    case 'linkedin':  selectedIcon = <IconLinkedin />;  break;
    case 'facebook':  selectedIcon = <IconFacebook />;  break;
    case 'web':       selectedIcon = <IconWeb />;       break;
    default:          selectedIcon = <IconWeb />;       break;
  }
  return (
  <div className="social">
    <a href={url} target="_blank" rel="noopener noreferrer">
      {selectedIcon}
    </a>
  </div>)
}
```

B, Usamos nuestro compoente en `src\components\Profile\Profile.js`

- Estilos

```css
.socialnet { display: flex; flex-direction: row; justify-content: center; flex-wrap: wrap;
  div { margin-right: 1rem; }
}
```

- Compoente

```js
import SocialIcon from "./SocialIcon";
const socialNetworks = [
  {name: 'twitter', url: 'http://ej-a.com'}, {name: "instagram", url: 'http://ej-b.com'}, {name: "linkedin", url: 'http://ej2.com'},
  {name: "facebook", url: 'http://ej3.com'}, {name: "web", url: 'http://ej4.com'},
];
...
  <Col className="socialnet" xs={12}>
    {socialNetworks.map((item, index) => (
        <SocialIcon key={index} name={item.name} url={item.url} />
      ))
    }
  </Col>
```

**Resultado:** Tenemos la barra de iconos de redes sociales

![05_redes_sociales_barra](./img/05_redes_sociales_barra.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 55. Pagina de Habilidades

**Objetivo:** Crear el siguiente diseño

![06_pagina_habilidades](./img/06_pagina_habilidades.PNG)

A, instalar barras: `yarn add react-skills-bars`

B, Creamos la informacion que queremos mostrar: `src\components\data\skills.js`

```js
export const frontendSkills = [
  { type: "JavaScript", level: 95 }, { type: "React JS", level: 95 },
  { type: "CSS3", level: 50 }, { type: "Gatsby", level: 85 },
]
export const fontendSkillsColors = {
  "bar": "#3498db", "title": { "text": "#fff", "background": "#2980b9" },
}

export const backendSkills = [
  { type: "Node JS", level: 95 }, { type: "Express JS", level: 70 },
  { type: "PHP", level: 60 }, { type: "Python", level: 85 },
]
export const backendSkillsColors = {
  "bar": "#00bd3f", "title": { "text": "#fff", "background": "#009331" },
}

export const soSkills = [
  { type: "MacOS", level: 100 }, { type: "Linux", level: 95 }, { type: "Windows", level: 95 },
]
export const soSkillsColors = {
  "bar": "#f09c00", "title": { "text": "#fff", "background": "#b46900" },
}
```

B, Creamos el componente que mostrara un conjunto de habilidades

- Estilos

```scss
.list-skills {  
    .skillbar {
      width: 100%;
      @media (min-width:768px) { width: 48%; }
      display: inline-flex;
      &:nth-child(2n) { margin-right: 0; }
      .skillbar-percent { color: black; font-size: 14px; font-weight: bold; }
    }
}
```

- Componete

```js
import React from "react" import loadable from "@loadable/component"
import "./ListSkills.scss" import { Row, Col } from "react-bootstrap"
// ReferenceError: window is not defined
const SkillBar = loadable(() => import("react-skills-bars"))
export default function ListSkills(props) {
  const { skills, barColors } = props
  return (
    <div className="list-skills">
      <Row>
      <Col xs={12} sm={12} >
        <SkillBar skills={skills} colors={barColors} animationDelay={0} animationDuration={1500} />
      </Col>
      </Row>
    </div>
  )
}
```

C, Lo agregamos en nuestra pagina

```js
import React from "react" import { Container } from "react-bootstrap" import BasicLayout from "../components/layouts/BasicLayout"
import ListSkills from "../components/ListSkills"
import {
  frontendSkills, fontendSkillsColors,
  backendSkills, backendSkillsColors,
  soSkills, soSkillsColors,
} from "../components/data/skills"
import "./skills.scss"

export default function Index() {
  return (
    <BasicLayout menuColor="#000">
      <Container className="skills">
        <div className="skills__block"><h2>Frontend</h2><ListSkills skills={frontendSkills} barColors={fontendSkillsColors} /></div>
        <div className="skills__block"><h2>Backend</h2><ListSkills skills={backendSkills} barColors={backendSkillsColors} /></div>
        <div className="skills__block"><h2>Sistemas Operativos</h2><ListSkills skills={soSkills} barColors={soSkillsColors} /> </div>
      </Container>
    </BasicLayout>
  )
}
```

**Resultado:** Creamos el diseño de este objetivo (mostrado arriba).

<!--

#########################################

-->

----

<!--

#########################################

-->

### 56. Pagina de Proyectos

**Objetivo:** Vamos a mostrar un conjunto de proyectos, en el siguiente
diseño.

![07_pagina_proyectos.PNG](./img/07_pagina_proyectos.PNG)

A, Creamos datos `src\components\data\projectsData.js`

```js
import facebookImg from '...'; import googleImg from '...'; import instagramImg from '...'; 
import pinteresImg from '...'; import twitterImg from '...';
export const projectsList = [
  { image: facebookImg, name: "facebook", description: "Interdum...", url: "http://example.com" },
  { image: googleImg, name: "Google", description: "Pellentesque...", url: "http://example2.com" },
  { image: instagramImg, name: "Instagram", description: "Eget ...", url: "http://example2.com" },
  { image: pinteresImg, name: "Pinteres", description: "Et ...", url: "http://example2.com" },
  { image: twitterImg, name: "Twitter", description: "Que sit...", url: "http://example2.com" },
]
```

B, Creamos Componente para mostrar un proyecto `src\components\ProyectItem`

- css

```scss
.proyect-item { margin-bottom: 30px; box-shadow:  0px 0px 15px 0px rgba(0,0,0,0.50); min-width: 200px;
  .image { height: 180px; background-color: grey; }
  a { text-align: right; margin: 0 1rem 1rem 1rem;
    button { font-weight: bold; background-color: var(--primary); border-color: var(--primary); width: 100%; }
  }
}
```

- componente

```js
import React from "react"
import { Card, Button } from "react-bootstrap";
//import { Container, Row, Col, Image } from "react-bootstrap";
import './ProyectItem.scss';
const ProyectItem = ({image, name, description, url}) => (
  <Card className="proyect-item">
    <div className="image"> <img src={image} width="100%" height="100%" /> </div>
    <Card.Body>
      <Card.Title>{name}</Card.Title> <Card.Text>{description}</Card.Text>
    </Card.Body>
    <a href={url} target="_blank" rel="noopener noreferrer" > <Button> Ver Proyecto </Button> </a>
  </Card>
)
export default ProyectItem;
```

C, Mostramos los proyecto `src\pages\proyects.js`

- css

```scss
.projects-container { h2 { margin: 50px 0 20px 0; } }
```

- componente

```js
import React from "react" import { Container, Row, Col } from "react-bootstrap";
import BasicLayout from '../components/layouts/BasicLayout';
import "./proyects.scss" import ProyectItem from "./../components/ProyectItem";
import { projectsList } from './../components/data/projectsData'
const ProyectsPage = () => (
  <BasicLayout menuColor="#020">
    <Container className="projects-container">
      <h2>Proyectos</h2>
      <Row>
        {projectsList.map((p, i) => (
          <Col xs={12} sm={5} lg={4}>
            <ProyectItem key={i} image={p.image} name={p.name} description={p.description} url={p.url} />
          </Col>
        ))}
      </Row>
    </Container>
  </BasicLayout>
)
export default ProyectsPage;
```

**Resuoltado:** Creamos el diseño de este objetivo.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 57. Compilando aplicacion

<!--
Error: ReferenceError: window is not defined "evitar problemas con variable window"
-->

- Fuente
  - [Plugin evitar problemas con variable window](https://yarnpkg.com/package/@loadable/component)
  - Para compilar usamos: `yarn build`

**Importante:** Como Gatsby al generar los estaticos (con node) no cuenta
con la variable global window, lo cual los lugares donde usemos esta va
a fallar, por lo que debemos de usar estos componentes, con un envoltorio
como lo haremos abajo.

A, instalar: `yarn add @loadable/component`

B, Lo usamos en el componente que da problemas en mi caso.

- Cambiamos la forma en como importamos el componete y listo.

```js
// eliminamos la forma anterior de importarlo
- //import SkillBar from "react-skills-bars"
//usamos esta forma, que lo que hara es construirlo en CSR
+ const SkillBar = loadable(() => import("react-skills-bars"))
  export default function ListSkills(props) { const { skills, barColors } = props
    return (
      <div className="list-skills"><Row>
        <Col xs={12} sm={12} >
          <SkillBar skills={skills} colors={barColors} animationDelay={0} animationDuration={1500} />
        </Col>
        </Row>
      </div>
    )
  }
```

**Resultado:** Podemos generar nuestra aplicacion sin problemas.

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

-->

## PROYECTO: Blog + Strapi + GraphQL

Este blog le podremos agregar contenido desde Strapi, con el uso de una
UI, que nos permitira agregar los diferentes campos y multimedia, y el
blog constara de una lista de todos los post con paginacion, en la cual
podremos entrar a cada uno de ellos, y estos podran conteneder estilos
y multimedia.

### 67. Prerar Strapi como Backend

<!-- editor de editor de texto -->

Fuente:

- [Editor del editor de texto enriquesido](https://ckeditor.com/ckeditor-5/online-builder/)
- [Agregar CKEditor en Strapi](https://strapi.io/documentation/developer-docs/latest/guides/registering-a-field-in-admin.html#setup)

A, Crear bd en mongo en mi caso la cree con MongoDB Compass Community
y al final pude conectarme a `mongodb://localhost:27017/blog_m51_db_tes`.

- NOTA: En mi caso use sqlite para rapido

B, Crear backend con Strapi: `npx create-strapi-app blog51_back`

C, Habilitamos GraphQL en Strapi `StrapiUI -> tienda` donde deberiamos de poder
ver la opcion de instalar graphQL,

- Al terminar de instalar deberiamos de poder entrar a `http://localhost:1337/graphql`

<!-- crear modelos en strapi -->

D, Vamos a `StrapiUI -> Creador de tipo de contenidos -> crea un tipo de coleccion`

D,1, Le damos de nombre post, con los siguientes datos.

![08_creacion_de_modelo](./img/08_creacion_de_modelo.PNG)

D,2, Vamos a configuracion -> permisos y le dejamos como publicas el de
count, find y findOne

E, (Opcional) Modificamos el editor de tecto erriquesido

**Resultado** Tenemos losto Strapi para hacerle peticiones graphQL y que
estas nos retorne los post que creamos.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 68. Conectando Strapi con Gatsby

- Fuente
  - [Conectar Strapi con Gatsby](https://www.gatsbyjs.com/plugins/gatsby-source-strapi)

A, Instalar Plugin: `yarn add gatsby-source-strapi`

B, Agregamos la siguiente configuracion en `gatsby-config.js`

```js
{
  resolve: `gatsby-source-strapi`,
  options: {
    apiURL: `http://localhost:1337`,
// limite de peticiones que hara al construirse, si tenemos 1001 el ultimo no se renderizara
    queryLimit: 1000, 
//PONEMOS EL NOMBRE DE NUESTRA COLECCION
    contentTypes: [`post`],
//If using single types place them in this array.
    //singleTypes: [`home-page`, `contact`],
// Por si tenemos protigida la coleccion
    // loginData: {
    //   identifier: "",
    //   password: "",
// },
  },
}
```

C, Ya deberiamos de poder hacer la siguiente consulta en `__graphql`

```r
query miQuery {
  allStrapiPost {
    nodes {
      titulo
    }
  }
}
```

**Resultado:** Ya podemos traer los posr de Strapi a Gatsby

<!--

#########################################

-->

----

<!--

#########################################

-->

### 70. Uso de Sass global

<!-- Agregar Sass global -->

**Objetivo:** Agregar un sass donde tengamos los etilos, los cuales se
apliquen a toda nuestra pagina.

A, Agregamos los siguientes archivos, el cual solo importaremos `globals.scss`
y este contendra los demas archivos segmentados.

```scss
// src\components\global\styles\breacks-points.scss
$breakpoint-up-sm: "576px"; $breakpoint-up-md: "768px"; $breakpoint-up-lg: "992px"; $breakpoint-up-xl: "1200px";
$breakpoint-down-xs: "575.98px"; $breakpoint-down-sm: "767.98px"; $breakpoint-down-md: "991.98px"; $breakpoint-down-lg: "1999.98px";

//src\components\global\styles\colors.scss
$font-grey: #777; $font-dark: #000; $background-grey-light: #f4f4f4;
$youtube: #c4302b; $twitter: #00acee; $instagram: #c13584; $facebook: #3b5998; $linkedin: #0e86a8;

//src\components\global\styles\index.scss
@import "./colors.scss";
@import "./breacks-points.scss";

//src\components\global\styles\globals.scss
@import "./index.scss";
p {
  color: $font-grey;
}
```

B, Importamos en `gatsby-browser.js`

```js
import './src/components/global/styles/globals.scss';
```

**Resultado:** Sin tenener que importar nuestros estilos globales a
nuestros compontes, estos e aplicaran.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 71. Agregar SemanticUI

<!-- uso de semanticUI -->

- Fuente
  - [Libreria semantic-ui](https://react.semantic-ui.com/)

A, Instalar semantic-ui: `yarn add semantic-ui-react semantic-ui-css`

B, importamos en `gatsby-browser.js`

```js
import 'semantic-ui-css/semantic.min.css'
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 72. CODIGO: Crear codigo base de nuestro proyecto

**Objetivo** Vamos a crear la plantilla que compartiran todos nuestros
posts, la cual lucira como la siguiente y tendran scrolls diferentes,
los cuales no sera el sroll del navegador.

![09_blogs_estructura_inicial](./img/09_blogs_estructura_inicial.PNG)

- El codigo esta en este comprimido.

- ![09_blogs_estructura_inicial](./img/_3_z_m5blog_inicial.zip)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 77. Obteniendo todos los post del blog

**Objetivo:** Vamos a hacer la peticion para tener la info de todos
los posts, y luego a partir de esta generar todos los estaticos.

A, Hacer peticion simple

```js
+ import { useStaticQuery, graphql } from "gatsby"
  export default function IndexPage() {
// consultar todos los registros de una coleccion
+   const result = useStaticQuery(graphql`
+     query allPosts {
+       allStrapiPost {
+         nodes {
+           titulo url seo_title description_seo description updated_at
+         }
+       }
+     }
+   `);
//podemos ver todos los post que tengamos
+   console.log('Peticion: ', result);

    return(
    <BasicLayout title="Home" >
      <h1>Home</h1>
      
    </BasicLayout>
    );
  }
```

**Problema:** Si los dejamos asi nos enfrentamos a pintar muchos posts,
en nuestra pagina, por lo que tambien debemos de implementar

<!--

#########################################

-->

----

<!--

#########################################

-->

### 78. Creacion de paginas dinamicas: PAGINACION

<!-- uso de paginacion, generar paginacion estatica -->

**TEORIA:** Gatsby para las paginas generadas de forma dinamica usa
templates, las cuales seran usadas para generar las paginas a partir
de un conjunto de informacion, lo cual usaremos lo siguiente.

- **templates/**: En esta carpeta se deben de poner la plantillas usadas
  para generar las paginas.
- **gatsby-node.js**: Aqui meteremos la configuracion de como se haran,
  por ejemplo, la consulta de datos y que template usar.

A, Instalamos: `yarn add gatsby-awesome-pagination`

- En mi caso se me agrego `"gatsby-awesome-pagination": "^0.3.6",`

B, Creamos nuestro template `src\templates\BlogTemplate.js`. es importante ver
el siguiente codigo para entender ambas partes

```js
import React from 'react'
import { graphql } from "gatsby"

export default function BlogTemplate(props) {
  const { data } = props;
// mas adelante veremos como el plugin envia info en props,
// de los cuales nos servira el limit, skipt, para hacer la peticion
// dentro de este componente, y otra util para la paginacion, por ejemplo,
// el total de paginas, etc.
  const posts = data.allStrapiPost.nodes;
  console.log(`Algo:`, posts);
  return (
    <div>
      <h1>Mi template</h1>
      {posts.map((p, i) => (
        <div>
          <p>
            Titulo: {p.titulo} <br />
            Url: {p.url}
          </p>
          <br />
        </div>
      )) }
    </div>
  )
}

// los datos de $skip y $limit nos lo manda el plugin de paginacion y el
// resultado llegara como props.data a nuestro este componete.
export const query = graphql`
  query($skip: Int!, $limit: Int!) {
      allStrapiPost(
        skip: $skip
        limit: $limit
        sort: {fields: created_at, order: DESC}
      ) {
        nodes {
          url seo_title description_seo description updated_at miniature { publicURL }
        }
      }
    }
`;
```

B, Creamos la sig. funcion en `gatsby-node.js` para crear los posts.

1. hacemos la consulta
2. Le pasamos el array de info de la consulta al plugin

```js
//: https://www.gatsbyjs.com/docs/node-apis/

const path = require('path');
const { paginate } = require('gatsby-awesome-pagination');

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions;

// Realmente no usaremos esta informacion, ya que solo nos interesa saber
// el numero de posts para que el plugin pueda trabajar, ya que con eso
// sabra calcular el skipt y el limit para enviarselo a `BlogTemplate`
// cada vez que lo use.
  const allPosts = await graphql(`
    query findllPosts {
      allStrapiPost(sort: {fields: created_at, order: DESC}) {
        nodes {
          url
        }
      }
    }
  `);
  const postList = allPosts.data.allStrapiPost.nodes;

  // Create your paginated pages
  paginate({
// The Gatsby `createPage` function
    createPage,
// An array of objects
    items: postList,
// How many items you want per page
    itemsPerPage: 5, 
// definimos en que ruta estara nuestro post
    pathPrefix: '/',
// indicamos que plantilla vamos a usar
    component: path.resolve('./src/templates/BlogTemplate.js'),
  })
}
```

**Resultado:** Podemos ver como la informacion de los posts, en la raiz
`/` por que en mi caso ahi espesifique que se generara, **CUIDADO**
la ruta `/1` no existira ya que es equivalente a la `/` se saltara a
la ruta `/2` para mostrar los siguientes resultados.

![10_blogs_paginacion](./img/10_blogs_paginacion.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 81. Agregando estilos a la paginacion y formatear fecha

- Fuentes
  - [Manejo/formateo de fechas](https://momentjs.com/)

**Objetivo:** Ya tenemos lel listado basico de posts, vamos a agregarle
estilos y el componente para mostrar cada uno de estos.

A, Instalamos moment que nos ayudara a formater la fecha: `yarn add moment`

B, Agregamos El que dibujara una blog: `src\components\BlogList\PostItemList`

- Estilos

```scss
.post-item-list { border: 1px solid black;
  .card { width: 100%;
    .extra .meta {
//separar el icono del texto de fecha
      i { margin-right: 10px; }
    }
  }
  .image-post { height: 120px; background-repeat: no-repeat; background-position: center; background-size: cover; }
}
```

- Componente

```js
  import { Link } from 'gatsby'; import React from 'react' import { Card, Icon } from 'semantic-ui-react';import './PostItemList.scss'
//importamos moment para manejar las fechas
  import moment from 'moment';
// por defecto va a mostrar en ingles, y con esto se mostraran en español
  import 'moment/locale/es'; 

  export default function PostItemList({post}) {
    return (
      <Link to={post.url} className='post-item-list'>
        <Card>
          <div className="image-post" style={{  backgroundImage: 'url("' + post.miniature.publicURL + '")', }} />
          <Card.Content> <Card.Header><h3>{post.titulo}</h3></Card.Header> </Card.Content>
          <Card.Content extra>
  //formatear fecha
            <Card.Meta> <Icon name="calendar alternate outline" /> {moment(post.created_at).format('LL')} </Card.Meta>
          </Card.Content>
        </Card>
      </Link>
    )
  }
```

C, Agregamos el componente que mostrara la lista de posts: `src\components\BlogList`

- Estilos

```scss
.blog-list { border: 1px solid black; /* centrar lista de posts */ justify-content: center; }
```

- Componente

```js
  import React from 'react' import { Grid, } from 'semantic-ui-react'; import './BlogList.scss';
+ import PostItemList from './PostItemList';
  export default function BlogList({posts}) { 
    return (
      <Grid className="blog-list">
        {posts.map((p, i) => (
          <Grid.Column mobile={16} tablet={8} computer={5} >
+           <PostItemList key={i} post={p} />
          </Grid.Column>
        )) }
      </Grid>
    )
  }
```

D, Usamos la lista de compoentes en nustra pagina

```js
  import React from 'react' import { graphql } from "gatsby" import BasicLayout from './../components/global/BasicLayout';
  import BlogList from './../components/BlogList';

  export default function BlogTemplate(props) {
    const { data } = props; const posts = data.allStrapiPost.nodes;
    return (
+     <BasicLayout title="Todos los Blogs" > <h1>Mi template</h1>
+       <BlogList posts={posts} />
+     </BasicLayout>
    )
  }
  export const query = graphql`...`;
```

**Resultado:** Tenemos nuestra lista de posts con estilos, nos falta
agregar que cada uno de estos posts exista y la paginacion.

- [11_blogs_paginacion_estilos](./img/11_blogs_paginacion_estilos.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 83. Componente para movernos entre las páginas

<!-- navegar para recorrer una paginacion -->

**Introduccion:** El objeto que nos pasa el plugin de paginacion a `BlogTemplate`,
ya tiene la url para ir al siguiente y/o anterior pagina, y tambien el
numero de paginas, con esto ya podemos trabajar.

A, Creamos compoente `src\components\PaginationNavigator`, al cual le pasamos
el objeto que nos da el plugin de paginacion

- Estilos

```scss
.pagination-navigator { text-align: center; display: flex; justify-content: space-between; margin-left: 3rem; margin-right: 3rem;
  span a { font-size: 20px; margin-right: 1rem; }
}
```

- Componente

```js
import React from 'react' import { Link } from 'gatsby' import './PaginationNavigator.scss'
export default function PaginationNavigator({ pageContext }) {
// creamos la lista de pagina
  let pagesList = [];
  for (let i=0; i<pageContext.numberOfPages; i++) {
    let url = (i+1==1) ? '/' : `/${i+1}` ;
    pagesList.push({number: i+1, url: url});
  }
  return (
    <div className="pagination-navigator">
// pintamos el enlace de anterior (si existe)
      { pageContext.previousPagePath ? <Link to={pageContext.previousPagePath}>Anterior</Link> : <span style={{color: 'gray' }}>Anterior</span> }
      <div>
        {pagesList.map((u, i)=>( <span> <Link to={u.url} key={i}>{u.number}</Link> {(i!=pagesList.length-1) && ' | '} </span> ))}
      </div>
// pintamos el enlace de seguientes (si existe)
      { pageContext.nextPagePath ? <Link to={pageContext.nextPagePath}>Siguiente</Link> : <span style={{color: 'gray' }}>Siguiente</span> }
    </div>
  )
}
```

B, Usamos el compoente en el componente que usa el plugin

```js
export default function BlogTemplate(props) {
  const { data, pageContext } = props;
  const posts = data.allStrapiPost.nodes;
  return (
    <BasicLayout title="Todos los Blogs" >
      <h1>Mi template</h1> <BlogList posts={posts} />
+     <PaginationNavigator pageContext={pageContext} />
    </BasicLayout>
  )
}
```

**Resultado:** Tenemos la siguiente paginacion

![12_blogs_paginacion](./img/12_blogs_paginacion.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 84. Generando una pagina por entrada en el blog

<!-- generar pagina estatica -->

- Fuente:
  - [Pasar variable a query](https://stackoverflow.com/questions/61977002/gatsbyjs-adding-variables-to-graphql)

**Introduccion** Hasta ahora los enlaces de cada post no llevan a ningun
lado, por lo que tenemos generar cada una de estas paginas, para que tengan
a donde ir estos enlaces.

**Objetivo:** Lo que haremos es recorrer todos los posts (que obtubimos
anterior en la query) y por cada uno de ellos usamos la funcion `createPage`,
pasandole un post el que contiene la URL la cual usamos para consultar
toda su info despues en el componente.

A, Creamos funcion para crear cada posts `gatsby-node.js`

```js
// necetiramos `path` para seleccionar el componente tempate
! const path = require('path');
  const { paginate } = require('gatsby-awesome-pagination');
  const BLOG_PAGINATION_TEMPLATE        = './src/templates/BlogListTemplate.js';
  const BLOG_PAGINATION_PATH            = '/';
  const BLOG_PAGINATION_AMOUNT_PER_PAGE = 5;
// el template que usaremos para generar cada uno de los posts
+ const BLOG_DETAILS_TEMPLATE           = 'src/templates/BlogDetailsTemplate/BlogDetailsTemplate.js';

  const getAllPosts = async (graphql) => {
    const allPosts = await graphql(` query findllPosts { allStrapiPost(sort: {fields: created_at, order: DESC}) { nodes { url } } } `);
    return allPosts.data.allStrapiPost.nodes;
  }
  const generatePostpagination = (postList, createPage) => {
    paginate({ createPage, items: postList, itemsPerPage: BLOG_PAGINATION_AMOUNT_PER_PAGE, pathPrefix: BLOG_PAGINATION_PATH, component: path.resolve(BLOG_PAGINATION_TEMPLATE), })
  }

//con esta funcion createmos los posts, donde necesitamos pasarle
// url: en donde estara nuestro post
// ID: en este caso el slug (recomendable): para buscar toda la info del post dentro del componente
// Componente: La platilla que usaremos para crear el post
+ const generatePostsDetailsPage = (postList, createPage) => {
+   postList.forEach(post => {
+     createPage({
+       path: `/${post.url}`,
+       component: path.resolve(BLOG_DETAILS_TEMPLATE),
+       context: {
+         data: post,
+         slug: post.url,
+       }
+     });
+   });
+ }

  exports.createPages = async ({ actions, graphql }) => {
    const { createPage } = actions;
    const postList = await getAllPosts(graphql);
    generatePostpagination(postList, createPage);
// usamos la funcion
+   generatePostsDetailsPage(postList, createPage)
  }
```

B, Creamos componente `src\templates\BlogDetailsTemplate\` que hara la
peticion para obtener toda la info, de un post, y renderizar la informacion
que conseguimos.

- Componente

```js
  import React from 'react'
  import BasicLayout from '../../components/global/BasicLayout';

  export default function BlogDetailsTemplate(props) {
    //conseguimos la info que nos llega de la peticion
    const { strapiPost: post } = props.data;
    return (
      <BasicLayout title="Todos los Blogs" > <h1>{post.titulo}</h1> </BasicLayout>
    )
  }

//consultamos toda la info del post que nos llego
+ export const query = graphql`
+   query getPostbyUrl($slug: String!) {
+     strapiPost(url: {eq: $slug}) {
+       titulo url seo_title created_at updated_at miniature { publicURL } description_seo description
+     }
+   }
+ `;
```

**Resultado:** Generamos cada uno de los posts y ahora podemos dar click,
sobre cada uno de ellos de la lista y nos llevara a la pagina que le
corresponde.

![Imagen](./img/13_blogs_generacion_de_posts.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 85. Agregar estilos y pintando toda la info en PostDetails

PENDIENTE    

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

-->

## Por el Camino

### gatsby-node

#### Usar archivo json (en filesystem) para generar paginas

- Fuente: [dev.to](https://dev.to/ashleemboyer/build-a-multi-page-gatsby-site-from-json-3kp)

Podemos importarlo directamentem, sin tener que hacer uso de extenciones
ni tecnicas raras.

```js
  const path = require('path');
// solo con esto ya lo tenemos disponible
+ const products_all = require(`./src/data/products.json`);
  const BLOG_DETAILS_TEMPLATE = 'src/templates/detalles-producto.js';

  exports.createPages = async ({ actions, graphql }) => {
    const { createPage } = actions;
//lo paso/ntercambio entre variables po si a futuro cambio la fuente
!   const allProductsResp = products_all;
    generateProductsDetailsPage(allProductsResp, createPage)
  }

  const generateProductsDetailsPage = (produtList, createPage) => {
!   console.log('Total: ', produtList.lenth);
!   produtList.forEach(product => {
      let endpoint = getUrlFormat(product.name);
      createPage({
        path: `productos/${endpoint}`,
        component: path.resolve(BLOG_DETAILS_TEMPLATE),
        context: { data: product, slug: endpoint, }
      });
      console.log("Generando: " + endpoint);
    });
  }

  function getUrlFormat(name) {
    let urlFormat = '';
    name = name.toLowerCase();
    urlFormat = name.replace(/\s/g,"-"); // sustituir espacios blancos
    return urlFormat;
  }
```
