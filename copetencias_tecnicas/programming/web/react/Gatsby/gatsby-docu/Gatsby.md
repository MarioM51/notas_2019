# JAMStack

## Indice

- [JAMStack](#jamstack)
  - [Indice](#indice)
  - [Introduccion](#introduccion)
    - [Que es el JAMStack](#que-es-el-jamstack)
    - [Beneficios del JAMStack](#beneficios-del-jamstack)
    - [Que proyectos son aptos para JAMStack](#que-proyectos-son-aptos-para-jamstack)
    - [Que proyectos NO son aptos para JAMStack](#que-proyectos-no-son-aptos-para-jamstack)
  - [QuikStart](#quikstart)
  - [Guia](#guia)
    - [Inicio](#inicio)
    - [Crear paginas](#crear-paginas)
    - [Crear sub-componentes, crear header](#crear-sub-componentes-crear-header)
    - [Enlaces entre paginas \<Link\>](#enlaces-entre-paginas-link)
    - [Desplegar](#desplegar)
    - [Que aprendimos en Guia](#que-aprendimos-en-guia)
  - [Introduction to Styling in Gatsby](#introduction-to-styling-in-gatsby)
    - [Usar estilos globales](#usar-estilos-globales)
    - [Modulos CSS](#modulos-css)
    - [Dar estilo a componente con Modulos css](#dar-estilo-a-componente-con-modulos-css)
    - [Que aprendimos en Styling in Gatsby](#que-aprendimos-en-styling-in-gatsby)
  - [Crear componentes Layout anidados](#crear-componentes-layout-anidados)
    - [Uso de Plugins (Typography.js)](#uso-de-plugins-typographyjs)
    - [Creacion de componente Layout](#creacion-de-componente-layout)
    - [Que aprendimos en Crear componentes Layout anidados](#que-aprendimos-en-crear-componentes-layout-anidados)
  - [Data in Gatsby](#data-in-gatsby)
    - [Tu primer consulta GraphQL](#tu-primer-consulta-graphql)
    - [Usar una StaticQuery](#usar-una-staticquery)
    - [Que apendimos](#que-apendimos)
  - [Plugins de Source y renderizando peticiones de data](#plugins-de-source-y-renderizando-peticiones-de-data)
    - [Source plugins](#source-plugins)
    - [Construir pagina con consulta](#construir-pagina-con-consulta)
    - [Que aprendimos en Source y renderizando](#que-aprendimos-en-source-y-renderizando)
  - [Transformer plugins](#transformer-plugins)
    - [Crear lista de blogs](#crear-lista-de-blogs)
    - [Que aprendimos en Transformer plugins](#que-aprendimos-en-transformer-plugins)
  - [Crear paginas de los datos programaticamente](#crear-paginas-de-los-datos-programaticamente)
    - [Creando rutas tontas=slugs](#creando-rutas-tontasslugs)
    - [Creadon paginas](#creadon-paginas)
    - [Que aprendimos en crear paginas de los datos programaticamente](#que-aprendimos-en-crear-paginas-de-los-datos-programaticamente)
  - [Preparing a Site to Go Live](#preparing-a-site-to-go-live)
    - [Add a manifest file](#add-a-manifest-file)
    - [Using gatsby-plugin-offline](#using-gatsby-plugin-offline)
    - [Add page metadata wiht React Helmet and gatsby-plugin-react-helmet](#add-page-metadata-wiht-react-helmet-and-gatsby-plugin-react-helmet)
    - [Que aprendimos en Preparing a Site to Go Live](#que-aprendimos-en-preparing-a-site-to-go-live)
  - [Plugins y Temas](#plugins-y-temas)
    - [Creating an Image Source Plugin](#creating-an-image-source-plugin)
      - [Agregar nuestro plugin a gatsby](#agregar-nuestro-plugin-a-gatsby)
      - [Convertir resultado de la peticion a Nodos](#convertir-resultado-de-la-peticion-a-nodos)
      - [Que aprendimos en Creating an Image Source Plugin](#que-aprendimos-en-creating-an-image-source-plugin)
    - [Creating a Remark Transformer Plugin](#creating-a-remark-transformer-plugin)
    - [Using a Theme](#using-a-theme)
      - [Using Multiple Themes Together](#using-multiple-themes-together)
      - [Shadow components](#shadow-components)
      - [Add another theme](#add-another-theme)
      - [Add a navigation menu](#add-a-navigation-menu)
    - [Building a Theme](#building-a-theme)
      - [Create a data directory using the onPreBootstrap lifecycle](#create-a-data-directory-using-the-onprebootstrap-lifecycle)
      - [Set up to create data-driven pages](#set-up-to-create-data-driven-pages)
    - [working withimages](#working-withimages)
      - [Importar con webpack (imagen como componente)](#importar-con-webpack-imagen-como-componente)
      - [Referencia a static folder](#referencia-a-static-folder)
      - [Consultando al sistema graphql de gatsby](#consultando-al-sistema-graphql-de-gatsby)
      - [Traer multiples imagenes](#traer-multiples-imagenes)
  - [Mis Practicas](#mis-practicas)
    - [Hacer consumo de API Rest para Data de gatsby](#hacer-consumo-de-api-rest-para-data-de-gatsby)
    - [Leer archivo json para Data de gatsby](#leer-archivo-json-para-data-de-gatsby)

## Introduccion

- Demo: mario51.surge.sh
- Codigo: [https://gitlab.com/MarioM51/hola-gatsby](gitlab) -> hello-gatsby
- Compu: cursos\0activ\web\gasby

### Que es el JAMStack

JAMStack (Javascript APIs Markdown Stack) esta conformado de 3
conformado por los 3 pilares de su propio nombre, no tenemos
una logica del lado del back-end, tambien son conocidos como
static site generators (Generadores de sitios estaticos) y
como su nombre lo indica generan (o compilan) sitios estaticos
para que solo se entregen estos, y ya no ahiga una construccion
de las paginas cada peticion, si no cada vex que se agrege
informacion.

- Javascript: Este se encarga de toda la logica de la
  applicacion.

- APIs: Todos los servicios que necesitemos de un backend
  los solicitamos por medio de APIs

- Markdown: El markdown es pre-compilado para generar las
  vistas el cual se compila antes de subir a la web, en
  lugar de, a la hora de que el usuario haga una peticion
  
**Direfencias con el las aplicaciones SSR,** Estas cada
ver que el usuario hace una peticion se genera la vista
como vemos en la imagen.

![01_arquitectura_SSR](./img/01_arquitectura_SSR.PNG)

**La arquitectura JAMStack** en cambio primero ocurre una
**unica compilacion**, para generar todos los archivos, que
el usuario pueda llegar a requerir.  

![02_arquitectura_JAMStack](./img/02_arquitectura_JAMStack.PNG)

**Diferentes herramientas** Tenemos diferentes 3 procesos en lo
que es JAMStack que  es el.

1. Generate: Estos son los diferentes motores para construir,
   los cuales podemos ver en la tabla.

2. Buils. EL Proceso de compilacion puede ocurrir ya sea en
   nuestro PC o en un servidor, a esta capa le llamamos Build
   en la que esta conformado de las siguientes herramientas.

3. Host: Para subir nuestros archivos HTML ya generados podemos usuar
   basicamente cualquier servidor, pero tenemos otros que estan
   mas optimizados, como lo son.

| Generate | Build             | Host            |
| -------- | ----------------- | --------------- |
| Gatsby   | Netlify           | Netlify         |
| Next     | Zeit              | Zeit            |
| Jenkyll  | Github actions    | Github Pages    |
| Nuxt     | CI/CD             | Gatsby          |
|          | Personal Computer | FTP tradicional |

### Beneficios del JAMStack

**Mas Seguras** Como la compilacion de nuestas vistas ocurre
mientras la compilacion, que es un ambiente mas controlado y
los usuarios no pueden intervenir en este ya que **no manejamos**
**entradas del usuario**, En cambio en una arquitectura SSR
usamos informacion probeniente del usuario,argumentos, parametros,
cookies, etc.

**Mar rapido** Ya que el trabajo del servidor se reduce al
envio de archivos.

**Respuestas completas** En cambio las CSR (client-side rendering)
solo se envian respuestas parciales, osea se envia la aplicacion
pero no la informacion, esa la obtiene despues.

### Que proyectos son aptos para JAMStack

Los proyectos que van bien con el JAMStack son los que no se
actualizan muy seguido, o cuyas actualizaciones van 1 a 1 con
una nueva pagina **por ejemplo**, genero un nuevo contenido se
genera una nueva pagina.

Segun estudios a **Amazon** le cuesta al rededor de 1M de
dolares por cada segundo adicional de carga

Ejmplos de sitios que funcionan bien con JAMStack

- Blogs
- Catalogos / Tiendas Online
- Landing Pages: Existen varios casos en el que desde estas
  los usuarios son **redirigidos a un stack diferente**
  ejemplo Spotify
- Paginas de documentacion: Ejemplo la documentacion oficial
  de React, esta en JAMStack.

### Que proyectos NO son aptos para JAMStack

Los proyectos que no son recomendables para el JAMStack son
los que complen con las siguientes caracteristicas.

1. Existen muchas paginas
2. Existen muchas actualizaciones

Ahi que recordar que cada vez que se actualiza la informacion
existe una actualizacion (todas las posibles paginas son
generadas de nuevo) lo que conlleva dependiendo de la cantidad,
este proceso proceso (**arriba de millones**) puede volverse
muy lento, por ejemplo.  

- **Wikipedia**: Esta tiene millones de paginas y millones de
  actualizaciones

Otro caso son si el proyecto depende de variables que no se
puden predecir, como lo es sesiones, preferencias contenido
muy personalizado, este tipo de paginas no se pueden
precompilar, por ejemplo.

- **Facebook** en este todos los usuarios tienen una vista
  diferente, de acuerdo a sus preferencias, por lo que estas
  no se pueden precompilar.

Un **enfoque de solucion** para este tipo de paginas es que,
el usuario entre a su pagina parsonalizada con un SSR o CSR,
y de esta pasen al JAMStack.

## QuikStart

debemos de tener intalado node con npm
<!-- instalar gatsby instalacion gatsby instalacion de gatsbyjs instalacion -->
Para instalar globalmente: ```npm install -g gatsby-cli```

<!-- crear proyecto gatsby, iniciar proyecto-->
Crear nuevo proyecto ```gatsby new my-site https://github.com/gatsbyjs/gatsby-starter-hello-world```

Iniciar proyecto en desarrollo: ```gatsby develop```

Para Crear proyecto listo para produccion: ```gatsby build```

Para publicar lo ya construido a produccion: ```gatsby serve```

## Guia

- Fuente: [part-one](https://www.gatsbyjs.org/tutorial/part-one/)

### Inicio

Creamos un nuevo proyecto con el template de hello word

```bash
gatsby new hello-world https://github.com/gatsbyjs/gatsby-starter-hello-world
```

Gatsby usa React y para construir sus componentes con una sintaxis JSX se
usa de la siguiente manera, si abrimos el archivo *src/pages/index.js*
de nuestro nuevo proyecto veremos

<!-- hola mundo, pagina basica, pagina inicio, index sencillo, pagina sencilla-->
```js  
Copysrc/pages/index.js
import React from "react"
export default () => (
  <div style={{ color: `purple` }}>
    <h1>Hello Gatsby!</h1>
    <p>What a world.</p>
    <img src="https://source.unsplash.com/random/400x200" alt="" />
  </div>
)
```

### Crear paginas
<!-- Añadir pagina, añadir nueva pagina, añadir otra pagina -->
<!-- Agregar pagina, Agregar nueva pagina, Agregar otra pagina -->
Gatsby funciona con React para crear componentes y con las herramientas
de gatsby hace que todo aquello contenido en ```src/pages/*.js``` sea
convertido a una pagina.

1. Creamos un nuevo archivo en  src/pages/about.js con.

```js
import React from "react"

export default () => (
  <div style={{ color: `teal` }}>
    <h1>About Gatsby</h1>
    <p>Such wow. Very React.</p>
  </div>
)
```

1. y ahora podemos navegar a **localhost:8000/about**

### Crear sub-componentes, crear header

Por ejemplo si queremos reutilizar partes de nuestra interface
como searia el header o cualquier otro elemento que se comparte
entre paginas lo hacemos de la siguiente manera.

1. Creamos `src/components/header.js`

```js
import React from "react"
export default () => <h1>This is a header.</h1>
```

1. Agregamos el componente en `about.js`

```js
import React from "react"
import Header from "../components/header"
export default () => (
<div style={{ color: `purple` }}>
+   <Header />
    <p>Que mundo.</p>
    <img src="https://source.unsplash.com/random/400x200" alt="" />
</div>
)
```

Ya deberia de funcionar, si usamo esto en about.js e index.js
veremos que en ambos se sustituye por lo del sub-componente.

Pero nosotros queremos que este header diga en que pagina estamos
por ejemplo Home y About, por lo que le pasamos un parametro
<!-- pasar parametro a componente, componente con parametros, pasar variable a componente, componente con variables -->

1. Modificamos src/components/header.js

```js
import React from "react"
export default props => <h1>{props.headerText}</h1>
```

1. Pasamos variable al componente

```js
import React from "react"
import Header from "../components/header"
export default () => (
<div style={{ color: `purple` }}>
 +  <Header headerText="Home" />
    <p>Que mundo.</p>
    <img src="https://source.unsplash.com/random/400x200" alt="" />
</div>
)
```

### Enlaces entre paginas \<Link\>

Usamos el componente \<Link\> por ejemplo modificamos index.js

```js
import React from "react" import { Link } from "gatsby" import Header from "../components/header"
export default () => (
  <div style={{ color: `purple` }}>
+   <Link to="/about">about</Link>
    ...
  </div>
)
```

lo mismo para about.js

```js
import React from "react" import Header from "../components/header"
export default () => (
  <div... <Link to="/">Home</Link>
)
```

Nota: Para los enlaces a paginas exteriores se usa el link de
html de siempre

**Resultado** tenemos dos paginas que reutilizan codigo para el header
en el cual le pasamos un parametro y estan enlazadas por un link.

![sitio_hola_gatsby](./img/03_sitio_hola_gatsby.gif)

### Desplegar

Al ser archivos estaticos podemos desplegar en cualquier servidor
basico que entregue archivos, comunmente llamados **static site host**
del cual hay muchos, nosotros usaremos [Surge](https://surge.sh/)
para publicar nuestra app en internet.

A. Instalamos el CLI de Surge

```shell
npm install --global surge
```

B. Creamos una cuenta y nos logueamos

```shell
surge login
```

C. Construimos el sitio, construir para produccion, hacer version para produccion.

```shell
gatsby build
```

D. Subimos nuestros archivos a Surge
<!-- Subir archivos estaticos a internet, subir proyecto gatsby a internet, publicar proyecto gatsby, deploy en internet, subir a internet, publicar en internet, publicar proyecto en internet-->
```shell
surge public/
```

E. Nos va a pedir el nombre del sub-dominio, en mi caso
le puse mario51.surge.sh

**Resultado:** Publicamos nuestro proyecto en internet :O.

### Que aprendimos en Guia

Todo lo que vimos en esta seccion (Guia) esta en la fuente contenida

- Learned about Gatsby starters, and how to use them to create new projects
- Learned about JSX syntax
- Learned about components
- Learned about Gatsby page components and sub-components
- Learned about React “props” and reusing React components

<!-- 

.

###################################################################
###################################################################
###################################################################

.

-->

## Introduction to Styling in Gatsby

Fuente: [Gatsbyjs.org > Tutorials > Introduction to Styling in Gatsby](https://www.gatsbyjs.org/tutorial/part-two/)

### Usar estilos globales

Todo sitio tiene unos estilos generales como lo son la tipografia, fondos,
colores.

A. La forma mas sencilla de crearlos es crear un archivo global.css el
cual la carpeta puede ser la que sea.

```text
├── package.json
├── src
│   └── pages
│       └── index.js
│   └── styles
│       └── global.css
```

Le agregamos unos estilos

```css
html {
  background-color: lavenderblush;
}
```

B. Creamos un archivo **gatsby-browser.js** en la reiz, este archivo
es de los cuales Gatsby busca, por lo cual el nombre es importante.

```text
├── package.json
├── src
│   └── pages
│       └── index.js
│   └── styles
│       └── global.css
├── gatsby-browser.js
```

C. Editamos gatsby-browser.js para incluir el archivo de estilos
a nuestro proyecto en general

```js
import "./src/styles/global.css"
// la diferencia es require solo funciona con node algo asi
// require('./src/styles/global.css')
```

D. Iniciamos el proyecto: ```gatsby develop```

**Resultado:** Si navegamos entre paginas el estilo que definimos se
vera en ambas, **Nota** lo hicimos de la manera facil, pero la recomendada
es usar un componente layout, lo veremos en su momento

### Modulos CSS

Los modulos css son clases, id, animaciones, etc de css que tienen un
ambito local por default

Estos son mas populares por ser mas seguros, por ejemplo, evita el
colicionamiento de nombre.

A. Creamos **src/components/container.module.css** con los estilos,
el cual "module.css" es importante para que Gatsby lo procese como
modulo y no css plano.

```css
.container {
  margin: 3rem auto;
  max-width: 600px;
}
```

B. Creamos **src/components/container.js**, (el nombre es arbitrario)
e importamos nuestro modulo css que apenas creamos, para poderlo importar
como componente.

```js
import React from "react"
import containerStyles from "./container.module.css"

export default ({ children }) => (
  <div className={containerStyles.container}>{children}</div>
)
```

C. Creamos un componente pero dentro del container que acabamos de crear.
**src/pages/about-css-modules.js**

```js
import React from "react"
import Container from "../components/container"
export default () => (
  <Container>
    <h1>About CSS Modules</h1>
    <p>CSS Modules are cool</p>
  </Container>
)
```

D. Desplegamos y deveriamos ver los estilos.

![04_modulos_css](./img/04_modulos_css.PNG)

### Dar estilo a componente con Modulos css

Vamos a crear un componente usuario y por medio de un modulo CSS vamos
a agregarle estilos a este, para al final usarlo en una lista de usarios.

A. Creamos `src/pages/lista-usuarios.module.css` es importante que termine
en `.module.css` para que gatsby lo reconosca y haga su magia

```css
.user { display: flex; align-items: center; margin: 0 auto 12px auto; }
.user:last-child { margin-bottom: 0; }
.avatar { flex: 0 0 96px; width: 96px; height: 96px; margin: 0; }
.description { flex: 1; margin-left: 18px; padding: 12px; }
.username { margin: 0 0 12px 0; padding: 0; }
.excerpt { margin: 0; }
```

B. Creamos la pagina **src/pages/lista-usuarios.js** e importamos los
estilo darriba.

```js
import React from "react"
import styles from "./about-css-modules.module.css"
import Container from "../components/container"

console.log(styles)
//definimos el componente de Usuarios
const User = props => (
  <div className={styles.user}>
    <img src={props.avatar} className={styles.avatar} alt="" />
    <div className={styles.description}>
      <h2 className={styles.username}>{props.username}</h2>
      <p className={styles.excerpt}>{props.excerpt}</p>
    </div>
  </div>
)
//definimos la vista
export default () => (
  <Container>
    <h1>About CSS Modules</h1>
    <p>CSS Modules are cool</p>
    <User
      username="Jane Doe"
      avatar="https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg"
      excerpt="I'm Jane Doe. Lorem ipsum dolor sit amet, consectetur adipisicing elit."
    />
    <User
      username="Bob Smith"
      avatar="https://s3.amazonaws.com/uifaces/faces/twitter/vladarbatov/128.jpg"
      excerpt="I'm Bob Smith, a vertically aligned type of guy. Lorem ipsum dolor sit amet, consectetur adipisicing elit."
    />
  </Container>
)
```

> **Nota**: Si nos fijamos en el los logs dejando el console veremos
> que nos crea un src-pages----about-css-modules-module---XXX---2lRF7 que
> asegura sea unico.

Desplegamos y deberiamos de ver lo siguinte.

![04_modulos_css](./img/05_lista_usuarios_con_estilos_importados.PNG)

### Que aprendimos en Styling in Gatsby

- aplicar estilos generales desde un archivo css global
- Definimos estilos en un contenedor para eviar colicion de nombres
- Darle estilos a un componente con su propio archivo css

<!-- 

.

###################################################################
###################################################################
###################################################################

.

-->

## Crear componentes Layout anidados

- Fuente [Home > Tutorials > Creating Nested Layout Components](https://www.gatsbyjs.org/tutorial/part-three/)

Los layouts son secciones que queremos compartir en multiples paginas,
por ejemplo, las paginas comparten un header y un footer determinado,
pero igual no todas queremos que lo tengan

### Uso de Plugins (Typography.js)

Ya ahi cientos de librerias publicadas en [Gatsby Plugin Library.](https://www.gatsbyjs.org/plugins/),
nosotros vamos a udar la libreria **Typography.js** para crear tipografias
globales para nuestro sitio

A. Instalamos las Typography.js y sus dependencias

```bash
npm install --save gatsby-plugin-typography react-typography typography typography-theme-fairy-gates
# o en caso de haber creado el proyecto con yarn
yarn add --save gatsby-plugin-typography react-typography typography typography-theme-fairy-gates
```

B. agregar una configuracion para typography, en src/utils/typography.js.

```js
import Typography from "typography"
import fairyGateTheme from "typography-theme-fairy-gates"
const typography = new Typography(fairyGateTheme)
export const { scale, rhythm, options } = typography
export default typography
```

C. Editamos el archivo **gatsby-config.js** (este por defecto o comunmente
ya esta creado) que esta en raiz, para incluir la libreria y su configuracion
que creamos.

```js
module.exports = {
  plugins: [ {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
  ],
}
```

D. Para saber si se creo la tipografica debemos de ver en el inspector la
siguinte peticion de "data-gatsby-typografy".

![06_creacion_tipografia](./img/06_creacion_tipografia.PNG)

**Resultado** si njos fijamos la tipografia por defecto cambio, por ejemplo,
en la tipografia por defecto la e termina en pico, y aqui termina cuadrada.

### Creacion de componente Layout

<!-- Crear layout, agregar layout, usar layout, uso de layout, 
compartir estilos compartidos, -->
Si navegamos a about veremos que tiene la siguiente diferencia

![06_creacion_tipografia](./img/07_nope_difetencias_estilos.gif)

Y lo que queremos es que compartan ese espacio de los bordes, para
esto usaremos un layout que a continuacion crearemos.

A. Creamos el archivo src/components/layout.js con

```js
import React from "react"
import { Link } from "gatsby"
export default ({ children }) => (  
  <header style={{ marginBottom: `1.5rem`, margin: `3rem auto`, maxWidth: 650, padding: `0 1rem` }}>
    <Link to="/" style={{ textShadow: `none`, backgroundImage: `none` }}>
      <h3 style={{ display: `inline` }}>MySweetSite</h3>
    </Link>
    <nav>
      <Link to="/">Home</Link> |
      <Link to="/about">About</Link> |
      <Link to="/lista-usuarios">lista usuarios</Link>
    </nav>
    {children}
  </header>
)
```

B. Importamos el el layout en la paginas que queremos usar, en mi
caso home.js, about.js y contact.js

```js
import React from "react"
import Layout from "../components/layout"
import Pagetile from "../components/pagetile"

export default () => (
<Layout>
    <Pagetile headerText="..." />
    ...
</Layout>
)
```

**Resultado:** Ya deberiamos de tener estilos compartidos entre
las diferentes paginas, en este caso, la sepracion de los bordes,
y el titulo del sitio con su menu de navegacion

![08_uso_layouts](./img/08_uso_layoutss.gif)

### Que aprendimos en Crear componentes Layout anidados

- Uso de plugins para crear layouts
- Crear tipografia para uso global con Typography.js
- Uso de layouts, para estilos y estructuras compartidas

<!-- 

.

###################################################################
###################################################################
###################################################################

.

-->

## Data in Gatsby

- Fuente: [Gatsby > Tutorials > Data in Gatsby](https://www.gatsbyjs.org/tutorial/part-four/)

Para gatsby data es todo aquello que esta fuera de los
componentes, esta se obtiene popularmente a travez de
GraphQL por lo que recomendamos este tutorial [howtographql](https://www.howtographql.com/)
pero tambien podemos obtener la informacion a travez
de archivos como markdown, cvs, json, etc.

Vamos a ver un ejemplo a travez de GrapQL en nuestro caso,
tambien usaremos css-in-js y el tema de tipografia Kirkham

A. Instalamos dependencias

<!-- instalar css emotion, incrustar css-->
```bash
npm install --save gatsby-plugin-typography typography react-typography typography-theme-kirkham gatsby-plugin-emotion @emotion/core
# o yarn add ....
```

B. Nuestro layout.js lo dejamos de la siguiente manera

```js
import React from "react"
import { Link } from "gatsby"
import { css } from "@emotion/core"
import { rhythm } from "../utils/typography"
import Container from "./container"

export default ({ children }) => (  
  <header css={css`
      margin: 0 auto;
      max-width: 700px;
      padding: ${rhythm(2)};
      padding-top: ${rhythm(1.5)};`}>
    <Link to="/" style={{ textShadow: `none`, backgroundImage: `none` }}>
      <h3 style={{ display: `inline` }}>MySweetSite</h3>
    </Link>
    <nav css={css`float: right;`}>
      <Link to="/">Home</Link> |
      <Link to="/about">About</Link> |
      <Link to="/lista-usuarios">lista usuarios</Link>
    </nav>
    <Container>
      {children}
    </Container>
  </header>
)
```

C. Nuestra configuracion de typography

```js
import Typography from "typography"
import kirkhamTheme from "typography-theme-kirkham"

const typography = new Typography(kirkhamTheme)

export default typography
export const rhythm = typography.rhythm
```

D. Nustra configuracion de gatsby debe estar
de la siguiente manera

```js
module.exports = {
  plugins: [
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
  ],
}
```

**Resultado:** Deberiamos de tener la siguinte vista

![06_creacion_tipografia](./img/9_vista_con_css-in-js.PNG)

### Tu primer consulta GraphQL

Tenemos informacion comun en nuestro sitio, como es el
titulo, logo, descripcion, rdes sociales, etc que podemos
tener esta informacion en distintas partes del sitio, lo
cual causa dos incomodidades 1) si lo cambiamos lo demos
de cambiar en todos lados, 2) si qujeremos cambiar algo
en especifico debemos de recordar en que componente y
tener cuidado ya es mas probable que jadamos algo, si
tocamos algo que tiene varias responsabilidades.

Por lo que lo mas correcto es tener esta informacion en
un solo archivo una sola vez, y al cambiar este, se cambia
en todos lados, sin tocar los compontes.

<!-- consultar siteMetadata con graphql, obtener siteMetadata, obtener informacion de siteMetadata informacion, traer siteMetadata, traer informacion de siteMetadata , traer informacion siteMetadata-->
**Objetivo:** Vamos a consultar el dato de lastUpdate de
nuestro siteMetadata.

A. **gatsby-config.js** aqui guardamos toda la informacion
referente al sitio, en un objeto llamado **siteMetadata**.

```js
module.exports = {
  siteMetadata: {
    title: `Title from siteMetadata`,
    lastUpdate: `29 de abril del 2019`
  },
  plugins: [
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
  ],
}
```

B. Agregamos la dependencia graphql a nuestro about.js
y lo consultamos al archivo de la siguiente manera.

```js
import React from "react"
import Pagetile from "../components/pagetile"
import Layout from "../components/layout"
+ import { graphql } from "gatsby"

export default ({ data }) => (
  <Layout style={{ color: `teal` }}>
    <Pagetile headerText="About e" />
    <p>Such wow. Very React.</p>
+   <footer>{data.site.siteMetadata.lastUpdate}</footer>
  </Layout>
  
)

+ export const query = graphql`
  query {
    site {
      siteMetadata {
        lastUpdate
      }
    }
  }
`
```

C. Al hacer un cambio en gatsby-config.js debemos de
reiniciar el debelop para que vea los cambios

**Resultado:** Debemos de poder ver lo que pusimos en
siteMetadata en mi caso lastUpdate.

![06_creacion_tipografia](./img/10_primer_consulta_graphql.PNG)

### Usar una StaticQuery

Esta esta disponible en Gatsby v2 la cual permite peticiones
dentro de NO-componentes como es el layout.

**Objetivo:** Vamos a hacer la peticion del titulo en nuestro
layout, el cual como no es un componente, si lo hacemos igual
que arriba causaria error.

A. Importamos de gatsby a useStaticQuery y agregamos la
consulta y regresamos la plantilla.

```js
import React from "react"
import { css } from "@emotion/core"
import { rhythm } from "../utils/typography"
import Container from "./container"
+ import { useStaticQuery, Link, graphql } from "gatsby"

export default ({ children }) => {
+ const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  )

+return (
  <header css={css`
      margin: 0 auto;
      max-width: 700px;
      padding: ${rhythm(2)};
      padding-top: ${rhythm(1.5)};`}>
    <Link to="/" style={{ textShadow: `none`, backgroundImage: `none` }}>
      <h3 style={{ display: `inline` }}>
        {data.site.siteMetadata.title}
      </h3>
    </Link>
    <nav css={css`float: right;`}>
      <Link to="/">Home</Link> |
      <Link to="/about">About</Link> |
      <Link to="/lista-usuarios">lista usuarios</Link>
    </nav>
    <Container>
      {children}
    </Container>
  </header>
  )
+}
```

**Resultado:** Vemos el titulo que pusicmos en siteMetadata dentro
de nuestro layout

![01_arquitectura_SSR](./img/11_consulta_con_StaticQuery.PNG)

**Por que dos tipos de peticiones?**, Esto es por la forma en que son
formateados y donde pueden ser utilizadas, pero en resumen es
**solo las paginas pueden hacer querys y las no-paginas hacen StaticQuery**

**Nota:** Si cambiamos el cualquier informacion del siteMetadata por
ejemplo el titulo estedeberia de cambiar, casi instantanemente.

### Que apendimos

- Que es el siteMetadata
- Hacer peticiones en GraphQL
- Saber la diferencia entre Querys y StaticQuerys

## Plugins de Source y renderizando peticiones de data

Fuente: [Gatsby > Tutorials > Source Plugins and Rendering Queried Data](https://www.gatsbyjs.org/tutorial/part-five/)

Vamos a aprender a consultar informacion por medio d librerias a
nuestros componentes, por lo que veremos algo de GraphiQL (fijarse
en la "i"), que es una herramienta para Integrar GraphQL.

Podemos acceder a este sitio por medio de ```http://localhost:8000/___graphql```

- [Video Introductori](https://www.gatsbyjs.org/graphiql-explore.mp4)

En el video podemos ver como por medio de click podemos construir
consultas graphQl

### Source plugins

La informacion de gatsby puede venir de donde sea APIs, databases,
CMSs, local files, etc. y cada uno de estos tiene un plugins que nos
ayuda a buscar la infomacion de estas fuentes.

**Objetivos:** Vamos a usar un plugin para buscar informacion en
archivos locales.

A. Instalamos plugin

```bash
yarn add --save gatsby-source-filesystem
# ò yarn add --save -D gatsby-source-filesyste
```

> **Error**: yarn add Missing list of packages to add to your project  
> **Descripcion**: Al ejecutar el comando el primer mensaje de error es este  
> **Causa**: Al parecer es un error con el package.json  
> **Fuente** [Link](https://github.com/yarnpkg/yarn/issues/3973)  
> **Solucion**: Agregar la bandera -D

B. Configuramos plugin en gatsby-config.js

```js
module.exports = {
  siteMetadata: {
    title: `Pandas Eating Lots`,
  },
  plugins: [
    {
+     resolve: `gatsby-source-filesystem`,
+     options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    `gatsby-plugin-emotion`,{
      resolve: `gatsby-plugin-typography`,
      options: { pathToConfigModule: `src/utils/typography`, },
    },
  ],
}
```

**Resultado:** Hacemos gatsby develop y deberiamos de poder hacer
una consulta para ver todos nuestros archivos dentr de ./src/

![12_consulta_archivos_con_graphql](./img/12_consulta_archivos_con_graphql.PNG)

### Construir pagina con consulta

Vamos a listar nuestros archivos dentro de una pagina

A. la consulta que creamos en GraphiQL la ponemos en una
nueva pagina **src/pages/my-files.js**

```js
import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"

export default ({ data }) => {
  console.log(data)
  return (
    <Layout>
      <div>
        <h1>My Site's Files</h1>
        <table>
          <thead>
            <tr>
              <th>relativePath</th>
              <th>prettySize</th>
              <th>extension</th>
              <th>birthTime</th>
            </tr>
          </thead>
          <tbody>
            {data.allFile.edges.map(({ node }, index) => (
              <tr key={index}>
                <td>{node.relativePath}</td>
                <td>{node.prettySize}</td>
                <td>{node.extension}</td>
                <td>{node.birthTime}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allFile {
      edges {
        node {
          relativePath
          prettySize
          extension
          birthTime(fromNow: true)
        }
      }
    }
  }
`
```

**Resultado:** Podemos ver una lista de nuestro archivos del proyecto
en la pagina, esto por medio de una consulta a nuestro filesystem
por medio de un plugin para esto

![12_consulta_archivos_con_graphql](./img/13_consulta_archivos_mostrados_en_html.PNG)

### Que aprendimos en Source y renderizando

- Uso de GraphiQL para crear consultas
- Usar el plugin gatsby-source-filesyste para consultar eso
- Mostrar los archivos en una tabla html

<!-- 

.

###################################################################
###################################################################
###################################################################

.

-->

## Transformer plugins

Fuente: [Gatsby > Tutorials > 6. Transformer Plugins](https://www.gatsbyjs.org/tutorial/part-six/)

Vimos como traer informacion pero esta venia de forma plana, ahora
veremos como transformar esta informacion, para su mejor manipulacion.

**Por ejemplo**, obtuvimos informacion de los archivos, pero que pasa si
queremos la informacion de estos archivos, para esto usamos plugins.

**Objetovo**, Vamos a usar archivos markdown para guardar info, y
luego los pasaremos a html, para esto usamos plugins.

A. Instalamos plugin para transformar markdown a html

```r
npm install --save gatsby-transformer-remark # o usar yarn
```

B. Agregamos la configuracion en **gatsby-config.js**

```js
module.exports = {
  siteMetadata: {
    title: `Pandas Eating Lots`,
  },
  plugins: [
    ...
    `gatsby-transformer-remark`,
  ],
}
```

C. Creamos *src/blogs/sweet-pandas-eating-sweets.md*

```txt
---
title: "Sweet Pandas Eating Sweets"
date: "2017-08-10"
---

# Pandas are really sweet.

Here's a video of a panda eating sweets.

## Listas

- item 1
- item 2
- item 3


## tablas
| Tables        |      Are      |  Cool |
| ------------- | :-----------: | ----: |
| col 3 is      | right-aligned | $1600 |
| col 2 is      |   centered    |   $12 |
| zebra stripes |   are neat    |    $1 |
```

D. Reiniciamos ```develop``` y entramos a GraphiQL y deberemos
ver algo como

```txt
query MyQuery { allMarkdownRemark { edges { node {
  frontmatter {
    title
    date
  }
  excerpt
  timeToRead
  html
} } } }
```

**Resultado** Podemos consultar-transformar el contenido de un
markdown guardado en nuestro filesystem

![14_consulta_markdown_desde_graphiql](./img/14_consulta_markdown_desde_graphiql.PNG)

### Crear lista de blogs

Ahora vamos a crear una lista de links que dirigan a nuestros
markdowns y poderlos visualizar como html, como cualquier blog

A. Creamos el archivo **src/pages/blogs.js**

```js
import React from "react"
import { graphql } from "gatsby"
import { css } from "@emotion/core"
import { rhythm } from "../utils/typography"
import Layout from "../components/layout"

export default ({ data }) => {
  console.log(data)
  return (
    <Layout>
      <div>
        <h1
          css={css`
            display: inline-block;
            border-bottom: 1px solid;
          `}
        >
          Amazing Pandas Eating Things
        </h1>
        <h4>{data.allMarkdownRemark.totalCount} Posts</h4>
        {data.allMarkdownRemark.edges.map(({ node }) => (
          <div key={node.id}>
            <h3
              css={css`
                margin-bottom: ${rhythm(1 / 4)};
              `}
            >
              {node.frontmatter.title}{" "}
              <span
                css={css`
                  color: #bbb;
                `}
              >
                — {node.frontmatter.date}
              </span>
            </h3>
            <p>{node.excerpt}</p>
          </div>
        ))}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allMarkdownRemark {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
          }
          excerpt
        }
      }
    }
  }
`
```

pero se ve peque la lista asi que agregamos otro articulo

```html
---
title: "Pandas and Bananas"
date: "2017-08-21"
---

Do Pandas eat bananas? Check out this short video that shows that yes! pandas do
seem to really enjoy bananas!

<iframe width="560" height="315" src="https://www.youtube.com/embed/4SZl1r2O_bY" frameborder="0" allowfullscreen></iframe>
```

**Resultado:** Y deberiamos ver la lista de la siguiente manera

![15_mostrar_markdowns_como_lista_de_blogs](./img/15_mostrar_markdowns_como_lista_de_blogs.PNG)

**Pero** si vemos el orden de los post no esta ordenado, para esto
igual que SQL tenemos diferentes sets para ordenar, filtrar, etc, lo
que consultamos.  
En este caso agregamos

```txt
export const query = graphql`
  query {
    allMarkdownRemark (
+     sort: {
        fields: [frontmatter___date]
        order: DESC
      }
    ){ totalCount ... }
  }
```

### Que aprendimos en Transformer plugins

- Usar el plugin "gatsby-transformer-remark" para consultar/transformar
  el markdown dentro de nuestro filesystem, y usar propiedades para mostrar
  el primer renglon (excerpts = extracto) de contenido de nuestro markdown
- Usar sort (equivalente a SQL) para ordenar los blogs por fecha

<!-- 

.

###################################################################
###################################################################
###################################################################

.

-->

## Crear paginas de los datos programaticamente

Fuente: [Home > Tutorials > 7. Programmatically Create Pages from Data](https://www.gatsbyjs.org/tutorial/part-seven/)

Podemos seguir agregando archivos en `src/pages` a mano, pero
tambien podemos agregar archivos de forma programatica, haciendo
peticiones a GraphQL

### Creando rutas tontas=slugs

**Los Slugs son** como rutas unicas donde se generara la pagina
para esto usaremos ```onCreateNode``` y ```createPages``` que son APIs
que veremos en varios sitios.

pare usarla le debemos de pasar una funcion la API de la siguiente manera.

**Objetivo:** Ya tenemos una lista de posts, pero ahora queremos que cada
post tenga su propia pagina=archivo.

A. Agregamos el archivo `./gatsby-node.js`

```js
exports.onCreateNode = ({ node }) => {
  console.log(node.internal.type)
}
```

Esta funcion sera llamada por gatsby cada ves que levantemos el servicio,
(no cuando modificquemos algo) y se ejecutara cada vez que se cree un nodo

```logs
Logs Mios - SitePlugin
...
Logs Mios - SitePlugin
Logs Mios - Site
Logs Mios - SiteBuildMetadata
Logs Mios - Directory
...
Logs Mios - Directory
Logs Mios - File
...
Logs Mios - File
Logs Mios - MarkdownRemark
Logs Mios - MarkdownRemark
Logs Mios - SitePage
...
Logs Mios - SitePage
success createPagesStatefull
```

B. Son mochos logs, pero nosotros solo queremos cuando detecta un archivo
markdown (MarkdownRemark) por lo que agregamos un codicional.

```js
exports.onCreateNode = ({ node }) => {
  if (node.internal.type === `MarkdownRemark`) {
    console.log(node.internal.type)
  }
}
```
<!-- creacion de slugs, -->
**C. crear Slugs**, En nuestro caso vamos a tomar el nombre de cada archivo
markdown, y lo convertiremos en una ruta, por ejemplo, pandas-and-bananas.md
pasara a ser "dominio.com/pandas-and-bananas", para eso nuestro objeto MarkdownRemark
tambien tiene el objeto File, el cual de ahi lo sacamos, de la siguiente manera.

C.1. Importamos "gatsby-source-filesystem" que tiene la funcion de crear el slug
     a partir del nodo.

C.1. Creamos el Slug con ```createFilePath```

C.2. Con ```createNodeField```  agregamos informacion adicional

```js
const { createFilePath } = require(`gatsby-source-filesystem`)
exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages` })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }
}
```

**D. Comprobamos** iniciamos el servicio develop y corremos la siguinte
consulta, y debemos de ver nuestro slugs recien creados.

```graphql
{
  allMarkdownRemark {
    edges {
      node {
        fields {
          slug
        }
      }
    }
  }
}
```

Ya que vimos que fueron creados los slugs podemos crear las paginas

![16_consultar_slugs](./img/16_consultar_slugs.png)

### Creadon paginas

**Objetivos:** Ya tenemos los slugs ahora necesitamos crear las paginas de
estos slugs, Para crear las paginas usamo ```createPages``` en donde necesitamos.

A. Crear el template para nuestra pagina ```src/templates/blog-post.js```

```js
import React from "react"
import Layout from "../components/layout"
export default () => {
  return (
    <Layout>
      <div>Hello blog post</div>
    </Layout>
  )
}
```

B. En `gatsby-node.js`

  B.1 Crear la consulta GraphQL de nuestros slugs desde `onCreateNode`.

  C.2 Crear un iterador que por cada slug se cree gatsby-node.js

```js
const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages` })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const result = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `)

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve(`./src/templates/blog-post.js`),
      context: {
        // Data passed to context is available in page queries as GraphQL variables.
        slug: node.fields.slug,
      },
    })
  })
}
```

**C. Comprobamos**, reiniciamos si vamos a una ruta que arroje un 404 podremos ver las
rutas tenemos, creadas en este caso podremos ver las nuevas generadas programaticamente.

![17-ver-todos-los-links-disponibles-por-404](./img/17-ver-todos-los-links-disponibles-por-404.png)

Pero si entramos a los en los enlaces no tiene informacion del post, por lo que
**objetivo** agregar informacion del markdown a nuestra pagina, y si nos fijamos
en el codigo de arriba, ya le pasamos esta informacion a la plantilla solo es cosa
de usarla.

D. Creamos consulta GraphQL para obtener la info que pasamos en el ```result.data.allMarkdownRemark.edges.forEach```
y lo recupetamos lo consultado dentro de ```data.markdownRemark```

```js
//src/templates/blog-post.js
import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"

export default ({ data }) => {
  const post = data.markdownRemark
  return (
    <Layout>
      <div>
        <h1>{post.frontmatter.title}</h1>
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </div>
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
      }
    }
  }
`
```

**Resultado** Podemos ver nuestros posts si entramos a los enlaces
que arriba teniamos.

![18-detalle-blog](./img/18-detalle-blog.png)

**Agregar liks a nuestros blogs** ya solo falta esto, y lo
podemos hacer por medio de obtener los slugs por una consulta
graphql y agregarlos

```js
//src/pages/blogs.js
import React from "react"
import { graphql, Link } from "gatsby"
import { css } from "@emotion/core"
import { rhythm } from "../utils/typography"
import Layout from "../components/layout"

export default ({ data }) => {
  console.log(data)
  return (
    <Layout>
      <div>
        <h4>{data.allMarkdownRemark.totalCount} Posts</h4>
        {data.allMarkdownRemark.edges.map(({ node }) => (
          <div key={node.id}>
            <Link to={node.fields.slug}css={css`text-decoration: none;color: inherit;`}>
              <h3 css={css`margin-bottom: ${rhythm(1 / 4)};`}>
                {node.frontmatter.title}{" "}
                <span css={css`color: #bbb;`}> — {node.frontmatter.date}</span>
              </h3>
            </Link>
            <p>{node.excerpt}</p>
          </div>
        ))}
      </div>
    </Layout>
  )
}

export const query = graphql`
query {
  allMarkdownRemark (
    sort: {
      fields: [frontmatter___date]
      order: DESC
    } ){
    totalCount
    edges {
      node {
        id
        frontmatter {
          title
          date(formatString: "DD MMMM, YYYY")
        }
        excerpt
        fields {
          slug
} } } } }
`
```

### Que aprendimos en crear paginas de los datos programaticamente

- Crear una pagina por cada archivo markdown
- Que es un ```Slug```
- Que en el archivo ```./gatsby-node.js``` podemos iterar
  todo los nodos que se crean con ```onCreateNode```
- Crear Slugs con createFilePath del plugin ```gatsby-source-filesystem```
- Consultar Slugs con GraphQL
- Crear una pagina con ```createPages``` y su respectiva plnatilla

<!-- 

.

###################################################################
###################################################################
###################################################################

.

-->

## Preparing a Site to Go Live

Fuente: [Home > Tutorials > 8. Preparing a Site to Go Live](https://www.gatsbyjs.org/tutorial/part-eight/)

Veremos los pasos comunes de como llevar un sitio a la vida, como
el uso de ua herramienta de diagnostico, en este caso Lighthouse,
esta herramienta la tenemos includa en el DevTools de Chrome, la cual
vera aspectos como la calidad, performance, accesibilidad, etc

A. Creamos una version de produccion ```gatsby build``` e iniciamos
el servicio ```gatsby serve``` y deberiamos de ver el sitio en
localhost:9000

B. Abrimos el devtools de chrome y camos a auditoria y le damos a
auditar

![08_links_a_slugs](./img/19-auditoria-con-chrome-Lighthouse.png)

<!-- 

.

-->

### Add a manifest file

Como vemos no teemos nada en PWA, este archivo es un requisito minimo
para las PWA, y dice al navegador como es que se va a comportar el
sitiouna ves cargado

**Objetivo** Vamos a agregar el archivo manifest a nuestro sitio
a travez de un plugin

A. Instal el plugin

```bash
npm install --save gatsby-plugin-manifest
```

B. Agregamos el icono en src/images/icon.png (como sea) en mi caso
agrege [este](https://image.flaticon.com/icons/png/128/258/258565.png)

C. Agregamos el plugin a nustro proyecto en gatsby-config.js

```js
{
  plugins: [
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Pandas y dulsuras`,
        short_name: `pandasDulces`,
        start_url: `/`,
        background_color: `#6b37bf`,
        theme_color: `#6b37bf`,
        // Enables "Add to Homescreen" prompt and disables browser UI (including back button)
        // see https://developers.google.com/web/fundamentals/web-app-manifest/#display
        display: `standalone`,
        icon: `src/images/icon.png`, // This path is relative to the root of the site.
      },
    },
  ]
}
```

**Reusultado** Deveriamos de poder ver en herramienta de desarrollador -> aplicacion
ver la configuracion del manifest que le agregamos

<!-- 

.

-->

### Using gatsby-plugin-offline

A. Instalar Plugin

```bash
yarn add --save -D gatsby-plugin-offline
```

B. Agregamos el plugin en gatsby-config.js

```js
 plugins: [ {
    resolve: `gatsby-plugin-manifest`, options: {...},
+   `gatsby-plugin-offline`, // debe estar despues del `gatsby-plugin-manifest`
  ]
```

**Resultado:** cargamos la pagina una vez y luego pasamos a offline y
cargamos de nuevo podremos ver cmo si carga esta, **nota** por alguna
**razon que no se** a la hora de cargar en mario51.surge.sh offline esta no carga

<!-- 

.

-->

### Add page metadata wiht React Helmet and gatsby-plugin-react-helmet

Esta info mjora nuestro SEO en nuestro caso usaremos *React Helmet* y
*gatsby-plugin-react-helmet* para ayudarnos en nuestros \<meta\>

A. Instalamos los paquetes

```bash
yarn add --save -D gatsby-plugin-react-helmet react-helmet
```

> **Error**: Element type is invalid: expected a string (for built-in components)
> or a class/function (for composite components) but got: undefined. Check the render method of SEO  
> **Descripcion:** Con solo usar la etiqueta \<SEO /> y haber revizado que las
> variables existieran y de hacerlo como lo hacia en el tutorial y etc, me muestra este
> error en el navegador.  
> **Causa:** Cuando hice el tutorial me descargo las verciones de 3.2.1 y 5.2.1 respectivamente de
> Helmet
> **Solucion:** Usar la misma version que use en este tutorial

<!-- instalar version especifica, definir version a instalar, instalar x version, yarn version, yarn definir version en yarn, agregar version a instalar-->
```bash
yarn add --save -D gatsby-plugin-react-helmet@3.2.1 react-helmet@5.2.1
```

B. Nos asseguramos que tengamos los siguientes campos (+) en ```gatsby-config.js```
para que se agregen a nuestro meta.

```js
module.exports = {
  siteMetadata: {
    title: `Pandas Eating Lots`,
    description: `A simple description about pandas eating lots...`,
    author: `gatsbyjs`,
  }
  ...
  plugins: [...
+   `gatsby-plugin-react-helmet`,
  ]
```

C. agregamos ```src/components/seo.js``` que es como un Util con lo siguiente,
lo cual agregara la meta-data mas comun, pero la deveremos sedusar en
nuestras paginas en nuestro caso los blog

```js
import React from "react"
import PropTypes from "prop-types"
import Helmet from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"

function SEO({ description, lang, meta, title }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
    `
  )

  const metaDescription = description || site.siteMetadata.description

  return (
    <Helmet
      htmlAttributes={{ lang, }}
      title={title}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      meta={[
        { name: `description`, content: metaDescription, },
        { property: `og:title`, content: title, },
        { property: `og:description`, content: metaDescription, },
        { property: `og:type`, content: `website`, },
        { name: `twitter:card`, content: `summary`, },
        { name: `twitter:creator`, content: site.siteMetadata.author, },
        { name: `twitter:title`, content: title, },
        { name: `twitter:description`, content: metaDescription, },
      ].concat(meta)}
    />
  )
}

SEO.defaultProps = {
  lang: `es`,
  meta: [],
  description: ``,
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}

export default SEO
```

D. Usamos nuestro util en ```src/templates/blog-post.js``` en el cual
agregamos los meta de titulo con el titulo del post y descripcion con
el extracto del cuerpo, pero puede ser el que sea carnal

```js
import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
+ import SEO from "../components/seo"

export default ({ data }) => {
  const post = data.markdownRemark
  return (
    <Layout>
+     <SEO title={post.frontmatter.title} description={post.excerpt} />
      <div>
        <h1>{post.frontmatter.title}</h1>
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </div>
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
      }
+     excerpt
    }
  }
`
```

**Resultado:** Si nos fijamos debemos de tener title y el meta de descripcion

![08_links_a_slugs](./img/20-agregar-metada.png)

Si corremos de nuevo el test deberiamos de tener una calificacion mas
alta, lo de "mejores practicas" es por que no estoy haciendo uso de https

![08_links_a_slugs](./img/21-auditoria-seo-y-offline.png)

### Que aprendimos en Preparing a Site to Go Live

- A usar herramienta de auditoria de google Chrome (Lighthouse)
- Agregar archivo manifest
- Usar "gatsby-plugin-offline" para carga offline de nuestro sitio
- Uso de "Helmet" y "gatsby-plugin-react-helmet" para agregar SEO

<!-- 

.

###################################################################
###################################################################
###################################################################

.

-->

## Plugins y Temas

Fuente: [Home > Tutorials > Plugin & Theme Tutorials](https://www.gatsbyjs.org/tutorial/plugin-and-theme-tutorials/)

### Creating an Image Source Plugin

**Por que usar plugins** estos pueden consultar informacion de varias
partes y ponerla en a disposicion de gatsby, pero no convendria hacerlo
de archivos locales, ya que para eso ya tenemos "gatsby-source-filesystem"

A. Nos creamos una cuenta en pixabay.com para poder consultar una la
API_KEY lo hacemos [aqui](https://pixabay.com/api/docs/#api_search_images)

```text
16268495-3c5e39594e7ebfac2df2f4d7d
```

Para consultar imagenes lo podemos hacer con la sig. url

```text
https://pixabay.com/api/?q=yellow+flowers&editors_choice=true&pretty=true&key=<KEY_HERE>
```

**Que hara nuestro Plugin** Vamos a crear nuestro propio plugin que
consultara imagenes de pixabay.com para ponerla a disposicion de gatsby,
para eso tendra el siguiente comportamiento.

- Aceptar configuraciones como asignar una API-KEY y parametros de
  busqueda
- Hacer una peticion a la api usando la configuracion asignada
- Convertir la informacion de la peticion al sistema de nodos de gatsby

B. Crear proyecto gatsby `gatsby new plugin-imagenes https://github.com/gatsbyjs/gatsby-starter-default`

<!-- generar package.json, crear package.json, agregar package.json-->
C. creamos una carpeta `plugins` donde crearemos nuestro plugin de
forma local, ya a futuro veremos como subir este plugin a npm. dentro
de la carpeta tendra la siguiente estructura, para crear package.json
agregamos ejecutamos `npm init --yes`

```text
|-- plugins
    |-- gatsby-source-pixabay
        |-- gatsby-node.js
        |-- package.json
```

D. agregar dependencias: `npm install node-fetch query-string --save`
se tuvo que agregar lo sig al package.json

```json
"dependencies": {
    "node-fetch": "^2.2.0",
    "query-string": "^6.1.0"
  }
```

E. En el archivo gatsby-node.js agregamos lo siguiente

1. `sourceNodes`: Agregamos el plugin como proceso de construccion

```js
const fetch = require("node-fetch")
const queryString = require("query-string")

exports.sourceNodes = ({ actions, createNodeId, createContentDigest }, configOptions) => {
  const { createNode } = actions

  // Gatsby adds a configOption that's not needed for this plugin, delete it
  delete configOptions.plugins

  // plugin code goes here...
  console.log("Testing my plugin", configOptions);
}
```

#### Agregar nuestro plugin a gatsby

Ya tenemos el esqueleto de nuestro plugin

A. En `gatsby-config.js` agregamos nuestro plugin

```js
plugins: [
  "...",
  {
    resolve: "gatsby-source-pixabay",
    options: {
      key: "16268495-3c5e39594e7ebfac2df2f4d7d",
      q: "yellow flowers",
    },
  },
],
```

B. iniciar proyecto: `gatsby develop` y debemos de poder en el build
el mensaje de nuestro plugin.

![22-agregar-mi-plugin](./img/22-agregar-mi-plugin.PNG)

C. En `gatsby-source-pixabay\gatsby-node.js` agregamos

1. Las opciones de la configuracion a parametros para la peticion
2. la peticion

```js
// console.log("Testing my plugin", configOptions);
// Convert the options object into a query string
const apiOptions = queryString.stringify(configOptions)
// Join apiOptions with the Pixabay API URL
const apiUrl = `https://pixabay.com/api/?${apiOptions}`
// Gatsby expects sourceNodes to return a promise
return (
// Fetch a response from the apiUrl
fetch(apiUrl)
  // Parse the response as JSON
  .then(response => response.json())
  // Process the JSON data into a node
  .then(data => {
    // For each query result (or 'hit')
    data.hits.forEach(photo => {
      console.log("Photo data is:", photo)
    })
  })
)
```

Si construimos ahora deberemos de poder ver el resultado de la
peticion que es un json.

#### Convertir resultado de la peticion a Nodos

A. Agregamos funcion para convertir por cada `hits` (fotos) que nos
da la API a nodos

B. La usamos por cada hits que tenga el resultado de la peticion

```js
const fetch = require("node-fetch")
const queryString = require("query-string")
exports.sourceNodes = ({actions, createNodeId, createContentDigest }, configOptions) => {
  const { createNode } = actions
  delete configOptions.plugins
  // Helper function that processes a photo to match Gatsby's node structure
+ const processPhoto = photo => {
    const nodeId = createNodeId(`pixabay-photo-${photo.id}`)
    const nodeContent = JSON.stringify(photo)
    const nodeData = Object.assign({}, photo, {
      id: nodeId,
      parent: null,
      children: [],
      internal: {
        type: `PixabayPhoto`,
        content: nodeContent,
        contentDigest: createContentDigest(photo),
      },
    })
    return nodeData
  }
  const apiOptions = queryString.stringify(configOptions)
  const apiUrl = `https://pixabay.com/api/?${apiOptions}`

  return (
    fetch(apiUrl)
      .then(response => response.json())
      .then(data => {
        data.hits.forEach(photo => {
          // Process the photo data to match the structure of a Gatsby node
+         const nodeData = processPhoto(photo)
          // Use Gatsby's createNode helper to create a node from the node data
+         createNode(nodeData)
        })
      })
  )
}
```

Ahora la siguiente en consulta en GrapiQL veremos nuestras fotos
disponibles.

```js
{
  allPixabayPhoto(limit: 10) {
    edges {
      node {
        largeImageURL
        pageURL
        tags
        user
      }
    }
  }
}
```

![28-consulta-resultado-peticion-a-consulta-graphql-gatsby](./img/28-consulta-resultado-peticion-a-consulta-graphql-gatsby.PNG)

#### Que aprendimos en Creating an Image Source Plugin

- Crear un plugin y pasarle parametros
- Hacer peticion a API de terceros
- Pasar la peticion al sistema de gatsby

<!-- 

.

.

.

-->

### Creating a Remark Transformer Plugin

gatsby-transformer-remark es un plugin para poder pasar el markdown a
html, pero luego uno quiere personalizar el contenido como videos,
resaltar texto, etc.

Cuando queremos modificar algo en markdown debemos de navegar en los
nodos para encontrar lo que queremos, en este caso los headers para lo
cual usaremos [unist-util-visit](https://github.com/syntax-tree/unist-util-visit).

**Objetivo** Vamos a crear un pligin en el que modificaremos el [AST](https://www.gatsbyjs.org/tutorial/remark-plugin-tutorial/#understanding-the-abstract-syntax-tree)
(Abstract Syntax Tree) de markdown en el para que los headers los
pintemos de color azul.

A. En `gatsby-config.js` agregamos las sig. plugins y configuraciones.
en la cual suponemos que los .md estaran en /src/markdowns/

```js
// Si nos fijamos nuestro plugin de "gatsby-transformer-remark" tiene el plugin de "gatsby-remark-purple-headers"
module.exports = {
  siteMetadata: {...},
  plugins: [
    ...
+   { resolve: "gatsby-source-filesystem", options: { name: `data`, path: `${__dirname}/src/markdowns/`, }, },
+   { resolve: `gatsby-transformer-remark`, options: { plugins: [`gatsby-remark-purple-headers`], }, },
  ],
}
```

B.Creamos /src/markdowns/hola.md

```md
# Hello World!

This is a [Real page](https://google.com)
```

C. Creamos plugins/gatsby-remark-purple-headers/index.js con su package.json

```js
const visit = require("unist-util-visit")
const toString = require("mdast-util-to-string")

module.exports = ({ markdownAST }, pluginOptions) => {
  visit(markdownAST, "heading", node => {
    let { depth } = node

    // Skip if not an h1
    if (depth !== 1) return

    // Grab the innerText of the heading node
    let text = toString(node)

    const html = `
        <h1 style="color: blue">
          ${text}
        </h1>
      `
    node.type = "html"
    node.children = undefined
    node.value = html
  })
  return markdownAST
}
```

**Resultado:** Podemos ver que el html tienelos estilos que le definimos

![29-cambiar.estilos-marckdown-desde-su-arbol](./img/29-cambiar.estilos-marckdown-desde-su-arbol.PNG)

<!-- 

.

.

.

-->

### Using a Theme

Para usar un tema lo hacemos al crear un proyecto igual que usar un
starter normal

```r
gatsby new tema-blog https://github.com/gatsbyjs/gatsby-starter-blog-theme
```

que nos descargara un proyecto (en este caso) con la siguiente estruc.

```r
my-blog
├── content
│   ├── assets
│   │   └── avatar.png
│   └── posts
│       ├── hello-world.mdx
│       └── my-second-post.mdx
├── src
│   └── gatsby-theme-blog
│       ├── components
│       │   └── bio-content.js
│       └── gatsby-plugin-theme-ui
│           └── colors.js
├── gatsby-config.js
└── package.json
```

- `avatar.png` - imagen de nuestro avatar
- `gatsby-config.js` - ifo. como autor, descripcion r. sociales
- `gatsby-theme-blog` - debe tener el mismo nombre que el tema ya que
  otro tema que tenga la misma configuracion podremos cambier el tema
  sin problemas.
- `posts` podremos eliminar y crear otros para listar nuestros propios post
- `colors.js` en este tema podemos cambiar entre tema claro y oscuro
  y personalizar los colores, promarios, cabeceras, etc.

<!-- 

.

.

.

-->

#### Using Multiple Themes Together

Vamos a ver como combinar multiples temas para crear un sitio, usando
gatsby-theme-blog, gatsby-theme-notes y gatsby-mdx-embed, y veremos
conceptos como el de `component shadowing`

A. Creamos proyecto como siempre

```r
gatsby new multiple-themes https://github.com/gatsbyjs/gatsby-starter-hello-world
cd multiple-themes
```

B. Instalamos los temas `npm install gatsby-theme-blog gatsby-theme-notes`

C. En `gatsby-config.js` agregamos los temas e informacion basica <!-- %{tyhnmpp}% -->

```js
siteMetadata: {
  title: `Your Site Title`,
  description: `A description for your blazing fast site, using multiple themes!`,
  author: `Your name`,
  menuLinks: [
    { name: `Blog`, url: `/`, },
    { name: `Notes`, url: `/notes`, },
  ],
  social: [
      { name: "twitter", url: "https://twitter.com/amber1ey", },
      { name: "github", url: "https://github.com/amberleyromo", },
  ],
},

plugins: [
  { resolve: `gatsby-theme-blog`, options: { basePath: `/blog`, }, },
  { resolve: `gatsby-theme-notes`, options: { basePath: `/notes`, }, },
],
```

C-2. Iniciamos proyecto y deberian de crearce las carpetas de nuestro
tema

```r
my-blog
├── content
│   ├── assets
│   └── posts
│   └── notes
├── src
```

D. Agregamos un blog `content/posts/hello-posts.md`

```md
---
title: My first blog post
date: 2020-02-15
---

Multiple themes are great!
```

E. Agregamos algunas notas `content/note/hello-notes.md`

```md
---
title: My first note
date: 2020-02-20
---

Multiple themes are awesome!
```

F. Reiniciamos el servicio y podremos entrar a `localhost:8000/blog/hello-posts`
o a `localhost:8000/notes/hello-notes`

G. Agregamos `content/assets/avatar.png|.jpg`

H. Eliminamos `page/index.js` y creamos `src/gatsby-theme-blog/` para
que el plugin pueda trabajar en esta

I. Modificamos el `basePath` del tema de blog para que nuestra pagina
inicie con este tema.

```js
{ resolve: `gatsby-theme-blog`, options: { basePath: `/`, }, },
```

#### Shadow components

A. Creamos `src/gatsby-theme-blog/components/bio-content.js`

```js
import React, { Fragment } from "react"
import { Styled } from "theme-ui"
export default () => (
  <Fragment>
    Words by <Styled.a href="http://example.com/">Mario</Styled.a>.
    <br />
    Change me. Your awesome bio, about how great you are!
  </Fragment>
)
```

Ambos temas usan `theme-ui` para la configuracion de estilos como
colores y etc

B. Creamos `src/gatsby-plugin-theme-ui/index.js`

```js
import merge from "deepmerge"
import defaultTheme from "gatsby-theme-blog/src/gatsby-plugin-theme-ui/index"

export default merge(defaultTheme, {
  colors: {
    background: "ghostwhite",
    text: "black",
    primary: "mediumvioletred",
    modes: {
      dark: {
        background: "indigo",
        text: "ghostwhite",
        primary: "gold",
      },
    },
  },
})
```

#### Add another theme

Los temas pueden ser grandes como el de blog, pero igual pueden ser
pequeños como `gatsby-mdx-embed` que lo que hacen es permitir agregar
contenido de las redes sociales directo en markdown

A. Instalar `yarn add @pauliescanlon/gatsby-mdx-embed`

B. Lo agregamos en el `gatsby-config.js`

```js
 plugins: [ `@pauliescanlon/gatsby-mdx-embed`, ... ]
```

C. agregamos `content/posts/nuevo-post.md`

```text
---
title: Jason and Jackson Talk Themes
date: 2020-02-21
---
Here is a video about composing and styling themes with J&J!
<YouTube youTubeId="6Z4p-qjnKCQ" />
```

#### Add a navigation menu

Si recordamos agregamos `menuLinks` en nuestro `gatsby-config.js` ver
`%{tyhnmpp}%)`, el cual usaremos para crear nuestro menu de navegacion

A. Creamos `src/components/navigation.js`

```js
import React from "react"
import { Link, useStaticQuery, graphql } from "gatsby"
import { Styled, css } from "theme-ui"

export default () => {
  const data = useStaticQuery(
    graphql`
      query SiteMetaData {
        site {
          siteMetadata {
            menuLinks {
              name
              url
            }
          }
        }
      }
    `
  )
  const navLinks = data.site.siteMetadata.menuLinks
  return (
    <nav css={css({ py: 2, })} >
      <ul css={css({ display: `flex`, listStyle: `none`, margin: 0, padding: 0, })} >
        {navLinks.map(link => (
          <li css={css({ marginRight: 2, ":last-of-type": { marginRight: 0, }, })} >
            <Styled.a as={Link} to={link.url} css={css({ fontFamily: `heading`, fontWeight: `bold`, textDecoration: `none`, ":hover": { textDecoration: `underline`, }, })}>
              {link.name}
            </Styled.a>
          </li>
        ))}
      </ul>
    </nav>
  )
}
```

Ahora creamos `src/gatsby-theme-blog/components/header.js`

```js
import React from "react"
import { css } from "theme-ui"
import Navigation from "../../components/navigation"
export default () => {
  return (
    <header>
      <div css={css({ maxWidth: `container`, mx: `auto`, px: 3, pt: 4, })} >
        <Navigation />
      </div>
    </header>
  )
}
```

Si recordamos el header por defecto del tema de blog que nos da por
defecto es el siguiente.

![30-header-default-theme-blog](./img/30-header-default-theme-blog.PNG)

Y con el header que le hicimos al agregar nuestro propio header ahora
luce se la siguiente manera.

**Resultado:** Podemos ver el siguiente sitio el cual usa el tema de
blog y notas para listar ver y listar blogs y notas practicamente sin
programar nada, solo agregando una estructura de carpetas y archivos
definida, y conociendola podemos sustituir componentes como lo hicimos
con el header.js.

![31-multuples-temas-blog-notas-y-mdx-y-shadowing](./img/31-multuples-temas-blog-notas-y-mdx-y-shadowing.gif)

<!-- 

.

.

.

-->

### Building a Theme

Vamos a aprender como estructurar y configurar el workspace de yarn para
crear temas de gatsby en el que crearemos dos `gatsby-theme-events` y
`site` cada workspace puede correr por si mismo.

A. Creamos un folder vacio con `package.json`

```json
{
  "private": true,
  "workspaces": ["gatsby-theme-events", "site"]
}
```

B. Configuramos workspaces

```r
.
├── gatsby-theme-events
│   └── package.json
├── site
│   └── package.json
└── package.json
```

C. en `gatsby-theme-events/package.json` ponemos

```json
{
  "name": "gatsby-theme-events",
  "version": "1.0.0",
  "main": "index.js", // este es una dependencia que ejecutaremos con node "site" es el que se desplegara
  "license": "MIT",
  "scripts": { "build": "gatsby build", "clean": "gatsby clean", "develop": "gatsby develop" }
}
```

D. Creamos `gatsby-theme-events/index.js` y va a estar `vacio` ya solo
lo agregamos para que se resuelva la ruta

E. En `site/package.json` agregamos

```json
{
  "private": true, // no lo vamos a publicar en npm
  "name": "site", // debe coincidir con 'workspaces' de package.json de la raiz
  "version": "1.0.0",
  "license": "MIT",
  "scripts": {
    "build": "gatsby build",
    "develop": "gatsby develop",
    "clean": "gatsby clean"
  }
}
```

F. Agregamos dependencias nuestro workspaces, ejecutando en el root
`yarn workspace site add gatsby react react-dom gatsby-theme-events@*`

Al terminar debemos de poder ver en `site/package.json`

```js
{
  "dependencies": {
    "gatsby": "^2.9.11",
    "gatsby-theme-events": "*",
    "react": "^16.8.6",
    "react-dom": "^16.8.6"
  }
}
```

G. Si ejecutamos `yarn workspaces info` veremos que site usa como
dependencia a gatsby-theme-events

```json
{
  "gatsby-theme-events": {
    "location": "gatsby-theme-events",
    "workspaceDependencies": [],
    "mismatchedWorkspaceDependencies": []
  },
  "site": {
    "location": "site",
+   "workspaceDependencies": ["gatsby-theme-events"],
    "mismatchedWorkspaceDependencies": []
  }
}
```

H. Agregamos [peer dependencies](https://nodejs.org/es/blog/npm/peer-dependencies/)
que son dependencias exclusivas para una dependencia haciendo que las de
arriba no encimen las de abajo (en la raiz). `yarn workspace gatsby-theme-events add -P gatsby react react-dom`

I. Instalamos dependencias ya que durante el desarrollo esta funcionara
como un sitio regular de gatsby (en la raiz). `yarn workspace gatsby-theme-events add -D gatsby react react-dom`

Al terminar deveriamos de ver `gatsby-theme-events/package.json` con

```json
{
  "peerDependencies": {
    "gatsby": "^2.9.11",
    "react": "^16.8.6",
    "react-dom": "^16.8.6"
  },
  "devDependencies": {
    "gatsby": "^2.9.11",
    "react": "^16.8.6",
    "react-dom": "^16.8.6"
  }
}
```

J. Ejecutamos ambos workspaces para ver que sodo ca bien

```r
yarn workspace site develop

yarn workspace gatsby-theme-events develop
```

Deberiamos de ver como al final se contruyen success.

Q. Creamos `gatsby-theme-events/data/events.yml` para poderle consultar
informacion

```yaml
- name: React Rally
  location: Salt Lake City, UT
  start_date: 2019-08-22
  end_date: 2019-08-23
  url: https://www.reactrally.com/

- name: DinosaurJS
  location: Denver, CO
  start_date: 2019-06-20
  end_date: 2019-06-21
  url: https://dinosaurjs.org/

- name: JSHeroes
  location: Cluj-Napoca, Romania
  start_date: 2020-04-23
  end_date: 2020-04-24
  url: https://jsheroes.io/

- name: The Lead Developer
  location: Austin, TX
  start_date: 2019-11-08
  end_date: 2019-11-08
  url: https://austin2019.theleaddeveloper.com/
```

L. Para poder leer yaml instalamos la siguiente dependencia

```r
yarn workspace gatsby-theme-events add gatsby-source-filesystem gatsby-transformer-yaml
```

- `gatsby-source-filesystem` nos permitira leer el archivo
- `gatsby-transformer-yaml` nos permitira trasformar el yaml a info

M. Agregamos las dependecias en `gatsby-theme-events/gatsby-config.js`

```js
module.exports = {
  plugins: [
    {
      resolve: "gatsby-source-filesystem",
      options: {
        path: "data",
      },
    },
    {
      resolve: "gatsby-transformer-yaml",
      options: {
        typeName: "Event",
      },
    },
  ],
}
```

L. reiniciar workspace `yarn workspace gatsby-theme-events develop` y
abrimos graphiql y debemos de ser capaces traer la info. del yaml con
la siguiente consulta.

```js
query MyQuery {
  allEvent {
    edges {
      node {
        name
      }
    }
  }
}
```

![32-consulta-a-yml-en-graphql](./img/32-consulta-a-yml-en-graphql.PNG)

#### Create a data directory using the onPreBootstrap lifecycle

Como requerimieto de nuestro tema es que el archivo y carpeta `gatsby-theme-events/data/events.yml`
exista, pero si no existe en el build gatsby se detendra con un error,
para evitar esto, vamos a crear la carpeta programaticamente.

A. Creamos `gatsby-theme-events/gatsby-node.js` para crear la carpeta

```js
const fs = require("fs")
// Make sure the data directory exists
exports.onPreBootstrap = ({ reporter }) => {
  const contentPath = "data"
  if (!fs.existsSync(contentPath)) {
    reporter.info(`creating the ${contentPath} directory`)
    fs.mkdirSync(contentPath)
  }
}
```

#### Set up to create data-driven pages

PENDIENTE....

### working withimages

- Fuente: www.gatsbyjs.org/docs/recipes/working-with-images/

Tenemos 3 formar de lidiar con imagenes

#### Importar con webpack (imagen como componente)

Importarlas con webpack, en el que las importamos como componente
y lo que psara es que webpack la pasara a la pcarpta public en el
build

```js
import FiestaImg from "../assets/fiesta.jpg"
...
<img src={FiestaImg} alt="A dog smiling in a party hat" />
```

#### Referencia a static folder

Nos aseg. que la imagen este en la carpeta startic con la sig. estruct.

```r
├── gatsby-config.js
├── src
│   └── pages
│       └── index.js
├── static
│       └── fiesta.jpg
```

Y agrregamos la ruta en nuestro codigo, en este caso la imagen esta en
la raiz de la carpeta estatica

```js
<img src={`fiesta.jpg`} alt="A dog smiling in a party hat" />
```

#### Consultando al sistema graphql de gatsby

<!-- consultar imagenes, consulta de imagenes, consultar varias imagenes -->

La ventaja de usar el plugin `gatsby-image` es que nos ahorra trabajo
para la optimizacion de las imagenes, como la carga en el tiempo correcto.

A. Instalar dependencias para manegar imagenes `yarn add gatsby-image gatsby-transformer-sharp gatsby-plugin-sharp`

B. agregar dependencias `gatsby-config.js`

```js
plugins: [ `gatsby-transformer-sharp`, `gatsby-plugin-sharp`, 'gatsby-image' ]
```

C. Tener las imagenes en `./src/images/`

C. Importamos `Img` de gatsby-image y useStaticQuery ya que createmos
una peticion

```js
import { useStaticQuery, graphql } from "gatsby" // to query for image data
import Img from "gatsby-image"; // to take image data and render it
```

Traer sola imagen

```js
const data = useStaticQuery(graphql`
  query {
    file(relativePath: { eq: "gatsby-astronaut.png" }) {
      childImageSharp {
        fluid {
          base64
          aspectRatio
          src
          srcSet
          sizes
        }
      }
    }
  }
`)

return ( <Img fluid={data.file.childImageSharp.fluid} alt="astronaut" /> )
```

#### Traer multiples imagenes

```js
const data = useStaticQuery(graphql`
  query {
    allFile(
      filter: {
        extension: { regex: "/(jpg)|(png)|(jpeg)/" }
        relativeDirectory: { eq: "dogs" }
      }
    ) {
      edges {
        node {
          base
          childImageSharp {
            fluid { base64 aspectRatio src srcSet sizes }
          }
        }
      }
    }
  }
`);

 return (
  <div>
    {data.allFile.edges.map(image => (
      <Img fluid={image.node.childImageSharp.fluid} alt={image.node.base.split(".")[0]} />
    ))}
  </div>

```

Cambiar resolucion, las imagenes se seguiran haciendo grandes o
pequeñas, la calidad=resolucion cambiara

```js
childImageSharp {
  fixed(width: 250, height: 250) {
    base64 aspectRatio src srcSet //le quitamoes sizes
  }
}
```

Para cambiar el `aspectRatio` lo hacemos de la siguiente manera
<!-- https://www.displayninja.com/what-is-aspect-ratio/ -->
```js
  <Img fluid={ {...image.node.childImageSharp.fixed, aspectRatio: 16/9} } alt={image.node.base} />
```

<!-- 

.

.

.

.

.

.
###################################################################
###################################################################
###################################################################

.

.

.

.

.

.

-->

## Mis Practicas

### Hacer consumo de API Rest para Data de gatsby

- [Fuente](https://www.gatsbyjs.org/packages/gatsby-source-rest-api/)

A. Instalamos dependencias

```bash
yarn add --save -D gatsby-source-rest-api
```

B. En el `gatsby-config.js` agregamos

```js
plugins: [
    //This plugin exists only once but can consume an array of endpoints
  {
    resolve: 'gatsby-source-rest-api',
    options: {
      endpoints: [
        'https://www.example1.com/posts',
        'https://www.example2.com/users',
      ],
    },
  },
],
```

C. Entramos a `http.../___graphql` para construir la consulta.

D. Hacemos la consulta en nuestro componente.

```js
import { useStaticQuery, graphql } from "gatsby"
export default () => {
  const data = useStaticQuery(
    graphql` {
      allRestApiProductos {
        edges {
          node {
            products {
              name
              price
            }
    } } } }
    `
  )
  return( <Layout> ...
```

### Leer archivo json para Data de gatsby

https://www.gatsbyjs.org/packages/gatsby-transformer-json/
