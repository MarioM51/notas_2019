# NextJS Crea tienda online

## index

- [NextJS Crea tienda online](#nextjs-crea-tienda-online)
  - [index](#index)
  - [Accesos rapidos](#accesos-rapidos)
  - [TODO: pendientes](#todo-pendientes)
  - [Resultados Finales](#resultados-finales)
    - [Introduccion](#introduccion)
    - [Fin seccion 5](#fin-seccion-5)
  - [1. Introduccion](#1-introduccion)
    - [XX. Navegar entre rutas estaticas](#xx-navegar-entre-rutas-estaticas)
    - [XX. Navegacion entre rutas dinamicas](#xx-navegacion-entre-rutas-dinamicas)
    - [XX. Navegacion entre rutas dinamicas dobles](#xx-navegacion-entre-rutas-dinamicas-dobles)
    - [32. Uso de Links](#32-uso-de-links)
    - [33. SEO con Next/Head](#33-seo-con-nexthead)
  - [Sección 5: Empezando con el E-Commerce](#sección-5-empezando-con-el-e-commerce)
    - [35, Agregar depedencias](#35-agregar-depedencias)
    - [37. Generando el Layout](#37-generando-el-layout)
    - [38. Variables de colores en SASS](#38-variables-de-colores-en-sass)
    - [39. Estructura del Header](#39-estructura-del-header)
    - [41. TopBar - Buscador](#41-topbar---buscador)
    - [42. Menu Web](#42-menu-web)
    - [43. Creando Backend Strapi con MongoDB](#43-creando-backend-strapi-con-mongodb)
  - [Sección 6: Sistema de usuarios](#sección-6-sistema-de-usuarios)
    - [48. Modal reutilizable](#48-modal-reutilizable)
    - [49. Sistema de formularios de Registro y Login](#49-sistema-de-formularios-de-registro-y-login)
    - [51. Validacion de formulario con Fromik y Yup](#51-validacion-de-formulario-con-fromik-y-yup)
    - [52 a 56. Registrando y Logueadn usuario en Strapi](#52-a-56-registrando-y-logueadn-usuario-en-strapi)
    - [57. Creando el AuthContext y useAuth](#57-creando-el-authcontext-y-useauth)
    - [58. Guardando token en el Context y recupararlo al cargar app](#58-guardando-token-en-el-context-y-recupararlo-al-cargar-app)
    - [61. Cerrar sesión](#61-cerrar-sesión)
    - [62. AuthFetch](#62-authfetch)

## Accesos rapidos

- [Documentacion Oficial de NextJS](https://nextjs.org/docs/getting-started)
- [Repositorio de Dependencias Yarn](https://yarnpkg.com/)
- [Libreria para usar sass/archivos scss](https://yarnpkg.com/package/node-sass)
- [Libreria semantic-ui](https://react.semantic-ui.com/)
- [Documentacion de Strapi](https://strapi.io/documentation/developer-docs/latest/getting-started/quick-start.html)
- [Formik: Crear y mantener estados de Formulario](https://yarnpkg.com/package/formik)
- [Yup: Validacion de Formulario](https://yarnpkg.com/package/yup)
- [Toast para notificaciones en react](https://yarnpkg.com/package/react-toastify)
- [Decodificar JWT](https://yarnpkg.com/package/jwt-decode)

## TODO: pendientes

- Ver por que al escribir en el formulario de login, de dispara el metodo
  `LoginForm(props)` cada vez que escribo

## Resultados Finales

### Introduccion

Vimos lo basico principalmente la navegacion entre rutas estaticas,
dinamicas y dinamicas dobles, y como capturar parametros/argumentos de
la ruta para mostrar ontenido personalizado/dinamico, como tambien crear
etiquetas del header como los meta de forma dinamica.

- [Codigo](./img/_1_intro.zip)
- [GIF](./img/_1_intro.gif)

### Fin seccion 5

Vimos la creacion del UI de nuestro home como la creacion inicial de
strapi, aun no lo usamos pero estamos listos para hacerlo.

- [Codigo](./img/_2_ui_home.zip)
- [GIF](./img/_2_ui_home.gif)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

-->

## 1. Introduccion

Vamos a nececitar las siguientes herramientas.

- [Robo 3T: the hobbyist GUI](https://robomongo.org/download)
- `yarn --version`
- `node --version`

**¿Que es Next.js?** Este es un motor SSR donde se renderiza el contenido,
del lado del servidor, lo cual para paginas con las que queremos visibilidad
este es ideal, ya que cuando usamos CSR el SEO falla.

**¿Diferencias con Gatsby?**: La diferencia es que Gatsby hace el render
en tiempo de compilazion, mientras que NextJS ase el render del lado del
servidor.

Crear proyecto: `yarn create next-app intro`

Iniciar proyecto: `yarn dev`
<!-- version para despliegue, version para desplegar-->
Crear version para produccion: `yarn build`

**Nota:** le agrege unas paginas para explicar como funciona

**Estructura de carpetas**: A diferencia de react tenemos

```r
.next/   # solo es contenido autogenerado principalmente para cuando estamos en desarrollo.
public/  # Contenido estatico como imagenes, svg, etc
styles/: # Tenemos los estilos globales como por modulo.
pages
  api/
    hello.js
  productos/
    trompo-chico.js  # http://localhost:3000/prodcutos/trompo-chico
  _app.js # el que se encarga del routing, todas las peticiones entran primero a esta
  index.js           # http://localhost/
  productos.js       # http://localhost:3000/prodcutos
```

### XX. Navegar entre rutas estaticas

**Introduccion:** A rutas estaticas nos referimos a que, cuando ponemos
la ruta se carga un archivo del mismo nombre.

**Objetivo** Solo debemos hacerlo como se hace en php, el cual ponemos
la ruta al archivo. **Ejemplo:** vamos a navegar a la pagina contacto
`x.com/contacto`, y dentro de contacto tenemos otra ruta ruta mapa, es
decir `x.com/contacto/mapa`.

A, Tenemos la misma estructura de archivos que tenemos arriba.

```r
pages
  contacto/
    mapa.js
  contacto.js
# de esta forma podemos haceptar rutas como las de abajo, donde cada
# peticion entrara al archivo que esta señalando
#   - x.com/contacto
#   - x.com/contacto/mapa
```

B, Tenemos el index de la sig manera `pages\index.js`

```js
import Head from 'next/head'
export default function Home() {
  return (
    <div>
      <Head><title>Create Next App</title><link rel="icon" href="/favicon.ico" /></Head>
      <h1>Hola Nextjs</h1>
    </div>
  )
}
```

C, Tenemos el contacto `pages\contacto.js`

- **NOTAR**: No necesitamos importar react, Nextjs ya lo hace por nosotros,
  y que tenemos el archivo en minusculas ya que es como accedera nuestro
  usuario.

```js
export default function Contacto() {
  return ( <div><h1>Contacto</h1></div> )
}
```

D, Tenemos lo de un elemento `pages\contacto\mapa.js`

```js
export default function Mapa() {
  return ( <div> <h1>Mapa</h1> </div> )
}
```

**Resultado:** Podemos navegar a estor archivos que creamos manualmente.

![01_first_run](./img/01-navegacion-rutas-estatica.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### XX. Navegacion entre rutas dinamicas

**Introduccion:** A rutas dinamicas nos referimos a que, cuando ponemos
la ruta no hay un archivo con ese nombre, solo se carga un producto.

A, Tenemos la siguiente estructura de carpetas

```r
pages
  productos/
    [producto].js
# de esta forma podemos aceptar rutas como las de abajo, donde tenemos
# el [producto] este lo tomara como un parametro que podremos capturar
#   - a.com/productos/trompo
#   - a.com/productos/valero
#   - a.com/productos/xxxxxx
```

B, En `pages\productos\[producto].js` tenemos

```js
import React from 'react';
import { useRouter } from 'next/router';
export default function Producto() {
//contiene la informacion de la ruta
  const router = useRouter();
  const productName = router.query.producto
  return (
    <div>
      <h2>Detalles del Producto: {productName}</h2>
    </div>
  )
}
```

**Resultado:** Podemos navegar dentro en cualquier ruta `a.com/productos/xxx...`
donde las x pueden ser lo que sea el largo que sea.

![02-navegacion-rutas-dinamicas-simple](./img/02-navegacion-rutas-dinamicas-simple.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### XX. Navegacion entre rutas dinamicas dobles

**Introduccion:** Podemos hacer lo mismo que arriba pero de mas niveles,
donde no solo la segunda parte de la peticion la tomara como dinamica,
si no que tambien la primera parte.

**Objetivo:** Supongamos que tenemos una coleccion de colecciones, por
ejemplo, tenemos una tienda de juegos, lo cual tenemos una relacion como
la sig. `tenemos muchas consolas, y cada consola tiene muchos juegos`.

A, Tenemos la siguiente estrucutra de carpetas/archivos

```r
pages/
  [consola]/
   [juego].js
# de esta forma podemos acceder a las siguietnes rutas
# - ej.com/xbox/halo
# - ej.com/xbox/portal
# - ej.com/ps4/crash
# - ej.com/ps4/star-wars
# - ej.com/wi/mario
# - ej.com/.../...  # osea en donde tenemos "..." podemos poner cualquier cosa
```

B, en `pages\[consola]\[juego].js`

```js
import React from 'react'; import { useRouter } from 'next/router';
export default function Juego() {
//contiene la informacion de la ruta
  const router = useRouter();
  const {consola, juego} = router.query;
  return (
    <div><h1>Juegos de {consola}</h1> <h2>Detalles del Juego: {juego}</h2></div>
  )
}
```

**Resultado:** Podemos hacer las siguiente peticiones, y tambien podriamos
tener 3 niveles o los que querramos.

![03-navegacion-rutas-dinamicas-doble](./img/03-navegacion-rutas-dinamicas-doble.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 32. Uso de Links

**Introduccion** Ahi que tener CUIDADO de no usar las rutas de html nativo,
ya si funcionara pero eso hara que recarge de nuevo todos los componetes,
por lo que tendremos que usar los de next, para que carge solo los
componentes necesarios y no todos.

```js
import Head from 'next/head' import Link from 'next/link'
export default function Home() {
  return (
    <div>
      <h1>Hola Nextjs</h1>
// FUNCIONARA PERO RECARGARA TODO DE NUEVO
      <a href="/contacto" >NO USAR ESTE LINK</a>
      <br /><br />
// FORMA CORRECTA EN NEXT
      <Link href="/contacto"><a>Contacto</a></Link>
      <br /><br />
// FUNCIONA IGUAL PARA LAS RUTAS DINAMICAS
      <Link href="/xbox/portal"><a>xbox / Portal</a></Link>
    </div>
  )
}
```

**Resultado:** Podemos navegar entre rutas, y no se recarga toda la
pagina de nuevo, si no que carga como si fuera una SPA.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 33. SEO con Next/Head

**Objetivo** Aprender como podemos agregar informacion meta y parecidos,
los cuales mejoran nuestro SEO

```js
  import React from 'react'; import { useRouter } from 'next/router';
//importamos el componente
+ import Head from 'next/head'
  export default function Juego() {
    const router = useRouter(); const {consola, juego} = router.query;
    const title = juego[0].toUpperCase()+juego.substring(1).toLowerCase();
    return (
      <>
//Esto es lo que ira dentro de nuestro <header>
+       <Head>
+         <title>{title}</title>
+         <meta description={"juego de " + consola}/>
+       </Head>
        <h1>Juegos de {consola}</h1> <h2>Detalles del Juego: {juego}</h2>
      </>
    )
  }
```

**Resultado:** Tenemos meta informacion agregada de forma dinamica.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

## Sección 5: Empezando con el E-Commerce

### 35, Agregar depedencias

0, Crear proyecto: `yarn create next-app nextjs`

A, Agregar scss a nextjs:

A,1, instalar libreria: `yarn add sass` <!-- usar scss, usar sass, agregar scss, agregar sass-->

A,2, crear estilos globales: `styles\scss\global.scss`, la ruta da igual
solo debemos de tener la libreria de node-sass

```scss
.container { background-color: lightgrey; }
```

A,3, Agregamos archivo en `pages\_app.js` para que todos los componentes
los puedan ver

```js
+ import './../styles/scss/global.scss'
  export default function MyApp({ Component, pageProps }) { return <Component {...pageProps} /> }
```

B, Usar estilos

```js
export default function Home() { 
  return (
// Podemos ver que usamos la clase como en css normal
!   <div class="container"> <h1>Hola</h1> </div>
  )
}
```

A,**Resultado**: Podemos ver los estilos aplicados en nuestro scss

<!-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->

B, Agregar semmanticUI: [Fuente](https://react.semantic-ui.com/usage#install)

B,1, Instalar libreria: `yarn add semantic-ui-react semantic-ui-css`

B,2, importamos libreria en `pages\_app.js`

```js
+ import 'semantic-ui-css/semantic.min.css'
  import './../styles/scss/global.scss' export default function MyApp({ Component, pageProps }) { return <Component {...pageProps} /> }
```

B,3, Comprobar que funciona semanticUI en `pages\index.js`

```js
+ import { Button } from 'semantic-ui-react'
  export default function Home() { return (
      <div>
        <h1>Hola desde NextJS</h1>
+       <Button primary>Primary</Button>
      </div>
    )
  }
```

B.**Resultado:** Tenemos un boton ya con estilos de semanticUI

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 37. Generando el Layout

<!-- uso de layouts -->

**Objetivo:** Agregar la estructura que compartiran todas nuestras,
paginas, por lo mientras solo agregara un contenedor.

A, Agregamos `src\layouts\BasicLayout\BasicLayout.scss`

- agregaremos que el contenido abarque todo el alto de la pagina, por
  si queremos agregar un footer este no se despegue del piso

```scss
.basic-layout {
  background-image: url("/bg-home.jpg");
  //expandir la imagen de fondo
  background-size: cover;
  background-position: center;
  //hacer scroll nativo, evitar scroll vertical
  max-height: 100vh;
  .content {
    // evitar que se haga scroll vertical, sobrepasa el alto
    height: calc(100vh - 110px);
    background-color: $background-grey-light;
    //agregar escroll dentro de div
    overflow-y: scroll;
    //ocultar barra de scroll, ocultar scroll, ocultar barra de scroll
    &::-webkit-scrollbar {
      display: none;
    }
  }
}
```

B, Agregamos `src\layouts\BasicLayout\BasicLayout.js`

- Por lo mientras nuestro layout solo les agregara el contenedor

```js
import { Container } from 'semantic-ui-react'
export default function BasicLayout(props) {
  const { children } = props;
  return (
// el fluid pegara los bordes horizontales, si no lo ponemos se separara de los limites horizontales
    <Container fluid className="basic-layout" >
      <h2>BasicLayout</h2>
      <Container className="content">
        {children}
      </Container>
    </Container>
  )
}
```

C, Para importar facilmente agregamos `src\layouts\BasicLayout\index.js`

<!-- importacion default, my-default, mdefa -->

```js
export { default } from './BasicLayout'
```

D, Usamos nuestro layout

```js
+ import BasicLayout from '../src/layouts/BasicLayout'
  export default function Home() { return (<div>
+       <BasicLayout>
           <h1>Home</h1>
+       </BasicLayout> 
</div> ) }
```

**Resultado:** Tenemos listo nuestro layout para agregar contenido en
comun en todas nuestras paginas.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 38. Variables de colores en SASS

<!-- uso de variables scss -->

A, Agregamos `src\styles\colors.scss`

```scss
$primary: #ff5400;
$success: #69af00;

$font-light: #fff; $font-grey: #778; $font-dark: #000;

$border-light: #fff; $border-grey-light: #a7a7a7; $boder-grey: #444444;

$background-light: #fff; $background-grey-light: #eee; $background-grey: #a7a7a7;
$background-grey-dark: #363636; $background-dark: #151515; $background-dark-absolute: #000;
```

B, Lo importamos en `src\styles\index.scss`

```css
+ @import './colors.scss';
  @import './global.scss'; @import './../layouts/BasicLayout/BasicLayout.scss';
```

C, Importamos el index a nuestro `pages\_app.js`

```js
import '../styles/scss/index.scss'
```

D, Ya lo podmos usar dode querramos

```css
.basic-layout { min-height: 100vh;
+ background-color: $background-grey-light;
```

**Resultado:** Tenemos los colores listos para reutilizar, y asi evitarnos
problemas.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 39. Estructura del Header

0A, Crear componente header

0A,1, Creamos los tres archivos del componente Header

estilos `src\Header\Header.scss`

```scss
.header {
  padding: 1rem;
  background: $background-grey-dark;
}
```

importar estilos en `styles\scss\index.scss`

```scss
@import '../../src/layouts/BasicLayout/BasicLayout.scss';
+ // Componentes...
+ @import '../../src/Header/Header.scss';
```

codigo `src\Header\Header.js`

```js
export default function Header() {
  return (
// los estilos se agregan en el styles\scss\index.scss
    <div class="header">
      <h3>TopBar</h3>
      <h3>Menu</h3>
    </div>
  )
}
```

facilitar importacion `src\Header\index.js`

```js
export { default } from './Header'
```

0A,1, Agregamos componente Header

<!-- Agregar imagen, usar imagen -->

A, Agregamos una imagen en `public\logo.png`

B, Agregar componente Topbar

0B,1, Creamos los tres archivos del componente Header

B,1 Agregar estilos `src\components\Header\TopBar\TopBar.scss`

- **Nota:** No olvidar importar en `styles\scss\index.scss`

```scss
.top-bar {
  background-color: $background-dark;
  height: 5em;
  .ui.grid {
    margin: 0; padding: 0;
    .column { padding: 0; }
  }

  &__left {
    display: flex !important;
    align-items: center;
    img {
      height: 4em;
      transition: all 0.5s;
      &:hover {
        transform: translate(-1em);
      }
    }
  }
}
```

C, En `src\components\Header\TopBar\TopBar.js`

```js
  import Link from 'next/link'
  import { Container, Grid, Image, Input } from "semantic-ui-react";
  export default function TopBar() {
    return (
      <div className="top-bar"> <Container> <Grid className="top-bar">
            <Grid.Column width={8} className="top-bar__left" >
+             <Logo />
            </Grid.Column>
            <Grid.Column width={8} className="top-bar__right" ><h2>Buscador</h2></Grid.Column>
          </Grid>
        </Container>
      </div>
    )
  }

  // las rutad de lo que hay en public lo toma como si fuera la raiz 
  function Logo() {
    return (
      <Link href="/" ><a>
+       <Image src="/logo.png" alt="logo" />
      </a>
      </Link>
    );
  }
```

**Resultado:** Tenemos la barra superior con una imagen, que si le pasamos
el mause encima hara un pequeño movimiento.

![04_header_top_bar_1](./img/04_header_top_bar_1.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 41. TopBar - Buscador

**Objetivo:** Crear input de busqueda

Creamos componente `src\components\Header\TopBar\TopBar.js`

```js
import { Container, Grid, Image, Input } from "semantic-ui-react"; 
export default function TopBar() {
  return (<div className="top-bar">
    <Container><Grid className="top-bar"><Grid.Column width={8} className="top-bar__left" ><Logo /></Grid.Column>
          <Grid.Column width={8} className="top-bar__right" >
+             <Search />
          </Grid.Column>
        </Grid> </Container> </div> )
}

+ function Search() {
+   return ( <Input id="search" icon={{name:"search"}} /> );
+ }
```

Agregamos estilos

```scss
.top-bar {
  ...
  &__left {... }

+ &__right {
    display: flex !important; align-items: center; justify-content: flex-end;
    .input {
      align-items: center; width: 90%; max-width: 30em;
      input {
        background-color: transparent !important; border: 0; border-bottom: 2px solid $primary !important;
// al seleccionar input cambia de color, evitar que cambie el color del input
        color: $primary !important; padding: 5px 5px; font-size: 20px; font-weight: bold;
      }
// estilos del icono del search
      i {
        color: $primary; opacity: 1 !important; font-size: 20px;
      }
    }
  }
```

**Resultado** Tenemos el menu de navegacion

![05_header_input_search](./img/05_header_input_search.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 42. Menu Web

**Objetivo:** Crear Menu

A, Creamos los estilos `src\components\Header\Menu\Menu.scss`, y los
importamos en `src\styles\index.scss`

```scss
.menu {
  background-color: $background-grey-dark; border-radius: 0;
  .ui.grid { margin: 0; }
  &__left, &__right {
    padding: 0 !important; display: flex !important; align-items: center;
    .ui.menu {
      border: 0; box-shadow: none; height: 100%; background: transparent;
      .icon {
        margin-right: 10px !important; color: $primary;
      }

      .item {
        color: $font-light !important; border-radius: 0 !important;
        &:hover { color: $font-light; background-color: $background-dark-absolute;
          &:before { background-color: $background-dark-absolute; }
        }
        &.m-0 i { margin: 0 !important; }
      }
    }
  }
  &__right { display: flex; justify-content: flex-end; }
}
```

B, Creamos componente Menu `src\components\Header\Menu\Menu.js`

```js
import React from 'react' import Link from 'next/link' import { Container, Menu as SMenu, Grid, Icon, Label } from "semantic-ui-react";
export default function Menu() {
  return (
    <div className="menu"> <Container> <Grid>
        <Grid.Column className="menu__left" width={10}><MenuPlatform /></Grid.Column>
        <Grid.Column className="menu__right" width={6}><MenuUser /></Grid.Column>
    </Grid> </Container> </div>
  )
}

function MenuPlatform() {
  return (
    <SMenu>
      <Link href="/playstation"><SMenu.Item as="a">PlayStation</SMenu.Item></Link>
      <Link href="/xbox"><SMenu.Item as="a">XBox</SMenu.Item></Link>
      <Link href="/WiiU"><SMenu.Item as="a">WiiU</SMenu.Item></Link>
    </SMenu>
  )
}

function MenuUser() {
  return (
    <SMenu><Link href="/playstation">
      <SMenu.Item><Icon name="user outline" />Mi Cuenta</SMenu.Item>
    </Link></SMenu>
  )
}
```

C, Lo agremos en `src\components\Header\Header.js`

```js
  import TopBar from "./TopBar";
+ import Menu from "./Menu";
  export default function Header() {
    return (<div className="header"><TopBar />
+      <Menu />
  </div> ) }
```

**Resultado:** Creamos el sig. menu

![06_header_menu_top_nav](./img/06_header_menu_top_nav.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 43. Creando Backend Strapi con MongoDB

- Fuente [Documentacion de Strapi](https://strapi.io/documentation/developer-docs/latest/getting-started/quick-start.html)

**Objetivo:** Crear el backend y para hacerlo rapido haremos uso de Strapi
y Mongo

A, Creamos una BD en mongo en mi caso termine con: `mongodb://localhost:27017/chat_bd`

B, Crear proyecto de strapi: `npx create-strapi-app carro-de-madera`, y
luego seguimos el wizard cli que nos da

**Resultado:** Tenemos un generico backend/CRM funcionando

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

.

-->

----

----

----

<!-- 

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

-->

## Sección 6: Sistema de usuarios

### 48. Modal reutilizable

<!-- crear modal -->

**Objetivo:** Para que el usuario se pueda registrar y loguear, le daremos
un modal al usuario para que este lo haga.

A, Creamos estilos `src\components\Modal\Modal.scss`

```scss
.mymodal {
  .header { background-color: $primary !important; display: flex !important; justify-content: space-between !important;
    i, span { color: $font-light; }
    i { font-size: 25px; margin-right: -15px; &:hover { cursor: pointer; }
    }
  }
}
```

B, Creamos componente `src\components\Modal\Modal.js

```js
import React from 'react'
import { Modal as SModal, Icon } from "semantic-ui-react";

export default function Modal(props) {
  // --agregar propiedades no controladas--, lo que nos permite agregar los props que nosotros
  // querramos sin tener uque declararlos, por ejemplo si le agregamos size="large" este se guardara
  // en res... y lo se agregara automaticamente.
  const { children, show, setShow, title, ...res } = props;

  const onClose = () => { setShow(false) }

  return (
    <SModal className="mymodal" open={show} onClose={onClose} {...res}>
      <SModal.Header>
        <span>{title}</span>
        <Icon name="close" onClick={onClose} />
      </SModal.Header>
      <SModal.Content>
        {children}
      </SModal.Content>
    </SModal>
  )
}
```

C, Usamos modal en `src\components\Header\Menu\Menu.js`

```js
  import Link from 'next/link' import { Container, Menu as SMenu, Grid, Icon, Label } from "semantic-ui-react";
+ import { useState } from 'react'
+ import Modal from '../../Modal/Modal';

  export default function Menu() {
+   const [showModal, setShowModal] = useState(false);
+   const onShowModal = () => setShowModal(true);
    return (
      <div className="menu">
      <Container> <Grid> <Grid.Column className="menu__left" width={10}> <MenuPlatform /> </Grid.Column>
          <Grid.Column className="menu__right" width={6}>
// al darle click mostrara el modal          
            <MenuUser onShowModal={onShowModal} />
          </Grid.Column>
//declaramos el modal, pero por defecto estara oculto
+         <Modal title="Registro" show={showModal} setShow={setShowModal} size="small" >
+           <h2>Contenido de modal</h2>
      </Modal> </Grid> </Container>
      </div>
    )
  }
  function MenuPlatform() {...}
  function MenuUser(props) {
+   const { onShowModal } = props;
    return ( <SMenu>
//mostramos el modal
#       <SMenu.Item onClick={onShowModal}><Icon name="user outline" />Mi Cuenta</SMenu.Item>
      </SMenu>
    ) }
```

**Resultado:** Podemos mostrar ocultar un modal

![07_mostrar_modal](./img/07_mostrar_modal.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 49. Sistema de formularios de Registro y Login

<!-- cambiar contenido de modal por boton -->

**Objetivo** Vamos a poner una navegacion dentro del modal entre el
formulario de registro y el de login por medio de un boton/link dentro
de cada formulario.

A, Creamos estilos en este caso que se aplique en todos `src\styles\global.scss`

```scss
.ui.form {
  .ui.input {
    input { border: 1px solid $border-grey-light; font-size: 16px; }
  }
  .actions { display: flex; align-items: center; justify-content: space-between; }
  .ui.button[type^="submit"] { background-color: $primary !important; color: $font-light; margin: 0; }
}
```

B, Cremos el Formulario de Login `src\components\Auth\LoginForm\LoginForm.js`

```js
import { Button, Form } from 'semantic-ui-react';
export default function LoginForm(props) {
  const { closeModal, setShowLogin } = props;
  return (
    <Form className="singup-form">
      <Form.Input name="email"    type="text" placeholder="Email" /> <Form.Input name="password" type="password" placeholder="Contraseña" />
      <div className="actions">
        <Button onClick={() => {setShowLogin(false)}}>ir a Registro</Button> <Button onClick={closeModal} type="submit" >Entrar</Button>
      </div>
    </Form>
  )
}
```

C, Creamos Formulario de registro `src\components\Auth\SingUpForm\SingUpForm.js`

```js
import { Button, Form } from 'semantic-ui-react';
export default function SingUpForm(props) {
  const { closeModal, setShowLogin } = props;
  return (
    <Form className="login-form">
      <Form.Input name="username" type="text" placeholder="username" /> <Form.Input name="email"    type="text" placeholder="Email" />
      <Form.Input name="password" type="password" placeholder="Contraseña" /> <Form.Input name="confirmPassword" type="password" placeholder="Confirma Contraseña" />
      <div className="actions">
        <Button onClick={() => {setShowLogin(true)}}>ir a Login</Button> <Button onClick={closeModal} type="submit" >Crear Cuenta</Button>
      </div>
    </Form>
  )
}
```

D, Creamos componente que mostrara uno u otro formulario

```js
import React, { useState } from 'react'; import LoginForm from "./LoginForm"; import SingUpForm from "./SingUpForm";
export default function Auth(props) {
  const { closeModal, setShowLoginState, showLoginState } = props;
  return showLoginState 
    ? <LoginForm closeModal={closeModal} setShowLogin={setShowLoginState} />
    : <SingUpForm closeModal={closeModal} setShowLogin={setShowLoginState} />
  ;
}
```

E, Usamos nuestro componente En `src\components\Header\Menu\Menu.js`

```js
  import { useState } from 'react' import Link from 'next/link' import { Container, Menu as SMenu, Grid, Icon, Label } from "semantic-ui-react"; import Modal from '../../Modal/Modal';
+ import Auth from '../../Auth/Auth';
  export default function Menu() {
    const [showModal, setShowModal] = useState(false); const onShowModal = () => setShowModal(true);
// el que decidira que formulario se muestra
+   const [ showLoginState, setShowLoginState] = useState(true);
    return (
      <div className="menu"> <Container> <Grid>
          <Grid.Column className="menu__left" width={10}> <MenuPlatform /> </Grid.Column>
          <Grid.Column className="menu__right" width={6}> <MenuUser onShowModal={onShowModal} /> </Grid.Column>
// cambiamos denamicamente el titulo del modal
#         <Modal title={showLoginState ? "Login" : "Registro"} show={showModal} setShow={setShowModal} size="small" >
// mostramos componente que muestra nuestros formularios dependiendo de cual querramos
+           <Auth showLoginState={showLoginState} closeModal={ ()=>{setShowModal(false)} } setShowLoginState={setShowLoginState} showLoginState={showLoginState} />
          </Modal>
      </Grid> </Container> </div>
    )
  }
```

**Resultado:** Estamos mostrando ambos formularios en el modal.

![08_mostrar_login_o_registro](./img/08_mostrar_login_o_registro.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 51. Validacion de formulario con Fromik y Yup

<!-- Validacion de formulario con Fromik y Yup -->

**Objetivo:** Vamos a instalar y hacer la validacion y funcionalidad del
login.

A, Instalar Formik: `yarn add formik`

B, Instalar Yup: `yarn add yup`

> **Error:** `Form submission canceled because the form is not connected`.  
> **Descripcion:** No se capturan datos del formulario al darle submit,
> no se dispara elevento submit, problemas de formulario, error formulario.  
> **Causa:** Tenia en el boton del submit un evento onClick, al quitarselo
> ya empezo a funcionar de nuevo.  

C, Implementamos la validacion en `src\components\Auth\SingUpForm\SingUpForm.js`

```js
  import { Button, Form } from 'semantic-ui-react';
//importamos
+ import { useFormik } from 'formik';
+ import * as Yup from "yup";

//estado inicial/modelo de nuestro formulario
+ function initialFormState() {
+   return { username: "", email: "", password: "", confirmPassword: "", }
+ }

//validacion de neustro formulario
+function validationSchema() {
   return {
     username: Yup.string()
       .required("El nombre es es obligatorio")
       .min(3, 'Minimo 3 caracteres')
       .max(20, 'Maximo 20 caracteres'),
     email: Yup.string()
       .required("El Email es obligatorio")
       .email("Formato de email incorrecto"),
     password: Yup.string()
       .required("La contraseña es obligatoria"),
     confirmPassword: Yup.string()
       .required("Confirmacion de contraseña obligatoria")
//con el paren podemos traer otros campos
       .test('passwords-match', 'Las contraseñas no coinciden',
            function(value){
              return this.parent.password === value
            }
        )
    }
+ }

  export default function SingUpForm(props) {
    const { closeModal, setShowLogin } = props;

//usar Formik
+   const formik = useFormik({
+     initialValues: initialFormState(),
//Usar Yup
+     validationSchema: Yup.object(validationSchema()),
//Aqui nos llegara la infomacion del formulario
+     onSubmit: (formData) => {
+       console.log(formData);
+     }
+   });

    return (
     <Form className="login-form" 
// enlasamos el formulario a Formik
+         onSubmit={formik.handleSubmit}>
        <Form.Input 
          name="username"
          type="text"
          placeholder="username"
// enlazar input
+         onChange={formik.handleChange}
// mostrar error de input
+         error={formik.errors.username}
        />
// lo mismo para los demas
        <Form.Input  name="email" type="text" placeholder="Email"
+           onChange={formik.handleChange} error={formik.errors.email} />
        <Form.Input name="password" type="password" placeholder="Contraseña"
+           onChange={formik.handleChange} error={formik.errors.password} />
        <Form.Input name="confirmPassword" type="password" placeholder="Confirma Contraseña"
+           onChange={formik.handleChange} error={formik.errors.confirmPassword} />
        <div className="actions"> <Button onClick={() => {setShowLogin(true)}}>ir a Login</Button>
// CUIDADO: si tenemos un evento onClick useFormik.onSubmit no se disparara
!         <Button type="submit" >Crear Cuenta</Button>
        </div>
      </Form>
    )
  }
```

**Resultado:** Tenemos la validacion de nuestro formulario de una forma
facil y rapida.

![08_validacion_de_formulario](./img/08_validacion_de_formulario.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 52 a 56. Registrando y Logueadn usuario en Strapi

<!-- Registrando y Logueadn usuario en Strapi -->

**Introduccion** Strapi por defecto trae diferentes plugins instalados,
entre ellos el de usuarios y roles, los cuales expone una APIs publicas
para registrar usuarios, estas podemos verlas en `strapiUI -> Configuracion ->`
`Plugins de usuarios -> roles y permisos -> public` y entre los desplegables
buscamos a `register` y veremos que tenemos la ruta `/auth/local/register`
para crear usuarios.

**NOTA:** En la pestaña de strapi `Creador de Tipos de Contenido` podemos
agregar o quitar campos que queremos que tengan nuestros usuarios, pero
nosotros dejaremos los campos por defecto.

**Objetivo:** Mandaremos una peticion http a nuestra instancia Strapi,
construllendo el DAO y la configuracion.

A, **Enviar peticion a Strapi**

A,1, Creamos variables estaticas

```js
export const BASE = 'http://127.0.0.1:1337';
```

A,2, Crear DAO de usuarios

```js
import { BASE, ERRORS } from "./../Constants";

const PATH_REGISTER = `${BASE}/auth/local/register`;
const PATH_LOGIN = `${BASE}/auth/local`;

export async function addUser(user) {
  delete user.confirmPassword;
  const params = buildPostParams(user);
  const result = myFetch(PATH_REGISTER, params);
  return result;
}

export async function getUserWithToken(email, password) {
  const credentials = { identifier: email, password: password }
  const params = buildPostParams(credentials);
  const result = myFetch(PATH_LOGIN, params);
  return result;
}

async function myFetch(url, params) {
  console.info('Peticion: ', url, params);
  const response = await fetch(url, params);
  const result = await response.json();
  console.info('Respuesta: ', result);
  if(!response.ok) { handleError(result); }
  return result;
}

function handleError(badResponse) {
  let message;
  try { message = badResponse.data[0].messages[0].message; }
  catch {                                              throw ERRORS.ERROR_MSG_NOT_FOUND; } //error al conceguir el mensaje de error del error
  if(message === 'Email is already taken.') {          throw ERRORS.ERROR_EMAIL_ALREADY_EXISTS; }
  if(message === "Email already taken") {              throw ERRORS.ERROR_USERNAME_ALREADY_EXISTS; }
  if(message === "Please provide your password.") {    throw ERRORS.BAD_REQUEST; }
  if(message === "Identifier or password invalid.") {  throw ERRORS.BAD_PASSWORD; }
  throw ERRORS.UNKNOW;
}

function buildPostParams(body, extraHeaders=null) {
  return { method: "POST", headers: { "Content-Type": "application/json", ...extraHeaders }, body: JSON.stringify(body), };
}
```

A,3, Crear Service de usuarios `api\services\UserService.js`

```js
import { addUser, getUserWithToken } from "./../daos/UserDAO";
export async function registerUser(user) {
  let response = await addUser(user);  
  validateToken(response?.jwt);
  return response;
}

export async function login(formData) {
  let loginResponse = await getUserWithToken(formData.email, formData.password);
  validateToken(loginResponse?.jwt);
  return loginResponse;
}

function validateToken(token) {
  const tokenIsNull       =  token == null; const tokenIsNotAString = !(typeof token === 'string'); const tokenIsEmply      =  (token.length === 0);
  if(tokenIsNull || tokenIsNotAString || tokenIsEmply) {
    console.error('All must be false: tokenisNull:', tokenIsNull, 'tokenisNotAString: ', tokenIsNotAString, 'tokenisEmply: ', tokenIsEmply);
    throw ERRORS.NOT_TOKEN;
  }
}
```

B, **Feedback a usuario de registro**

B,1, Instalar toast: `yarn add react-toastify` <!-- usar toast, mostrar toast -->

B,2, Importmaos toas a nuestro proyecto en `pages\_app.js` y lo
agregamos en nuestra raiz.

```js
  import 'semantic-ui-css/semantic.min.css' import './../src/styles/index.scss'
// necesitamos tanto los estilos como el contenedor
+ import "react-toastify/dist/ReactToastify.css";
+ import { ToastContainer } from 'react-toastify';
  export default function MyApp({ Component, pageProps }) {
    return (
      <>
        <Component {...pageProps} />
//lo agregamos en la raiz para que los demas componetes lo puedan ver
+       <ToastContainer
+         position="top-right" autoClose={5000} hideProgressBar newestOnTop
+         closeOnClick rtl={false} pauseOnFocusLoss={false} draggable pauseOnHover />
      </>
    );
  }
```

C, **Registro usuario**, implementar registro en la UI

C,1, Usamo el toast y DAO para registrar usuario en `src\components\Auth\SingUpForm\SingUpForm.js`

```js
+ import UserDAO from './../../../../api/daos/UserDAO'
+ import { toast } from 'react-toastify';

  export default function SingUpForm(props) { const { closeModal, setShowLogin } = props;
    const [ loading, setLoading ] = useState(false);

    const formik = useFormik({ initialValues: initialFormState(), validationSchema: Yup.object(validationSchema()),
      onSubmit: (formData) => {
// hacemos la peticion empezando por mostrar el loading
+       setLoading(true);
+       UserDAO.addUser(formData)
+         .then(resp => {
// oculatamos loading y damos feed de registro exitoso
+           setLoading(false);
+           setShowLogin(true);
+           toast.success('Usuario registrado exitosamente');
+         })
//mostramos feed de error en login
          .catch(error => {
            setLoading(false);
            if(error?.msg != null && typeof error.msg === 'string') { toast.error(error?.msg); }
            else { toast.error('Error, intente mas tarde'); }
          })
        ;//fin de peticion mal o bien
      }
    });
    return (
      ...
        <div className="actions"><Button onClick={() => {setShowLogin(true)}}>ir a Login</Button>
          <Button type="submit"
//SemanticUI ya implementa en los botones un fedd de loading
+           loading={loading}>
            Crear Cuenta
          </Button>
        </div>
    )
```

D, **Login usuario**, implementar login en la UI `src\components\Auth\LoginForm\LoginForm.js`

```js
import React, { useState } from 'react'; import { Button, Form } from 'semantic-ui-react'; import { useFormik } from 'formik'; import * as Yup from "yup"; import { login } from './../../../../api/services/UserService' import { toast } from 'react-toastify';
export default function LoginForm(props) {
  const { closeModal, setShowLogin } = props;
  const [ loading, setLoading ] = useState(false)
  const formik = useFormik({
    initialValues: initialFormState(),
    validationSchema: Yup.object(validationSchema()),
//Aqui nos llegara la infomacion del formulario
    onSubmit: (formData) => { setLoading(true);
      login(formData)
        .then(resp => { setLoading(false); closeModal(); toast.success('Usuario Logueado'); })
        .catch(error => {
          setLoading(false);
          if(error?.msg != null && typeof error.msg === 'string') { toast.error(error?.msg); } else { console.error(error); toast.error('Error, intente mas tarde'); }
        })
      ;
    }
  });

  return (
    <Form className="singup-form" onSubmit={formik.handleSubmit} >
      <Form.Input  name="email" type="text" placeholder="Email" onChange={formik.handleChange} error={formik.errors.email} />
      <Form.Input  name="password"  type="password" placeholder="Contraseña" onChange={formik.handleChange} error={formik.errors.password} />
      <div className="actions">
        <div>
          <Button type="button" onClick={()=>{setShowLogin(false)}}>ir a Registro</Button>
          <Button type="button" onClick={ ()=>{ console.log("no implementado"); } }>¿Olvidaste tu contraseña?</Button>
        </div>
        <Button type="submit" loading={loading} >Entrar</Button>
      </div>
    </Form>
  )
}
function initialFormState() { return { email: "", password: "", } }
function validationSchema() {
  const msgErrorPass = 'La contraseña debe estar entre 6 y 16 caracteres';
  return {  email: Yup .string() .required("El Email es obligatorio") .email("Formato de email incorrecto"), password: Yup .string() .required("La contraseña es obligatoria") .min(6, msgErrorPass).max(16, msgErrorPass), }
}
```

**Resultado:** Podemos hacer tanto rregistro como login de usuarios

![09_realizar_registro_y_login](./img/09_realizar_registro_y_login.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 57. Creando el AuthContext y useAuth

<!-- uso de useContext, compartir info. entre componentes, uso de context -->

**Intruduccion:** Ya tenemos la creacion del login junto con su registro,
ahora lo que nos falta es tener disponible este token, en cualquier parte
de la app, para lo cual usaremos un `useContext`.

**Objetivo** Crear un `useContext` donde guardaremos el token, y asi
tenerlo disponible para enviarlo desde cualquier parte de nuestra app
al servidor.

A, Creamos creador/fabrica de Context en este caso de Auth `src\contexts\AuthContext.js`

- `NOTA`: Una una cosa es crearlo y otra aobjenerlo, este es para crearlo

```js
import { createContext } from 'react';
// objeto que guardara lo relacionado con el auth
const AuthContext = createContext({
  auth: undefined,         // info del usuario
  persistAuth: () => null, // persistir info de auth
  cleanAuth: () => null,   // limpiar info de auth
  reloadAuth: () => null,  // re-cargar info de auth en caso de reiniciar navegador
});
export default AuthContext;
```

B, Importamos Context a la raiz de nuestro proyecto `pages\_app.js`

```js
import 'semantic-ui-css/semantic.min.css' import "react-toastify/dist/ReactToastify.css"; import { ToastContainer } from 'react-toastify'; import './../src/styles/index.scss'
//useMemo nos ayudara para recordar informacion, en caso de que se recarge la
//aplicacion esta info se conservara, en este caso, para que recuerde nuestros
//contexts
! import React, { useMemo } from 'react';
+ import AuthContext from "../src/contexts/AuthContext";
  
export default function MyApp({ Component, pageProps }) {
//ejemplo: el "useMemo" en este caso si se re-asigna la info auth: {username: 'Mario', mail: 'mario@mail.com'}
// con los mismos datos este no se recargara, ya que en caso de no usarlo se recargara nuestra app de nuevo
+ const authData = useMemo(
+   () => {
+     return {
// NOTA: A FUTURO: haremos que esta info. sea la que llege del back
+       auth: {username: 'Mario', mail: 'mario@mail.com'},
+       persistAuth: () => null,
+       cleanAuth: () => null,
+       reloadAuth: () => null,
+     };
+   },
+   []
+ );

  return (
// pasamos nuestro contex a la raiz de nuestra app
+   <AuthContext.Provider value={authData}>
      <Component {...pageProps} />
      <ToastContainer ... />
+   </AuthContext.Provider>
  );
}
```

C, Creamos Hook para llamar el contex que agregamos en la raiz `src\hooks\useAuth.js`

```js
import { useContext } from 'react';
import AuthContext from "./../contexts/AuthContext";
const useAuth = () => useContext(AuthContext)
export default useAuth;
```

D, Usar context

```js
+ import useAuth from "./../../../hooks/useAuth";
  export default function LoginForm(props) { 
// el hook traera nuestro context
+   const auth = useAuth();
+   console.log(auth);
```

**Resultado:** Podemos traer un componente en cualquier parte de nuestra
aplicacion, y dicho componetente tendra la informacion de mientras
nuestra aplicacion este viva.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 58. Guardando token en el Context y recupararlo al cargar app

A, Creamos funcion que asignara nuestro usuario como logueado

```js
export default function MyApp({ Component, pageProps }) {
// Creamos estado de auth para poderlo cambiar despues de X evento
// en este caso cuando se haga login
+ const [auth, setAuth] = useState(undefined);
//por si queremos forzar recargar el usuario, por ejemplo, cuando hagamos logout
+ const [realodUser, setRealodUser] = useState(false);

// funcion que ejecutaran los componetes mas abajo que compartan el
// el context, la cual asignara el estado guardando lo que ahi en el token
+ const loginUI = (token) => {
+   if(token) {
+     const userInfo = decodeJWT(token);
+     const authtemp = {userId: userInfo.id, token: token};
+     setAuth(authtemp);
+   } else {
+     setAuth(null);
+   }
+ }

// este se ejecutara solo cuando se carge la pagina, el cual si hay
// un token en el LocalStorage lo metera en el context si no ira un nulo
  useEffect(()=>{
    const token = getToken();
    if(token) {
      loginUI(token);
    } else {
      setAuth(null);
    }
    setRealodUser(false);
   }, [realodUser]);

  const authData = useMemo(
    () => (
      {
// ya no usamos info estatica, si no un estado que cambiara en algun momento
#       auth: auth,
// le pasamos la funcion para que la puedan ver componentes abajo
#       persistAuth: loginUI,
        cleanAuth: () => null,
// de esta manera cualquier componente podra recargar el estado global
// del usuario
!       reloadAuth: setRealodUser,
      }
    ),
    [auth]
  );

  return (...)
}

//funcion para extraer info del token
+ function decodeJWT(token) {
+   const userInfoRaw = window.atob(token.split(".")[1]);
+   const userInfo = JSON.parse(userInfoRaw);
+   return userInfo;
+ }
```

B, En `api\daos\UserDAO.js` creamos metodos para persistir el token, y
asi si se recarga la app, aun poder tener disponible esta info.

```js
export function saveToken(token) { localStorage.setItem(TOKEN, token); }
export function getToken() { return localStorage.getItem(TOKEN); }
```

C, En `api\services\UserService.js` usamos el guardar token

```js
  export async function login(formData) { let loginResponse = await getUserWithToken(formData.email, formData.password); validateToken(loginResponse?.jwt);
+   saveToken(loginResponse.jwt);
```

E, En `src\components\Auth\LoginForm\LoginForm.js` usamos el metodo
que creamos apenas, para que la info conseguida aqui pueda ser vista
por toda nuestra aplicacion.

```js
  export default function LoginForm(props) { const { closeModal, setShowLogin } = props; const [ loading, setLoading ] = useState(false)
//recordar que agregamos una funcion que cambiara el estado en nuestro
// root de la app
!   const auth = useAuth();
    const formik = useFormik({ initialValues: initialFormState(), validationSchema: Yup.object(validationSchema()),
      onSubmit: (formData) => { setLoading(true); login(formData).then(resp => { setLoading(false); closeModal(); toast.success('Usuario Logueado');
//mandamos token para poner nuestra app como en estado de logueado
+           auth.persistAuth(resp.jwt);
          })
```

**Resultado** Guardamos la informacion que conseguimos en el componente
de login en el contexto, y de esta forma tener esta info disponible
en toda nuestra app, incluso cuando **recarga** la pagina por que cuando
inicia el `useEffect` busca en el localStorage

<!--

#########################################

-->

----

<!--

#########################################

-->

### 61. Cerrar sesión

**Objetivo** Vamos a ocultar el boton para mostrar el formulario de login,
y mostrar un boton de logout, para que no se pueda loguar varias veces

A, Creamos la funcion de logout en `pages\_app.js` y la guaramos dentro
del contexto

```js
import { useRouter } from "next/router";
export default function MyApp({ Component, pageProps }) {
  ...
+ const router = useRouter();

+ const logout = () => {
+   saveToken(null);
+   setAuth(null);
+   setRealodUser(true);
//rediregir usuario, navegacion
+   router.push("/")
+ }

  const authData = useMemo(
    () => ( { auth: auth, persistAuth: loginUI, reloadAuth: setRealodUser,
+       cleanAuth: logout,
      }
    ),
    [auth]
  );
```

B, Capturamos el contexto de auth en `src\components\Header\Menu\Menu.js`
para poder decidir si mostramos el boton que le permitira loguarse o el
que le permita hacer logout

```js
  function MenuUser(props) {
    const { onShowModal } = props;
+   const authContext = useAuth();
    return (
      <SMenu>
+       {authContext.auth 
// el usuario esta logueado
+       ? (
+           <SMenu.Item onClick={authContext.cleanAuth}>
+             <Icon name="user outline" />
+             Logout
+           </SMenu.Item>
+         )
// El usuario NO esta logueado
+       : (
          <SMenu.Item onClick={onShowModal}>
            <Icon name="user outline" />
            Mi Cuenta
          </SMenu.Item>
+         )
        }
      </SMenu>
    )
  }
```

**Resultado:** Al entrar podemos ver un boton que permite hacer login y
cuando estamos loguado uno para hacer logout

<!--

#########################################

-->

----

<!--

#########################################

-->

### 62. AuthFetch

**Objetivo:** Vamos a crear un util que se encarge de hacer peticiones
protegiadas de nuestro back, ya que debemos de cojer el token, comprobarlo
y enviarlo, y para no repetir estos pasos haremos un fetch generico.
