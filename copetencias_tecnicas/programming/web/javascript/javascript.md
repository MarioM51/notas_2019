# JavaScript

- [JavaScript](#javascript)
  - [Introduccion / Base](#introduccion--base)
  - [Arreglos, arrays, matriz](#arreglos-arrays-matriz)
  - [Uso de splice](#uso-de-splice)
  - [CREACION DE OBJETOS EN JAVASCRIPT](#creacion-de-objetos-en-javascript)
  - [EL OBJETOS DATE](#el-objetos-date)
  - [EL OBJETOS MATH](#el-objetos-math)
  - [DOCUMENT OBJECT MODEL = DOM](#document-object-model--dom)
    - [document.querySelector("#id") y document.querySelectorAll(".clase")](#documentqueryselectorid-y-documentqueryselectorallclase)
    - [miElementDOM.setAttribute("class","rojo")](#mielementdomsetattributeclassrojo)
  - [NAVEGACION ENTRE NODOS](#navegacion-entre-nodos)
    - [Crear nodos, crear elemento bajo la estrucutra DOM](#crear-nodos-crear-elemento-bajo-la-estrucutra-dom)
  - [Sección 9](#sección-9)
    - [JavaScript: Hacer una galería con instrucciones DOM](#javascript-hacer-una-galería-con-instrucciones-dom)
  - [Sección 10. JavaScript: Objetos del navegador BOM (Browser Object Model)](#sección-10-javascript-objetos-del-navegador-bom-browser-object-model)
    - [64. Objetos Navegador](#64-objetos-navegador)
    - [65. El objeto Window](#65-el-objeto-window)
    - [66. Crear ventanas emergentes con los objetos window y document](#66-crear-ventanas-emergentes-con-los-objetos-window-y-document)
    - [69. location](#69-location)
    - [69. history](#69-history)
    - [69. navigator](#69-navigator)
    - [70. manejo del tiempo con setTimeout() y setInterval()](#70-manejo-del-tiempo-con-settimeout-y-setinterval)
    - [72. guardar Cookies, crear Cookies, uso de Cookies](#72-guardar-cookies-crear-cookies-uso-de-cookies)
  - [Sección: 11. JavaScript: Manejo de eventos](#sección-11-javascript-manejo-de-eventos)
    - [75. Los eventos en JavaScript](#75-los-eventos-en-javascript)
    - [Clase 76. Los eventos de Mouse](#clase-76-los-eventos-de-mouse)
    - [Clase 77. El evento onSelect](#clase-77-el-evento-onselect)
    - [Clase 77. El evento onKeyUp](#clase-77-el-evento-onkeyup)
    - [Clase 79. onfocus onblur](#clase-79-onfocus-onblur)
    - [Clase 80. El evento onSubmit](#clase-80-el-evento-onsubmit)
    - [Clase 83. onchange() para la etiqueta \<select\>](#clase-83-onchange-para-la-etiqueta-select)
  - [Sección 12. JavaScript: Ejercicios, ejemplos y examenes](#sección-12-javascript-ejercicios-ejemplos-y-examenes)
    - [Clase 86. Relacionar dos combo-box con JavaScript](#clase-86-relacionar-dos-combo-box-con-javascript)
    - [Reloj](#reloj)
    - [Examen cambiar estilos](#examen-cambiar-estilos)
    - [Examen agregar botones interactuables dinamicamente](#examen-agregar-botones-interactuables-dinamicamente)
    - [Saltos de Rana](#saltos-de-rana)
  - [Sección 17. Nuevas características de ES6](#sección-17-nuevas-características-de-es6)
    - [Clase 143. ES6: El manejo de las llamadas de callback por medio de las promesas](#clase-143-es6-el-manejo-de-las-llamadas-de-callback-por-medio-de-las-promesas)
  - [Vainilla Web Components](#vainilla-web-components)
  - [Por el Camino](#por-el-camino)
    - [Mostrar imagen default si falla la carga de imagen](#mostrar-imagen-default-si-falla-la-carga-de-imagen)
    - [Leer Json+LD del header](#leer-jsonld-del-header)
    - [Detectar que el usuario preciono el boton hacia atras](#detectar-que-el-usuario-preciono-el-boton-hacia-atras)
    - [Limpiar input en tanto usuario levanta tecla](#limpiar-input-en-tanto-usuario-levanta-tecla)

## Introduccion / Base

Pedir Dato por PopUp, Para recuperar un dato desde una vrntana en JS
podemos usar

```js
var nombre = window.prompt("Â¿Cual es tu nombre?");
```

Swith

```js
switch(nombre) {
  case "mario": console.log("11"); break;
  case "pancho": console.log("22"); break;
  default: console.log("33"); break;
}
```

Ternario: `var algo = (true) ? "a" : "b";`

Convertir de cadena a numero

```js
Number("6")
var miNum = Number("seis") //esto daria un NaN = Not a Number
if(isNaN(miNum))  {}´ñ´ñ``

pasar a minusculas o mayusculas, tamaño de un string

```js
cadena.toLowerCase();
cadena.toUpperCase();
miString.lenght // es una propiedad por lo cual no lleva parentecis
```

Buscar una subcadena en una cadena, buscar en string, buscar en cadena

```js
 miString.indexOf("rq")
```

Partir una cadena, substring, cortar una cadena

```js
console.log(miString.slice(13)); //v
console.log(miString.substr(5, 8)); //marquez
```

tomar un caracter de una cadena, tomar un caracter de una string, cadena
a caracter, string to char, string a arreglo de caracteres

```javascript
cadena.charAt(pos)
```

Leer los caracteres de una cadena, buscar caracteres invalidos en una cadena. recorrer una cadena

Fuente: file://E:\cursos\00_Pasos_Notas\_2019\recursos\Web_JS_CSS_HTML\ejemplo_pront\index.html#sdft_45

Ejemplo_Extenso:

```js
function hayCaracteresInvalidos(cadenaIn) {
  if(cadenaIn==null || cadenaIn == "") {
    throw "empty";
    return false;
  }
  var cadena = cadenaIn+"";
  cadena = cadena.toLowerCase();
  let caracteresInvalidos = "áàéíóúèìòù!·#%&/(){}[]<>='¿?'`*+-,;:-_";
  for(let i=0; i<cadena.length; i++) {
    let caracterAChecar = cadena.charAt(i);
    debugger;
    for(let j=0; j<cadena.length; j++) {
      let caracterInvalido = caracteresInvalidos.charAt(j);
      if(caracterAChecar == caracterInvalido) {
        return true;
      }
    }
  }
  return false;
}

let caracteresInvalidos = null;
try {
  let miStringA = "qwertyuioplkjhàgfdsazxcvbnm";
  caracteresInvalidos = hayCaracteresInvalidos(miStringA);
  if(caracteresInvalidos) {
    window.document.write("En " + miStringA + " hay caracteres invalidos");
  } else {
    window.document.write(miStringA + " paso la prueba de caracteres invalidos");
  }
} catch(err) {
  if(err=="empty") {window.document.write("cadena vacia") }
}
```

## Arreglos, arrays, matriz

```js
var dias = new Array();
dias[0] = "Lunes"
dias.push("Martes");
dias.push("Martes");
dias["mario"] = "nom: mario, app: marquez, edad: 22";
window.document.write("<br>"+dias[1]); //Martes
window.document.write("<br>"+dias["mario"]);//nom: mario, app: marquez, edad: 22
```

Tenemos diferentes metodos para los arreglos que nos ayudaran en su manejo.

```javascript
miArray.push("1","2"); //Agrega uno o mas elemento
var last = miArray.pop("1","2"); //quita el ultimo y lo retorna
var first = miArray.shift("1","2"); //agrega el primero
miArray.unshift("0"); //Agrega un elemento al inicio de la list
```

## Uso de splice

Nota: Agrega directamente al arreglo, no retorna uno modifiacado,

return null : splice({posicion_donde_agregar}, elementos_a_remplazar, elemento_a_agregar);

```js
const months = ['Jan', 'March', 'April', 'June'];
months.splice(1, 0, 'Feb');
// inserts at index 1
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "June"]

months.splice(4, 1, 'May');
// replaces 1 element at index 4
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "May"]

// agregar al lado, agregar a un lado
const indexToInsert = months.lastIndexOf("April");
months.splice(indexToInsert, 0, "XXX");
// expected output: Array ["Jan", "Feb", "March", "XXX", "April", "May"]

months.splice(indexToInsert+1, 0, "XXX");
// expected output: Array ["Jan", "Feb", "March", "XXX", "April", "XXX",  "May"]
```

## CREACION DE OBJETOS EN JAVASCRIPT

```javascript
var mario = new Object();
mario.nombre = "Mario M";
mario.edad = 26;
mario.casado = false;
mario.mascotas = ["nona", "humo"];
mario.direccion = {calle: "Eje 10", num: 11, ciudad: "Coyoacan"};
document.write("nombre: " + mario.nombre + " Direccion: " + mario.direccion.calle);
// Salida: nom: mario, app: marquez, edad: 22nombre: Mario M Direccion: Eje 10
```

## EL OBJETOS DATE

La fecha la trae desde 1970(unix) los MESES los trae
en arreglo desde Enero=0 y diciembre=11, Los DIAS el
domingo=0 y Lunes=6. La informacion es tomada la cual
esta asignada en el equipo del usuario que ejecuta el JS

```js
var hoy = new Date();
dia = hoy.getDay();
diaMes = hoy.getDate();
mes = hoy.getMonth();
anio = hoy.getFullYear();
hora = hoy.getHours();
minuto = hoy.getMinutes();
segundo = hoy.getSeconds();
miliSegundo = hoy.getMilliseconds();
```

Crear fecha

```js
// year, month, day, hours, minutes, seconds, milliseconds
const d = new Date(2018, 11, 24, 10, 33, 30, 0);
```

## EL OBJETOS MATH

`https://www.w3schools.com/js/js_math.asp`

Generar numero aleatorio

```js
var alumnos = new Array("A","B","C","D","E","F","G","H");
var numAleatorio = Math.random();
numAleatorio = numAleatorio * alumnos.length;
numAleatorio = Math.floor(numAleatorio); //redondeamos abajo
document.write("Letra de pa pos " + numAleatorio + ", " + alumnos[numAleatorio]);
```

## DOCUMENT OBJECT MODEL = DOM

El DOM es la reprecentacion de nuestro documento html (pagina web)
el cual podemos manipular desde javaScript y podremos acceder
desde el como si este fuera un Arbol de nodos.

```js
  <style> .mi-clase-1 { color: red; } </style>

  <ul id="mi-lista-1"> <li>Uno</li> <li>Dos</li> <li class="mi-clase-1">Tres</li> <li>Cuatro</li> </ul> 

  -------------------
  var miElemento = document.getElementById("mi-lista-1");
  console.log(miElemento);
  console.log(miElemento.childNodes.length);

  var miClse = document.getElementsByClassName("mi-clase-1");
  console.log("Contenido del primer elementos con clase mi-clase: " + miClse[0].innerHTML); // Salida: Tres

  var miElemento = document.getElementsByTagName("span");
```

la salida sera el mismo componente con html y 9. ya que tambien
cuenta los elementos del contenido como elemento, (si contamos el
texto del li nos dara 9)

### document.querySelector("#id") y document.querySelectorAll(".clase")

Estos funciona igual que los selectores de CSS, casi no se usaba ya que
no era compatible con Internet Explorer 8.

```javascript
document.querySelector("#id")
document.querySelectorAll(".clase")
var elems = document.querySelectorAll("p[class]")
```

### miElementDOM.setAttribute("class","rojo")

```javascript
var miSpan = document.getElementsByTagName("span");
miSpan[1].setAttribute("class","mi-clase-1");
```

**Error**: miSpan.setAttribute is not a function
Recordar que getElements...(atencion en la "S")
trae un arreglo lo cual debemos señalar a uno
de ellos para manejarlo como nodo que tiene los
metodos/props como el setAttribute y etc.

## NAVEGACION ENTRE NODOS

```javascript
var medio = document.getElementById("medio");
console.log("El de enmedio: " + medio.innerHTML);
console.log(medio.nextSibling.nextSibling);
console.log(medio.nextElementSibling);

var lista = document.querySelector("ul");
console.log("Primero: ");
lista.firstChild.setAttribute("class", "rojo");
console.log("ultimo: ");
lista.lastChild.setAttribute("class", "verde");

<ul id="lista"><li>Uno</li><li>DOs</li><li id="medio">Tres</li><li>Cuatro</li><li>Cinco</li></ul>
```

### Crear nodos, crear elemento bajo la estrucutra DOM

```javascript
document.createElement("p");          node.appendChild(texto_sabiasQue);
```

Necesitamos hacer 3, pasos para agregar un nodo

```r
1. Crearlo      1. Crearlo
2. poblarlo  ò  2. Añadirlo
3. Añadirlo     3. Añadirlo
```

- Ejercicio: Crear un sabias que, añadir texto aleatorio
crear elemento.

file://E:\cursos\00_Pasos_Notas\_2019\recursos\Web_JS_CSS_HTML/1_sabias_que.gif

```javascript
var cosas = new Array();
cosas.push("...");                     //agregamos varios de estos
var aleatorio = Math.floor(Math.random()*cosas.length);

var title_sabiasQue = document.createElement("h1");         //creamos elemento
title_sabiasQue.innerHTML = "Sabias que?";            //poblamos elemento
var texto_sabiasQue = document.createElement("p");
texto_sabiasQue.innerHTML = cosas[aleatorio];

var conteiner_sabiasQue = document.getElementById("sabias-que");
conteiner_sabiasQue.appendChild(title_sabiasQue);          //añadimos elemento
conteiner_sabiasQue.appendChild(texto_sabiasQue);
```

······ Eliminar nodo, eliminar elemento removeChild
Este lo elimina pero lo regresa para poderlo guardar
en una variable, no podemos eliminar desde el mismo
nodo solo desde el padre

ELIMINAR

```javascript
var listaEliminar = document.getElementById("lista-eliminar");
listaEliminar.removeChild(document.getElementById("to-delete"));
//listaEliminar.removeChild(listaEliminar.firstChild);                                   /*duda por que no funciona*/
```

## Sección 9

### JavaScript: Hacer una galería con instrucciones DOM

file:///E:/cursos/00_Pasos_Notas/_2019/recursos/Web_JS_CSS_HTML/galeria/galeria.html

Resultado:
  Resultado:
  Vista: file:///E:/cursos/00_Pasos_Notas/_2019/recursos/Web_JS_CSS_HTML/4_galeria_quitar_img_grande.gif
  Codigo: file:///E:/cursos/00_Pasos_Notas/_2019/recursos/Web_JS_CSS_HTML/galeria/galeria.html

1. Creamos la vista, conformado por el html y el css

2. Agregamos el evento de click y saber si pulso sobre una imagen o no
file:///E:/cursos/00_Pasos_Notas/_2019/recursos/Web_JS_CSS_HTML/2_galeria_1.PNG

```javascript
let fotos = document.querySelector("#galeria-container");
fotos.addEventListener("click", function(e) {
  if(e.target.tagName == "IMG") {
    console.log("Pulso sobre una img");
  } else {
    console.log("NO pulso una img");
   
  }
}, false);
```

3, Agregamos el fondo negro cuando se pulse sobre una img

file:///E:/cursos/00_Pasos_Notas/_2019/recursos/Web_JS_CSS_HTML/3_galeria_sombra.gif

```javascript
var fondo = document.createElement("div");
fondo.id = "fondo";
document.body.appendChild(fondo);

fondo.style.position = "absolute";
fondo.style.top = 0;
fondo.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
fondo.style.cursor = "pointer";

fondo.style.width = window.innerWidth+"px";
fondo.style.height = window.innerHeight+"px";

fondo.style.top = window.pageYOffset+"px";
fondo.style.left = window.pageXOffset+"px";
```

4, Identificar que imagen que seleccionamos, y cargamos

la img grande.
file:///E:/cursos/00_Pasos_Notas/_2019/recursos/Web_JS_CSS_HTML/4_galeria_carga_img_grande.gif

```javascript
//vemos que imagen seleccionamos
let nameImgSelected = e.target.src.substr(e.target.src.length-10, 10);

//cargamos la imagen grande
let bigImgSrc = "./img/fondos/"+nameImgSelected;
console.log(bigImgSrc);
let imagen = document.createElement("img");
imagen.id = "imagen";
imagen.src = bigImgSrc;
imagen.style.position = "absolute";
imagen.style.display = "block";

//ya que no carga de inmediato agregamos la img cuando este cargada
imagen.addEventListener("load", function() {
  fondo.appendChild(imagen); 
}, false);
```

4.1, Centramos imagen

```javascript
imagen.addEventListener("load", function() {
  let posXCentro = (window.innerWidth-imagen.width)/2;
  let posYCentro = (window.innerHeight-imagen.height)/2;
  imagen.style.top =  posYCentro+"px";
  imagen.style.left = posXCentro+"px";
```

5, Eliminamos imagen

Resultado: file:///E:/cursos/00_Pasos_Notas/_2019/recursos/Web_JS_CSS_HTML/4_galeria_quitar_img_grande.gif

```javascript
fondo.addEventListener("click", function(e) {
  if(imagen) {
    fondo.parentNode.removeChild(fondo);
  }
}, false);
```

----

## Sección 10. JavaScript: Objetos del navegador BOM (Browser Object Model)

### 64. Objetos Navegador

Cuando se carga una página en un navegador,
se crea un número de objetos característicos
del navegador según el contenido de dicha
página.

- **window:** Es el objeto de más alto
  nivel, contiene las propiedades de la
  ventana y en el supuesto de trabajar
  con frames, un objeto window es
  generado para cada frame, tiene cosas
  como el window.alert, window.console
- **location.** Contiene las propiedades de
  la URL activa.
- **history.** Objeto que contiene las propiedades
  que representan a las URL que el usuario ha visitado
  anteriormente.
- **document.** Este objeto contiene todas
  las propiedades del documento actual,
  como pueden ser su color de fondo,
  enlaces, imágenes, etc

Gerarquia

```r
-navigator
-window
  -parent, frames, self, top
  -history
  -location
  -document
    -link
    -anchor
    -form
      -elementos form
```

### 65. El objeto Window

Entrada de texto, pedir info, ventana formulario

```javascript
window.prompt("ingresa texto"); 
```

### 66. Crear ventanas emergentes con los objetos window y document

Abrir una ventana, abrir ventana

```javascript
document.getElementById("boton-abrir-ventana").addEventListener("click", function() {
let ventana = window.open("ruta", "Nueva ventana", "status, height=200 width=200");
ventana.window.status = "mi ventana nueva";
ventana.document.write("<h2>Hola</h2>");
}, false);
```

### 69. location

```javascript
window.location.href
window.location.pathname
window.location.protocol
window.location.port
window.location.assign("www.adondeRedirigir.com")
```

### 69. history

```javascript
window.history.back()
window.history.forward()
```

### 69. navigator

```javascript
navigator.appName
navigator.appCodename
navigator.platform
navigator.cookieEnabled

navigator.plataform      //que Sistema operativo tenemos
navigator.language       //si es español
navigator.javaEnabled   
```

### 70. manejo del tiempo con setTimeout() y setInterval()

```javascript
let miTimer = setTimeout(function, miliSeg) Ejecuta una vez una funcion en el tiempo indicado
setInterval(function, miliSeg) Ejecuta varias veces la funcion en el lapzo de tiempo indicado
clearTimeout(miTimer);
```

### 72. guardar Cookies, crear Cookies, uso de Cookies

PENDIENTE

## Sección: 11. JavaScript: Manejo de eventos

### 75. Los eventos en JavaScript

Tenemos dos tipos de eventos los generados por el usuario
y los generados por el sistema(carga de un archivo)

### Clase 76. Los eventos de Mouse

```javascript
miElemento.onMouseOver  = function() { }
miElemento.onMouseOut   = function() { }
miElemento.onmousedown   = function() { }
miElemento.onmouseup   = function() { }
miElemento.onclick     = function() { }
miElemento.onmousemove   = function(e) {
  let x = e.clientX //podemos sacar las cordenadas con este
}    
```

### Clase 77. El evento onSelect

```javascript
miElemento.onselect = function() { }
```

### Clase 77. El evento onKeyUp

```javascript
miElemento.onkeyup= function() { }
miElemento.onkeydown= function() { }
```

### Clase 79. onfocus onblur

```javascript
miElemento.onfocus = functio() {}
miElemento.onblur = functio() {}
```

### Clase 80. El evento onSubmit

```javascript
miFormulario.onsubmit = function() {}
```

### Clase 83. onchange() para la etiqueta \<select\>

```javascript
miElementoSelect.onchange = function() {}
```

<!-- Sección 12: JavaScript: Ejercicios y ejemplos -->
## Sección 12. JavaScript: Ejercicios, ejemplos y examenes

### Clase 86. Relacionar dos combo-box con JavaScript

Haremos un select de paises que cuando seleccionemos un pais
carge sus estados paises y estados, relacionar 2. selects,
estados y paises, relacion pais estado.

### Reloj

**TAGS**: Mostrar hora y fecha con javascript, campurar hora,
capturar segundos campurar minutos, capturar dia, campurar mes,
capturar año, obtener hora, obtener segundos, obtener minutos,
obtener dia, obtener mes, obtener año.

**Ejercicio:** Haremos un reloj que se actualize cada segundo y si los
segundos son menores a 10 se le agrega un 0 para que no
pierda su tamaño el reloj.

**commit:** No se subio esta completo en el sig archivo.

**Codigo:**

```html
<!DOCTYPE html><html><head>
<script>
window.onload = function() {
  let divHoraCompleta = document.getElementById("horaCompleta");
  let divFechaCompleta = document.getElementById("fechaCompleta");
  window.setInterval(()=>{
    divHoraCompleta.innerHTML = getHoraCompleta();
    divFechaCompleta.innerHTML = getFechaCompleta();
  }, 1000);
}
function getHoraCompleta() {
  var now = new Date();
  let hora = now.getHours(), minuto=now.getMinutes(), segundo= (now.getSeconds()<10) ? "0"+now.getSeconds() : now.getSeconds();
  return hora+":"+minuto+":"+segundo+"";
}
function getFechaCompleta() {
  var now = new Date();
  let anio = now.getFullYear(), mes=now.getMonth()+1, dia=now.getDay();
  return anio+"/"+mes+"/"+dia+"";
}
</script>
</head>
<body><span id="horaCompleta"></span> <br /> <span id="fechaCompleta"></span></body>
</html>
```

**Resultado:**

```r
13:7:43
2019/7/1
```

### Examen cambiar estilos

**TAGS:** *examen javascript, cambio de estilos con javascript, cambiar estilos, formulario cabia estilos,
examen certificacion javascript, examen de certificacion para javascript certificacion cambia estilos,
certificacion cambiar estilos, examen de javascript.

**Ejercicio:** Se usara un formulario para cambiar los estilos
desde un formulario.

**Codigo:** El codigo esta completo seria crear los 2 sigiente
archivos y se otendria el resultado completo.

1, cambiarEstilos.html : vista/formulario

```html
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <script src="cambiarEstilos.js"></script>
    <link rel="stylesheet" href="cambiarEstilos.css">
</head>
<body>
    <h1 id="h1-editable">Titulo editable</h1>
    Tamaño: <select id="tamanio"></select>
    <br />
    Color: <select id="color"></select>
    <br />
    Decoracion: <select id="peso"></select>
</body>
</html>
```

2, cambiarEstilos.js : Logica

```javascript
var title;

window.onload = function() {
    title = window.document.getElementById("h1-editable");
    console.log(title);

    //tamaño
    let selectTamanio = window.document.getElementById("tamanio");
    console.log(selectTamanio);
    selectTamanio.addEventListener("change", onChangeTamanio);
    addOptionsToSelect(selectTamanio);

    //color
    let selectColor = window.document.getElementById("color");
    selectColor.addEventListener("change", onChangeColor);
    let colores = ["", "red","green","white","black","lightgreen","yellow","orange"];
    addOptionsToSelect(selectColor, colores);

    //decoracion
    let selectDecoration = window.document.getElementById("peso");
    selectDecoration.addEventListener("change", onChangePeso);
    var decorations = ["", "none","underline red","underline wavy red","inherit","initial","unset"];
    addOptionsToSelect(selectDecoration, decorations);

}


function onChangeTamanio(e) {
    let valueSelected = getSelectValueFromEventChanged(e);
    setAtributoToElement(title, "font-size", valueSelected)
}

function onChangeColor(e) {
    let valueSelected = getSelectValueFromEventChanged(e);
    setAtributoToElement(title, "color", valueSelected)
}

function onChangePeso(e) {
    let valueSelected = getSelectValueFromEventChanged(e);
    setAtributoToElement(title, "font-weight", valueSelected)
}

//utils
function getSelectValueFromEventChanged(select) {
    let indexSelected = select.path[0].selectedIndex
    return select.path[0].options[indexSelected].value;
}

function setAtributoToElement(element, attribute, value) {
    element.setAttribute(attribute, value);
    switch(attribute) {
        case "font-size":       element.style.fontSize = value+"px"; break;
        case "color":           element.style.color = value; break;
        case "font-weight":     element.style.textDecoration = value; break;

        default: console.log("chinchin");
    }
    console.log("changeado: " + attribute + ", " + value);
}

function addOptionsToSelect(select, arrayOptions=null) {
    if(arrayOptions == null) {
        let optionTemp = window.document.createElement("option");
        optionTemp.innerHTML = "";
        optionTemp.value = "";
        select.appendChild(optionTemp);
        for (let i=8; i<=44; i++) {
            let optionTemp = window.document.createElement("option");
            optionTemp.innerHTML = i;
            optionTemp.value = i;
            select.appendChild(optionTemp);
        }
    } else {
        for (let i=0; i<=arrayOptions.length; i++) {
            let optionTemp = window.document.createElement("option");
            optionTemp.innerHTML = arrayOptions[i];
            optionTemp.value = arrayOptions[i];
            select.appendChild(optionTemp);
        }
    }
}
```

**Resultado:** ![alt text](./img/1_certificacion_formulario_cambia_estilos.gif "1_certificacion_formulario_cambia_estilos")

### Examen agregar botones interactuables dinamicamente

**TAGS:** *agregar botones dinamicamente, agregar boton con javascript, agregar botones con javascript*.
**Ejercicio:** Se creara X cantidad de botones desde un for
el cual al precionar cada uno mostrara el cual fue el que
se clickeo, y ademas sus colores de estos seran alternados.

**Codigo:** El codigo esta completo seria crear los 3 siguiente
archivos y se otendria el resultado completo.

1, botones.html

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <title>Botones</title>
    <script src='botones.js'></script>
    <link rel='stylesheet' href='botones.css'>
</head>
<body>
    <div id="container-botones">
        mi primer div
    </div>
</body>
</html>
```

2, botones.css

```css
button { width: 60px;
    height: 30px; margin-right: 10px;
    margin-top: 5px; margin-bottom: 5px; }

.button-blue { background-color: #9999ff; }

.button-green { background-color: #99ff99; }
```

3, botones.js

```javascript
window.onload = function() {
    let mipDiv = window.document.getElementById("container-botones");
    crearBotones(20, mipDiv);
}

function crearBotones(cantidadBotones, componentePadre) {
    for(let i=1; i<=cantidadBotones; i++) {
        let botonTemp = crearBoton(i);
        componentePadre.appendChild(botonTemp);
    }
}

function crearBoton(index) {
    let botonNuevo = window.document.createElement("button");
    botonNuevo.innerHTML = index;
    let claseBoton = (index%2 != 0) ? "button-blue": "button-green";
    botonNuevo.setAttribute("class", claseBoton);
    botonNuevo.setAttribute("id", index);
    botonNuevo.addEventListener("click", onClickButton);
    return botonNuevo;
}

function onClickButton(e) {
    alert(e.path[0].id);
}
```

**Resultado:** ![alt text](./img/2_certificacion_crear_x_botones_interac.png "2_certificacion_crear_x_botones_interac")

### Saltos de Rana

**TAGS:** *Saltos de rana, examen certificacion rana, examen rana*.
**Ejercicio:** Crearemos una interface de una rana y varias casillas donde
le indicaremos cuantas casillas debe de saltar, y en caso de que llegue
al final ira al otro lado.

**commit:** No se subio a un repo pa no llenarme de cosas, pero el codigo
esta de forma completo en su archivo, solo seria descargar la imagen de la
rana.

**Codigo:**

1. rana.html

```html
<html>
<head>
  <title>Rana</title><script src="rana.js"></script>
  <link rel="stylesheet" href="rana.css">
</head>
<body>
  <h1>Rana</h1>
  Casillas a Mover: <input type="number" max="100" min="0" id="inputMover">
  <button id="btnMover">Mover</button>
  <br /><div id="casillas"></div>
</body>
</html>
```

2, rana.css

```css
h1 {
  color: green;
}
.casilla {
  width: 60px; height: 30px;
  border: solid 1px black;
  float: left; margin: 5px;
}
.aqui {
  background-color: #afa;
  border: solid 1px black;
  background-image: url("./img/rana.png");
  background-size: 60px 30px;
}
.disabled {
  border: 1px solid #999999;
  background-color: #cccccc;
  color: #666666;
}
```

1, rana.js

```javascript
/* logica */ var casillasRecorridas=0, pos=0, asendente = true;
/*ui */ var casillas, inputMover, btnMover, numCasillas=10;
window.onload = function() {
  dibujarCasillas(numCasillas);

  btnMover = document.getElementById("btnMover");
  btnMover.addEventListener("click", onClickMover);
 
  inputMover = document.getElementById("inputMover");
 
  casillas = Array.from(window.document.getElementById("casillas").children);
}

function onClickMover() {
  let casillasAMover = Number(inputMover.value);
  console.log("casillasAMover: " + casillasAMover);
  mover(casillasAMover);
  btnMover.disabled = true;
  btnMover.setAttribute("class", "disabled");
}

function dibujarCasillas(numCasillas) {
  let divCasillas = window.document.getElementById("casillas")
  for(let i=0; i<numCasillas; i++) {
   
    let casillaTemp = document.createElement("div");
    let estilos = (i!=0) ? "casilla" : "casilla aqui";
   
    casillaTemp.setAttribute("class", estilos);
    divCasillas.appendChild(casillaTemp);
  }
}

function mover(casillasAMover) {
  setTimeout(()=>{
    console.log("Casillas recorridas: " + this.casillasRecorridas + " de " + casillasAMover);
    //me quito quito de donde estaba antes o despinto
    if(asendente && pos != 0) {
      casillas[pos-1].innerHTML = "";
      casillas[pos-1].setAttribute("class", "casilla");
    }
    if(!asendente && pos != casillas.length-1) {
      casillas[pos+1].innerHTML = "";
      casillas[pos+1].setAttribute("class", "casilla");
    }
   
    //proceso para avanzar
    //  saber si estoy avanzando o retrocediendo
    if(pos == 0) { asendente=true; }    
    if(pos == casillas.length-1) { asendente=false; }
   
    //  me muevo o pinto
    let casillaTemp = casillas[pos];
    casillaTemp.setAttribute("class", "casilla aqui");
   
    //  señalo a donde me voy a mover a continuasion
    if(asendente) { pos++; } else { pos--; }
   
    casillasRecorridas++;
    if(casillasRecorridas <= casillasAMover) {
      mover(casillasAMover);
    } else {
      casillasRecorridas = 1;
      btnMover.disabled = false;
      btnMover.setAttribute("class", "");
    }
  }, 500);
}
```

**Resultado:** ![alt text](./img/3_certificacion_rana_saltarina.gif "3_certificacion_rana_saltarina")

----

## Sección 17. Nuevas características de ES6

### Clase 143. ES6: El manejo de las llamadas de callback por medio de las promesas

Sintaxis:

```js
let miPromesa = new Promise(function(resolve, reject){ ... })
miPromesa.then(
  (resp) => { ... },
  (err)=>{ ... })
```

Analogia: Son como cuando pedimos una pizza, si fueramos Java
ya no hariamos nada hasta que la Pizza llegue paraempezar a
preparar las los platos refresco etc. Pero si somos JavaScript
podemos realizar la tarea de los platos, refresco y etc mientras
esperamos la Pizza, tenemos una promesa que esta llegara, y
tenemos un plan si llega y otro si no llega.

<!-- estados de una promesa, estados en que puede estar una Promesa, los 3 estados de un promesa, los tres estados de un promesa-->

Teoria:

- Las promesas tienen 3 estados
  - **Pending** (pendiente): Estado inicial, no cumplida.
  - **Fulfilled** (Cumplida): termino satisfactoriamente, comunmente lo
    manejamos en resolve()
  - **Rejected** (Rechazada): ocurrio un error, comunmente lo manejamos en
    reject(), significa que la operación falló.

- Las promesas se pueden encadenar

- Para encadenar varias promesas podemos usar
  - Encedenarlos : para obtener informacion que depende entre
    ellas, por ejemplo la lista de alumnos y luego de ahi obtener
    las calificaciones.
  - all[] : indicamos que se lanzen todas las promesas para obtener
    informacion que no depende entre ellas. PorEjemplo: los empleados
    y los departamentos para empezar a llenar un formulario.
  - race[] : indicamos si se cumple "alguna" de las promesas.

Ejemplo: Lo que pasara es que segun el primer timeout que termine
que podria simbolisar, una peticion a servidor, esta podria salir
bien o mal y de acuerdo a eso ejecutar resolve o reject

```javascript
let miPromesa = new Promise(function(resolve, reject){
    setTimeout(()=>{ resolve("Todo salio bien"); }, 1300);
    setTimeout(()=>{ reject("Cataplum");}, 1200);
});

miPromesa.then((resp) => { console.log(resp + " - 1"); })
                 .catch((err) => { console.log(err + " - 2"); })
;
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Vainilla Web Components

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Por el Camino

### Mostrar imagen default si falla la carga de imagen

- [Fuente](https://stackoverflow.com/questions/980855/inputting-a-default-image-in-case-the-src-attribute-of-an-html-img-is-not-vali)

```html
<img src="foo.jpg" onerror="if (this.src != 'error.jpg') this.src = 'error.jpg';">
```

### Leer Json+LD del header

<!-- tomar jsonld, capturar json ld, json-ld -->

Fuete: [stackoverflow](https://stackoverflow.com/questions/38602543/is-there-a-way-to-access-json-ld-via-javascript-if-it-doesnt-have-an-id)

You can get all JSON-LD blocks with

```js
//get the first one
document.querySelectorAll('script[type="application/ld+json"]');

//get all of them
document.querySelector('script[type="application/ld+json"]');
```

Here's a full example:

```js
var jsonld = JSON.parse(document.querySelector('script[type="application/ld+json"]').innerText);
document.getElementById('result').innerText = jsonld.endDate;
<html>
  <head>
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "Event",
        "name": "A random event",
        "startDate": "2013-09-14T21:30",
        "endDate": "2013-09-14T21:30"
      }
    </script>
  </head>
  <body>
    <p>The end date is: <strong id="result"></strong></p>
  </body>
</html>
```

### Detectar que el usuario preciono el boton hacia atras

**Problema:** Al precionar el boton atras se muestra la misma vista que
estaba antes, ya que la carga del cache, lo que nos da el problema de
que se muestra una version de la UI desactualizada con respecto al estado
de la aplicacion.

**Ejemplo:** En una lista de productos donde se muestra el carrito con
la cantidad de productos por ejemplo 1 producto en el carrito, y luego,
el usuario entra a la pagina de un producto y agrea otro producto, entonces
ya seran 2 productos en el carrito de compra, pero el usuario le da al
boton de atras se carga pa paina cacheada mostrando que en el carrito
solo existe 1 producto.

![4_detectar_que_el_usuario_viene_del_back_button](./img/4_detectar_que_el_usuario_viene_del_back_button.gif)

**Solucion**: Podemos usar los eventos **pageshow y pagehide** para
ejecutar codigo cuando sea el caso, y usar este evento para sincronizar
el estado y la UI.
**Fuente:**[stackoverflow](https://stackoverflow.com/a/6364985).

```js
function pageShown(evt) {
  if (evt.persisted) {
    alert("pageshow event handler called.  The page was just restored from the Page Cache (eg. From the Back button.");
  } else {
    alert("pageshow event handler called for the initial load.  This is the same as the load event.");
  }
}

function pageHidden(evt) {
  if (evt.persisted) {
    alert("pagehide event handler called.  The page was suspended and placed into the Page Cache.");
  } else {
    alert("pagehide event handler called for page destruction.  This is the same as the unload event.");
  }
}

window.addEventListener("pageshow", pageShown, false);
window.addEventListener("pagehide", pageHidden, false);
```

```ts
public static toSlug(str: string): string {
  str = str.replace(/^\s+|\s+$/g, ""); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to = "aaaaeeeeiiiioooouuuunc------";
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-"); // collapse dashes

  return str;
}

```

### Limpiar input en tanto usuario levanta tecla

```ts
try {
  if (this.confirmEmailCode.length > 6) {
    this.confirmEmailCode = this.confirmEmailCode.substring(0, 6);
  }
  const rexNumber = /[0-9]/g;
  const cleaned = this.confirmEmailCode.match(rexNumber)?.join('');
  if (cleaned == undefined) {
    this.confirmEmailCode = '';
  } else {
    this.confirmEmailCode = cleaned;
  }
} catch (error) {
  this.confirmEmailCode = '';
}
```
