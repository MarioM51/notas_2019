# Web NPM Packages and Monorepos

## ASDF administrador de versiones de node

Esta herramienta CLI nos permite tener ma de una version de node en
nuestro sistema, y tambien podemos utilizarlos para otros idiiomas como
phyton, go, java, etc.

## Iniciar proyecto

**Objetivo:** Vamos a inicializar un proyecto con typescript, esLint,
prettier y jest. Y que cada antes de hacer un commit se ejecute prettier
y los test unitarios, para evitar subir un codigo mal formateado o que
tenga test unitarios que no pase.

A, Crear proyecto: `npm init -y`

B, Agregar typescript

B,1 Descargar/agregar typescript como devDependency: `npm add -D typescript`

- Este agregara/descargara en nuestro node_modules a la carpeta typescript

B,2 Iniciar conf de typescript, el cual solo creara el archivo `tsconfig.json`

```r
npx tsc --init
```

- Lo que hace npx buscar los programas que tenemos dentro de node_modules
  de nuestro proyecto, por lo que da lo mismo ejecutar
  `./node_modules/typescript/bin/tsc --init`

B,3, Configurar typescript

```json
//                            ./tsconfig.json 
{
  "compilerOptions": {
      "target": "es5",
//el 'import ... from "..."' compilara a const 'foo = require("foo");'
      "module": "commonjs",
      "declaration": true,
      "outDir": "./build",
      "strict": true,
// poder usar 'import foo from "foo";' sin obligarnos a hacer 'import * as foo from "foo"'
      "esModuleInterop": true,
//evitar checar intaxis en librerias      
      "skipLibCheck": true,
//case sensitive en los imports
      "forceConsistentCasingInFileNames": true,
  },
  "include": ["src"],
  "exclude": ["**/*.test.ts"],
}
```

...

C, Agregar esLint

C,1, Agregar esLint como devDependency:

```r
npm add -D eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin
```

C,2, Configurar eslint

```js
// ./.eslintrc.js
module.exports = {
    env: {
        node: true,
        jest: true,
    },
    parser: "@typescript-eslint/parser",
    parserOptions: {
        sourceType: "module"
    }
    extends: ["plugin:@typescript-eslint/recommended"],
    rules: {}
}
```

...

D, Agregar prettier

D,1, Instalamos, y agregamos el plugin y conf para evitar conflictos
con eslint

```r
npm add -D prettier eslint-config-prettier eslint-plugin-prettier
```

D,2, Configuramos prettier dentro de eslint, para evitar conflictos

```js
//                    ./.eslintrc.js
module.exports = {
    ...
    parserOptions: {...}
    extends: [
        "plugin:@typescript...",
+       "prettier/@typescript-eslint",
+       "plugin:prettier/recommended",
    ],
    rules: {}
}
```

NOTA: Puede que veamos errores en nuestro `./.eslintrc.js` para eso podemos
ir a en la paleta de comandos y escribir `open settings (JSON)` y vamos
a //EsLint y pasamos source.fixAll.eslint y source.fixAll.markdownlinta true

D,3, Configuramos prettier

```js
//                                 ./.prettierrc.js
module.exports = {
    semi: true, //agregar ";" auto

// agregar coma al final de arrays/objetos, como en este objeto o el
//arreglo de arriba (archivo ./.eslintrc.js) de "extends"
    trailingComa: "all",

//usar comillas simples
    singleQuote: true,

    printWidth: 120, //linea de caracteres
    tabWidth: 2,
}
```

...

E, Agregar Jest

E,1, instalar jest

```r
npm add -D jest @types/jest ts-jest ts-node
```

E,2, Configurar jest

```js
//                               ./jest.config.ts
export default {
    collectCoverageFrom: [
//incluit todos los archivos .ts
        "src/**/*.ts",
// excluir el archivo index
        "!src/index.ts",
    ],
    coverageThreshold: {
        global: {
            branches: 100,
            functions: 100,
            lines: 100,
            statements: 100,
        }
    },
//Buscar los test dentro de este directorio
    roots: ['<rootDir>/src'],
//indicar que vamos a trabajar con typescript
    preset: "ts-jest",
//ambiente donde se ejecutaran los tests
    testEnvironment: "node",
}
```

F, Instalar y configurar husky, esta herramienta nos ayudara a ejecutar comandos
antes de hacer un commit, ya que nosotros podemos olvidar hacer estos
pasos previos.

F,1, Instalar: `npm i -D husky`

F,2, Configurar

```js
// husky.config.js
module.exports = {
  hooks: {
    "pre-commit": "npm run check-types && npx lint --fix",
    "pre-push": "npm run test:coverage",
  }
}
```

...

X, Configurar `package.json` en opciones de libreria, y scripts para
nuestro flujo de trabajo.

```json
{
//debera ser unico en el repositorio npm
  "name": "some_uniq_name",
//semantic version, util para desplieges
  "version": "0.0.1",
  "license": "MIT",
  "author": "Mario Marquez",
  "private": false,
  "keywords": ["something", "mindful"],
  "description": "Some mindful description",

  "devDependencies": { ... },
//cuando alguien incluya nuestro proyecto como libreria, este solo
//incluira lo que hay en build/, para reducir su tamaño
  "files": ["build/**/*"],
//comandos utiles
  "scripts": {
    "check-types": "tsc --noEmit",
    "build": "tsc",
    "test": "jest",
    "test:coverage": "jest --clearCache && jest --coverage",
    "lint": "esLint 'src/**/*.ts'",
  },
//nuestro main estara dentro de build
  "main": "build/index.js",
//tendremos unos tipos personalizados
  "types": "build/index.d.ts"
}
```



## Libreria basica de convertir base N a decimal

