# HTML

- [HTML](#html)
  - [Historia](#historia)
  - [La directiva \<!DOCTYPE html\>](#la-directiva-doctype-html)
  - [Los Enlaces \<a\>](#los-enlaces-a)
  - [Los metadatos](#los-metadatos)
  - [Las etiquetas estructurales o semánticas](#las-etiquetas-estructurales-o-semánticas)
  - [Los formularios](#los-formularios)
  - [Manejo de imagenes](#manejo-de-imagenes)
  - [Manejo de audio y video](#manejo-de-audio-y-video)

## Historia

Hasta el 2006 html era un caos por las grandes empresas como microsoft y
flash, pero en este a;o se reunieron lo que fue mozilla(netscape),
opera (europa) y apple, donde definieron su propio estandart, luego
w3c se unio a ellos y de ahi salio HTML5.

En el 2008 empezo el boom de los smartphones y apple dijo no entra flash
y lo que preciono a los desarrolladores y a las empresa como Adobe a  
empezar a impulzar html5 y css3.

Luego la guerra de los navegadores la empezo a perder Microsoft con la
llegada de Google Chrome, lo que hizo que Microsoft se empezara a acoplar
a HTML5.

## La directiva \<!DOCTYPE html\>

En esta directiva es importante ya que le dice al navegador que estamos
usando HTML5 y asi no haiga problemas con la visualizacion con este.

## Los Enlaces \<a\>

<!-- abrir enlace en otra pestañá, abrir link en otra pestañá, abrir link en una pestaña nueva, abrir enlace en una pestaña nueva, -->

Abrer enlaces en otra pestaña usamos target a blank

```html
<a href="..." target="_blank" rel="noopener noreferrer">...</a>
```

<!-- problema de seguridad, vulnerabilidades, -->

El problema es que target=’_blank’ es una **vulnerabilidad de seguridad**.
La nueva página tiene acceso al objeto window a través del ```window.opener```.

Para evitarlo utilizamos el atributo rel=’noopener’, que bloquea el uso
de este objeto.

```html
<a href="" rel="noopener noreferrer" target="_blank">...</a>
```

## Los metadatos

Otro cambio simplificado es la etiqueta `<meta>`, que indica el conjunto de
caracteres que utilizaremos en la pagina.

```html
<meta charset="utf-8">
<title>mi titulo</title>
```

## Las etiquetas estructurales o semánticas

Anteas para nuestras secciones principales como encabezado, articulo, y pie
lo haciamos a travez de divs con un id con sicho nombre.

No existen reglas extrictas pera que poner dentros de las estiquetas semanticas
ya que puede llegar a ser vastante abstracto/difuso, pero es bastante
recomensable para tener un buen SEO.

- `<header>`: Son los elementos de introduccion a la pagina o a una seccion.
Puede haber varios encabezados en una misma pagina y por lo genral, puede
tener elementos de navegacion.

- `<section>`: Por lo general envuelve etiquetas article.

- `<article>`: Sirve para definir informacion que forma parte de lo
mismo, cmo lo es una nota, el cual tiene un titulo, header etc. pero
todo el contenido no se sale del mismo tema.

- `<aside>`: Como su nombre lo indica es "a un lado" que es generalmente
informacion que no esta relacionada con la pagina, como lo es un menu, publicidad

- `<footer>`: Pie de pagina, este lo podemos poner como un pie de pagina o
como un pie de articulo.

- `<nav>`: Esto indica los enlaces para la navegacion dentro de nuestra
pagina, como es un menu de navegacion.

## Los formularios

- **email**: `<input type="email">`: Tiene una validacion
en el que espera un @ y un punto para que entre algo parecido a un email.

- **url**: `<input type="url">`: Tiene una validacion
en el que espera una url

- **tel**: `<input type="tel">`: Espera un numero telefonico
lo que nos ayuda es en poner un teclado numerico en el movil.

- **number**: `<input type="number" min="1" max="100" step="2">`:
Valida que sea un numero y podemos hacer un avanze de 2 en 2 por el step

- **range**: `<input type="range">`: Nos puede dar un slider para
indicar un rango, igual le podemos poner un max, min y step.

- **Fechas**: Tenemos 6 formas de definir una fecha.

  - **time**: `<input type="time">`: Nos apoyara con un control y  validacion de hora.
  
  - **date**: `<input type="date">`: Nos apoyara con un control y  validacion de fecha.
  
  - **date**: `<input type="datetime">`: fecha y hora en formato UTC (Universal Time Controler).
  
  - **date**: `<input type="datetime-local">`: NO se cual es la dif.
  
  - **date**: `<input type="month">`: solo podemos seleccionar el mes
  
  - **date**: `<input type="week">`: solo la semana.
  
- **Color**: `<input type="color">`: Para introducir colores en formato hezadecimal **#ffffff**

- **Atrubutos en formularios**:

  - **autofocus**: `<input type="text" autofocus>`: Este pone por defecto el cursos en el capo que se le indique
  
  - **placeholder**: `<input type="text" placeholder="algo">`: Es un texto temporal de orientacion al usuario.
  
  - **pattern**: `<input type="text" pattern="algo">`: Para el uso de expreciones regulares o patrones de validacion de texto.
  
  - **required**: `<input type="text" required />`: Es para obligar que se adigne el campo.
  
- **Pseudo-clases**: Son atributos que el navegador pone a veces de forma dinamica que podemos usar para asignar estilos y dar orientacion usuario
  - **required**: `:required { backgroud: #fdd; }`: Podemos agregar estilos
  
  - **valid**: `:valid { backgroud: #dfd; }`: Son los campos que no tienen required.
  
  - **invalid**: `:invalid { backgroud: #f99; }`: Son los campos que no han pasado la validacion.

## Manejo de imagenes

Mostrar imagen codificada en base64, base64 a imagen, mostrar imagen en base64,

```html
<img src="data:image/png;base64, iVB..." 
```

## Manejo de audio y video

Una de las restricciones en los moviles con el video es que la primera opcion debemos de poner el formato .mp4.

- **Video**: `<video src="ruta/video.ogg" controls>no soporta video</video>`: Con esto nos apareceria un reproductor de video con controles.
Para agregar compativilidad con otros navegadores podemos agredarlo de la siguiente manera.
El cual intentara el navegador correr los distintos formatos (es importante intentarlo en los diferentes naveadores)

```html
<video>
  <source src="ruta/video.mp4" type="video/mp4" />
  <source src="ruta/video.webm" type="video/webm" />
  <source src="ruta/video.ogg" type="video/ogv" />
  <p>no soporta</p>
</video>
```

**Audio**: `<audio src="ruta/audio.mp3" controls>no soporta audio</audio>`:
Pasa lo mismo para el audio, podemos agregar los siguiente formatos para
aumentar la compativilidad.

```html
<audio>
  <source src="ruta/audio.m4a" type="audio/mp4" />
  <source src="ruta/audio.ogg" type="audio/ogg" />
  <p>no soporta tu navegador</p>
</audio>
```

**Etiquetas**:

- **preload="none|auto|metadata"**: Ahi que tener cuidado con esta ya que
le podemos costar datos al usuario o si son muchos. Sus valores pueden ser "none" no hace
precarga "auto" carga el automaticamente, "metadata" solo carga los metadatos del archivo.

- **\<video|audio autoplay /\>**: Con esto al cargar la pagina iniciara la reproduccion.

- **\<video|audio loop /\>**: Para poner loop en la reproduccion.

- **\<video poster="ruta/imagen.png|gif" /\>**: Sirve para poner una
  imagen representativa del video. ya sea que tenga la precarga o no.

- **\<video width="111px" haight="111px" /\>**: podemos poner el tamaño
  de nuestro video para que se adapte, es recomendable tener el poster
  con el mismo tamaño. Y en caso querer ponerlo con css
