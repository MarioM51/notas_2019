# CSS Grid y Flexbox, La Guía Definitiva, Crea + 10 Proyectos

- [CSS Grid y Flexbox, La Guía Definitiva, Crea + 10 Proyectos](#css-grid-y-flexbox-la-guía-definitiva-crea--10-proyectos)
  - [Intro](#intro)
    - [¿Que es Flexbox?](#que-es-flexbox)
    - [3. El Contenedor Flex o Flex Container](#3-el-contenedor-flex-o-flex-container)
    - [4. Flex Direction](#4-flex-direction)
    - [5. Flex Wrap](#5-flex-wrap)
    - [6. Flex Flow y Order](#6-flex-flow-y-order)
    - [7. Flex Grow, Flex Shrink, Flex Basis y el Shorthand Flex](#7-flex-grow-flex-shrink-flex-basis-y-el-shorthand-flex)
    - [9. Alineando con Flexbox](#9-alineando-con-flexbox)
    - [10.11.12. Poniendo todo en práctica](#101112-poniendo-todo-en-práctica)
  - [Sección 3: PROYECTO: Diseño de tipo Cards en Flexbox](#sección-3-proyecto-diseño-de-tipo-cards-en-flexbox)
    - [14. Agregando Media Queries a los Cards](#14-agregando-media-queries-a-los-cards)
    - [15. Estilos a los cards](#15-estilos-a-los-cards)
  - [Sección 4: Creando Menús de Navegación con Flexbox](#sección-4-creando-menús-de-navegación-con-flexbox)
    - [17. Creando un Menú con Iconos (Pt 1 de 2)](#17-creando-un-menú-con-iconos-pt-1-de-2)
    - [18. Creando un Menú con Iconos (Pt 2 de 2)](#18-creando-un-menú-con-iconos-pt-2-de-2)
    - [19. Creando un Dropdown Menu, sub sub menu](#19-creando-un-dropdown-menu-sub-sub-menu)
  - [Sección 5: PROYECTO: Sitio Web de Cursos](#sección-5-proyecto-sitio-web-de-cursos)
    - [23. Instalando Dependencias](#23-instalando-dependencias)
    - [24. Compilando SASS con Gulp](#24-compilando-sass-con-gulp)
    - [25. Agregando un watch](#25-agregando-un-watch)
    - [26. Comenzando los estilos del Proyecto](#26-comenzando-los-estilos-del-proyecto)
    - [27. Estilos al carrito de compras con SASS](#27-estilos-al-carrito-de-compras-con-sass)
    - [28. Agregando una fuente de Google Fonts](#28-agregando-una-fuente-de-google-fonts)
    - [29. Creando un Mixin](#29-creando-un-mixin)
    - [30. Creando otros mixins útiles](#30-creando-otros-mixins-útiles)
    - [31. Creando la imagen principal y buscador](#31-creando-la-imagen-principal-y-buscador)
    - [32. Creando mixins para media queries](#32-creando-mixins-para-media-queries)
    - [33. Creando la barra principal](#33-creando-la-barra-principal)
    - [34.35. Creando la sección de los cursos](#3435-creando-la-sección-de-los-cursos)
    - [36. Creando el footer](#36-creando-el-footer)
  - [Sección 6: PROYECTO: Login de Netflix con SASS, Gulp y Flexbox](#sección-6-proyecto-login-de-netflix-con-sass-gulp-y-flexbox)
    - [44. CSS al Body](#44-css-al-body)
    - [45. CSS al Logo](#45-css-al-logo)
    - [47. CSS a los inputs](#47-css-a-los-inputs)
    - [46. Primeros pasos con el formulario](#46-primeros-pasos-con-el-formulario)
    - [49. Creando el botón de Iniciar Sesión](#49-creando-el-botón-de-iniciar-sesión)
    - [50. Finalizando el formulario](#50-finalizando-el-formulario)
    - [51. CSS al Footer](#51-css-al-footer)
    - [RESULTADO PROYECTO](#resultado-proyecto)
  - [Sección 7: PROYECTO: Diseño de un Blog](#sección-7-proyecto-diseño-de-un-blog)
    - [57. Estilos al Header](#57-estilos-al-header)
    - [59. Creando la sección de categorias](#59-creando-la-sección-de-categorias)
    - [61. Creando el footer](#61-creando-el-footer)
  - [Sección 8: PROYECTO: Holy Grail Layout o Layout de 1 o 2 columnas con contenido al centro](#sección-8-proyecto-holy-grail-layout-o-layout-de-1-o-2-columnas-con-contenido-al-centro)
    - [65. Agregando los Sidebars](#65-agregando-los-sidebars)
  - [Sección 9: Otros diseños en Flexbox](#sección-9-otros-diseños-en-flexbox)
    - [66. Creando cajas de comentarios](#66-creando-cajas-de-comentarios)
    - [67. Diseño con Footer y header siempre arriba y fijo abajo aunque poco contenido](#67-diseño-con-footer-y-header-siempre-arriba-y-fijo-abajo-aunque-poco-contenido)
    - [68. Diseño con elementos en distinta alineación](#68-diseño-con-elementos-en-distinta-alineación)
    - [70. Creando un Diseño Masonry en Flexbox](#70-creando-un-diseño-masonry-en-flexbox)
  - [Sección 10: PROYECTO: AdminFlex o Panel de Administración en Flexbox](#sección-10-proyecto-adminflex-o-panel-de-administración-en-flexbox)
    - [75. Estilos al Nombre del sitio](#75-estilos-al-nombre-del-sitio)
    - [77. Estilos a los menús](#77-estilos-a-los-menús)
    - [78. Creando la barra lateral](#78-creando-la-barra-lateral)
    - [79. Estilos al buscador](#79-estilos-al-buscador)
    - [80. Estilos al Menú Lateral](#80-estilos-al-menú-lateral)
    - [81. Ocultando y mostrando el Menú Principal](#81-ocultando-y-mostrando-el-menú-principal)
    - [82.83. Estilos al área principal Creando los formularios](#8283-estilos-al-área-principal-creando-los-formularios)
    - [52\_flexadmin\_formulario\_fin](#52_flexadmin_formulario_fin)
  - [Sección 11: Introducción a CSS Grid](#sección-11-introducción-a-css-grid)
    - [84. ¿Qué es CSS Grid? y términos básicos](#84-qué-es-css-grid-y-términos-básicos)
    - [85. El Grid Container](#85-el-grid-container)
    - [86. Definiendo columnas y rows](#86-definiendo-columnas-y-rows)
    - [87. Colocando elementos dentro de los rows y columns](#87-colocando-elementos-dentro-de-los-rows-y-columns)
    - [88. Grid Shorthand, grid-row, grid-column, grid-area](#88-grid-shorthand-grid-row-grid-column-grid-area)
    - [89. Repeat en CSS Grid](#89-repeat-en-css-grid)
    - [90. Grid Template Areas](#90-grid-template-areas)
    - [91. Grid Template Areas en práctica](#91-grid-template-areas-en-práctica)
    - [92. Modificar el orden de los elementos según la resolución con Grid](#92-modificar-el-orden-de-los-elementos-según-la-resolución-con-grid)
    - [93. Grid FR, Repeat y otras opciones disponibles](#93-grid-fr-repeat-y-otras-opciones-disponibles)
    - [94. Pendiente](#94-pendiente)
    - [96. Grid Auto-fit y Auto-fill](#96-grid-auto-fit-y-auto-fill)
  - [Sección 12: PROYECTO: Blog de Recetas en CSS Grid](#sección-12-proyecto-blog-de-recetas-en-css-grid)
    - [98. Finalizando el Proyecto auto-fit](#98-finalizando-el-proyecto-auto-fit)
  - [Sección 13: PROYECTO: Sitio Web de Bienes Raices con CSS Grid](#sección-13-proyecto-sitio-web-de-bienes-raices-con-css-grid)
    - [100. Creando y utilizando variables CSS](#100-creando-y-utilizando-variables-css)
    - [102. Agregando Fuentes de Google Fonts](#102-agregando-fuentes-de-google-fonts)
    - [103. Creando la sección de Nosotros](#103-creando-la-sección-de-nosotros)
    - [105.107. Creando la sección de Anuncios](#105107-creando-la-sección-de-anuncios)
    - [108. Creando el Footer](#108-creando-el-footer)
  - [Sección 14: PROYECTO: Sitio Web para un Freelancer](#sección-14-proyecto-sitio-web-para-un-freelancer)
    - [110. Creando el Header](#110-creando-el-header)
    - [112. Creando la sección de Servicios](#112-creando-la-sección-de-servicios)
    - [114. Creando la sección de Portafolio](#114-creando-la-sección-de-portafolio)
    - [115. Utilizando Javascript Fetch API para llenar el portafolio](#115-utilizando-javascript-fetch-api-para-llenar-el-portafolio)
    - [116. Estilos al Portafolio](#116-estilos-al-portafolio)
    - [117. Creando el Footer](#117-creando-el-footer)
  - [Sección 15: PROYECTO: AirBNB, recreando la página Principal](#sección-15-proyecto-airbnb-recreando-la-página-principal)
    - [123. Creando la primer sección de Anuncios](#123-creando-la-primer-sección-de-anuncios)
    - [125. HTML para la sección Premium](#125-html-para-la-sección-premium)
    - [128. Creando la sección de Hospedaje](#128-creando-la-sección-de-hospedaje)
    - [128. Creando la sección de Destinos y anuncios](#128-creando-la-sección-de-destinos-y-anuncios)
    - [132.135. Creando el footer y botón mostrar/ocultar el footer JS](#132135-creando-el-footer-y-botón-mostrarocultar-el-footer-js)
    - [138. Últimos Ajustes al Proyecta](#138-últimos-ajustes-al-proyecta)
  - [Mis descubimientos](#mis-descubimientos)
    - [Errores](#errores)
    - [Falta por estudiar](#falta-por-estudiar)
  - [Proyectos](#proyectos)

## Intro

### ¿Que es Flexbox?

el modulo de caja flexible, comunmente llamado flexbox
fue diseñado con un modelo unidimencional de layout, y
como un metodo que pueda ayudar a distribuir el espacio
entre los elementos de una interfaz y mejorar las
capacidades de alineacion.

### 3. El Contenedor Flex o Flex Container

**IMPORTANTE:** Siemre debemos de agregar un elemento padre, para poder aplicar el
flexbox, y solo afecta a los elementos hijo, no afecta a los elementos nieto.

```html
<div class="contenedor">
  <div class="contenedor-flex">
      <div class="elemento-flex elemento-1">
          <h2>Servicio 1</h2>
      </div>
      <div class="elemento-flex elemento-2">
          <h2>Servicio 2</h2>
      </div>
      <div class="elemento-flex elemento-3">
          <h2>Servicio 3</h2>
      </div>
  </div>
</div>
```

Por defecto pondra los div un lado del otro y el contenedor tomara
todo el ancho disponible, este es el mas comun.

```css
.contenedor-flex {
    display: flex;
    border: 1px solid black;
}
```

![01_display_flex](./img/01_display_flex.PNG)

Este el contenedor flex solo tomara el espacio que ocupan sus componentes, es poco usado.

```css
.contenedor-flex {
    display: inline-flex;
}
```

![02_display_flex](./img/02_display_inline_flex.PNG)

### 4. Flex Direction

Siempre la declaramos en el contenedor flex, solo puede ir en dos direcciones (calumnas y filas),con css-grid ya podremos ir en otras direcciones.
row, valor por defecto que hace que un contenedor se ponga a un lado del otro,el cual luce igual que arriba.

```css
.contenedor-flex {
    display: flex-inline;
    flex-direction: row;    //por defecto
}
```

colum

```css
.contenedor-flex {
    display: flex;
    flex-direction: column;
}
```

![02_display_flex](./img/03_flex-direction_column.PNG)

row-reverse

```css
.contenedor-flex {
    display: flex;
    flex-direction: row-reverse;
}
```

![02_display_flex](./img/04_flex-direction_row-reverse.PNG)

column-reverse, notar que el orden de los div se invierte

```css
.contenedor-flex {
    display: flex;
    flex-direction: column-reverse;
}
```

![05_flex-direction_column-reverse](./img/05_flex-direction_column-reverse.PNG)

### 5. Flex Wrap

Si ponemos varios div estos se desvordaran por ya que por defectoesta la propiedad **flex-wrap: nowrap**, pero podemos solucionar estocambiando esta propiedad.

```css
.contenedor-flex {
  display: flex; flex-direction: row;
  flex-wrap: nowrap    /* por defecto */
}
```

![wrap_nowrap](./img/06_flex-wrap_nowrap.PNG)

Evitamos que se desvorden los div

```css
.contenedor-flex {
    display: flex; flex-direction: row;
    flex-wrap: wrap;
}
```

![07_flex-wrap_wrap](./img/07_flex-wrap_wrap.PNG)

Tenemos la opcion d ponerlos al revez con

```css
.contenedor-flex {
    display: flex; flex-direction: row;
    flex-wrap: wrap-reverse;
}
```

![wrap-reverse](./img/08_flex-wrap_wrap-reverse.PNG)

### 6. Flex Flow y Order

Podemos controlar en que orden mostrar los elementos, estos pueden sermuy utiles por ejemplo con query mostrar **un orden u otro segun el tamaño de pantalla**.
Aqui a pesar de tener en el html ordenados por su numero, losmostramos, segun lo indicamos en el css

```css
@media screen and (max-width:370px) {
  .elemento-flex:nth-child(1) { order: 2; }
  .elemento-flex:nth-child(2) { order: 3; }
  .elemento-flex:nth-child(3) { order: 1; }
}
```

![09_order](./img/09_order.PNG)

### 7. Flex Grow, Flex Shrink, Flex Basis y el Shorthand Flex

Hasta ahora los nuestros elementos solo crecen segun su contenido, pero
igual podemos hacer que se repartan el espacio disponible, que va mas
con la filosofia flexible.

Y podemos indicar que componentes pueden creser mas o menos.
flex-grow

```css
.elemento-flex {
    flex-grow: 1;
}
.elemento-flex:nth-child(1) { flex-grow: 2; }
.elemento-flex:nth-child(2) { }
.elemento-flex:nth-child(3) { }
```

![flexGrow](./img/10_flex-grow.gif)

flex-shink, Tenemos el contrario, para hacer que no crest.

```css
.elemento-flex {
    flex-grow: 1;
}
.elemento-flex:nth-child(1) { }
.elemento-flex:nth-child(2) { flex-grow: 0; flex-shink: 0; }
.elemento-flex:nth-child(3) { }
```

![flex-shink](./img/11_flex-shink.gif)

flex-basis, podemos marcar un crecimiento inicial, podemos usar
pixeles, rem etc.

```css
.elemento-flex {  flex-grow: 1; }
.elemento-flex:nth-child(1) { }
.elemento-flex:nth-child(2) { }
.elemento-flex:nth-child(3) { flex-basis: 60%; }
```

![12_flex-basis](./img/12_flex-basis.gif)

flex, podemos usar los 3 parametros con esta propiedad que es 1) grow, 2) shink, 3) basis

### 9. Alineando con Flexbox

Podemos indicar de donde empezara a acomodar los elementos

al final

```css
.contenedor-flex {
    display: flex;
    justify-content: flex-end;
}
```

Observar que a diferencia de arriba este conserva el orden.

![13_justify-content_flex-end](./img/13_justify-content_flex-end.PNG)

al centro

```css
.contenedor-flex {
    display: flex;
    justify-content: center;
}
```

Observar que a diferencia de arriba este conserva el orden.

![14_justify-content_center](./img/14_justify-content_center.PNG)

centrar usando todo el espacio disponible

```css
.contenedor-flex {
    display: flex;
    justify-content: space-around;
}
```

![15_justify-content_space-aroundr](./img/15_justify-content_space-aroundr.PNG)

centrar usando todo el espacio disponible sin espacios al final

```css
.contenedor-flex {
    display: flex;
    justify-content: space-between;
}
```

![16_justify-content_space-aroundr](./img/16_justify-content_space-aroundr.PNG)

Si al contenedor lo hacemos mas alto, sus elementos se haranmas altos por defecto

```css
.contenedor-flex { border: 1px solid black;
  height: 100px;
  display: flex;
}
```

![17_contenedor_flex_alto-por_defecto](./img/17_contenedor_flex_alto-por_defecto.PNG)

<!-- alinear elementos horizontalmente, alinear elemento horizontalmente -->
align-items: flex-start, mantener alto de elemento y arriba.

```css
.contenedor-flex { border: 1px solid black;
  height: 100px;
  display: flex;
  align-items: flex-start;
}
```

![18_align-items_flex-start](./img/18_align-items_flex-start.PNG)

align-items: flex-end, mover elemento abajo.

```css
.contenedor-flex { border: 1px solid black;
  height: 100px;
  display: flex;
  align-items: flex-end;
}
```

![18_align-items_flex-end](./img/18_align-items_flex-end.PNG)

align-items: center, mover elemento al centro.

```css
.contenedor-flex { border: 1px solid black;
  height: 100px;
  display: flex;
  align-items: center;
}
```

![19_align-items_center](./img/19_align-items_center.PNG)

align-self, Para mover solo un elemento usamos este
<!-- alinear elementos verticalmente, alinear elemento verticalmente -->

```css
.contenedor-flex { border: 1px solid black; height: 100px;
  display: flex;
  justify-content: space-around;
}
.elemento-flex { }
.elemento-flex:nth-child(1) {  align-self: start; }
.elemento-flex:nth-child(2) { align-self: center; }
.elemento-flex:nth-child(3) { align-self: flex-end; }
```

![20_align-self](./img/20_align-self.PNG)

### 10.11.12. Poniendo todo en práctica

Iniciamos con E:\cursos\0activ\web\flexbox\basicos-flexbox\07-practica-inicio

![21_proyecto_basico](./img/21_proyecto_basico.gif)

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

## Sección 3: PROYECTO: Diseño de tipo Cards en Flexbox

Partiremos de E:\cursos\0activ\web\flexbox\cards-flexbox-inicio donde solo tenemos
un conjunto de productos los cuales queremos mostrar como cartas.

![22_proyecto_cards_inicio](./img/22_proyecto_cards_inicio.gif)

### 14. Agregando Media Queries a los Cards

![23_proyecto_cards_una_dos_y_tres_columnas](./img/23_proyecto_cards_una_dos_y_tres_columnas.gif)

### 15. Estilos a los cards

![24_proyecto_cards_estilos](./img/24_proyecto_cards_estilos.gif)

----

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

## Sección 4: Creando Menús de Navegación con Flexbox

### 17. Creando un Menú con Iconos (Pt 1 de 2)

1, Descargamos los iconos de fontawesome.com

2, En nuestro html incluimos: ``` <link rel="stylesheet" href="./css/all.min.css"> ```

3, Vamos a fontawesome.com -> iconos buscamos el icono que queremos y lo seleccionamosel cual nos dara eodigo que usaremos, en mi caso ``` <i class="fab fa-facebook-square"> ```.
Esto nos deberia de dar el icono de face.

4, Creamos la estructura

```html
<nav clas="iconos">
  <ul>
    <li>
      <div class="icono"><i class="fas fa-home"></i></div>
      <div class="texto"><a href="#">
        Inicio
        <span>Pagina principal</span></a>
      </div>
    </li>
    <li>
      <div class="icono"><i class="fas fa-envelope"></i></div>
      <div class="texto"><a href="#">
        Contacto
        <span>Escribenos aqui</span></a>
      </div>
    </li>
    <li>
      <div class="icono"><i class="fas fa-dollar-sign"></i></div>
      <div class="texto"><a href="#">
        Tienda Virtual
        <span>Adquiere nuestros articulos</span></a>
      </div>
    </li>
    <li>
      <div class="icono"><i class="fas fa-question"></i></div>
      <div class="texto"><a href="#">
        Asistencia
        <span>Nesecitas ayuda</span></a>
      </div>
    </li>
  </ul>
</nav>
```

![25_menu_con_iconos_solo_html](./img/25_menu_con_iconos_solo_html.PNG)

### 18. Creando un Menú con Iconos (Pt 2 de 2)

css base

```css
/* css base */
body { box-sizing: border-box; margin:0; background: #ff9966;  /* fallback for old browsers */ background: -webkit-linear-gradient(to right, #ff5e62, #ff9966);  /* Chrome 10-25, Safari 5.1-6 */ background: linear-gradient(to right, #ff5e62, #ff9966); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */ font-family: 'IBM Plex Serif', serif; }
.contenedor { background-color: white; min-height: calc( 100vh - 20px ); max-width: 1000px; margin: 0 auto; padding: 20px 30px 0 30px; }
nav { background-color: #3F51B5; }
nav ul { list-style:none; margin:0; padding:0; }
nav ul li { padding: 20px; }
nav ul li:hover { background-color: #1a2770; }
nav ul a { color: white; text-decoration: none; font-size: 1.4rem; }
```

css para el menu con iconos

```css
* {
    box-sizing: border-box;   /* ignorar el padding general*/
}

.iconos ul li {
    display: flex; /* mostrar el titulo e icono un lado al otro (columnas) */
}

.iconos .icono {
    flex: 0 1 2rem; /* damos separacion entre icono titulo */
}

.iconos .texto span {
    display: block; /* mostrar bajo el texto */
    font-size: .8rem;
    margin-top: .5rem;
}

@media screen and (min-width:480px) { /* mostrar 2 iconos arriba 2 abajo */
    .iconos ul {
        display: flex; /* acomodamos horizontalmnete y no verticalmente*/
        flex-wrap: wrap; /* ahi un punto que se oculta el menu asi que hacemos que caiga cuando no ahiga espacio*/
    }

    .iconos ul li {
        flex: 0 1 50%;  /* mostramos 2 arriba 2 abajo */
    }
}

@media screen and (min-width:760px) { /* mostrar de forma vertical */
    .iconos ul li {
        flex: 1;  /* repartir cada opcion en espacios iguales */
    }
}
```

Resultado:

![menu_con_iconos](./img/26_menu_con_iconos.gif)

### 19. Creando un Dropdown Menu, sub sub menu

Menu mas completo: [codepen.io](https://codepen.io/barkins/pen/KzrKrj)

![27_1_menu_dropdown_prefabricado](./img/27_1_menu_dropdown_prefabricado.gif)

html

```html
<h2>Dropdown Menu</h2>
<nav class="dropdown">
  <ul>
    <li><a href="#">Inicio</a></li>
    <li><a href="#">Nosotros</a>
      <ul>
        <li><a href="#">Misión</a></li>
        <li><a href="#">Visión</a></li>
        <li><a href="#">Valores</a></li>
      </ul>
    </li>
    <li><a href="#">Tienda Virtual</a>
      <ul>
        <li><a href="#">Producto 1</a></li>
        <li><a href="#">Producto 2</a></li>
        <li><a href="#">Producto 3</a></li>
      </ul>
    </li>
    <li><a href="#">Asistencia</a></li>
  </ul>
</nav>
```

Estilos

```css
.dropdown ul {
    display: flex;
    flex-direction: column; /* por defecto es row pero cuando sea pequeño se mostrara asi column=vertical */
}

@media screen and (min-width:770px) {
    .dropdown ul {
        flex-direction: row; /* lo mostramos en filas=horizontal */
    }
}

.dropdown ul li {
    display: flex;
    justify-content: center;
    flex-direction: column; /* poner el sub-menu en la parte inferior */
}


@media screen and (min-width:770px) {
    .dropdown ul li {        position: relative;
        display: flex;
        flex: 1 1 100%;
    }

    .dropdown ul li ul { /*  sub menu */
        position: absolute;
        top: 100%;          /*  acomodamos siempre abajo del primer nivel de menu */
        background-color: #ff5e62;
        display: none; /*  por defecto no mostrar asta pasar el raton por encima */
    }

    .dropdown ul li:hover ul { /* mostrar sub-menus cuando pacemos encima el raton */
        display: flex;
        flex-direction: column; /* mostrar sub-menu verticalmente  */
        left: 0; /* alinear con el menu padre a la izq  */
        width: 100%; /* mostrar el mismo ancho que el menu padre  */
    }
}
```

Resultado:

![27_menu_dropdown](./img/27_menu_dropdown.gif)

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

## Sección 5: PROYECTO: Sitio Web de Cursos

<!-- Diseño de udemy, pantilla de udemy, home de udemy, plantilla carrito de compras,file:///E:/cursos/0activ/web/flexbox/carrito/index.html
-->

Crearemos un proyecto muy parecido al de udemy

<!-- implementar sass, instalar sass, agregar sass, setup sass, agregar sass -->
### 23. Instalando Dependencias

Debemos de tener node y npm instalados.

1, Instalamos Gulp para gestionar el auto-prefix (webkit-XXX ms-XXX etc) para mayor compativilidad, ymanejo de pipelines.

<!-- usar gulp, uso de gulp, iniciar gulp, instalar gulp, compilar sass con gulp, sass compilar, compilar scss compilar, transcompilar scss, trascompilar scss -->

```r
npm install -g gulp gulp-cli
```

2, Generamos el package.json `npm init`

**Error:** node se congela, o al querer instalar se congela.**Causa:** Al parecer un tema de permisos.**Solucion:** Ejecutar los comnados como administrador.

3, Agregamos gulp como una dependencia de desarrollo `npm install --save-dev gulp gulp-autoprefixer gulp-sass`

3.1. Instalamos el prefixer y sass (se puede en la misma linea pero gulp es el mas pesado)

```r
npm install --save-dev gulp gulp-autoprefixer gulp-sass
```

4, en package.json Configuramos el auto-prefixer indicando que solo
agrege conpativilidad con aquellos navegadores con mas de 1% de uso.

```json
"browserslist": [ "> 1%", "last 2 versions"]
```

### 24. Compilando SASS con Gulp

Trabajaremos sobre el gulpfile para compilar nuestro sass y agregar los prefijos de forma auto.

```js
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

//funcion para compilar el sass
function css() {
    return gulp
    .src('scss/app.scss')
    .pipe(autoprefixer({
        overrideBrowserslist: ['last 2 versions'],
        cascade: false
    }))
    .pipe(sass({
        autputStyle: 'expanded' // expanded = desarrollo compressed = produccion
    }))
    .pipe(gulp.dest('css'))
    ;
}

//registrar tareas como tareas
gulp.task('css', css);
```

2, Agregamos codigo sass

```css
$color-primario: #ffaacc;
body {
    background-color: $color-primario;
}
```

1. Compilamos css usando la funcion que agregamos: ``` gulp css ```

**Resultado:** Nos debe de crear una carpeta con un archivo css con el codigoque le agregamos.

### 25. Agregando un watch

**Objetivo** Para no tener que compilar cada rato vamos a agregar un watchpara que cada ver que cambie algo, se compile nuestro scss

1. En gulpfile agregamos nuestro watch

```js
function watchArchivos() {
    gulp.watch('scss/*.scss', css);
}
gulp.task('watch', watchArchivos);
```

2, Iniciamos el watch el cual no terminara, y ejecutara la funcion
que indicamos cada vez que hagamos un cambio en los archivos que
indicamos. `gulp watch`

<!-- ver cambios en vivo, mostrar cambios en vivo, refrescar cambios en vivo, aplicar auto-refresh, aplicar auto refresh, poner auto-refresh, poner auto refresh, implementar auto-refresh, implementar auto refresh, -->

3, Para ver cambios en vivo podemos usar un plugin llamado live-server de visualstudio code

<!-- live server -->

![28_live_server_watch_scss](./img/28_live_server_watch_scss.gif)

### 26. Comenzando los estilos del Proyecto

Trabajamos sobre el header.- poner el logo e icono de carrito uno al
lado de otro.- ponemos el logo e icono de carro a los extremos izq y
der.- ocultamos contenido del carrito.

```scss
.header {
    display: flex; /* poner el logo e icono de carrito uno al lado de otro */
    justify-content: space-between; /* ponemos el logo e icono de carro a los extremos izq y der */
    align-items: center;
    .logo {        padding: 1.4rem 0;
    }
}

.oculto {
    display: none; /* ocultar contenido del carrito */
}
```

### 27. Estilos al carrito de compras con SASS

- mostrar el carrito cuando tengamos el raton encima.- hacer que no afectar el demas contenido al mostrar contenido oculto.

```scss
.carrito-compras {
  position: relative; /* 1de2 evitar que afecte al contenido de la pagina cuando se muestre el carrito */
  &:hover .contenido { /* el & es para que cuado transcompile este junto el hover */
    display: block;  /* mostramos el carrito en un cuadro */
    position: absolute; /* 2de2 evitar que afecte al contenido de la pagina cuando se muestre el carrito */
    right: 0;
    background-color: $gris;
    padding: 1rem;
    max-width: 400px;

    img { max-width: 150px; }

    .boton {      background-color: $btn-danger;
      padding: .5rem 1rem;
      margin-top: 1rem; /* no se puede aplicar por que los enlaces tienen un display inline */
      display: block;
      text-align: center;
      color: white;
      text-decoration: none;
    }
  }
}
```

### 28. Agregando una fuente de Google Fonts

1, Agregamos fuentes de google

```html
<link href="https://fonts.googleapis.com/css?family=Raleway:400,900" rel="stylesheet">
```

2, Creamos las variables en otro archivo que empieze por "_" (en mi caso "_variables") pàra incluirlo

<!--uso de variables en scss, uso de variables scss, como declarar variables en scss,uso de variables en .scss, uso de variables .scss, como declarar variables en .scss,-->

```scss
 /* Colores */
$color-primario: #66c6f3;
$color-secundario: #714d51;
$color-terciario: #e44e52;
$btn-danger: #ff4444;
$gris: #f2f2f2;

/* Fuentes */
$fuente-principal: 'Releway', sans-serif;
```

3, importamos el archivo, importar archivo scss importar scss

```css
@import 'variables';
```

### 29. Creando un Mixin

**Objetivo:** Poder reutilizar el estilo del boton que creamos parael carrito.

1, Creamos archivo para importar "_mixins.scss"

```scss
@mixin boton($color) {
  background-color: $color;
  padding: .5rem 1rem;
  margin-top: 1rem; /* no se puede aplicar por que los enlaces tienen un display inline */
  display: block;
  text-align: center;
  color: white;
  text-decoration: none;
}
```

2, usar mixin

```scss
.boton {    @include boton($btn-danger);
}
```

### 30. Creando otros mixins útiles

Creamos mixin

```scss
@mixin box-sizing($box-model) {
    box-sizing: $box-model;
}
```

lo aplicamos

```scss
html {
    @include box-sizing(border-box);
}

/* evitar que el la pagina cresca al agregar paddings (como es inherit tomara el border-box que definimos para html) */
*,*:after,
*:before {
    @include box-sizing(inherit);
}
```

### 31. Creando la imagen principal y buscador

creamos mixin para agregar imagen de fondo que rellene

```scss
@mixin background-full-image($imagen) {
  background-image: url($imagen);
  background-repeat: no-repeat;
  background-position: center center;
}
```

```css
.contenido-hero {
  flex: 1;

  h2 {
    font-weight: 400;
    font-size: 2.4rem;
  }

  p {
    font-size: 1.2rem;
  }

  form {
      position: relative;
      input[type="submit"] {
        position: absolute;
        right: 0;
        top: 0;
        background-position-x: 10px;
        width: 3rem;
        height: 3rem;
        /* quitamos el texto del boton y ponemos lupa*/
        @include background-full-image('../img/lupa.png');
        display: block;
        text-indent: -9999px;
        border-left: -5px;
        cursor: pointer;
      }

      input[type="text"] {
          width: 100%;
          height: 3rem;
      }
  }
}
```

### 32. Creando mixins para media queries

creamos variables de tamaños de queries, <!-- variables pixeles de pantallas, variables de pixeles de pantallas, tamaños de pantallas, medida pantallas -->

```scss
$telefono: 480px;
$tablet: 768px;
$escritorio: 1024px;
```

Creamos mixin para reutilizar los media queries <!-- mixin para media querys -->

```scss
/* media queris */
@mixin telefono() {
  @media only screen and (min-width:#{$telefono}) {
    @content
  }
}

@mixin tablet() {
  @media only screen and (min-width:#{$tablet}) {
    @content
  }
}

@mixin escritorio() {
  @media only screen and (min-width:#{$escritorio}) {
    @content
  }
}
```

lo aplicamos

```scss
form {...
  @include tablet() {
      width: 50%;
  }...
}
```

**Resultado:**

![29_riel_buscador](./img/29_riel_buscador.gif)

### 33. Creando la barra principal

**Objetivo** Crearemos la fila de secciones, de lo de la izq a derecha.

![30_objetivo_datos](./img/30_objetivo_datos.PNG)

agerar mixin para degradado

```scss
@mixin degradado($color1, $color2) {
  background: linear-gradient(to right, $color1 0%,  $color2 100%);
}
```

estilos

```scss
.barra {
  min-height: 6rem;
  padding: 1rem;
  display: flex;
  align-items: center;
  @include degradado($color-secundario, $color-terciario);
  color: white;
  .iconos {
    display: flex;
    flex: 1;
    flex-direction: column;
    @include tablet() {
      flex-direction: row;
    }
  }

  .icono {
    flex: 1;
    background-position: left center !important;
    background-size: 2.5rem;
    text-align: center;
     &.icono1 {
      @include background-full-image('../img/icono1.png');
    }

    &.icono2 {
      @include background-full-image('../img/icono2.png');
    }

    &.icono3 {
      @include background-full-image('../img/icono3.png');
    }
  }
}
```

Resultado

![31_resultado_datos.gif](./img/31_resultado_datos.gif)

### 34.35. Creando la sección de los cursos

Inicio

![32_lista_cursos_inicio](./img/32_lista_cursos_inicio.PNG)

```css
.cursos {
    display: flex;
    flex-wrap: wrap; /* acomodamos los cursos auno al lado del otro y que caigan*/
    justify-content: space-between;

    .curso {
        border: 1px solid darken($gris, 40);
        margin-bottom: 1rem;

        .imagen-curso {
            width: 100%;
        }

        flex: 0 1 100%;
        @include tablet() {
            flex: 0 1 calc(33% - 1rem);
        }

        @include escritorio() {
            flex: 0 1 calc(25% - 1rem);
        }
    }
}

.info-curso {
    padding: 1rem;
    h4 { margin: 0; font-size: 1.2rem; }
    .precio {
        display: flex;
        justify-content: space-between;
        .regular {
            text-decoration: line-through;
            color: red;
        }
        .oferta {
            font-size: 1.6rem;
            font-weight: 700;
            color: darken(green, 5);
        }
    }
    .boton {
        @include boton($color-secundario);
    }
}
```

Resultado

![33_lista_cursos_resultado](./img/33_lista_cursos_resultado.gif)

### 36. Creando el footer

```scss
.footer {
    border-top: 1px solid darken($gris, 40);
    margin-top: 1rem;
    padding-top: 2rem;
    .contenedor-footer {
        display: flex;
        flex-direction: column;
        @include telefono() {
            flex-direction: row;
        }
    }
}

.menu {
    display: flex;
    flex-direction: column;
    margin-bottom: 1rem;
    flex: 1;
    @include escritorio { flex: 0 1 calc(30% - 1rem); }
    a { color: black; margin-bottom: 2rem; }
}
```

![34_footer_resultado](./img/34_footer_resultado.gif)

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

## Sección 6: PROYECTO: Login de Netflix con SASS, Gulp y Flexbox

1, instalamos gulp y dependencias.

<!-- evitar problemas con padding -->

2, Agregamos snnipted para evitar problemas con el padding margin. mixin

```scss
@mixin boxing($box-model) {
  box-sizing: $box-model;
}
```

uso

```scss
html {
  @include boxing(border-box);
}

*,*:before,
*:after {
  @include boxing(inherit);
}
```

agregamos html

```scss
<body>
  <header class="site-header">
      <img src="img/logo.svg" alt="logo" />
  </header>
  <main class="formulario-login contenedor">
      <form action="#">
        <legend>Inicia sesion</legend>
        <div class="campo">
          <input type="text"  id="usuario"/>
          <label>Email o numero de telefono</label>
        </div>
        <div class="campo">
          <input type="password"  id="password"/>
          <label>Contraseña</label>
        </div>
        <div class="submit">
          <input type="submit" value="Iniciar sesion">
        </div>
        <div class="acciones">
          <div class="recuerdame">
            <input type="checkbox">
            <label>Recuerdame</label>
          </div>
          <div class="ayuda"><a href="acciones">¿Necesitas ayuda?</a></div>
        </div>
      </form>
    <div class="enlaces-formulario">
      <a href="#">Iniciar sesion con facebook</a>
      <a href="#">¿Promera vez en netflix? <span>Suscribite</span></a>
    </div>
  </main>
  <footer class="site-footer">
    <div class="contenedor">
      <p>¿Preguntas? Llama al 01800-234-1234</p>
      <nav class="menu-footer">
        <a href="#">Terminos de las tarjetas de regalo</a>
        <a href="#">Terminos de uso</a>
        <a href="#">Declaracion de privacidad</a>
      </nav>
    </div>
  </footer>
  <select class="laguage">
    <option value="eng">English</option>
    <option value="esp">Español</option>
  </select>
</body>
```

### 44. CSS al Body

1. Agregamos fondo con una imagen, dos fondos segun el tamaño, margen a los lados.

```scss
a { /* quitamos los estilos a los enlaces solo cuando pasemos el mouse encima */
  color: $gris-medio;
  text-decoration: none;
  &:hover{
    text-decoration: underline;
  }
}

body {
  /* cuando sea movil el fondo es negro*/
  background-color: $negro;
  font-family: $font-main;
  color: $gris-medio;

  @include tablet() { /* cuando sea tablet o mayor se pone imagen de fondo de imagen */
    background-color: unset;
    background-image: url(../img/bg.jpg);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: bottom center;
  }
}

.contenedor { /* dejamos un margen a los lados*/
  width: 90%;
  margin: 0 auto;
}
```

### 45. CSS al Logo

```scss
.logotipo {
  margin-top: .5rem;
  max-width: 100px;

  @include tablet() {    margin-top: .8rem;
    margin-left: 2%;
  }
}
```

### 47. CSS a los inputs

```scss
form {
  legend {
    color: $blanco;
    font-size: 2rem;
    margin: 2rem 0;
    font-weight: bold;
  }

  .campo {
    display: flex;
    position: relative; /* damos efecto como si el label fuera placeholder*/
    margin: 0.8rem;
    label {
      position: absolute; /* damos efecto como si el label fuera placeholder*/
    }
    input {
      flex: 1; /* que cresca el input al ancho del contenedor */
      padding: 1.3rem 1rem .7rem 1rem;      border: none;
      background-color: $gris-claro;
      border-radius: 5px;
         &:focus {
        background-color: $gris-oscuro;
        color: $blanco;
        outline: none; /* quitar linea azul que aparece al seleccionar input*/
      }
    }
  }
}
```

### 46. Primeros pasos con el formulario

```scss
label {
  transition: font-size .2s ease, top .2s ease; /* animacion del "placeholder" */
}

input {
  &:focus {
    + label { /* efecto de que sube y se hace pequeño al seleccionarlo */
      font-size: .6rem;
      top: .5rem;
    }
  }
}
```

### 49. Creando el botón de Iniciar Sesión

mixin para boton

```scss
@mixin boton($bg-color: #4171f3, $txt-color: white) {
  background-color: $bg-color;
  color: $txt-color;
  display: block;
  width: 100%;
  text-align: center;
  text-decoration: none;
  border: none;
  border-radius: $radius;
  height: 2.5rem;

  &:hover {
    cursor: pointer;
  }
}
```

lo aplicamos

```scss
form {
...
 .submit {
    display: flex;
    margin-top: 2rem;
    input[type="submit"]{
      @include boton($primario, $blanco);
    }
  }
}
```

### 50. Finalizando el formulario

```scss
.enlaces-formulario {
  padding-bottom: 2rem;
  a {
    font-size: .8;
    display: flex;
  }
  .nuevo-usuario {
    font-size: 1rem;
    margin-top: 1rem;
    span { color: $blanco; }
  }
}
```

### 51. CSS al Footer

```scss
.site-footer {
  padding-top: 1rem;
  border-top: 1px solid $gris-medio;
  padding-bottom: 7rem;
  @include tablet() {
    border-top: unset;
    background-color: rgba(0,0,0,0.85); /* transparencia */
    padding: 1rem 0 3rem 0;
  }
  .contenedor{    @include tablet() {
      max-width: 60%;
    }  }
  .menu-footer {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    a {
      flex: 0 0 auto;
      font-size: .8rem;
      margin-right: 2rem;
      margin-bottom: 1rem;
         &:last-of-type { /* el ultimo enlace no requiere margen a la derecha, seleccionar ultimo elemento*/
        margin-right: 0;
      }
    }
  }
  .language {
    appearance: none; /* para a aplicar estilos a select debemos de poner este para que se apliquen, no se aplican cambios a select almenos que pongamos esto*/    padding: 1rem 2rem;
    border-radius: .8rem;
    background-color: black;
    color:$gris-medio;
  }
}
```

Resultado

![35_select_con_estilos](./img/35_select_con_estilos.PNG)

### RESULTADO PROYECTO

![36_login_netflix_resultado](./img/36_login_netflix_resultado.gif)

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

<!--
.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

## Sección 7: PROYECTO: Diseño de un Blog

html inicial

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cards Flexbox</title>
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.0/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <header class="header hero">
        <div class="barra">
            <div class="contenedor">
                <div class="logo">
                    <img src="img/logo.png" class="logo-img" />
                </div>
                <div class="navegacion">
                    <ul>
                        <li><a href="#">Inicio</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Nosotros</a></li>
                        <li><a href="#">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- Barra -->
        <div class="header-contenido">
            <div class="contenedor">
                <h1>Bienvenido al Blog de viajes</h1>
            </div>
        </div>
    </header>

    <div class="contenedor">
            <main class="contenido-principal">
                <h2>Últimas Entradas</h2>
                <article class="entrada">
                    <img src="img/01.jpg" alt="texto entrada">
                    <div class="contenido">
                        <h3>Tips para viajar con poco presupuesto</h3>
                        <p>Publicado el: <span>19 Julio, 2018</span></p>
                        <p>Por: <span>Blog Viajes</span></p>
                        <a href="#" class="boton"> Leer más</a>
                    </div>
                </article>
                <article class="entrada">
                    <img src="img/02.jpg" alt="texto entrada">
                    <div class="contenido">
                        <h3>Todo lo necesario para tu viaje</h3>
                        <p>Publicado el: <span>19 Julio, 2018</span></p>
                        <p>Por: <span>Blog Viajes</span></p>
                        <a href="#" class="boton"> Leer más</a>
                    </div>
                </article>
                <article class="entrada">
                    <img src="img/03.jpg" alt="texto entrada">
                    <div class="contenido">
                        <h3>El equipo necesario para tomar las mejores fotografías</h3>
                        <p>Publicado el: <span>19 Julio, 2018</span></p>
                        <p>Por: <span>Blog Viajes</span></p>
                        <a href="#" class="boton"> Leer más</a>
                    </div>
                </article>
                <article class="entrada">
                    <img src="img/04.jpg" alt="texto entrada">
                    <div class="contenido">
                        <h3>Checklist para tu próximo viaje</h3>
                        <p>Publicado el: <span>19 Julio, 2018</span></p>
                        <p>Por: <span>Blog Viajes</span></p>
                        <a href="#" class="boton"> Leer más</a>
                    </div>
                </article>
                <article class="entrada">
                    <img src="img/05.jpg" alt="texto entrada">
                    <div class="contenido">
                        <h3>Los mejores lugares para visitar este otoño</h3>
                        <p>Publicado el: <span>19 Julio, 2018</span></p>
                        <p>Por: <span>Blog Viajes</span></p>
                        <a href="#" class="boton"> Leer más</a>
                    </div>
                </article>
                <article class="entrada">
                    <img src="img/06.jpg" alt="texto entrada">
                    <div class="contenido">
                        <h3>Los mejores lugares para visitar con montañas</h3>
                        <p>Publicado el: <span>19 Julio, 2018</span></p>
                        <p>Por: <span>Blog Viajes</span></p>
                        <a href="#" class="boton"> Leer más</a>
                    </div>
                </article>
            </main>
    </div><!--.contenido-->
</body>
</html>
```

<!--  cubrir pantalla con imagen, cubrir pantalla con fondo, fondo cubra toda la pantalla, abarcar pantalla con imagen, abarcar pantalla con fondo, fondo abarcar toda la pantalla, abarcar toda la pantalla con imagen, abarcar toda la pantalla con fondo, fondo que abarcque toda la pantalla, cubrir todo el alto de la pantalla, tomar todo el alto de la pantalla, asignar todo el alto de la pantalla, cubrir toda la pantalla -->
- Crear fondo con imagen que cubra toda la pantalla
- centrar elemento de forma vertical en un column

```css
.hero {
    background-image: url(../img/hero.jpg);
+   height: 100vh;    /* VIEW*HIGHT en este caso todo los disponible en la pantalla, llenar la pantalla ###### */
    background-repeat: no-repeat;
    background-position: center center;
    background-size: cover; /* que tome todo el espacio disponible */
    background-attachment: fixed; /* fijamos la imagen de fondo para que al hacer scroll esta no se mueva*/
}

.header {
    display: flex;
    flex-direction: column;
    position: relative;
    /*align-items: center; cuando es column solo centra horizintalmente, queremos centrar de forma vertical igual */
    justify-content: center;

}

.barra {
    position: absolute;
    background-color: #008fd1;
    width: 100%;
    top: 1rem;
    padding: 1rem 0; /* arriba-abajo izq-der*/
}
```

Resultado

![37_blog_inicio](./img/37_blog_inicio.gif)

### 57. Estilos al Header

```css
@media screen and (min-width: 768px) {
  .barra .contenedor { /* cuando sea escritorio */
    flex-direction: row; /*  mostrar en horizontal */
    justify-content: space-between; /*  pasamos el menu de navegacion a la der */
  }
}
.logo, .navegacion { text-align: center; }

.navegacion ul {
  list-style: none; /* quitar puntos a lista, quitar viñetas a lista, quitar estilos lista, remover estilos lista */
  margin-block-start: 0;
  margin-block-end: 0;
  padding-inline-start: 0;
   display: flex; /* acomodar lista de forma horizontal*/  flex-direction: column; /* mostramos VERTICAL los enlaces de navegacopn */
}

.navegacion ul li a {
  color: white;
  text-decoration: none;
  text-transform: uppercase; /* pasar a mayusculas, concertir a mayusculas */
}
.navegacion ul li a:hover { text-decoration: underline; }

@media screen and (min-width: 768px) {
  .navegacion {
    flex: 0 0 25rem; /* separamos los enlaces de navegacio */
    margin-right: 2rem;
  }
  .navegacion ul {
    flex-direction: row;  /* mostramos HORIZONTAL los enlaces de navegacopn */
    justify-content: space-between; /* separamos los enlaces de navegacio */
  }}

.header-contenido h1 {
  color: #008fd1;
  font-size: 2.4rem;
  text-transform: uppercase;
  max-width: 20rem;
}

.logo-img { max-width: 20rem; }
```

![38_estilos_headers](./img/38_estilos_headers.gif)

### 59. Creando la sección de categorias

agregar font-awesome <!-- importar font-awesome, usar font-awesome añadir font-awesome, agregar fontawesome, usar fontawesome añadir fontawesome, importar fontawesome -->

```html
-proyecto/
--css/
--img/
--webfonts/

<link href="css/all.css" rel="stylesheet">
```

agregar html de categorias

```html
...</main><div class="categorias"><ul>
  <li>
    <a href=""><i class="fas fa-map-pin"></i>Actividades</a>
  </li>
    <li><a href=""><i class="fas fa-music"></i>Festivales</a>
  </li>
    <li><a href=""><i class="fas fa-utensils"></i>Restaurantes</a>
  </li>
    <li><a href=""><i class="fas fa-check"></i>Tips</a>
  </li>
</ul></div>
```

css

```css
.categorias ul {
    display: flex;
    flex-wrap: wrap;
    padding: 0;/* por defecto las listas tienen un padding izquierdo */
    list-style: none;
}

.categorias ul li {
    flex: 1 0 calc(50% - 1rem);
    /* aqui tenemos el problema de como tenemos 50%-1rem y abajo    un padding de 2rem, hay 1rem de sobra lo que hace que tenga    un scroll horizontal, por lo que ponemos html{box-sizing:border-box;} *, *:after, *:before { box-sizing: inherit; }*/
    padding: 1rem;           }

@media screen and (min-width:768px) {
    .categorias ul li {
        flex: 1; /* en escritorio mostramos en solo una fila */
    }
}

.categorias ul li:nth-child(1) {  background-color: #008fd1; }
.categorias ul li:nth-child(2) {  background-color: #db008d; }
.categorias ul li:nth-child(3) {  background-color: #9800d1; }
.categorias ul li:nth-child(4) {  background-color: #eca300; }

.categorias ul li a {
    color: white;
    display: flex; /* pudimos usar un align pero en este caso usamos flex para poder usar align-items que es mas sensillo */
    flex-direction: column;
    align-items: center;
    text-decoration: none;
}
.categorias ul li a:hover { text-decoration: underline; }

.categorias ul li a i {
    font-size: 3rem; /* agrandar iconos, hacer iconos mas grandes, hacer los iconos mas grandes */
    margin-bottom: 1rem;
}
```

![39_blog_categorias](./img/39_blog_categorias.gif)

### 61. Creando el footer

Agregamos footer html

```html
<footer class="footer">
  <div class="contenedor">
    <div class="caja nosotros">
      <h2>Sobre Nosotros</h2>
      <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam aspernatur, a officiis architecto et asperiores temporibus debitis labore velit aliquam culpa dolore quisquam qui aliquid ipsa. Minima perferendis ut quis.</p>
    </div>
    <div class="caja entradas">
      <h2>Ultimas entradas</h2>
      <ul>
        <li>consectetur adipisicing 1</li><li>conuscsdr adaafqsig 2</li><li>cotetr adi 3</li><li>cons 4</li>
      </ul>
    </div>
    <div class="caja siguemos">
      <h2>Síguenos</h2>
      <nav class="sociales">
        <ul>
          <li><a href="http://facebook.com"><span> Facebook</span></a></li>
          <li><a href="http://twitter.com"><span> Twitter</span></a></li>
          <li><a href="http://youtube.com"><span> YouTube</span></a></li>
          <li><a href="http://instagram.com"><span> Instagram</span></a></li>
          <li><a href="http://pinterest.com"><span> Pinterest</span></a></li>
        </ul>
      </nav>
    </div>
  </div>
  <p class="copyright">Blog de viajes, Flexbox 2019 &copy;</p>
</footer>
</body>
```

agregar estilos <!-- barra de redes sociales, barra redes sociales barra, botones de redes sociales, botones redes sociales botones, enlaces de redes sociales, enlaces redes sociales enlaces, iconos de redes sociales, iconos redes sociales iconos, barra iconos de redes sociales, barra iconos redes sociales barra iconos, barra de iconos de redes sociales, barra de iconos redes sociales barra iconos, -->

```css
.footer {
    background-color: #008fd1;
    padding-top: 1rem;
    color: #f2f2f2;
}

.footer h2 { text-transform: uppercase; }

@media screen and (min-width:768px) {
    .footer .contenedor { display: flex; }
    .footer .caja { flex: 1; }
}

.copyright {
    background-color: #004d70;
    margin: 0;
    padding: 1rem;
    text-align: center;
}

/* Redes Sociales */
.sociales ul li span{ display: none; }
.sociales ul {
    list-style: none;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    padding: 0;
}
@media screen and (min-width:768px) {
    .sociales ul { justify-content: flex-end; }
}
.sociales ul li { margin-right: .5rem; /* SEPARACION ENTRE ICONOS */ }
.sociales ul li a { text-decoration: none; }
.sociales ul li a:before {
    font-family: "Font Awesome 5 Brands";
    color: white;  /* COLOR DE LOS ICONOS */
    font-size: 2rem; /* TAMAÑO DE LOS ICONOS */
}
.sociales ul li a[href*='facebook.com']::before  { content: "\f39e"; }
.sociales ul li a[href*='twitter.com']::before   { content: "\f099"; }
.sociales ul li a[href*='youtube.com']::before   { content: "\f431"; }
.sociales ul li a[href*='instagram.com']::before { content: "\f16d"; }
.sociales ul li a[href*='pinterest.com']::before { content: "\f231 "; }
/* Fin Redes sociales*/
```

Resultado

![39_blog_footer](./img/39_blog_footer.gif)

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

## Sección 8: PROYECTO: Holy Grail Layout o Layout de 1 o 2 columnas con contenido al centro

html

```html
<body>
<header class="encabezado">
  <img class="logo" src="./img/logo.jpg" >
</header>
<nav class="navegacion">
  <ul><li><a href="#">Inicio</a></li> <li><a href="#">Nosotros</a></li> <li><a href="#">Blog</a></li> <li><a href="#">Contacto</a></li></ul>
</nav>
<section class="contentenido">
  <main class="contentenido-principal">
    <h1>Tutilo de la entrada</h1>
    <img class="imagen-destacada" src="img/imagen.jpg" />
    <p>Lorem</p>
    <p>Lorem</p>
    <p>nemo</p>
  </main>
  <aside class="sidebar">
    <h2>Ultimas entradas</h2>
    <ul>
      <li>commodi voluptatumcommodi voluptatumcommodi voluptatum</li>
      <li>dicta qui, nam commodi voluptatum labore</li>
      <li>ptatum labore. Culpa quo iust</li>
      <li> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus officia sint</li>
    </ul>
  </aside>
  <aside class="sidebar derecha">
    <h2>Derecha</h2>
    <ul><li>commodi</li><li>dicta</li><li>ptatum</li><li>Lorem</li></ul>
  </aside>
</section>
<footer class="footer">
    <p>Blog de viajes con flexbox 2018</p>
</footer>
</body>
```

- Implementamos CSS
  - hacer que al agregar padding no afecte el ancho de los elementos  - hacer las imagenes responsivas  - hecer menu responsivo
<!-- para evitar problemas con el padding, para evitar problemas con el margin, para evitar problemas con padding, para evitar problemas con margin, evitar desbordamientos, evitar que afecte el padding, evitar desvordamientos, -->

```css
html { box-sizing: border-box;   }
*, *:before, *:after { box-sizing: inherit; }

body { font-family: Arial, Helvetica, sans-serif;  display: flex; flex-direction: column; /* se vera igual pero ahora disponemos las propiedades de flex*/ }

img { max-width: 100%; }

.encabezado { padding: 2rem 1rem; display: flex; justify-content: center; }

.navegacion { background-color: #05bae6; }

.navegacion ul { max-width: 800px; margin: 0 auto; display: flex; flex-direction: column; justify-content: space-around; list-style: none; padding: 0; }

.navegacion ul li { text-align: center; padding: 1rem 0rem; }

.navegacion ul li a { color: #ffffff; text-decoration: none; }

@media screen and (min-width:768px) { .navegacion ul { flex-direction: row; } }
```

Resultado

![40_holy_grail](./img/40_holy_grail.PNG)

### 65. Agregando los Sidebars

```css.sidebar { background-color: #e5e5e5; padding: 2rem; }

.contentenido-principal { padding: 1rem; margin: 0 auto; }

@media screen and (min-width:768px) { /* para escritorio */
    .contentenido {
        display: flex; /* ponemos ultimas-entradas de lado derecho */
    }
    .sidebar {
        flex: 0 0 30%; /* epartimos espacio ultimas-entradas y main-content*/
        order: -1; /* re-acomodamos el sidebar al lado izq  */
    }
    .derecha {
        order: 2;
        flex: 0 0 5%;
    }
}
.footer {
    background-color: #db008d;
    color: white;
    text-align: center;
}
```

Resultado

![41_holy_grail_resultado](./img/41_holy_grail_resultado.gif)

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

## Sección 9: Otros diseños en Flexbox

### 66. Creando cajas de comentarios

estructura/contenido

```html
<body>
  <section class="cometarios">
    <h1>Comentarios</h1>
    <article class="comentario">
      <img src="./img/imagen.jpg" />
      <p>Lorem ipsum, dolor sit amet consectetur adip</p>
      <div class="sub-comentarios">
          <article class="comentario sub-comentario">
            <img src="./img/logo.jpg" />
            <p>Lorem ipsum, dolor sit amet cons</p>
          </article>
          <article class="comentario sub-comentario">
            <img src="./img/logo.jpg" />
            <p>praesentium placeat, vero sed!</p>
          </article>
      </div>
    </article>
    <article class="comentario">
      <img src="./img/logo.jpg" />
      <p>Lorem ipsum, dolor sit amet  magni </p>
      <div class="sub-comentarios">
        <article class="comentario sub-comentario">
          <img src="./img/logo.jpg" />
            <p>Lorem ipsum, dolor sit amet consect</p>
        </article>
      </div>
    </article>
  </section>
</body>
```

estilos

```css
html { box-sizing: border-box; }
*, *:before, *:after { box-sizing: inherit; }

body { background: linear-gradient(to right, #759ccf 0%,  #75cf98 100%); }

.comentario { padding: 1rem; max-width: 700px; display: flex; flex-wrap: wrap; margin-bottom: 10px; }

.comentario img { flex: 1; max-height: 50px; max-width: 50px; }

.comentario p { flex: 1; margin: 0; padding: 0 1rem;    }

.sub-comentarios { margin-top: 10px; margin-left: 2rem; }

```

Resultado

![42_comentarios](./img/42_comentarios.PNG)

### 67. Diseño con Footer y header siempre arriba y fijo abajo aunque poco contenido

<!-- mantener header arriba, mantener header fijo arriba, mantener header en la parte superior, mantener header fijo en la parte superior tener header siempre arriba, tener header siempre fijo arriba,, tener header siempre en la parte superior, tener header siempre fijo en la parte superior hacer header que siempre este arriba, hacer header que siempre este fijo arriba, hacer header que siempre este en la parte superior, hacer header que siempre este fijo en la parte superiortener header arriba siemrpe, poner header siempre arriba, poner header arriba siempre, pisicionar header siempre arriba, pisicionar header arriba siempre
-->
Como hacer el header fijo incluso con scroll: [Docs](https://www.w3schools.com/howto/howto_js_sticky_header.asp)

```html
<body class="container-site">
    <div class=".contenido-superior"><p>Scroll down to see the sticky effect.</p></div>
    <header class="header-site" id="myHeader"><h1>Nombre del sitio</h1></header>
    <main class="main-site">
        <h2 >Contenido Principal</h2>
        <p>Lorem ipsum </p>
    </main>
    <footer class="footer-site">Footer del sitio</footer>
</body>
<script src="./js/index.js"></script>
```

javascript: Le agregara clase que lo mantendra arriba, cuando detecteque el contenido superior se oculte.

```js
window.onscroll = function() {myFunction()};
var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
```

css

```css
html { box-sizing: border-box; }
*, *:before, *:after { box-sizing: inherit; margin: 0; padding: 0; }
p { margin-bottom: 15px; }
body { background: linear-gradient(to right, #759ccf 0%,  #75cf98 100%); }
h2 { text-align: center; }

.contenido-superior {
    background-color: #f1f1f1;
    padding: 30px;
    text-align: center;
}

.header-site {    padding: 10px 16px; background: #555; color: #f1f1f1;
}

.sticky { /* clase que agregara js para mantener el header arriba*/
    position: fixed;
    top: 0;
    width: 100%;
}
.sticky + .main-site {
    padding-top: 60px; /* calcular este bien si no el cambio de clase en js se notara*/
}

/* mantener el footer abajo con flex*/.container-site {  /* container-site = <body> */    display: flex;    flex-direction: column; /* displemos de cosas de flex sin cambiar estrucura de columnas*/    min-height: 100vh; /* le damos una altura igual a la de la pantalla */}

.main-site {    margin: 0 auto; max-width: 700px; width: 80%; padding: 1rem; background-color: #b2dfbd;
    flex: 1; /* hacer que cresca verticalmente y asi mandar el footer abajo */}

.footer-site {
    padding: 1rem 1rem;
    font-weight: bold;
    text-align: center;
    color: white;
    background-color: #212121;
    /* mantener footer siemre abajo lo quitamos ya que roba espacio
    position: fixed; width: 100%; bottom: 0; */
}
```

**Resultado** Podemos ver como el header a pesar de hacer scroll hacia abajo este siempre
se mantiene arriba y el footer independientemente del contenido que ahiga abajo este siempre
aparece hasta abajo independientemente de la alturade nuestro navegador.

![43_header_siempre_arriba_footer_aunque_poco_contenido_aparesca_abajo_solo_al_inicio](./img/43_header_siempre_arriba_footer_aunque_poco_contenido_aparesca_abajo_solo_al_inicio.gif)

### 68. Diseño con elementos en distinta alineación

Pendiente...

![44_centrar_vertical_horizontal_hamborgesa](./img/44_centrar_vertical_horizontal_hamborgesa.PNG)

### 70. Creando un Diseño Masonry en Flexbox

- Debemos de tener una altura fija, si no la tenemos solo se hara una fila vertical.
- Las Cards se ordenaran verticalmente, por ejemplo si tenemos 30 elementos el primer elemento de
la segunda columna sera el 15ª elemento, o si tenemos 3 columnas sera el 10ª elemento.

<!-- cards diseñador visual de cards , cards diseñador grafico de cards, diseñador de cajas, diseñador de cards
crear cards visualmente, crear cards facilmente, crear card visualmente, crear card facilmente, crear cajas visualmente, crear cajas facilmente, crear caja visualmente, crear caja facilmente, crear box visualmente, crear box facilmente, crear boxes visualmente, crear boxes facilmente, hacer cards visualmente, hacer cards facilmente, hacer card visualmente, hacer card facilmente, hacer cajas visualmente, hacer cajas facilmente, hacer caja visualmente, hacer caja facilmente, hacer box visualmente, hacer box facilmente, hacer boxes visualmente, hacer boxes facilmente-->
<!-- deseñador de cards, deseñador de card,  deseñador de cajas,  deseñador de caja,  deseñador de box, deseñador de boxes, -->

x. diseñamos nuestra card en: [cssmatic](https://www.cssmatic.com/box-shadow)

![45_cssmatic_box-shadow](./img/45_cssmatic_box-shadow.PNG)

```html
<body>
<div class="contenedor">
<h1>Nuestro Blog</h1>
<div class="blog">
    <div class="entrada">
      <img src="img/4.jpg">
      <div class="contenido"><h2>Proin nec erat</h2><p>Aliquam erat elit, posuere quis blandit vitae, porta sit amet</p></div>
    </div>
    <div class="entrada">
      <img src="img/2.jpg">
      <div class="contenido"><h2>Curabitur pulvinar viverra </h2> <p>Aliquam </p></div>
    </div>
    <div class="entrada">
      <img src="img/3.jpg">
      <div class="contenido"><h2>molestie eu ut mi.</h2> <p>Aturpis vitae nisi vulputate dignissim</p></div>
    </div>
    <div class="entrada">
      <img src="img/1.jpg">
      <div class="contenido"><h2>Proin nec erat</h2> <p>arcu non convallis sodales</p></div>
    </div>
    <div class="entrada">
      <img src="img/5.jpg">
      <div class="contenido"><h2>molestie eu ut mi.</h2> <p>Aliquam er vulputate dignissim</p></div>
    </div>
    <div class="entrada">
      <img src="img/6.jpg">
      <div class="contenido"><h2>viverra tincidunt.</h2> <p>Aliquam erat elit, posuere quis</p></div>
    </div>
    <div class="entrada">
      <img src="img/7.jpg">
      <div class="contenido"><h2>molestie eu ut mi.</h2> <p>odales, </p></div>
    </div>
    <div class="entrada">
      <img src="img/8.jpg">
      <div class="contenido"><h2>Cureu ut mi.</h2><p>Aliquam erat elit, posuere quis blandit vitae</p></div>
    </div>
    <div class="entrada">
      <img src="img/9.jpg">
      <div class="contenido"><h2>eu ut mi.</h2><p>Aliqrcu non convallis sodales, </p> </div>
    </div>
    <div class="entrada">
      <img src="img/10.jpg">
      <div class="contenido"><h2>Curabitur pulvi</h2><p> tincidunt odio. </p></div>
    </div>
    <div class="entrada">
      <img src="img/11.jpg">
      <div class="contenido"><h2>Curabitur pul</h2><p>Aliquam erat elit sem. In sed turpis vitae nisi </p></div>
    </div>
</div> </div> </body>
```

estilos

```css
html {box-sizing: border-box; }
*, *::after, *::before{box-sizing: inherit}
body{ font-family: 'Roboto', sans-serif; background-color: #f3f3f3; }
h1 { text-align: center; font-size: 2.4rem; font-weight: 300; }
.contenedor { max-width: 1100px; margin: 0 auto;
    padding-left: 1rem; }

img {    max-width: 100%; /* hacer la imagenes responsive */
}

.blog {
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    max-height: 10700px; /* (altura para movil) requisito, debemos de calcular a ojo de buen cubero */
}

.entrada {
    width: calc(100% - 1rem); /* UNA FILA */
    -webkit-box-shadow: 13px 18px 12px -5px rgba(145,145,145,1);
    -moz-box-shadow: 13px 18px 12px -5px rgba(145,145,145,1);
    box-shadow: 13px 18px 12px -5px rgba(145,145,145,1);
    margin-bottom: 1rem;
}

@media screen and (min-width: 490px) {
    .blog {
        max-height: 5500px; /* (altura para tablet) */
    }
    .entrada { width: calc(50% - 1rem); } /* DOS FILAS */
}

@media screen and (min-width: 768px) {
    .blog {
        max-height: 3000px; /* (altura para escritorio) */
    }
    .entrada { width: calc(33% - 1rem); } /* TRES FILAS */
}

.contenido {
    padding: 1rem; /* si no tenemos el box-sizing: border-box; se chocaleatara el diseño*/
}
```

**Resultado:**

![45_masonry](./img/45_masonry.gif)

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

## Sección 10: PROYECTO: AdminFlex o Panel de Administración en Flexbox

<!-- file://E:\cursos\0activ\web\flexbox\adminFlex -->

1, descargar dependencias node, descargar librrias package.json: `npm install`

```json
"devDependencies": {
  "gulp": "^4.0.2",
  "gulp-autoprefixer": "^7.0.1",
  "gulp-sass": "^4.0.2"
},
```

- Hacemos que el body y .pagina tome todo el alto disponible

```html
<!DOCTYPE html><html lang="en">
<head>
  <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.0/normalize.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
  <link rel="stylesheet" href="css/fontawesome-all.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="pagina">
    <header class="header">
      <div class="nombre-sitio"></div>
      <h1 class="escritorio">Admin Flex</h1>
      <h1 class="mobil">AF</h1>
    </header>
    <main class="contenedor-principal">
    </main>
  </div>
<script src="js/app.js" defer></script>
</body>
</html>
```

estilos

```css
.pagina {
  height: 100%;
  display: flex;
  flex-direction: column;
}

.contenedor-principal {
  flex: 1;
  display: flex;
}
```

![46_inicio_flexadmin](./img/46_inicio_flexadmin.PNG)

### 75. Estilos al Nombre del sitio

- ocultar o mostrar con css el titulo

<!-- css ocultar o mostrar segun el tamaño de la pantalla, css ocultar o mostrar elemento segun el tamaño de la pantalla-->

```scss
.header {
  background-color: #b2b2b2;
  display: flex;
  .nombre-sitio {
    flex: 0 0 5rem;
    background-color: $secundario;
    h1 { color: white; }
    .escritorio {
      display: none; /* para movil ocultamos el titulo de escritori */
      @include desktop { display: inline; } /* para escritorio mostramos el titulo de escritori */
    }
    .movil { @include desktop { display: none; } } /* para escritorio escondemos el titulo de mobil */
    @include desktop { flex: 0 0 20rem; }
  }
}
```

![47_ocultar_mostrar_segun_pantalla_con_css](./img/47_ocultar_mostrar_segun_pantalla_con_css.gif)

### 77. Estilos a los menús

- Separamos y ponemos a los extremos las flecha para desplegar y botones header.
- Centar flechas para desplegar

```html
<header class="header">
  <div class="nombre-sitio">
    <h1 class="escritorio">Admin Flex</h1>
    <h1 class="movil">AF</h1>
  </div>
  <div class="barra">
    <div class="menu-izquierdo">
      <i class="fas fa-arrow-left"></i>
      <i class="fas fa-arrow-right"></i>
    </div>
    <div class="menu-derecho">
        <div class="caja mensaje">
          <a href="#">
            <i class="fas fa-comment-alt"></i>
            Mensajes
            <span>10</span>
          </a>
        </div>
        <div class="caja alertas">
          <a href="#">
            <i class="fas fa-bell"></i>
            Alertas
            <span>9</span>
          </a>
        </div>
        <div class="caja logout">
          <a href="#">
            <i class="fas fa-comment-alt"></i>
            Cerrar Sesion
          </a>
        </div>
    </div>
  </div>
</header>
```

estilos

```scss
.barra {
  /* separamos y ponemos a los extremos las flecha para desplegat y botonesl header */
  display: flex;
  justify-content: space-between; /* alinear a la derecha, alinear derecha, poner a la derecha, se necesitan dos elementos*/
  flex: 1;
  background-color: $primario;
  .menu-izquierdo {
    display: flex;
    align-items: center; /* alineamos verticalmente */
    padding: 1rem;
  }
  .menu-derecho {
    display: flex;
    .caja {
      padding: 0 1rem;
      display: flex;
      align-items: center;
      
      &:hover { background-color: darken($primario, 5); } /* obscurecer */
      a {
        color: $blanco;
        text-decoration: none;
        span {                                /*  cantidad de notificaciones redondos */
          background-color: $terciario;
          border-radius: 50%;
          padding: .5rem;
          margin: ;
        }
      }
    }
  }
}
```

Resultado

![48_botones_barra_header](./img/48_botones_barra_header.PNG)

### 78. Creando la barra lateral

Vista

```html
<main class="contenedor-principal">
  <aside class="aside-bar">
      <div class="usuario">
          <img src="./img/usuario.svg" alt="Imagen de perfil" height="100px">
          <p>Bienvenido: <span>Admin</span></p>
      </div>
      <div class="buscador"><input type="text" placeholder="Buscar..."></div>
      <div class="menu-admin">
          <h2>Menu de Administracion</h2>
          <ul class="menu">
              <li>
                  <a href="#"><i class="fas fa-address-book"></i>Clientes</a>
                  <ul>
                      <li><a href="#"><i class="fa fa-list"></i>Ver Todos</a></li>
                      <li><a href="#"><i class="fa fa-plus"></i>Agregar Nuevo</a></li>
                  </ul>
              </li>
              <li>
                  <a href="#"><i class="fas fa-chart-line"></i>Ventas</a>
                  <ul>
                      <li><a href="#"><i class="fa fa-list"></i>Ver Todos</a></li>
                      <li><a href="#"><i class="fa fa-plus"></i>Agregar Nuevo</a></li>
                  </ul>
              </li>
              <li>
                  <a href="#"><i class="fas fa-box"></i>Inventario</a>
                  <ul>
                      <li><a href="#"><i class="fa fa-list"></i>Ver Todos</a></li>
                      <li><a href="#"><i class="fa fa-plus"></i>Agregar Nuevo</a></li>
                  </ul>
              </li>
              <li>
                  <a href="#"><i class="fas fa-file-alt"></i>Facturas</a>
                  <ul>
                      <li><a href="#"><i class="fa fa-list"></i>Ver Todos</a></li>
                      <li><a href="#"><i class="fa fa-plus"></i>Agregar Nuevo</a></li>
                  </ul>
              </li>
              <li>
                  <a href="#"><i class="fas fa-calendar-alt"></i>Calendario</a>
              </li>
              <li>
                  <a href="#"><i class="fas fa-pencil-alt"></i>Edifar Perfil</a>
              </li>
          </ul> <!-- Fin menu -->
      </div>
  </aside>
```

**Resultado**:

![49_adminflex_menu_navegacion](./img/49_adminflex_menu_navegacion.PNG)

### 79. Estilos al buscador

```scss
.aside-bar {
    flex: 0 0 $aside-bar-width; /* el ancho debe ser el mismo al de la barra header*/    background-color: $grisOscuro;
    padding: 1rem;
    color: $blanco;
    font-weight: normal;
    .usuario {
        display: flex;
        align-items: center;
        justify-content: space-around;
        span {
            display: block; /* el nombre de usuario lo ponemos abajo de bienvenido */
            font-weight: bold;
            text-align: center;
        }
        img { max-width: 5rem; }
    }
    .buscador {
        margin-top: 1rem;
        input[type="text"]{
            width: 100%;
            height: 3rem;
            border: none;
            border-radius: 10px;
            padding-left: 1rem;
        }
    }
}
```

![50_adminflex_info_usuario_buscador](./img/50_adminflex_info_usuario_buscador.PNG)

### 80. Estilos al Menú Lateral

```css
.menu-admin {
  margin-top: 1rem;
  .menu {
    list-style: none;
    padding: 0 0 0 1rem;
    > li {
      padding: .5rem;
      a {        color: $blanco; text-decoration: none;        padding: .5rem;
        transition: padding-left .3s ease;
        &:hover{
          background-color: $primario;
          padding-left: 1rem;
        }
      }
      ul { /* por defecto NO se van a mostrar los sub-menus */
        display: none;        margin-top: 1rem;
        background-color: darken($grisOscuro, 3);
        li { /* le damos separacion para que no parescan ensimados */
          margin-bottom: 1rem;
          &:last-of-type{
            margin-bottom: 0;
          }
        }
      }      &:hover { /* mostrar cuando hover */
        ul { display: block; }
      }
} } }
```

![50_flexadmin_asidebar_menu_lateral](./img/50_flexadmin_asidebar_menu_lateral.gif)

### 81. Ocultando y mostrando el Menú Principal

Agregamos click listener para alternar flecha y poner/quitar una claseque sera la que oculte el sidebar.

```js
const menuIzquierdo = document.querySelector('.menu-izquierdo');

menuIzquierdo.addEventListener('click', (e)  => {
 const claseMenu = e.target.classList;
 const contenedor = document.querySelector('.contenedor-principal'), // Selecciona el contenedor
       flechaIzq = document.querySelector('.fa-arrow-left'),
       flechaDer = document.querySelector('.fa-arrow-right');
   if(claseMenu.contains('fa-arrow-left') ) { // cerrar el menú lateral
    contenedor.classList.add('no-menu');
    e.target.style.display = 'none';
    flechaDer.style.display = 'block';
 } else if( claseMenu.contains('fa-arrow-right')) {
    contenedor.classList.remove('no-menu');
    e.target.style.display = 'none';
    flechaIzq.style.display = 'block';
 }
});
```

estilos

```scss
.no-menu  { .aside-bar { margin-left: -$aside-bar-width; } }

.menu-izquierdo {...
  i { color: $blanco; font-size: 1.3rem; }
  .fa-arrow-right { /* por defecto ocultamos la de derecha que es el que mostrara*/
    display: none;
  }
}

.aside-bar {...
  transition: margin-left .3s ease; /* agregamos animacion al ocultar */}
```

Resultado:

![51_mostrar_ocultar_sidebar_menu_lateral_con_amimacion](./img/51_mostrar_ocultar_sidebar_menu_lateral_con_amimacion.gif)

### 82.83. Estilos al área principal Creando los formularios

agregamos

```css
.contenedor-principal .contenido {
  flex: 1;
  padding: 2rem;
}
```

### 52_flexadmin_formulario_fin

formulario hrml

```html
<form action="" class="formulario">
  <div class="campo">
      <label for="nombre">Nombre Cliente</label>
      <input type="text" id="nombre">
  </div>
  <div class="campo">
      <label for="nombre">Apellido Cliente</label>
      <input type="text" id="nombre">
  </div>
  <div class="campo">
      <label for="nombre">Empresa</label>
      <input type="text" id="nombre">
  </div>
  <div class="campo">
      <label for="nombre">Dirección</label>
      <input type="text" id="nombre">
  </div>
  <div class="campo expandir">
      <label for="nombre">Otra Información</label>
      <textarea name="" id="" cols="30" rows="10"></textarea>
  </div>

  <div class="campo expandir">
          <label>Membresia:</label>
          <div class="opciones">
              <input type="radio" id="normal"
              name="contact" value="normal">
              <label for="normal">Normal</label>
                     <input type="radio" id="basico"
              name="contact" value="basico">
              <label for="basico">Básico</label>
                     <input type="radio" id="premium"
              name="contact" value="premium">
              <label for="premium">Premium</label>

              <input type="radio" id="gold"
              name="contact" value="gold">
              <label for="gold">Gold</label>
          </div>
  </div>
  <div class="campo enviar expandir">
      <input type="submit" class="boton" value="Enviar">
  </div>
</form>
```

estilos css

```css
.formulario {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  .campo {
    display: flex;
    flex: 0 0 100%;
    margin-bottom: 1rem;
    @include tablet { flex: 0 0 calc(50% - 1rem); }
    &.expandir {
      flex: 0 0 100%;
      align-items: flex-start; /* para subir el label del textarea y no quede en medio */
      label { flex: 0 0 12.5%; }
      textarea { flex: 1; }
    }
    label { flex: 0 0 25%; }
    input[type="text"] { flex: 0 0 75%; }
    .opciones {
      display: flex;
      flex: 0 0 calc(50% - 1rem);
      @include tablet { flex: 1; }
      appearance: none; /* quitar estilos select quitar estilos, y*/
      flex-wrap: wrap;
      label { padding-left: 1rem; }
    }
    &.enviar {
      display: flex;
      justify-content: flex-end;
      .boton{ @include boton($primario); }
    }
  }
}
```

Resultado

![52_flexadmin_formulario_fin](./img/52_flexadmin_formulario_fin.gif)

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

## Sección 11: Introducción a CSS Grid

<!-- Introduccion a Grid, introduccion grid-->

### 84. ¿Qué es CSS Grid? y términos básicos

A diferencia de flexbox que es udimencional. Grid es un sistema de 2 dimenciones.
Grid es una nueva forma de realizar layout ofrece un control superior a otros
flameworks como bootstrap u otros.

![53_flexbox_vs_grid](./img/53_flexbox_vs_grid.PNG)

A diferencia de Flexbox que es flexible, Grid es agrupa el contenido
en un area definida.

![54_flexbox_vs_grid_2](./img/54_flexbox_vs_grid_2.PNG)

### 85. El Grid Container

<!-- file://E:\cursos\0activ\web\flexbox\cssgrid-basicos -->

html

```html
<body>
  <div class="servicios">
    <div class="servicio servicio-1">
      <h2>Servicio 1</h2>
    </div>
    <div class="servicio servicio-2">
      <h2>Servicio 2</h2>
    </div>
    <div class="servicio servicio-3">
      <h2>Servicio 3</h2>
    </div>
  </div>
</body>
```

grid asignar ancho con grid, <!-- grid poner ancho con grid, grid repartir espacio horizontalmente con grid -->

```css
.servicios { border: 3px solid black;
  display: grid;
+ grid-template-columns: 33.3% 33.3% 33.3%;
}
```

![55_grid-template-columns](./img/55_grid-template-columns.PNG)

Grid crear columnas con grid, <!-- grid añadir columnas con grid, -->

```css
.servicios { display: grid; grid-template-columns: 33.3% 33.3% 33.3%;
  height: 200px;
+ grid-template-rows: 50% 50%;
}
```

![56_grid-template-rows](./img/56_grid-template-rows.PNG)

Grid asignar celda horizintal con grid o asignar tamaño por celda horizontal <!-- mover elemento entre celda, asignar celda, mover elemento de celda -->

```css
.servicio-1 {      background-color: darkviolet;
  grid-column-start: 4;
  grid-column-end: 5;
}
```

![57_grid-column-start-y-end](./img/57_grid-column-start-y-end.gif)

Grid asignar celda vertical con grid o asignar tamaño por celda vertical.

```css
.servicio-1 {      background-color: darkviolet;
  grid-column-start: 4;
  grid-column-end: 5;
  grid-row-start: 1;
  grid-row-end: 2;
}
```

![58_grid-row-start-y-end](./img/58_grid-row-start-y-end.gif)

### 86. Definiendo columnas y rows

html

```html
<body>
  <div class="servicios">
    <div class="servicio servicio-1"><h2>Servicio 1</h2></div>
    <div class="servicio servicio-2"><h2>Servicio 2</h2></div>
    <div class="servicio servicio-3"><h2>Servicio 3</h2></div>
    <div class="servicio servicio-4"><h2>Servicio 4</h2></div>
  </div>
</body>
```

css

```css
.servicios { border: 3px solid black; height: 200px;
  display: grid;
  grid-template-columns: 50% 50%;/* definimos columanas */ /* Version para movil */
  grid-template-rows: 50% 50%;/* definimos columanas */
}

@media screen and (min-width: 768px) {            /* Version para escritorio */
  .servicios {
    grid-template-columns: 25% 25% 25% 25%;
    grid-template-rows: 33.3% 33.3% 33.3%;
  }
}
.servicio-1 {      background-color: darkviolet;
  grid-column: 1 / 2; /* start / end */
  grid-row: 1 / 3; /* start / end */
}
.servicio-2 { background-color: limegreen; }
.servicio-3 { background-color: teal;      }
.servicio-4 { background-color: chocolate; display: none;  }

@media screen and (min-width: 768px) {
  .servicio-1 {       grid-column: 1 / 2; /* start / end */
    grid-row: 1 / 4; /* start / end */
  }
  .servicio-2 { background-color: limegreen;
    grid-column: 2 / 4; /* start / end */
    grid-row: 1 / 3; /* start / end */
  }
  .servicio-3 { background-color: teal;
    grid-column: 4 / 5; /* start / end */
    grid-row: 1 / 4; /* start / end */
  }
  .servicio-4 { background-color: chocolate;
    display: grid;
    grid-column: 2 / 4; /* start / end */
    grid-row: 3 / 4; /* start / end */
  }
}
```

Resultado

![59_grid-y_meadia-querys](./img/59_grid-y_meadia-querys.gif)

### 87. Colocando elementos dentro de los rows y columns

Ordenar elementos en un roden vertical

Por defecto vamos a tener un orden horizontal como este

![60_orden_por_defecto](./img/60_orden_por_defecto.PNG)

Pero podemos cambiarlo por un orden vertical, el cual usamos la

```css
.servicios {
    display: grid;
    grid-template-columns: 33.3% 33.3% 33.3%;
    grid-template-rows: 50% 50%;
    /* grid-auto-flow: row;  por defecto */
+   grid-auto-flow: column;
}
```

![61_orden_vertical](./img/61_orden_vertical.PNG)

### 88. Grid Shorthand, grid-row, grid-column, grid-area

Podemos reducir la sintaxis de la siguiente manera donde es *rows / columns*

```css
/*grid-template-columns: 33.3% 33.3% 33.3%;grid-template-rows: 50% 50%; */
grid: 33.3% 33.3% 33.3% / 50% 50%; /* ES LO MISMO QUE LO DE ARRIBA */
}
```

Lo mismo podemos hacer para repartir los espacios el orden es
**grid-inicio / columna-inicio / row-end**.

```css
.servicio-1 {
  /*grid-row: 1 / 3; grid-column: 1 / 3; HACEN LO MISMO*/  grid-area: 1 / 1 / 3 / 3;
}
}
```

### 89. Repeat en CSS Grid

Podemos hacer esto loca cual estamos haciendo mucha repeticion

```scss
.servicios { border: 3px solid black; height: 200px;
  display: grid;
  grid: 100%  / 25% 25% 25% 25%;
}
```

Podemos sustituirlo por este ```repeat``` que es mas limpio y da el mismo resultado

```css
.servicios { border: 3px solid black; height: 200px;
  display: grid;
  grid: 100%  / repeat(4, 25%);
}
```

### 90. Grid Template Areas

Podemos tener una template basica con lo siguiente
hrml

```html
<body>
  <div class="contenedor">
    <header class="header"> <p>Nombre del Sitio</p> </header>
    <main class="contenido-principal"> </main>    <aside class="sidebar"> </aside>
    <footer class="footer"> </footer>
  </div>
</body>
```

css

```css
.contenedor { height: 100vh; width: 90%; margin: 0 auto;
    display: grid;
    grid: 100px auto 100px / repeat(4, 25%);
}

.header { background-color: coral; color: white; text-align: center;
    grid-column: 1/ 5;
}

.contenido-principal { background-color: darkred;
    grid-column: 2 / 5;
    grid-row: 2 / 3;
}
.sidebar { background-color: olive;
    grid-column: 1 / 2;
    grid-row: 2 / 3;
}
.footer { background-color: navy;
    grid-column: 1 / 5;
    grid-row: 3 / 4;
}
```

![62_template_1](./img/62_template_1.PNG)

Pero puede dar el mismo resultado con `grid-template-areas`.

Nota: Podemos dejar un espacion en blanco con un punto

```css
.contenedor { height: calc(100vh - 1rem); width: 90%; margin: 0 auto;
  display: grid;
  grid: 100px auto 100px / repeat(4, 25%);
  grid-template-areas:    "header  header    header    header"
    "sidebar principal principal principal"
    "  .     footer    footer    footer"
  ;
}

.header { margin-bottom: 1rem; background-color: coral; color: white; text-align: center;
  grid-area: header; }

.contenido-principal { background-color: darkred;
  grid-area: principal;
}
.sidebar { margin-right: 1rem; background-color: olive;
    grid-area: sidebar;
}
.footer { margin-top: 1rem; background-color: navy;
  grid-area: footer;
}
```

![62_template_2](./img/62_template_2.PNG)

### 91. Grid Template Areas en práctica

Podemos igual crear con **grid-template-areas** cards las
cuales eran mas faciles de crear.

```html
<body>
  <div class="contenedor-media">
    <h1 class="titulo">Nombre del Automovil</h1>
    <img src="../img/01.jpg" class="imagen">
    <p class="descripcion"> Vestibulum sodal sodal sodal sodal</p>
  </div>
</body>
```

estilos

```css
.contenedor-media { margin: 0 auto; margin-bottom: 1rem; max-width: 600px; border: 1px solid black;
    display: grid;
    grid-template-areas:
        "titulo titulo  titulo  titulo"
        "imagen descrip descrip descrip"
    ;
}
.titulo      { grid-area: titulo; }
.imagen      { grid-area: imagen; }
.descripcion { grid-area: descrip; }
```

![62_template_3_carro](./img/62_template_3_carro.PNG)

### 92. Modificar el orden de los elementos según la resolución con Grid

html

```html
<body>
  <div class="contenedor-media">
    <h1 class="titulo">Nombre del Automovil</h1>
     <img src="../img/01.jpg" class="imagen">
     <div class="info"><p>Fecha: 20 - Diciembre - 2018</p></div>

    <blockquote class="slogan">"Autos Increibles para el 2018"</blockquote>

    <div class="entrada">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
  </div>
</body>
```

estilos

```css
.contenedor-media { margin: 0 auto; margin-bottom: 1rem; max-width: 600px; border: 1px solid black;
    display: grid;
    grid-template-areas: /* Version para movil */
        "titulo"
        "slogan"
        "info"
        "imagen"
        "entrada"
    ;
}

@media screen and (min-width: 468px) {  /* Version para escritorio */
    .contenedor-media {
        grid-template-areas:
            "titulo titulo"
            "entrada entrada"
            "imagen slogan"
            "imagen info"
        ;
    }
}

.titulo { grid-area: titulo; }
.slogan { grid-area: slogan; }
.info   { grid-area: info; }
.entrada { grid-area: imagen; }
.entrada { grid-area: entrada; }
```

**Resultado:** Si observamos en el html y css el orden de loselementos esta diferente, por ejemplo el "slogan" lo tenemos comocuarto elemento del html, pero se muestra como el segundo, eso espor que el el *grid-template-areas* lo definimos asi.

![62_template_4](./img/62_template_4.gif)

### 93. Grid FR, Repeat y otras opciones disponibles

<!-- uso de fr, uso de fraccciones, uso de gragmentos-->
Podemos usar de fracciones los cuales nos facilitaran las cosas

```css
grid-template-columns: 33.3% 33.3% 33.3%;
grid: 100px auto 100px / repeat(4, 25%);
/* IGUAL A */
grid-template-columns: 1fr 1fr 1fr;
grid: 100px auto 100px / repeat(4, 1fr);
```

### 94. Pendiente

![63_template_experimental](./img/63_template_experimental.PNG)

### 96. Grid Auto-fit y Auto-fill

**auto-fill** Si queremos evitar nos el detalle de los media queries en un diseño podemos
usar estos, los cuales haran de forma atm el tema responsivo

```css
.servicios { border: 3px solid black; height: 300px;
    display:grid;
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
}
```

![64_auto-fill](./img/64_auto-fill.gif)

**auto-fit** Si queremos evitar los espacios en blanco usamos, pondra todos
los elementos que quepan mas el espacio requerido para llenar todo el espacio, y
en caso contrario al llegar al minimo reacomodara los elementos.

```css
.servicios { border: 3px solid black; height: 300px;
    display:grid;
    grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
}
```

![65_auto-fit](./img/65_auto-fit.gif)

## Sección 12: PROYECTO: Blog de Recetas en CSS Grid

<!-- E:\cursos\0activ\web\flexbox\receta-inicio -->

```html
<body>
  <header class="header contenedor"> <img src="img/logo.jpg" class="logo"> </header>

  <div class="navegacion">
    <nav class="contenedor">      <a href="#">Recetas</a> <a href="#">Productos</a> <a href="#">Recomendaciones</a> <a href="#">Videos</a>
    </nav>
  </div>

  <div class="slider contenedor"> <img src="img/platillo.jpg"> </div>

  <main class="contenedor contenido-principal">
    <h1 class="titulo">La mejor receta para hacer pizza en tu hogar</h1>
    <div class="entrada">
      <p>Sed neque sapien, rutrum vel elit at, consequat maximus libero</p>
      <p>Nulla facilisi. Nullam sed leo leo. Proin ipsum lacus</p>
      <blockquote>Orci varius natoque penatibus et </blockquote>
      <p>Orci varius natoque</p>
    </div>

    <div class="informacion">
      <p>30 Minutos</p> <p>4 Personas</p> <p>$200</p>
    </div>
    <div class="imagen1"> <img src="img/mini1.jpg"> </div>
    <div class="imagen2"> <img src="img/mini2.jpg"> </div>
    </div>
  </main>
  <footer class="footer">
    <div class="contenedor">
      <div class="widget">
        <h3>Recetas</h3>
        <nav class="menu">
          <a href="#">Comida mexicana</a> <a href="#">Pizzas</a> <a href="#">Postres</a> <a href="#">Comida Asiatica</a> <a href="#">Comida Rápida</a>
        </nav>
      </div>
      <div class="widget">
        <h3>Recetas</h3>
        <nav class="menu">
          <a href="#">Comida mexicana</a> <a href="#">Pizzas</a> <a href="#">Postres</a> <a href="#">Comida Asiatica</a> <a href="#">Comida Rápida</a>
        </nav>
      </div>
      <div class="widget">
        <h3>Recetas</h3>
        <nav class="menu">
          <a href="#">Comida mexicana</a> <a href="#">Pizzas</a> <a href="#">Postres</a> <a href="#">Comida Asiatica</a> <a href="#">Comida Rápida</a>
        </nav>
      </div>
      <div class="widget">
        <h3>Recetas</h3>
        <nav class="menu">
          <a href="#">Comida mexicana</a> <a href="#">Pizzas</a> <a href="#">Postres</a> <a href="#">Comida Asiatica</a> <a href="#">Comida Rápida</a>
        </nav>
      </div>
    </div>
    <p class="copyright">2018 Receta Fácil</p>
  </footer>
</body>
```

estilos de grid

```css
/** Grid aqui**/
.navegacion nav {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
}

@media screen and (min-width: 768px) {
  .contenido-principal {
    display: grid;
    grid-template-columns: minmax(100px, max-content) auto minmax(
        100px,
        max-content
      );
    grid-template-rows: repeat(4, auto);
    grid-gap: 1rem;
  }
  .titulo {      grid-column: 1 / 4; /* el titulo cubrira toda una fila */ }
  .informacion { grid-row: 2 / 3;  }
  .imagen1 {     grid-row: 3 / 4; grid-column: 3 / 4; }
  .imagen2 {     grid-row: 4 / 5; grid-column: 1 / 2; }
  .entrada {     grid-row: 2 / 5; grid-column: 2 / 3; }
}
```

![66_recetas_contenido](./img/66_recetas_contenido.PNG)

### 98. Finalizando el Proyecto auto-fit

usaremos el auto-fit

```css
.footer .contenedor {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
}
```

![67_recetas_footer](./img/67_recetas_footer.gif)

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

## Sección 13: PROYECTO: Sitio Web de Bienes Raices con CSS Grid
<!-- file://E:\cursos\0activ\web\flexbox\bienes -->

html del header

```html
<header class="header">
  <div class="barra contenedor">
    <div class="nombre-sitio">
      <img src="./img/logo.png" class="logo">
    </div>
    <nav class="menu">
      <a href="#">Nosotros</a>
      <a href="#">Casas</a>
      <a href="#">Contacto</a>
    </nav>
  </div>
</header>
```

### 100. Creando y utilizando variables CSS

Definir variables en css, definir variables sin usar sass, variables en css variables

```css
:root {
  --primario: #7abe47;
  --primario-dark: #57a42b;
  --secundario: #ffc845;
  --negro: #000000;
  --claro: #ffffff;
  --gris: #dddddd;
  --padding: 1rem;
  --padding2: 2rem;
}
```

estilos del header

```css
.header {
  margin-top: 2rem;
  background-color: var(--primario);
  padding: var(--padding);
}
.header .barra {
  display: grid;
  grid-template-rows: repeat(2, auto);
  grid-row-gap: 2rem;
  justify-content: center;
}
.header .menu {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
}
.header .menu a {
  text-align: center;
  color: var(--claro);
  text-decoration: none;
  font-size: 1.4rem;
}
@media (min-width: 992px) {
  .header .barra {
    grid-template-rows: unset;
    grid-template-columns: 20% 1fr 40%;
    justify-content: space-around;
  }
  .header .menu {
    grid-column: 3 / 4;
    display: grid;    align-content: center; /* centramos verticalmente el menu*/
  }
}
```

![68_bienes_r_header](./img/68_bienes_r_header.gif)

### 102. Agregando Fuentes de Google Fonts

<!-- codigo normailize codigo, codigo normalizar codigo, codigo para normalizar estilos, quitar estilos por defecto,estilos para normalizar estilos, css para normalizar, css normalizacion, -->

1, normalizar los estilos

```css
html { line-height: 1.15; /* 1 */ -webkit-text-size-adjust: 100%; /* 2 */ }
body { margin: 0; }
main { display: block; }
h1 { font-size: 2em; margin: 0.67em 0; }
hr { box-sizing: content-box; /* 1 */ height: 0; /* 1 */ overflow: visible; /* 2 */ }
pre { font-family: monospace, monospace; /* 1 */ font-size: 1em; /* 2 */ }
a { background-color: transparent; }
abbr[title] { border-bottom: none; /* 1 */ text-decoration: underline; /* 2 */ text-decoration: underline dotted; /* 2 */ }
b, strong { font-weight: bolder; }
code, kbd, samp { font-family: monospace, monospace; /* 1 */ font-size: 1em; /* 2 */ }
small { font-size: 80%; }
sub, sup { font-size: 75%; line-height: 0; position: relative; vertical-align: baseline; }
sub { bottom: -0.25em; }
sup { top: -0.5em; }
img { border-style: none; }
button, input, optgroup, select, textarea { font-family: inherit; /* 1 */ font-size: 100%; /* 1 */ line-height: 1.15; /* 1 */ margin: 0; /* 2 */ }
button, input { /* 1 */ overflow: visible; }
button, select { /* 1 */ text-transform: none; }
button, [type="button"], [type="reset"], [type="submit"] { -webkit-appearance: button; }
button::-moz-focus-inner, [type="button"]::-moz-focus-inner, [type="reset"]::-moz-focus-inner, [type="submit"]::-moz-focus-inner { border-style: none; padding: 0; }
button:-moz-focusring, [type="button"]:-moz-focusring, [type="reset"]:-moz-focusring, [type="submit"]:-moz-focusring { outline: 1px dotted ButtonText; }
fieldset { padding: 0.35em 0.75em 0.625em; }
legend { box-sizing: border-box; /* 1 */ color: inherit; /* 2 */ display: table; /* 1 */ max-width: 100%; /* 1 */ padding: 0; /* 3 */ white-space: normal; /* 1 */ }
progress { vertical-align: baseline; }
textarea { overflow: auto; }
[type="checkbox"], [type="radio"] { box-sizing: border-box; /* 1 */ padding: 0; /* 2 */ }
[type="number"]::-webkit-inner-spin-button, [type="number"]::-webkit-outer-spin-button { height: auto; }
[type="search"] { -webkit-appearance: textfield; /* 1 */ outline-offset: -2px; /* 2 */ }
[type="search"]::-webkit-search-decoration { -webkit-appearance: none; }
::-webkit-file-upload-button { -webkit-appearance: button; /* 1 */ font: inherit; /* 2 */ }
details { display: block; }
summary { display: list-item; }
template { display: none; }
[hidden] { display: none; }
```

- agregar fuentes de google, fuentes google

```html
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
```

```css
body { font-family: 'Roboto', sans-serif; }
```

### 103. Creando la sección de Nosotros

```html
...</header>
<section class="nosotros contenedor">
  <div class="imagen">
    <img src="./img/nosotros.jpg" class="img-nosotros">
    <div class="contenido-nosotros">
      <h1>Sobre Nosotros</h1>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam veritatis fugiat sit suscipit aut minus asperiores ut ea fugit repellat? Similique distinctio nam maxime aperiam velit, quos impedit facilis odio!</p>
    </div>
  </div>
</section>
```

estilos

```css
.nosotros { text-align: center; padding: 2rem; }
.contenido-nosotros {
  display: grid;
  align-content: center;
}
@media (min-width: 992px) {
  .nosotros {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-column-gap: 1rem;    text-align: left;
  }
}
```

![69_bienes_r_nosotros](./img/69_bienes_r_nosotros.gif)

### 105.107. Creando la sección de Anuncios

```html
<section class="contenedor">
  <h2>Ala venta</h2>
  <div class="venta">
    <div class="anuncio anuncio-destacada">
      <img src="img/casa1.jpg" class="casa1" />
      <div class="card">
        <h3 class="titulo">Casa 1 labore soluta</h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde quia minima </p>
          <p>a corporis, placeat dicta magni maxime autem veniam labore soluta ea, numquam debitis vero. Autem obcaecati explicabo amet porro laborum!</p>
          <p>Lorem, placeat dicta magni maxime autem veniam labore soluta ea, numquam debitis vero. Autem obcaecati</p>
          <div class="precio"> Precio <span>$3,000,000</span> </div>
          <a href="#" class="boton">Mas Informacion</a>
      </div>
    </div>
    <div class="anuncio">
      <img src="img/casa2.jpg" class="casa1" />
      <div class="card">
        <h3 class="titulo">Casa 1 labore soluta</h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde quia minima</p>
          <div class="precio"> Precio <span>$3,000,000</span> </div>
          <a href="#" class="boton">Mas Informacion</a>
      </div>
    </div>
  </div>
</section>
```

Estilos

```css
/* ---------------   Anuncios   --------------- */
h2 { font-size: 2.2rem; }
h3 { font-size: 2rem; }

.venta{
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(3, auto);
  grid-gap: 1rem;
}

.anuncio-destacada { /* version movil */
  grid-column: 1 / 3;
  display: grid;
  background-color: wheat !important;}

@media (min-width: 430px) { /* version tablet */
  .anuncio-destacada {
    grid-template-columns: repeat(2, 1fr);
  }
}

@media (min-width: 768px) { /* version escritorio */
  .venta {
    grid-template-columns: repeat(3, 1fr);
    grid-auto-rows: repeat(2, auto);
  }
}
.card { padding: var(--padding); }

.anuncio {
  display: grid;
  background-color: var(--claro);
}

.anuncio .precio {
  font-size: 1.4rem;
  font-weight: 400;
}

.anuncio .precio span{
  font-weight: 300;
  color: var(--primario-dark);
}

.boton {
  background-color: var(--primario);
  display: block;
  padding: var(--padding);
  text-align: center;
  color: var(--claro);
  text-transform: uppercase;
  text-decoration: none;
  margin-top: 1rem;
  bottom: 5px;
}
```

Resultado.

![69_bienes_r_alaventa](./img/69_bienes_r_alaventa.gif)

### 108. Creando el Footer

```html
<footer class="footer">
  <div class="contenedor">
    <div class="widget">
      <h3>Nosotros</h3>
      <p>Lorem, ipsum dolor sit amet consectetur adipisicing.</p>
    </div>
    <div class="widget navegacion">
      <h2>Navegacion</h2>
      <nav>
        <a href="#">Nosotros</a>
        <a href="#">Casas</a>
        <a href="#">Contacto</a>
      </nav>
    </div>
    <div class="widget">
      <h3>Casas a la venta</h3>
      <div class="contenedor-casas">
        <div class="casa">
          <div class="foto"><img src="img/casa2.jpg" /></div>
          <div class="info">
            <p>Casa 1</p>
            <p>2,000,000</p>
          </div>
        </div>
        <div class="casa">
          <div class="foto"><img src="img/casa3.jpg" /></div>
          <div class="info">
            <p>Casa 2</p>
            <p>2,000,000</p>
          </div>
        </div>
        <div class="casa">
          <div class="foto"><img src="img/casa4.jpg" /></div>
          <div class="info">
            <p>Casa 3</p>
            <p>2,000,000</p>
          </div>
        </div>
      </div>
    </div>
  </div><!-- Contenedor -->
  <p class="copyright">Derechos reservadoe de Bienes Raicez</p>
</footer>
```

Estilos

```css
.footer {
  background-color: var(--primario);
  margin-top: var(--padding2);
  color: var(--claro);
}

.footer .navegacion a {
  display: block;
  color: var(--claro);
}

.footer .contenedor-casas {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  grid-gap: 1rem;
}

.footer .contenedor-casas .casa {
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 1rem;
}

.footer .contenedor-casas .casa p {
  margin: 0;
}

@media (min-width: 768px) {
  .footer .contenedor {
    display: grid;
    grid-template-columns: 30% 20% 2fr;
    grid-column-gap: var(--padding2);
  }
}

.copyright {
  background-color: var(--primario-dark);
  text-align: center;
  padding: var(--padding);
}
```

Resultado.

![70_bienes_r_footer](./img/70_bienes_r_footer.gif)

## Sección 14: PROYECTO: Sitio Web para un Freelancer

<!-- file://E:\cursos\0activ\web\flexbox\freelancer-inicio -->

### 110. Creando el Header

Html del Header

```html
<div class="hero">
  <div class="contenedor">
    <div class="contenido">
      <h1 class="titulo">Diseño y desarrollo web</h1>
      <p class="subtitulo">Diseño, Programacion e E-Commerce</p>
    </div>
  </div>
</div>
```

Css

```css
.hero {
  background-image: url(../img/fondo.jpg);
  height: 100vh;
  background-size: cover;
  background-repeat: no-repeat;
}

.hero .contenedor {
  display: grid;
  align-content: center;
  height: 100vh; /* para centrar verticalmente*/
  text-align: center;
  color: var(--color-claro);
}

.titulo {
  margin: 0;
}

.subtitulo {
  font-size: 1.6rem;
}
```

**Resultado**:

![71_freelancer_header](./img/71_freelancer_header.PNG)

### 112. Creando la sección de Servicios

Html del Servicios

```html
<div class="servicios contenedor">
      <div class="servicio">
        <h2>Diseño Web</h2>
        <i class="fas fa-pencil-alt"></i>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, laboriosam quo sint ratione debitis</p>
      </div>
      <hr class="separacion" />
      <div class="servicio">
        <h2>Programacion web</h2>
        <i class="fas fa-code"></i>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, laboriosam quo sint ratione debitis</p>
      </div>
      <hr class="separacion" />
      <div class="servicio">
        <h2>E-Commerce</h2>
        <i class="fas fa-shopping-bag"></i>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, laboriosam quo sint ratione debitis</p>
      </div>
    </div>
```

Css

```css
p { font-family: var(--fuente-principal); font-size: 1.2rem; line-height: 2rem; }

.servicios {
  text-align: center;
  margin-top: 3rem;
}

.servicios h2 { padding-bottom: .5rem; }

.servicios h2::after { /* linea amarilla entre titulo de servicio e icono*/
  height: .5rem; width: 7rem; background-color: var(--color-primario);
  content: ''; display: block; margin: 0 auto; margin-top: 1rem;
}

.servicios i { font-size: 3rem; color: var(--color-secundario); }

.servicios .separacion { display: block; }

@media (min-width: 768px) {
  .servicios {
+   display: grid;
    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
    grid-column-gap: 1rem  ;
  }
  .servicios .separacion { display: none; }
}
```

**Resultado**:

![72_freelancer_servicios](./img/72_freelancer_servicios.gif)

### 114. Creando la sección de Portafolio

```html
<section class="portafolio contenedor">
  <h2>Mi Portafolio</h2>
  <div class="listado" id="listado"><!-- Rellenedo con javascript desde json--></div>
</section>
```

### 115. Utilizando Javascript Fetch API para llenar el portafolio

<!--
USO DE DE FETCH DE FORMA ORDENADA (CON PROMESAS DENTRO DE PROMESAS) EN
react-native.md->(Crear funciones para crear usuario en el backend)
-->

- agregar json, agregar datos.json

```json
{
  "portafolio" : [
    { "id" : 1, "nombre" : "Sitio Web", "desc" : "Molestias quam iure quia. Magni accusamus, itaque alias ipsa." },
    { "id" : 2, "nombre" : "App", "desc" : "Molestias quam iure quia. Magni accusamus, itaque alias ipsa." },
    { "id" : 3, "nombre" : "App", "desc" : "Molestias quam iure quia. Magni accusamus, itaque alias ipsa." },
    { "id" : 4, "nombre" : "Aplicación Web", "desc" : "Molestias quam iure quia. Magni accusamus, itaque alias ipsa." },
    { "id" : 5, "nombre" : "App", "desc" : "Molestias quam iure quia. Magni accusamus, itaque alias ipsa." },
    { "id" : 6, "nombre" : "App", "desc" : "Molestias quam iure quia. Magni accusamus, itaque alias ipsa." },
    { "id" : 7, "nombre" : "App", "desc" : "Molestias quam iure quia. Magni accusamus, itaque alias ipsa." },
    { "id" : 8, "nombre" : "App", "desc" : "Molestias quam iure quia. Magni accusamus, itaque alias ipsa." }
  ]
}
```

- Agregar javascript para agregar elementos desde json

<!-- agregar html desde json, tomar elementos de archivo json, leer archivo json, leer json, guardar informacion en json y mostrarla en html, crear html desde json, crear elementos desde json, crear html desde archivo json, crear elementos desde archivo json, convertir json a html, convertir de json a html, pasar json a html, convertir archivo json a html, pasar archivo json a html, -->

```js
// equivalente al onready, equivalente al onload
document.addEventListener('DOMContentLoaded', function() { //equivalente al document.ready
  cargarPortafolio();
});
function cargarPortafolio() {
  let html = '';
  // USO DE DE FETCH DE FORMA ORDENADA (CON PROMESAS DENTRO DE PROMESAS) EN react-native.md->(Crear funciones para crear usuario en el backend)
  fetch('datos.json')
    .then((resp)=> {
      return resp.json();
  }).then((datos) => { /* comillas para concatenar, comillas especiales, comillas para crear html*/
    datos.portafolio.forEach(proyecto => {      html +=      `
      <div class="elemento">
        <img src="img/${proyecto.id}.jpg">
        <div class="contenido">
          <h3>/${proyecto.nombre}</h3>
          <p>/${proyecto.desc}</p>
        </div>
      </div>
      `;
    }); //foreach
    document.querySelector('#listado').innerHTML = html;
  });
}
```

**Resultados** Podemos ver la informacion del json en el html

### 116. Estilos al Portafolio

```css
.elemento {
  width: calc(100% - 1rem); /* UNA FILA */
  -webkit-box-shadow: 13px 18px 12px -5px rgba(145,145,145,1);
  -moz-box-shadow: 13px 18px 12px -5px rgba(145,145,145,1);
  box-shadow: 13px 18px 12px -5px rgba(145,145,145,1);   margin-bottom: 3rem;
  padding-bottom: 2rem;
}

.elemento .contenido {
  padding: 0.5rem;
}

@media (min-width: 768px) {
  .listado {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-column-gap: 2rem;
    grid-auto-rows: max-content; /* hacer que un elemento tome el maximo alto de sus elementos*/
  }
  .elemento:nth-child(3) {
     /* el proyecto de la imagen mas grande tomara mas filas para evitar espacios vacios a lo alto*/
    height: fit-content;
    padding-bottom: 7rem;
    grid-row-end: span 2;
  }
}
```

**Resultado**:

![74_freelancer_portafolio](./img/74_freelancer_portafolio.gif)

### 117. Creando el Footer

```html
<footer class="footer">
  <div class="contenedor">
    <div class="widget">
      <h3>Contacto</h3>
      <p>Tel: 276 716 33 15</p>
      <p>Direccion del studio</p>
      <p>Tlaxcala My Camarada</p>
    </div>
    <div class="widget">
      <h3>Sobre Mi</h3>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis ad facere eligendi distinctio enim. Velit possimus quis nam quas adipisci, explicabo nisi officia iure, molestias ipsum, doloremque labore itaque tenetur!</p>
    </div>
  </div>
</footer>
```

Estilos

```css
.footer {
  color: var(--color-claro);
  background-image: url(../img/contacto.jpg);
  background-repeat: no-repeat;
  background-size: cover;
  padding: 2rem 0;
  margin-top: 5rem;
}

@media (min-width:768px) {
  .footer .contenedor {
    display: grid;
    grid-template-columns: repeat(2, 30%) auto;  }
}
```

**Resultado**:

![75_freelancer_footer](./img/75_freelancer_footer.gif)

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

## Sección 15: PROYECTO: AirBNB, recreando la página Principal

<!-- file://E:\cursos\0activ\web\flexbox\airbnb -->

```html
<div class="hero">
    <header class="header contenedor">
      <div class="logo">
        <img src="img/logo.png" alt="" class="logo">
      </div>
      <nav class="navegacion">
        <a href="#">Vender</a>
        <a href="#">Ayuda</a>
        <a href="#">Registro</a>
        <a href="#">Iniciar Sesion</a>
      </nav>
    </header>

    <div class="contenido-hero contenedor">
      <h1>Encuentra hospedaje para tus proximas vacaciones</h1>
      <form action="#">
        <input type="text" class="busqueda" placeholder="New York, Londres, Roma, Guadalajara">
        <input type="submit" value="Buscar" />
      </form>
    </div>
  </div>
```

Estilos

```css
:root {
  --fuente-principal: 'Lato', sans-serif;
  --color-primario: #da595e;
  --color-oscuro: #484848;
  --color-claro: #fff;
  --hospedaje: #275154;
  --concierto: #da595e;
  --clase: #59899a;
  --recorrido: #940ad4;
}
html { box-sizing: border-box;   }
*, *:before, *:after { box-sizing: inherit; }
body { font-family: var(--fuente-principal); }
h1 { font-size: 2.4rem; font-weight: 400; }
h2 { font-size: 2rem;}
h3 { font-size: 1.8rem;}
/* ##################### Hero ##################### */
.contenedor { max-width: 1100px; margin: 0 auto; }

.hero {
  height: 100vh;
  background-image: url(../img/arriba.jpg);
  background-size: cover;
  background-repeat: no-repeat;
  display: grid;
  grid-template-rows: 1fr 2fr; /* el hero tiene 2 columnas 1 de 1/3 y otra de 2/3*/
}

.header {
  padding-top: 1rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 90%;
  align-self: flex-start;
}
/* quitar estilos enlace, quitar estilos a enlace */
.navegacion a {
  color: var(--color-claro);
  text-decoration: none;
  margin-right: 1rem;
}
.navegacion a:last-of-type { margin-right: 0; }

.contenido-hero {
  display: grid;
  grid-template-columns: 5% auto 5%; /* creamos 3 columnas para la parte del buscador */
  grid-template-rows: max-content auto;
}

.contenido-hero h1 {
  color: var(--color-claro);
  grid-column: 2 / 3;
}

.contenido-hero form {
  grid-column: 2 / 3;
  grid-row: 2 / 3;
  position: relative;
  display: flex;
}

/* estilos al buscador */
.contenido-hero form input[type="text"] {
  flex: 1;
  height: 4rem;
  border: none;
  border-radius: 10px;
  background-image: url(../img/lupa.png);
  background-repeat: no-repeat;
  background-position: calc(1% + 1rem) calc(1% + .8rem);
  padding-left: 5rem; /* input evitar que se encime el texto, poner icono a input, agregar icono a input */
  font-size: 1.6rem;
 }
.contenido-hero form input[type="submit"] {
  position: absolute;
  right: 1rem;
  top: .6rem;
  padding: .7rem 2rem;
  border: none;
  background-color: var(--color-primario);
  color: var(--color-claro);
  border-radius: 10px;
  text-transform: uppercase;
  font-size: 1.2rem;
  border: 1px solid var(--color-primario);
}

.contenido-hero form input[type="submit"]:hover {
  background-color: var(--color-claro);
  color: var(--color-primario);
  cursor: pointer;
}
```

**Resultado:** Tenemos la parte del header y hero, aun hacer cambio de imagenesya que esa parte la haremos con javascript.

![76_airbnb_header_hero_buscador](./img/76_airbnb_header_hero_buscador.PNG)

### 123. Creando la primer sección de Anuncios

html

```html
<main class="contenido contenedor">   <section class="hacer">
    <h2>Que Hacer</h2>
    <div class="contenedor-cards">
      <div class="card">
        <img src="img/hacer1.jpg" >
        <div class="info">
          <p class="categoria concierto">Concierto</p>          <p class="titulo">Musica electronica 2018</p>          <p class="precio">$1,200 por persona</p>
        </div>
      </div>
      <div class="card">
        <img src="img/hacer2.jpg" >
        <div class="info">
          <p class="categoria concierto">Concierto</p>          <p class="titulo">Rock en los Angeles</p>          <p class="precio">$900 por persona</p>
        </div>
      </div>
      <div class="card">
        <img src="img/hacer3.jpg" >
        <div class="info">
          <p class="categoria clase">Clase de cocina</p>          <p class="titulo">Comida Mexicana Nivel 1</p>          <p class="precio">$1,200 por persona</p>
        </div>
      </div>
      <div class="card">
        <img src="img/hacer4.jpg" >
        <div class="info">
          <p class="categoria recorrido">Paseo en Bici</p>          <p class="titulo">Paseo en el bosque</p>          <p class="precio">$200 por persona</p>
        </div>
      </div>
    </div><!-- contenedor-cards -->
  </section>
</main>
```

Estilos

```css
/* ##################### Contenido Principal ##################### */
img { width: 100%; }
.contenedor-cards {
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));  /* Responsivo */
}
.info .categoria {
  text-transform: uppercase;
  font-size: .9rem;
  font-weight: 700;
}
.info .titulo {
  font-size: 1.6rem;
  font-weight: 900;
  margin: 0;
}
.info .precio {
  color: var(--color-oscuro);
  font-size: .9rem;
}
.hospedaje { color: var(--hospedaje); }
.concierto{ color: var(--concierto); }
.clase{ color: var(--clase);}
.recorrido{ color: var(--recorrido); }
```

**Resultado:**.

![77_airbnb_que_hacer](./img/77_airbnb_que_hacer.gif)

### 125. HTML para la sección Premium

html

```html
<section class="modulo">
  <h2>Precentamos MiViajePlus.con</h2>
  <div class="contenedor-cards premium">
    <div class="info">
      <h2>Una nueva seccion de alojamientos de lujo</h2>
      <a href="#" class="boton"> Explorar alojamientos</a>
    </div>
  </div>
</section>
```

Estilos

```css
.premium {
  background-image: url(../img/hospedaje-pro.jpg);
  background-repeat: no-repeat;
  /* como premium ya aplica la clase de contenedor-cards pero no necesitamos las 4 columnas en el movil*/
  grid-template-columns: unset;
  height: 30rem;
}

.premium .info { text-align: center; color: var(--color-claro); }

@media (min-width: 768px) {
  .premium {
    grid-template-columns: auto 30%; /* el auto es el espacion libre */  }
  .premium .info {
    text-align: right;
    grid-column: 2 / 3;
  }
  .premium .info h2 { color: black; }
}
.premium .info {
  display: flex;
  flex-direction: column;  justify-content: center; /* centrar vertical y horizontal*/
  padding: 1rem;
}

.boton {
  box-shadow: 7px 9px 22px 1px rgba(0,0,0,0.75); -moz-box-shadow: 7px 9px 22px 1px rgba(0,0,0,0.75); -webkit-box-shadow: 7px 9px 22px 1px rgba(0,0,0,0.75);
  background-color: var(--color-claro);
  display: block;
  padding: 1rem; text-decoration: none; font-size: 1.3rem; font-weight: 700; text-align: center; border-radius: 10px;
}

.boton:hover {
  color: var(--color-claro);
  background-color: #d1d1d1; }
```

**Resultado:**

![78_airbnb_oferta_vip](./img/78_airbnb_oferta_vip.gif)

### 128. Creando la sección de Hospedaje

html

```html
<section class="modulo">
  <h2>Hospedajes</h2>
  <div class="contenedor-cards-hospedajes">
    <div class="card">
      <img src="img/hospedaje1.jpg" >
      <div class="info">
        <p class="categoria hospedaje">Casa completa - 2 camas</p>
        <p class="titulo">Casa completa con todos los servicios y dos recamaras</p>
        <p class="precio">$3,200 por noche</p>
      </div>
    </div>
    <div class="card">
      <img src="img/hospedaje2.jpg" >
      <div class="info">
        <p class="categoria hospedaje">1 cuarto con 2 camas</p>
        <p class="titulo">1 Cuarto con 2 camas y alberca</p>
        <p class="precio">$1,900 por noche</p>
      </div>
    </div>
    <div class="card">
      <img src="img/hospedaje3.jpg" >
      <div class="info">
        <p class="categoria hospedaje">Cabaña completa - 4 camas</p>
        <p class="titulo">Baba;a en el bosque para 6 personas</p>
        <p class="precio">$2,500 por noche</p>
      </div>
    </div>
  </div>
</section>
```

Estilos

```css
.contenedor-cards-hospedajes {
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
}
```

**Resultado:** Si nos fijamos el css es el mismo que
el de *.contenedor-cards* incluso podriamos reutilizar
pero como lo queria mas pequeños pos ja.

![79_airbnb_hospedajes](./img/79_airbnb_hospedajes.gif)

### 128. Creando la sección de Destinos y anuncios

Aqui vamos a reutilizar lo que tenemos

```html
<section class="modulo destinos">
  <h2>Destinos populares</h2>
  <div class="contenedor-cards">
    <div class="card">
      <img src="img/populares1.jpg" >
      <div class="info"> <p class="titulo">Austria</p> </div>
    </div>
    <div class="card">
      <img src="img/populares2.jpg" >
      <div class="info"> <p class="titulo">Francia</p> </div>
    </div>
    <div class="card">
      <img src="img/populares3.jpg" >
      <div class="info"> <p class="titulo">Grecia</p> </div>
    </div>
    <div class="card">
      <img src="img/populares4.jpg" >
      <div class="info"> <p class="titulo">Inglaterra</p> </div>
    </div>
  </div>
</section>
 <section class="modulo">
<h2>Que Hacer En Tlaxcala</h2>
<div class="contenedor-cards">
  <div class="card">
    <img src="img/newyork1.jpg" >
    <div class="info">
      <p class="categoria concierto">CLase</p> <p class="titulo">Comida Japonesa para principiantes</p> <p class="precio">$1,200 por persona</p>
    </div>
  </div>
  <div class="card">
    <img src="img/newyork2.jpg" >
    <div class="info">
      <p class="categoria concierto">Concierto</p> <p class="titulo">Festival EDF 2019</p> <p class="precio">$900 por persona</p>
    </div>
  </div>
  <div class="card">
    <img src="img/newyork3.jpg" >
    <div class="info">
      <p class="categoria clase">Clase de cocina</p> <p class="titulo">Paella Domingos</p> <p class="precio">$1,200 por persona</p>
    </div>
  </div>
  <div class="card">
    <img src="img/newyork4.jpg" >
    <div class="info">
      <p class="categoria recorrido">Paseo en Caballo</p> <p class="titulo">Paseo a caballo</p> <p class="precio">$200 por persona</p>
    </div>
  </div>
</div><!-- contenedor-cards -->
</section>
```

Estilos <!-- poner texto dentro de imagen, meter texto dentro de imagen, hacer imagen con texto encima -->

```css
.destinos .contenedor-cards { grid-template-columns: repeat(auto-fit, minmax(200, 1fr)); }

.destinos .card { margin: 0 auto; position: relative; }
.destinos .info { position: absolute; bottom: 1rem; left: 1rem; }

.destinos .info .titulo { color: var(--color-claro); font-size: 1.8rem; }
.destinos .card img{ max-height: 400px; max-width: 270px; }
```

**Resultado:**

![80_airbnb_Destinos_anuncios](./img/80_airbnb_Destinos_anuncios.PNG)

### 132.135. Creando el footer y botón mostrar/ocultar el footer JS

Html

```html
<footer id="footer">
  <div class="contenedor-footer contenedor">
    <div class="nav-footer">
      <h3>MiViaje.com</h3>
      <nav class="menu">
        <a href="#">Empleo</a> <a href="#">Prensa</a> <a href="#">Politicas</a> <a href="#">Ayuda</a>
      </nav>
    </div> <!-- nav-footer -->
    <div class="nav-footer">
      <h3>Descubre</h3>
      <nav class="menu">
        <a href="#">Confianza y seguridad</a> <a href="#">Credito de Viajero</a> <a href="#">AirBnb Cities</a> <a href="#">Viajes de negocios</a>
      </nav>
    </div> <!-- nav-footer -->
    <div class="nav-footer">
      <div class="menu">
        <h3>Hospedaje</h3>
        <nav class="menu">
          <a href="#">Razones para hospedar</a> <a href="#">Hospitalidad</a> <a href="#">Como ser un anfitrion</a> <a href="#">Cominidad anfitriones</a>
        </nav>
      </div> <!-- nav-footer -->
  </div>
  <div class="nav-footer">
    <h3>Redes Sociales</h3>
    <nav class="redes-sociales">
      <ul>
        <li><a href="https://facebook.com" target="_blank"><span>Facebook</span></a></li>
        <li><a href="https://twitter.com" target="_blank"><span>Twitter</span></a></li>
        <li><a href="https://instagram.com" target="_blank"><span>Instagram</span></a></li>
      </ul>
    </nav>
  </div> <!-- nav-footer -->
  </div>
</footer>
<a href="#" class="btn-flotante">Pie de pagina</a>
```

Javascript

```js
document.addEventListener('DOMContentLoaded', function() { //equivalente al document.ready
  setupBotonFlotante();
});

function setupBotonFlotante() {
  const btnFlotante = document.querySelector(".btn-flotante");
  const footer = document.querySelector("#footer");
  btnFlotante.addEventListener('click', (e)=>{
    e.preventDefault(); // evitar que se habra el enlace, tratar enlace como boton

    //buscar si un elemento tiene una clase, saber si un elemento tiene una clase, buscar clase en elemento, buscar una clase en elemento, buscar si existe una clase en un elemento, saber si existe una clase en un elemento, buscar si existe clase en un elemento, saber si existe clase en un elemento, buscar si existe clase en elemento,  saber si existe clase en elemento,    if(footer.classList.contains("footer-activo")) {
      // quitar clase a elemento, remover clase a elemento, borrar clase a elemento
      footer.classList.remove('footer-activo');
      btnFlotante.innerText = "Pie de pagina";
    } else {
      // agregar clase a elemento, añadir clase a elemento
      footer.classList.add('footer-activo');
      btnFlotante.innerText = "X Cerrar";
    }
  });
}
```

Estilos

```css
/* ##################### Footer ##################### */
#footer {
  -webkit-box-shadow: 0px -10px 9px -1px rgba(0,0,0,0.28);
  -moz-box-shadow: 0px -10px 9px -1px rgba(0,0,0,0.28);
  box-shadow: 0px -10px 9px -1px rgba(0,0,0,0.28);

  position: fixed;
  bottom: -100%; /* por defecto escondemos el footer */
  background-color: var(--color-claro);
  width: 100%;
  padding-bottom: 4rem;
  padding-left: 2rem;
  padding-right: 2rem;
  max-height: 95%;
  transition: all .3s ease-in; /* animacion */
}

.footer-activo {  bottom: 0 !important; /* motrar elemento agregando clase desde javascript */
}

.contenedor-footer {  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
}
.nav-footer h3{ font-size: 1.2rem; font-weight: 900; }

.nav-footer a {
  display: block; /* evitar que los enlaces se muestren uno al lado del otro */
  margin-bottom: .5rem;
  text-decoration: none;
  color: var(--color-oscuro);
}
.nav-footer a:hover { text-decoration: underline; }
.nav-footer a:last-of-type { margin: 0; }

/* Redes redes-sociales */
.redes-sociales ul li span{ display: none; }
.redes-sociales ul {
    list-style: none;
    display: flex;
    flex-wrap: wrap;
    padding: 0;
}
@media screen and (min-width:768px) {
    .redes-sociales ul { justify-content: flex-end; }
}
.redes-sociales ul li {  margin-right: 1rem; /* SEPARACION ENTRE ICONOS */ }
.redes-sociales ul li a { text-decoration: none; }
.redes-sociales ul li a:before {
    font-family: "Font Awesome 5 Brands";
    color: var(--color-oscuro);  /* COLOR DE LOS ICONOS */
    font-size: 2rem; /* TAMAÑO DE LOS ICONOS */
}
.redes-sociales ul li a[href*='https://facebook.com']::before  { content: "\f39e"; }
.redes-sociales ul li a[href*='https://twitter.com']::before   { content: "\f099"; }
.redes-sociales ul li a[href*='https://instagram.com']::before { content: "\f16d"; }
/* Fin Redes redes-sociales*/

.btn-flotante {
  -webkit-box-shadow: 7px 9px 22px 1px rgba(0,0,0,0.75);
  -moz-box-shadow: 7px 9px 22px 1px rgba(0,0,0,0.75);
  box-shadow: 7px 9px 22px 1px rgba(0,0,0,0.75);
  position: fixed;
  bottom: 1rem;
  right: 1rem;
  background-color: var(--color-claro);
  color: var(--color-oscuro);
  padding: .5rem 3rem;
  text-decoration: none;
  transition: all .3s ease;
}
/* cuando se posiciona encima el mause */
.btn-flotante:hover { background-color: var(--color-primario); color: var(--color-claro); }
```

**Resultado:**

![81_airbnb_footer](./img/81_airbnb_footer.gif)

### 138. Últimos Ajustes al Proyecta

Agregamos transicion para que la imagen no cambie de forma abrupta

```css
.hero {...
  -webkit-transition: background-image 0.2s ease-in-out;
  transition: background-image 0.2s ease-in-out;
}
```

Javascript, funcion para cambiar la imagen.

```js
function setupCambioDeImagenHero() {
  const imagenHero = document.querySelector('.hero');

  const imagenes = ['arriba2.jpg','arriba.jpg'];
  let i = 0;
  let tiempo = 0;
 
  setInterval(() => {
    imagenHero.style.backgroundPositionY = -1*tiempo+"px";
    if(tiempo>10) {      tiempo=0;      if(i === imagenes.length-1) { i=0; }      else { i++; }
      imagenHero.style.backgroundImage = 'url(../img/'+imagenes[i]+')';
    }
    tiempo += 1;
  }, 150);
}
```

**Resultado:**

![82_airbnb_cambio_imagen](./img/82_airbnb_cambio_imagen.gif)

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

## Mis descubimientos

### Errores

El body no ocupa todo el ancho, body no ocupa todo el ancho, no ocupa
todo el ancho el body/ El body no cubre todo el ancho, body no cubre
todo el ancho, no cubre todo el ancho el body / al reducir la pagina
el body se encoje / el body deja un espacio vacio body, espacio vacio
en el body, espacio vacio body espacio vacio

```css
body {
  min-width: fit-content;
}
```

### Falta por estudiar

<!--Cosas que faltan por estudiar
	- Estados de una promesa
	- funciones de flecha gorda y sus formas de declararlas
	

-->

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!--
.

#############################################################################################################
#############################################################################################################

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

## Proyectos

<!-- proyectos lista -->
- file:///E:/cursos/0activ/web/flexbox/bienes/index.html

- file:///E:/cursos/0activ/web/flexbox/airbnb/index.html

- file:///E:/cursos/0activ/web/flexbox/carrito/index.html

- file:///E:/cursos/0activ/web/flexbox/blog-viajes-inicio/mio/index.html

- file:///E:/cursos/0activ/web/flexbox/adminFlex/index.html

- file:///E:/cursos/0activ/web/flexbox/receta-inicio/index.html

- file:///E:/cursos/0activ/web/flexbox/masonry/index.html

- file:///E:/cursos/0activ/web/flexbox/holy-grail/index.html

- file:///E:/cursos/0activ/web/flexbox/freelancer-inicio/index.html

- file:///E:/cursos/0activ/web/flexbox/footer_fijo/index.html

- file:///E:/cursos/0activ/web/flexbox/login-netflix/netflix_mio/index.html

