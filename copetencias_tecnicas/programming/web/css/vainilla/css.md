# CSS

Importar archivo en css: `@import url('archivo.css');`

Importar archivo de css en un archivo html

<!-- agregar css, importar css, incluir css 
agregar estilos, importar estilos, incluir estilos  -->

```css
<link href="path/estilos.css" rel="stylesheet" type="text/css" />
```

Agregar css dentro del html

```css
<style>
  div { ... }
<style>
</head><body>...
```

## Selectores

**ETIQUETA**  Para aplicar un estilo sobre una ETIQUETA, solo se pone la etiqueta
Ejemplo:

```css
div { color: red; }
```

- ID: Para aplicar estilo a in ID, se utiliza el simbolo "#" (gato)
- La idea de los id es que solo se utilizen para identificar
  un componente, seria un error poner un id a mas de un componente
- Ejemplo:

```css
#nombre-id { color: red; }

<div id="nombre-id" >
```

**CLASES**: Las clases son usadas para definir un tipo de estilo
  el cual podemos aplicat en varios componentes, el cuaal usamos
  el punto "."
Ejemplo:

```css
.mi-clase { height: 50px; }

<div class="mi-clase">
```

Podemos aplicar un estilo solamente a una etiqueta que
tenga cierta clase, es decir solo se va a aplicar sobre
la etiqueta que tenga dicha clase

Ejemplo:

```css
div.mi-clase { background-color: blue; }
```

- Seleccionar componentes hijo de una clase

```css
.mi-clase div { color: red; }
```

**ATRIBUTOS:** Podemos seleccionar un componente que tenga un x atributo

```css
input[type] { color: red; }
```

- Para seleccionar un componente con un atributo con un cierto valor

```css
p[class="verde"] { background-color: lightblue; }
```

- Para seleccionar un componente con una clase que almenos tenga una
x palabra en su valor, por ejemplo para leccionar aquellos parrafos
que esten un ideama

```css
p[lang|="es"] { color: red; }

<p lang="traduccion-es"> <- Seleccionariamos este
<p lang="traduccion-in">
```

- Seleccionar que empieze con X texto el valor de la propiedad, seleccionar por propiedad.

```css
p[title^="algo"] { color: red; }

<p title="algo de esto">lorem</p>
```

**Selector Nth-child**: Selecciona elementos
segun un numero que se le indique

<!-- seleccionar x elemento, tomar x elemento -->
- Seleccionar los elementos pares

```css
p:nth-child(even) { color: red; }
```

- Selecciona los elementos impares

```css
p:nth-child(odd) { color: red; }
```

- Selecciona un elemento indicandolo por su numero. Nota funciona raro.

```css
p:nth-child(3) { color: red; }
```

**Selector first-letter**: Selecciona la primer letra del elemento.

```css
p:first-letter { color: red; }
```

**SELECTORES ADYACENTES**: Estos elementos solo funciona en aquellos
componetnens sean hermanos, los cuales podemos buscar cosas como
selecciona el componente que este junto a x componente
de un mismo componente osea son hermanos

```css
p + p { color: blue; }
<div>
  <p>
  <p>   <- seleccionar este
  <p>   <- seleccionar este
  <div>
  <p>   <- Este no aplicaria
</div>
```

- margin
<!-- TAGS: orden de asignacion en estilos css, orden parametros css, orden de estilos-->
Sintaxis

```css
margin; arriba, abajo, izq, der
```

## Degradados

para mas detalles: [Ducu](https://www.w3schools.com/css/css3_gradients.asp)

```css
background-image: linear-gradient(red, yellow);
```

## Fuentes

- Importar Fuentes

```css
@font-face {
  font-family: "Bitstream Vera Serif Bold";
  src: url("path/fuente.ttf");
}
body { font-family: "Bitstream Vera Serif Bold", serif }
```

- Sombra de textos

```css
text-shadow: [ejeX]px [ejeY]px [desenfoado]px [color];
```

- Multiples columnas y su entre-espaciado, separador y su color

```css
column-count: 2;
column-gap: 20px;
column-rule: solid;
column-rule-color: color;
```

## Media Querys

```css
@media screen and (min-width: 400px) {
  body { background-color: #ddf; }
}

@media screen and (min-width: 800px) {
  body { background-color: #fdd; }
}
```

Documentacion Lista: [Link](https://www.w3schools.com/css/css_pseudo_classes.asp)

## quitar estilos boton

Quitar estilos a boton, boton sin estilos, boton como link

```css
.btn-link {
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
}
```
