# Dart

- [Dart](#dart)
  - [Accesos rapidos](#accesos-rapidos)
  - [Introduccion](#introduccion)
    - [Hola mundo](#hola-mundo)
    - [Tipo de datos](#tipo-de-datos)
    - [Uso de const y final](#uso-de-const-y-final)
    - [Operadores matematicos, logicos, de asignacion](#operadores-matematicos-logicos-de-asignacion)
    - [Control de flujo, for, switch, if, random](#control-de-flujo-for-switch-if-random)
    - [Funciones, argumentos opcionales, funciones flecha, cartear iterables](#funciones-argumentos-opcionales-funciones-flecha-cartear-iterables)
    - [Callbacks](#callbacks)
    - [Tipos no muy comunes y Future](#tipos-no-muy-comunes-y-future)
    - [Async-Await](#async-await)
    - [Catch-Error en Futures](#catch-error-en-futures)
    - [Streams Basico](#streams-basico)
    - [Stream: onError, onDone, cancel](#stream-onerror-ondone-cancel)
    - [Streams Multi-Listeners](#streams-multi-listeners)
    - [Threads: periodic](#threads-periodic)
    - [Threads: Isolates](#threads-isolates)
  - [Orientado a Objetos](#orientado-a-objetos)
    - [Propiedades Finales en clases](#propiedades-finales-en-clases)
    - [Constructores constantes](#constructores-constantes)
    - [Constructores Factory (palabra reservada factory)](#constructores-factory-palabra-reservada-factory)
    - [Estaticos](#estaticos)
    - [Patron Singleton](#patron-singleton)
    - [Clases Abstractas, herencia, override y super](#clases-abstractas-herencia-override-y-super)
    - [Mixin](#mixin)
  - [Proyectos](#proyectos)
    - [Convertir peticion a Objeto](#convertir-peticion-a-objeto)

## Accesos rapidos

- Pagina oficial de dart: [dart.dev](https://dart.dev/)
- Documentacion: [api.dart.dev](https://api.dart.dev/)
- [guia de estilos](https://dart.dev/guides/language/effective-dart/style)
- Paquetes/dependencias: [pub.dev](https://pub.dev/)

## Introduccion

- Fuente: [get-dart/get-dart](https://dart.dev/get-dart)

instalar dart: `choco install dart-sdk`

### Hola mundo

```js
// HolaMundo.dart
main() {
  print('Hola Dart');
}
```

Ejecutamos con: `dart HolaMundo.dart`

### Tipo de datos

Usa un tipado estatico pero en algunos escenario se puede dinamico, pero
no es recomendable

```dart
main() {
  print('================- Numeros -==================');
  // No es dinamico infiere el tipo de dato
  var myNum = 11;
  print('MyNum: ${myNum}'); // concatenacion
  
  // Numeros
  int myEntero = 11;
  print(myEntero);

  double myDouble = 11.2;
  print(myDouble);



//========================================================================
  print('================- Cadenas -==================');

// Cadenas
  String cadena1 = 'Mi "Cadena" o \'string\' '; // Mi "Cadena" o 'string' 
  print(cadena1);

  String bloque = 
"""
----
bloque 
de
texto
multi
lineas
----
""";
  print(bloque);



// Booleans
  print('================- Booleanos -==================');
  // Boolean
  bool miBoleano;
  print(miBoleano); // existen los null en el booleano

  miBoleano = true;
  print(miBoleano);



// Listas
  print('\n================- Listas -==================');
  List<String> myList = ['Superman', 'Batman', 'Sancho'];
  myList.add("WonderWoman");
//usar el objeto de antes
  myList..add("Flash")
        ..add("Flash");//soporta duplicados a diferencia de los sets
  print('Lista de cadenas: ${myList}');
  
  List<int> myListNums = new List();
  myListNums.addAll([1,2,3]);
  print('Lista de numeros: ${myListNums}');



//LAS LISTAS TAMBIEN FUNCIONAN COMO ARREGLOS
  List<bool> mybools = new List(4);
  mybools[0] = true;
  mybools[1] = false;
  //mybools[2] = true; null
  mybools[3] = true;
  //mybools[4] = true; // Invalid value: Not in range 0..3, inclusive: 4
  print('"Lista"-Arreglo de Booleanos: ${mybools}');



// Sets
  print('\n================- SETS -==================');
  
  var cosas = {'Mario', true, 20, 170.6}; // Infiere que es una lista de Objetos (todo hereda de objeto)
  cosas.add(20); // se da cuenta de que ya esta y no lo agrega
  print('Set de cosas: ${cosas}');

  Set<String> mascotas = {'Choco', 'Nona', 'Humo', 'Choco'}; // no se agrega choco
  print('Set de mascotas: ${mascotas}');



// MAPAS
  print('\n================- SETS -==================');
  Map<dynamic, Object> nona = { // declaracion de dinamicos
    'Nombre': 'Nona',
    'Edad': 15,
    'Activa': true,
    111: 111.1
  };
  print('Map de nona: ${nona}');
  print('Map de nona.111: ${nona[111]}');


  Map<String, dynamic> choco = new Map();
  choco.addAll({
    'Nombre': 'Nona',
    'Edad': 15,
    'Activa': true,
    "111": 111.1}
  );


// MAPAS Enums https://dart.dev/guides/language/language-tour#enumerated-types
  print('\n================- ENUMS -==================');
  print(Color.red);
}

enum Color { red, green, blue }
```

### Uso de const y final

```dart
main() {
  final numA = 10; // inferira el tipo de dato
  const numB = 10; // inferira el tipo de dato
  
  final String cadena = 'MiCadena'; // mas ligera en memoria por que reserva el espacio en memoria
  //cadena = "cambio"; //ERROR
  
  const bool bandera = true;  // mas pesada y solo conjela la referencia no el espacio de memoria
  //bandera = false; //ERROR

  final List<String> configDinamica = ['up', 'up','right','down'];
  configDinamica.add('left'); // lo acepta
  //configDinamica = new List(); ERROR

  //const List<String> configEstatica = ['arriba', 'arriba','der','abajo'];
  List<String> configEstatica = const['arriba', 'arriba','der','abajo'];
  //configEstatica.add('izq'); // Dara error en runTime
}
```

### Operadores matematicos, logicos, de asignacion

```dart
main() {
  int resto = 10 % 3; // 1 = producto = resto de la divicion
  int productoEntero = 10 ~/ 3; // 3 = parte entera del producto, ya que div sria 3.333...

  // asignar variable solo si la variable es nulo, sirve para evitar un nullpointException
  int varNull = null;
  varNull ??= 21;

  // ternario
  int a = (1>2) ? 3 : 4; //
  

  // asignar el que no sea nulo
  String nullUno = null;
  String nullDos = null;
  
  String valor = 'valor';
  String aUsar = valor ?? nullUno; // si asignara "valor"
  String mostrarDos = nullDos ?? nullUno ?? 'mostrar'; // si asignara "mostrar"

  /**
   * Condiciones los de siemrep <, >, =, ||, ...
   */
  // checar tipos, comprovar tipo de variable
  int entero = 1;
  print(entero is int); // true
  print(entero is String); // false
}
```

### Control de flujo, for, switch, if, random

```dart
import 'dart:io';
import 'dart:math';

main(List<String> args) {

  List<String> myList = ['Superman', 'Batman', 'Sancho'];
  for (var nombre in myList) {
    stdout.writeln(nombre);
  }

  for (var i = 0; i <= 10; i++) {
    if (i==4) {
      continue;
    } else if(i==9) {
      stdout.writeln('iteracion: ${i} Adios');
      break;
    }
    stdout.writeln('iteracion: ${i}');
  }


  forDeAfuera:for (var i = 0; i<=3; i++) {
    stdout.writeln('Iteracion ${i}');
    for (var j = 0; j <3; j++) {
      stdout.writeln('Iteracion Doble: ${j}');
      if(i == 2 && j == 1) {
        stdout.writeln('Denener ambos for en: ${i}:${j} ');
        break forDeAfuera;
      }
    }
  }
  
  int bandera = Random().nextInt(3);
  switch (bandera) {
    case 1:
      stdout.writeln('Switch a');
    break;
    case 2:
      stdout.writeln('Switch b');
    break;
    case 3:
      stdout.writeln('Switch c');
    break;
    default:
      stdout.writeln('default');
  }


  stdout.write('Ingreda tu edad: ');
  //String edadSt = stdin.readLineSync();
  String edadSt = '60';
  if(int.parse(edadSt) < 18 ) {
    stdout.writeln('Chamaco');
  } else {
    stdout.writeln('Ansiano');
  }
}
```

### Funciones, argumentos opcionales, funciones flecha, cartear iterables

```dart
main(List<String> args) {
  // Si ejecutamos "dart basico/06_funciones.dart a burro c d 11" nos imprimira "[a, burro, c, d, 11]"
  print(args);
  // Usamos funcion sin pasarle un argumento opcional
  saludar("Sancho");
  saludar("Mario", 'Marquez');

  // argumentos con nombre
  log(level: LOG.info, msg: 'Algo');

  // manejo/entendiendo las referencias
  List<String> nombres = ['mario', 'pancho','sancho', 'pedro', 'pancho'];
  //ya que se pasa la referencia, clonamos para no afectar la original
  List<String> nombresMayus = listMayus(nombres);
  print('Original   : ${nombres}');
  print('Mayus      : ${nombresMayus}');

  //funciones de flecha (igual que sql) filtrar
  //CUIDADO: sin el ".toList();" retorna un iterable lo cual no se castea automaticamente a list
  List<String> nombresConP = nombres.where((n) => n.startsWith('p') ).toList();
  print('nombresConP: ${nombresConP}');
  print('nombresConP (sin repetir): ${nombresConP.toSet()}');
}

enum LOG { debug, info, warn, error }

//argumentos opcionales
void saludar(String nombre, [String apellidos = '<sin-apellido>']) {
  print('Hola ${nombre.toUpperCase()} ${apellidos}' );
}

// argumentos por valor
void log({String msg, LOG level}) {
  switch (level) {
    case LOG.debug: print('DEBUG--: ${msg}'); break;
    case LOG.error: print('ERROR---------------- ${msg}'); break;
    case LOG.info: default: print('INFO--------: ${msg}'); break;
  }
}

List<String> listMayus(List<String> nombres) {
  nombres = [ ...nombres ]; // clonar lista para no modificar la original
  for (var i = 0; i < nombres.length; i++) {
    nombres[i] = nombres[i].toUpperCase();
  }
  return nombres;
}
```

### Callbacks

Los callback no son muy usados en dart/flutter ya que se suele usar mas
las funciones asincronas

```dart
main() {
  print('Buscando usuario con ID 2 ...');
  findUserById(2, (String usuario) {
    print("Usuario encontrado: ${usuario}");
  });
}
  
String findUserById(int i, Function callback) {
  String usuario = 'Mario';
  callback(usuario);
}
```

### Tipos no muy comunes y Future

```dart

import 'dart:collection';
import 'dart:io';

main() {

  print("-----------------------= Cola =--------------------------");
  // En teoria es mas eficiente para recorrerlo
  Queue<int> cola = new Queue();
  cola.addAll([1,2,3]);
  Iterator colaIter = cola.iterator;
  while (colaIter.moveNext()) {
    print(colaIter.current);
  }

  // son lo mismo que las promesas en JS, una funcion que se ejecutara en el futuro.
  print("\n-----------------------= FUTURE =--------------------------");
  Future<String> timeout = Future.delayed(Duration(seconds: 2), () { 
    print('print A'); return '200'; 
  });
  timeout.then((resp) => print('print B: ${resp}') );
  print('print C');



  print("\n-----------------------= FUTURE 2 =--------------------------");
  String path = Directory.current.path+'\\basico\\datos.txt';
  File file = new File(path);
  Future<String> txtF = file.readAsString(); // .readAsStringSync() para que el hilo principal se detenga
  txtF.then((txt) => print('Print D: \n${txt}') );

}
```

### Async-Await

```dart
import 'dart:io';
// de esta forma hacemos que se ejecute linea por linea asiendolo mas legible
main() async {
  String fileName = 'datos.txt';
  String txt = await leerArchivoSameFolder(fileName);
  print(txt);
}
// Transforma una funcion a asincrona y a fuersa retorna un Future
Future<String> leerArchivoSameFolder(String filename) async {
  String path = Directory.current.path+'\\basico\\' + filename;
  File file = new File(path);
  Future<String> txtF = file.readAsString();
  return txtF;
}
```

### Catch-Error en Futures

```dart
main(List<String> args) {

  Future<String> timeout = Future.delayed(Duration(seconds: 2), () { 
    if(1==1) { throw 'Mi-Error'; }
    print('print A'); return '200'; 
  });

  timeout
    .then((resp) => 
      print('print B: ${resp}'
    ))
    .catchError((error) => {
      if(error=='Mi-Error') { print('Controlado') }
    })
  ;
  print('print C');
}
```

### Streams Basico

```dart
import 'dart:async';

// Los Stream son flujos de datos, muy utiles en botones o sockets, cosas
// que no sabemos cuando o cuantas veces se ejecutaran
main() {
  final myStream = StreamController();

  myStream.stream.listen((dato) {// muy parecido al suscribe
    print("Entro: ${dato}");
  });
  
  meterDatos(myStream);
}
  
void meterDatos(StreamController myStream) async {
  await Future.delayed(Duration(milliseconds: 300));
  myStream.sink.add('Dato-1');
  await Future.delayed(Duration(seconds: 1));
  myStream.sink.add('Dato-2');
  await Future.delayed(Duration(milliseconds: 600));
  myStream.sink.add('Dato-3');
}
```

### Stream: onError, onDone, cancel

```dart
import 'dart:async';

// Los Stream son flujos de datos, muy utiles en botones o sockets, cosas
// que no sabemos cuando o cuantas veces se ejecutaran
main() {
  final myStream = StreamController<String>(); // se infiere "StreamController<String>();"

  var listener; // lo declaramos asi para poder usar el 'cancel() desde dentro'
  listener = myStream.stream.listen( // muy parecido al suscribe
    (dato) {
      print("Entro: ${dato}");
    }, onError: (error) {
      if(error == 'error-fatal') {
        print('Error fatal cerrando');
        listener.cancel();
        return; // para que ya no siga el hilo
      }
      print("Error Controlado: ${error}"); // si no lo controlamos los demas ya no entraran
    },
    //cancelOnError: true // por defecto false, si es true no importa que tengamos el `onError` se detendra el listener
    onDone: () {
      print('Stream Cerrado');
    }
  );

  meterDatos(myStream);
}


// Cuidado si no definimos el tipo aqui "<String>" no nos dara errores de compilacion
void meterDatos(StreamController<String> myStream) async {
  await Future.delayed(Duration(milliseconds: 300));
  myStream.sink.add('Dato-1');

  await Future.delayed(Duration(seconds: 1));
  myStream.sink.add('Dato-2');
  
  await Future.delayed(Duration(milliseconds: 200));
  myStream.sink.addError('Error Dato-3');

  await Future.delayed(Duration(milliseconds: 600));
  myStream.sink.add('Dato-4');

  //myStream.sink.add(11); // causara error de contruccion porque solo hacepta Strings

  //await Future.delayed(Duration(milliseconds: 200));
  //myStream.sink.addError('error-fatal');
  myStream.sink.close(); // es necesario cerrar los streams cuando sepamos que ya no resiviran datos

  //await Future.delayed(Duration(milliseconds: 100)); // error en runTime por que ya se cerro
  //myStream.sink.add('Dato-5');
}
```

### Streams Multi-Listeners

```dart
// Solo podemos tener un listener por stream activo, podemos asignarle diferentes pero
// antes de asignarle otro debemos de cancelar el anterior, podemos tener varios usando
// broadcast
import 'dart:async';
import 'dart:io';

main() {
  final myStream = StreamController<int>.broadcast();
  myStream.stream.listen((dato) => stdout.write);
  myStream.stream.listen((dato) => { stdout.write(" Multi $dato X 2: ${dato*2}, ") });
  myStream.stream.listen((dato) => { if(dato < 3) stdout.write("Menor a 3\n") });
  myStream.stream.listen((dato) => { if(dato > 4) stdout.write("mayor a 4\n") });

  myStream.sink..add(1)..add(2)..add(4)..add(6)..add(8)..add(9)..add(11);
}
```

### Threads: periodic

Fuente: [stackoverflow](https://stackoverflow.com/questions/52569602/flutter-run-function-every-x-amount-of-seconds)

Con el siguiente codigo, ejecutamos un codigo cada dos segundos, el cual
podemos vijilar una variable, y en tanto esta cambia terminamos este loop.

```dart
import 'dart:async';

void main() async {
  var bandera = true;

  print("inicia delay");
  Future.delayed(Duration(milliseconds: 5200), () {
    print("evento");
    bandera = false;
  });

  print("inicia timer");
  var timer = Timer.periodic(Duration(seconds: 1), (Timer t) {
    if(bandera) {
      print("echo");
    } else {
      t.cancel();
    }
  });
  print("termina main");
}
```

**Resultado:** Tenemos una funcion que se ejecuta cada dos segundos de
forma indefinida, pero en este caso la detenemos con una bandera.

### Threads: Isolates

En cada codigo de abajo estamos corriendo la misma funcion, en tres hilos
diferentes de ejecucion, lo cual agrege un delay al final, para que el
hilo del main sea el ultimo en terminar, y de esa forma se ve el resultado
de cada hilo.

```dart
import 'dart:isolate';

void main() async { 
   Isolate.spawn(foo,'Hello!!'); 
   Isolate.spawn(foo,'Greetings!!'); 
   Isolate.spawn(foo,'Welcome!!'); 
   
   print('execution from main1'); 
   print('execution from main2'); 
   print('execution from main3');

   await Future.delayed(Duration(milliseconds: 500));
}

void foo(var message){ 
   print('execution from foo ... the message is :${message}'); 
}
```

## Orientado a Objetos

Casi igual que java

```dart
class Persona {
  String nombre;
  int edad;
  bool activo;
  String _pass; // propiedad privada

  // Constructores
  Persona() {
    print('Hola desde constructor');
  }

  //solo de esta manera podemos tener varios contrustores
  Persona.toSave(this.nombre, [this.edad] );


  get pass => '--$_pass--';
  set pass (String passIn) => this._pass = passIn.toUpperCase();

  // DA LO MISMO solo arriba usamos las arrowFunction
  // set pass (String passIn) {
  //   this._pass = passIn;
  // }

  @override
  String toString() {
    return 'nombre: $nombre, edad: $edad, activo: $activo';
  }
}

------------------------------------------------------------------------------------------------

import 'clases/Persona.dart';

main() {
  final mario = new Persona(); // la palabra reservada es opcional pero ehh
  mario.nombre = 'Mario';
  mario.edad = 27;
  mario.activo = true;
  print(mario); // el toString se llama de forma auto.
  mario.pass = 'LaPass'; // estamos usando el setter
  print('Uso de get para var privada ${mario.pass}'); // estamos usando el getter

  // no existe sobrecarga de constructores asi que usamos "contructores nombrados"
  final marioA = new Persona.toSave('Mario');
  final marioB = new Persona.toSave('Mario', 20);

  print('MarioA - $marioA');
  print('MarioB - $marioB');
}
```

### Propiedades Finales en clases

```dart
/** Sabemos que el area de un cuadrado es lado*lado pero como lo definimos
 *  si el area es final (es correcto por que no debemos poderla cambiar)
 * SOLUCION: podemos hacer esto desde el constructor
 */ 
class Cuadrado {
  final int lado;
  final int area; // ¿como la asignamos si es final?

  // de esta forma declaramos el cuadrado y asignamos su area
  Cuadrado(int lado): this.lado=lado, this.area=lado*lado;

  @override
  String toString() {
    return 'Cuadrado: Lado: $lado, Area: $area';
  }
}

---------------------------------------
import 'pathToClass/Cuadrado.dart';
main() {
  final peque = new Cuadrado(5);
  print(peque); //  imprime: Cuadrado: Lado: 5, Area: 25
}
```

### Constructores constantes

```dart
class Cuadrado {
  final int lado;
  final int area;
  //constructor constante
  const Cuadrado(int lado): this.lado=lado, this.area=lado*lado;

  @override String toString() { return 'Cuadrado: Lado: $lado, Area: $area'; }

}

main() {
  // crea dos objetos diferentes (apunta a dos ref diferentes)
  final peque1 = new Cuadrado(5);
  final peque2 = new Cuadrado(5);

  // si tiene los mismo valores no creara otro, apuntara a la misma referencia
  final grande1 = const Cuadrado(25);
  final grande2 = const Cuadrado(25);

  // tiene diferentes asi que crea otro espacio de memoria
  final mediano1 = const Cuadrado(15);
  final mediano2 = const Cuadrado(13);

  print(peque1 == peque2);
  print(grande1 == grande2);
  print(mediano1 == mediano2);
}
```

### Constructores Factory (palabra reservada factory)

Este nos sirve para construir un objeto, desde otro constructor lo cual
nos ayuda a tomar deciciones de forma mas rapida, por ejemplo, abajo
estamos creando rectangulos, los cuales automaticamente se infiere su
tipo ya sea cuadrado o rectangulo.

```dart
class Rectangulo {
  int alto;
  int ancho;
  String tipo;

  factory Rectangulo(int ancho, [int alto]) {
    if(alto==null ||  ancho == alto) {
       return Rectangulo._cuadrado(ancho);
    } else {
      return Rectangulo._rectangulo(ancho, alto);
    }
  }

  Rectangulo._cuadrado(int anchoIn) {
    this.ancho = anchoIn;
    this.alto = anchoIn;
    this.tipo = 'Cuadrado';
  }

  Rectangulo._rectangulo(int anchoIn, int altoIn) {
    this.ancho = anchoIn;
    this.alto = altoIn;
    this.tipo = 'Rectangulo';
  }

  @override String toString() { return '${alto}AnchoX${ancho}Alto = $tipo'; }
}

main() {
  var uno = new Rectangulo(10); print(uno); //       10Ancho X 10Alto = Cuadrado
  var dos = new Rectangulo(10, 10); print(dos); //   10Ancho X 10Alto = Cuadrado
  var tres = new Rectangulo(10, 15); print(tres); // 15Ancho X 10Alto = Rectangulo
}
```

### Estaticos

```dart
class Herramientas {
  static int limite = 10; // si cambia en una instancia cambia en todas
  // si no ledamos el const la lista podra cambiarse, con final solo se protege la ref.
  static const List<String> lista = ['Martillo', 'Pinzas', 'Tornillo'];

  static void diHola() {
    print('Hola');
  }
}

main() {
  print('Limite de herramientas: ${Herramientas.limite}');
  //Herramientas.lista.add('algo'); // error en tiempo de ejecucion
  Herramientas.diHola();
}
```

### Patron Singleton

```dart
/** Sumpongamos que tenemos esta clase que hace peticiones y en runTime puede cambiar
 *  de version (por lo que no podemos usar const), y debenmos de tener la misma url
 * en toda nuestra aplicacion
 */
class MiService {
  String url = 'algo.com/api';

  request() {
    print('peticion a $url');
  }
}

class MiServiceSingleton {
  
  static final MiServiceSingleton _singleton = new MiServiceSingleton._internal();

  //CUIDADO: con la estructura del factory me equivoque con  "factory() {..." no defini el nombre de la clase
  factory MiServiceSingleton() {
    return _singleton;
  }

  MiServiceSingleton._internal();

  String url = 'otra.com/api';
  request() {
    print('peticion a $url');
  }
}

main() {
  final s1 = new MiService();
  s1.url = 'algo.com/api/v2/';
  s1.request();               //peticion a algo.com/api/v2/
  //--------------------------
  final s2 = new MiService();
  // Error: peticion a otro lado, por que es otro objeto
  s2.request();               // "peticion a algo.com/api"   apunta a una url diferente

  print("-.---------------------------");

  final ss1 = new MiServiceSingleton();
  ss1.url = 'otra.com/api/v2/';
  ss1.request();              // "peticion a otra.com/api/v2/"
  //--------------------------
  final ss2 = new MiServiceSingleton();
  // A pesar de usar objetos "diferentes" el cambio afecta en ambos, esto por que
  //realmente estamos usando el mismo objeto
  ss2.request();              // "peticion a otra.com/api/v2/"
  
  print(ss1==ss2);//true
}
```

### Clases Abstractas, herencia, override y super

Igual que java

### Mixin

Sirven para pasarle metodos a una clase y se pueden usar mas de uno

- Los mixin no pueden instanciarse igual que los abtractos, pero no
  tendran constructores

```dart
mixin Log {
  info(String msg) { print('Info: $msg'); }
  debug(String msg) { print('debug: $msg'); }
}
//----------------------------------------------
mixin Security {
  String getToken() { return 'secreto'; }
}
//----------------------------------------------
abstract class Service with Log, Security {
  static const url = 'algo.com/api';
  String token;

  Service() {
    this.token = getToken();
  }

  String postRequest(String dataToSend);
  
}
//----------------------------------------------
class ServiceClients extends Service {
  String path = Service.url + '/clientes';

  String postRequest(String dataToSend) {
    debug("Enviando $dataToSend a $path con token $token...");
    info('Token nuevo: ${getToken()}' );
    return "--$dataToSend--";
  }
}
//----------------------------------------------
main() {
  final sc = new ServiceClients();
  final resp = sc.postRequest("datoa");
  print(resp);
}
```

## Proyectos

<!-- crear un nuevo proyecto -->

**Objetivo:** Vamos a crear un proyecto que consumira una api y haremos
unas cosas con esta info.

A.  Desde consola ejecutar `dart create -t console-full myApp`

> O tambien Podemos en vsCode pulsar `ctrl+shift+p` y seleccionar
> `dart: new Proyect` y seleccionar `console project` el cual nos
> creara un proyecto

Nos creara una estructura como la de abajo

```r
./myApp/
  .dart_tool/
    package_config.json  # dependencias (no tocar)
  bin/
    myApp.dart # Main
  lib/
    myApp.dart # Diferentes partes de nuestra app
  test/
    myApp_test.dart # test unitarios
  .gitignore
  .packages           # dependencias (no tocar)
  analysis_options.yaml # Como analizar nuestro codigo (no se paque???)
  CHANGELOG.md      # Registro de cambios en dependencias y parecidos
  pubspec.lock      # Dependencias (no tocar)
  pubspec.yaml      # dependencias
  README.md
```

B. Ejecutar proyecto `dart bin/myApp.dart` <!-- iniciar p`royecto -->

C. Buscar paquetes: pub.dev -> Buscar "http" -> [http](https://pub.dev/packages/http/install)

D. Instalar dependencia, vamosa a `pubspec.yaml` y agregamos como nos
indica la documentacion

```yml
dependencies:
  http: ^0.12.2
```

E. Vamos a consultar la APIWeb `https://reqres.in/api/users`

<!-- consumir API, hacer peticion, relaizar peticion-->

**IMPORTANTE:** No es comun usar esta forma y/o no es recomendado, pero
nos puede sacar del apuro, es mejor usar objetos para capturar sus datos

```dart
import 'dart:convert' as convert; // es recomendable primero los paquetes del coree
import 'package:http/http.dart' as http; // luego paquetes fuera del core

import 'package:myApp/myApp.dart' as my_app; // al final nuestros paquetes

Future<void> main(List<String> arguments) async {
  var resp = await http.get('https://reqres.in/api/users');
  final body = convert.jsonDecode(resp.body);
  print(body);
  print('body.page: ${body['page']}');
  print('body.page: ${body['data'][2]['email']}');
}
void main() {
  http.get('https://reqres.in/api/users').then((resp) {
    final body = convert.jsonDecode(resp.body);
    print(body);
    print('body.page: ${body['page']}');
    print('body.page: ${body['data'][2]['email']}');
  });
}
```

**Resultado:** Podemos hacer una peticion y extraer sus datos.

### Convertir peticion a Objeto

Recordar, necesitamos la sig, dependencia

```yml
dependencies:
  http: ^0.12.2
```

<!-- convertir json a objeto, pasar de json a objeto, pagina para convertir de json a objetos-->

A. Vamos a [quicktype](https://app.quicktype.io/) y seleccionamos dart
y le pegamos el json de la API

```json
{"page":1,"per_page":6,"total":12,"total_pages":2,"data":[
  {"id":1,"email":"george.bluth@reqres.in","first_name":"George","last_name":"Bluth",
  "avatar":"https://reqres.in/img/faces/1-image.jpg"},{"
  id":2,"email":"janet.weaver@reqres.in","first_name":"Janet","last_name":"Weaver",
  "avatar":"https://reqres.in/img/faces/2-image.jpg"},
  {"id":3,"email":"emma.wong@reqres.in","first_name":"Emma","last_name":"Wong",
  "avatar":"https://reqres.in/img/faces/3-image.jpg"},
  {"id":4,"email":"eve.holt@reqres.in","first_name":"Eve","last_name":"Holt",
  "avatar":"https://reqres.in/img/faces/4-image.jpg"},
  {"id":5,"email":"charles.morris@reqres.in","first_name":"Charles","last_name":"Morris",
  "avatar":"https://reqres.in/img/faces/5-image.jpg"},
  {"id":6,"email":"tracey.ramos@reqres.in","first_name":"Tracey","last_name":"Ramos",
  "avatar":"https://reqres.in/img/faces/6-image.jpg"
}],
"support":{"url":"https://reqres.in/#support-heading",
"text":"To keep ReqRes free, contributions towards server costs are appreciated!"}
}
```

B. Construira un codigo como el siguiente

```dart
import 'dart:convert';

class UsersPage {

// Properties
  int page;
  int perPage;
  int total;
  int totalPages;
  List<User> users;

//Constructor
  UsersPage({ this.page, this.perPage, this.total, this.totalPages, this.users });

// Factoryes
  factory UsersPage.fromJson(Map<String, dynamic> json) => UsersPage(
      page: json['page'],
      perPage: json['per_page'],
      total: json['total'],
      totalPages: json['total_pages'],
      users: List<User>.from(json['data'].map((x) => User.fromJson(x))),
  );


// Utils
  Map<String, dynamic> toJson() => {
      'page': page,
      'per_page': perPage,
      'total': total,
      'total_pages': totalPages,
      'users': List<dynamic>.from(users.map((x) => x.toJson())),
  };

  // este es el que resivira el json resivido
  static UsersPage usersPageFromJson(String str) {
    return UsersPage.fromJson(json.decode(str));
  } 

  static String usersPageToJson(UsersPage users) {
    return json.encode(users.toJson());
  }
}


class User {
    int id;
    String email;
    String firstName;
    String lastName;
    String avatar;

// Constructors
    User({ this.id, this.email, this.firstName, this.lastName, this.avatar, });

// Utils
    factory User.fromJson(Map<String, dynamic> json) => User(
      id: json['id'],
      email: json['email'],
      firstName: json['first_name'],
      lastName: json['last_name'],
      avatar: json['avatar'],
    );

    Map<String, dynamic> toJson() => {
      'id': id,
      'email': email,
      'first_name': firstName,
      'last_name': lastName,
      'avatar': avatar,
    };
}

```

C. Podemos llamar el metodo para hacerlo mas facil

```dart
import 'dart:convert' as convert; // es recomendable primero los paquetes del coree
import 'package:http/http.dart' as http; // luego paquetes fuera del core
import 'package:myApp/model/UsersPage.dart';

import 'package:myApp/myApp.dart' as my_app; // al final nuestros paquetes

void main() {
  http.get('https://reqres.in/api/users').then((resp) {
    //final body = convert.jsonDecode(resp.body);
    //var userPage = UsersPage.fromJson(body);
    var userPage = UsersPage.usersPageFromJson(resp.body);
    print(userPage.users[1].email); // janet.weaver@reqres.in
  });
}
```

**Resultado:** Tenemos la peticion convertida a un objeto de una forma
sencilla y automatica.
