# Flutter Introduccion

## Accesos Rapidos

- Links
  - [pagina Oficial: flutter.dev](https://flutter.dev/)
  - [Documentacion Material: widgets/material](https://flutter.dev/docs/development/ui/widgets/material)
  - [Iconos material.io](https://material.io/resources/icons/)
  - [quicktype, Convertir de json a clases](https://app.quicktype.io/)
  - [Explorador SQLite](https://sqlitebrowser.org/dl/)
  - [Libreria para SQL](https://pub.dev/packages/sqflite#-readme-tab-)
  - [Manejo de estado/UI](https://pub.dev/packages/provider)
  - [Abrir navegador Web](https://pub.dev/packages/url_launcher)
  - [Manejo de GoogleMaps](https://pub.dev/packages/google_maps_flutter/install)
  - [Generar codigos QR Online](https://www.qrcode.es/es/generador-qr-code/)
  - [manejo de shared preferences](https://pub.dev/packages/shared_preferences/install)
  - [manejar streams rxdart](https://pub.dev/packages/rxdart)
  - [Como funcionan los Stream explicacion visual](https://rxmarbles.com/)
  - [image picker](https://pub.dev/packages/image_picker)
  - [Servicio para subir imagenes](https://cloudinary.com/documentation/image_upload_api_reference)
  - [Util para poder subir imagenes](https://pub.dev/packages/mime_type#-readme-tab-)
  - [Guia para subir a AppStores](./img/_10_guia_de_despliegues_AppStores.pdf)
  - [Plugin/Libreria para notificaciones](https://pub.dev/packages/firebase_messaging)

## Pendientes

- Checar mas como evitar repintado inecesarios, empezando por [aqui](https://stackoverflow.com/questions/64527001/body-of-a-scaffold-is-rebuilt-when-opening-a-drawer-menu)

## Indice

<!-- 
Ya estan referenciados
 - #70-navegar-con-rutas-con-nombre
 - #187-shared-preferences-y-cargar-informacion-antes-de-que-inicie-la-applicacion
-->

- [Flutter Introduccion](#flutter-introduccion)
  - [Accesos Rapidos](#accesos-rapidos)
  - [Pendientes](#pendientes)
  - [Indice](#indice)
  - [Resultados Finales](#resultados-finales)
    - [1. Aplicacion contador](#1-aplicacion-contador)
    - [2. App Widgets](#2-app-widgets)
    - [3. AppPeliculas](#3-apppeliculas)
    - [4. Disenios](#4-disenios)
    - [5. SQLite-QR-Mapas](#5-sqlite-qr-mapas)
    - [6. Preferencias de Usuario](#6-preferencias-de-usuario)
    - [7. Formulario Login con validaciones usando BLoC, Streams e InheritedWidget](#7-formulario-login-con-validaciones-usando-bloc-streams-e-inheritedwidget)
    - [8. Crud de producto con imagen API-REST](#8-crud-de-producto-con-imagen-api-rest)
    - [9. Registro y Login contra Firebase](#9-registro-y-login-contra-firebase)
    - [10. Enviar y recibir notificacion](#10-enviar-y-recibir-notificacion)
    - [11. App Noticias multiples pestanias y conservar scroll](#11-app-noticias-multiples-pestanias-y-conservar-scroll)
  - [Errores](#errores)
    - [El Drawer re-pinta el widget al abrir o cerrar](#el-drawer-re-pinta-el-widget-al-abrir-o-cerrar)
    - [Error al hacer peticiones en release](#error-al-hacer-peticiones-en-release)
    - [Error no se pueden hacer peticiones http](#error-no-se-pueden-hacer-peticiones-http)
    - [No inicia la app MissingPluginException](#no-inicia-la-app-missingpluginexception)
  - [Introduccion](#introduccion)
    - [Plugions de VsCode](#plugions-de-vscode)
    - [Instalar Flutter](#instalar-flutter)
    - [Crear proyecto](#crear-proyecto)
    - [39. Estructura de un proyecto en Flutter](#39-estructura-de-un-proyecto-en-flutter)
    - [Conceptos de Flutter](#conceptos-de-flutter)
  - [Aplicacion Contador](#aplicacion-contador)
    - [Widget Scaffold](#widget-scaffold)
    - [Columnas y estilos](#columnas-y-estilos)
    - [51. Contador problema del StatelessWidget y StatefulWidget](#51-contador-problema-del-statelesswidget-y-statefulwidget)
    - [53. Creando más acciones en nuestro contador](#53-creando-más-acciones-en-nuestro-contador)
  - [Seccion 6: Componentes](#seccion-6-componentes)
    - [60. Inicio de proyecto](#60-inicio-de-proyecto)
    - [Crear Items de lista de forma dinamica](#crear-items-de-lista-de-forma-dinamica)
    - [crear menu-lista - Agregar recurso (json, imagen...) a flutter / crear menu](#crear-menu-lista---agregar-recurso-json-imagen-a-flutter--crear-menu)
    - [crear menu-lista - Convertir JSON a lista de objetos](#crear-menu-lista---convertir-json-a-lista-de-objetos)
    - [crear menu-lista - Construir vista a partir de un Future con FutureBuilder](#crear-menu-lista---construir-vista-a-partir-de-un-future-con-futurebuilder)
    - [68. Utilidades - Obtener un ícono en funcion de un string](#68-utilidades---obtener-un-ícono-en-funcion-de-un-string)
    - [69. Navegar a una nueva pantalla en Flutter](#69-navegar-a-una-nueva-pantalla-en-flutter)
    - [70. Navegar con rutas con nombre](#70-navegar-con-rutas-con-nombre)
    - [72. Pasar las rutas a su propio archivo y crear CardWidget](#72-pasar-las-rutas-a-su-propio-archivo-y-crear-cardwidget)
    - [84. RESUMEN: agregar pantalla](#84-resumen-agregar-pantalla)
    - [73. Cargar imagen de la red y animacion de carga](#73-cargar-imagen-de-la-red-y-animacion-de-carga)
    - [76. Widget - Alert](#76-widget---alert)
    - [78. Animaciones con Container y AnimatedContainer](#78-animaciones-con-container-y-animatedcontainer)
    - [79-80. inputFields, email y password](#79-80-inputfields-email-y-password)
    - [81. selec. Fecha - Datepicker](#81-selec-fecha---datepicker)
    - [82. selec. Fecha - Cambiar el idioma del Datepicker](#82-selec-fecha---cambiar-el-idioma-del-datepicker)
    - [83. Dropdown y DropdownMenuItem](#83-dropdown-y-dropdownmenuitem)
    - [84.85. Slider - Checkbox y Switches](#8485-slider---checkbox-y-switches)
    - [86-87. InfiniteScroll - Basico con Listview Builder](#86-87-infinitescroll---basico-con-listview-builder)
    - [89. InfiniteScroll - Agregar animacion de carga](#89-infinitescroll---agregar-animacion-de-carga)
    - [89. Pull to refresh](#89-pull-to-refresh)
    - [RESUMEN DE LA SECCION](#resumen-de-la-seccion)
  - [Sección 7:Aplicación de películas](#sección-7aplicación-de-películas)
    - [97. Crear un swiper, e inicio de app](#97-crear-un-swiper-e-inicio-de-app)
    - [98.  Widget personalizado - CardSwiper](#98--widget-personalizado---cardswiper)
    - [99. Consumir servicio - TheMovieDB, ApiKey y documentación](#99-consumir-servicio---themoviedb-apikey-y-documentación)
    - [102. Mostrar póster de películas en el Swiper](#102-mostrar-póster-de-películas-en-el-swiper)
    - [Swipe Horizontal tradicional](#swipe-horizontal-tradicional)
    - [108. TEORIA: Patrón Bloc y al manejo de estado de la aplicación](#108-teoria-patrón-bloc-y-al-manejo-de-estado-de-la-aplicación)
    - [109. Scroll-Infinito con Streams](#109-scroll-infinito-con-streams)
    - [111. Optimizaciones para nuestra aplicación, uso de builder](#111-optimizaciones-para-nuestra-aplicación-uso-de-builder)
    - [112. Seleccionar item: Pasar argumentos de una página a otra usando el PushNamed](#112-seleccionar-item-pasar-argumentos-de-una-página-a-otra-usando-el-pushnamed)
    - [113. Seleccionar item: Header bonito](#113-seleccionar-item-header-bonito)
    - [114. Seleccionar item: Lista de actores](#114-seleccionar-item-lista-de-actores)
    - [118. Seleccionar item: Hero Animation](#118-seleccionar-item-hero-animation)
    - [120. BuscarPelicula: Basico](#120-buscarpelicula-basico)
    - [123. BuscarPelicula: Buscar en themoviedb.org](#123-buscarpelicula-buscar-en-themoviedborg)
  - [Seccion 8: Diseños en Flutter](#seccion-8-diseños-en-flutter)
    - [Diseño basico](#diseño-basico)
    - [Diseño medio: imagen de fondo completa con boton para hacer scroll](#diseño-medio-imagen-de-fondo-completa-con-boton-para-hacer-scroll)
    - [Diseño Avanzado: gradientes](#diseño-avanzado-gradientes)
  - [Sección 9: QRScanner - SQLite](#sección-9-qrscanner---sqlite)
    - [149. Tema de la aplicación (estilos globales)](#149-tema-de-la-aplicación-estilos-globales)
    - [150. Cambiar Panel: Definiendo escenario/problema](#150-cambiar-panel-definiendo-escenarioproblema)
    - [151. Cambiar Panel: Solucion con gestor de estado provider](#151-cambiar-panel-solucion-con-gestor-de-estado-provider)
    - [154. Leer un código QR](#154-leer-un-código-qr)
    - [156. Modelo para manejar los Scans](#156-modelo-para-manejar-los-scans)
    - [157. SQFLite Crear DB y Tabla](#157-sqflite-crear-db-y-tabla)
    - [159. SQFLite - Crear registros](#159-sqflite---crear-registros)
    - [160. SQFLite - Obtener registros (byId, byType, All)](#160-sqflite---obtener-registros-byid-bytype-all)
    - [161. SQFLite - UpdateById, DeleteById, DeleteAll](#161-sqflite---updatebyid-deletebyid-deleteall)
    - [163. ScanProvider - Centralizar la información de los Scans](#163-scanprovider---centralizar-la-información-de-los-scans)
    - [167. Borrar todos los Scans](#167-borrar-todos-los-scans)
    - [168. Borrar un Scan de forma independiente](#168-borrar-un-scan-de-forma-independiente)
    - [170. Abrir Navegador web URL](#170-abrir-navegador-web-url)
    - [172. Crear proyecto de GoogleMaps](#172-crear-proyecto-de-googlemaps)
    - [174. Personalizar mapa](#174-personalizar-mapa)
  - [Sección 10: Preferencias de usuario](#sección-10-preferencias-de-usuario)
    - [187. Shared Preferences y cargar informacion ANTES DE QUE INICIE LA APPLICACION](#187-shared-preferences-y-cargar-informacion-antes-de-que-inicie-la-applicacion)
    - [Cambiar BarColor en runtime](#cambiar-barcolor-en-runtime)
    - [Guardar ultima pantalla abierta](#guardar-ultima-pantalla-abierta)
  - [Seccion 11: Validación de formularios - Patrón Bloc](#seccion-11-validación-de-formularios---patrón-bloc)
    - [Diseño del formulario](#diseño-del-formulario)
    - [199. LoginBloc - Controlar los campos del formulario](#199-loginbloc---controlar-los-campos-del-formulario)
    - [201. InheritedWidget](#201-inheritedwidget)
    - [202. Conectar los inputs con los Streams en el InheritedWidget](#202-conectar-los-inputs-con-los-streams-en-el-inheritedwidget)
    - [204. StreamTransformer y Validaciones](#204-streamtransformer-y-validaciones)
    - [205. Des/Habilitar boton Combinar Streams](#205-deshabilitar-boton-combinar-streams)
    - [206. Obtener el último valor emitido por un stream](#206-obtener-el-último-valor-emitido-por-un-stream)
    - [207. Mantener la data de los streams después de un hot reload](#207-mantener-la-data-de-los-streams-después-de-un-hot-reload)
    - [Extra: Ventaja de usar InheritedWidget](#extra-ventaja-de-usar-inheritedwidget)
  - [CRUD hacia servicios REST, uso de cámara y galería de imágenes](#crud-hacia-servicios-rest-uso-de-cámara-y-galería-de-imágenes)
    - [213. Diseño de la pantalla de detalle de producto](#213-diseño-de-la-pantalla-de-detalle-de-producto)
    - [214.116 Validación de formularios - FormWidget](#214116-validación-de-formularios---formwidget)
    - [215. Modelo para manejar los productos](#215-modelo-para-manejar-los-productos)
    - [217. Preparar nuestra base de datos - Firebase](#217-preparar-nuestra-base-de-datos---firebase)
    - [219-220-221. Cargar, Eliminar y mostrar productos de Firebase](#219-220-221-cargar-eliminar-y-mostrar-productos-de-firebase)
    - [222. Editar un producto](#222-editar-un-producto)
    - [223. Bloquear botón y mostrar Snackbar](#223-bloquear-botón-y-mostrar-snackbar)
    - [225. Seleccionar y mostrar una fotografía](#225-seleccionar-y-mostrar-una-fotografía)
    - [229. Backend - Subir fotografías](#229-backend---subir-fotografías)
    - [232. Mostrar imágenes cargadas](#232-mostrar-imágenes-cargadas)
  - [Sección 13:Login y manejo de Tokens](#sección-13login-y-manejo-de-tokens)
    - [242. Usar token para validar peticiones en Firebase](#242-usar-token-para-validar-peticiones-en-firebase)
  - [Detalles finales de la aplicación de productos (uso de objetos en Streams)](#detalles-finales-de-la-aplicación-de-productos-uso-de-objetos-en-streams)
  - [Sección 15:Despliegue en Google PlayStore y Apple AppStore](#sección-15despliegue-en-google-playstore-y-apple-appstore)
    - [Cambiar el icono de la Aplicacion](#cambiar-el-icono-de-la-aplicacion)
    - [256. Configurar el splashscreen](#256-configurar-el-splashscreen)
    - [Construir APK](#construir-apk)
    - [................PENDIENTE](#pendiente)
  - [Sección 16:Push notifications](#sección-16push-notifications)
    - [266. Configuración de FCM (Firebase Cloud Messaging)](#266-configuración-de-fcm-firebase-cloud-messaging)
    - [267. Cambiar el ID de la aplicación - Android](#267-cambiar-el-id-de-la-aplicación---android)
    - [268. Provider para controlar las notificaciones y FCM Token](#268-provider-para-controlar-las-notificaciones-y-fcm-token)
    - [269. Recibir notificación - onMessage, onLaunch y onResume](#269-recibir-notificación---onmessage-onlaunch-y-onresume)
    - [270. Reaccionar cuando recibimos una notificación](#270-reaccionar-cuando-recibimos-una-notificación)
    - [271. Uso del Navigator en el initState](#271-uso-del-navigator-en-el-initstate)
    - [Notas finales de la seccion](#notas-finales-de-la-seccion)
  - [Sección 17:Provider - Aplicación de noticias](#sección-17provider---aplicación-de-noticias)
    - [283. NewsAPI - Obtener noticias del mundo](#283-newsapi---obtener-noticias-del-mundo)
    - [289. Mantener Scroll al navegar entre paginas](#289-mantener-scroll-al-navegar-entre-paginas)
    - [290. Menu superior lista de categorias](#290-menu-superior-lista-de-categorias)
    - [293. Cambiar el color de la categoría seleccionada](#293-cambiar-el-color-de-la-categoría-seleccionada)
    - [Cargar categorias de API](#cargar-categorias-de-api)
  - [Paquete propio de Mapbox](#paquete-propio-de-mapbox)
    - [.................. SECCION SALTADA](#-seccion-saltada)
  - [FIN DEL CURSO](#fin-del-curso)
  - [Por el camino](#por-el-camino)
    - [Logs](#logs)
  - [Flutter 2](#flutter-2)
    - [312. Reconstruir proyectos de IOS, Android y Web](#312-reconstruir-proyectos-de-ios-android-y-web)
    - [314. Flutter Fix](#314-flutter-fix)
    - [315. Null-Safety](#315-null-safety)
    - [317. Dart migrate](#317-dart-migrate)

<!--

#########################################

-->

----

<!--

#########################################

-->

## Resultados Finales

### 1. Aplicacion contador

En esta app vemos lo basico, con un contador el cual podemos aumentar,
reducir y reiniciar

- [.GIF](./img/07_app_contador.gif)
- [.zip](./img/08_unoX.zip)

### 2. App Widgets

Vemos diferentes Widgets, como son los dialogs, avatars, imagen con animacion
de carga, Cards, animaciones, inputs (box, password, datepicker, select, sliders/rangos),
y como hacer un scroll infinito basico como arriba y para abajo (sin streams)

- [.GIF](./img/_2_app_widgets.gif)
- [.zip](./img/_2_app_widgets.zip)

### 3. AppPeliculas

Vemos 2 tipos de swipes horizontales diferentes, uno de ellos con scroll
infinito usando Streams, visualizar detalles con una trancicion Hero,
un buscador que igual lleva a detalles, y Scroll dentro de otro Scroll.

- [.GIF](./img/_3_peliculas.gif)
- [.zip](./img/_3_peliculas2.zip)

### 4. Disenios

Vemos como hacer 3 diseños para mejorar la construccion de estos

- [Imagen](./img/_4_disenios.JPG)
- [.zip](./img/_4_disenio.zip)

### 5. SQLite-QR-Mapas

usamos una librearia para escanear codigos QR, y persistimos los datos
realizando un CRUD con SQLite, y dependiendo si es un enlace o cordenadas
abrimos el navegafor web o abrimos el mapa. Tambien vimos como eliminar
un registro haciendo dismis con Dismissible. Tambien personalizamos el
mapa para agregar markers, cambiar la posicion, y tipo visualizacion.

- [Gif](./img/_5_sqlite_QR_mapas.gif)
- [Codigo](./img/_5_sqlite_QR_mapas.zip)

### 6. Preferencias de Usuario

Vimos como persistir informacion tipo propiedad/valor, para no complicarse y
tambien como obtener esta informacion al inicio de la app, para traer
datos de forma asincrona y tenener que construir widgets de forma cmpleja.

- [Gif](./img/_6_preferencias_de_usuario.gif)
- [Condigo](./img/_6_preferencias_de_usuario.zip)

### 7. Formulario Login con validaciones usando BLoC, Streams e InheritedWidget

Vimos el uso del patron BLoC con InheritedWidget, lo cual nos permitio
compartir el BLoC de nuestro login en toda nuestra aplicacion, permitiendo
usar sus streams y valores de forma facil, en cualquier parte de nuestra
aplicacion gracias al InheritedWidget que permite compartir desde la Raiz
de nuestro arbol.

- [Gif](./img/_7_validacion_formu_login_BLoC_streams.gif)
- [Codigo](./img/_7_validacion_formu_login_BLoC_streams.zip)

### 8. Crud de producto con imagen API-REST

Vimos la creacion de un CRUD de produtos que usa API-WEB, para hacer
dichas opraciones, las cuales incluye su imagen del producto.

- [Gif](./img/_8_crud_productos_con_imagen.gif)
- [Codigo](./img/_8_crud_productos_con_imagen.zip)

### 9. Registro y Login contra Firebase

Aqui vimos como usar la API de firebase para hacer login y la interaccion
con la App, para mostrar el error pertinente o dejarlo pasar.

- [Gif](./img/_9_login_y_registro_en_firebase.gif)
- [Codigo](./img/_9_login_y_registro_en_firebase.zip)

### 10. Enviar y recibir notificacion

Vimos como usar el servicio de Firebase y su plugin para flutter, para
enviar y recibir notificaciones con sonido, incluso cuando la aplicacion
esta terminada.

- [Gif](./img/_11_enviar_y_recibir_notificacion.gif)
- [Codigo](./img/_11_enviar_y_recibir_notificacion.zip)

### 11. App Noticias multiples pestanias y conservar scroll

- [Gif](./img/_12_news_seleccionar_categoria_y_mostrar_contenido_conjelar_scroll.gif)
- [Codigo](./img/_12_news_seleccionar_categoria_y_mostrar_contenido_conjelar_scroll.zip)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Errores

### El Drawer re-pinta el widget al abrir o cerrar

<!--
problemas con el drawer, error con el drawer, el metodo build se llama solo
se hace un rebuild sin solicitarlo, se llama el metodo build solo.
-->

- Fuente
  - [checar link](https://stackoverflow.com/questions/64527001/body-of-a-scaffold-is-rebuilt-when-opening-a-drawer-menu)

**Descripcion**: Cada ves que el usuario habre/despliega o cierra/esconde
el drawer, el metodo build se llama de nuevo, y esto provoca que se
haga una nueva peticion http, lo cual puede cambiar el contenido o ser
un problema de rendimiento.
**Escenario:** Tenemos el siguiente codigo

```dart
class CustomScaffoldState extends State<CustomScaffold> {
Widget build(BuildContext context) {
return Scaffold(
  drawer: mySideMenu(widget.username),
  body: FutureBuilder(
    future: requestDataFromHttp(),
    builder: (context, snapshot) {
      if (snapshot.hasData) { /*show info from request*/ }
      if (snapshot.hasError) { /* show error */ }
      else{ /*show isLoading*/ }
    }
  ));
}
```

**Causa**: El drawer tiene este comportamiento ya que se da por sentado
qeue, el metodo build no tiene efectos secundarios (mas info en fuente),
por lo que es un error de nuestra parte
**Solucion**: Hacegurar que nuestro metodo buid no tenga efectos, secundarios,
en este caso debemos de pasar que se inicie la peticion en el `initState` y
no en el build, de la siguiente manera, donde podemos ver que.

- Lo pasamos a un statefull para poder usar el `future` en ambos metodos
- el future lo iniciamos en el `initState` y luego este, es usado en el
  metodo build, asi evitando que este lo inicie.

```dart
class Example extends StatefulWidget { @override  _ExampleState createState() => _ExampleState(); }

class _ExampleState extends State<Example> {
  Future future;

  @override void initState() {
    future = requestDataFromHttp();
    super.initState();
  }

  @override Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) { /*show info from request*/ }
        if (snapshot.hasError) { /* show error */ }
        else{ /*show isLoading*/ }
      }
    );
  }
}
```

### Error al hacer peticiones en release

Debemos de agregar los permisos en ambos manifests, no solo en uno como
es por defecto TODO: agregar los archivos.

1, `android\app\src\profile\AndroidManifest.xml`

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android" package="com.example.cdmt_flutter_backoffice">
<!-- esta ya se encuentra aqui al inicio -->
!   <uses-permission android:name="android.permission.INTERNET"/>
</manifest>
```

2, `android\app\src\main\AndroidManifest.xml`

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android" package="com.example.cdmt_flutter_backoffice">
<!-- esta es la que agregamos -->
+   <uses-permission android:name="android.permission.INTERNET"/>    
    <application android:name="io.flutter.app.FlutterApplication" 
```

----

### Error no se pueden hacer peticiones http

**Error**: Insecure HTTP is not allowed by platform.  
**Descripcion:** Lanza este error al intentar hacer una peticion http.  
**Causa:** La configuracion de la app de android por defecto no permite.  
comunicacion http, solo https.  
**Fuente:** [stackoverflow](https://stackoverflow.com/questions/64197752/bad-state-insecure-http-is-not-allowed-by-platform).  
**Solucion:** Agregar configuracion para permitir peticiones http, en
el manifest.

Paso 1. En `android/app/src/main/AndroidManifest.xml` agregar

```xml
<!-- inportante que exita esta -->
! <uses-permission android:name="android.permission.INTERNET" /> <!-- This Line -->

<application
        android:name="io.flutter.app.FlutterApplication"
        android:label="receipt"
+       android:usesCleartextTraffic="true" <!-- This Line -->
        android:icon="@mipmap/ic_launcher">
```

Paso 2. Para IOs `ios/Runner/info.plist`

```xml
<key>NSAppTransportSecurity</key>
<dict>
  <key>NSAllowsArbitraryLoads</key>
  <true/>
</dict>
```

----

### No inicia la app MissingPluginException

**Error:** /ui_dart_state.cc(186)] Unhandled Exception: MissingPluginException(No implementation found for method getAll on channel plugins.flutter.io/shared_preferences.  
**Descripcion:** Al levantar la app en debug sin ningun problema, pero
con el apk de produccion al abrir lanza este error haciendo que no inicie.  
**Causa:** Creo una actualizacion del api de android.  
**Fuente:** [stackoverflow](https://stackoverflow.com/questions/50687801/flutter-unhandled-exception-missingpluginexceptionno-implementation-found-for/66417139#66417139).  
**Solucion**: Seguir los pasos de la fuente.

Step 1: Update buildTypes in /android/app/build.gradle

```r
android {
    compileSdkVersion 30
    sourceSets { main.java.srcDirs += 'src/main/kotlin' }
    lintOptions { disable 'InvalidPackage' }
    defaultConfig { ... }
    signingConfigs { release { ... } }
    buildTypes {
+       debug {
+           proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
+       }
        release {
            // TODO: Add your own signing config for the release build.
            // Signing with the debug keys for now, so `flutter run --release` works.
            signingConfig signingConfigs.debug
            //signingConfig signingConfigs.release

+           minifyEnabled false
+           shrinkResources false
+           useProguard true
+           proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
}
```

Step 2: Add file android/app/proguard-rules.pro

```r
-keep class androidx.lifecycle.DefaultLifecycleObserver
```

Step 3: Update code\android\app\src\main\kotlin\com\example\code\MainActivity.kt

```js
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {
  override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
    GeneratedPluginRegistrant.registerWith(flutterEngine) // add this line
  }
}

```

**Resultado:** Ya no deberia de salir el error, tener cuidado de checar
si si se guarda la sesion al cerrar la app.

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Introduccion

- **AOT:** (Ahead of Time) Compilado rapido y predecible a codigo nativo
- **JIT:** (Just In Time) Compilado rapido y capacidad de Hot Reload
- **60fts:** Es pocible hacer animaciones a esta velocidad
- **Compilado a Codigo Nativo**: Compila a codigo maquina usando C++
  librerias de C++, lo cual no hay conexiones raras en medio.

### Plugions de VsCode

- Flutter
- Dart
- Awesome Flutter Snippets

### Instalar Flutter

- Fuente: [flutter.dev](https://flutter.dev/docs/get-started/install/)

**Nota:** Ya debemos de tener instalado dart

A. Seguimos instaucciones

- Descargar .zip para windows
- Descomprimir en cualquier carpeta proponen `C:\src\flutter`
- Añadir la variable de entorno de de bin/
- Ejecutar `flutter --version` <!-- en mi caso: Flutter 1.22.5 -->
- Ejecutar `flutter doctor` debe indicar que todo bien

<!--

#########################################

-->

----

<!--

#########################################

-->

### Crear proyecto

Fuente: [flutter.dev](https://flutter.dev/docs/get-started/test-drive?tab=terminal#create-app)

A. Ejecutamos: `flutter create myapp`

**Nota:** En mi caso ya tengo conectado el mi dispositivo y con el
depurador activo, por lo que al correr el comando de abajo me detecta
mi telefono.

**Nota2:** La primera vez purde tardar unos minutos.

B. Ejecutar proyecto: `flutter run` <!-- nos metemos a la carpeta que se genero-->

**Resultado:** Tenemos el demo que se crea de forma automatica, el cual
luciria de esta for

![01_first_run](./img/01_first_run.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 39. Estructura de un proyecto en Flutter

- **lib/** : En esta carpeta tendremos nuestro codigo

- **.metadata** : (no tocar) da informacion a flutter de la version y posibles actu. que pueda tener

- **.packages** : (no tocar) manejo de las dependencias de flutter

- **nombre_proyecto.iml** : (no tocar) tiene metadata del proyecto, el cual no vamos a tocar de
forma directa

- **pubspec.yaml** : es parecido al package.json donde manejaremos dependencias,
archivos estaticos (img, json, fuentes), cuando hagamos un cambio sobre este archivo
VS code auto. va a ejecutar el comando ```flutter packages get``` para manejar nuestras
dependencias/recursos, y en caso de que no lo haga podemos hacerlo manualmente. Y para
notar los cambios necesitaremos de un **full-restart** (boton de stop) .

- **android/ & ios/** : Haremos cambios aventuales para trabajar sobre una conf en especifico
de alguna plataforma.

<!--

#########################################

-->

----

<!--

#########################################

-->

### Conceptos de Flutter

**Widget:** En flutter la parte del UI todo es un widget, abajo podemos
ver como estaria construido por debajo los widgets de Texto y AppBar,
las cuales no son mas que clases en las cuales usamos sus constructores,
para meterle propiedades y haci ir personalizando cada elemento.

![02_widget](./img/02_widget.png)

**StatelessWidget y StateFullWidget**: Tenemos estos dos tipos, los cuales
son clases abstractas, los cuales nos ayudan a crear vistas con estado
y sin estado.

**Arbol de Widgets**: Este es parecido al DOM de html, donde tenemos un
elemento dentro de otro, y podemos navegar esta anidacion apoyandonos
de los nodos mas cercanos

![03_arbol_de_widgets](./img/03_arbol_de_widgets.png)

**Material**: Es una de las librerias que podemos, que traen los Widgets
que necesitamos, ahi otros pero este es de los mas populares.

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Aplicacion Contador

- Enlaces
  - [Documentacion MaterialApp class](https://api.flutter.dev/flutter/material/MaterialApp-class.html)

**Objetivo:** Re-crearemos la aplicacion que sale por defecto al crear
un nuevo proyecto, pero le agregaremos dos tres cosas para entender los
conceptos de flutter.

A. El Main

- `material.dart` es donde tenemos los widgets de fluter con un diseño
  de google, ahi otros paquetes para que tenga un diseño de IOs, pero eso
  luego lo veremos.
- `mtl.runApp();`: El que inicia nustra app y le entra un Widget
- `mtl.MaterialApp`: Podemos usar este como contenedor root que puede
  contener elementos dentro de la pantalla del telefono

```dart
import 'package:flutter/material.dart' as mtl;

void main() {
  mtl.runApp(new WelcomePage());
}

class WelcomePage extends mtl.StatelessWidget {
  
  @override
  mtl.Widget build(mtl.BuildContext context) {
    // https://api.flutter.dev/flutter/material/MaterialApp-class.html
    return mtl.MaterialApp(
      home: mtl.Center(
        child: mtl.Text('Hola flutter'),
      )
    );
  }
}
```

**Resultado:** Podemos ver una pantalla con un hola

![4_hola_mundo.PNG](./img/04_hola_mundo.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Widget Scaffold

**Intro:** Este nos permite crear pantallas que tienen un Titlebar,
cuerpo, y boton flotante

**Objetivo:** Vamos a re-estructurar par no hacer un desorden y agregar
un escafolding con las 3 partes que mensionamos.

A. Creamos `lib\src\screens\HomeScreen.dart` que contendra nuestra primera
pantalla

```dart
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  
  void onPressFloatingButtonAdd() {
    print('Boton precionado');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold( // https://api.flutter.dev/flutter/material/Scaffold-class.html
      appBar: AppBar(
        title: Text('Home'),
        centerTitle: true,
      ),
      body: Center (
        child: Text('Puerco'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => { onPressFloatingButtonAdd() } ,
        child: const Icon(Icons.add),
      ),
    );
  }
}
```

B, Extraemos el `MaterialApp` a su propio archivo `lib\src\App.dart`

```dart
import 'package:flutter/material.dart';
import 'package:uno/src/screens/HomeScreen.dart';

class App extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    // https://api.flutter.dev/flutter/material/MaterialApp-class.html
    return MaterialApp(
      home: Center(
        child: HomeScreen()
      )
    );
  }
}
```

C, El main quedaria haci `lib\main.dart` el cual solo usaria nuestro `MaterialApp`

```dart
import 'package:flutter/material.dart' as mtl;

import 'src/App.dart';

void main() {
  mtl.runApp(new App());
}
```

**Resultado** Tenemos una pantalla/codigo mas decente el cual podemos
empezar a agregar logica.

![05_scaffold](./img/05_scaffold.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Columnas y estilos

**Problema** Si vemos nuestro `HomeScreen` vemos que el componente `Center`
como muchos otros, solo aceptan un child como hijo, y nosotros queremos
poner 2, para solucionar esto hacemos uso de `columnas y filas` las
cuales aceptan multiples childs.

```dart
  import 'package:flutter/material.dart';
  class HomeScreen extends StatelessWidget {
    
    void onPressFloatingButtonAdd() {
      print('Boton precionado');
    }

    final textStyle = TextStyle(fontSize: 25);

    @override
    Widget build(BuildContext context) {
      return Scaffold( // https://api.flutter.dev/flutter/material/Scaffold-class.html
        appBar: AppBar(
          title: Text('Home'),
          centerTitle: true,
        ),
        body: Center (

#         child: Column( // https://api.flutter.dev/flutter/widgets/Column-class.html
+           mainAxisAlignment: MainAxisAlignment.center,
+           children: [
+             Text('Veces Pulsado', style: textStyle ),
+             Text('1', style: textStyle),
+           ],
+         )
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => { onPressFloatingButtonAdd() } ,
          child: const Icon(Icons.add),
        ),
      );
    }
  }
```

**Resultado:** Aprendimos a meter varios widgets como el hijo de otro,
en este caso usamo columnas para meter varios textos dentro de un center.

![06_calumnas](./img/06_calumnas.PNG)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 51. Contador problema del StatelessWidget y StatefulWidget

**Problema** Resulta que si agregamos una variable que queremos modificar
no vamos a poder por que nos obliga el `StatelessWidget` a solo usar
propiedades finales.

```dart
  import 'package:flutter/material.dart';
  class HomeScreen extends StatelessWidget {
+   final conteo = 10;
 
    // Controlador
    void onPressFloatingButtonAdd() {
      print('Boton precionado');
?     // conteo++; ERROR
    }

    // vista
    final textStyle = TextStyle(fontSize: 25);

    @override
    Widget build(BuildContext context) {
      return Scaffold( // https://api.flutter.dev/flutter/material/Scaffold-class.html
        appBar: AppBar(
          title: Text('Home'),
          centerTitle: true,
        ),
        body: Center (
          child: Column( // https://api.flutter.dev/flutter/widgets/Column-class.html
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Veces Pulsado', style: textStyle ),
#             Text('$conteo', style: textStyle),
            ],
          )
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat, // centrar floatinButton abajo
        floatingActionButton: FloatingActionButton(
          onPressed: () => { onPressFloatingButtonAdd() } ,
          child: const Icon(Icons.add), // https://material.io/resources/icons/
        ),
      );
    }
  }
```

**Solucion:** Debemos de transformar a `StateFulwidget`

**Problema:** Si el codigo de abajo precionamos el boton de incrementar,
este parece que no se incrementa, si lo hace pero no se esta dibujando,
entonces debemos de indicar que cuando `cambie la variable de redibuje`.

**Nota:** podemos ver el cambio con el hot reload de VSCode.

<!-- no se realizan los cambios, no cambia de valor, no hay cambios -->

```dart
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {

  // @override
  // Widget build(BuildContext context) {
  //   return //Pasamos lo mismo abajo
  // }

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State {
  var _conteo = 10;

  // Controlador
  void onPressFloatingButtonAdd() {
    print('Boton precionado');
    _conteo++;
  }

  // vista
  final _textStyle = TextStyle(fontSize: 25);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold( // https://api.flutter.dev/flutter/material/Scaffold-class.html
      appBar: AppBar(
        title: Text('Home'),
        centerTitle: true,
      ),
      body: Center (
        child: Column( // https://api.flutter.dev/flutter/widgets/Column-class.html
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Veces Pulsado:', style: _textStyle ),
            Text('$_conteo', style: _textStyle),
          ],
        )
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat, // centrar floatinButton abajo
      floatingActionButton: FloatingActionButton(
        onPressed: () => { onPressFloatingButtonAdd() } ,
        child: const Icon(Icons.add), // https://material.io/resources/icons/
      ),
    );
  } 
}
```

**Solucion:** Indicar que se rexdubije cuando cambia la variable usando
el metodo `setState(function)` y meterle aqui lo que queremos cambiar.
O tambien podemos hacer solo llamaar el metodo cuando queremos que se
repinten los cambios

```dart
  void onPressFloatingButtonAdd() {
    setState(() {
      _conteo++;
    });
    //_conteo++;
    //setState(() { }); da el mismo resultado
  }
```

**Resultado:** Cada vez que pulsemos el boton podremos ver el cambio
reflejado en la pantalla

<!--

#########################################

-->

----

<!--

#########################################

-->

### 53. Creando más acciones en nuestro contador

**Objetivo:** Crearemosvarios floating buttons para poder aumentar, reducir
y reiniciar nuestro contador. El cual vamos a usar una Fila para poder
lograr esto

```dart
  @override
  Widget build(BuildContext context) {
    ...
    ), // Column
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat, // centrar floatinButton abajo
#     floatingActionButton: _createLoatingButtons(),
    );
  }

+ Row _createLoatingButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        SizedBox(width: 5), // espacio de la izq
        FloatingActionButton(
          onPressed: onPressFloatingButtonReset,
          child: const Icon(Icons.refresh), // https://material.io/resources/icons/
        ),
        Expanded(child: SizedBox(),), // espacio blanco grande
        FloatingActionButton(
          onPressed: onPressFloatingButtonRemove,
          child: const Icon(Icons.remove),
        ),
        SizedBox(width: 20.0),
        FloatingActionButton(
          onPressed: onPressFloatingButtonAdd,
          child: const Icon(Icons.add),
        ),
        SizedBox(width: 5),
      ],
    );
```

**Resultado:** Tenemos tres botones cada uno con una accion diferente

![07_app_contador](./img/07_app_contador.gif)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Seccion 6: Componentes

Vamos a crear una aplicacion que use varios/muchos widgets interezantes,
para irnos familarizando con cada uno de ellos.

### 60. Inicio de proyecto

A. Creamos un nuevo proyecto

B. Borramos todo el contenido de main.dart y usamos el snniped `mateapp`
para crear la estructura. Quedando asi

```dart
import 'package:flutter/material.dart';
import 'package:widgets/src/MyApp.dart';
void main() => runApp(MyApp());
```

C. Agregamos el page principal

```dart
import 'package:flutter/material.dart';
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, // ocultar etiqueta de debug, ocultar etiqueta debug
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: ListView(
          children: [
            ListTile(title: Text('Colors')),
            Divider(),
            ListTile(title: Text('Aninals')),
            Divider(),
            ListTile(title: Text('Things')),
          ],
        )
      ),
    );
  }
}
```

**Resultado:** Tenemos una lista basica

![09_lista_basica](./img/09_lista_basica.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Crear Items de lista de forma dinamica

**Objetivo**: Vamos a separar la lista para tenerlo en su propio page, y
crearemos los elementos a partir de una lista

A. Extraemos el `ListView` en este nuevo archivo `lib\src\pages\HomePage.dart`

```dart
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {

  static const opciones = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco' ];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      body: ListView(
        children: _crearListaOpciones(),
      )
    );
  }

  List<Widget> _crearListaOpciones() {
    final opcionesList = List<Widget>();
    opciones.forEach((option) {
      opcionesList
        ..add(ListTile(
                title: Text(option),
                subtitle: Text('Algo'),
                leading: Icon(Icons.qr_code),
                trailing: Icon(Icons.arrow_right),
                onTap: () { print(option); },
        ))
        ..add(Divider());
    });
    return opcionesList;
  }
}
```

B. En `lib\src\MyApp.dart` Traemos nuestro nuevo Widget

```dart
import 'package:flutter/material.dart';
import 'package:widgets/src/pages/HomePage.dart';
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, // ocultar etiqueta de debug, ocultar etiqueta debug
      title: 'Material App',
      home: HomePage()
    );
  }
}
```

**Resultado:** Tenemos la lista creada con mas elementos, y si precionamos
uno de ellos nos mostrara un mensaje diferente.

![10_lista_construida_dinamicamente](./img/10_lista_construida_dinamicamente.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### crear menu-lista - Agregar recurso (json, imagen...) a flutter / crear menu

**Objetivo:** Vamos a agregar un json para tenerlo disponible en nuestro
codigo.

A. Creamos `data/menu_opts.json`

```json
[
  { "ruta" : "alert", "icon" : "add_alert", "texto": "Alertas" },
  { "ruta" : "avatar", "icon" : "accessibility", "texto": "Avatars" },
  { "ruta" : "card", "icon" : "folder_open", "texto": "Cards - Tarjetas" }
]
// luego lo obtendremos con
// String dataSt = await rootBundle.loadString('assets/data/menu_opts.json');
```

B. Agregamos la nueva carpeta/archivo que creamos a los recursos, para
que en ios y andorid esten disponibles

B.1. Vamos a `pubspec.yaml` y agregamos

- **Nota:** Al agregar nuevos recursos vamos a re-lanzar la aplicacion
  para que detecte los cambios

```yaml
flutter:
  ...
  assets:
    - assets/data/menu_opts.json
```

**Resultado:** Tenemos disponible el json en nuestro codigo.

<!--

#########################################

-->

----

<!--

#########################################

-->

### crear menu-lista - Convertir JSON a lista de objetos

**Objetivo:** El json que agregamos lo vamos a convertir en un objeto

A. Copiamos y pegamos el JSON en [quicktype](https://app.quicktype.io/)

B. Pegamos el codigo que nos da.

- El metodo que ahorita nos importa es el `menuOptionFromJson`

```dart
import 'dart:convert';

class MenuOption {
  String path;
  String icon;
  String text;
  String description;
  
  MenuOption({ this.path, this.icon, this.text, this.description, });

  factory MenuOption.fromJson(Map<String, dynamic> json) => MenuOption(
      path: json["path"],
      icon: json["icon"],
      text: json["text"],
      description: json["description"],
  );

  Map<String, dynamic> toJson() => {
      "path": path,
      "icon": icon,
      "text": text,
      "description": description,
  };

  static List<MenuOption> menuOptionFromJson(String str) {
    return List<MenuOption>.from(json.decode(str).map((x) => MenuOption.fromJson(x)));
  } 

  static String menuOptionToJson(List<MenuOption> data) {
    return json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
  } 

  @override
  String toString() {
    return 'option:(path: $path, icon: $icon, text: $text, description: $description)';
  }

}
```

B. Creamos un servicio para obtener la lista de objetos

```dart
import 'package:flutter/services.dart';
import 'package:widgets/src/models/MenuOption.dart';

class MenuOptionsService {
  static Future<List<MenuOption>> getall() async {
    String dataSt = await rootBundle.loadString('assets/data/menu_opts.json');
    List<MenuOption> options = MenuOption.menuOptionFromJson(dataSt);
    return options;
  }
}
```

**Resultado:** Ya podemos obtener el json en forma de lista de objetos
para que mas adelante lo usemos para construir un menu.

<!--

#########################################

-->

----

<!--

#########################################

-->

### crear menu-lista - Construir vista a partir de un Future con FutureBuilder

- Fuente: [FutureBuilder](https://api.flutter.dev/flutter/widgets/FutureBuilder-class.html)

**Objetivo:** Si vemos como obtenemos nustra lista de opciones de menu,
es por medio de un future, lo cual debemos de construir la vista de forma
asyncrona, para esto nos sirve el FutureBuilder.

**FutureBuilder**: Este tiene tres tiempos, `snapshot.error y snapshot.data`
los cuales indican si fallo o salio bien, si no tenemos nunguno significa
que aun esta cargando.

```dart
import 'package:flutter/material.dart';
import 'package:widgets/src/LocalServices/MenuOptionsService.dart';
import 'package:widgets/src/models/MenuOption.dart';

class HomePage extends StatelessWidget {

  @override Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
// ya no le damos una lista si no un widget que la contendra
#     body: _crearListaOpciones(),
    );
  }

+ Widget _crearListaOpciones() {
// este nos permitira construir un widget temporal mientrase se resuelve el Future
+   return FutureBuilder( // hereda de "StatefulWidget" que a su vez hereda de "Widget"
+     future: MenuOptionsService.getall(),
+     // initialData: [], podemos asignar datos por default mientras los otros aun no estan
//construira la lista cuando "snapshot.data" ya este listo
+     builder: (context, AsyncSnapshot<List<MenuOption>> snapshot) {
        return ListView(
+         children: _buildListView(snapshot.data),
+       );
+     },
+   );
+ }

  // nuestro contructor de la vista
+ List<Widget> _buildListView(List<MenuOption> options) {
    if(options == null) { return new List<Widget>(); } // al principio llega un nulo no se por que
    final opcionesList = List<Widget>();
    options.forEach((option) {
      opcionesList
        ..add(ListTile(
                title: Text(option.text),
                subtitle: Text(option.description),
                leading: Icon(Icons.qr_code),
                trailing: Icon(Icons.arrow_right),
                onTap: () { print(option.path); },
        ))
        ..add(Divider());
    });
    return opcionesList;
  }
}
```

**Resultado:** Podemos ver la lista que se construlle de forma asincrona,
(va a leer el jsony lo convierte a objetos) pero como es muy rapida la
operacion no lo notamos. **Falta:** Tomar el icono de forma dinamica.

![11_lista_construida_async_con_FutureBuilder](./img/11_lista_construida_async_con_FutureBuilder.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 68. Utilidades - Obtener un ícono en funcion de un string

**Problema** Tenemos el nombre del icono declarado en el modelo, pero
no sabemos como cargar este icono en funcion del string que tenemos en
el modelo.

**Nota:** Flutter no cuenta con algun util que nos ayude con esto, pero
podemos aser una nosotros que nos saque del aprieto

A. Creamos utileria: `lib\src\utils\IconBuilder.dart`

```dart
import 'package:flutter/material.dart' show Icon, Icons, IconData, Colors ;

class IconBuilder {
  static const _icons = <String, IconData> {
    'add_alert': Icons.add_alert,
    'accessibility': Icons.accessibility,
    'folder_open': Icons.folder_open,
  };
  
  static Icon getIcon(String nameIcon) {
    return Icon(_icons[nameIcon], color: Colors.blue,);
  }
}
```

B. Agregamos nuestra utilidad en `lib\src\pages\HomePage.dart`

```dart
List<Widget> _buildListView(List<MenuOption> options) {
  if(options == null) { return new List<Widget>(); }
  final opcionesList = List<Widget>();
  options.forEach((option) {
    opcionesList
      ..add(ListTile( title: Text(option.text), subtitle: Text(option.description),
#             leading: IconBuilder.getIcon(option.icon),
              trailing: Icon(Icons.arrow_right), onTap: () { print(option.path); },
      )) ..add(Divider());
  });
  return opcionesList;
}
```

**Resultado:** Tenemos los iconos generado en funcion de un string.

![12_crear_icono_en_funcion_de_string](./img/12_crear_icono_en_funcion_de_string.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 69. Navegar a una nueva pantalla en Flutter

**Objetivo:** Cuando pulsemos sobre alguna opcion de la lista vamos a
navegar a otra pantalla

A. Actualmente tenemos lo de abajo lo cual cuando pulsamos se pinta la
propiedad path de menuOption.

```dart
List<Widget> _buildListView(List<MenuOption> options) {
  ...
  opcionesList..add(ListTile(...
*   onTap: () { print(option.path); },
  ))...
}
```

B. Creamos dos nuevas pantallas-paginas AlertPage y AvatarPage en `lib\src\pages`

```dart
import 'package:flutter/material.dart';
class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar'),
      ),
      body: Text('Hola'),
    );
  }
}
```

**Nota:** La nevegacion que usaremos no es la recomendable pero es la
mas forma mas escencial para entender como funciona. El **PROBLEMA** es <!-- $ref{vbuijn00} -->
que por cada boton o pantalla deberiamos de crear este arbol de desicion
lo cual es codigo repetido.

C. Necesitamos el context para poder hacer la navegacion, en `lib\src\pages\HomePage.dart`

```dart
List<Widget> _buildListView(List<MenuOption> options, BuildContext context) {
  if(options == null) { return new List<Widget>(); } // al principio llega un nulo no se por que
  final opcionesList = List<Widget>();
  options.forEach((option) {
    opcionesList ..add(ListTile(
              title: ...
              onTap: () {
// Creamos la ruta                
+               final route = MaterialPageRoute(
+                 builder: (context) {
+                   //if(option.path == 'alert') ... if(option.path == 'avatar')...
+                   return AvatarPage();
+                 }
+               );
// navegamos a la ruta que creamos
+               Navigator.push(context, route);
              },
      )) ..add(Divider());
  }); return opcionesList;
}
```

D. Y para retornar solo hacemos uso del pop, en cualquier evento de boton
por ejemplo.

```dart
onPressed: () => { Navigator.pop(context) },
```

**Resultado:** Podemos navegar de una pantallaA a pantallaB.

[13_navegacion_basica.gif](./img/13_navegacion_basica.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 70. Navegar con rutas con nombre

Para solucionar el problema de arriba `$ref{vbuijn00}`, usamos este metodo
lo cual nos permite definir solo en un lugar nuestros paths.

A, En `lib\src\MyApp.dart` agregamos nuestras rutas

```dart
import 'package:flutter/material.dart'; import 'package:widgets/src/pages/AlertPage.dart'; import 'package:widgets/src/pages/AvatarPage.dart'; import 'package:widgets/src/pages/HomePage.dart';
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, // ocultar etiqueta de debug, ocultar etiqueta debug
      title: 'Material App',
-     //home: HomePage()
+     initialRoute: 'home', //ruta al iniciar la aplicaion
//agregar arbol de rutas, declarar arbol de rutas, agregar ruteo, añadir ruteo
+     routes: <String, WidgetBuilder>{
+       'home' : (BuildContext context) => HomePage(),
+       'alert' : (BuildContext context) => AlertPage(),
+       'avatar' : (BuildContext context) => AvatarPage(),
+     },
// por si no encuentra la ruta definimos una por defecto
+     onGenerateRoute: (RouteSettings settings) {
+       print('Ruta: ${settings.name} no encontrada');
+       return MaterialPageRoute(
+         builder: (context) {
+           return AvatarPage();
+         }
+       );
+     },
    );
  }
}
```

B. En nuestros totones de navegacion agregamos

```dart
  onTap: () {
-   // final route = MaterialPageRoute(
-   //   builder: (context) {
-   //     //if(option.path == 'alert') ... if(option.path == 'avatar')...
-   //     return AvatarPage();
-   //   }
-   // );
// "option.path" contiene los nombre que declaramos arriba fijarse en el "menu_opts.json"
+   Navigator.pushNamed(context, option.path);
  },
```

**Resultado:** El mismo de arriba solo que de una mejor manera. **Problema**
tenemos una ruta que no va a ningun lado.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 72. Pasar las rutas a su propio archivo y crear CardWidget

**Problema:** En este caso no tenemos muchas pantallas, pero podria darse
el caso donde tengamos muchas, lo cual afectaria la lectura de nuestro
codigo por lo que vamos a separarlo a su propio archivo.

0, Creamos la nueva pantalla `lib\src\pages\CardPage.dart`

```dart
import 'package:flutter/material.dart';
class CardPage extends StatelessWidget {
  @override Widget build(BuildContext context) {
    return Scaffold( appBar: AppBar( title: Text('Cartas'), ), body: Text('Hola CardPage'), );
  }
}
```

A. Creamos `lib\src\Routes.dart` con el map de rutas que tenemos en `lib\src\MyApp.dart`

```dart
import 'package:flutter/material.dart' show WidgetBuilder, BuildContext;
import 'package:widgets/src/pages/AlertPage.dart';
import 'package:widgets/src/pages/AvatarPage.dart';
import 'package:widgets/src/pages/CardPage.dart';
import 'package:widgets/src/pages/HomePage.dart';
Map<String, WidgetBuilder> getRutesApp(BuildContext context) {
  return {
    'home'    : (context) => HomePage(),
    'alert'   : (context) => AlertPage(),
    'avatar'  : (context) => AvatarPage(),
    'card'    : (context) => CardPage(),
  };
}
```

B. sustituimos en `lib\src\MyApp.dart`

```dart
class MyApp extends StatelessWidget {
  @override Widget build(BuildContext context) {
    return MaterialApp(...
//sustituimos
#     routes: getRutesApp(context),
```

C. Creamos nueva targeta en `lib\src\pages\CardPage.dart`

```dart
import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cartas'),
      ),
      body: ListView(
        //padding: EdgeInsets.all(20),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        children: <Widget>[
          _card1()
        ],
    ));
  }


  Widget _card1() {
    return Card(
      child: Column(
        children: [
          ListTile(
            leading: Icon(Icons.ac_unit, color: Colors.blue),
            title: Text('Titulo de targeta 1'),
            subtitle: Text('Targeta basica,'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FlatButton(
                child: Text('Aceptar'),
                onPressed: () { },
              ),
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () { },
              )
            ],
          ),
        ]
      ),
    );
  }
}
```

**Resultado:** Creamos un tipo de targeta sencillo donde le agregamos
padding diferentes elementos con filas y columnas.

![14_cards_simple](./img/14_cards_simple.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 84. RESUMEN: agregar pantalla

**Objetivo:** Ya teniendo todo configurado de nuestro arbol de rutas,
que vimos en [70. Navegar con rutas con nombre](#70-navegar-con-rutas-con-nombre),
ahora vamos a agregar una nueva pagina.

A. Creamos la pagina a mostrar, comunmente en un Scalfold

```dart
class SliderPage extends StatefulWidget { // state-les o state-full
  SliderPage({Key key}) : super(key: key);

  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Entradas'),
        ),
        body: _sliderBody() //retorna algun objeto tipo Widget
    );
  }
}
```

B. Agregamos la ruta en nuestro arbol

```dart
import 'package:flutter/material.dart' show WidgetBuilder, BuildContext;
...
import 'package:widgets/src/pages/SliderPage.dart';

Map<String, WidgetBuilder> getRutesApp(BuildContext context) {
  return {
    'home'          : (context) => HomePage(),
    ...
    'slider'        : (context) => SliderPage(),
  };
}
```

C. Agregamos la opcion de nuestro menu desde el json

```json
[ 
  ... {
    "path" : "slider",
    "description": "Inputs numericos dentro de un rango, seleccionandolo por arrastre",
    "icon" : "menu",
    "text": "Sliders"
  }
]
```

D. Si no esta el icono que queremos usar lo agregamos, para poder obtener
el icono que queremos por su nombre en string

```dart
import 'package:flutter/material.dart' show Icon, Icons, IconData, Colors ;

class IconBuilder {
  static const _icons = <String, IconData> {
    'add_alert'       : Icons.add_alert,
    ...
    'menu'            : Icons.menu,
  };
  static Icon getIcon(String nameIcon) { return Icon(_icons[nameIcon], color: Colors.blue,); }
}
```

**Resultado:** Ya tenemos una nueva pagina agregada, con el uso de
buenas practicas.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 73. Cargar imagen de la red y animacion de carga

**Objetivo:** Usaremos propiedades mas avanzadas del Card

0, Agregamos la animacion de carga en nuestros assets

<!-- agregar carpeta, añadir carpeta -->

```yaml
assets:
    - assets/img/
```

A. En `lib\src\pages\CardPage.dart` agregamos otro car

```dart
import 'package:flutter/material.dart';
class CardPage extends StatelessWidget {
  @override Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text('Cartas'), ),
      body: ListView( padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        children: <Widget>[ _card1(), SizedBox(height: 30),
+         _card2(),
        ],
    ));
  }

  Widget _card1() {... }

+ Widget _card2() {
    return Card(
      // avita que los elementos se salgan del Card asi evitamos que la imagen se salga y tenga esuinas punteadas
      clipBehavior: Clip.antiAlias,
      // por defecto es uno, sombrado que da la ilucion que la Card esta mas arriba
      elevation: 3,
      // esquinar redondeadas
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Column(
        children: [
          FadeInImage(
            image: NetworkImage('https://static.photocdn.pt/images/articles/2017_1/iStock-545347988.jpg'),
            placeholder: AssetImage('assets/img/loading_jar.gif'), // imagen a mostrar mientras carga
            fadeInDuration: Duration(milliseconds: 200), // tiempo de la animacion
            fit: BoxFit.cover, // como se acomoda la imagen (ej. none tomara el tamaño de la imagen ocultando parte de esta)
            height: 220.0,   // si no lo declaramos, cuando la imagen no este se movera el contedido de abajo
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text('Algun texto para poner aqui deber conseguir o algo asi'),
          )
        ],
      ),
    );
  }
}
```

**Resultado:** Tenemos un card que carga una imagen y tiene le agregamos
la animacion de de carga.

![15_mostrar_imagen_mas_web_animacion_de_carga](./img/15_mostrar_imagen_mas_web_animacion_de_carga.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 76. Widget - Alert

<!-- mostrar modal, mostrar ventana, modal de confirmacion, preguntar confirmacion,
pedir al usuario -->

**Objetivo:** Vamos a crear una alerta con imagen, botones y texto

```dart
import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alerts'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Mostrar Alerta'), color: Colors.blue, textColor: Colors.white, shape: StadiumBorder(), onPressed: () => _mostrarAlert(context),
        ),
      ),
    );
  }

  _mostrarAlert(BuildContext context) {
+   showDialog(
      context: context,
      barrierDismissible: true, // cerrar al pulsar afuera de este
      builder: (context) {
+       return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          title: Text('Mi titulo'),
          content: Column(
            mainAxisSize: MainAxisSize.min, // evitar ue tome todo lo disponible, asi solo tomara lo necesario
            children: [
              Text('Mi Contenido, evitar ue tome todo lo disponible, asi solo tomara lo necesario'),
              FlutterLogo(size: 200)
            ],
          ),
          actions: [
            FlatButton(
              child: Text('Cancelar'),
              onPressed: (){ Navigator.of(context).pop(); }, // accion de regresar
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: (){},
), ], ); } ); } }
```

**Resultado:** Tenemos la sig. alerta

![16_alerta_con_titulo_texto_imagen_botones](./img/16_alerta_con_titulo_texto_imagen_botones.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 78. Animaciones con Container y AnimatedContainer

- Fuentes
  - [Curves-class](https://api.flutter.dev/flutter/animation/Curves-class.html)
  - [animated-container](https://flutter.dev/docs/cookbook/animation/animated-container)

**Recordatorio:** Para que se noten los cambios en la pantalla debemos
de hacerlo dentro de un `StatefulWidget` y llamar al metodo `setState(() { });`
ya sea dentro o al final de los cambios, para que flutter repinte.

**Introduccion:** Al cambiar las propiedades de un `container` este cambiara
de forma inmediata, y para animar este cambio usamos `AnimatedContainer`.

**Objetivo:** Vamos a crear una cuadro que cambie su alto, ancho, borde
y color de una forma animada.

```dart
import 'dart:math';
import 'package:flutter/material.dart';

// use el snniped "statefulW" para rapido
class AnimationsPage extends StatefulWidget {
  AnimationsPage({Key key}) : super(key: key);
  @override
  _AnimationsPageState createState() => _AnimationsPageState();
}

class _AnimationsPageState extends State<AnimationsPage> {

  final _random = Random();

  double _width = 50.0;
  double _height = 50.0;
  Color _color = Colors.green;
  var _borderRadius = BorderRadius.circular(8);

  @override
  Widget build(BuildContext context) {
      return Container( 
       child: Scaffold(
         appBar: AppBar( title: Text('Animaciones'), ),
         body: Center(
         //child: Container( // cambiara de forma instantanea y ni pide el duration
           child: AnimatedContainer( // a dif. del Container pide el duration
             duration: Duration(milliseconds: 600),
             curve:  Curves.bounceOut,  // Lista> https://api.flutter.dev/flutter/animation/Curves-class.html
             height: _height,
             width: _width,
             decoration: BoxDecoration(
               borderRadius: _borderRadius,
               color: _color,
             ),
           ),
         ),
         floatingActionButton: FloatingActionButton( child: Icon(Icons.play_arrow), onPressed: () => _playAnimation(), ),
       )
    );
  }

  void _playAnimation() {
    _height = _random.nextInt(450).toDouble();
    _width = _random.nextInt(300).toDouble();
    _color = Color.fromRGBO(_random.nextInt(250), _random.nextInt(250), _random.nextInt(250), 1);
    _borderRadius = BorderRadius.circular(_random.nextInt(90).toDouble());
    
    setState(() { }); // pintar los cambios
  }
}
```

**Resultado:** Tenemos el siguiente cuadro animado.

![17_animaciones_con_animatedContainer](./img/17_animaciones_con_animatedContainer.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 79-80. inputFields, email y password

**Introduccion:** Para el email la unica diferencia es el keyboarType,
lo mismo para campos numericos. fechas, telefono, etc. Para las claves
usamos obscureText

```dart
import 'package:flutter/material.dart';

class InputsPage extends StatefulWidget {
  InputsPage({Key key}) : super(key: key);

  @override
  _InputsPageState createState() => _InputsPageState();
}

class _InputsPageState extends State<InputsPage> {
  String _nombre = '';
  String _email = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Entradas'),
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          children: [
            _crearInput1(),
            Text('Nombre: $_nombre'),
            Divider(),
            _crearInputEmail(),
            Divider(),
            _crearInputPassword(),
          ],
        ),
      ),
    );
  }

  Widget _crearInput1() {
    return TextField(
      //autofocus: true,
      onChanged: (inputTxt) { setState(() { _nombre = inputTxt; });  },
      // maxLength: 10, pone en rojo cuando se pasa el usuario pero no funciona bien junto con el "counter"
      decoration: InputDecoration(
        labelText: 'Nombre', // texto izq-arriba
        hintText: 'ej. Pedro',
        counter: Text("Caracteres ${_nombre.length}"), // texto der-abajo 
        helperText: 'Primer nombre o alias', // texto izq-abajo
        suffixIcon: Icon(Icons.account_balance), // icono der
        prefixIcon: Icon(Icons.description), // icono izq
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20)
        ),
      ),
    );
  }

  Widget _crearInputEmail() {
    return TextField(
      onChanged: (inputTxt) { setState(() { _email = inputTxt; });  },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email',
        hintText: 'ej. mario@mail.com',
        suffixIcon: Icon(Icons.email),
      ),
    );
  }

  Widget _crearInputPassword() {
    return TextField(
      //onChanged: (inputTxt) { setState(() { _email = inputTxt; });  },
      keyboardType: TextInputType.emailAddress,
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Contraseña',
        suffixIcon: Icon(Icons.lock),
      ),
    );
  }
}
```

**Resultado:** Tenemos los siguientes inputs

![18_inputs](./img/18_inputs.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 81. selec. Fecha - Datepicker

**Objetivo:** Crear un input de fecha que se pueda seleccionar de un picker.

```dart
  import 'package:flutter/material.dart';
  class InputsPage extends StatefulWidget {...}
  class _InputsPageState extends State<InputsPage> {
    ...
+   String _fecha = '';
+   final _dateInputController = new TextEditingController();

    @override Widget build(BuildContext context) {
      return Container(
        child: Scaffold( appBar: AppBar( title: Text('Entradas'), ),
          body: ListView(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            children: [ ...
+             _crearInputDate(context),
            ],
          ),
        ),
      );
    }

    Widget _crearInput1() {...} Widget _crearInputEmail() {...} Widget _crearInputPassword() {...}

+   Widget _crearInputDate(BuildContext context) {
+     return TextField(
+       keyboardType: TextInputType.datetime,
//vinculamos el input con el controller para asignar su txt fuera de el
++      controller: _dateInputController,
+       decoration: InputDecoration(
+         labelText: 'Fecha',
+         suffixIcon: Icon(Icons.calendar_today),
+       ),
+       onTap: () {
// quitar el focus
+         FocusScope.of(context).requestFocus(new FocusNode());
+         _selectDate(context);
+       },
+     );
+   }

+   _selectDate(final BuildContext context) async {
// mostramos el picker
+     DateTime picked = await showDatePicker(
+       context: context,
+       initialDate: new DateTime.now(),
+       firstDate: new DateTime(2020),
+       lastDate: new DateTime(2025),
+     );
+     if(picked!=null) {
+       setState(() {
//capturamos y mostramos valor
+         _fecha = picked.toString();
+         _dateInputController.text = _fecha;
+       });
+     }
+   }
+ }
```

**Resultado:** Al pulsar sobre el input de fecha nos sale el siguinte picker
y al darle Ok pone la fecha en este.

![19_datePicker_seleccionar_fecha.png](./img/19_datePicker_seleccionar_fecha.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 82. selec. Fecha - Cambiar el idioma del Datepicker

- Fuentes:
  - [internationalization](https://flutter.dev/docs/development/accessibility-and-localization/internationalization)
  - [GlobalMaterialLocalizations class](https://api.flutter.dev/flutter/flutter_localizations/GlobalMaterialLocalizations-class.html)

**Antes de:** necesitamos una dependencia de mas para poder llamar otros
idiomas

A. Agregar en `pubspec.yaml`

```yaml
dependencies:
  flutter:
    sdk: flutter
  flutter_localizations:
    sdk: flutter
```

B. Agregamos los locales en `a`

```dart
  import 'package:flutter/material.dart';
+ import 'package:flutter_localizations/flutter_localizations.dart';

  import 'package:widgets/src/pages/AvatarPage.dart';
  import 'Routes.dart';

  class MyApp extends StatelessWidget {
    @override Widget build(BuildContext context) {
      return MaterialApp(
        debugShowCheckedModeBanner: false, title: 'Material App', initialRoute: 'home', routes: getRutesApp(context),
        onGenerateRoute: ...,
+       localizationsDelegates: [
+         // ... app-specific localization delegate[s] here
+         //AppLocalizations.delegate,
+         GlobalMaterialLocalizations.delegate,
+         GlobalWidgetsLocalizations.delegate,
+         //GlobalCupertinoLocalizations.delegate,
+       ],
+       supportedLocales: [
+         const Locale('en', 'US'),
+         const Locale('es', 'MX'),
+       ],
      );
    }
  }
```

C. Cambiamos de idioma al Picker

```dart
  DateTime picked = await showDatePicker( context: context,
+   locale: Locale('es', 'MX'), //debe coincidir con el de arriba
    ...
  );
```

**Resultado:** Tenemos el mismo picker pero ahora en español

<!--

#########################################

-->

----

<!--

#########################################

-->

### 83. Dropdown y DropdownMenuItem

**Introduccion:** Estos son la lista de opciones que al darle click se
despliegan para que e usuario pueda escojar una.

A. Ejemplo: .

```dart
class _InputsPageState extends State<InputsPage> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold( appBar: AppBar( title: Text('Entradas'), ),
        body: ListView( padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          children: [
            ...
            _crearDropdown(),
          ],
  ), ), ); }

  List<String> _mascotas = ['nona', 'nonito', 'choco', 'cual'];
  String _optSelected = 'nona';
  

  List<DropdownMenuItem<String>> _getOpcionesDropdown() {
    

    List<DropdownMenuItem<String>> lista = new List();
    _mascotas.forEach( (mascota){
      lista.add( DropdownMenuItem(
        child: Text(mascota),
        value: mascota,
      ));

    });
    return lista;
  }

  Widget _crearDropdown() {
    return Row(
      children: [
        Text('Marcota: '),
        SizedBox(width: 20),
        DropdownButton(  // si queremos que use todo el ancho lo envolvemos en un Expanded
          value: _optSelected,
          items: _getOpcionesDropdown(),
          onChanged: (opt) {
            print('opt: $opt');
            _optSelected = opt;
          },
        ),
      ],
    );
  }

}
```

**Resultado:**

![20_dropbox](./img/20_dropbox.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 84.85. Slider - Checkbox y Switches

- Fuente: [Hacer escala de grices](https://stackoverflow.com/questions/47851690/making-an-image-widget-disabled-grayed-out)

**Objetivo1:** Crearemos un Slider.

**Objetivo2:** Vamos a crear estos dos elementos que pueden contener,
elementos booleanos, y aplicar alguna funcionalidad.

```dart
import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  SliderPage({Key key}) : super(key: key);

  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  
  double _valueSlider = 100.0;

  bool _activeSliderCheckbox = false;
  bool _scaleGrayCheckbox = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Entradas'),
        ),
        body: _sliderPageBody()
    );
  }

  Container _sliderPageBody() {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Column(
        children: [
          _crearSlider(),
          _crearCheckboxListTile(),
          _crearSwitchListTile(),
          _imageToResize(),
        ],
      ),
    );
  }

  Widget _crearSlider() {
    return Column(
      children: [
        Row(
        children: [
          Text('Tamaño: '),
          Expanded(
            child: Slider(
              value: _valueSlider,
// le pasamos un nulo para apagarlo
              onChanged: (_activeSliderCheckbox) ? null : (value) { // des-habilitar checkbox, deshabilitar checkbox, desabilitar checkbox
                setState(() {
                  _valueSlider = value;
                });
              },
              max: 300,
              min: 100,
            ),
          ),
          _crearCheckBox(),
        ],
      ),
      Text('Valor: ${_valueSlider.toInt()}'),
      ],
    );
  }
  
  Widget _imageToResize() {    
    return Expanded(
      child: ColorFiltered(
        colorFilter: _scaleGrayCheckbox ? ColorFilter.mode( Colors.white, BlendMode.saturation) : ColorFilter.mode(Colors.transparent, BlendMode.saturation),
        child: Image(
          image: Image.asset("assets/img/loading_jar.gif").image,
          width: _valueSlider,
        ),
      ),
    );
  }

  Checkbox _crearCheckBox() {
    // SOLO ES EL CUADRO SIN EL TEXTO
    return Checkbox(
      value: _activeSliderCheckbox,
      onChanged: (isActive) {
        setState(() {
          _activeSliderCheckbox = isActive;
        });
      },
    );

  }

  CheckboxListTile _crearCheckboxListTile() {
    // PERMITE CREARLO CON TEXTO Y OCUPARA TODO EL ANCHO
    return CheckboxListTile(
      title: Text('Escala de Grises'),
      value: _scaleGrayCheckbox,
      onChanged: (isActive) {
        setState(() {
          _scaleGrayCheckbox = isActive;
        });
      },
    );
  }

  SwitchListTile _crearSwitchListTile() {
    return SwitchListTile(
      title: Text('Escala de Grises'),
      value: _scaleGrayCheckbox,
      onChanged: (isActive) {
        setState(() {
          _scaleGrayCheckbox = isActive;
        });
      },
    );
  }
}
```

**Resultado:** Creamos un slider que nos sirve para aumentar o reducir
el tamaño de una imagen.

![22_slider_checkbox_grayscale](./img/22_slider_checkbox_grayscale.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 86-87. InfiniteScroll - Basico con Listview Builder

**Introduccion:** Nos permite crear una lista a partir de un metodo, lo
cual nos permitira hacer cosas como el infiniteScroll, en este ejemplo
cargaremos imagenes de 10 en 10.

A, En `lib\src\pages\ListPage.dart`

```dart
import 'package:flutter/material.dart';

class ListPage extends StatefulWidget {
  ListPage({Key key}) : super(key: key);

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {

//con este sabremos si el scroll llega al final
  final scollCtrl = new ScrollController();
  
  final _miLista = new List();
  int _ultimoElementoCargado = 0;

  @override
  void initState() {
    super.initState(); // no olvidar, no quitar
    agregar10();
    scollCtrl.addListener(() {
//  detectar fin de scroll, termino scroll, termino del scroll final del scroll, final scroll, fin del scroll
      double posActual = scollCtrl.position.pixels;
      double finScroll = scollCtrl.position.maxScrollExtent;
      if(posActual == finScroll) {
        agregar10();
        print('Llego al final');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
       child: Scaffold(
         appBar: AppBar(
           title: Text('Listas'),
         ),
         body: _listPageBody(),
       )
    );
  }

  Widget _listPageBody() {
    return ListView.builder(
      controller: scollCtrl,
      itemCount: _miLista.length,
      itemBuilder: (context, int index) {
        final imagen = _miLista[index];
        return FadeInImage(
          image: NetworkImage('https://picsum.photos/500/300/?image=$imagen'),
          placeholder: AssetImage('assets/img/loading_jar.gif'),
        );
      },
    );
  }

  void agregar10() {
    for (var i = 0; i<10; i++) {
      _ultimoElementoCargado++;
      _miLista.add(_ultimoElementoCargado);
    }
    setState(() { });
  }
}
```

**Resultado:** (ver gif un titulo abajo) Tenemos un ScrollInfinito basico,
el cual cada imagen tiene su carga, mas adelante le agregaremos una
animacion de carga cada vez que otros 10 elementos sean cargados.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 89. InfiniteScroll - Agregar animacion de carga

**Problema:** Con lo de arriba ya funciona nuestro scrollInfinito, pero
el no se da a entender cuando esta cargando y cuando termino de cargar,
que ya ahi nuevos elementos.

**Objetivo:** Vamos a implementar una animacion de carga abajo y mover
el scroll tantito abajo para dar a entender que ya estan cargados los
nuevos elementos.

A, En `lib\src\pages\ListPage.dart`

```dart
  import 'dart:async';
  import 'package:flutter/material.dart';
  class ListPage extends StatefulWidget { ListPage({Key key}) : super(key: key); @override _ListPageState createState() => _ListPageState(); }
  class _ListPageState extends State<ListPage> {
  
    final _miLista = new List();
    int _ultimoElementoCargado = 0;


  //con este sabremos si el scroll llega al final, CUIDADO: cuando le agregamos un listener estos se quedan, por lo
  //debemos de eliminarlo cuando salgamos de la pagina/pantalla
+   final _scollCtrl = new ScrollController();
+   bool _isLoading = false;    

    @override
    void initState() {
      super.initState(); // no olvidar, no quitar
      agregar10();
      _scollCtrl.addListener(() {
+       double posActual = _scollCtrl.position.pixels;
+       double finScroll = _scollCtrl.position.maxScrollExtent;
+       if(posActual == finScroll) {
-         // agregar10();
+         _fetchData();
+       }
      });
    }

+   @override
+   void dispose() {
+     super.dispose();
+     _scollCtrl.dispose();
+   }

    @override
    Widget build(BuildContext context) {
      return Container(
        child: Scaffold(
          appBar: AppBar(
            title: Text('Listas'),
          ),
// hace lo mismo que el Row o Column pero los encima uno delante del otro.
+         body: Stack(
            children: [
              _listPageBody(),
// cuando se redibuje este retornara null o el Loader en funcion de "_isLoading"
+             _crearLoading(),
            ],
          ),
        )
      );
    }

    Widget _listPageBody() {
      return ListView.builder(
+       controller: _scollCtrl,
        itemCount: _miLista.length, itemBuilder: (context, int index) {
          final imagen = _miLista[index]; return FadeInImage( image: NetworkImage('https://picsum.photos/500/300/?image=$imagen'), placeholder: AssetImage('assets/img/loading_jar.gif'), );
        },
      );
    }

    void agregar10() { for (var i = 0; i<10; i++) { _ultimoElementoCargado++; _miLista.add(_ultimoElementoCargado); } setState(() { }); }

//simulamos tiempom de carga
+   void _fetchData() {
      _isLoading = true;
      setState(() { });
      final duration = new Duration(seconds: 2);
// cuando pasen dos segundos llama a "respuestaHttp"
+     new Timer(duration, respuestaHttp ); // CUIDADO: no poner "respuestaHttp()" por que se llamaria inmediatamente
    }


// al ejecutar esto ya sabemos que tenemos respuesta
+   void respuestaHttp() { 
// para quitar el loading
+     _isLoading = false;
// animacion de mover Scroll abajo para dar a entender que hay nuevo.
+      _scollCtrl.animateTo(
        _scollCtrl.position.pixels + 100, 
        curve: Curves.fastOutSlowIn,
        duration: Duration(milliseconds: 300),
      );
// indicamos que empieze la carga de verdad      
+     agregar10();
    }

+   Widget _crearLoading() { // mostramos animacion de carga (bolita de abajo)
      if(_isLoading) {
        return Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
// Widget de mostrar carga
+               CircularProgressIndicator(),
              ],
            ),
            SizedBox(height: 20) // separacion con el piso
          ],
        );
      } else {
// Regresa esto cuando no esta cargando
+       return Container(); 
      }
    }

  }
```

**Resultado:** Tenemos un scroll infinito con animaciones para darle
feddback al usuario

![23_infiniteScroll_con_animacion_carga](./img/23_infiniteScroll_con_animacion_carga.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 89. Pull to refresh

**Introduccion:** En teoria el Infite-scroll que acabamos de hacer sirve
para cargar registros antiguios. Para cargar registros nuevos se suele usar
el refresh cuando hagamos un swipe hacia arriba cuando estamos hasta arriba.

**Objetivo:** Vamos a cargar nuevos registros como lo indicamos en la
introduccion de este titulo.

A. En `lib\src\pages\ListPage.dart`

```dart
import 'dart:async';
import 'package:flutter/material.dart';
class ListPage extends StatefulWidget {...}
class _ListPageState extends State<ListPage> {
  final _scollCtrl = new ScrollController(); final _miLista = new List();  
  bool _isLoading = false;

// 1. Empezamos a cargar del 20, para tener 20 registros simulados como nuevos
  int _primerElementoCargado = 20;
  int _ultimoElementoCargado = 20;

  @override void initState() {...}
  @override void dispose() {...}
  @override Widget build(BuildContext context) {....}

  Widget _listPageBody(BuildContext context) {
// 2. detectara el swipe-arriba cuando estemos arriba, necesitaremos definir "onRefresh"
+   return RefreshIndicator( 
+     onRefresh: _cargarNuevosRegistros,
      child: ListView.builder(
        controller: _scollCtrl,
        itemCount: _miLista.length,
        itemBuilder: (context, int index) {...},
      ),
    );
  }

  void agregar10Abajo() {...}

+ void agregar10Arriba() {
+   int masNuevo = _primerElementoCargado-10;
+   for (var i = _primerElementoCargado; i>masNuevo; i--) {
+     if(i == masNuevo || i < 2) { print('Ya no hay elementos nuevos'); break; }
+     _primerElementoCargado--;
+     _miLista.insert (0, _primerElementoCargado);
+   }
+   setState(() { });
+ }

  void _fetchData() {...}
  void respuestaHttp() { ...}
  Widget _crearLoading() { ...}

+ Future<Null> _cargarNuevosRegistros() async {
+   final duration = new Duration(seconds: 2);
+   new Timer(duration, (){
+     agregar10Arriba();
+   });
+   return Future.delayed(duration);
+ }
}
```

**Resultado:** Podemos cargar nuevos registros conservando los de abajo.

![24_recargar_cargar_nuevos_registrosPull_to_refresh](./img/24_recargar_cargar_nuevos_registrosPull_to_refresh.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### RESUMEN DE LA SECCION

- [_2_app_widgets.zip](./img/_2_app_widgets.zip)

![_2_app_widgets.zip](./img/_2_app_widgets.gif)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Sección 7:Aplicación de películas

### 97. Crear un swiper, e inicio de app

- Fuente: [pub.dev](https://pub.dev/packages/flutter_swiper)

A. Instalamos dependencia para crear un swipe de forma facil

```yaml
dependencies:
  flutter:
    sdk: flutter

  cupertino_icons: ^1.0.0
+ flutter_swiper: ^1.1.6
```

B. Asi queda nuestro main en la ruta por default

```dart
import 'package:flutter/material.dart';
import 'package:peliculas/src/MyApp.dart';

void main() { runApp(MyApp()); }
```

C. Asi nuestro App, `lib\src\MyApp.dart`

```dart
import 'package:flutter/material.dart';

import 'package:peliculas/src/pages/HomePage.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Peliculas',
      initialRoute: '/',
      routes: {
        '/'  : (ctx) => HomePage(),
      },
    );
  }
}
```

D. Nuestro `lib\src\pages\HomePage.dart`

```dart
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);
  @override _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Peliculas'),
          backgroundColor: Colors.indigoAccent,
          actions: [ IconButton( icon: Icon(Icons.search), onPressed: () { print('hello'); }) ],
         ),
         body: _buildHomePageBody(),
       ),
    );
  }

  Widget _buildHomePageBody() {
    return Container(
      child: Column(
        children: [
          _buildSwipeTargetas() ],
      ),
    );
  }

  Widget _buildSwipeTargetas() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      // IMPORTANTE: declarar el ancho y alto, si no, no se mostrara nada
      width: double.infinity,
      height: 250,
      // swipe horizontal
      child: Swiper(
        layout: SwiperLayout.STACK, // necesita del "itemWidth" para que no falle
        itemWidth: 200,
        itemBuilder: (BuildContext context,int index){
          return new Image.network("http://via.placeholder.com/350x150",fit: BoxFit.fill,);
        },
        itemCount: 3,
        pagination: new SwiperPagination(),
        control: new SwiperControl(),
      ),
    );
  }
}
```

**Resultado:** Queda un Swipe de la sig. forma

[25_swipe_de_targetas_horizontal](./img/25_swipe_de_targetas_horizontal.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 98.  Widget personalizado - CardSwiper

**Objetivo:** Vamos a separar nuestro swiper y personalizarlo

A, Extraemos lo que tenemos en `_buildSwipeTargetas` a su propio archivo,
en micaso en, `lib\src\widgets\CartsSwiper.dart`. Notar que creamos una
nueva solo para widgets, equivalente a compoentes en react.

```dart
  import 'package:flutter/material.dart';
  import 'package:flutter_swiper/flutter_swiper.dart';

// NOTA: notar que es un Stateless, ya que es dinamico solo cuando se
// construlle en adelante es estatico
= class CartsSwiper extends StatelessWidget {

// le pasaremos la informacion de las peliculas en esta variable   
+   final List<dynamic> peliculas;

// es obligatorio pasarle las pelicus
+   CartsSwiper({@required this.peliculas});

    @override
    Widget build(BuildContext context) {
      return Swiper(
        layout: SwiperLayout.STACK, // necesita del "itemWidth" para que no falle
        itemWidth: 200,
        itemBuilder: (context, index){
          return ClipRRect( // para que las targetas tengan borde redondeado
            borderRadius: BorderRadius.circular(20),
            child: Image.network("http://via.placeholder.com/350x150",fit: BoxFit.cover),
          );
        },
// el tamaño es segun la cantidad que le pasemos
        itemCount: peliculas.length,
        pagination: new SwiperPagination(),
        control: new SwiperControl(),
      );
    }//fin metodo
  }
```

B, Nuestro `lib\src\pages\HomePage.dart` queda asi

```dart
import 'package:flutter/material.dart';
import 'package:peliculas/src/widgets/CartsSwiper.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Peliculas'),
          backgroundColor: Colors.indigoAccent,
          actions: [ IconButton( icon: Icon(Icons.search), onPressed: () { print('hello'); }) ],
         ),
         body: _buildHomePageBody(context),
       ),
    );
  }

  Widget _buildHomePageBody(BuildContext context) {
// obtener tamaño de pantalla, obtener el tamaño de pantalla, obtener el alto de pantalla, ancho de pantalla
+   final _screenSize = MediaQuery.of(context).size;
+   final double width = _screenSize.width;
+   final double height = _screenSize.height;

    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 10),
            // IMPORTANTE: declarar el ancho y alto, si no, no se mostrara nada
            width: double.infinity,
            height: height * 0.5,
            child: _buildSwipeTargetas(),
            // swipe horizontal
          )
        ],
      ),
    );
  }

  Widget _buildSwipeTargetas() {
    return CartsSwiper(
      peliculas: [1,2,3,4,5], // a futuro le pasaremos objetos y no numeros
    );
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 99. Consumir servicio - TheMovieDB, ApiKey y documentación

- Fuente
  - [TheMovieDB - Doc-API](https://developers.themoviedb.org/3/movies/get-now-playing)
  - [Documentacion API - Imagenes](https://developers.themoviedb.org/3/getting-started/images)
  - [JSON a objeto - quicktype](https://app.quicktype.io/)

A, Creamos una cuenta en y obtenemos la API-Key en `https://www.themoviedb.org/settings/api`

B. Pasar el JSON a objeto

- En `TheMovieDB - Doc-API` hacemos una peticion y asi obtener el JSON
  de respuesta el cual podemos podemos obtener el objeto de
  [quicktype](https://app.quicktype.io/) <!-- convertir json a objeto-->

- Separamos cada uno de ellos en un archivo

B.1, `lib\src\models\entities\OriginalLanguage.dart`

```dart
enum OriginalLanguage { EN, ES, JA }
final originalLanguageValues = EnumValues({ "en": OriginalLanguage.EN, "es": OriginalLanguage.ES, "ja": OriginalLanguage.JA });
class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;
    EnumValues(this.map);
    Map<T, String> get reverse {
        if (reverseMap == null) { reverseMap = map.map((k, v) => new MapEntry(v, k)); }
        return reverseMap;
    }
}
```

B.2, `lib\src\models\entities\Dates.dart`

```dart
class Dates {
  DateTime maximum;
  DateTime minimum;
  
  Dates({ this.maximum, this.minimum,});

  factory Dates.fromJson(Map<String, dynamic> json) => Dates(
      maximum: DateTime.parse(json["maximum"]),
      minimum: DateTime.parse(json["minimum"]),
  );

  Map<String, dynamic> toJson() => {
      "maximum": "${maximum.year.toString().padLeft(4, '0')}-${maximum.month.toString().padLeft(2, '0')}-${maximum.day.toString().padLeft(2, '0')}",
      "minimum": "${minimum.year.toString().padLeft(4, '0')}-${minimum.month.toString().padLeft(2, '0')}-${minimum.day.toString().padLeft(2, '0')}",
  };
}
```

B.3, `lib\src\models\entities\Movie.dart`

```dart
import 'OriginalLanguage.dart';
class Movie {
    bool adult;
    String backdropPath;
    List<int> genreIds;
    int id;
    OriginalLanguage originalLanguage;
    String originalTitle;
    String overview;
    double popularity;
    String posterPath;
    DateTime releaseDate;
    String title;
    bool video;
    double voteAverage;
    int voteCount;

    Movie({this.adult, this.backdropPath, this.genreIds, this.id, this.originalLanguage,
            this.originalTitle, this.overview, this.popularity, this.posterPath, this.releaseDate,
            this.title, this.video, this.voteAverage, this.voteCount,
    });

    factory Movie.fromJson(Map<String, dynamic> json) => Movie(
        adult: json["adult"],
        backdropPath: json["backdrop_path"],
        genreIds: List<int>.from(json["genre_ids"].map((x) => x)),
        id: json["id"],
        originalLanguage: originalLanguageValues.map[json["original_language"]],
        originalTitle: json["original_title"],
        overview: json["overview"],
        popularity: json["popularity"].toDouble(),
        posterPath: json["poster_path"],
        releaseDate: DateTime.parse(json["release_date"]),
        title: json["title"],
        video: json["video"],
        voteAverage: json["vote_average"].toDouble(),
        voteCount: json["vote_count"],
    );

    Map<String, dynamic> toJson() => {
        "adult": adult,
        "backdrop_path": backdropPath,
        "genre_ids": List<dynamic>.from(genreIds.map((x) => x)),
        "id": id,
        "original_language": originalLanguageValues.reverse[originalLanguage],
        "original_title": originalTitle,
        "overview": overview,
        "popularity": popularity,
        "poster_path": posterPath,
        "release_date": "${releaseDate.year.toString().padLeft(4, '0')}-${releaseDate.month.toString().padLeft(2, '0')}-${releaseDate.day.toString().padLeft(2, '0')}",
        "title": title,
        "video": video,
        "vote_average": voteAverage,
        "vote_count": voteCount,
    };
}
```

B.4. `lib\src\models\dtos\MoviePaginationResponse.dart`

```dart
import 'dart:convert';

import 'package:peliculas/src/models/entities/Dates.dart';
import 'package:peliculas/src/models/entities/Movie.dart';

class MoviePaginationResponse {
    Dates dates;
    int page;
    List<Movie> movies;
    int totalPages;
    int totalResults;
    
    MoviePaginationResponse({this.dates, this.page, this.movies, this.totalPages, this.totalResults});

    factory MoviePaginationResponse.fromJson(Map<String, dynamic> json) => MoviePaginationResponse(
        dates: Dates.fromJson(json["dates"]),
        page: json["page"],
        movies: List<Movie>.from(json["results"].map((x) => Movie.fromJson(x))),
        totalPages: json["total_pages"],
        totalResults: json["total_results"],
    );

    Map<String, dynamic> toJson() => {
        "dates": dates.toJson(),
        "page": page,
        "movies": List<dynamic>.from(movies.map((x) => x.toJson())),
        "total_pages": totalPages,
        "total_results": totalResults,
    };

    // utils
    static MoviePaginationResponse fromString(String str) {
      return MoviePaginationResponse.fromJson(json.decode(str));
    } 

    static String objectToJson(MoviePaginationResponse data) {
      return json.encode(data.toJson());
    }
}
```

C. Crear service

C.1, `lib\src\services\MovieService.dart`

```dart
const String URL = 'https://api.themoviedb.org/3';
const String API_KEY = 'sacar de https://www.themoviedb.org/settings/api';
const String LANG = 'es-MX';
```

C.2, `lib\src\services\MovieService.dart`

```dart
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:peliculas/src/models/dtos/MoviePaginationResponse.dart';
import 'ServiceConstants.dart';

class MovieService {
  static Future<MoviePaginationResponse> getNowPlaying(int page) async {
    final requestUrl = '$URL/movie/now_playing?api_key=$API_KEY&language=$LANG&page=$page';
    var responseRaw = await http.get(requestUrl);
    print('Respuesta: ${responseRaw.body}');
    var moviePage = MoviePaginationResponse.fromString(responseRaw.body);
    return moviePage;
  }
}
```

D. Usar `lib\src\pages\HomePage.dart`

```dart
  Widget _buildSwipeTargetas() {
+   MovieService.getNowPlaying(1)
+     .then((MoviePaginationResponse moviesPage) {
+       print('Objeto: ${moviesPage.movies[0].originalTitle}'); // Objeto: Wonder Woman 1984
+     })
+   ;

    return CartsSwiper( peliculas: [1,2,3,4,5], );
  }
```

**Resultado** Estamos consumiendo la api y convirtiendola a objeto, listos
para usarlo en nuestra aplicacion.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 102. Mostrar póster de películas en el Swiper

**Objetivo:** Vamos a mostrar el swiper con las imagenes, de las peliculas
que consumimos arriba.

A, Creamos nuestro `CartsSwiper` a travez de un `FutureBuilder` para
poderlo construir cuando este lista la respuesta http

```dart
import 'package:flutter/material.dart';
import 'package:peliculas/src/models/dtos/MoviePaginationResponse.dart';
import 'package:peliculas/src/services/MovieService.dart';
import 'package:peliculas/src/widgets/CartsSwiper.dart';

class HomePage extends StatefulWidget {...}
class _HomePageState extends State<HomePage> {...}
  Widget _buildHomePageBody(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    final double height = _screenSize.height;
    return Container(
      child: Column(
        children: [
          Container( padding: EdgeInsets.only(top: 10), color: Colors.grey[300], width: double.infinity, height: height * 0.4,
+           child: _buildSwipeTargetas(),
            // swipe horizontal
          )
        ],
      ),
    );
  }

  Widget _buildSwipeTargetas() {
+   return FutureBuilder(
+     future: MovieService.getNowPlaying(1),
+     initialData: new MoviePaginationResponse(),
+     builder: (BuildContext context, AsyncSnapshot<MoviePaginationResponse> snapshot) {
+       if(!snapshot.hasData || snapshot.data.movies == null) {
+         return Center(child: Column( children: [ CircularProgressIndicator() ] ) );
+       } else {
+         return CartsSwiper(movies: snapshot.data.movies);
+       }
+     },
+   );
  }
}
```

B, Resivimos los objetos de peliculas, por lo que podemos tomar de aqui
esas imagenes

```dart
  import 'package:flutter/material.dart';
  import 'package:flutter_swiper/flutter_swiper.dart';
  import 'package:peliculas/src/models/entities/Movie.dart';
  import 'package:peliculas/src/services/MovieService.dart';

  class CartsSwiper extends StatelessWidget {    
    final List<Movie> movies;
    CartsSwiper({@required this.movies});
    @override
    Widget build(BuildContext context) {
      return Swiper(
        //layout: SwiperLayout.STACK, // necesita del "itemWidth" para que no falle
        //itemWidth: 200,
        itemCount: movies.length,
        viewportFraction: 0.6,
        scale: 0.7,
        //pagination: new SwiperPagination(), // mostrar puntos de abajo
        //control: new SwiperControl(),  // mostrar fleachas a los lados
        itemBuilder: (context, index){
          return ClipRRect( // para que las targetas tengan borde redondeado
            borderRadius: BorderRadius.circular(20),
            child: FadeInImage(
+             image: NetworkImage(MovieService.getPosterUrl(posterName: movies[index].posterPath)),
              placeholder: AssetImage('assets/img/no-image.jpg'),
            ),
          );
        },
      );
    }//fin metodo
  }
  
  static String getPosterUrl({@required String posterName}) {
    if(posterName.isEmpty) {
      return "http://ridauto.es/wp-content/uploads/2015/04/placeholder-600x400.png";
    } else {
      return "https://image.tmdb.org/t/p/w200/$posterName";
    }
  }
```

**Resultado:** Tenemos el siguiente swiper de peliculas

![25_swipe_peliculas](./img/25_swipe_peliculas.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Swipe Horizontal tradicional

A. Creamos `lib\src\widgets\MovieHorizontal.dart` encargado de construir
nuestro swipe horizontal

```dart
import 'package:flutter/material.dart';
import 'package:peliculas/src/models/entities/Movie.dart';
import 'package:peliculas/src/services/MovieService.dart';

class MovieHorizontal extends StatelessWidget {
  final List<Movie> movies;
  final double height;

  MovieHorizontal({@required this.movies, @required this.height});
  
  @override
  Widget build(BuildContext context) {    
    return Container(
      height: 150, // tamoaño de cada targeta
// componente que acomoda los elementos de forma horizontal
+     child: PageView(
        children: _targetasPeliculas(),
        controller: PageController(
          initialPage: 1,
// cuantas paginas mostrar al mismo tiempo          
+         viewportFraction: 0.25,
        ),
      ),
    );
  }

  List<Widget> _targetasPeliculas() {
    return movies.map((m) {
      print('Title: ${m.title}');
      return Container(
        margin: EdgeInsets.only(right: 15),
        decoration: BoxDecoration(border: Border.all(color: Colors.red), ),
        child: Column(
          children: [
//para hacer las esquinas redondas
+           ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: FadeInImage(
                placeholder: AssetImage('assets/img/no-image.jpg'),
                image: NetworkImage(MovieService.getPosterUrl(posterName: m.posterPath)),
                fit: BoxFit.cover,
                height: 120-2.0,
              ),
            ),
            Text(
              m.title, 
              //overflow: TextOverflow.ellipsis, // solucionar el overflowed
              style: TextStyle(fontSize: 10),
              maxLines: 2,
              textAlign: TextAlign.center,
            )
          ]
        ) 
      );
    }).toList();
  }
}
```

B. Agregamos nuestro `MovieHorizontal` dentro de su propio contenedor

```dart
import 'package:flutter/material.dart';
import 'package:peliculas/src/models/dtos/MoviePaginationResponse.dart';
import 'package:peliculas/src/services/MovieService.dart';
import 'package:peliculas/src/widgets/CartsSwiper.dart';
import 'package:peliculas/src/widgets/MovieHorizontal.dart';
class HomePage extends StatefulWidget { HomePage({Key key}) : super(key: key); @override _HomePageState createState() => _HomePageState(); }

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    final double height = _screenSize.height;
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Peliculas'),
          backgroundColor: Colors.indigoAccent,
          actions: [ IconButton( icon: Icon(Icons.search), onPressed: () { print('hello'); }) ],
         ),
         body: Container(
          decoration: BoxDecoration(
            //border: Border.all(color: Colors.orange ),
            color: Colors.grey[300]
          ),
          child: Column(
            children: [
              _buildSwipeTargetas(height: height*0.3),
+             _footer(height: height*0.27),
+             _footer(height: height*0.27),
            ],
          ),
        ),
       ),
    );
  }

  Widget _buildSwipeTargetas({double height}) {...}

+ Widget _footer({double height}) {
    return Container(
      width: double.infinity,
+     height: height,
      //decoration: BoxDecoration(border: Border.all()),
      child: Column(
+       crossAxisAlignment: CrossAxisAlignment.start, // pasar a la izq el texto
        children: [
          Container(padding: EdgeInsets.all(5), child: Text('Populares', style: TextStyle(fontSize: 18) )),
          FutureBuilder(
            future: MovieService.getPopulars(1),
            builder: (BuildContext context, AsyncSnapshot<MoviePaginationResponse> snapshot) {
              if(!snapshot.hasData || snapshot.data.movies == null) {//mostrar animacion decarga
                return Center(child: Column( children: [ CircularProgressIndicator() ] ) );
              } else {
                return Container(
//aqui se contrulle el swipe horizontal
+                 child: MovieHorizontal(movies: snapshot.data.movies, height:height),
                );
              }
            },
          ),
        ],
      ),
    );
  }//_footer()
}
```

**Resultado:** Tenemos el sig. swipe horizontal

![26_swipe_opcion_2_horizontal](./img/26_swipe_opcion_2_horizontal.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 108. TEORIA: Patrón Bloc y al manejo de estado de la aplicación

Bloc (Business Logic Component) este es un patron de diseño, que nos
sirve para centralizar toda la informacion (estados) de nuestra aplicacion

**Problema** Si queremos comunicar los dos componentes de abajo, ej. el
componente de la izq. tiene info. que el de la der. necesita, por lo que
tendriamos que subir la informacion a un componente que compartan, para
luego poderlo bajar. **LO CUAL** agrega cohecion a nuestro codigo, haciendo que se rompa mas facil
y informacion que un componente no necesita.

![27_patron_bloc_problema_que_soluciona](./img/27_patron_bloc_problema_que_soluciona.jpg)

**Solucion:** Agregamos un modulo que tenga nuestra informacion, que
puedan ver ambos y si un componente necesita info del otro, solo le
pedimos la info. a este y ya no tenemos que pasar la propiedad entre
los componentes.

![27_patron_bloc_solucion](./img/27_patron_bloc_solucion.jpg)

**Uso de Streams:** El patron BLoC los usa para que si cambia algun dato,
los componetes que esten usando este, se actualizen de forma automatica.

Tenemos 3 puntos importante

- Trabaja solo con entradas y salidas
- Para meter datos usaremos el `sink` y para salidas el `stream` (eventos de stream)
- Importante cerrar el stream cuando ya no lo estemos utilizando

**Recordar que:** Tenemos dos tipos de streams los `singleSuscription`
(solo uno a la vez puede escuchar) y los `broadcast` que varios pueden
escuchar al mismo tiempo.  

**Ejemplo escuchar salidas/entradas:** Podemos ver como se declaramos
el tipo de entrada y salida, el ej. maneja Strings pero puede ser de
cualquier tipo.

![28_streams_declarar_tipo_entrada_y_salida](./img/28_streams_declarar_tipo_entrada_y_salida.jpg)

**Cerrar un stream:** Como mencionamos debemos de cerrarlo, ya que estos
ocupan recursos.

![29_cerrar_stream](./img/29_cerrar_stream.jpg)

**Como lo usaremos en Flutter** Tenemos un Widget llamado `StreamBuilder`,
que es parecido al `FutureBuilder`, pero este usara un stream.

![30_streambuilder_teoria](./img/30_streambuilder_teoria.jpg)

**Ejemplo uso de StreamBuilder** Abajo podemos ver como un boton emite
un evento y un StreamBuilder esta escuchando para construir un nuevo
widget segun vaya entrando data.

![30_streambuilder_teoria_ejemplo](./img/30_streambuilder_teoria_ejemplo.jpg)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 109. Scroll-Infinito con Streams

**Problema:** Si vemos nuestro codigo estamos usamos una paginacion, y
solo estamos trayendo la primera pagina, al llegar al fin no pide la
segunda pagina, una solucion seria redibujar el swipe con la pagina2
pero eso se veria mal, por lo que veremos como usarlo con streams para
que se construlla poco a poco.

A, declarar stream (entrada y salida). `lib\src\services\MovieService.dart`.  
B, Cuando tengamos la respuesta agregamos la info. al stream (usamos la entrada)

```dart
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:peliculas/src/models/dtos/MoviePaginationResponse.dart';
import 'package:peliculas/src/models/entities/Movie.dart';
import 'ServiceConstants.dart';

class MovieService {
  
// Se encargara de aumentar cada vez que es llamado getPopulares para obtener la sig. pagina
+ int _popularsPage = 0;
// es el objeto que usara el Stream y por ende detectaremos cuando este cambie
+ final _page = new MoviePaginationResponse();

// el stream como tal
+ final _popularesStreamController = StreamController<MoviePaginationResponse>.broadcast();

// introducir informacion al stream
+ Function(MoviePaginationResponse) get popularesSink {
+   return _popularesStreamController.sink.add;
+ }

// recibir/escuchar peliculas
+ Stream<MoviePaginationResponse> get popularesStream {
+   return _popularesStreamController.stream;
+ }

//cerrar conexion
+ void dispose() {
+   _popularesStreamController?.close();
+ }
// Implementacion
  Future<MoviePaginationResponse> getNowPlaying(int page) async {...}

  Future<MoviePaginationResponse> getPopulars() async {
+   _popularsPage++;
    final requestUrlPopulars = '$URL/movie/popular?api_key=$API_KEY&language=$LANG&page=$_popularsPage';
    var responseRaw = await http.get(requestUrlPopulars);
    //print('Peticion getPopulars: ${responseRaw.statusCode}');

    MoviePaginationResponse moviePopularsPage = MoviePaginationResponse.fromString(responseRaw.body);
+   if(_page.movies == null) {
+     _page.movies = new List<Movie>();
+   }
+   _page.movies.addAll(moviePopularsPage.movies);
// Agregamos peliculas al rio
+   this.popularesSink(_page);
    return moviePopularsPage;
  }

  static String getPosterUrl({@required String posterName}) {...}
}
```

C, Sustituir en `lib\src\pages\HomePage.dart` el futureBuilder por el
`StreamBuilder`

- Como dejamos de iniciar el metodo en `future: MovieService.getPopulars(1),`
  demos de agregalo cuando se inicia la pagina.

```dart
...
class _HomePageState extends State<HomePage> {
  final _movieService = new MovieService();
  @override Widget build(BuildContext context) {
//Llamamos al la API el cual tambien se encarga de meter las peliculas al stream
+   _movieService.getPopulars();
    ...
  }

  Widget _footer({double height}) {
    return Container( width: double.infinity, height: height,
      child: Column( crossAxisAlignment: CrossAxisAlignment.start,
        children: [ Container(padding: EdgeInsets.all(5), child: Text('Populares', style: TextStyle(fontSize: 18) )),
// Sustituimos
-         //FutureBuilder(
+         StreamBuilder(
-           //future: MovieService.getPopulars(1),
+           stream: _movieService.popularesStream,
            builder: (BuildContext context, AsyncSnapshot<MoviePaginationResponse> snapshot) {
              if(!snapshot.hasData || snapshot.data.movies == null) { return Center(child: Column( children: [ CircularProgressIndicator() ] ) ); } 
              else { return Container( 
                child: MovieHorizontal(
                  movies: snapshot.data.movies, 
                  height:height,
// mas adelante lo agregamos para llamarlo cunado se termine el scroll
+                 onEndScroll: _movieService.getPopulars
                ),
              );
  } }, ), ], ), ); }//_footer()
```

<!-- detectar fin de scroll, termino scroll, termino del scroll final del scroll, final scroll, fin del scroll-->

D. En `lib\src\widgets\MovieHorizontal.dart` debemos de detectar fin del contenido
o del scroll

```dart
import 'package:flutter/material.dart';
import 'package:peliculas/src/models/entities/Movie.dart';
import 'package:peliculas/src/services/MovieService.dart';

class MovieHorizontal extends StatelessWidget {
  
  final List<Movie> movies;

  final double height;

// agregamos callback para que se ejecute al final del scroll
+ final Function onEndScroll;

// declaramos controlador para acceder a el
+ final pageViewCtrl = new PageController(
+   initialPage: 1,
+   viewportFraction: 0.25, // cuantas paginas mostrar al mismo tiempo
+ );

  MovieHorizontal({
    @required this.movies, 
    @required this.height,
+   @required this.onEndScroll}
  );
  
  @override
  Widget build(BuildContext context) {
    
+   pageViewCtrl.addListener(() {
//  detectar fin de scroll, termino scroll, termino del scroll final del scroll, final scroll, fin del scroll
+     double posActual = pageViewCtrl.position.pixels;
+     double finScroll = pageViewCtrl.position.maxScrollExtent;
+     if(posActual >= finScroll) {
+       onEndScroll();
+     }
+   });
    
    return Container(
      //height: this.height,
      height: 150, // tamoaño de cada targeta
      //decoration: BoxDecoration(border: Border.all(color: Colors.blue), ),
      child: PageView(
        children: _targetasPeliculas(),
// sustituimos el controlador anonimo, por el que declaramos
+       controller: pageViewCtrl,
      ),
    );
  }

  List<Widget> _targetasPeliculas() {
    return movies.map((m) {
      return Container( margin: EdgeInsets.only(right: 15), child: Column( children: [
            ClipRRect( borderRadius: BorderRadius.circular(15), child: FadeInImage( placeholder: AssetImage('assets/img/no-image.jpg'), image: NetworkImage(MovieService.getPosterUrl(posterName: m.posterPath)), fit: BoxFit.cover, height: 120, ), ),
            Text( m.title,  style: TextStyle(fontSize: 10), maxLines: 2, textAlign: TextAlign.center, ) ] )  ); })
      .toList();
  }
}
```

**Resultado:** Tenemos un infinite-scroll y a diferencia del anterior
este no se repinta todo, solo pinta lo que se le agrego cuando llego
al final del scroll.

[32_infinite_scroll_con_stream](./img/32_infinite_scroll_con_stream.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 111. Optimizaciones para nuestra aplicación, uso de builder

1. Cuidar que no se hagan varias peticiones al llegar al final del scroll
   ya que esto consume datos y son inecesarias estas

Otra es el hecho de que en `lib\src\widgets\MovieHorizontal.dart` en `Widget build(`
estamos usando un `child: PageView(`, lo que significa que va a pintar
todo lo que tenga sean mil pintara mil. Por lo que **DEBEMOS** de cambiar
al builder el cual va a **solo pintar lo que se ve**.

- Necesitaremos crear un metodo que crea el item individual
- s

```dart
...
class MovieHorizontal extends StatelessWidget {
! final List<Movie> movies;

  final pageViewCtrl = new PageController(...);
  MovieHorizontal({@required this.movies, @required this.height, @required this.onEndScroll});
  
  @override Widget build(BuildContext context) {
    
    pageViewCtrl.addListener(() {...});
    
    return Container( height: 150,
//sustituimos por el 'PageView.builder' el cual es mas optimizado
-    //child: PageView( este es es poco optimizado
+     child: PageView.builder(
-     //children: _targetasPeliculas(), // no lo pide el builder
// igual lo necesitamos para el 'builder' CUIDADO: no es obligatorio pero fallara en ejecucion si no lo ponemos
+       itemCount: movies.length,
//lo necesita el 'builder'
+       itemBuilder: (context, i) {
+         return _creatTargeta(movies[i]);
+       },
        controller: pageViewCtrl,
      ),
    );
  }

//lo emiminamos ya que lo coupabamos cuando no teniamos el builder
-  //List<Widget> _targetasPeliculas() { return movies.map((m) { }).toList(); }

// es lo del metodo que eliminamos solo sin lo que le dejamos
+ Widget _creatTargeta(Movie m) {
    return Container(
        margin: EdgeInsets.only(right: 15),
        //decoration: BoxDecoration(border: Border.all(color: Colors.red), ),
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: FadeInImage(
                placeholder: AssetImage('assets/img/no-image.jpg'),
                image: NetworkImage(MovieService.getPosterUrl(posterName: m.posterPath)),
                fit: BoxFit.cover,
                height: 120,
              ),
            ),
            Text(
              m.title, 
              //overflow: TextOverflow.ellipsis, // solucionar el overflowed
              style: TextStyle(fontSize: 10),
              maxLines: 2,
              textAlign: TextAlign.center,
            )
          ]
        ) 
      );
  }
}
```

**Resultado:** En telefonos mas pedorros correra mejor

<!--

#########################################

-->

----

<!--

#########################################

-->

### 112. Seleccionar item: Pasar argumentos de una página a otra usando el PushNamed

**Objetivo:** Ya tenemos nuestro scroll de elementos, pero ahora necesitamos
poder seleccionar cada uno de ellos, por lo que empezaremos por detectar
el tap, y pasar a otra pantalla pasando un valor, para saber que elmento
se selecciono.

**Nota:** Tenemos dos soluciones comunes, 1) crear nuestro Widget que
resiva por contructor la info que le queremos mandar y 2) pasarselo por
argumentos. Nosotros usaremos la opcion 2 para no tener que usar siempre
el constructor.

A. En `lib\src\widgets\MovieHorizontal.dart`

- Pasamos la targeta a una variable
- La envolvemos en un `GestureDetector`
- Agregamos evento onTap y dentro de este la navegacion junto con argumentos

```dart
  Widget _creatTargeta(Movie m, BuildContext context) {
// lo mismo de arriba solo guardado en variable
    final targeta = Container(...);

// agregar evento, añadir evento, detectar tap, detectar evento,
      return GestureDetector(
        child: targeta,
        onTap: () {
          print('mostrando pelicula: ${m.id}');
// enviar argumentos, enviar variable, pasar variable, navegar pasando variable
          Navigator.pushNamed(context, '/movie-details', arguments: m);
        },
      );
  }
}
```

B. Creamos nueva pantalla y lo agregamos en el arbol de rutas

```dart
import 'package:flutter/material.dart';
import 'package:peliculas/src/models/entities/Movie.dart';

class MovieDetails extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

// capturar argumentos, en este caso la pelicula, obtener argumentos
    final Movie movie = ModalRoute.of(context).settings.arguments;

    return Container(
      child: Scaffold(
        body: SafeArea(
          child: Text('${movie.originalTitle}'),
        )
      ),
    );
  }
}
```

**Resultado** Podemos navegar de A a B pasando un objeto de argumento.

![33_navegacion_con_argumentos.mp4](./img/33_navegacion_con_argumentos.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 113. Seleccionar item: Header bonito

**Objetivo** Enpezaremos a travajar con el diseño del header.

```dart
import 'package:flutter/material.dart';
import 'package:peliculas/src/models/entities/Movie.dart';
import 'package:peliculas/src/services/MovieService.dart';

class MovieDetails extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

// capturamos argumentos, en este caso la pelicula
    final Movie movie = ModalRoute.of(context).settings.arguments;

    return Scaffold(
//casi igual que un listScroll
      body: CustomScrollView(
// de esta manera creamos una lista de slivers que son widgets pero creo mas precontruidos
        slivers: [
          _buildAppBar(movie),
//De esta agregamos elementos normales (no-slivers) dentro del CustomScrollView
          SliverList(
            delegate: SliverChildListDelegate([ // el lo mismo que 'childern'
              SizedBox(height: 10),
              posterTitulo(movie, context),
              _descripcion(movie),
              _descripcion(movie),
              _descripcion(movie),
              _descripcion(movie),
            ]),
          )
        ],
      ),
    );
  }

  Widget _buildAppBar(Movie m) {
// deberia de hacer una animacion bonita al hacer scroll
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.indigoAccent,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          m.title,
          style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
        background: FadeInImage(
          image: NetworkImage(MovieService.getPosterUrl(posterName: m.backdropPath)),
          placeholder: AssetImage('assets/img/no-image.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  posterTitulo(Movie m, BuildContext cnt) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          Hero(
            tag: m.id,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: NetworkImage(MovieService.getPosterUrl(posterName: m.posterPath)),
                height: 150.0,
              ),
            ),
          ),
          SizedBox(width: 20.0),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(m.title, style: Theme.of(cnt).textTheme.headline6, overflow: TextOverflow.ellipsis ),
                Text(m.originalTitle, style: Theme.of(cnt).textTheme.subtitle1, overflow: TextOverflow.ellipsis ),
                Row(
                  children: <Widget>[
                    Icon( Icons.star_border ),
                    Text( m.voteAverage.toString(), style: Theme.of(cnt).textTheme.subtitle1 )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _descripcion( Movie pelicula ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Text(
        pelicula.overview,
        textAlign: TextAlign.justify,
      ),
    );
  }
}
```

**Resultado:** Tenemos el siguiente diseño del header. **Nota** devemos
tener suficiente contenido para hacer scroll y que el header se comporte
haci

![34_disenio_header](./img/34_disenio_header.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 114. Seleccionar item: Lista de actores

**Objetivo:** Dentro de los detalles pelicula, vamos a mostrar una lista
scrolable con los actores de la pelicula.

A, Creamos los objetos con quicktype

- `lib\src\models\entities\Cast.dart`
- `lib\src\models\dtos\CreditsResponse.dart`

B, Creamos servicio en `lib\src\services\MovieService.dart`

```dart
Future<CreditsResponse> getMovieCast(int idM) async {
  final requestUrl = '$URL/movie/$idM/credits?api_key=$API_KEY&language=$LANG';
  var responseRaw = await http.get(requestUrl);
  //print('Peticion getNowPlaying: ${responseRaw.statusCode}');
  
  return CreditsResponse.fromString(responseRaw.body);
}
```

C, En `lib\src\pages\MovieDetails.dart` agregamos el consumo y mostramos
la lista

```dart

import 'package:peliculas/src/models/entities/Cast.dart';
import 'package:peliculas/src/models/entities/Movie.dart';
import 'package:peliculas/src/services/MovieService.dart';

class MovieDetails extends StatelessWidget {

  final _movieService = new MovieService();

    @override
  Widget build(BuildContext context) {
    final Movie movie = ModalRoute.of(context).settings.arguments;
    return Scaffold( body: CustomScrollView( slivers: [ _buildAppBar(movie),
          SliverList(
            delegate: SliverChildListDelegate([ // el lo mismo que 'childern'
              SizedBox(height: 10), posterTitulo(movie, context), _descripcion(movie),
+             _buildListCast(movie.id),
              SizedBox(height: 40.0),
  ]), ) ], ), ); }

  Widget _buildAppBar(Movie m) {..}
  Widget posterTitulo(Movie m, BuildContext cnt) { ... }
  Widget _descripcion( Movie pelicula ) {...}

+ Widget _buildListCast(int idMovie) {
    return Container(
      height: 150,
      decoration: BoxDecoration( border: Border.symmetric(horizontal: BorderSide(color: Colors.grey) ), ), // agregar borde
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Actores', style: TextStyle(fontSize: 16)),
          Container(
            height: 125,
            child: FutureBuilder(
              future: this._movieService.getMovieCast(idMovie),
              builder: (context, AsyncSnapshot<CreditsResponse> snapshot) {
                if(snapshot.hasData || snapshot.data?.cast != null) {
// cUIDADO: no meter un scrollable dentro deotro escrolable, meter este en un container con altura definida
                  return ListView.builder(
                    padding: EdgeInsets.only(top: 0), // quitar espacio vacio, quitar separacion
                    itemCount: snapshot.data.cast.length,
                    itemBuilder: (context, i) {
                      //if(i>=10) {
                      return _buildActorCard(snapshot.data.cast[i]);
                    }
                  );
                } else {
                  return Center(child: Column( children: [ CircularProgressIndicator() ] ) );
                }
              },
            ),
          ),
        ],
      ),
    );
  }

+ Widget _buildActorCard(Cast cast) {
    return Container(
      //decoration: BoxDecoration(border: Border.all(color: Colors.red), ), // agregar borde
      child: Row(
        children: [
          FadeInImage(
            image: NetworkImage(MovieService.getPosterUrl(posterName: cast.profilePath)),
            placeholder: AssetImage('assets/img/no-image.jpg'),
            height: 100,
            fit: BoxFit.cover,
          ),
          SizedBox(width: 5.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Nombre: ${cast.name}'),
              Text('Como: ${cast.character}'),
              Text('Poularidad: ${cast.popularity.toStringAsFixed(1)}'),
              Text(''), Text(''),
            ],
          ),
          
        ],
      )
      //
    );
  }
```

**Resultado:** Tenemos la lista de actores en la cual podemos hacer scroll

![35_scroll_dentro_de_scroll](./img/35_scroll_dentro_de_scroll.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 118. Seleccionar item: Hero Animation

- Fuente: [hero-animations](https://flutter.dev/docs/development/ui/animations/hero-animations)

**Objetivo:** Vamos a realizar estra tranzaccion (ver fuente) para que
se vea chulo.

Pasos.

1. Identificar el elemento que queremos que se mueva en este caso la
   imagen de poster que tambien funciona como boton
2. Identificar donde queremos que vaya este elemento, en este caso la
   imagen de poster que esta al lado de la calificacion
3. A ambos elementos envolverlos en un Hero y a ambos ponerlo en un
   ID unico para ambos elementos.

- **Nota** Usaremos el ID de pelicula pero a futuro no podremos, por que
  tendremos dos botones con el mimo tag.

A. En `lib\src\pages\HomePage.dart` idenficamos el elemeto que queremos
que flote (en este caso la imagen del tap en `_creatTargeta().targeta`)

```dart
  Widget _creatTargeta(Movie m, BuildContext context) {
    final targeta = Container( margin: EdgeInsets.only(right: 15),
      child: Column(
        children: [
+         Hero(
+           tag: m.id, // debe ser unico en la pantalla
            child: ClipRRect( borderRadius: BorderRadius.circular(15),
              child: FadeInImage( placeholder: AssetImage('assets/img/no-image.jpg'), image: NetworkImage(MovieService.getPosterUrl(posterName: m.posterPath)), fit: BoxFit.cover, height: 120, ),
            ),
+         ),
    ...
  }
```

B, En `lib\src\pages\MovieDetails.dart` hacer lo mismo asegurandonos
que tengan el mismo tag.

```dart
 Widget posterTitulo(Movie m, BuildContext cnt) {
    return Container( padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
+         Hero(
+           tag: m.id,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image( image: NetworkImage(MovieService.getPosterUrl(posterName: m.posterPath)), height: 150.0, ),
            ),
+         ),
    ...
  }
```

**Resultado:** Tenemos una animacion que nos da la sensacion que es el
mismo elemnto, aun cuando trancionamos de pagina.

![36_transicion_hero](./img/36_transicion_hero.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 120. BuscarPelicula: Basico

**Objetivo:** Vamos a hacer una busqueda, sin salir de la pantalla, la
cual como vayamos metiendo texto se vayan reduciendo los resultados.

**Introduccion:** Su vemos `showSearch` ya esta implementado en flutter,
lo cual necesitaremos seguir las instrucciones para usarlo, lo cual consta
de un `SeachDelegate` la cual es una interface que debemos seguir.

1. Seguir la interface `SearchDelegate`

```dart
import 'package:flutter/material.dart';

class MySearchDelegate extends SearchDelegate {

  final todas = ['a1', 'aa2', 'aaa3', 'aaaa4', 'aaaaa5', 'b1', 'bb2', 'bbb3', 'c1', 'cc', 'dddd', 'eee' ];
  var _valSeleccionado = '';

//las acciones de los botones derechos de nuestro appBar 
// ej. un icono para limpiar el texto o cancelar
  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.cancel),
//Accion boton Der
        onPressed: () {
          super.query = '';
        },
      ),
    ];
  }

//Icono de la Izq. por ejemplo para regresar
  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
//Accion boton izq
        close(context, '');
      },
    );
  }

// las sugerencias, que se le muestra al usuario cuando escribe
  @override
  Widget buildSuggestions(BuildContext context) {

//finltramos resultados
    List<String> sugerencias = (super.query.isEmpty) ? todas :
      todas.where((p) => p.startsWith(query) ).toList();

//mostramos resultados
    return ListView.builder(
      itemCount: sugerencias.length,
      itemBuilder: (ctx, i) {
        return ListTile(
          leading: Icon(Icons.movie),
          title: Text(sugerencias[i]),
          onTap: () {
// valor seleccionado
            _valSeleccionado = sugerencias[i];
            super.showResults(context);
          },
        );
      },
    );
  }

// muestra la pantalla de resultados
  @override
  Widget buildResults(BuildContext context) {
    return Center(
      child: Container(
        height: 100, width: 100, color: Colors.green[100],
        child: Center(child: Text('Sel: $_valSeleccionado')),
      ),
    );
  }
}
```

B. Agregamos el metodo predefinido en nuestro boton de busqueda en mi
caso `lib\src\pages\HomePage.dart`

```dart
+ import 'package:peliculas/src/seach/MySearchDelegate.dart';
  class _HomePageState extends State<HomePage> {
  ....
    return Container(
      child: Scaffold( appBar: AppBar( title: Text('Peliculas'), backgroundColor: Colors.indigoAccent,
          actions: [ IconButton(  icon: Icon(Icons.search),
            onPressed: () {
+             showSearch(context: context, delegate: MySearchDelegate(), query: '');
            })
          ],
          ...
  }
```

**Resultado:** Ya tenemos idea de como se construlle una busqueda, con
lo siguiente construido.

![37_busqueda_introduccion](./img/37_busqueda_introduccion.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 123. BuscarPelicula: Buscar en themoviedb.org

**Objetivo:** Ya tenemos una idea de como crear la busqueda, ahora vamos
a perzonalizarlo a nuestro caso. Lo cual son pocos cambios en la misma

A, Agregamos servicio de busqueda

```dart
Future<MoviePaginationResponse> search(String query) async {
  final requestUrl = '$URL/search/movie?api_key=$API_KEY&language=$LANG&query=$query';
  var responseRaw = await http.get(requestUrl);
  
  return MoviePaginationResponse.fromString(responseRaw.body);
}
```

B, Quedaria asi nuestro

```dart
import 'package:flutter/material.dart';
import 'package:peliculas/src/models/dtos/MoviePaginationResponse.dart';
import 'package:peliculas/src/models/entities/Movie.dart';
import 'package:peliculas/src/services/MovieService.dart';

class MySearchDelegate extends SearchDelegate {

+ Movie _seleccionada;
+ final _movieService = new MovieService();

  @override List<Widget> buildActions(BuildContext context) {...}
  @override Widget buildLeading(BuildContext context) {...}

// las sugerencias, que se le muestra al usuario cuando escribe
  @override
  Widget buildSuggestions(BuildContext context) {
    if(super.query?.isEmpty ){
      return Container();
    } else {
+     return FutureBuilder(
        future: _movieService.search(super.query),
        builder: (BuildContext context, AsyncSnapshot<MoviePaginationResponse> snapshot) {
          if(!snapshot.hasData || snapshot.data.movies == null) {
            return Center(child: CircularProgressIndicator());
          }
+         return ListView.builder(
            itemCount: snapshot.data.movies.length,
            itemBuilder: (ctx, i) {
+             return ListTile(
                  leading: FadeInImage(
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  image: NetworkImage(MovieService.getPosterUrl(posterName: snapshot.data.movies[i].posterPath)),
                  width: 35,
                ),
                
                title: Text(snapshot.data.movies[i].title),
                onTap: () {
// valor seleccionado
+                 _seleccionada = snapshot.data.movies[i];
+                 _seleccionada.unique = '${_seleccionada.id}-search';
+                 super.close(context, null);
+                 Navigator.pushNamed(context, '/movie-details', arguments: snapshot.data.movies[i]);
                },
              );
            },
          );
        },
      );
    }
  }
//ya no lo usamos, podriamos usarlo.
  @override Widget buildResults(BuildContext context) { return Container(); }
}
```

**Resultado:** Tenemos una busqueda de peliculas.

![38_buscar_peliculas](./img/38_buscar_peliculas.gif)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Seccion 8: Diseños en Flutter

Veremos la creacion de los siguientes 3 diseños

![39_diseños_a_crear](./img/_4_disenios.JPG)

### Diseño basico

El siguiente diseño tenemos una vista con scroll, y que igual funciona
demodo horizontal

```dart
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {

  final titleStyle    = TextStyle( fontSize: 18.0, fontWeight: FontWeight.bold );
  final subTitleStyle = TextStyle( fontSize: 16.0, color: Colors.grey );
  final primaryColor  = Colors.blue;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            _buildImage(),
            _buildTitle(),
            _buildActions(),
            //tiene problemas para la vita horizontal
            //_wrapOnScroll([ _buildDescriptionArea(), _buildDescriptionArea(), _buildDescriptionArea(), ]),
            _buildDescriptionArea(), _buildDescriptionArea(), _buildDescriptionArea(), _buildDescriptionArea(),
          ],
        )
      )
    );
  }

  Widget _buildImage() {
    return Container(
      width: double.infinity,
      child: Image(
        image: NetworkImage('https://img.ev.mu/images/articles/960x/930210.jpg'),
        height: 200,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
      child: Row(
        children: [
          Expanded( // separar elementos, hacer que ocupe todo el espacio disponible, cubrir espacio disponible
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start, //alinear texto en columna, alinear a la izquerda
              children: [
                Text('Lago con un puente', style: titleStyle),
                SizedBox(height: 7),
                Text('Lago de casa', style: subTitleStyle),
              ],
            ),
          ),
          Icon(Icons.star, color: Colors.red, size: 30.0),
          Text('4.1', style: TextStyle(fontSize: 20.0))
        ],
      ),
    );
  }

  Widget _buildActions() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly, // repartir elementos
        children: [
          _buildCustomButton('Llamar', Icons.call),
          _buildCustomButton('Route', Icons.near_me),
          _buildCustomButton('Share', Icons.share),
        ],
      ),
    );
  }

  Widget _buildCustomButton(String text, IconData  icon) {
    return Column(
        children: [
          Icon(icon, color: primaryColor, size: 40),
          Text(text, style: TextStyle(color: primaryColor)),
        ]
    );
  }

  Widget _buildDescriptionArea() {
    const text = 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestiae inventore provident 
    sunt quae sint voluptas, vitae odit temporibus dolorem doloremque possimus? Ducimus, exercitationem. 
    Accusamus temporibus error officiis inventore soluta incidun  dolor, sit amet consectetur adipisicing 
    elit. Molestiae inventore provident suntt'; 
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      child: Text(text+'\n', textAlign: TextAlign.justify,)
    );
  }

  Widget _wrapOnScroll(List<Widget> v) {
    return Expanded(
      child: Scrollbar(
        child: ListView(
          scrollDirection: Axis.vertical,
          children: v,
        ),
      ),
    );
  }
}
```

**Resultado:** Tenemos la sig. vista

![14_cards_simple](./img/40_disenio_basico.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Diseño medio: imagen de fondo completa con boton para hacer scroll

Crearemos el siguiente diseño

```dart
import 'package:flutter/material.dart';

class ScrollPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.vertical,
        children: [
          _buildPage1(),
          _buildPage2(),
        ],
      ),

    );
  }

  Widget _buildPage1() {
    //return Image(image: AssetImage('assets/scroll.png'));
    return Stack(
      children: [
        _colorFondo(),
        _imagenFondo(),
        _textos()
      ],
    );
  }

  Widget _buildPage2() {
    return Stack(
      children: [
        _colorFondo(),
        Center(
          child: RaisedButton(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
              child: Text('Entrar', style: TextStyle(fontSize: 20)),
            ),
            color: Colors.amber[300],
            textColor: Colors.white,
            shape: StadiumBorder(),
            onPressed: () {}, // es necesario, por que si no se pintaran los estilos de disabled
          ),
        )
      ],
    );
  }

  Widget _colorFondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Color.fromRGBO(108, 192, 218, 1.0),
    );
  }

  Widget _imagenFondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Image(
        image: AssetImage('assets/scroll.png'),
        fit: BoxFit.cover, // que la imagen cubra todo el espacio disponible
      ),
    );
  }

  Widget _textos() {
    final estiloTexto = TextStyle(color: Colors.white, fontSize: 50.0);
    return SafeArea(
      child: Column(
        children: <Widget>[
          SizedBox(height: 20.0 ),
          Text('11°', style: estiloTexto ),
          Text('Miércoles', style: estiloTexto ),
          Expanded( child: Container() ), // separacion maxima, separar elementos lo mas posible
          Icon( Icons.keyboard_arrow_down, size: 70.0, color: Colors.white )
        ],
      ),
    );
  }
}
```

**Resultado:** Podemos trancicionar entre ambas, con un scroll con iman,
para que quede exactamente mostrando una pantalla u otra.

![41_desenio_medio](./img/41_desenio_medio.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Diseño Avanzado: gradientes

**Intro:** Abajo vemos como funcionan los gradientes

![42_LinearGradient_cordenadas](./img/42_LinearGradient_cordenadas.JPG)

**Objetivo:** Vamos a crear el siguiente diseño.

```dart

import 'dart:ui';

import 'package:flutter/material.dart';

class GradosPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
      Stack( // en este las cosas se ponenen una ensima de la otra
        children: [
          _fondoApp(),
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildTitle(),
                _buildBottoms(),
              ],
            ),
          ),
        ]
      ),
      bottomNavigationBar: _buildCustomNavbar(context),
    );
  }

  Widget _fondoApp() {
    final gradient = Container(
// ocupar toda la pantalla
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          //begin: Alignment.topCenter, end: Alignment.bottomCenter, // aqzul abajo rojo arriba
          begin: FractionalOffset(0.0, 0.5),
          end: FractionalOffset(0.0, 1.0),
          colors: [
            Color.fromRGBO(52, 54, 101, 1),
            Color.fromRGBO(35, 37, 57, 1),
          ]
        )
      ),
    );

    final pinkBox = Transform.rotate(
// girar elemento
      angle: 3.1416/4,
      child: Container(
        height: 310,
        width: 310,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(80),
          gradient: LinearGradient(
            colors: [
              Color.fromRGBO(236, 98, 188, 1),
              Color.fromRGBO(241, 142, 172, 1)
            ]
          ),
        ),
      )
    );

    return Stack(
      children: [
        gradient,
//mover elemento de posicion
        Positioned(
          top: -90,
          child: pinkBox,
        )
      ],
    );
  }

  Widget _buildTitle() {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.only(left: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Text('Trandiciones clasicas', 
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.white)),
            SizedBox(height: 15),
            Container(
              width: 210,
              child: Text(
                'Las mas usadas en el mundo del diseño, por expertos del UI/UX', 
                style: TextStyle(color: Colors.grey[300]))
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCustomNavbar(BuildContext context) {
    
    return Theme(
//cambiar el color a elemento que no se puede, no se puede cambiar color, sin opcion para cambiar el color
      data: Theme.of(context).copyWith(
        canvasColor: Color.fromRGBO(55, 57, 84, 1.0),
        primaryColor: Colors.pinkAccent,
// cambiar de color a icono
         textTheme: Theme.of(context).textTheme
          .copyWith( caption: TextStyle( color: Color.fromRGBO(116, 117, 152, 1.0) ) )
      ),
      child: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            title: Container()  // ignore: deprecated_member_use
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.pie_chart_outlined),
            title: Container()  // ignore: deprecated_member_use
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.supervised_user_circle),
            title: Container() // ignore: deprecated_member_use
          ),
        ],
      ),
    );
  }

  Widget _buildBottoms() {
    return Column(
      children: [ 
        SizedBox(height: 20),
        Table(
          //border: TableBorder.all(width: 1),
          children: <TableRow>[
            TableRow(
              children: [
                _buildButtom('Texto 1', Icons.access_alarm_outlined, Colors.blue),
                _buildButtom('Texto 2', Icons.access_alarm_outlined, Colors.purple),
              ]
            ),
            TableRow(
              children: [
                _buildButtom('Texto 3', Icons.access_alarm_outlined, Colors.pinkAccent),
                _buildButtom('Texto 4', Icons.access_alarm_outlined, Colors.orange),
              ]
            ),
            TableRow(
              children: [
                _buildButtom('Texto 3', Icons.access_alarm_outlined, Colors.pinkAccent),
                _buildButtom('Texto 4', Icons.access_alarm_outlined, Colors.orange),
              ]
            ),
            TableRow(
              children: [
                _buildButtom('Texto 3', Icons.access_alarm_outlined, Colors.pinkAccent),
                _buildButtom('Texto 4', Icons.access_alarm_outlined, Colors.orange),
              ]
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildButtom(String texto, IconData icono, Color color) {
    return Opacity(
      opacity: 0.8,
        child: Container(
          height: 120.0,
          margin: EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            color: Color.fromRGBO(62, 66, 107, 0.7),
            borderRadius: BorderRadius.circular(10.0)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox( height: 5.0 ),
              CircleAvatar(
                backgroundColor: color,
                radius: 35.0,
                child: Icon( icono, color: Colors.white, size: 30.0 ),
              ),
              Text( texto , style: TextStyle( color: color )),
              SizedBox( height: 5.0 )
            ],
          ),
 
        ),
    );
  }
}
```

**Resultado** Tenemos el siguiente diseño

![43_disenio_avanzado](./img/43_disenio_avanzado.png)

<!--

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Sección 9: QRScanner - SQLite

Estos son algunos de los temas principales de la sección:

- Lanzar la cámara y leer un código QR
- Almacenarlo en SQLite
- CRUD hacia SQLite - Almacenamiento interno
- Uso de mapas
- GoogleMaps
- Marcadores
- Abrir URLs
- Provider como gestor de estado
- Y más

El objetivo es crear una aplicación, usando QR y SQLite para guardar
los QR que se an escaneado, y otras operaciones CRUD con estos.  
Tambien detectara si es un enlace (ira a la url), mapa (mostrara un mapa)

A. Creamos la base de nuestra aplicacion

```r
lib/main.dart
lib/src/
  MyApp.dart
lib/src/pages/
  HomePage.dart
  MapPage.dart # lo checaremos al final
lib/src/widgets/
  CustomFloatingButtom.dart
  CustomNavbar.dart
```

A. CustomNavbar

```dart
import 'package:flutter/material.dart';

class CustomNavbar extends StatelessWidget {
  
  const CustomNavbar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      elevation: 0, // sombreado en el BottomNavigationBar sombra BottomNavigationBar sombreado BottomNavigationBar
      currentIndex: 0,
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.map), label: 'Mapa',),
        BottomNavigationBarItem(icon: Icon(Icons.compass_calibration), label: 'Direcciones',),
      ],
    );
  }
}
```

B. CustomFloatingButtom

```dart
import 'package:flutter/material.dart';

class CustomFloatingButtom extends StatelessWidget {
  const CustomFloatingButtom({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.filter_center_focus),
      elevation: 0,
      onPressed: () {

      },
    );
  }
}
```

C, HomePage

```dart
import 'package:flutter/material.dart';
import 'package:qr_sqlite/src/widgets/CustomFloatingButtom.dart';
import 'package:qr_sqlite/src/widgets/CustomNavbar.dart';

class HomePage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historial'),
        elevation: 0,
        actions: [
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: () {}
          )
        ],
      ),
      body: Text('Home'),
      bottomNavigationBar: CustomNavbar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: CustomFloatingButtom(),
    );
  }
}
```

D, MyApp

```dart
import 'package:flutter/material.dart';
import 'package:qr_sqlite/src/pages/HomePage.dart';
import 'package:qr_sqlite/src/pages/MapPage.dart';
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      initialRoute: 'home',
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
       'home' : (ctx) => HomePage(),
       'map'  : (ctx) => MapPage(),
     },
    );
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 149. Tema de la aplicación (estilos globales)

Ya tenemos temas por defecto en flutter, y podemos aplicarlos de la sig.
manera

```dart
import 'package:flutter/material.dart';
import 'package:qr_sqlite/src/pages/HomePage.dart';
import 'package:qr_sqlite/src/pages/MapPage.dart';
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp( title: 'QR-SQLite', initialRoute: 'home', debugShowCheckedModeBanner: false, routes: <String, WidgetBuilder>{...},
// todo el tema por defecto
//   theme: ThemeData.dark(), TAMA POR DEFECTO
+    theme: ThemeData.dark(),
//PERSONALIZAMOS ALGUN TEMA
//   theme: ThemeData.dark().copyWith(
//     primaryColor: Colors.teal[700],
//     floatingActionButtonTheme: FloatingActionButtonThemeData(
//       backgroundColor: Colors.teal[300],
//     )
     
    );
  }
}
```

**Resultado:** Tenemos la siguiente look

![44_Inicio_app_sqlite_maps](./img/44_Inicio_app_sqlite_maps.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 150. Cambiar Panel: Definiendo escenario/problema

**Objetivo:** Queremos cambiar la pantalla cuando se precione en alguno
de los botones que tenemos abajo, pero sin cambiar de pagina o hacer
una transaccion, **solo cambiar el contenido**.

A, Creamos los diferentes paneles/contenidos que quereos mostrar

**Nota:** Los "Panels" son los diferentes contenidos que se mostraran

A1, `lib\src\pages\panels\DireccionsPanel.dart`

```dart
import 'package:flutter/material.dart';
class DireccionsPanel extends StatelessWidget {
  const DireccionsPanel({Key key}) : super(key: key);
  @override Widget build(BuildContext context) {
    return Container( child: Center( child: Text('Lista de direcciones'), ), );
  }
}
```

A2, `lib\src\pages\panels\MapsPanel.dart`

```dart
class MapsPanel extends StatelessWidget {
  const MapsPanel({Key key}) : super(key: key);
  @override Widget build(BuildContext context) {
    return Center( child: Text('Lista de mapas'), );
  }
}
```

B. Creamos un nuevo StatelesWidget dentro de `lib\src\pages\HomePage.dart`
que pueda cambiar de widget que retrna segun un condicional.

```dart
...
class HomePage extends StatelessWidget {
  
  @override Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(...),
// este nos dara contenido diferente segun un condicional
+     body: _HomePageBody(),
      ...
    );
  } 
}

class _HomePageBody extends StatelessWidget {
  const _HomePageBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var currentIndex = 0;

// segun cambie 'currentIndex' (ahorita solo directo al codigo) cambiara
// de panel que cambia
+   switch (currentIndex) {
      case 0:
        return MapsPanel();
      break;
      case 1:
        return DireccionsPanel();
      break;
      default:
        return MapsPanel();
    }
  }
}
```

C. En `lib\src\widgets\CustomNavbar.dart` capturamos la opcion que preciono el usuario

```dart
import 'package:flutter/material.dart';

class CustomNavbar extends StatefulWidget {...}

class _CustomNavbarState extends State<CustomNavbar> {
+ var currentPanel = 0;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar( elevation: 0,  currentIndex: currentPanel, items: [...],
// capturamos el indez donde tenemos nuestro menu de navegacion de abajo
+     onTap: (int optionPresed) {
+       setState(() {
+         currentPanel = optionPresed;
+         print('currentPanel: $currentPanel');
+       });
+     },
    );
  }
}
```

<!-- cambiar de contenido, manejo de paneles, uso de paneles-->

**Problema:** ¿como cambiamos de panel segun lo precionado en otro
widget que esta en otra clase/archivo?

Solucion aqui abajo

<!--

#########################################

-->

----

<!--

#########################################

-->

### 151. Cambiar Panel: Solucion con gestor de estado provider

<!-- intercambiar informacion entre widgets comunicacion widgets, comunicacion entre widgets
pasar informacion entre widgets.
-->

- Fuente [provider](https://pub.dev/packages/provider)

**Introducion** Arriba tenemos el problema de como intercambiar info. entre
widgets, para en este caso mostrar un contenido u otro segun un menu.

**Solucion** Usaremos un gestor de estados (en este caso `provider`) para
tener centralizada la informacion, y que en si no usar dos variables que
tengan la misma informacion, si no que estemos usando la misma variable
en ambos lugares.

A. Instalamos `provider`

```yaml
 dependencies:
  flutter:
    sdk: flutter
  cupertino_icons: ^1.0.0
+ provider: ^4.3.2+3
```

B, Creamos nuestro estado `lib\src\providers\UIStateProvider.dart`: Notar
que usamos `notifyListeners` en el set, que es el que notificara los
cambios que re pintar.

```dart
import 'package:flutter/material.dart';

class UIStateProvider extends ChangeNotifier {

  int _homeCurrentPanel = 1;

  int get homeCurrentPanel {
    return this._homeCurrentPanel;
  }

  set homeCurrentPanel(int homeCurrentPanelIn) {
    this._homeCurrentPanel = homeCurrentPanelIn;
+   super.notifyListeners();
  }
}
```

C, Agregamos el estado a nuestro `MaterialApp` (`lib\src\MyApp.dart`)

```dart
+ import 'package:provider/provider.dart';
+ import 'package:qr_sqlite/src/providers/UIStateProvider.dart';
  
  class MyApp extends StatelessWidget {
    @override Widget build(BuildContext context) {
//incluimos el estado que creamos
+     return MultiProvider(
+       providers: [
+         ChangeNotifierProvider(
+           create: (ctx) => new UIStateProvider()
+         )
+       ],
        child: MaterialApp(
          title: 'QR-SQLite'...
```

D, Cambiamos nuestro valor usando nuestro provider en `lib\src\widgets\CustomNavbar.dart`

```dart
  import 'package:flutter/material.dart';
+ import 'package:provider/provider.dart';
+ import 'package:qr_sqlite/src/providers/UIStateProvider.dart';

  class CustomNavbar extends StatefulWidget {
    const CustomNavbar({Key key}) : super(key: key);
    @override _CustomNavbarState createState() => _CustomNavbarState();
  }

  class _CustomNavbarState extends State<CustomNavbar> {
    var currentPanel = 0;

    @override
    Widget build(BuildContext context) {
// capturar estado, traer estado.
+     final uiStateProvider = Provider.of<UIStateProvider>(context);
      return BottomNavigationBar(
        elevation: 0, // sombreado en el BottomNavigationBar sombra BottomNavigationBar sombreado BottomNavigationBar
#       currentIndex: uiStateProvider.homeCurrentPanel,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.map), label: 'Mapa',),
          BottomNavigationBarItem(icon: Icon(Icons.compass_calibration), label: 'Direcciones',),
        ],
        onTap: (int optionPresed) {
          setState(() {
// modificamos el estado y en otro lugar se actualizara 
#           uiStateProvider.homeCurrentPanel = optionPresed;
          });
        },
      );
    }
  }
```

F, Donde necesitamos este valor lo usamos desde el estado que creamos
en `lib\src\pages\HomePage.dart`

```dart
import 'package:provider/provider.dart';
import 'package:qr_sqlite/src/providers/UIStateProvider.dart';

class HomePage extends StatelessWidget {...}

class _HomePageBody extends StatelessWidget {
  const _HomePageBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
//capturar el estado que incluimos en 'MyApp'
+   final uiStateProvider = Provider.of<UIStateProvider>(context);

#   switch (uiStateProvider.homeCurrentPanel) {
      case 0:
        return MapsPanel();
      break;
      case 1:
        return DireccionsPanel();
      break;
      default:
        return MapsPanel();
    }
  }
}
```

**Resultado:** Podemos cambiar de panel en nuestro home

![45_CAMBIAR_DE_PANEL](./img/45_CAMBIAR_DE_PANEL.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 154. Leer un código QR

<!-- generar qr online, generador de QR online, generador online de QR -->

- Fuente:
  - [Libreria para leer de QR](https://pub.dev/packages/flutter_barcode_scanner)
  - [pagina para generar QR](https://www.qrcode.es/es/generador-qr-code/)

A, Agregamos libreria

```yaml
dependencies:
  flutter:
    sdk: flutter
  cupertino_icons: ^1.0.0
+ flutter_barcode_scanner: ^1.0.2
```

B, Agregamos el metodo para escaneo en nuestro floatingButtom

```dart
+ import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
  class CustomFloatingButtom extends StatelessWidget {
    const CustomFloatingButtom({Key key}) : super(key: key);
    @override Widget build(BuildContext context) {
      return FloatingActionButton( child: Icon(Icons.filter_center_focus), elevation: 0,
        onPressed: () async {
// regresara un "-1" (sin commllas) si cancelamos el escaneo
+         String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode('#3d8bef', 'Cancelar', false, ScanMode.QR);
+         print(barcodeScanRes);
        },
      );
    }
  }
```

**Resultado:** Al pusar nuestro floatingButtom se mostrara, nos pedira
permisos para usar la camara, y podremos con esta escanear un codigo QR.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 156. Modelo para manejar los Scans

**Objetivo:** Nuestros Scans los vamos a mover como objetos, para luego
guardarlos en SQLite para eso usaeremos el siguiente json

```json
{ "id": 1, "type": "http", "value": "http://mario1.com" }
```

A, Con `quicktype.io` creamos los objetos

```dart
import 'dart:convert';

import 'TypeScanEnum.dart';
import 'package:meta/meta.dart';

class ScanModel {

  int id;
  TypeScan type;
  String value;
  
  ScanModel({ this.id, this.type, @required this.value, });

  factory ScanModel.fromJson(Map<String, dynamic> json) => ScanModel(
    id: json["id"],
    type: typeValues.map[json["type"]],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "type": typeValues.reverse[type],
    "value": value,
  };

//Arreglo de objetos
  // static List<ScanModel> scanModelFromJson(String str) {
  //   return List<ScanModel>.from(json.decode(str).map((x) => ScanModel.fromJson(x)));
  // } 

  // String scanModelToJson(List<ScanModel> data) { 
  //   return json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
  // }

//un solo objeto
  ScanModel scanModelFromJson(String str) {
    return ScanModel.fromJson(json.decode(str));
  }

  String scanModelToJson(ScanModel data) {
    return json.encode(data.toJson());
  };
}

enum TypeScan { HTTP, GEO }
final typeValues = EnumValues({ "geo": TypeScan.GEO, "http": TypeScan.HTTP });
class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;
  EnumValues(this.map);
  Map<T, String> get reverse {
    if (reverseMap == null) {
        reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 157. SQFLite Crear DB y Tabla

<!-- libreria sqlite libreria -->

- Fuente:
  - [sqlitebrowser](https://sqlitebrowser.org/dl/)
  - [sqflite](https://pub.dev/packages/sqflite#-readme-tab-)
  - [path_provider](https://pub.dev/packages/path_provider#-installing-tab-)

**Introduccion** usaremos las siguientes herramientas

- `path_provider`: Para manejo de rutas lo cual nos ayudara a conseguir
  la ruta donde esta guardada nuestra BD en el telefono, porque varia
  ya sea android/ios, donde esta instalada la app, etc
- `sqflite`: Libreria para manejo de BD SQlite

A, Creamos un singleton para inicializar nuestra BD en el telefono. y
la creacion de nuesta BD y la tabla.

```dart
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class SqliteConfig {

  static Database _database;

  //singleton
  static final db = SqliteConfig._();
  SqliteConfig._();
  Future<Database> get database async {
    if(_database != null) {
      return _database;
    } else {
      _database = await initDB();
      return _database;
    }
  }

  Future<Database> initDB() async {
    // path donde se almacena nuesta D.B.
    Directory documentsDir = await getApplicationDocumentsDirectory();
    final path = join(documentsDir.path, 'MyDB.db');

    return openDatabase(
      path,
      version: 1, // importante para aplicar cambios del schema
      onOpen: (db) {}, // se ejecuta cuando ya se creo y esta disponible ls BD
// configurar la creacion de la BD
      onCreate: (Database db, int version) async { // version es el mismo de arriba
        await db.execute('''
          CREATE TABLE Scans(
            id INTEGER PRIMARY KEY,
            type TEXT,
            valor TEXT,
          );
        ''');
        print('BD Creada en: $path');
      }
    );
  }
}
```

B, La mandamos a llamar solo para que se cree

```dart
  import 'package:qr_sqlite/src/DAOs/SqliteConfig.dart';
  ...
  @override Widget build(BuildContext context) {
    ...
+   SqliteConfig.db.database;
    ...
```

***Resultado:** al ejecutar deberiamos de ver en la consola algo
parecido a, lo que significa que se creo nuestra BD y no fallo la
creacion de nuestra tabla.

```r
BD Creada en: /user/0/com.algo.com/MyDB.db
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 159. SQFLite - Crear registros

**Objetivo** Crear el metodo para la insercion de un nuevo registro, los
cuales tenemos dos formas, desde SQL plano, o desde un MAP para construir
el SQL de forma automatica.

```dart
import 'package:qr_sqlite/src/models/ScanModel.dart';
import 'SqliteConfig.dart';

class ScanDAO {
  
  static Future<int> addRaw(ScanModel newScan) async {
    final db = await SqliteConfig.db.database;

    final id    = newScan.id;
    final type  = newScan.type;
    final value = newScan.value;

    final sqlIsert = 'INSERT INTO Scans(id, type, value) VALUES($id, $type, $value)';
    int idAsigned = await db.rawInsert(sqlIsert);

    return idAsigned;
  }

// es lo mismo que lo de arriba
  static Future<int> add(ScanModel newScan) async {
    final db = await SqliteConfig.db.database;
    // regresa el id asignado en la BD
    int idAsigned = await db.insert('Scans', newScan.toJson());
    return idAsigned;
  }
}
```

B, Usamos el metodo, por ahora solo provamos

```dart
ScanDAO.addScan(new ScanModel(id: 1, type: TypeScan.HTTP, value:'http://mario.com'))
  .then((resp) => { 
    print('Insertado: $resp')
  }
);
```

**Resultado:** Hicimos una insercion,

<!--

#########################################

-->

----

<!--

#########################################

-->

### 160. SQFLite - Obtener registros (byId, byType, All)

**Objetivo:** Consultar un registro, igual que arriba podemos usar el
`rawQuery`, pero de la sig. manera es mas comodo.

```dart
static Future<ScanModel> getScanById(int id) async {
  final db = await SqliteConfig.db.database;
  List<Map<String, dynamic>> respQueryById = await db.query('Scans', where: 'id = ?', whereArgs: [id]  );
  
  if(respQueryById.isNotEmpty) {
    return ScanModel.fromJson(respQueryById.first);
  } else {
    return null;
  }
}

static Future<List<ScanModel>> getAll() async {
  final db = await SqliteConfig.db.database;
  List<Map<String, dynamic>> respQueryAll = await db.query('Scans');
  
  if(respQueryAll.isNotEmpty) {
    List<ScanModel> scans = respQueryAll
      .map((Map<String, dynamic> row) => ScanModel.fromJson(row) ).toList()
    ;
    return scans;
  } else {
    return [];
  }
}


static Future<List<ScanModel>> getByType(TypeScan type) async {
  final db = await SqliteConfig.db.database;
  List<Map<String, dynamic>> respQueryByType = await db.query('Scans', where: 'type = ?', whereArgs: [typeValues.reverse[type]]);
  
  if(respQueryByType.isNotEmpty) {
    List<ScanModel> scans = respQueryByType
      .map((Map<String, dynamic> row) => ScanModel.fromJson(row) ).toList()
    ;
    return scans;
  } else {
    return [];
  }
}
```

Usamos el metodo

```dart
ScanDAO.getScanById(id)
  .then((ScanModel scanFinded) => { 
    print('Consultado: $scanFinded')
  }
);

ScanDAO.getAll()
  .then((List<ScanModel> scans) => {
    print('Todos: $scans')
  })
;

ScanDAO.getByType(TypeScan.GEO)
  .then((List<ScanModel> scans) => {
    print('TipoHttp: $scans')
  })
;
```

**Resutado:** Deberiamos poder ver el registro con el is señalado.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 161. SQFLite - UpdateById, DeleteById, DeleteAll

Declaracion

```dart
static Future<int> updateById(ScanModel updateScan) async {
  final db = await SqliteConfig.db.database;
  // CUIDADO: Si no metemos el where sobre-escribe todos
  int respInsert = await db.update('Scans', updateScan.toJson(), where: 'id = ?', whereArgs: [updateScan.id]);
  return respInsert;
}


static Future<int> deleteById(int id) async {
  final db = await SqliteConfig.db.database;
  // CUIDADO: Si no metemos el where borrara todos
  int respInsert = await db.delete('Scans', where: 'id = ?', whereArgs: [id]);
  return respInsert;
}


static Future<int> deleteAll() async {
  final db = await SqliteConfig.db.database;
  int respInsert = await db.delete('Scans');
  return respInsert;
}
```

Uso

```dart
final updateScan = new ScanModel(id: 1, type: TypeScan.GEO, value:'http://actualizaco2.com');
ScanDAO.updateById(updateScan)
  .then((int rowsUpdated) => {
    print('rowsUpdated: $rowsUpdated')
  })
;

ScanDAO.deleteById(4)
  .then((int rowsDelete) => {
    print('Eliminados: $rowsDelete')
  })
;

ScanDAO.deleteAll()
  .then((int rowsUpdated) => {
    print('rowsDeleted: $rowsUpdated')
  })
;
```

**Resultado:** Tenemos las operaciones dichas

<!--

#########################################

-->

----

<!--

#########################################

-->

### 163. ScanProvider - Centralizar la información de los Scans

**Introduccion** Los Scans en la bd van a tener efecto sobre nuestra UI,
la cual esta compuesta de varios Widgets separados, por ejemplo, la
lista donde mostramos los registros tipo GEO, y la otra tipo HTTP el boton
para eliminar todos debe estar activo/desactivo segun tengamos registros etc.
Y como vimos es conveniente usar un solo sitio para escuchar/notificar
para hacer cambios en la BD.

A, Creamos el estateProvider, que es el que gestionara lo que hay en la
BD con lo que hay en la UI, en `lib\src\providers\ScanListOnUIStateProvider.dart`

```dart
import 'package:flutter/material.dart';
import 'package:qr_sqlite/src/DAOs/ScanDAO.dart';
import 'package:qr_sqlite/src/models/ScanModel.dart';

class ScanListOnUIStateProvider extends ChangeNotifier {

  List<ScanModel> scansOnUI = [];
  var selectedType = TypeScan.GEO;

  void addScan(String value) async {
    final newScan = ScanModel(value: value, type: selectedType);
    // regresa el ID asignado
    int id = await ScanDAO.add(newScan);
    newScan.id = id;
    this.scansOnUI.add(newScan);
    notifyListeners();
  }

  void loadScans() async {
    final scansFinded = await ScanDAO.getAll();
    this.scansOnUI = [... scansFinded];
    notifyListeners();
  }

  void loadScansByType(TypeScan type) async {
    final scansFinded = await ScanDAO.getByType(type);
    this.scansOnUI = [... scansFinded];
    selectedType = type;
    notifyListeners();
  }

  void deleteById(int id) async {
    await ScanDAO.deleteById(id);
    this.loadScansByType(selectedType);
    // notifyListeners(); se canga en el "loadScansByType"
  }

  void deleteAll() async {
    await ScanDAO.deleteAll();
    this.scansOnUI.clear();
    notifyListeners();
  }
}
```

B, Agregamos el estado a nuestro material app ``

```dart
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
//incluimos el estado que creamos
    return MultiProvider(
      providers: [
        ChangeNotifierProvider( create: (ctx) => new UIStateProvider() ),
+       ChangeNotifierProvider( create: (ctx) => new ScanListOnUIStateProvider() )
      ],
      child: MaterialApp(
...
```

C, En el home `lib\src\pages\HomePage.dart` cambiamos el tipo de Scan
que vamos a agregar.

```dart
class _HomePageBody extends StatelessWidget {
  const _HomePageBody({Key key}) : super(key: key);
  @override Widget build(BuildContext context) {
//capturamos el estado que incluimos en 'MyApp'
    final uiStateProvider = Provider.of<UIStateProvider>(context);
+   final uiListScans = Provider.of<ScanListOnUIStateProvider>(context);

    switch (uiStateProvider.homeCurrentPanel) {
      case 0:
+       uiListScans.selectedType = TypeScan.GEO;
        return HomeMapsPanel();
      break;
      case 1:
+       uiListScans.selectedType = TypeScan.HTTP;
        return HomeDireccionsPanel();
      break;
      default:
+       uiListScans.selectedType = TypeScan.GEO;
        return HomeMapsPanel();
    }
  }
}
```

D. Para agregar un nuevo Scan `lib\src\widgets\CustomFloatingButtom.dart`

```dart
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
+ import 'package:provider/provider.dart';
+ import 'package:qr_sqlite/src/providers/ScanListOnUIStateProvider.dart';
+ import 'package:qr_sqlite/src/providers/UIStateProvider.dart';

class CustomFloatingButtom extends StatelessWidget {
  const CustomFloatingButtom({Key key}) : super(key: key);
  @override Widget build(BuildContext context) {
    return FloatingActionButton( child: Icon(Icons.filter_center_focus), elevation: 0,
      onPressed: () async {
        // como no redibujamos nada le ponemos false
//capturamos el estado
+       final uiListScans = Provider.of<ScanListOnUIStateProvider>(context, listen: false);
+       final uiStateProvider = Provider.of<UIStateProvider>(context, listen: false);

//lo dejamos mienstras para hacer pruebas
      //String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode('#3d8bef', 'Cancelar', false, ScanMode.QR);
        String barcodeScanRes = (uiStateProvider.homeCurrentPanel == 0) ? 'geo:111,111' : 'http://mario.com';
// se encargara de notificar el cambio en la pantalla
+       uiListScans.addScan(barcodeScanRes);
      },
    );
  }
}
```

E, Cuando carge el Panel le ponemos el tipo de Scan que cargara/mostrara

```dart
import 'package:flutter/material.dart';
import 'package:qr_sqlite/src/models/ScanModel.dart';
+ import 'package:provider/provider.dart';
+ import 'package:qr_sqlite/src/providers/ScanListOnUIStateProvider.dart';

class HomeDireccionsPanel extends StatelessWidget {
  const HomeDireccionsPanel({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pColor = Theme.of(context).primaryColor;
+   final uiListScans = Provider.of<ScanListOnUIStateProvider>(context);
+   uiListScans.loadScansByType(TypeScan.HTTP);
//PARA EL OTRO PANEL ES LO MISMO SOLO LE ESTA CAMBIA
//  uiListScans.loadScansByType(TypeScan.GEO);

    return ListView.builder(
      itemCount: uiListScans.scansOnUI.length,
      //itemExtent: 45, separacion vertical entre items de la lista
      itemBuilder: (ctx, int i) {
      return ListTile(
        dense: true, visualDensity: VisualDensity.compact, leading: Icon(Icons.map, color: pColor), title: Text(uiListScans.scansOnUI[i].value),
        subtitle: Text('ID: ${uiListScans.scansOnUI[i].id}'), trailing: Icon(Icons.keyboard_arrow_right, color: pColor),
        onTap: () { print('Precionado: ${uiListScans.scansOnUI[i]}'); },
      );
     },
    );
  }
}
```

**Resultado:** Segun el el panel que estemos se nos mostrara la lista
de derecciones o de mapas.

![46_mostrar_diferente_consulta_sqlite_en_panel](./img/46_mostrar_diferente_consulta_sqlite_en_panel.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 167. Borrar todos los Scans

**Objetivo:** Eliminar todos los registros

En `lib\src\pages\HomePage.dart`

```dart
class HomePage extends StatelessWidget {
  @override Widget build(BuildContext context) {
    final uiStateProvider = Provider.of<UIStateProvider>(context);
    final title = (uiStateProvider.homeCurrentPanel == 0) ? 'Ubicaciones' : 'Urls';
    return Scaffold(
      appBar: AppBar( title: Text(title), elevation: 0,
        actions: [
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: () {
+             final uiListScans = Provider.of<ScanListOnUIStateProvider>(context, listen: false);
+             uiListScans.deleteByType(uiListScans.selectedType);
            }
          )
        ],
      ),
      ...
    );
  }
}
```

**Resultado:** Podemos eliminar por tipo

<!--

#########################################

-->

----

<!--

#########################################

-->

### 168. Borrar un Scan de forma independiente

**Objetivo:** Eliminar un elemento cuando hagamos swipe sobre el.

En, `lib\src\panels\HomeDireccionsPanel.dart` es lo mismo para mapas.

```dart
Widget build(BuildContext context) {
    final pColor = Theme.of(context).primaryColor;
    final uiListScans = Provider.of<ScanListOnUIStateProvider>(context);
    uiListScans.loadScansByType(TypeScan.HTTP);
    final scans = uiListScans.scansOnUI; // cuidado no editar "scan" esditar directo "uiListScans.scansOnUI"

    return ListView.builder(
      itemCount: scans.length,
      //itemExtent: 45, separacion vertical entre items de la lista
      itemBuilder: (ctx, int i) {
+     return Dismissible(
+       key: Key(scans[i].toString()),
+       onDismissed: (direction) {
// error: A dismissed Dismissible widget is still part of the tree
// Causa: estaba eliminando "scans.removeAt(i)" y ahi que eliminar la fuente
+         uiListScans.scansOnUI.removeAt(i);
//eliminar de la BD
+         Provider.of<ScanListOnUIStateProvider>(context, listen: false).deleteById(scans[i].id);
        },
        background: Container(color: Theme.of(context).errorColor),
        child: ListTile(
```

Es lo mismo para panel, de hecho refactorizamos para reutilizar para
ambas listae

**Resultado:** Podemos eliminar los elementos uno por uno.

![.GIF](./img/47_delete_one.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 170. Abrir Navegador web URL

Fuente: [url_launcher](https://pub.dev/packages/url_launcher)

**objetivo:** Este paquente nos ayudara a lanzar una url en el navegador.

A, Instalamos paquete

```yaml
dependencies:
  flutter:
    sdk: flutter
+ url_launcher: ^6.0.3
```

B, Creamos util para manejar los scans y enviar a url

```dart
import 'package:flutter/cupertino.dart';
import 'package:qr_sqlite/src/models/AppException.dart';
import 'package:qr_sqlite/src/models/ScanModel.dart';
import 'package:url_launcher/url_launcher.dart';
class ScanLauncher {
  static void launchScan(BuildContext ctx, ScanModel scan) async  {
    switch (scan.type) {
      case TypeScan.HTTP:
        _launchURL(scan.value);
      break;
      case TypeScan.GEO:
        Navigator.pushNamed(ctx, 'map', arguments: scan);
      break;
      default:
        throw AppException('Tipo de scan no reconicido', ErrorTyme.BAD_INPUT);
    }    
  }

  static void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw new AppException('Url no reconocida', ErrorTyme.BAD_INPUT);
    }
  }
}
```

C, Lo utilizamoz al darle click sobre un scan en `lib\src\panels\HomeScanListPanel.dart`

```dart
child: ListTile(
  ...
  onTap: () {
+   ScanLauncher.launchScan(context, scans[i]);
  },
),
```

**Resultado:** Cuando le damos click sobre un enlace nos cambia
discretamente al navegador, con la url que le agregamos.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 172. Crear proyecto de GoogleMaps

- Fuente: [google_maps_flutter](https://pub.dev/packages/google_maps_flutter/install)

A, Instalar dependencia

```yaml
dependencies:
  flutter:
    sdk: flutter
+ google_maps_flutter: ^1.0.10
```

B, Crear API key (ver video `172. Crear proyecto de GoogleMaps`)

C, Agregamos la API-Key en `android/app/src/main/AndroidManifest.xml:`

```xml
<manifest ...
  <application ...
    <meta-data android:name="com.google.android.geo.API_KEY"
               android:value="YOUR KEY HERE"/>
  />
```

C, Agregamos Mapa en `lib\src\pages\MapPage.dart`

```dart
  import 'package:flutter/material.dart';
+ import 'dart:async';
+ import 'package:google_maps_flutter/google_maps_flutter.dart';

  import 'package:qr_sqlite/src/models/ScanModel.dart';

//debemo de usarlo dentro de un "StatefulWidget"
! class MapPage extends StatefulWidget {
    @override
    _MapPageState createState() => _MapPageState();
  }

  class _MapPageState extends State<MapPage> {

// desde aqui controlaremos el mapa
+   Completer<GoogleMapController> _controller = Completer();

// declaramos la camara/punto que se costrara
+   static final CameraPosition _initialPoint = CameraPosition(
+     target: LatLng(37.42796133580664, -122.085749655962),
+     zoom: 14.4746,
+   );

    @override
    Widget build(BuildContext context) {
      final ScanModel scan = ModalRoute.of(context).settings.arguments;
      
      return Scaffold(
        appBar: AppBar(
          title: Text('Mapa'),
        ),
// mostramos el mapa
+       body: GoogleMap(
+         mapType: MapType.hybrid,
+         initialCameraPosition: _initialPoint,
+         onMapCreated: (GoogleMapController controller) {
+           _controller.complete(controller);
+         },
+       ),
      );
    }
  }
```

**Resultado:** Podemos ver el mapa en el punto declarado

![48_abrir_mapa](./img/48_abrir_mapa.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 174. Personalizar mapa

**Objetivo:** Por defecto tenemos el mapa en vista satelite, vamos a
cambiar a una vista mas comoda.

Le agregamos la siguiete funcionalidad

- Boton para regresar a la pocision inicial
- Boton para cambiar tipo de visualizacion de mapa
- Marcador para señalar la ubicacion abierta

```dart
  import 'dart:async';

  import 'package:flutter/material.dart';
  import 'package:google_maps_flutter/google_maps_flutter.dart';
  import 'package:qr_sqlite/src/models/ScanModel.dart';
  import 'package:qr_sqlite/src/widgets/MyToast.dart';

  class MapPage extends StatefulWidget {
    @override
    _MapPageState createState() => _MapPageState();
  }

  class _MapPageState extends State<MapPage> {

    Completer<GoogleMapController> _controller = Completer();

//declaramos el tipo de mapa para que sea dinamico (al cambiarlo usar el setState)
+   MapType mapType = MapType.normal;

    @override
    Widget build(BuildContext context) {
      final ScanModel scan = ModalRoute.of(context).settings.arguments;    

      final CameraPosition _initialPoint = CameraPosition(
        target: scan.getLatLng(),
        zoom: 17.0,
      );

//decarar marcador
+     Set<Marker> markers = new Set<Marker>();
+     markers.add(new Marker(
+       markerId: MarkerId('geo-location'),
+       position: scan.getLatLng()
+     ));

      
      return Scaffold(
        appBar: AppBar(
          title: Text('Mapa'),
          actions: [
            IconButton(
              icon: Icon(Icons.location_on),
              onPressed: () async {
//mover mapa, cambiar ubicacion mapa
+               final GoogleMapController controller = await _controller.future;
+               controller.animateCamera(CameraUpdate.newCameraPosition(_initialPoint));
              },
            )
          ],
        ),
        body: GoogleMap(
          mapType: mapType,
//agregar marcador al mapa, mapa con marcadores
          markers: markers,
          zoomControlsEnabled: false,
          initialCameraPosition: _initialPoint,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.layers),
          onPressed: () {
  //cambiar visualizacion del mapa
            mapType = (mapType == MapType.satellite ) ? MapType.normal : MapType.satellite;
            setState(() { });
          },
        ),
      );
    }
  }
```

**Resultado:** Tenemos la funcionalidad dicha en este objetivo,
ver gif final (_5_sqlite_QR_mapas)

<!--

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Sección 10: Preferencias de usuario

Veremos como manejar informacion en preferencias, la cual nos sirve para
guardar datos pequeños sin complicaciones, igual veremos

- un menu lateral
- Guardar la ultima pantalla abierta para iniciar ahi la app de nuevo

A, Creamos el menu horizontal

```dart
import 'package:flutter/material.dart';
import 'package:preferencias/src/pages/HomePage.dart';
import 'package:preferencias/src/pages/SettingPage.dart';

class DrawerCustom extends StatelessWidget {
  const DrawerCustom({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            child: Center(child: Text('Perro')),
// parecido al background
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/menu.jpg'),
                fit: BoxFit.fill
              )
            )
          ),
          ListTile(
            leading: Icon(Icons.pages, color: Colors.blue),
            title: Text('Home'),
            onTap: () {
// navegar sin poder retroceder, evitar que el usuario retroceda, hacia atras, negar ir a atras
              Navigator.pushReplacementNamed(context, HomePage.NAME);
            },
          ),
          ListTile(
            leading: Icon(Icons.pages, color: Colors.blue),
            title: Text('Collection A'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.pages, color: Colors.blue),
            title: Text('Collection C'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.settings, color: Colors.blue),
            title: Text('Preferencias'),
            onTap: () {
              Navigator.pushReplacementNamed(context, SettingPage.NAME);
            },
          ),
        ],
      ),
    )
    );
  }
}
```

B, Creamos la pagina de preferencias, en todas las paginas es igual

```dart
import 'package:flutter/material.dart';
import 'package:preferencias/src/widgets/DrawerCustom.dart';

class SettingPage extends StatefulWidget {
  static const NAME = 'settings';
  @override _SettingPage createState() => _SettingPage();
}

class _SettingPage extends State<SettingPage> {
  bool _colorSecundario;
  int _genero;
  String _nombre = 'Pedro';
  TextEditingController _textController;
  @override void initState() {
    super.initState();
    _colorSecundario = false;
  }

  _setSelectedRadio( int valor ) {
    _genero = valor;
    setState(() {});
  }

  @override Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text('Ajustes'), ),
//Agregar menu horizontal, menu desplegable, menu escondido, menu hamburgeza
      drawer: DrawerCustom(),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text('Settings', style: TextStyle(fontSize: 45.0, fontWeight: FontWeight.bold )),
          ),
          Divider(),
          SwitchListTile( value: _colorSecundario, title: Text('Color secundario'),
            onChanged: ( value ){
              setState(() { _colorSecundario = value; });
            },
          ),
//radioButton, seleccionar solo una opcion
          RadioListTile( value: 1, title: Text('Masculino'),
            groupValue: _genero,
            onChanged: _setSelectedRadio,
          ),
          RadioListTile( value: 2, title: Text('Femenino'),
            groupValue: _genero,
            onChanged: _setSelectedRadio
          ),

          Divider(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(
                labelText: 'Nombre',
                helperText: 'Nombre de la persona usando el telĂ©fono',
              ),
              onChanged: ( value ) {
                //prefs.nombreUsuario = value;
              },
            ),
          )
        ],
      )
    );
  }
}
```

**Resultado:** Tenemos un menu y podemos del cual podemos navegar a
diferentes pantallas y un formulario/interactivo, ver gif adelante.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 187. Shared Preferences y cargar informacion ANTES DE QUE INICIE LA APPLICACION

Fuente: [shared_preferences](https://pub.dev/packages/shared_preferences/install)

<!-- manejo del async, como usar el async al inicio, problemas con el async,
como manejar el async, como quitar el async, quitar async, eliminar async.
eliminar el async. evitar async evitarlo, evitar el async -->

**Problema:** Estamos usando la datos asincronos para pintar widgets
iniciales, por ejemplo, poner el nombre del usuario, cargar la ultima
pantalla en la que estuvo antes de cerrar la app, etc.  
¿Como le hacemos para que estos datos esten listos de forma sincrona,
sin recurrir a FutureBuilders, ya que usar algo complejo en algo simple
es tedioso/lento?,

**Solucion:** Vamos a cargar estos datos antes de que inicie la aplicacion,
asi dispondremos de ellos sin problemas.

**Objetivo:** Ya tenemos la introduccion de datos, ahora vamos a persistirla,
y obtendremos estos datos, para tenerlos disponibles al inicio de la app.

0, Instalar dependencia

```yml
dependencies:
  flutter:
    sdk: flutter
+ shared_preferences: ^0.5.12+4
```

A, Creamos el util para manejar el sharedPreferences

1. Lo hacemos de tipo singleton
2. Todo es sincrono (sin async), lo que sea asincrono al `init`

```dart
import 'package:shared_preferences/shared_preferences.dart';
class UserPref {

// crear singleton
+ static final UserPref _singleton = new UserPref._internal();
+ UserPref._internal();
+ factory UserPref() {
+   return _singleton;
+ }

// inicialimamos las variables que se obtienen asincronamente como null
// y asignamos valor el el init
+ SharedPreferences sharedP;
+ init() async {
+   sharedP = await SharedPreferences.getInstance();
+ }

// todo lo demas funciona de forma SINCRONA
  static const KEY_GENERO    = 'GENERO';
  static const KEY_COLOR_SEC = 'COLOR_SEC';
  static const KEY_NAME      = 'NAME';

  void save(String key, dynamic value) {
    if(value.runtimeType == int) { sharedP.setInt(key, value); }
    else if(value.runtimeType == String) { sharedP.setString(key, value); }
    else if(value.runtimeType == bool) { sharedP.setBool(key, value); }
    else { throw 'Tipo no soportado'; }
  }

  String getGeneroName() {
    int genero = getGenero();
    switch (genero) { case 0: return 'vacio'; break; case 1: return 'Masculino'; break; case 2: return 'Femenino';  break; default: return 'error'; }
  }

  int getGenero() { return sharedP.getInt(KEY_GENERO) ?? 0; }
  String getNombre() { return sharedP.getString(KEY_NAME) ?? 'vacio'; }
  bool getColorSecundario() { return sharedP.getBool(KEY_COLOR_SEC) ?? false; }

  void setGenero(int value) { save(KEY_GENERO, value); }
  void setNombre(String name) { save(KEY_NAME, name); }
  void setColorSecundario(bool value) { save(KEY_COLOR_SEC, value); }
}
```

B, Agregamos la carga al inicio de nuestra app

```dart
Future<void> main() async {
+ WidgetsFlutterBinding.ensureInitialized(); // NO OLVIDAR
+ final userPref = new UserPref();
+ await userPref.init();

  runApp(MyApp());
}
```

C. Podemos usar nuestros datos de forma sincrona

```dart
class HomePage extends StatelessWidget {
+ UserPref userPref = new UserPref();
  @override Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text('Algo') ), drawer: DrawerCustom(), body: Column( mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
+         Text('Color secundario: ${ userPref.getColorSecundario() }'), Divider(),
+         Text('Género: ${ userPref.getGeneroName() }'), Divider(),
+         Text('Nombre usuario: ${ userPref.getNombre() }'), Divider()
        ],
      ),
```

**Resultado:** Tenemos listos datos asincronos al inicio de la aplicacion,
para luego usarlos de forma sincrona.

<!--

#########################################

-->

----

<!--

#########################################

-->

### Cambiar BarColor en runtime

**Objetivo:** Cambiar el color de la barra superior en tiempo de ejecucion.
Ya tenemos implementado su cambio/guardado/recuperacion, por lo que
solo lo usamos.

A, Es lo mismo para ambas partes

```dart
class _SettingPage extends State<SettingPage> {
  UserPref userPref = new UserPref();
  bool _colorSecundario;
  @override
  void initState() {
    super.initState();
//recuperamos valor inicial
    _colorSecundario = userPref.getColorSecundario();   
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text('Ajustes'),
// cambiamos de color
!       backgroundColor: (_colorSecundario) ? Colors.amber : Colors.purpleAccent,
      ),
      drawer: DrawerCustom(), body: ListView( children: <Widget>[
          Container(...), Divider(),
          SwitchListTile( value: _colorSecundario, title: Text('Color secundario'),
            onChanged: ( value ){
              setState(() {
// cambiamos de valor
+               _colorSecundario = value;
+               userPref.setColorSecundario(_colorSecundario);
              });
            },
          ),
      ...
```

**Resultado:** El color de la appbar cambia al precionar un boton

<!--

#########################################

-->

----

<!--

#########################################

-->

### Guardar ultima pantalla abierta

**Objetivo:** Vamos a guardar la ultima ruta en la que accedio el usr,
para cuando la vuelva abrir este vea la aplicacion donde la dejo.

1, Cuando nevegemos a algun lado guardamos

```dart
Navigator.pushReplacementNamed(context, SettingPage.NAME);
userPref.setLatScreen(SettingPage.NAME);
```

2, Cuando iniciamos la aplicacion recuperamos

```dart
class MyApp extends StatelessWidget {
+ final UserPref userPref = new UserPref();
  @override Widget build(BuildContext context) { return MaterialApp(
+     initialRoute: userPref.getLastScreen(),
```

**Resultado:** si cerramos la aplicacion, al abrirla se nos abrira la
misma pantalla donde se quedo.

![_6_preferencias_de_usuario](./img/_6_preferencias_de_usuario.gif)

<!--

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Seccion 11: Validación de formularios - Patrón Bloc

En esta sección aprenderemos:

- Implementar un Bloc
- Uso de formularios con Bloc
- Diseño de un login elegante
- InheritedWidget
- Conectar inputs con Streams
- Validaciones
- Persistencia de los valores de un Bloc después de un HotReload

Esta sección es bastante ilustrativa en cuanto al uso de StreamTransformers y Bloc, el login lo haremos funcionar un poco más adelante en el curso, por ahora sólo me interesa trabajar con las validaciones y construcción del mismo.

### Diseño del formulario

**Objetivo:** Vamos a hacer el siguinete diseño.

![49_desenio_login](./img/49_desenio_login.gif)

A, Creamos `lib\src\pages\LoginPage.dart`

```dart
import 'package:flutter/material.dart';
import 'package:form_bloc/src/widgets/LoginForm.dart';
import 'package:form_bloc/src/widgets/MyBackground.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    final double allHeight = _screenSize.height;
    final double allWidth = _screenSize.width;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          MyBackground(heightTop: allHeight*0.4),
          LoginForm(width: allWidth*0.85, marginTop: allHeight*0.3 ),
        ],
      ),
      
    );
  }
}
```

B, Creamos fondo en `lib\src\widgets\MyBackground.dart`

```dart
import 'package:flutter/material.dart';
class MyBackground extends StatelessWidget {
  final heightTop;

  const MyBackground({Key key, double heightTop}): this.heightTop = heightTop, super(key: key) ;

  @override Widget build(BuildContext context) {
    final backgound = Column(
      children: [
        Container(
          height: heightTop,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(63, 63, 156, 1),
                Color.fromRGBO(90, 70, 198, 1),
              ]
            )
          ),
        ),
      ],
    );

    final header = Container(
      //decoration: BoxDecoration( border: Border.all(color: Colors.black), ), // agregar borde
      margin: EdgeInsets.only(top: 40),
      width: double.infinity,
      child: Column(
        children: [
          Icon(Icons.lock, color: Colors.white, size: 70),
          SizedBox(height: 20,),
          Text('Aplicacion Subidora', style: TextStyle(color: Colors.white, fontSize: 24) ),
        ],
      ),
    )
    ;

    return Stack(
      children: [
        backgound,
        buildCircle(radius: 50, hori: 30, verti: 90),
        buildCircle(radius: 40, hori: 140, verti: 10),
        buildCircle(radius: 100, hori: 130, verti: 210),
        header,
      ],
    );
  }

  buildCircle({@required double radius, @required double hori, @required double verti}) {
    return Positioned(
      top: verti,
      left: hori,
      child: CircleAvatar(
        backgroundColor: Color.fromRGBO(255,255,255,0.05),
        radius: radius,
      )
    );
  }
}
```

B, Creamos contenido del formulario en `lib\src\widgets\LoginForm.dart`

```dart
import 'package:flutter/material.dart';

class LoginForm extends StatelessWidget {

  final double width;
  final double marginTop;
  const LoginForm({Key key, @required double width, @required double marginTop})
      : this.width=width, this.marginTop = marginTop, super(key: key) ;

  @override
  Widget build(BuildContext context) {

    final boxDecoration = BoxDecoration(
      //border: Border.all(color: Colors.black),
      color: Colors.white,
      borderRadius: BorderRadius.circular(5),
      boxShadow: [
        BoxShadow(
          color: Colors.grey,
          offset: Offset(0.0, 0.5), //(x,y)
          blurRadius: 6.0,
        ),
      ],
    );

    final emailInput = Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(Icons.alternate_email, color: Colors.deepPurple),
          hintText: 'Ejemplo: mario@mail.com',
          labelText: 'Correo electronico',
        ),
      ),
    );

    final passInput = Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        obscureText: true,
        obscuringCharacter: '*',
        decoration: InputDecoration(
          icon: Icon(Icons.lock, color: Colors.deepPurple),
          hintText: 'Palabra secreta',
          labelText: 'Contraseña',
        ),
      ),
    );

    final submitButton = RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0)
      ),
      color: Colors.deepPurple,
      textColor: Colors.white,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 80, vertical: 15),
        child: Text('Ingresar'),
      ),
      onPressed: () {},
    );

    final form = Container(
      width: width,
      padding: EdgeInsets.symmetric(vertical: 50),
      decoration: boxDecoration,
      child: Column(
        children: [
          Text('Login', style: TextStyle(fontSize: 20),),
          emailInput,
          passInput,
          SizedBox(height: 30),
          submitButton,
        ],
      ),
    );

    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [
            SizedBox(height: marginTop),
            form,
            SizedBox(height: 20),
            Text('Recuperar contraseña'),
            SizedBox(height: 100),
          ],
        ),
      ),
    );
  } 
}
```

**Resultado:** Creamos el diseño propuesto

<!--

#########################################

-->

----

<!--

#########################################

-->

### 199. LoginBloc - Controlar los campos del formulario

**Objetivo:** Vamos a crear el BLoC que va a controlar la vista del login.

A, Creamos nuestro BloC en `lib\src\blocs\LoginBLoC.dart` que nos servira
para centralizar la logica en un solo punto.

```dart
import 'dart:async';

class LoginBLoC {

// creamos los flujos para ambos campos
  final _emailController = StreamController<String>.broadcast();
  final _passController = StreamController<String>.broadcast();


//creamos "getters/setters" para obtener la funcion para agregar datos al stream
// y asi evitarnos escribir lo de abajo en capas superiores
  Function(String) getChangeEmail() {
    return _emailController.sink.add;
  }

  Function(String) getChangePass() {
    return _passController.sink.add;
  }

//Creamo "getters/setters" para obtener los screams, y asi poder obtenerlos
//para escuchar los datos que pasen por este, y asi le podremos agregar logica
//que se ejecute cada vez que cambie el input
  Stream<String> getEmailStream() {
    return _emailController.stream;
  }

  Stream<String> getPassStream() {
    return _passController.stream;
  }

// cerramos los inputs
//los "?" es para evitarlos llamarlos si son null
  void dispose() {
    _emailController?.close();
    _passController?.close();
  }

}
```

**Resultado:** Tenemos listo para ser usados los streams para el login

<!--

#########################################

-->

----

<!--

#########################################

-->

### 201. InheritedWidget

Fuentes

- [Youtube:InheritedModel](https://www.youtube.com/watch?v=ml5uefGgkaA)
- [DocOficial:InheritedModel](https://api.flutter.dev/flutter/widgets/InheritedWidget-class.html)

A, Creamos el widget que le pasara el BloC que creamos `LoginBLoC` a
todos los widgets que tenga abajo.

```dart
import 'package:flutter/material.dart';

import 'LoginBLoC.dart';

// "InheritedWidget" tambien es un tipo de Widget, por lo que podremos
// agregarlo a nuestro arbol de widgets
class MyInheritedWidget extends InheritedWidget {

//unica instancia en toda mi aplicacion del BLoC
  final loginBLoC = new LoginBLoC();

//lo necesitamos para usar el constructor de "InheritedWidget"
  MyInheritedWidget({
    Key key,
    // dentro de este widget y sus sub-widgets tendremos disponible lo que
    // declaremos aqui, en este caso el "loginBLoC"
    Widget child
  }): super(key: key, child: child);
  

// este metodo decidira si notificar o no a los hijos para que se dibujen
// en la gran mayoria si queremos hacerlo, asi que mientras le damos siemre true
  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return true;
  }
  
// aqui en el "ctx" recibimos lo que seria todo nuestro arbol de elementos de la app.
// NOTA: el metodo de llama "of" por ejemplo cuando lo llamemos, sera de la sig forma
// 'final loginBLoC = MyInheritedWidget.of(contex);'
  static LoginBLoC of (BuildContext ctx) {
    // retornara la instancia que le indiquemos en este caso "loginBLoC"
    return (
      // buscara en el arbol completo de la app nuestra instancia "loginBLoC"
      ctx.dependOnInheritedWidgetOfExactType<MyInheritedWidget>().loginBLoC
    );
  }
}
/*
//viejo
static LoginBloc of ( BuildContext context ) {
  return ( context.inheritFromWidgetOfExactType(Provider) as Provider ).loginBloc;
}

//nuevo
static LoginBloc of ( BuildContext context ){
   return context.dependOnInheritedWidgetOfExactType<Provider>().loginBloc;
}
*/
```

B, Lo agregamos en el punto mas alto necesario de nuestra aplicacion,
nuestro "widget heredador" que creamos, en este caso `MyInheritedWidget`

**Notar** que este esta envolviendo toda nuestra aplicacion

```dart
class MyApp extends StatelessWidget {
  @override Widget build(BuildContext context) {
+   return MyInheritedWidget(
      child: MaterialApp(
        title: 'Form BLoC', debugShowCheckedModeBanner: false, initialRoute: 'login',
        routes: { 'login': (ctx) => LoginPage(), 'home': (ctx) => HomePage(), },
      ),
+   );
  }
}
```

### 202. Conectar los inputs con los Streams en el InheritedWidget

**Objetivo:** Con el `LoginBLoC` ya disponible gracias al `MyInheritedWidget`,
vamos a conectar los inputs a los streams, para luego asignarles logica

```dart
import 'package:flutter/material.dart';
import 'package:form_bloc/src/blocs/LoginBLoC.dart';
import 'package:form_bloc/src/blocs/MyInheritedWidget.dart';

// fijaros que estamos usando un Stateless y estamos redibujando el
// stateless
class LoginForm extends StatelessWidget {

  final double width;
  final double marginTop;
  const LoginForm({Key key, @required double width, @required double marginTop})
      : this.width=width, this.marginTop = marginTop, super(key: key) ;

  @override
  Widget build(BuildContext context) {

//tomamos el BLoC del MyInheritedWidget, obtener BLoC
+   final LoginBLoC loginBLoC = MyInheritedWidget.of(context);

    final boxDecoration = BoxDecoration(...)

// envolvemos nuestro componente en un input
+   final inputEmailBuilder = StreamBuilder(
+     stream: loginBLoC.getEmailStream(), // necesitamos el stream
+     builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: TextField(
// le enviamos los datos al stream, esta asi raro porque el metodo trae
// al metodo que usamos.
+           onChanged: (txt) { 
+             loginBLoC.getChangeEmail()(txt);
+           },
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
//solo por demostracion que los datos fluyen a travez del stream
              icon: Icon(Icons.alternate_email, color: Colors.deepPurple),
+             counterText: snapshot.data,
              hintText: 'Ejemplo: mario@mail.com',
              labelText: 'Correo electronico',
            ),
          ),
        );
      },
    );

// Hacemos lo mismo para password
    final passInput = StreamBuilder(
+     stream: loginBLoC.getPassStream(),
+     builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: TextField(
+           onChanged: (input) {
+             loginBLoC.getChangePass()(input);
*           },
            keyboardType: TextInputType.emailAddress,
            obscureText: true,
            obscuringCharacter: '*',
            decoration: InputDecoration(
              icon: Icon(Icons.lock, color: Colors.deepPurple),
+             counterText: snapshot.data,
              hintText: 'Palabra secreta',
              labelText: 'Contraseña',
            ),
          ),
        );
      },
    );

    final submitButton = RaisedButton(...);

    final form = Container(...);

    return SingleChildScrollView( child: Center( child: 
        Column(
          children: [ SizedBox(height: marginTop), form, SizedBox(height: 20), Text('Recuperar contraseña'), SizedBox(height: 100), ,
        ),
      ),
    );
  } 
}
```

**Resultado:** Estamos usando el patron BLoC con Streams en nuestros
campos de login.

![50_conectar_streams_a_inputs](./img/50_conectar_streams_a_inputs_traidos_del_InheritedWidget.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 204. StreamTransformer y Validaciones

<!-- validar formuario, -->

**Introduccion:** Para mostrar un error, flutter nos ofrece el campo
`errorText` dentro del InputDecuoation, donde si tiene asignado algo
lo muestra y eslilo cambia a rojo, cuando esta asignado null, este texto
y estilo desaparecen. Entonces debemos de retornar un texto o un nulo,
en la propiedad "errorText", en funcion de que esta correcto el input.

**Objetivo:** Mostrar mensaje si esta correcto no el input.

A, Creamos validaciones `lib\src\utils\Valitators.dart`

```dart
import 'dart:async';
class Valitators {

//validar contraseña
  final validatePass = StreamTransformer
    <String, //tipo de dato que entra
    String> //tipo de dato que sale
    .fromHandlers( 
      // "pass" es la varialbe que entra del stream, y el sink nos indica si el stream esta activo o y para
      // poder notificar que algo fallo en este
      handleData: (String pass, EventSink<String> sink) {
        if(pass.length <= 6) {
          sink.add(null); // por si vuelve a estar mal el boton se vuelva a bloquear
          sink.addError('Debe tener mas de 6 caracteres');
        } else {
          //significa que el flujo es correcto
          sink.add(pass);
        }
      }
    )
  ;

//validar email
  final validateEmail = StreamTransformer<String, String>
    .fromHandlers(
      handleData: (String email, EventSink<String> sink) {
        Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regExp = new RegExp(pattern);
        if(regExp.hasMatch(email)) {
          sink.add(email);
        } else {
          sink.add(null); // lo mismo bloquear de nuevo el boton
          sink.addError('Verifique formato');
        }
      }
    )
  ;
}
```

B. Los agregamos a nuestro stream agregando funcionalidad

```dart
class LoginBLoC with Valitators {


  Stream<String> getEmailStream() {
    return _emailController.stream
//le agregamos a la tuberia este check que 1) notificara de un error,
// 2) bloquera el flujo si hay un error, 3) si es correcto lo dejara pasar
+     .transform(validateEmail) // variable "validatePass" la traemos del "Valitators"
    ;
  }

  Stream<String> getPassStream() {
    return _passController.stream
+     .transform(validatePass)
    ;
  }
```

C. Obtenemos el input del stream o el error en nuestra UI `lib\src\widgets\LoginForm.dart`

```dart
final passInput = StreamBuilder( stream: loginBLoC.getPassStream(), builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container( margin: EdgeInsets.only(top: 20), padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextField( onChanged: (input) { loginBLoC.getChangePass()(input); },
        keyboardType: TextInputType.emailAddress, obscureText: true, obscuringCharacter: '*',
        decoration: InputDecoration(
          icon: Icon(Icons.lock, color: Colors.deepPurple),hintText: 'Palabra secreta', labelText: 'Contraseña',
//NOTAR QUE si el flujo esta mal este llega como null
!             counterText: snapshot.data,
// nos llegara lo que mandamos por "sink.addError" en nuestro `class Valitators`
+             errorText: snapshot.error,
        ),
      ),
    );
  },
);

//lo mismo que pass
final inputEmailBuilder = StreamBuilder(...);
```

**Resultado:** Se nos mostrara un error y bloquera el flujo (se demuestra
ya que no muestra texto abajo-der cuando esta mal), o permitira el flujo.

![51_validar_inputs_con_streams](./img/51_validar_inputs_con_streams.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 205. Des/Habilitar boton Combinar Streams

Fuente: [combineLatest](https://rxmarbles.com/#combineLatest)

**Recordar que:** Para des-habilitar un boton su `onPress` o equivalente
lo debemos de pasar a nulo.

**Introduccion:** combinar stream, nos combina los ultimos dos valores
que entraron al stream, tal como podemos ver en la fuente `combineLatest`

**Objetivo:** Bloquear el boton cuando alguno de los inputs este mal, osea
habilitar el boton solo cuando ambos inputs esten bien.

A. Instalar paquete

```yml
rxdart: ^0.25.0
```

B, Comninamos ambos streams `lib\src\blocs\LoginBLoC.dart`

```dart
  import 'dart:async';
  import 'package:form_bloc/src/utils/Valitators.dart';
// nos facilita la combinacion de streams y otras cosas con estos
+ import 'package:rxdart/rxdart.dart';

class LoginBLoC with Valitators {

- //final _emailController = StreamController<String>.broadcast();
- //final _passController = StreamController<String>.broadcast();

// Para poder trabajar con rxDart debemos de cambiarnos a "" ya que rxDart no los StreamController.
// lo cual seguira funcionando exactamente igual
# final _emailController = BehaviorSubject<String>();
# final _passController = BehaviorSubject<String>();

// creamoscombiner para ejecutar el metodo cuando ambos sean validos
+ Stream<bool> getFormValid() {
+   return Rx.combineLatest2(
+     this.getEmailStream(),    // primer stream a combinar
+     this.getPassStream(),     // segundo stream a combinar
+     (e, p) { return true; }   // callback cuando ambos stream sean validos, recordar que cuando esta mal
+   );                          //    el valitator no pasa datos por el stream, solo cuando es valido
  }
  ...
```

C. Creamos nuestro boton en funcion del stream `lib\src\widgets\LoginForm.dart`

```dart
+ final submitButton =  StreamBuilder(
+   stream: loginBLoC.getFormValid(),
+   builder: (BuildContext context, AsyncSnapshot snapshot){
      return Container(
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)
          ),
          color: Colors.deepPurple,
          textColor: Colors.white,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80, vertical: 15),
            child: Text('Ingresar'),
          ),
// este stream solo dara data cuando los dos streams que combinamos den data
!         onPressed: !snapshot.hasData ? null : () {}, // iniciar con el boton deshabilitado por defecto deshabilitado al inicio
        ),
      );
    },
  );
```

**Resultado:** El boton inicia des-habilitado y cuando ambos inputs
estan correctos este se activa.

![52_combinar_streams_para_habilitar_boton](./img/52_combinar_streams_para_habilitar_boton.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 206. Obtener el último valor emitido por un stream

**Objetivo:** Obtener los dos valores de los stream cuando el usuario
pulse sobre el boton de ingresar.

A, Agregamos la obtencion de los valores de los streams `lib\src\blocs\LoginBLoC.dart`

```dart
class LoginBLoC with Valitators {
  final _emailController = BehaviorSubject<String>(); final _passController = BehaviorSubject<String>();

// obtener valor del stream
+ String get email => _emailController.value;
+ String get pass => _passController.value;
  ...
}
```

B, Los podemos usar

```dart
  ...
  // al metodo del boton le agregamos un metodo que le entre el bloc
+ onPressed: !snapshot.hasData ? null : () { _login(loginBLoC); },
  ...

//podemos capturar los valores sin problema
+ void _login(LoginBLoC loginBLoC) {
+   print('Email: ${loginBLoC.email}, Pass: ${loginBLoC.pass}');
+ }
```

**Resultado:** Lo dicho.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 207. Mantener la data de los streams después de un hot reload

**Problema:** Cuando hacemos un hot-reaload se pierde la informacion,
que teniamos en el stream.

**Causa:** Se pierde la informacion de nuestro bloc porque el InheritedWidget
es un widget,, este se instanciara de nuevo con el hot-reload, instanciando
de nuevo nuestro bloc, por lo que debemos de evitar que se pierda la instancia
anterior.

**Solucion:** Hacer nuestro `InheritedWidget` haciendo un singleton para
que cuando se intente inicializar de nuevo InheritedWidget, pase por nuestro
factory para regresar la instancia anterior si esta existe.

```dart
class MyInheritedWidget extends InheritedWidget {

// Aqui se guardara nuestra instancia ante una re-instanciacion
  static MyInheritedWidget _singleton;
// checara si ya se instancio si es asi retorna la ionstanciada anteriormente
  factory MyInheritedWidget({ Key key, Widget child }) {
    if(_singleton == null) {
      _singleton = new MyInheritedWidget._internal(key: key, child: child);
    }
    return _singleton;
  }

// usamos el mismo constructor solo que ahora, desde el constructor privado
  MyInheritedWidget._internal({ Key key, Widget child })
      : super(key: key, child: child);

//lo necesitamos para usar el constructor de "InheritedWidget"
//  MyInheritedWidget({ Key key, Widget child }): super(key: key, child: child);
...
```

**Resultado:** Nuestra pantalla conservara los datos despues de hacer
un hot-restart

### Extra: Ventaja de usar InheritedWidget

**Objetivo** Demostrar como podemos usar nuestro BloC en distintas,
partes de nuestra aplicacion sin complicaciones, campturando lo ingresado
en el login en la pantalla de home.

A, Navegacion

- Como vemos es una navegacion de siempre simple/normal sin pasar argumentos
  ni nada

```dart
  void _login(BuildContext context, LoginBLoC loginBLoC) {
+   Navigator.of(context).pushReplacementNamed('home');
  }
```

B, Uso, al navegar no es necesesario que pasemos de un componente a otro
ya que todo lo tiene la raiz del arbol que podemos capturarla.

```dart
class HomePage extends StatelessWidget { const HomePage({Key key}) : super(key: key);
  @override Widget build(BuildContext context) {
//tomamos el BloC del "context" o mejor dicho del InheritedWidget
    final LoginBLoC loginBLoC = MyInheritedWidget.of(context);
    return Scaffold(
      appBar: AppBar( title: Text('Que tal'), ),
      body: Center(
//usamos las propiedades que ya guardamos
+       child: Text('Email: ${loginBLoC.email} \n Pass: ${loginBLoC.pass} '),
), ); } }
```

**Resultado:** Podemos capturar los valores de una forma facil.

![_7_validacion_formu_login_BLoC_streams.gif](./img/_7_validacion_formu_login_BLoC_streams.gif)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## CRUD hacia servicios REST, uso de cámara y galería de imágenes

Crearemos un CRUD de productos, en el cual incluye el manejo de imagenes.

Temas que veremos en la seccion.

- Formularios sin Bloc
- Modelos
- CRUD hacia Firebase
- Llamados de servicios REST
- Cargar imágenes
- Cámara
- Galería de imágenes

### 213. Diseño de la pantalla de detalle de producto

**Objetivo** Vamos a grear el siguiente formulario

![53_formulario_agregar_producto_A](./img/53_formulario_agregar_producto_A.png)

```dart
import 'package:flutter/material.dart';

class ProductPage extends StatelessWidget {
  const ProductPage({Key key}) : super(key: key);

  static const ROUTE_NAME = 'product';
  
  @override
  Widget build(BuildContext context) {

//DECLARACION    
    final appBar = AppBar(
      title: Text('Productos'),
      actions: [
        IconButton(
          icon: Icon(Icons.photo_size_select_actual),
          onPressed: () {},
        ),
        IconButton(
          icon: Icon(Icons.camera_alt),
          onPressed: () {},
        )
      ],
    );
//NOTAR QUE NO ESTAMOS USANDO "TextField"
    final inputName = TextFormField(
      autofocus: true,
      decoration: InputDecoration(
        labelText: 'Nombre del producto',
        hintText: 'ej. Taza de cafe',
      ),
    );

    final inputPrice = TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: 'Precio',
        hintText: 'ej. 199.99',
      ),
    );

    final submitButtom = RaisedButton.icon(
      onPressed: (){},
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      color: Theme.of(context).primaryColor,
      textColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0)
      ),
    );

    final form = SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(15),
        child: Form(
          child: Column(
            children: [
              inputName,
              inputPrice,
              submitButtom,
            ]
          )
        )
      ),
    );

//IMPLEMENTACION
    return Scaffold(
      appBar: appBar,
      body: form,
    );
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 214.116 Validación de formularios - FormWidget

**Objetivo:** Vamos a una validacion, usando la propiedad de los inputs

```dart
class ProductPage extends StatelessWidget {
  ...
//creamos una llave especificamente para un formulario
+ final formKey = GlobalKey<FormState>();

  @override Widget build(BuildContext context) {
    ...
    final inputName = TextFormField( autofocus: true,
// agregamos validador que se iniciara al preguntar si el formualrio esta bien
// de forma manual
+     validator: _validateInputName,
      decoration: InputDecoration(...),
    );

    final inputPrice = TextFormField( keyboardType: TextInputType.number,
+     validator: _validateInputPrice,
      decoration: InputDecoration( labelText: 'Precio', hintText: 'ej. 199.99', ),
    );

    final submitButtom = RaisedButton.icon(
// en nuestro caso aqui preguntaremos si esta bien
+     onPressed: _onSubmit,
      label: Text('Guardar'), icon: Icon(Icons.save), color: Theme.of(context).primaryColor, ...
    );

  }//termina metodo build

//Creamos las funciones las cuales deben de tretornar un string, que se
//mostrara al usuario
+ String _validateInputName(String text) {
    var errorHintResult;
    if(text.length < 3) { errorHintResult = 'Debe tener mas de 3 caracteres'; }
    return errorHintResult;
  }

+ String _validateInputPrice(String text) {
    if(text.isEmpty) { return 'Campo obligatorio'; }
    try { double.parse(text); } 
    catch (e) { return 'debe ser un numero valido'; }
    return null;
  }

+ void _onSubmit() {
// este disparara los validadores dontro del formulario
+   bool formIsValid = formKey.currentState.validate();
    if(formIsValid) { print('formulario correcto'); }
  }
}
```

**Resultado:** Cuando pulsamos sobre "guardar" nos muesta las validaciones
que declaramos.

![54_validacion_formaulario_facil](./img/54_validacion_formaulario_facil.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 215. Modelo para manejar los productos

**Objetivo** Siempre es bueno manejar todo el formulario en un solo
objeto, por lo que vamos a hacerlo.

A. Creamos el modelo en

```dart
import 'dart:convert';
class ProductModel {
  int id;
  String name;
  double price;
  bool available;
  String photoName;
  
  ProductModel({ this.id, this.name, this.price, this.available, this.photoName, });

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
    id         : json["id"],
    name       : json["name"],
    price      : json["price"].toDouble(),
    available  : json["available"],
    photoName  : json["photoName"],
  );

  Map<String, dynamic> toJson() => { "id": id, "name": name, "price": price, "available": available, "photoName": photoName, };

  static ProductModel fromString(String str) { return ProductModel.fromJson(json.decode(str)); }
  static String toMap(ProductModel data) { return json.encode(data.toJson()); }

  static List<ProductModel> fromCollection(String str) {
    Map<String, dynamic> decodedList = json.decode(str);
    List<ProductModel> products = [];
    decodedList.forEach((key, productRaw) {
      final product = ProductModel.fromJson(productRaw);
      product.id = key;
      products.add(product);
    });
    return products;
  }
}
```

B, Lo usamos en nustro formulario

```dart
  import 'package:flutter/material.dart';
+ import 'package:form_bloc/src/model/ProductModel.dart';
  class ProductPage extends StatefulWidget {...}

  class _ProductPageState extends State<ProductPage> {
    final formKey = GlobalKey<FormState>();
// agregamos producto
+   final newProduct = new ProductModel();
    @override Widget build(BuildContext context) {

  //DECLARACION    
      final appBar = AppBar(...);

      final inputName = TextFormField(
//le damos valor inicial, y el "onSaved" se ejecuta solo cuando le llamemos a "formKey.currentState.save()"
+       initialValue: newProduct.name,
+       onSaved: (value) { newProduct.name = value; },
        autofocus: true, validator: _validateInputName, decoration: InputDecoration( labelText: 'Nombre del producto', hintText: 'ej. Taza de cafe', ),
      );

//cambiamos a statefull por este capo
      final switchAvailable = SwitchListTile(
+       value: (newProduct.available != null) ? newProduct.available : false,
        title: Text('Disponible'), activeColor: Theme.of(context).primaryColor,
// aunque funciona diferente el "SwitchListTile" es facil relacionarlo con nustro modelo
+       onChanged: (isActive) { setState(() { newProduct.available = isActive; }); },
      );

      final inputPrice = TextFormField(
//le damos valor inicial, y el "onSaved" se ejecuta solo cuando le llamemos a "formKey.currentState.save()"
+       initialValue: (newProduct.price != null) ? newProduct.price.toString() : '',
+       onSaved: (value) { newProduct.price = double.parse(value); },
        keyboardType: TextInputType.number, validator: _validateInputPrice, decoration: InputDecoration( labelText: 'Precio', hintText: 'ej. 199.99', ),
      );

      final submitButtom = RaisedButton.icon(...);

      final form = SingleChildScrollView(
        child: Container( padding: EdgeInsets.all(15),
          child: Form( key: formKey, child: Column( children: [ inputName, inputPrice, switchAvailable, submitButtom, ] ) )
        ),
      );
  //IMPLEMENTACION
      return Scaffold( appBar: appBar, body: form, );
    }

    String _validateInputName(String text) {...}

    String _validateInputPrice(String text) {...}

    void _onSubmit() {
// este disparara los validadores dontro del formulario
      bool formIsValid = formKey.currentState.validate();
      if(formIsValid) {
// ejecutamos los metodos "onSaved"
+       formKey.currentState.save();
        print('formulario correcto, Nombre:: ${newProduct.name}, precio: ${newProduct.price} ');
      }
    }
  }
```

**Resultado:** Cuando le damos en "Guardar" podemos ver los datos intro.
por el usr, dentro de nuestro modelo.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 217. Preparar nuestra base de datos - Firebase

A, Creamos BD en firebase (ver 217)

B, Agregamos dependencia http

```yml
dependencies:
  flutter:
    sdk: flutter
+ http: ^0.12.2
```

C, crear service para productos

```dart
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:form_bloc/src/model/ProductModel.dart';

class ProductService {
  static const _BASE = 'https://flutter-maps-SECRETO-rtdb.firebaseio.com';
// notar que usamos la misma que nos da firebase, pero solo le agregamos un ".json"
  final _URL = '${_BASE}/products.json';

  Future<ProductModel> save(ProductModel newProduct) async {
    final respRaw = await http.post(_URL, body: ProductModel.toStringg(newProduct),  );  
    Map<String, dynamic> raspMaped = json.decode(respRaw.body);
    if(raspMaped['name'] != null) {
      newProduct.id = raspMaped['name'];
    }
    return newProduct;
  }
}
```

D, Usamos nuestro service

```dart
  final productService = ProductService();
  ...
  void _onSubmit() {
    bool formIsValid = formKey.currentState.validate();
    if(formIsValid) {
      formKey.currentState.save();
+     productService.save(newProduct).then((saved) { print('Guardado: ${saved.id}'); });
    }
```

**Resultado** Podemos crear nuesvos registros en firebase.

![55_firebase_crear_registro](./img/55_firebase_crear_registro.jpg)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 219-220-221. Cargar, Eliminar y mostrar productos de Firebase

**Objetivo** Cargar todos los productos de firebase

A, Agregamos servicio `lib\src\services\ProductService.dart`

```dart
Future<List<ProductModel>> findAll() async {
  final respRaw = await http.get(_URL);
  List<ProductModel> miList = ProductModel.fromCollection(respRaw.body);
  return miList;
}

Future<String> deleteById(String id) async {
  final respRaw = await http.delete(_URL+'/$id.json');
  if(respRaw.statusCode == 200) {
    return id;
  }
  return null;
}
```

B, Pintamos los productos en `lib\src\pages\HomePage.dart`

```dart
  import 'ProductPage.dart';
  class HomePage extends StatelessWidget {
    const HomePage({Key key}) : super(key: key);
    static const ROUTE_NAME = 'home';
    @override Widget build(BuildContext context) {
      final productService = ProductService();
      final myFloatingButtom = FloatingActionButton(...);
      
+     final productList = FutureBuilder(
+       future: productService.findAll(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.hasData) {
            List<ProductModel> products = snapshot.data;
            return ListView.builder(
              itemCount: products.length,
              itemBuilder: (BuildContext context, int i) {
+               return _buildItemList(products[i]);
            },
            );
          } else { return Center(child: CircularProgressIndicator()); }
        },
      );
      return Scaffold( appBar: AppBar( title: Text('Que tal'), ), body: productList, floatingActionButton: myFloatingButtom, );
    }

+   Widget _buildItemList(ProductModel product, int index) {
    return Dismissible(
      key: Key(product.id),
      background: Container(color: Colors.red[50],),
      onDismissed: (direction) {
        products.removeAt(index);
        productService.deleteById(product.id);
      },
      child: ListTile(
        title: Text(product.name),
        subtitle: Text('Precio: ${product.price}'),
      ),
    );
  }

```

**Resultado:** Podemos ver la lista de productos que estan guardados en
firebase. Tambien podemos eliminar.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 222. Editar un producto

**Objetivo:** Agregar edicion de producto

0, **NOTA:** Eliminamos el enviar el id al backend ya que puede dar
problemas a la hora de actualizar `lib\src\model\ProductModel.dart`

```dart
Map<String, dynamic> toJson() => {
// evitamos enviar el ID para no tener problemas con la actualizacion
  //"id"         : id,
  "name": name, "price"  price, "available": available, "photoName": photoName,
};
```

A, Agregamos servicio `lib\src\services\ProductService.dart`

```dart
Future<ProductModel> update(ProductModel editProduct) async {
  final respRaw = await http.put('$_URL/${editProduct.id}.json', body: ProductModel.toStringg(editProduct),  );
  Map<String, dynamic> raspMaped = json.decode(respRaw.body);
  if(raspMaped['name'] != null) { editProduct.id = raspMaped['name']; }
  return editProduct;
}
```

B, Agregamos navegar al formulario de producto con el producto que
seleccionamos en `lib\src\pages\HomePage.dart`

```dart
Widget _buildItemList(BuildContext context, ProductModel product, int index) {
  return Dismissible(...
    child: ListTile(
//navegamos al formulario y enviamos el producto.
+       onTap: () { Navigator.of(context).pushNamed(ProductPage.ROUTE_NAME, arguments: product); },
      ...
    ),
  );
}
```

C, Capturamos producto y diferenciamos entre editar y agregar

```dart
@override
Widget build(BuildContext context) {

  var labelSubmitButtom = 'Guardar';
  if(ModalRoute.of(context).settings.arguments != null) {
    product = ModalRoute.of(context).settings.arguments;
    labelSubmitButtom = 'Actualizar';
  }

  final submitButtom = RaisedButton.icon(
+   onPressed:  (labelSubmitButtom == 'Guardar') ? () { _onSubmit(context); } : (){ _actualizar(context); },
    ...
  );

  _actualizar(BuildContext context) {
//no olvidar validar y capturar datos del formulario
+   bool formIsValid = formKey.currentState.validate();
    if(formIsValid) {
+     formKey.currentState.save();
+     productService.update(product)
        .then((updated) {
          print('Actualizado: ${updated.id}');
+         Navigator.of(context).pushNamed(HomePage.ROUTE_NAME);
        })
      ;
    }
  }
}
```

**Resultado:** Podemos editar, ver gif mas abajo para verlo.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 223. Bloquear botón y mostrar Snackbar

**Objetivo:** No tenemos implementado el darle a entender que los cambios
se estan realizando, para eso agregaremos una animacion de carga y
cuando retroceda un mensaje de la accion que se realizo.

A, En `lib\src\pages\ProductPage.dart` agregamos vandera de carga

```dart
class _ProductPageState extends State<ProductPage> {
  var isLoading = false;

  @override
  Widget build(BuildContext context) {
    final form = SingleChildScrollView( child: Container( padding: EdgeInsets.all(15),
          child: Form( key: formKey, child: Column(
              children: [ inputName, inputPrice, switchAvailable,
// con el setState mostramos uno u otro
+               (!isLoading) ? submitButtom : CircularProgressIndicator(),
              ]
    ...
  }

  void _add(BuildContext context, ) {
    bool formIsValid = formKey.currentState.validate();
    if(formIsValid) { formKey.currentState.save();
+     setState(() { isLoading = true; });
      productService.save(product)
        .then((saved) { 
//Quitamos indicador de carga pero como quitamos pantalla no es necesario
          //setState(() { isLoading = false; });
//enviar informacion hacia atras, pop con argumentos, enviar informacion en pop
          Navigator.of(context).pop(InternalMsgsEnum.SAVED_OK);
          ...
        })
      ;
    }
  }
```

B, Agregamos Util para mostrar SnackBar

```dart
import 'package:flutter/material.dart' show SnackBar, Text, GlobalKey, ScaffoldState;
import 'InternalMsgsEnum.dart';
class SnackUtils {
//necesitamos el key del Scalfold para poderlo mostrar
+ static void showSnackBar(InternalMsgsEnum msgEnum, GlobalKey<ScaffoldState> key) {
    var msg = '';
    switch (msgEnum) {
      case InternalMsgsEnum.SAVED_OK: msg = 'Registro guardado con exito'; break;
      case InternalMsgsEnum.UPDATE_OK: msg = 'Registro actualizado con exito'; break;
      default: msg = 'Mensage sin definir';
    }
//crear snackbar    
+   final snackBar = SnackBar( content: Text(msg), duration: Duration(milliseconds: 1500), );
//mostrar snackbar
+   key.currentState.showSnackBar(snackBar);
  }
}
```

C, recuperar informacion del pop, y mostrar snackBar

```dart
class HomePage extends StatelessWidget {
  ...
// Creamos key para poderla refenciar al mostrar snackbar.
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
// agregar key
+     key: scaffoldKey,
      ...
    );
  }

  Widget _buildItemList(BuildContext context, ProductModel product, int index) {
    return Dismissible(
      ...
      child: ListTile(
        onTap: () {
          Navigator.of(context).pushNamed(ProductPage.ROUTE_NAME, arguments: product)
//capturar informacion del pop, hacer pop recuperando info.
+         .then((beforeAction) {
+           if(beforeAction.runtimeType == InternalMsgsEnum) {
+             _showMsgOfBeforeAction(context, beforeAction);
+           }
          });
        },
    ...
  }


+ void _showMsgOfBeforeAction(BuildContext context, InternalMsgsEnum action) {
    Future.delayed(const Duration(milliseconds: 50), () {
      if(action == InternalMsgsEnum.SAVED_OK) {
        SnackUtils.showSnackBar(action, scaffoldKey);
      } else if(action == InternalMsgsEnum.UPDATE_OK) {
        SnackUtils.showSnackBar(action, scaffoldKey);
      }
    });
  }
```

**Resultado:** Tenemos lo necesario para hacer entender al cliente, que
la peticion esta trabajando y que se realizo correctamente.

![56_crud_productos](./img/56_crud_productos.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 225. Seleccionar y mostrar una fotografía

- Fuentes:
  - [image picker](https://pub.dev/packages/image_picker)

A, Agregar dependencia

```yml
dependencies:
+ image_picker: ^0.6.7+21
```

B, En Android para evitarnos problemas avanzamos el sdk minimo en
`android/app/build.gradle`

```groovy
defaultConfig {
    // TODO: Specify your own unique Application ID (https://developer.android.com/studio/build/application-id.html).
    applicationId "com.example.form_bloc"
// estaba en 16 lo pasamos a 21
!   minSdkVersion 21 
    targetSdkVersion 29
    versionCode flutterVersionCode.toInteger()
    versionName flutterVersionName
}
```

C, Para ios hacemos lo siguiente: `ios\Runner\Info.plist`

```xml
  <dict>
+   <key>NSPhotoLibraryUsageDescription</key>
+   <string>Se requieren permisos a archivos</string>

+   <key>NSCameraUsageDescription</key>
+   <string>Se requiere permiso a camara</string>

+   <key>NSMicrophoneUsageDescription</key>
*   <string>Se requieren permisos al microfono</string>
```

> Error: `Unable to load assets`
> Slolucion: Cambiar este codigo por el de abajo

```dart
_mostrarFoto() {
    return Image(
          image: AssetImage(foto?.path ?? 'assets/no-image.png'),
          height: 300.0,
          fit: BoxFit.cover,
    );
}
```

> Codigo sin dicho errro

```dart
_mostrarFoto() {
  if (producto.fotoUrl!=null) { return Container(); }
  else {
    if(foto!=null){
      return Image.file( foto, fit: BoxFit.cover, height: 300.0, );
    } return Image.asset('assets/no-image.png');
  }
}
```

D, Agregar la seleccion de fotografia

```dart
// este contiene el File
+ import 'dart:io';
//el plugion que instalamos para seleccionar la imagen
+ import 'package:image_picker/image_picker.dart';

  class ProductPage extends StatefulWidget {...}

  class _ProductPageState extends State<ProductPage> {
// aqui guararemos la foto seleccionada/tomada, para luego mostrarla
+   File file;

  @override Widget build(BuildContext context) {

    final appBar = AppBar( title: Text('Productos'),
      actions: [
        IconButton( icon: Icon(Icons.photo_size_select_actual),
//solo cambia un parametro para seleccionar la img de la camara o de
//la galeria
+         onPressed: () { _pickPhoto(ImageSource.gallery); } ,
        ),
        IconButton( icon: Icon(Icons.camera_alt),
+         onPressed: () { _pickPhoto(ImageSource.camera); },
        )
      ],
    );

+   final productImage = Image(
//mostramos la imagen solo si esta esta disponible
+     image: AssetImage( file?.path ?? 'assets/img/no-image.png'),
+     height: 250,
+     fit: BoxFit.cover,
+   );
    
    ...
    final form = SingleChildScrollView(
      child: Container( padding: EdgeInsets.all(15), child: Form( key: formKey,
          child: Column( children: [
// agregamos el widget responsable de mostrar la imagen
+             productImage,
              inputName, inputPrice, switchAvailable, (!isLoading) ? submitButtom : CircularProgressIndicator(),
            ]
          )
        )
      ),
    );
  } // build()

// nos funciona tanto para tomar foto como seleccionar una foto
// de la galeria
+ void _pickPhoto(ImageSource imageSource) async {
+   final picker = ImagePicker();
+   PickedFile imagePicker = await picker.getImage(
+     source: imageSource,
+     imageQuality: 10, // va de 0 a 100
+     //maxWidth: 240,
+     //maxHeight: 135,
+   );
+   file = File(imagePicker.path);
+   if (file != null) { product.photoName = null; }
+   setState(() { });
+ }
```

**Resultado:** Podemos tomar una foto y la mostramos en tanto la tenemos
disponible ([ver gif](./img/_8_crud_productos_con_imagen.gif)).

<!--

#########################################

-->

----

<!--

#########################################

-->

### 229. Backend - Subir fotografías

- Fuentes
  - [Documentacion cloudinary](https://cloudinary.com/documentation/image_upload_api_reference)
  - [Util para poder subir imagenes](https://pub.dev/packages/mime_type#-readme-tab-)

A, Crear una cuenta en `api.cloudinary.com` y checar como usar la api

En mi caso me me dio la siguiente forma para usarlo

```r
curl --location --request POST 'https://api.cloudinary.com/v1_1/<LA_QUE_NOS_DIO>/image/upload/?upload_preset=<GENERAR_LLAVE>' \
--header 'Cookie: _cld_session_key=XXXXXXXXXXXXXXX' \
--form 'file=@"/E:/Pictures/random/cuadro_amarillo.png"'
```

B, Comprobamos que podamos subir una imagen, en mi caso en postman y
lo encerrado es la url que me muestra la imagen.

![57_servicio_tercero_subir_imagen](./img/57_servicio_tercero_subir_imagen.JPG)

C, instalar mime

```yaml
dependencies:
  flutter:
    sdk: flutter
+ mime_type: ^0.3.2
```

D, añadimos metodo en nuestro service `lib\src\services\ProductService.dart`

```dart
+ import 'dart:io' show File;
+ import 'package:mime_type/mime_type.dart' as mime;
+ import 'package:http_parser/http_parser.dart' show MediaType;

class ProductService {

  ...
  
+ Future<String> uploadFile(File image) async {
    const base = 'https://api.cloudinary.com/v1_1/<CENSORED/image/upload/?upload_preset=<CENSORED>';
    final url = Uri.parse(base);

//obtenemos el tipo de imagen
    final List<String> mimeType = mime.mime(image.path).split('/'); // ejemplo: "image/jpeg"
// wrapper a archivo para poderlo subir
    final file  = await http.MultipartFile.fromPath('file', image.path, contentType: MediaType(mimeType[0], mimeType[1]));
    
// agregamos archivo a la peticiom
    final uploadRequest = http.MultipartRequest('POST', url);
    uploadRequest.files.add(file);
//enviamos peticion
    final streamResponse = await uploadRequest.send();

    final resp = await http.Response.fromStream(streamResponse);

    if(resp.statusCode == 200 || resp.statusCode == 201) {
      final respData = json.decode(resp.body);
      final pathOfFileUploaded = respData['secure_url'];
      return pathOfFileUploaded;
    } else {
      return null;
    }
  }
}
```

E, Agregamos nuestro metodo en el page `lib\src\pages\ProductPage.dart`

```dart
void _add(BuildContext context, ) {
// este disparara los validadores dontro del formulario
  bool formIsValid = formKey.currentState.validate();
  if(formIsValid) {
// ejecutamos los metodos "onSaved"
    formKey.currentState.save();
    setState(() { isLoading = true; });
//agregar producto      
+   _uploadProduct()
+     .then((success) {
+       if(success) {
+         Navigator.of(context).pop(InternalMsgsEnum.SAVED_OK);
+       } else {
+         print('algo fallo');
+       }
+     })
    ;
  }
}

Future<bool> _uploadProduct() async {
  bool infoProductUploaded = false;
  bool imageProductUploaded = false;
  // al querer actualizar si "product.photoName" es nulo significa que llego nulo y asi se quedo,
  // o que llego con valor y al agregar imagen este paso a nulo, lo que significa que hay que subir 
  // la imagen que este en file, 
  if(file != null && (product.id == null || product.photoName == null) ) {
    print('Subiendo imagen...');
    String urlPhoto = await productService.uploadFile(file);
    if(urlPhoto != null) { imageProductUploaded = true; product.photoName = urlPhoto; }
  } else { 
    print('imagen de producto a actualizar no se cambio o NO se asigno'); imageProductUploaded = true; 
  }
  if(product.id == null) {
    final ProductModel respProductUploaded = await productService.save(product);
    if(respProductUploaded.id != null) { infoProductUploaded = true; }
  } else {
    final ProductModel respProductUploaded = await productService.update(product);
    if(respProductUploaded.id != null) { infoProductUploaded = true; }
  }
  if(infoProductUploaded && imageProductUploaded) {
// la imagen y el producto se subieron correctamente
    return true;
  } else {
    if(infoProductUploaded==false && imageProductUploaded==false) { print('fallo registro y subida de imagen de producto'); }
    else if(infoProductUploaded==false) { print('fallo registro de producto'); } 
    else if(imageProductUploaded==false) { print('Fallo subida de imagen de producto'); }
    return false;
  }     
}

```

**Resultado:** Podemos subir la foto al backend desde la app, **falta**
que se muestre la imagen subida en el backend.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 232. Mostrar imágenes cargadas

**Objetivo:** Mostrar las imagenes que tengamos guardadas en el back,t
tanto en la lista de productos como en la del formulario.

A, Para mostrarlo en la lista en `lib\src\pages\HomePage.dart`

```dart
Widget _buildItemList(BuildContext context, ProductModel product, int index) {
 return Dismissible(
  key: Key(product.id),
  background: Container(color: Colors.red[50],),
  onDismissed: (direction) { products.removeAt(index); productService.deleteById(product.id); },
// lo agregamos para poser segur navegando al formulario
+     child: GestureDetector(
    onTap: () {
      Navigator.of(context).pushNamed(ProductPage.ROUTE_NAME, arguments: product)
      .then((beforeAction) { _actionRealized(context, beforeAction); });
    },
    child: Container(
      margin: EdgeInsets.only(bottom: 25),
      padding: EdgeInsets.symmetric(horizontal: 5),
//como paso a ser un elemento mas complejo lo mostramos en Cards
+         child: Card( elevation: 2,
        child: Column( crossAxisAlignment: CrossAxisAlignment.start,
          children: [
// si tenemos la imagen "photoName" significa que ya esta en el back
+               (product.photoName == null) 
              ? Image(image: AssetImage('assets/img/no-image.png'),) 
              : FadeInImage(
                placeholder: AssetImage('assets/img/jar-loading.gif'),
                image: NetworkImage(product.photoName), height: 150, width: double.infinity, fit: BoxFit.cover,
              ),
              Text(product.name, style: TextStyle(fontSize: 20)), Text('Precio: ${product.price}'),
          ], 
), ), ), ), ); }
```

B, Para mostrarlo en el formulario en `lib\src\pages\ProductPage.dart`
mostramos la imagen que nos llegue de product

```dart
final productImage = (product.photoName == null) 
  ? Image(
      image: AssetImage( file?.path ?? 'assets/img/no-image.png'),
      height: 250,
      fit: BoxFit.cover,
    ) 
  : FadeInImage(
    placeholder: AssetImage('assets/img/jar-loading.gif'),
    image: NetworkImage(product.photoName),
  )
;
```

**Resultado:** Podemos ver los prosuctos con sus respectivas imagenes
que guardamos en el back.

![_8_crud_productos_con_imagen](./img/_8_crud_productos_con_imagen.gif)

<!--

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Sección 13:Login y manejo de Tokens

Fuente:

- [Documentacion Firebase regustro de usuarios](https://firebase.google.com/docs/reference/rest/auth#section-create-email-password)

En la aplicacion que hemos estado haciendo, la parte de login vamos a
gregarle funcionalidad, usando el auth de firebase, el cual necesitaremos
hacer uso de JWT, que guardaremos en las preferencias de usuario, para
mandalo de regreso y asi auth al usuario.

- Temas de la sección:
  - Funcionamiento completo del login
  - Registro de nuevos usuarios
  - Firebase Auth Rest API
  - Manejo de Tokens
  - Alertas
  - Habilitar autenticación en Firebase

A, Creamos pantalla de auth igual que el login, lo unico a resltar es
que, al comprobar si tenemos la misma pass, hacemos lo sig.

- vemos que agregamos un nuevo stream igual que los otros

**Nota:** Cambie el codigo para separar en DAO, Service y Presenter

```dart
Stream<bool> getConfirmPassValid() {
  return Rx.combineLatest3(
  this.getEmailStream(),
  this.getPassStream(),
  this.getPassConfirmStream(),
  (email, pass, confirmPass) { 
    if(email != null && pass != null && confirmPass != null) {
      if(pass == confirmPass){
        return true; 
      } else {
        passConfirmController.sink.addError('No coincide');
      }
    }
    return false; 
  }
);
}
```

B, Vamos a firebase -> proyecto -> authenticacion -> UsersSign-in

![58_firebase_conf_auth](./img/58_firebase_conf_auth.JPG)

C, Creamos util o DAO para guardar el token en el telefono

- Ver [187. Shared Preferences y cargar informacion ANTES DE QUE INICIE LA APPLICACION](#187-shared-preferences-y-cargar-informacion-antes-de-que-inicie-la-applicacion)

D, Vemos como consumir la api y agregamos nuestro DAO en la
[Doc](https://firebase.google.com/docs/reference/rest/auth#section-create-email-password)

```dart
import 'dart:convert'; import 'package:form_bloc/src/model/ErrorModel.dart'; import 'package:http/http.dart' as http;
import 'package:form_bloc/src/model/InternalMsgsEnum.dart'; import 'UserSharedPreferencesDAO.dart';

class AuthUserDao {
  
  static final _base        = 'https://identitytoolkit.googleapis.com/v1/accounts';
  final        _urlSingUp   = '$_base:signUp?key=$_KEY';
  final        _urlLogIn    = '$_base:signInWithPassword?key=$_KEY'; static const _tokenName = 'idToken';
  static const _KEY = '<CENSURADO>'; final _usrpref = new UserSharedPreferencesDAO();

  Future<Map<String, dynamic>> baseRequest(final String email, final String pass, final String url) async {
    final regData = { 'email': email, 'password': pass, 'returnSecureToken' : true, };
    final respRaw = await http.post(url, body: json.encode(regData) );
    return json.decode(respRaw.body);
  }

  Future<bool> addUser(String email, String pass) async {
    Map<String, dynamic> respMap = await this.baseRequest(email, pass, _urlSingUp);
    if(respMap.containsKey(_tokenName)) {
      _usrpref.token = respMap[_tokenName];
      final String savedToken = _usrpref.token;
      if(savedToken != null && savedToken.isNotEmpty) { return true; }
    } else {
      String rason = 'error';
      try { rason = respMap['error']['message']; } catch(e) {}
      if(rason == 'EMAIL_EXISTS') { throw InternalMsgsEnum.ALREADY_EXIST; }
      else { throw InternalMsgsEnum.ERROR; }
    }
    return false;
  }

  Future<bool> authUser(String email, String pass) async {
    Map<String, dynamic> respMap = await this.baseRequest(email, pass, _urlLogIn);
    if(respMap.containsKey(_tokenName)) {
      _usrpref.token = respMap[_tokenName];
      final String savedToken = _usrpref.token;
      if(savedToken != null && savedToken.isNotEmpty) { return true; }
    } else {
      String rason = 'error';
      try { rason = respMap['error']['message']; } catch(e) {}
      if(rason == 'INVALID_PASSWORD')              { throw InternalMsgsEnum.INVALID_PASSWORD;  } 
      else if(rason.contains('TOO_MANY_ATTEMPTS')) { throw InternalMsgsEnum.TOO_MANY_ATTEMPTS; } 
      else if(rason.contains('EMAIL_NOT_FOUND'))   { throw InternalMsgsEnum.EMAIL_NOT_FOUND;   }
      else { throw InternalMsgsEnum.ERROR; }
    }
    return false;
  }
}
```

B, Usamos los dos metodos que agregamos en su respectivo formulario

```dart
static void login(BuildContext context) {
    _authUserDao.authUser(_loginBLoC.email, _loginBLoC.pass)
      .then((success) => { if(success) { Navigator.of(context).pushReplacementNamed(HomePage.ROUTE_NAME) } })
      .catchError((error) {
        final errorMsg = 'Error, intente mas tarde';
        if(error is InternalMsgsEnum) {
          if     (error == InternalMsgsEnum.INVALID_PASSWORD)  { _loginBLoC.passController.sink.addError(_invalidPassword); return; }
          else if(error == InternalMsgsEnum.TOO_MANY_ATTEMPTS) { _loginBLoC.emailController.sink.addError(_tooManyAttemps); return; }
          else if(error == InternalMsgsEnum.EMAIL_NOT_FOUND)   { _loginBLoC.emailController.sink.addError(_badCredentials); return; }
        }
        _loginBLoC.emailController.sink.addError(errorMsg);
      })
    ;
  }

//
static Future<void> register(BuildContext context) async {
  try {
    bool success = await _authUserDao.addUser(_loginBLoC.email, _loginBLoC.pass);
    if(success) { return ; }
    else { throw InternalMsgsEnum.ERROR; }
  }
  catch (e) { throw e; }
}
```

**Resultado:** Podemos hacer login y register respectivamente, **falta**
enviar el token, para peticiones de solo usuarios registrados, pero
desde un punto visual ya no tendriamos diferencias.

![.GIF](./img/_9_login_y_registro_en_firebase.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 242. Usar token para validar peticiones en Firebase

**Objetivo:** Ta estamos haviendo login correctamento, donde igual estamos
guardando el token que nos llega. Nos falta poner como privadas nuestras
peticiones, y enviarles el token para solo asi poder acceder a ellas.

A, Privatisar peticiones

- Vamos a `firebase -> database -> rules` y agregamos estas reglas.

```json
{
  "rules": {
//  ".read": true,
//  ".write": true
    ".read": "auth != null",
    ".write": "auth != null"
  }
}
```

B, Si hacemos mas peticiones, este nos deberia de bloquearlas (agregar
como manejar esto), por lo qe vamos a agregar el token en nuestra
peticion.

```dart
  Future<List<ProductModel>> findAll() async {
// agregamos el auth de la sig manera en el url
#   final respRaw = await http.get('$_URL.json?auth=${_usrpref.token}');
// agregamos una manera de lidear con los posibles errores
+   RequestHandlerErrors.ifAreThereErrorThrowIt(respRaw);
```

**Resultado:** Nos identificamos al servicio de firebase para que no,
nos bloquee las peticiones.

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Detalles finales de la aplicación de productos (uso de objetos en Streams)

**Objetivo:** En esta seccion vamos a cambiar en Productos el uso del
DAO directo en Precenter, para pasarlo a un BLoC.

A, Crear BLoC para productos

```dart
import 'dart:io'; import 'package:form_bloc/src/dao/ProductDAO.dart';
import 'package:rxdart/rxdart.dart'; import 'package:form_bloc/src/model/ProductModel.dart';
class ProductBLoC {
//declaracion
  final _productsController  = new BehaviorSubject<List<ProductModel>>();
  final _isLoadingController = new BehaviorSubject<bool>();
  final _productsDAO = new ProductDAO();

  void findAllProduct() async {
    List<ProductModel> allProducts;
    try { allProducts = await _productsDAO.findAll(); }
    catch (e) { _productsController.sink.addError(e); return ; }
    _productsController.sink.add(allProducts);
  }

  Future<ProductModel> saveProduct(ProductModel newProduct) async {
    _isLoadingController.sink.add(true);
    ProductModel productAdded;
    try { productAdded = await _productsDAO.save(newProduct); } 
    catch(e) { throw e; }
    _isLoadingController.sink.add(false);
    return productAdded;
  }

  Future<String> saveProductImage(File image) async {
    _isLoadingController.sink.add(true);
    final url = await _productsDAO.uploadFile(image);
    _isLoadingController.sink.add(false);
    return url;
  }

  Future<ProductModel> updateProduct(ProductModel newProduct) async {
    _isLoadingController.sink.add(true);
    final productAdded = await _productsDAO.update(newProduct);
    _isLoadingController.sink.add(false);
    return productAdded;
  }

  Future<String> deleteProduct(String idProduct) async {
    final idProductDeleted = await _productsDAO.deleteById(idProduct);
    return idProductDeleted;
  }
// utils
  dispose() { _productsController?.close(); _isLoadingController?.close(); }
//getters/setters
  Stream<List<ProductModel>> get productsStream  => _productsController.stream;
  Stream<bool>               get isLoading  => _isLoadingController.stream;
}
```

B, Agregamos nuestro block en `lib\src\blocs\MyInheritedWidget.dart`

```dart
class MyInheritedWidget extends InheritedWidget {

+ final productBLoC = new ProductBLoC()

+ static ProductBLoC productsOf (BuildContext ctx) {
+   return (ctx.dependOnInheritedWidgetOfExactType<MyInheritedWidget>().productBLoC);
+ }
```

<!-- obtener context al inicio, -->

X, En `lib\src\pages\HomePage.dart` remplazamos el dao por nuestro BLOC

```dart
...
class _HomePageState extends State<HomePage> {
- //final productService = ProductDAO();

// @override
  // void initState() { 
- //   this.requestAllProducts = productService.findAll();
// ALTERNATIVA CUANDO ESTAMOS NUESTRO BLOC LO USAMOS COMO SINGLETON  
  //   productBLoC = new ProductBLoC(); // usando singleton
  //   productBLoC.findAllProduct(); 
  //   super.initState();
  // }

  // este metodo se ejecuta despues del "initState" lo cual ya tenemos
  //disponible el context para usar a MyInheritedWidget
+ @override void didChangeDependencies() {
+   super.didChangeDependencies();
+   productBLoC = MyInheritedWidget.productsOf(context);
//NO_SE creo es para si cambia algo desde aqui se detecte
+   if (this.productBLoC != productBLoC) { this.productBLoC = productBLoC; }
+   productBLoC.findAllProduct();
+ }

@override
  Widget build(BuildContext context) {
    ...
// CAMBIAR DAO POR EL BLOCK
- //final productList = FutureBuilder(
- //  future: this.requestAllProducts,
+   final productList = StreamBuilder(
+     stream: productBLoC.productsStream,  

...
    onDismissed: (direction) {
        setState(() {  products.removeAt(index); });
// lo mismo para el eliminar producto
        //productService.deleteById(product.id);
        productBLoC.deleteProduct(product.id);
      },
```

TODO: PENDIENTE, es lo mismo para agregar el producto y modificar

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Sección 15:Despliegue en Google PlayStore y Apple AppStore

**Introduccion:** Para poder hacer los desplieges necesitaremos licencias
en las respectivas tiendas, la de IOS (AppStore) esta 100 dolares el año,
mientras que la de Android (Play Store) cuesta 30 dolares de por vida.

Resumen para deplegar en [este pdf](./img/_10_guia_de_despliegues_AppStores.pdf)

### Cambiar el icono de la Aplicacion

- Fuentes:
  - [flutter launcher icons](https://pub.dev/packages/flutter_launcher_icons)

- Requisitos
  - Necesitamos un icono de 1024x1024 si no IOS empieza a ladrar
  - Toman en cuenta las siguientes dimenciones en android

```r
mipmap-mdpi    - 48
mipmap-hdpi    - 72
mipmap-xxhdpi  - 96
mipmap-xxhdpi  - 144
mipmap-xxxhdpi - 192
```

<!-- icono de aplicacion icono aplicacion, icono de la aplicacion -->

A, instalamos dependencia

```yml
dev_dependencies:
  flutter_test:
    sdk: flutter

+ flutter_launcher_icons: ^0.8.1
```

B, Agregamos configuracion (ya debemos de tener el icono)

- La ruta/nombre del icono es irrelevante

```yml
flutter_icons:
  android: "launcher_icon"
  ios: true
  image_path: "assets/icons/launch_icon.png"
```

C, Ejecutamos el sig. comando en la raiz del comando

```r
flutter packages pub run flutter_launcher_icons:main
```

**Resultado:** Ya deberiamos de poder ver el icono que señalamos, en el
acceso de nuestra aplicacion en nuestro dispositivo.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 256. Configurar el splashscreen

A, Abrir el archivo `android\app\src\main\res\drawable\launch_background.xml`

- Descomentamos

En mi caso usare el mismo que el icono para lanzar la app, pero podemos
poner las que querramos, solo respetar las dimenciones propuestas por
android

```xml
<?xml version="1.0" encoding="utf-8"?>
<!-- Modify this file to customize your launch splash screen -->
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:drawable="@android:color/white" />

    <!-- You can insert your own image assets here -->
    <item>
        <bitmap
            android:gravity="center"
            android:src="@mipmap/launcher_icon" />
    </item>
</layer-list>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Construir APK

<!-- generar apk, crear apk, apk de produccion -->

Construir: `flutter build apk --release`

<!--

#########################################

-->

----

<!--

#########################################

-->

### ................PENDIENTE

todo: terminar seccion

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Sección 16:Push notifications

**Objetivo:** Vamos a crear una aplicacion de notificaciones sencilla,
la cual constara de poderlas revir estas ya sea que nuestra app este
cerrada o abierta, y que si le damos click a la notificacion en el
notifications bar o mientras tenemos abierta la app, esta nos mkandara
una pantalla en espesifico.

- Requisitos: Para IOs necesitamos una cuenta de desarrollador de IOS

### 266. Configuración de FCM (Firebase Cloud Messaging)

- Fuente:
  - [README firebase messaging](https://pub.dev/packages/firebase_messaging)
  - [Generar llave](https://developers.google.com/android/guides/client-auth)

A, Agregamos plugin

```yml
dependencies:
  flutter:
    sdk: flutter

+ firebase_messaging: ^7.0.3
```

B, Configurar Firebase

B,1 Vamos a `android\app\build.gradle` y modificamos el `applicationId`
que este dentro del bloque `defaultConfig`

- **NOTA:** Lo podemos dejar como esta, pero si lo cambiamos debemos de
cambiarlo tambien en los siguietnes lugares
- **Recomendacion:** Usar el buscador/repmpazador del IDE para buscar
  en todos archivos el ID antiguo y sustituir por el nuevo.

<!-- cambiar id de la aplicacion, cambiar dominio de la aplicacion,
cambiar paquete de la aplicacion, cambiar package
error Overlay manifest:package attribute declared at AndroidManifest.xml:2:5-40 value=(com.example.example)
	has a different value=(com.example.example) declared in main manifest at AndroidManifest.xml:2:5-40
	Suggestion: remove the overlay declaration at AndroidManifest.xml 	and place it in the build.gradle:

-->

En `android\app\build.gradle`

```r
  defaultConfig {
#modificamos
!     applicationId "com.mario51.notifications"
```

En `android\app\google-services.json` Nos aseguramos que tengamos el mismo

```json
"client": [ { "client_info": { "mobilesdk_app_id": "1:227764933071:android:4a86ef4d61a5bd070a42ba",
        "android_client_info": {
!         "package_name": "com.mario51.notifications"
        }
```

En `android\app\src\main\AndroidManifest.xml`

```r
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
#modificamos
!   package="com.mario51.notifications">
```

En `android\app\src\debug\AndroidManifest.xml`

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.mario51.notifications">
```

Renombramos la carpeta            `android\app\src\main\kotlin\com\example\xxx`
a que coincida con lo que pusimos `android\app\src\main\kotlin\com\mario51\xxx`

En `android\app\src\main\kotlin\com\mario51\notifications\MainActivity.kt`
cambiamos el paquete

```r
+ package com.mario51.notifications
- #package com.example.xxxx
  import io.flutter.embedding.android.FlutterActivity
  class MainActivity: FlutterActivity() { }
```

**Recomendado:** Reiniciar app en este punto para saber si salio bien.

B,2, Ejecutamos para sacar la llave, si no le hemos movido nada la pass
que nos pide es android

```r
keytool -list -v \
-alias androiddebugkey -keystore ~/.android/debug.keystore

Enter keystore password:
Alias name: ... Creation date: Oct 5, 2019 Entry type: ...
...
Certificate fingerprints:
         SHA1: <CENSURADO> # ESTE ES EL QUE COPIAREMOS
         SHA256: <CENSURADO>
Signature algorithm name: -... Subject Public Key Algorithm: ... Version: ...
```

B,2 Creamos aplicacion en firebase `proyecto -> añadir Aplicacion (logo Android)`

B,3, Pegamos el `applicationId` y el `SHA1` que copiamos

B,4, Nos dara un archivo a descargar y lo ponemos donde nos indique
en mi caso a la altura de la carpeta src `android\app\google-services.json`

B,5, En `android\build.gradle` agregamos

```java
buildscript {
  repositories {
// NOS ASEGURAMOS QUE ESTE EGREGADA
!   google()
  }
  dependencies {
    classpath 'com.android.tools.build:gradle:3.5.0' classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
+   classpath 'com.google.gms:google-services:4.3.4'
}

allprojects {
  repositories {
// NOS ASEGURAMOS QUE ESTE ESTE
!   google()  // Google's Maven repository
    ...
  }
```

B,6, En `android\app\build.gradle` junto a los otros `apply plugin` que
ya estan en este archivo

```dart
  apply plugin: 'kotlin-android'
// agregamos
+ apply plugin: 'com.google.gms.google-services'

dependencies {
//agregamos  
+ implementation platform('com.google.firebase:firebase-bom:26.3.0')
```

B.6. Detenemos y corremos la aplicacion, **todo deberia de ir bien**

B,7. Agregamos en `android\app\src\main\AndroidManifest.xml` lo sig.
para poder interactuar con las notificaciones

```xml
    </intent-filter>
+   <intent-filter>
+     <action android:name="FLUTTER_NOTIFICATION_CLICK" />
+     <category android:name="android.intent.category.DEFAULT" />
+   </intent-filter>
  </activity>
```

**Resultado:** Creamos un proyecto de firebase y lo ligamos a nuestra
app, como tambien configuraciones, aun nos **falta** probar y asegurar
que corran en el backglound/cuando esta cerrada nuestra app.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 267. Cambiar el ID de la aplicación - Android

- Fuente:
  - [README firebase messaging](https://pub.dev/packages/firebase_messaging)
  - [Ultima versiones](https://firebase.google.com/support/release-notes/android#latest_sdk_versions)

A, En `android\app\build.gradle` agregamos la sig. libreria

```r
dependencies {
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    implementation platform('com.google.firebase:firebase-bom:26.3.0')
#// buscar la ultima version en https://firebase.google.com/support/release-notes/android#latest_sdk_versions
    implementation 'com.google.firebase:firebase-messaging:21.0.1'
}
```

```java
package io.flutter.plugins.firebasemessagingexample;
import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugins.firebasemessaging.FlutterFirebaseMessagingService;
public class Application extends FlutterApplication implements PluginRegistrantCallback {
  @Override
  public void onCreate() {
    super.onCreate();
    FlutterFirebaseMessagingService.setPluginRegistrant(this);
  }
  @Override
  public void registerWith(PluginRegistry registry) {
    GeneratedPluginRegistrant.registerWith(registry); // esta linea pueque de error
  }
}

// error incompatible types: PluginRegistry cannot be converted to FlutterEngine
// Execution failed for task ':app:compileDebugJavaWithJavac'
//---------------------------- SI EL CODIGO DE ARRIBA DA ROLLOS USAR EL DE ABAJO```java

package <MI-DOMINIO-CAMBIAR>;
import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugins.firebasemessaging.FlutterFirebaseMessagingService;
// Nota, esta linea la agregue
import io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin;
public class Application extends FlutterApplication implements PluginRegistrantCallback {
  @Override
  public void onCreate() {
    super.onCreate();
    FlutterFirebaseMessagingService.setPluginRegistrant(this);
  }
  @Override
  public void registerWith(PluginRegistry registry) {
    // GeneratedPluginRegistrant.registerWith(registry);
    FirebaseMessagingPlugin.registerWith(registry.registrarFor("io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin"));
  }
}
```

X, En el manifest agregamos

```r
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.example.notifications">
    <application
      android:name=".Application"
# MODIFICAMOS
#       android:name="io.flutter.app.FlutterApplication"

```

**Resultado:** Hicimos la configuracion para que nuestra aplicacion
soporte las notificaciones en el backgound.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 268. Provider para controlar las notificaciones y FCM Token

- Fuente
  - [Readme firebase messaging](https://pub.dev/packages/firebase_messaging)

**Observacion:** Me dio errores raros al iniciarlo las 2 o 3 primeras
veces, pero despues de unos cunatos reinicios completos ya no salieron
estos.

A, Agregamos la siguiente configuracion/handlers, lo cual solo podemos
hacer cosas simples sin tanto rollo

```dart
import 'package:firebase_messaging/firebase_messaging.dart';
class PushNotificationProvider {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  initNotificationConf() async {
    //pedimos permiso de poder recibir notificacions
    await _firebaseMessaging.requestNotificationPermissions();
    final token = await _firebaseMessaging.getToken();
    print('=============== Mua Token: $token');
    _firebaseMessaging.configure(
      onMessage: _myOnMessage,
      onBackgroundMessage: _myOnBackgroundMessage,
      onLaunch: _myOnLaunch,
      onResume: _myOnResume,
    );
  }
//cuando recibimos la notificacion con la app abierta
  static Future<dynamic> _myOnMessage(Map<String, dynamic> message) {
    print('============== OnMessage ================');
    print('LLego: $message');
  }
//cuando recibimos la notificacion con la app cerrada
  static Future<dynamic> _myOnBackgroundMessage(Map<String, dynamic> message) async {
    print('============== BackgroundMessage ================');
    print('LLego: $message');
  }
  static Future<dynamic> _myOnLaunch(Map<String, dynamic> message) {
    print('============== OnLaunch ================');
    print('LLego la notificacion: $message');
  }
  static Future<dynamic> _myOnResume(Map<String, dynamic> message) {
    print('============== OnResume ================');
    print('LLego la notificacion: $message');
  }
}
```

B. Agregamos nuestra conf al inicio de nuestra app

```dart
+ import 'providers/PushNotificationProvider.dart';
  class MyApp extends StatefulWidget {
    @override
    _MyAppState createState() => _MyAppState();
  }

  class _MyAppState extends State<MyApp> {

    @override
    void initState() { 
      super.initState();
+     final pushProvider = new PushNotificationProvider();
+     pushProvider.initNotificationConf();
    }
```

**Resultado:** Al correr la app, talvez no a la primera (ver observacion)
deberiamos de poder obtener un token que nos genera firebase. Lo que
sifnifica que podemos usar este token para enviarle notificaciones a
este usuario en particular.

```r
=============== Mua Token: dsA...:APA9...-T6Zj..-7kE..-_Raz...
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 269. Recibir notificación - onMessage, onLaunch y onResume

**Objetivo:** Ya con el token generado y a la mano, podremos enviarle
una notificacion al usuario desde firebase.

A, En `Firebase -> proyecto -> Interacción -> Cloud Messaging`
llenamos la info que nos pide entre ello el token y le damos en
`enviar emnsaje de prueba` y nos deberia de llegar la notificacion

**Nota:** para recibir en android la notificacion cuando esta en el
backgound (no cerrada), debemos de enviar el siguiente parametro, dentro
de los campos que nos pide firebase en `Otras opciones (opcional)`.

```r
click_action: FLUTTER_NOTIFICATION_CLICK
```

![59_firebase_ui_enviar_notificacion](./img/59_firebase_ui_enviar_notificacion.JPG)

- Observaciones
  - Si tenemos la app abierta veremos el `OnMessage`
  - Si tenemos la app en background(no terminada) no veremos nada cuando
    llege, pero si cuando le demos click veremos el `OnResume`
  - Si tenemos la app en terminada (no en background) podremos ver el
    `OnBackgroundMessage`, si nos llega la notificacion, pero es complicarlo
    comprobarlo ya que,no tenemos acceso a los logs.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 270. Reaccionar cuando recibimos una notificación

**Objetivo** Programaremos la accion cuando el usuario le da click sobre
la notificacion, y cuando la recibimos estando dentro de esta, para eso
usaremos streams para escuchar en otras partes de la app.

A, Agregamos el listener

```dart
  import 'dart:async'; import 'package:firebase_messaging/firebase_messaging.dart';
  class PushNotificationListener { final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
+   static final _notificationStreamController = StreamController<NotificationModel>.broadcast();
+   static Stream<NotificationModel> get notificationStream => _notificationStreamController.stream;

//el mismo curpo para todas las funciones "myOn..."
    static Future<dynamic> _myOnMessage(Map<String, dynamic> message) async {
      print('============== OnMessage ================');
+     notifyNotification(message);
    }

+   static notifyNotification(Map<String, dynamic> message) {
+     final notification = NotificationModel.fromJsonMap(message);
+     _notificationStreamController.sink.add(notification);
+   }
```

B, Opcional: Manejaremos nuestras notificaciones como modelso

```dart
class NotificationModel {
  final String _title; final String _message; final String _category;
  final recibed = DateTime.now();
  NotificationModel(String title, String message, String category):  this._title=title, this._message=message, this._category=category;
//convertimos el json que llega a nuestro modelo personalizado
  factory NotificationModel.fromJsonMap(Map<String, dynamic> jsonMap) {
    try {
+     String title = jsonMap['notification']['title'];
+     String message = jsonMap['notification']['body'];
+     String category = jsonMap['data']['category'];
+     return new NotificationModel(title, message, category);
    } catch(e) {
      print('Error al parsear notificacion'); print(e); return new NotificationModel('Unknown', jsonMap.toString(), 'unknown');
    }
  }
// getters/setters
  String get title    => this._title;  String get message  => this._message; String get category => this._category;
}
```

C, Agregamos el escucha en cualquiera de nuestras paginas.

- **Requisito:** ya debio de haber corrido el metodo `initNotificationConf`
  no necesario en el mismo bloque, pero en el ciclo de nuestra app

```dart
PushNotificationProvider.notificationStream
  .listen((notification) => 
    print(notification.title)
  )
;
```

**Resultado:** Tenemos disponible el evento de la notificacion en nuestra
app a traves de un listener del stream.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 271. Uso del Navigator en el initState

<!-- context initState context, context en initState y context -->

**Objetivo:** En nuestro caso particular queremos mostrar cuantas
notificaciones tenemos a travez de una señalizacion en el icono del
menu.

A, Agregamos nuestro BLoC, para no manerar el listener directamente
y este se encargara de la lista de notificaciones en `lib\src\services\NotificationsBLoC.dart`.

```dart
import 'dart:async';

import '../utils/PushNotificationListener.dart';
import '../models/NotificationModel.dart';

class NotificationsBLoC {
  static final _singleton = new NotificationsBLoC._internal(); NotificationsBLoC._internal(); factory NotificationsBLoC() { return _singleton; }

  final _notifications = new List<NotificationModel>();
  final _notificationsStreamController = StreamController<List<NotificationModel>>.broadcast();
  Stream<List<NotificationModel>> get notificationsStream => _notificationsStreamController.stream;

//ejecutamos este para escuchar y agregar las notificaciones que lleguen
  void listenNotifications() {
    PushNotificationListener.notificationStream
      .listen((notification) { 
        addNotification(notification); 
      })
    ;
  }

  void addNotification(NotificationModel notification) {
    _notifications.add(notification);
    _notificationsStreamController.sink.add(_notifications);
  }

  void dispose() {  _notificationsStreamController.close(); }
}
```

B. Agregamos la escucha de notificaciones

```dart
class _MyAppState extends State<MyApp> {

  @override
  void initState() { 
    super.initState();
    final pushProvider = new PushNotificationListener();
    pushProvider.initNotificationConf();
// notar que puede ser otro lugar pero ya debio de ejecutarse el "initNotificationConf();"
    final notificationsBLoC = new NotificationsBLoC();
    notificationsBLoC.listenNotifications();
  }
```

<!-- mostrar notificaciones en icono notificaciones icono -->

C. Modificar icono para mostrar notificaciones

```dart
import 'package:flutter/material.dart';
import '../models/NotificationModel.dart';
import 'package:notifications/src/services/NotificationsBLoC.dart';
import 'package:notifications/src/widgets/NamedIcon.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key}) : super(key: key);
  final scaffoldKey = GlobalKey<ScaffoldState>();
  static const NAME = 'home';
//incluimos el BLOC
  final notificationsBLoC = new NotificationsBLoC();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Home'),
// modificar el icono del Drawer
        leading: _buildIconDrawer(),
      ),
      drawer: Drawer(),
      body: Center(
        child: RaisedButton(
          child: Text('Notificar'),
//simular que llega notificacion
          //onPressed: () => notificationsBLoC.addNotification(new NotificationModel('titulo', 'msg', 'cate')),
          onPressed: ()=>{},
        ),
      ),
    );
  }

  Widget _buildIconDrawer() {
//este escuchara notificaciones y las mostrara el numero de notificaciones
//dentro del icono.
    return StreamBuilder(
      stream: notificationsBLoC.notificationsStream,
      builder: (BuildContext context, AsyncSnapshot<List<NotificationModel>> snapshot){
        return NamedIcon(
          iconData: Icons.menu,
          notificationCount: (snapshot.hasData) ? snapshot.data.length : 0,
          onTap: () { scaffoldKey.currentState.openDrawer(); },
        );
      },
    );
  }
}
```

**Resultado:** Tenemos un feddback en la pantalla cuando nos llega una
notificacion.

![_11_enviar_y_recibir_notificacio](./img/_11_enviar_y_recibir_notificacion.gif)

### Notas finales de la seccion

Realice mas cambios en donde tenemos una pantalla de lista de notificaciones,
pero utilize puros streams, todo esta en el codigo.

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Sección 17:Provider - Aplicación de noticias

Aprenderemos a usar Provider como manejador de estado de su aplicación,
pero también veremos los siguientes temas:

- Provider, ChangeNotifierProvider y Multiprovider
- NewsAPI
- PageView
- PageControllers
- Mapas
- Mantener el estado de la aplicación

**PageView:** Este ya lo usamos, y nor permite tener varias paginas, las
cuales podemos acceder a ellas haciendo swipe horizontal o vertical,
segun lo tengamos configurdo.

**Objetibo** Crearemos la base de nuestro proyecto, la cual sera un
navigation bar inferior que nos llevara a dos paginas diferentes.

A, Instalamos el privider

```yml
dependencies:
  flutter:
    sdk: flutter
+ provider: ^4.3.3
```

B, Creamos el helper/providar que nos ayudara con la navigacion

```dart
import 'package:flutter/material.dart' show ChangeNotifier, PageController, Curves;
class NatigationHelper with ChangeNotifier {
  final _pageController = PageController();
  int   _actualPage     = 0;
  PageController get  pageController => this._pageController;
  int get  actualpage => this._actualPage;
  void setActualpage(int actualPage, {bool fromSwipe=false}) {
    this._actualPage=actualPage;
    // existe un problema, que cuando viene del swipe este metodo se ejecuta dos veses, por lo que
    // asignamos tiempos y banderas para evitar que el "notifyListeners()" se llame dos veces, lo cual
    // da una un pequeño congelamiento
    if(fromSwipe) {
      Future.delayed(Duration(milliseconds: 500), () => { notifyListeners() });
    } else {
      _pageController.animateToPage(_actualPage, duration: Duration(milliseconds: 500), curve: Curves.easeOut);
      if(canPass) { notifyListeners();
    }
      canPass = false;
      Future.delayed(Duration(milliseconds: 500), () => { canPass = true });
    }
  }
}
```

C, Creamos el mateapp de siempre, lo interezante esta en el Home

```dart
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      initialRoute: HomePage.NAME,
      debugShowCheckedModeBanner: false,
      routes: {
        HomePage.NAME : (ctx) => HomePage(),
      },
      theme: myTheme,
    );
  }
}
```

D, Creamos nustra pantalla home

```dart
import 'package:flutter/material.dart';
import 'package:news/src/helpers/NatigationHelper.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  static const NAME = 'home';
  const HomePage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<NatigationHelper> (
      create: (ctx) { 
// instancia compartida entre componentes hijos, la cual se guardara en el context de los hijos
// en este caso, para que lo puedan usar "_Pages" y  "_Navigatio"
+       return new NatigationHelper();
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('news'),
        ),
        body: _Pages(),
        bottomNavigationBar: _Navigation(),
      ),
    );
  }
}

class _Navigation extends StatelessWidget {
  const _Navigation({Key key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final navigationHelper = Provider.of<NatigationHelper>(context);

    return BottomNavigationBar(
      currentIndex: navigationHelper.actualpage,
      onTap: (selected) {
// estamos usando el setter que dispara el notify y la navegacion por el controller del page
        navigationHelper.actualpage = selected;
      },
      items: [
        BottomNavigationBarItem( icon: Icon(Icons.account_circle), label: 'Algo1'),
        BottomNavigationBarItem( icon: Icon(Icons.account_circle), label: 'Algo2')
      ],
    );
  }
}

class _Pages extends StatelessWidget {
  const _Pages({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final navigationHelper = Provider.of<NatigationHelper>(context);
    return PageView(
      //physics: BouncingScrollPhysics(), // evitar mostrar sombra de arrastre
      physics: NeverScrollableScrollPhysics(), // evitar mostrar sombra de arrastre
//controlaremos la navegacion fuera de la vista
+     controller: navigationHelper.pageController,
      children: [
        Container(child: Center(child: Text('page1'))),
        Container(child: Center(child: Text('page2'))),
      ],
    );
  }
}
```

**Resultado:** Tenemos una navegacion entre paginas basica.

![60_news_inicio_navegationbar_y_pages](./img/60_news_inicio_navegationbar_y_pages.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 283. NewsAPI - Obtener noticias del mundo

- Fuentes:
  - [API para obtener noticias](https://newsapi.org/)

**Objetivo:** Vamos a construir el DAO para la obtencion de noticias

A, Creamos una cuenta en newsapi.org y le damos a obtener key

B, Hacemos una peticion y convertimos con `quicktype`

C, Creamos nuestro DAO

```dart
import 'package:http/http.dart' as http; import 'package:news/src/model/NewsResponseModel.dart';
class NewsDAO {
  static const _KEY = '<CENSURADO>';
  static const _BASE = 'newsapi.org';
  Future<NewsResponse> getHeadline() async {
    final url = _buildUrl('top-headlines', {'sources': 'techcrunch'});
    final responseRaw = await http.get(url);
    final responseModel = NewsResponse.fromBodyResponse(responseRaw.body);
    return responseModel;
  }
  static String _buildUrl(String endPoint, Map<String, dynamic> params) {
    return Uri.http(_BASE, 'v2/$endPoint', { ...params, 'apiKey' : _KEY}).toString();
  }
}
```

D, Creamos nuestro Provider para manejarnos con la UI, sin tocar directo
el DAO

```dart
import 'package:flutter/material.dart' show ChangeNotifier;
import 'package:news/src/dao/NewsDAO.dart';
import 'package:news/src/model/Article.dart';
import 'package:news/src/model/NewsResponseModel.dart';

class NewsProvider with ChangeNotifier {
  final _articlesList = new List<Article>();
  final _newsDAO = new NewsDAO();

  NewsProvider() { getHeadlines(); }

  void getHeadlines() async {
    const headlinesPoint = 'top-headlines';
    final NewsResponse resp = await _newsDAO.getHeadline(headlinesPoint);
    this._articlesList.addAll(resp.articles);
    notifyListeners();
  }

  List<Article> get articlesList => this._articlesList;
}
```

E, Creamos la pagina de lista de noticias

```dart
import 'package:news/src/providers/NewsProvider.dart';import 'package:news/src/widgets/NewsList.dart';import 'package:provider/provider.dart';
class HeadlinesPage extends StatefulWidget {
  HeadlinesPage({Key key}) : super(key: key);
  @override _HeadlinesPageState createState() => _HeadlinesPageState();
}
class _HeadlinesPageState extends State<HeadlinesPage> { 
  NewsProvider newsProvider;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    newsProvider = Provider.of<NewsProvider>(context);
  }
  @override
  Widget build(BuildContext context) {
    return NewsList(list: newsProvider.articlesList);
  }
}
```

F, Creamos `NewsList` para mosttar la lista de noticias

```dart
import 'package:flutter/material.dart';
import 'package:news/src/model/Article.dart';
import 'package:news/src/theme/DarkTheme.dart';
class NewsList extends StatelessWidget {
  final List<Article> newsList;
  const NewsList({Key key, @required List<Article> list}): this.newsList=list, super(key: key) ;
  @override Widget build(BuildContext context) {
    return Scrollbar(
      //isAlwaysShown: true, es a fuerza el "controller" si lo asignamos a true
      //controller: ScrollController(),
      child: Container( padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: ListView.builder(
          itemCount: newsList.length,
          itemBuilder: (ctx, i) { return _New(newsList[i], i); }
), ), ); } }

class _New extends StatelessWidget {
  final Article article; final int index;
  const _New(Article article, int index, {Key key}) : this.article = article, this.index = index, super(key: key) ;
  @override
  Widget build(BuildContext context) {
    final source = Text( '#${index+1}. ${article.source.name}',  maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 10));

    final title = Text( '${article.title}',  overflow: TextOverflow.ellipsis,  maxLines: 3,
      style: TextStyle(color: myTheme.accentColor, fontSize: 16, fontWeight: FontWeight.bold) );

    final image = (article.urlToImage != null && article.urlToImage.isNotEmpty)
      ? FadeInImage( placeholder: AssetImage('assets/giphy.gif'), image: NetworkImage(artice.urlToImage), ) : Image( image: AssetImage('assets/no-image.png'), ) ;

    final imageStyled = ClipRRect( borderRadius: BorderRadius.only(topLeft: Radius.circular(30), bottomRight: Radius.circular(30)), child: image, );

    final description = Container( padding: EdgeInsets.only(left: 5, right: 40),
      child: Text((article.description != null && article.description.isNotEmpty) ? article.description : '', textAlign: TextAlign.justify, ), );    
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        source, title, imageStyled, description,
        SizedBox(height: 15), Divider(thickness: 2,endIndent: 10, indent: 10,), SizedBox(height: 15),
      ],
    );
  }
}
```

**Resultado** Podemos mostrar la lista de noticias que nos llega de la
API

![61_news_lista_noticias](./img/61_news_lista_noticias.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 289. Mantener Scroll al navegar entre paginas

<!-- 
no alterar scroll, no mover scroll, conservar, scroll, concervar scroll,
congelar scroll, conjelar escroll
-->

**Problema:** Si estamos en una pagina y nos movemos a otra, al regresar
a esta la pocicion del scroll en la que estabamos se perdie.
**Solucion:** Debemos de mantener algo asi como el stado del cliente,
por lo cual podremos hacerlo de la siguiente manera.

**Error:** Tenia un 'Scrollbar' en el componente 'NewsList' lo cual
causo error, pero se lo quite y ya no dio problemas.

```dart
// agregamos el AutomaticKeepAliveClientMixin
+ class _HeadlinesPageState extends State<HeadlinesPage> with AutomaticKeepAliveClientMixin {
    NewsProvider newsProvider;
    @override void didChangeDependencies() { super.didChangeDependencies(); newsProvider = Provider.of<NewsProvider>(context); }
    @override Widget build(BuildContext context) {
      // vista con scroll
      return (newsProvider.articlesList.length > 0) ? NewsList(list: newsProvider.articlesList)  : Center(child: CircularProgressIndicator());
    }
// al agregar "AutomaticKeepAliveClientMixin" nos obligara a implementar
// este metodo, lo cual indicara cuando queremos mantener el estado, en
// este caso siempre lo tendremos en true.
+   @override
+   bool get wantKeepAlive => true;
  }
```

**Resulado:** Al movernos entre paginas y volver, el scroll se mantiene
donde estaba, no como antes que se perdia este.

<!--

#########################################

-->

----

<!--

#########################################

-->

### 290. Menu superior lista de categorias

- Fuente
  - [Categorias](https://newsapi.org/docs/endpoints/top-headlines)

**Objetivo:** Vamos a crear un navigation bar superior con las categorias
pero estan no cabran, por lo que podremos hacer scroll horizontal.

> **Error**: `Vertical viewport was given unbounded height`
> **Descripcion**: Al querer usar la siguietne leist de la sig forma me dio
> este error.

```dart
@override Widget build(BuildContext context) { return SafeArea(
  child: Scaffold( body: Column( children: [
        ListView.builder(itemCount: newsProvider.categories.length, itemBuilder: (ctx, i) {
            return FlatButton.icon( icon: Icon(newsProvider.categories[i].icon), label: Text(newsProvider.categories[i].name), onPressed: ( ) => {},
  ); }, ) ], ), ), );
```

> **Causa:** NO SE, creo por que tiene un problema con que alto ocupara
> la lista, ya que como puede ser infinita en casos pueda dar problemas
> **Solucion** Envolvi la lista en un expanded y ya funciono

```dart
@override
  Widget build(BuildContext context) { return SafeArea( child: Scaffold( body: Column( children: [
+   Expanded(
      child: ListView.builder( itemCount: newsProvider.categories.length, itemBuilder: (ctx, i) {
          return FlatButton.icon( icon: Icon(newsProvider.categories[i].icon), label: Text(newsProvider.categories[i].name), onPressed: ( ) => {}, 
        ); }, ), ) ], ), ), ); }
```

A, Creamos la pagina con el menu

```dart
import 'package:flutter/material.dart'; import 'package:news/src/model/CategoryModel.dart'; import 'package:news/src/providers/NewsProvider.dart'; import 'package:provider/provider.dart';
class CategoriesPage extends StatefulWidget {
  CategoriesPage({Key key}) : super(key: key); @override CategoriesPageState createState() => CategoriesPageState();
}
class CategoriesPageState extends State<CategoriesPage> {
  NewsProvider newsProvider;
  @override void didChangeDependencies() { super.didChangeDependencies(); newsProvider = Provider.of<NewsProvider>(context); }
  @override Widget build(BuildContext context) {
    return SafeArea( child: Scaffold( body: Column( children: [
// le pasamos la lista de categorias
            TopNavBar(categories: newsProvider.categories),
            Expanded( child: Center( child: Text('Cuerpo'), ), )
], ), ), ); } }

class TopNavBar extends StatelessWidget {
  final List<CategoryModel> categories;
  const TopNavBar({Key key, @required this.categories}): super(key: key);
  @override Widget build(BuildContext context) {
    return Container( height: 60, color: Colors.black26, padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [ Expanded( child: ListView.builder( scrollDirection: Axis.horizontal, itemCount: categories.length,
              itemBuilder: (ctx, i) { final name = categories[i].name;
                return GestureDetector( onTap: () => {}, child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      children: [
                        Icon(categories[i].icon), Text('${name[0].toUpperCase()}${name.substring(1).toLowerCase()}'), ],
                    ),
), ); }, ), ) ], ), ); } }
```

**Resultado:** Podemos visualizar las categorias con un scroll horizontal
para poder ver todas

![62_news_topNavBar_categorias](./img/62_news_topNavBar_categorias.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

### 293. Cambiar el color de la categoría seleccionada

**Objetivo** Queremos que al dar click sobre una categoria, se nos muestre
como seleccionada para darle feddback al usuario.

A, En `lib\src\providers\NewsProvider.dart`

```dart
class NewsProvider with ChangeNotifier {
  final List<CategoryModel> categories = [ new CategoryModel(Icons.science, 'science'), ... ];

// agregamos la categoria seleccionada, la cual si no esta asignada
// retornara la primera en la lista
+ CategoryModel _selectedCategory;
  ...
  
+ CategoryModel get selectedCategory => this._selectedCategory ?? categories[0];

//cuando modifiquemos notificaremos el cambio
+ set selectedCategory(CategoryModel selected) {
+   this._selectedCategory = selected;
+   notifyListeners();
+ }
```

B, En tanto pintamos las categorias, agregamos un condicional donde si
es igual al seleccionado cambiamos su color

```dart
class TopNavBar extends StatelessWidget {
  final List<CategoryModel> categories;  
  const TopNavBar({Key key, @required  List<CategoryModel> categories}): this.categories = categories, super(key: key) ;
  @override Widget build(BuildContext context) {
// aqui tendremos el color seleccionado
+   final newsProvider = Provider.of<NewsProvider>(context);
    return Container( height: 60, color: Colors.black26, padding: EdgeInsets.only(top: 10),
      child: Column( children: [ Expanded( child: ListView.builder( itemBuilder: (ctx, i) {
                final name = categories[i].name;
// cambiamos el color si la categoria es la seleccionada
+               Color categoryColor = Colors.white;
+               if(newsProvider.selectedCategory.name == name ) {
+                 categoryColor = myTheme.accentColor;
+               }
```

**Resultado:** Cuando seleccionamos una categoria esta se resalta.

![63_news_seleccionar_categoria_1](./img/63_news_seleccionar_categoria_1.gif)

<!--

#########################################

-->

----

<!--

#########################################

-->

### Cargar categorias de API

**Objetivo:** Vamos a mostrar las diferentes categorias, lo cual usaremos
un Map de listas (una coleccion de colecciones) donde guardaremos el
contenido de cada categoria.

A, En `lib\src\providers\NewsProvider.dart` agregamos lo siguiente

```dart
class NewsProvider with ChangeNotifier {
  final _articlesList = new List<Article>(); final _newsDAO = new NewsDAO();
  final List<CategoryModel> categories = [ new CategoryModel(Icons.science, 'science'), ... ];

// nuestra coleccion de colecciones, en este caso un mapa de listas
+ final Map<String, List<Article>> _categoriesContent = {};

  NewsProvider() {
    getHeadlines();
// la primer categoria la cargamos al inicio
+   categories.forEach((cat) => {  _categoriesContent[cat.name] = null });
+   getHeadlinesByCategory(this.selectedCategory.name)
+     .then((list) => {
+       setCategoryContent(this.selectedCategory.name, list)
+     })
+   ;
  }

// obtenemos lo que es una categoria
+ Future<List<Article>> getHeadlinesByCategory(String category) async {
+   const headlinesPoint = 'top-headlines';
+   final NewsResponse resp = await _newsDAO.getHeadline(headlinesPoint, category: category);
+   return resp.articles;
+ }

// cuando obtengamos una categoria si esta es nula entonces disparamos
// la carga http de dicha categoria
+ List<Article> getCategoryContent(String category) { 
+   List<Article> categoryContent = this._categoriesContent[category];
+   if(categoryContent == null) {
+     getHeadlinesByCategory(category)
+       .then((list) => {
// cuando este lista la categoria la agregamos a nuesca coleccion de colecciones
+         setCategoryContent(category, list)
+       })
+     ;
+   }
+   return categoryContent;
+ }

// cuando una categoria sea asignada (significa que ya cargo) notificamos
// para que la UI se pinte.
+ void setCategoryContent(String category, List<Article> list) {
+   _categoriesContent[category] = list;
+   notifyListeners();
+ }
```

B, Mostramos

```dart
class CategoriesPageState extends State<CategoriesPage> {
  NewsProvider newsProvider;
  @override void didChangeDependencies() { super.didChangeDependencies(); newsProvider = Provider.of<NewsProvider>(context); }
// se repintara cuando llamemos al 'notifyListeners()' el cual lo tenemos
// al seleccionar una categoria y cuando ya ahiga  cargado las categorias
! @override Widget build(BuildContext context) {
// obtenemos la categoria la cual pueque que obtengamos un nulo, pero
// este sera temporal por que el `getCategoryContent` disparara el get
// cuando termine de cargar el http.
+   List<Article> categoryContent = newsProvider.getCategoryContent(newsProvider.selectedCategory.name);
    return SafeArea( child: Scaffold( body: Column(
          children: [ TopNavBar(categories: newsProvider.categories),
            Expanded(
// ya tenemos listo el widget para solo pasar la lista de noticias y 
// que este lo muestre, tenemos un if que si es nula la lista entonces
// muestre un loading.
+             child: NewsList(list: categoryContent),
) ], ), ), ); } }
```

**Resultado** Podemos ver el contenido de cada categoria, en su respectiva
pantalla.

![_12_news_seleccionar_categoria_y_mostrar_contenido_conjelar_scroll](./img/_12_news_seleccionar_categoria_y_mostrar_contenido_conjelar_scroll.gif)

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Paquete propio de Mapbox

### .................. SECCION SALTADA

## FIN DEL CURSO

Detengo el curso, es provable hecharme el avanzado, pero mientras asi

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Por el camino

### Logs

Cuando queremos ver los logs de una aplicacion (hecha con flutter) que
instalamos en el telefono, y queremos ver sus logs, talvez por que la
apk debug y la apk release se comportan de manera diferente y queremos
ver que pasa con la de release, podemos ver los logs con el siguiente
comando

Ver logs de apk release `flutter logs`

<!-- 

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

.

-->

----

----

----

<!-- 

.

#############################################################################################################
#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

-->

## Flutter 2

Veremos como pasar una aplicacion de flutter 1 a flutter 2, como igual
actualizar dart.

Actualizar flutter (tambien va a actualizar dart): `flutter upgrade`

NOTA: La actualizacion no causa problemas, lo que si puede que nos traiga
problemas es la implementacion del null-safely pero eso lo veremos adelante.

### 312. Reconstruir proyectos de IOS, Android y Web

En dado caso que nuestro proyecto haigamos creado con Flutter 1, no tendremos
la carpeta de web, lo podemos hacer con el siguiente comando (ya debemos
tener Flutter 2), tl cual a terminar hara la carpeta web.

<!-- Crear ejecutable, compilar apk, crear apk-->

Compilar proyecto: `flutter create .`

<!-- abrir aplicacion en el navegador, lanzar aplicacion en chrome -->

Iniciar proyecto en web: `flutter run -d chrome`

<!--

#########################################

-->

----

<!--

#########################################

-->

### 314. Flutter Fix

Cuando hacemos migraciones mayores por ejemplo de 1.X a 2.X, lo mas
probable es que aparesca codigo obsoleto o deprecado, lo cual tendremos
que actualizar por que tarde o temprano sera un codigo que ya no compile.

**Lo que se agrego:** FUe que nuestro IDE/Plugin de dart nos da el fix,
para actualizar el codigo solo componer en raton encima.

El comando de abajo checara lo deprecado del mismo lenguaje dart.

Ver codigo deprecado en dart: `dart fix`

### 315. Null-Safety

Este nos ayuda con los errores de null, dandonos advertencias de que
el codigo puede caer en un null, o etiquetas para obligarnos a no poner
un null.

Para activar null-safety debemos de mover nuestra version de dart a
la 2.12 o mayor en nuestro `pubspec.yaml`.

```r
environment:
  sdk: ">=2.12.0 <3.0.0"  # antes estaba en la 2.7.0
```

Ejemplo

```dart
// nos dira que es a es imposible que sea nulo por lo que podemos
// quitar el condicional
if(a == null) ; 

// Nos dira que estamos haceptando nulos, lo cual debemos obligatoriamente
// definir un valor de por defecto o indicar que es requerido
CreditsResponse({ this.id, this.cast }); // con problemas
CreditsResponse({ this.id = 0, required this.cast }); // solucionado
```

### 317. Dart migrate

Como los cambios del null migrate pueden ser muchos, tenemos esta
herramienta que nos ayudara a correguir todos estos errores de
compilacion del null-safety.

**ADVERTENCIA:** Debemos de crear un respaldo/commit ya que esto hara
varios cambios lo que puede que no nos quede de otra que dar un paso
atras.

Checar si nustras depencias cumplen con el
null-safety: `flutter pub outdated --mode=null-safety`

En mi caso me la primer columna es la version que yo tengo, y y las que
tienen una **✓** son las versiones que si soportan.

```r
direct dependencies:
Package Name     Current   Upgradable  Resolvable   Lates
cupertino_icons  ✗1.0.0   ✓1.0.3      ✓1.0.3      ✓1.0.3
flutter_swiper   ✗1.1.6   ✗1.1.6      ✗1.1.6      ✗1.1.6
http             ✗0.12.2  ✗0.12.2     ✓0.13.3     ✓0.13.3
```

levantar la herramienta para migrar y asi implementar el null safety
ejecutamos el sig. comando `dart migrate`

**IMPORTANTE:** Podemos ver que el `flutter_swiper` no tiene una version
que sea compatible con el null-safely, por lo que la herramienta
**no nos dejara hacer la migracion**, la unica forma de aplicarlo es
quitando o sustituyendo esta dependencia, por una que si soporte el
null-safely.

Actualizar dependencias: `flutter pub upgrade`  
Luego: `flutter pub upgrade --major-versions`
