# RxJs

## Basicos

### Actualizar lista en observable

```ts
export class PostsService {
  public popular$ = of(Array<PostDto>());

  public findPopulares(): void {
    const found = [
      new PostDto('Popular Primero'),
      new PostDto('Popular Segundo'),
    ];
    
    this.popular$.pipe(
      map(l =>  ({l, ...found}))
    );
  }
}
```

### Condicional en Pipe

