# 1. Codigo Limpio

- [1. Codigo Limpio](#1-codigo-limpio)
  - [1.1. Introduccion](#11-introduccion)
    - [1.1.1. Siempre habrá codigo](#111-siempre-habrá-codigo)
    - [1.1.2. Bad Code](#112-bad-code)
    - [1.1.3. The Total Cost of Owning a Mess and Attitude](#113-the-total-cost-of-owning-a-mess-and-attitude)
    - [1.1.4. What Is Clean Code?](#114-what-is-clean-code)
    - [1.1.5. Schools of Thought](#115-schools-of-thought)
    - [1.1.6. We Are Authors](#116-we-are-authors)
    - [1.1.7. The Boy Scout Rule](#117-the-boy-scout-rule)
    - [1.1.8. Prequel and Principles](#118-prequel-and-principles)
    - [1.1.9. Conclusion](#119-conclusion)
  - [1.2. Meaningful Names](#12-meaningful-names)
    - [1.2.1. Avoid Disinformation](#121-avoid-disinformation)
    - [1.2.2. Use Searchable Names](#122-use-searchable-names)
    - [1.2.3. Avoid Mental Mapping](#123-avoid-mental-mapping)
    - [1.2.4. Nombre de Clases](#124-nombre-de-clases)
    - [1.2.5. Nombre de metodos](#125-nombre-de-metodos)
    - [1.2.6. Don’t Be Cute](#126-dont-be-cute)
    - [1.2.7. Pick One Word per Concept](#127-pick-one-word-per-concept)
    - [1.2.8. No hagas juegos de palabras](#128-no-hagas-juegos-de-palabras)
    - [1.2.9. Use Solution Domain Names](#129-use-solution-domain-names)
    - [1.2.10. Use Problem Domain Names](#1210-use-problem-domain-names)
    - [1.2.11. Add Meaningful Context](#1211-add-meaningful-context)
    - [1.2.12. Don’t Add Gratuitous Context](#1212-dont-add-gratuitous-context)
    - [1.2.13. Final Words](#1213-final-words)
  - [Funciones](#funciones)
    - [Do One Thing](#do-one-thing)
    - [One Level of Abstraction per Function](#one-level-of-abstraction-per-function)
    - [Reading Code from Top to Bottom: The Stepdown Rule](#reading-code-from-top-to-bottom-the-stepdown-rule)
    - [Switch Statements](#switch-statements)
    - [Argumentos de Funciones](#argumentos-de-funciones)
    - [Evitar efectos secundarios](#evitar-efectos-secundarios)
    - [Argumento como salida](#argumento-como-salida)
    - [No juntes comando y consulta](#no-juntes-comando-y-consulta)
  - [4. Comentarios](#4-comentarios)
  - [5. Formateo](#5-formateo)
    - [Metafora del periodico](#metafora-del-periodico)
  - [6. Objetos y estructura de datos](#6-objetos-y-estructura-de-datos)
    - [Diferencia entre estructura de datos (DTO \[Data Trasfer Object\]) y objetos](#diferencia-entre-estructura-de-datos-dto-data-trasfer-object-y-objetos)
    - [La ley de Demeter](#la-ley-de-demeter)
    - [DTO (Data transfer object)](#dto-data-transfer-object)
  - [7. Manejo de errores](#7-manejo-de-errores)
    - [Mala idea regresar codigos de error](#mala-idea-regresar-codigos-de-error)
    - [Escribe primero los bloques try-catch-finally](#escribe-primero-los-bloques-try-catch-finally)
    - [Usa excepciones sin chequeo (Unchecked Exceptions)](#usa-excepciones-sin-chequeo-unchecked-exceptions)
    - [Define clases de excepciones en funcion de las necesidades de su llamante](#define-clases-de-excepciones-en-funcion-de-las-necesidades-de-su-llamante)
    - [Define un flujo normal](#define-un-flujo-normal)
    - [No regreses nulos](#no-regreses-nulos)
    - [No pases nulos](#no-pases-nulos)
  - [8. Limites](#8-limites)
    - [Librerias y frameworks](#librerias-y-frameworks)
    - [Ejemplo log4j](#ejemplo-log4j)
    - [Tests de aprendizaje son mejores que gratis](#tests-de-aprendizaje-son-mejores-que-gratis)
    - [Usando codigo que aun no existe](#usando-codigo-que-aun-no-existe)
    - [Limites limpios](#limites-limpios)
  - [9. Pruebas unitarias](#9-pruebas-unitarias)
    - [Manten los test unitarios limpios](#manten-los-test-unitarios-limpios)
    - [Los test unitarios permiten los ...ble](#los-test-unitarios-permiten-los-ble)
    - [Test limpios](#test-limpios)
    - [Lenguaje de dominio especifico para tests](#lenguaje-de-dominio-especifico-para-tests)
    - [Un doble estandart](#un-doble-estandart)
    - [Un assert por test](#un-assert-por-test)
    - [F.I.R.S.T](#first)
  - [10. Clases (Repaso)](#10-clases-repaso)
    - [Cohecion](#cohecion)
      - [Mantener cohecion crea mas clases](#mantener-cohecion-crea-mas-clases)
    - [Organizarse para el cambio](#organizarse-para-el-cambio)
    - [Aislarse del cambio](#aislarse-del-cambio)
    - [TODO](#todo)
  - [11. Sistemas (Repaso)](#11-sistemas-repaso)
    - [¿Como construlles una ciudad?](#como-construlles-una-ciudad)
    - [Construccion separada de un sistema de su uso](#construccion-separada-de-un-sistema-de-su-uso)
    - [Separacion del main, fabricadores e inyeccion de dependencias (TODO: Repasar estos)](#separacion-del-main-fabricadores-e-inyeccion-de-dependencias-todo-repasar-estos)
    - [Escalar](#escalar)
    - [Preocupaciones principales y transversales](#preocupaciones-principales-y-transversales)
      - [Java Proxies](#java-proxies)
      - [Analogia de AOP](#analogia-de-aop)
      - [Pure Java AOP Frameworks](#pure-java-aop-frameworks)
    - [Los sistemas necesitan lenguajes de dominio especifico (DSL)](#los-sistemas-necesitan-lenguajes-de-dominio-especifico-dsl)
  - [12. Emergencia](#12-emergencia)
    - [Regla 1, Corre todos los tests](#regla-1-corre-todos-los-tests)
    - [Regla 2, Refactoriza y elimina duplicacion](#regla-2-refactoriza-y-elimina-duplicacion)
    - [Regla 3, Expresividad](#regla-3-expresividad)
    - [Regla 4, Reduce cantidad de metodos y clases](#regla-4-reduce-cantidad-de-metodos-y-clases)
  - [10. Concurrencia](#10-concurrencia)
    - [Mitos e ideas falsas](#mitos-e-ideas-falsas)
    - [Desafios](#desafios)
  - [Refinamiento sucecivo (saltado)](#refinamiento-sucecivo-saltado)
    - [JUnit Internals](#junit-internals)

## 1.1. Introduccion

### 1.1.1. Siempre habrá codigo

Los programadores nunca van a desaparecer, es como decir que un dia
existirá una matemática que no necesite matemáticos, claro se crearan
herramientas donde se requiera menos para hacer mas, pero aun asi nunca
existirá una herramienta que a partir de una descripción vaga la convierta
en una solución especifica.

### 1.1.2. Bad Code

La premisa "escribir buen codigo" no es un tema que se deba de menospreciar,
compañías han caído por el mal codigo, ya que  cada dia les era mas difícil
agregar nuevas características y los errores fueron siendo mas frecuentes,
al punto de tener que tirar la aplicación.

Como programadores con experiencia, es común que nos encontremos impedidos
por el mal codigo, tanto asi que se le llama **wading** a esta situación.
Y esto también es culpa de los programadores, los cuales no teníamos ganas,
tiempo y/o claro, etc. y lo fuimos dejando diciendo mas tarde lo arreglamos
pero no sabíamos que **Más tarde es igual a nunca**.

### 1.1.3. The Total Cost of Owning a Mess and Attitude

El mal codigo disminuye la productividad del equipo, incluso lentamente
a zero, y aumenta la presión sobre el equipo.

Parecerá e incluso sera cierto que la culpa de los desastres en el codigo,
no sea completamente nuestra, pero algo innegable es que nosotros igual
tenemos la culpa de los desastres, por que el trabajo de los gerentes es
defender los horarios, y de los stake-holders defender al cliente, incluso
con pasión, lo cual **nos haga retroceder por que nos sentamos intimidados**,
pero eso esta mal, ya que nosotros sabemos mejor que ellos que desastre
se nos puede avecinar.

Por ejemplo, si un gerente le exige a un cirujano que acelere el lavado
de manos, por que eso toma mucho tiempo, como el cirujano sabe mejor que
puede pasar si no se hace bien esta actividad, es su **responsabilidad**
indicar que no es posible.

### 1.1.4. What Is Clean Code?

**Bjarne Stroustrup** (inventor de c++) lo define como, codigo elegante y
eficiente, fácil de hacerlo, difícil de no ver errores cometidos, minima
dependencia, buen manejo d errores y eficiente.

**Pragmatic Dave Thomas y Andy Hunt**, dicen lo mismo con otras palabras,
pero ademas usan una metáfora de una ventana rota, una ventana rota,
hará que parezca que no le importa a las personas, y hará que otras no
les importe, entonces se verán **tentados a hacer mas desastre**, y asi
poco a poco llegar a algo que no podamos controlar.  
El codigo limpio es concentrado, osea que hace bien una sola cosa, cada
función, clase, modulo, tiene esta filosofía, es decir que evitar que
un elemento haga multiples cosas.

**Grady Booch** (famoso escritor de POO), Dice que el codigo liempo es
como una frase bien escrita, por ejemplo, un libro que sus parrafos son
capases de ser fácilmente remplazados por imágenes, sonidos y personas.
Asi debería ser nuestro codigo, ser capaz de quitar, sustituir decisiones,
re-diseñar, y cambiar intenciones.

**Dave Thomas** (fundador de OTI y eclipse), Dice que el codigo debe
de poder ser legible y mejorable, aparte del autor de este, debe tener
pruebas unitarias y de aceptación, tener nombres claros, ofrece solo
una manera de hacer las cosas, en lugar de varias maneras de hacer una
cosa, tener dependencias mínimas, proveer apis mínimas y claras.

**Michael Feathers** (Autor de "Working Effectively with Legacy Code"),
dice que el codigo parece estar escrito por alguien que le importa, que
cada detalle tiene una razón de ser, y que parece que se preocupa por
alguien mas que tenga que leerlo o mantener el codigo.

**Ron Jeffries**, autor de "Extreme Programming Installed", el sigue las
reglas de Beck, como son

- Corre todos los tests
- No contiene duplicaciones
- Expresa todas las ideas de diseño
- Minimiza el numero de entidades como lo son clases, métodos, etc

La duplicación es como no tener una idea clara, por lo que hay que repensarlo,
cuando este claro, para luego expresarlo correctamente.
Expresividad incluye nombres significativos, mirar dos veces si un objeto
o método esta haciendo mas de una cosa, abstraer logica de colecciones.

### 1.1.5. Schools of Thought

Ahi que rechazar la idea del penamiento exclusivo, de que solo somos
estudiantes de un solo maestro u institucion, que nosotros tenemos la
verdad absoluta y los demas estan mal.
En el libro veremos un modo epecifico de enseñansa de unas tecnicas
espesificas, los cuales se han pensado y experimentado mucho, pero eso
no quiere decir que tengamos la verdad absoluta, por ejemplo, para pelear
tenemos karate, Jut Jit su, estilo libre, Muai Tai, etc. Y cada institucion
los jusgan con sus propios parametos.

### 1.1.6. We Are Authors

En documentaciones tecnicas nos piden el autor, y una cosa sobre los autores
es que tienen la responsabilida de comunicarse bien con los lectores,
asi que cada vez que escribas una linea piensa en eso.

**La porporcion del tiempo que escribimos vs leemos codigo** es 1:10,
por lo que es un factor importante a preocuparnos por su lectura, ya
que si leemos mas rapido podremos escribir mas rapido.

### 1.1.7. The Boy Scout Rule

Es un hecho comun que el codigo se deteriore con el tiempo, y para evitarlo
podemos aplicar en nuestra profesion una regla de los Boy Scout Rule.

> Deja el campamento mas limpio de como lo encontraste

Esto lleva a una mejora continua lo cual es una tecnica fundamental en
multiples campos.

### 1.1.8. Prequel and Principles

Este libro es una precuela a "Agile Software Development: Principles,
Patterns,and Practices (PPP)"

### 1.1.9. Conclusion

Los libros de arte no prometen hacerte un buen artista, lo
mismo pasa con este, no promete hacerte un buen programador, lo que hara
es enseñarte na serie de tecnicas, herramientas, practicas, y ejemplo
tras ejemplo, como mal/buen codigo y por que es mal/buen codigo, mal
codigo transformado a buen codigo, y los demas depende de ti, ya que la
practica hace al maestro.

## 1.2. Meaningful Names

Los programadores nombramos todo, variables, funciones, argumentos, clase,
jars, wars, ears, etc. asi que mas vale que lo hagamos bien, por ejemplo,
tenemos dos funciones que hacen lo mismo

```java
public List<int[]> getThem() {
  List<int[]> list1 = new ArrayList<int[]>();
  for (int[] x : theList)
    if (x[0] == 4)
      list1.add(x);
  return list1;
}
```

Que ¿es la theList, list1, la posicion cero que significa, el cuatro
que significa?, ¿que hace la funcion?

```java
public List<int[]> getFlaggedCells() { // Flagged=marcada
  List<int[]> flaggedCells = new ArrayList<int[]>();
  for(int[] cell : gameBoard)
    if(cell[STATUS_VALUE] == FLAGGED) // Mejor aun if(cell.isFlagged())
      flaggedCells.add(cell);
  return flaggedCells;
}
```

En este codigo podemos ver estas preguntas respondidas, tambien vemos
que podemos crear la funcion `cell.isFlagged`, la cual  oculta **numero majicos**
y expresa aun mekjor que esta pasando.

### 1.2.1. Avoid Disinformation

Esto es cuando se hacen reducciones, como en lugar de poner "disco duro"
le pongamos "dd", seguro nosotros tenemos relacionada esta contraccion,
pero seguro otros no tendran ni idea que significa "dd".

Otra es especificar el tipo de dato en colecciones, ya que podemos poner
un accountList, lo cual en un modo estricto (si no hay dos cuentas iguales)
no es una lista, o talvez si, por lo que es mas recomendable usar accounts
para referirse a simplemente un conjunto, o accountsLoggedToday el cual
sabemos que son las cuantas que se loguearon hoy, y de las cuales se pueden
repetir.

Otro ejemplo son la palabras que solo generan ruido, parecido a lo dicho
arriba, por ejemplo, ProductData, ProductInfo, ProductValues, Los cuales
no tienen una distincion importante, y peor aun podriamos tener los tres
nombre funcionando en nuestro codigo, y solo confundirnos.

Lo mismo pasa con las funciones, si no tenemos nombres significativos,
o usamos palabras de adorno podriamos tener algo como.

```js
getActiveAccount();
getActiveAccounts();
getActiveAccountInfo();
```

Como vamos a saber que funcion llamar?

### 1.2.2. Use Searchable Names

Buscar un nombre de una letra o numeros es dificil, en cambio buscar cosas
como CLASSES_PER_STUDENT es facil, por ejemplo.

```java
for (int j=0; j<34; j++) {
  s += (t[j]*4)/5;
}
```

```java
int realDaysPerIdealDay = 4;
const int WORK_DAYS_PER_WEEK = 5;
int sum = 0;
for (int j=0; j < NUMBER_OF_TASKS; j++) {
  int realTaskDays = taskEstimate[j] * realDaysPerIdealDay;
  int realTaskWeeks = (realdays / WORK_DAYS_PER_WEEK);
  sum += realTaskWeeks;
}
```

### 1.2.3. Avoid Mental Mapping

Los programadores suelen ser inteligentes y con tal de demostrar esta
habilidad, en la clase host usan una variable u para referirce a url, y
asi hacer del codigo un mapa mental, pero ¿porque hacer eso?, obiamente
demuestra mas inteligencia (sarcasmo).

### 1.2.4. Nombre de Clases

Estas deberian de ser nombres de sustantivos o frases sustantivas, como
Customer, Account y AddressParser, evitando nombres como Data, Info,
Manager, una clase no debe ser un verbo.

### 1.2.5. Nombre de metodos

Estos deberian de tener nombre de verbo o frases de verbo.

### 1.2.6. Don’t Be Cute

No usar chistes en el codigo, vale es divertido, pero si el nombre no
da a entender a cualquier persona que es lo que hace entonces no lo hagas.

### 1.2.7. Pick One Word per Concept

Usar sinonimos diferentes como fetch, retrieve, y get de forma indiscriminadamente
trae confuciones al lector, mientras que el escritor lo puso con la misma
intencion.

### 1.2.8. No hagas juegos de palabras

Usar la misma palabra para dos propositos diferentes, por ejemplo, si
tenemos un metodo add para agregar elemento, y otro add para agregar
sumar alguna propiedad, nos vamos a confundir cual es el proposito del
vervo add.
Seria mejor usar insert para agregar un elemento, y usar increase para
sumar alguna propiedad, de esta manera separamos los "sinonimos" en
diferentes intenciones.

### 1.2.9. Use Solution Domain Names

Nuestro codigo va a ser leido por otros programadores, asi que usa terminos
que ellos conoscan, por ejemplo, CustomerFactory, un programador va a
entender que esa clase sigue un determinado patron de diseño, pero no
va a saber si le damos una clase llamada JobQueue (cola de trabajos),
aunque el termino vaya mejor con el problema sera dificil de entenderlo
por los lectores.

En caso contrario si no hayamos un nombre de nuestra jerga, entonces
usar una del negocio, y ya el programador podra preguntar a un experto
del domino de que trata ese termino.

### 1.2.10. Use Problem Domain Names

Saber separar conceptos de problema y de solucion, es parte del trabajo
de un buen programador y diseñador.

### 1.2.11. Add Meaningful Context

Muchos de los conceptos no seran autoexplicativos, estos necesitaran de
un contexto, este contexto lo podremos poner en nombre de namespaces,
clases, funciones, y en dado caso que no podamos hacerlo, podemos ponerlo
junto con el nombre de variable.

Por ejemplo, si tenemos `state` por si solo no sabremos de que se trata,
pero si lo tenemos envuento en un namespace, clase o funcion, ya sabremos
de que se trata.

```r
userStreet {
  ...
  state
  ...
}
```

La forma de arriba es la mejor incluso el compilador sabra el contexto
de un nombre, pero en el extremo que no podamos, podremos usar un prefijo,
por ejemplo, `addresState`, `userFormState` lo cual no es lo mas recomendable
pero nos saca del apuro.

Si vemos la siguiente funcion veremos que tenemos un contexto y sus
conceptos, pero **¿hay suficiente contexto?**, si le leemos la funcion
detenidamente si lo tiene, pero tenemos los siguiente problemas.

- A un primer vistaso no tiene contexto
- La funcion es grande
- La funcion esta metida en una clase el cual no sabemos a cual
  contexto le pertenece, ¿`number` es de estadisticas o del mensaje?.
- Sus variables se usan en todo y peor aun, pudieramos vernos tentados
  a levantaruna de estas a nivel de clase, lo cual una funcion de calculo
  podria intervenir en una variable de mensaje.

```java
class GuessStatistics {

  private void printGuessStatistics(char candidate, int count) {
    String number;
    String verb;
    String pluralModifier;
    if (count == 0) {
      number = "no";
      verb = "are";
      pluralModifier = "s";
    } else if (count == 1) {
      number = "1";
      verb = "is";
      pluralModifier = "";
    } else {
      number = Integer.toString(count);
      verb = "are";
      pluralModifier = "s";
    }
    String guessMessage = String.format(
    "There %s %s %s%s", verb, number, candidate, pluralModifier
    );
    print(guessMessage);
  }
}

```

**¿Que paso?**, tenemos el contexto de estadisticas y el contexto de mensaje
de estadisticas, puede ser un detalle quisquilloso, pero si separamos
estos dos contextos, sacando el mensaje a su propia clase podriamos
tener algo como.

```java
public class GuessStatisticsMessage {
  private String number;
  private String verb;
  private String pluralModifier;

  public String make(char candidate, int count) {
    createPluralDependentMessageParts(count);
    return String.format(
      "There %s %s %s%s",
      verb, number, candidate, pluralModifier);
  }

  private void createPluralDependentMessageParts(int count) {
    if (count == 0) {
      thereAreNoLetters();
    } else if (count == 1) {
      thereIsOneLetter();
    } else {
      thereAreManyLetters(count);
    }
  }

  private void thereAreManyLetters(int count) {
    number = Integer.toString(count);
    verb = "are";
    pluralModifier = "s";
  }

  private void thereIsOneLetter() {
    number = "1";
    verb = "is";
    pluralModifier = "";
  }

  private void thereAreNoLetters() {
    number = "no";
    verb = "are";
    pluralModifier = "s";
  }

}
```

### 1.2.12. Don’t Add Gratuitous Context

Si tenemos una app llamada estacion de telefono rapida, es una mala idea
nombrar a todas, todas con el prefijo `ETR`, por que cuando pongas el
autocompletado, te saldra una listota con todas las clases de tu app,
lo cual no es una buena idea.
Por ejemplo, luego tienes un modulo de `Cuentas` y luego una clase
de `Contactos` y terminas con `ETRCuentasContactos`, el cual deberia de
ser solamente `Contactos` (hablando de clases, las variables se pueden extender),
se tienen 10 caracteres agregando un contexto que no necesitamos.
Asi que se consizo, por ejemplo, si tenemos MAC Addresess, port Addresess,
y Web Addresess, podriamos usar MAC, URI, y port dentro de su clase Connection.

### 1.2.13. Final Words

A veces nombrar es mas un problema de habilidad de describir que tecnico,
y tenemos el temor de que otro programador objete, pero si estamos 80%
seguros de que el cambio es para mejor (siguiendo las reglas vistas)
incluso nos lo agradeceran.
Y ya tenemos herramientas de refactorizado que nos ayudan, y si la
herramienta es buena deja que esta herramienta tome las deciciones por ti.

## Funciones

La programacion paso de rutinas y subtutinas con fortran a programas,
sub-programas y funciones, y el unico que sebrevivo el tiempo fueron
las funciones, las cuales son centrales hoy en dia, asi que debemos
ponerle atencion especial.

Las funciones no deben de tener muchas identaciones, para fors, ifs, etc,
(maximo unos 3 niveles) y tampoco muchas lineas, deberian estar alrededor
de 20 lineas, y por mucho de unos 150 de largo.

Por ejemplo, este codigo deberia ser reducido al de mas abajo.

```java
// #=aptjh38-1=#
public static String renderPageWithSetupsAndTeardowns(PageData pageData, boolean isSuite) throws Exception {
  boolean isTestPage = pageData.hasAttribute("Test");
  if (isTestPage) {
    WikiPage testPage = pageData.getWikiPage();
    StringBuffer newPageContent = new StringBuffer();
    includeSetupPages(testPage, newPageContent, isSuite);
    newPageContent.append(pageData.getContent());
    includeTeardownPages(testPage, newPageContent, isSuite);
    pageData.setContent(newPageContent.toString());
  }
  return pageData.getHtml();
}
```

```java
//#=aptjh38-2=#
public static String renderPageWithSetupsAndTeardowns(PageData pageData, boolean isSuite) throws Exception {
  if (isTestPage(pageData)) {
    includeSetupAndTeardownPages(pageData, isSuite);
  }
  return pageData.getHtml();
}
```

### Do One Thing

Hay que recordar que las funciones son una forma de abstraer un proceso,
es decir, igual que las jerarquias en las organizaciones, en teoria todo
lo controla el director, pero este no lo hace todo, este tiene personas
a quien le dan ordenes, y a su vez estar personas tienes personas a las
que les dan ordenes. O en otras palabras estamos descomponiendo un concepto
grande en otros conceptos mas pequeños.

Con eso en mente puede resultar complicado saber si una funcion hace una
sola cosa, pero lo podemos descubrirlo transformando nuestra funcion a
lenguaje natural, y si solo hace eso entonces si, solo hace una cosa
pero si esta haciendo mas cosas, entonces esta haciendo mas de una cosa,
lo cual deberiamos de soluionar delegando las tareas que no encajan en
la descripcion en lenguaje natural.

Por ejemplo, el codigo de arriba podremos describirlo como

> `renderPageWithSetupsAndTeardowns` checa si la pagina es de test, si
> lo es entonces incluimos unos desmontajes, en caso contrario se
> renderiza el html.

podemos ver que el codigo `#=aptjh38-1=#` no encaja por que estamos
obteniendo e incluyendo cosas, mientras que el `#=aptjh38-2=#` si lo
hace.

Otra forma de saber si esta haciendo mas de una cosa, es ver si podemos
extrar una funcion de esta, si podemos deberiamos de hacerlo.

Igual no hay exajerar, para saber si ya nos pasamos pensar si la reduccion
hace una en el nivel de abtraccion y contexto, por ejemplo, reducir
`#=aptjh38-2=#` quitando el if, a su propia funcion no hace una diferencia
o nivel de abstraccion.

### One Level of Abstraction per Function

/

### Reading Code from Top to Bottom: The Stepdown Rule

Queremos leer el codigo como si fuera de arriba para abajo, donde no se
nos presenten personajes que no se usaran, donde arriba el nivel de
abstraccion de arriba hacia abajo, como si fuera un parrafo 'TO'.

> Para incluir las configuraciones y desmontajes primero las agregamos,
> luego agregamos el contenido de test ...
>
> Para agregar las conf. y desm. debemos ir a ...
>
> Para incluir el contenido generamos ...

Al leer de esa manera no leemos cosas algo asi como en una linea temporal
ordenada.

### Switch Statements

Los `switch` por su naturaleza hacen mas de una cosa, y tarde o temprano
los necesitamos, lo recomendable es que estos queden en clases de bajo
nivel, y evitar duplicarlos con polimorfismo.

```java
public Money calcularPago(Empleado e) throws InvalidEmployeeType {
  switch (e.type) {
  case COM:
    return calcularPagoPorComision(e);
  case HOR:
    return calcularPagoPorHora(e);
  case SAL:
    return calcularPagoPorDia(e);
  default:
    throw new ErrorTipo(e.type);
  }
}
```

Este codigo viola tiene 4 cosas mal

1. Es un metodo largo
2. el principio de unica responsabilidad, ya que puede hacer cualquier
   de los 3 casos, y debemos tenemos mas de una razon para modificarlo
3. Viola el principio de abierto/cerrado, porque cada vez que agregemos
   un nuevo tipo tendremos que modifcar este.
4. Y lo peor es que internamente estas clases tendran una estructura
   similar, como es entregarPago(), acasoSeLePagaHoy(), etc.

La solucion a esto seria crear un Abstract Factory que navie pueda ver que cree instancias
adecuadas de Empleado y las funciones como entregarPago(), acasoSeLePagaHoy()
seran resueltas por polimorfismo a travez de la interface de empleado.

```java
public abstract class Employee {
  public abstract boolean isPayday();
  public abstract Money calculatePay();
  public abstract void deliverPay(Money pay);
}
//-- -- -- -- -- -- -- -- -
public interface EmployeeFactory {
  public Employee makeEmployee(EmployeeRecord r) throws InvalidEmployeeType;
}
//-- -- -- -- -- -- -- -- -
public class EmployeeFactoryImpl implements EmployeeFactory {
  public Employee makeEmployee(EmployeeRecord r) throws InvalidEmployeeType {
    switch (r.type) {
    case COMMISSIONED:
      return new CommissionedEmployee(r);
    case HOURLY:
      return new HourlyEmployee(r);
    case SALARIED:
      return new SalariedEmploye(r);
    default:
      throw new InvalidEmployeeType(r.type);
    }
  }
```

No todos los switchs son malos, si aparecen pocos y no tenemos repeticiones
podemos violar la regla de no tenerlos.

### Argumentos de Funciones

Los argumentos son dificiles de entender, asi que el numero recomendable
de argumentos es 0 o 1, a veces no podremos evitar 2, por ejemplo,
`Point(0,0)` pero este tipo de casos tienen un orden natural el cual estos
casos no hay problemas de usar 2 o mas, pero por ejemplo, `escribir(streamDeSalida, valor)`
tiene el problema de que requerimos de un momento para saber que hace,
lo ideal seria un `escritorDeSalida.pon(valor)` donde el stream es atributo de
escritor, lo cual deja las cosas claras y aisladas.

Cuando se trata mas de 3 o 4 valores, lo mas seguro es nos hace falta
crear un objeto, por ejemplo, `circulo(4,4,2,2,black)` el cual es dificil de
entender y facil de comenter un error, podriamos covertirlo a
`circulo(new Punto(4,4)); circulo.radio(2).anchoLinea(2).color(black);`
el cual es auto-explicativo.

Tambien los buenos nombres de funcion entran aqui, p. ej, la funcion
`assertEquals(1,2)` podri ser refactorizada a `assertExpectedEqualsActual(1, 2)`
el cual es auto-explicativo.

### Evitar efectos secundarios

```java
public class ValidadorUsuario {
  private Criptografo criptografo;
  
  public boolean revisaClave(String nombreUsuario, String clave) {
    Usuario usuario = UsuarioRepo.findByName(nombreUsuario);
    if (usuario != Usuario.NULL) {
      String codedPhrase = user.getPhraseEncodedByPassword();
      String phrase = cryptographer.decrypt(codedPhrase, password);
      if ("Valid Password".equals(phrase)) {
        Session.initialize();
        return true;
      }
    }
    return false;
  }
}
```

En el codigo de arriba, la funcion `revisaClave` contiene `Session.initialize();`
el cual es un efecto secundario el cual no es indicado por su nombre el
cual podria ser `revisaClaveEInicializaSession` lo cual evidentemente
rompe con el principio de unica responsabilidad.
Tambien tenemos un acoplamiento temporal, por que si llamamos por segunda
ver `Session.initialize();` podriamos tener una sobre-escritura.

La solucion aqui seria quitar el uso de session en esta funcion.

### Argumento como salida

En resumen este tipo de argumentos deben ser evitados, se usaban antes
por que no existia la POO

```java
appendFooter(s);
print(s)
```

Para entender el codigo de arriba, necesitamos un revizar la firma, y sabiendo
que si no retorna nada entonces a `s` le agrega algo, y si retorna algo
podria significar que `s` es el footer y lo agrega como tal.
En cualquier caso necesitamos dejar de pensar en nuestro problema y pensar
en como funciona la implementacion, lo cual debemos de evitar lo mas
posible.

Una mejor aproximacion seria la siguiente, lo cual deja clara la intencion
de, a que se le agregara dicho pie,

```java
reporte.agregaPie(pie);
```

Otra que no entiendo seria, el cual evitamos el uso de argumentos

```java
reporte.agregaPie();
```

### No juntes comando y consulta

```java
//Codigo sucio
//boolean asigna(prop, value)
if (asigna("username", "mario"))...
```

La intencion no esta clara, estamos asignando y luego haciendo algo, o
estamos consultadon y a partir de esto hacemos algo, y la funcion la podriamos
renombrar a `asignaYRetornaExito` lo cual viola la unica responsabilidad.

Lo mas prudente seria hacer lo siguiente.

```java
if (doesExitAttribute("nombre")) {
  setAttribute("nombre", "unclebob");
  ...
}
```

Aqui dejamos claro que lo que queremos es asignar en caso de no exista,
y lo logramos por medio de no.

Lo mismo aplica para retornar codigos de error, por ejemplo

```java
if (eliminaPagina(page) == ERR_AUTH)
```

lo cual podriamos decir que la funcion se podria llamar `eliminaYRetornaResultadoDeEliminacion`
violando la unica responsavilidad, y tambien obligando al que llama la funcion
por checar el error.

## 4. Comentarios

En resumen, la gran mayoria de los comentarios son una mala practica, porque,
deben ser mantenidos y en su mayoria no se hace este mantenimiento, y si un comentario
ya no es compatible con el codigo por esto, este puede llevar a confuciones, y
la fuente mas confiable y precisa de que cosa hace el codigo, es el codigo.

Por lo tanto si un codigo necesita de comentarios, significa que el codigo no es
lo suficientemente expresivo, y deberiamos de hacer un esfuerzo para que el codigo
que hacemos, sea lo suficientemente expresivo para no necesitar comentarios.

Hay comentarios que por rasones legales debemos de ponerlos, pero estos deberian
ser pequeños y refererir a un documento externo, y algunos ides los colapsan en
automatico por una buena razon.

Los comentarios que son **buenos**, son los que explican cosas que son por
naturaleza propia complejos, por ejemplo, las expreciones regulares

```java
// format matched kk:mm:ss EEE, MMM dd, yyyy
Pattern timeMatcher = Pattern.compile("\\d*:\\d*:\\d* \\w*, \\w* \\d*, \\d*");
```

**Explicar razones de ser,** hay veces que se implementa codigo que parece no tener
lugar, pero que se puso por una razon externa, por ejemplo.

```java
//The wait is done because in slow connections, sometimes the page is not ready
settimeout(()=>{ productList.updateUI(); }, 100)
```

**Marcas de posicion:** Estos si son dispersos sulen ser utiles, pero si son
seguidos solo son ruido

```java
// Actions //////////////////////////////////
```

**Marcas de cierre de bloques**, estos comunmente significa que tenemos codigo
anidado y largo, por lo que es señal de que debemos de hacer mas limpia la funcion.

```java
class A {

  public doSomething() {
    try {
      if(...) {
        
        ...
        
      } else {
      
        ...
        
      }//end else
      
      
    } catch(e Exception) {
    
      ...
      
    } //end

  }//end doSomething()

}//end Clas
```

**Codigo comentado**: No hagas esto, unos lo hacen para recordar codigo, por si
truena entonces tener accesible el codigo de como estuvo funcionando antes, pero
para eso ya tenemos git, y otros al verlo no van a querer eliminarlo pensando en
que es importante.

Por lo que el codigo comentado son como escombros, y nadie quiere escombros
estorvando, asi que usa git y no comentes codigo eliminalo

```java
class A {

  public doSomethingA() {
    ...
  }
  
/*
  public doSomethingB() {
    ...
  }
*/

}
```

**Comentarios explicatorios**, un buen uso de los comentarios y de herramientas
de documentacion, son estos comentarios, donde le ahorran al programador de
averiguar o le ayudan a entender un algoritmo complejo, por que aveces uno solo
quiere saber que hace, no como lo hace.

Ejemplos comunes son funciones de naturalesa matematica, como lo son numeros primos,
calcular trayectorias, formulas de temperatura, probabilidad, etc., los cuales por mucha
factorizacion que hagamos no ayuda mucho a la exprecividad, ya que el propio
problema a implementar es complejo.

<!--

--

-->

## 5. Formateo

El archivo promedio de lineas por archivo en proyectos como, junit, tomcat,
estan entre un minimo de 10 y maximo de 500 lineas, y la media entre
40 y 65.
Lo que significa que podemos hacer software de valor que este en estos rangos,
que no parecen seguir una regla de oro.

### Metafora del periodico

Un buen articulo de periodico, nos indica con un titulo de qe trata el
ontenido, y el primer parrafo nos da un resumen de este, y conforme vas
bajando encuentras mas y mas detalles. La mayoria de sus articulos son
cortos, y hay muy pocos son tan largos como para tomar una pagina entera.

<!--

--

-->

## 6. Objetos y estructura de datos

Cuidado con agregar getter/setters ciegamente como lo hacen muchos, la
razon de hacer una propiedad privada es para ocultar detalles de implmentacion,
y quel que use nuestra clase solo de ordenes y no tenga que pensar una
implementaion, incluso debemos de imposibilitar que soluciones que estan
dentro de nuestra clase, quien la use pueda que replicar soluciones por
fuera de la clase, cosa que pasa con los getters y setters.

### Diferencia entre estructura de datos (DTO [Data Trasfer Object]) y objetos

Los objetos no exponen datos, los ocultan por medio de abstracciones para
que estos puedan ser manejados,

Los DTOs exponen sus datos, y solo tienen funciones de poco valor.

### La ley de Demeter

Esta ley dice que un modulo no debe de no debe de tener conocimiento de
las cosas internas de los objetos que manipula, por ejemplo.

```java
//Problema
public void soSomething(Context ctxt) {
  String outputDir = ctxt.getOptions().getScratchDir().getAbsolutePath();
  ...
}
```

En este ejemplo, estamos violando esta ley, ya que llamar esa cadena de
metodos para obtener esa informacion, quiere decir que este objeto
conoce las cosas internas del objeto Context.

¿Pero como lo soculionamos?, seria preguntarnos que queremos hacer con
la informacion que necesitamos, en este caso, resulta que se quiere la
ruta absoluta para escribir en un archivo.

```java
//averiguar
String outFile = outputDir + "/" + className.replace('.', '/') + ".class";
FileOutputStream fout = new FileOutputStream(outFile);
BufferedOutputStream bos = new BufferedOutputStream(fout);
```

Por lo tanto, dejamos que este proceso de obtener un stream de salida lo
haga el context.

```java
//solucion
BufferedOutputStream bos = ctxt.createScratchFileStream(classFileName);
```

De esta manera tenemos los intenernos de context escondidos, y solo
obtenemos lo necesario para que la dependencia de nuestra clase con
context sea resuelta sin tener que saber los interiores de context.

### DTO (Data transfer object)

```java
public class Address {
  private String street;
  private String streetExtra;
  private String city;
  private String state;
  private String zip;
  public Address(String street, String streetExtra, String city, String state, String zip) {
    this.street = street;
    this.streetExtra = streetExtra;
    this.city = city;
    this.state = state;
    this.zip = zip;
  }
  public String get...() {
   return this. ...();
  }
```

Son objetos muy utiles, especialmente cuando lideamos con entradas ya
sea desde el usuario o desde una DB., pero en el caso de arriba no tiene
mucho caso que sus variables sean privadas, por que al final son publicas
por medio de sus getters

<!--

--

-->

## 7. Manejo de errores

A veces los dispositivos fallan o las entrada con anormales, y es nuestra
responsabilidad indicar que alguno de estos escenarios ocurrio, y actuar
de forma correcta ante estos escenarios anormales.

Hay bases de codigo que su lectura se ve empobrecida, por que estan
lideando con el manejo de errores.

### Mala idea regresar codigos de error

Abajo podemos ver como despues de hacer algo, con su resultado vemos
si salio mal o bien, y actuamos segun lo ocurrido, pero al hacer esto
convinamos lo que es el manejo de errores con la logica, lo cual nos
cuesta un momento mas leer de que se trata esto.

```java
public class DeviceController {
...
  public void sendShutDown() {
    DeviceHandle handle = getHandle(DEV1);
    if (handle != DeviceHandle.INVALID) {
      retrieveDeviceRecord(handle);

      if (record.getStatus() != DEVICE_SUSPENDED) {
        pauseDevice(handle);
        clearDeviceWorkQueue(handle);
        closeDevice(handle);
      
      } else {
        logger.log("Device suspended. Unable to shut down");
      }
    } else {
      logger.log("Invalid handle for: " + DEV1.toString());
    }
  }
.
```

En cambio, abajo podemos ver funciones que hacen una sola cosa, como tambien
la intenciones de logica y de manejo de error separadas, y por esto es
mas facil de leer, y aun asi tenemos la misma y mas robustes. Aqui tener
cuidado de no exponer logica de mas, por ejemplo, `tryToShutDown` eso
es algo interno, por lo que debe ser privado.

```java
public class DeviceController {
  
  ...
  
  public void sendShutDown() {
    try {
      tryToShutDown();
    } catch (DeviceShutDownError e) {
      logger.log(e);
    }
  }

  private void tryToShutDown() throws DeviceShutDownError {
    DeviceHandle handle = getHandle(DEV1);
    DeviceRecord record = retrieveDeviceRecord(handle);
    pauseDevice(handle);
    clearDeviceWorkQueue(handle);
    closeDevice(handle);
  }
  
  private DeviceHandle getHandle(DeviceID id) {
    if...
      throw new DeviceShutDownError("Invalid handle for: " + id.toString());
    ...
  }
  ...
}
```

### Escribe primero los bloques try-catch-finally

El bloque `try` debe ser como una transaccion, es la razon por la que
tiene su propio scope, el cual debe ocurrir todo el bloque, y si falla
una parte de este bloque se cancela/destrulle todo el bloque, y el `catch`
es el encargado de dejar la aplicacion en un estado estable. Por esto
al esccribir codigo que puede lanzar una excepcion, debemos primero
implementar su manejo de errores.

Por ejemplo, si requerimos de la lectura de un archivo, primero hacemos
el test unitario de que debe lanzar una excepcion indicando que el archivo
no exite o algo mal tiene este.

```java
@Test(expected = StorageException.class)
public void retrieveSectionShouldThrowOnInvalidFileName() {
  sectionStore.retrieveSection("invalid - file");
}
```

Implementamos haciendo que falle nuestro test (regla de tdd)

```java
public List<RecordedGrip> retrieveSection(String sectionName) {
  // dummy return until we have a real implementation
  return new ArrayList<RecordedGrip>();
}
```

ahora implementamos para que pase nuestro test.

```java
public List<RecordedGrip> retrieveSection(String sectionName) {
  String streamFile = this.getFile(sectionName);
  return new ArrayList<RecordedGrip>();
}

private FileInputStream getFile(String sectionName) {
  try {
    FileInputStream stream = new FileInputStream(sectionName);
    return stream;
  //} catch (Exception e) { // es mejor reducir la el filtro.
  } catch (FileNotFoundException e) {
    throw new StorageException("retrieval error", e);
  }
}
```

De esta forma no se nos olvidara checar las excepciones

### Usa excepciones sin chequeo (Unchecked Exceptions)

```java
private FileInputStream getFile(String sectionName) throw FileNotFoundException;
```

Existia una discucion de los veneficios y desventajas de usarlas, pero
se termino por decidir en no usarlas, por que violan el principio de
openn/close, ya que al crear una excepcion checada obliga a implementaciones
de mas alto nivel a ser cambiadas y re-desplegadas.

### Define clases de excepciones en funcion de las necesidades de su llamante

Hay muchas formas de clasificar una excepcion, ya sea su fuente (ej. de que modulo),
funcion (ej. matematica, escritura), ambito (ej. red, disco), etc. abajo
vemos un ejemplo de una mala clasificacion.

```java
ACMEPort port = new ACMEPort(12);
try {
  port.open();
} catch (DeviceResponseException e) {
  reportPortError(e);
  logger.log("Device response exception", e);
} catch (ATM1212UnlockedException e) {
  reportPortError(e);
  logger.log("Unlock exception", e);
} catch (GMXError e) {
  reportPortError(e);
  logger.log("Device response exception");
} finally {
  …
}
```

Aqui tenemos mucha duplicacion y dependemos de las deciciones de diseño
que toma ACME, por lo mejor para estos casos es hacer un wrapper, por que
aqui estamos viendo que ponerse de acurerdo con alguien complicado, asi
que creamos un intermediario que nos facilite la comunicacion con este.

```java
LocalPort port = new LocalPort(12);
try {
  port.open();
} catch (PortDeviceFailure e) {
  reportError(e);
  logger.log(e.getMessage(), e);
} finally {
  …
}

//------------------
public class LocalPort {
  private ACMEPort innerPort;
  
  public LocalPort(int portNumber) {
    innerPort = new ACMEPort(portNumber);
  }

  public void open() {
    try {
      innerPort.open();
    } catch (DeviceResponseException e) {
      throw new PortDeviceFailure(e);
    } catch (ATM1212UnlockedException e) {
      throw new PortDeviceFailure(e);
    } catch (GMXError e) {
      throw new PortDeviceFailure(e);
    }
  }
  …
}
```

En este caso el caller solo le interesa si se abrio o no el puerto, no
nos interesa saber exactamente como fallo, solo la info necesaria para
escribirla en los logs.

En comun es una buena idea crear wrappers para librerias de terceros, ya
que nos creamos de una desacoplacion, la cual nos protege de si algun
dia queremos cambiar de libreria solo cambiariamos el wrapper.

### Define un flujo normal

Cuando implementamos en capas de alto nivel, no deberiamos lidiar con excepciones,
ya que contaminan la logica de alto nivel, por ejemplo, queremos calcular
el total de gastos de meriendas de un empleado, pero si no se encuentra
ninguna merieda, entonces obtenemos el gasto de otra manera. Abajo podemos
ver el ruido de lidear con el caso especifico lo cual causa que estemos
haciendo detalles que no correspnden a este nivel de abstraccion.

```java
try {
  MealExpenses expenses = expenseReportDAO.getMeals(employee.getID());
  m_total += expenses.getTotal();
} catch(MealExpensesNotFound e) {
  m_total += getAverageExpenses();
}
```

Lo ideal seria esconder esto, para que quede de esta manera.

```java
MealExpenses expenses = expenseReportDAO.getMeals(employee.getID());
m_total += expenses.getTotal();
```

Para lograr esto, podemos retornar objetos que implmenten una misma interface,
y de esta forma si ocurre un caso u otro, nuestra clase de alto nivel
no tendra que lidear con el caso especifico.

```java
public class AverageMealExpenses implements MealExpenses {
  public int getTotal() {
  // return the per diem default
  }
}

public class EmployeeMealExpenses implements MealExpenses {
  public int getTotal() { }
}
```

### No regreses nulos

Regresar un nulo es ponerte trabajo a ti mismo o a quien use tu implementacion,
de revisar si lo regresado no es un nulo y asi evitarse un nullPointExcception,
por ejemplo.

```java
//si
public List<Employee> getEmployees() {
  if( .. there are no employees .. )
    return null
  }
  ...
}

//entonces==========================
List<Employee> employees = getEmployees();
if (employees != null) {
  for(Employee e : employees) {
    totalPay += e.getPay();
  }
}
```

Por lo tatnto lo correcto a hacer seria.

```java
// Si...
public List<Employee> getEmployees() {
  if( .. there are no employees .. )
    return Collections.emptyList();
  }
  ...
}

//entonces==========================
List<Employee> employees = getEmployees();
for(Employee e : employees) {
  totalPay += e.getPay();
}
```

Por lo que vemos este ultimo codigo es mas limpio.

### No pases nulos

Si regresar nulo es malo, pasar un nulo es peor, y no hay una manera
limpia de hacerlo, por ejemplo.

```java
public double xProjection(Point p1, Point p2) {
  if (p1 == null || p2 == null) {
    throw InvalidArgumentException(...
  }
  p1.XXXXX
}

public double xProjection(Point p1, Point p2) {
  assert p1 != null : "p1 should not be null";
  assert p2 != null : "p2 should not be null";
  p1.XXXXXX
}
```

las dos formas es codigo que ensucia nuestra lectura, entonces lo mas
sensato es que cuando veamos o querramos pasar un nulo. Sea conciderado
un error y mover el codigo de tal manera que esto no ocurra.

```java
//esto es un error
xProjection(null, new Point(12, 13));
```

<!--
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-->

## 8. Limites

Es comun tener dependencias de codigo que no es nuestro, o tenemos modulos
integrados que solo nos apoyan, por lo que debemos tener maneras limpias
de integrarlos sin que traingan su suciedad a nuestro modulo.

### Librerias y frameworks

Siempre hay una linea de lucha entre los que lo usan y quienes crean estos,
ya que los creadores quieren avarcar la mayoria de casos posibles, mientras
quienes usan solo quieren cubrir su caso especifico.

Por ejemplo, `Map` abajo de java5 no existian los genericos, cuando salio
java7 por buenas razones todos querian/necesitaban usar genericos, y hay
vaces de codigo que son enormes y por temas de tiempo y/o cantidad de cambios,
les es imposible usar genericos, todo por que no hicieron un wrapper
como el sig.

```java
public class Sensors {
  private Map sensors = new HashMap();

  //singleton...
  private Sensors() { 
    ...
    this.sensors.add(new Sensor("light"));
    this.sensors.add(new Sensor("smoke"));
    this.sensors.add(new Sensor("light"));
  }
  
  public Sensor getById(String id) {
    return (Sensor) sensors.get(id);
  }

  //snip
}
```

Esto a pesar de ser parecer tedioso, es una buena idea por que limitamos
el Map a solo las funciones que queremos de el, por ejemplo, solo queremos
que sea de escritura, y de esta manera hacemos que `Map<Sensores>` sea
mal usada y cualquier cambio sobre la libreria `Map` solo tendriamos que
hecharle un vistaso a nuestro wrapper.

Ten cuidado con no compartir mucho estos wrappers, ya creas dependencias,
por lo tanto mantenlos solo dentro de su modulo, asi que evita pasarlos como
argumento o regresarlos como wrapper, en cambio pasalos y regresalos como mapa.

Aprender una libreria externa es dificil, integrarla es dificil y hacerlo
al mismo tiempo es doblemente dificil, ya que si no sabemos bien como
funciona una, podremos tener un mal momento si un fallo es culpa de
nuestro codigo o del codigo de la libreria.

Una regla es no testear librerias externas, ese es trabajo de quien hace
la libraria, pero lo que si debemos de hacer, es testear el trabajo en
conjunto de nuestro codigo y la libreria, para ver si se comporta como
nosotros esperamos.

### Ejemplo log4j

si queremos incluir esta libreria, debemos aprenderla a usar asi que primero,
la usamos a travez de tests unitarios lo cual necesitaremos usar una
clase wrapper, y esto parece trivial, pero resulta que log4j es mas
complicada de usar de lo esperado.
Por ejemplo para usarla necesitamos quitar los Appenders por default y
poner los nuestros, tambien definir el stream-out, por lo que terminamos
con el siguiente set de tests que ayudan a confirmar que funciona como
nosotros creemos y a confirmar que no va a fallar.

```java
//Listing 8-1
public class LogTest {
  private Logger logger;
  
  @Before
  public void initialize() {
    logger = Logger.getLogger("logger");
    logger.removeAllAppenders();
    Logger.getRootLogger().removeAllAppenders();
  }

  @Test
  public void basicLogger() {
    BasicConfigurator.configure();
    logger.info("basicLogger");
  }

  @Test
  public void addAppenderWithStream() {
    logger.addAppender(new ConsoleAppender( new PatternLayout("%p %t %m%n"), ConsoleAppender.SYSTEM_OUT));
    logger.info("addAppenderWithStream");
  }

  @Test
  public void addAppenderWithoutStream() {
    logger.addAppender(new ConsoleAppender(
      new PatternLayout("%p %t %m%n")));
    logger.info("addAppenderWithoutStream");
  }
  
}
```

### Tests de aprendizaje son mejores que gratis

El codigo `Listing 8-1` es aprender haciendo tests, lo cual es un ganar
ganar, porque a parte de que tenemos ejemplo concretos de como usar la
libreria, tenemos test unitarios que garantizan que alguna vez usar la
libreria de esa manera era la forma correcta.

Las librerias externas constantemente cambian, ya sea para arreglar bugs,
extender capasidades, o tapar fallas de seguridad (un caso grave en log4j en 2020),
tendremos que actualizar nuestra libreria.
Y estos test garantizaran que seguen funcionando como experamos, o en caso
contrario nos daremos cuenta de cual es el caso espezifico de por que ya
no funciona.

Asi que si crea test unitarios que usen solo la libreria, no con el fin
de testear la libreria, si no el de testear que funciona como se espera,
y de aprender a usarla.

### Usando codigo que aun no existe

Seguido nos cruzamos con que ya debemos de incluir la integracion de
otro modulo pero este modulo no existe, y en lugar de decidir parar hasta
que dicho modulo este completado, pordemos definir un wrapper que implemente
la interface que nosotrosdecieriamos.

Por ejemplo, una vez teniamos que usa un transmisor de radio pero al no
estar listo, el equipo creo un objeto `transmisor.transmite(outputStream, frequency)`
la cual no hacia nada, solo simulaba el comportamiento que nosotros deseabamos,
Para crearlo usamos el patron Adapter (GoF).

### Limites limpios

Es mejor depender de algo que controlas (Wrappers/Adapters) que de algo
que no controlas (usar la libreria directamente). Ya que pude que tarde o
temprano estas cambien, y si no seguimos este consejo, tendremos un
mal rato re-adaptando, en cambio seguirlas, esta re-adaptacion no sera
mas que cambiar una clase o modulo, y todo nuestro codigo interno seguira
funcionando igual.

<!--
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-->

## 9. Pruebas unitarias

Antes solo como asegurarse se escribian drivers que permitian a otro
programa usar nuestro programa, y los test eran de integracion, ya que
estos dependican mucho de sistema operativo y librerias, de haber sabido
lo que se hoy, ubiera probado cada rincon de mis codigos con pruebas
unitarias.

A mediados de los 90 nadie hablaba de los test unitarios, ahora el movimiçento
del agilismo y TDD a resonado y en medio de la locura/priza por adoptarlo,
se han perdido varios detalles de que significa escribir tests unitarios efectivos.

Hay 3 reglas del TDD

- No escribas codigo productivo, hasta que tengas un test unitario que falle.
- No escribas mas del test unitarios, que ya es suficiente para fallar.
- No escribiras mas codigo productivo, hasta que el test unitario pase

Este es un ciclo de 30seg, el cual garantiza que se escriba codigo que
esta siendo testeado por test unitarios. Y a lo largo del tiempo uno tendra
tantos test, que llegara a ser una base de codigo igual o mas grande que
el productivo, el cual funcionara incluso a generar datos para directivos.

### Manten los test unitarios limpios

Hay equipos que deciden que los test unitarios es codigo secundario, por
lo que se dan el permiso de que este solo cubra el codigo si importar
la limpieza de estos.
Pero tener test sucios es igual o pero que no tenerlos, ya que con cada
cambio que se haga al codigo productivo, es mas probable que sea mas
dificil que los test fallen y sean ms dificiles hacer que pasen, hasta
llegado el momento de descartar por completo los test, pero sin test
unitarios regresamos a viejos problemas de no usar test unitarios.

### Los test unitarios permiten los ...ble

Hablo de flexible, reutilizable, mantenible, ya que con test tienes la
seguridad de hacer refactorizaciones sabiendo que no rompes otras funcionalidades,
incluso un codigo con la mejor arquitectura y diseño pero SIN tests unitarios,
se ve pequeño al lado de una arquitectura y diseño pobre pero CON test unitarios.

### Test limpios

¿Que hace a un test unitario estar limpio?, ¡¡LEGIBILIDAD!!, ¿que hace
un test legible? lo mismo que en produccion claridad, simplisidad, dencidad
de exprecion. Por ejemplo.

```java
// Listing 9-1

// test sucio
public void testGetPageHieratchyAsXml() throws Exception {
  // crea pagina
  crawler.addPage(root, PathParser.parse("PageOne"));
  crawler.addPage(root, PathParser.parse("PageOne.ChildOne"));
  crawler.addPage(root, PathParser.parse("PageTwo"));
  request.setResource("root");
  request.addInput("type", "pages");

  // realiza peticion
  Responder responder = new SerializedPageResponder();
  SimpleResponse response = (SimpleResponse) responder.makeResponse(new FitNesseContext(root), request);

  //confirma formato
  String xml = response.getContent();
  assertEquals("text/xml", response.getContentType());
  //confirma contenido
  assertSubString("<name>PageOne</name>", xml);
  assertSubString("<name>PageTwo</name>", xml);
  assertSubString("<name>ChildOne</name>", xml);
}

// test limpio
public void testGetPageHierarchyAsXml() throws Exception {
  //crea pagina
  makePages("PageOne", "PageOne.ChildOne", "PageTwo");
  
  // realiza peticion
  submitRequest("root", "type:pages");

  //confirma formato
  assertResponseIsXML();
  //confirma contenido
  assertResponseContains("<name>PageOne</name>", "<name>PageTwo</name>", "<name>ChildOne</name>");
}
```

Ambos hacen lo mismo, pero el primero tiene muchos detalles que el lector
no necesita saber, mientras que el segundo con un simple metodos en una linea
corta puede leer lo que se trata de explicar en 3 lineas largas en el segundo.

### Lenguaje de dominio especifico para tests

Como vemos en `Listing 9-1` el test limpio tiene metodos que son especificos
para los test, y que no son para ser utilizados en produccion, y estos
no nacen por adelantado, estos se van formando con refactorizaciones, de
tal manera a llegar a test que sean leibles.

### Un doble estandart

Si vemos el siguiente test, vemos detalles que no entendemos, como que
es la funcion `tic()` y todos los metodos de `...state()` o de `...TempAlarm()`

```java
@Test
public void turnOnLoTempAlarmAtThreashold() throws Exception {
  hw.setTemp(WAY_TOO_COLD);
  controller.tic();
  assertTrue(hw.heaterState());
  assertTrue(hw.blowerState());
  assertFalse(hw.coolerState());
  assertFalse(hw.hiTempAlarm());
  assertTrue(hw.loTempAlarm());
}
```

Por lo que se re-factorizo de la siguiente manera, donde mayuscula es
encendido y minuscula apagado, el cual viola el principio de mapa mental,
pero en este caso parece correcto, solo nos faltaria definir una
documentacion para definir este mapa mental, pero de ahi el test de abajo
es mucho mas facil de leer.

```java
//Listing 9-5
@Test
public void turnOnLoTempAlarmAtThreshold() throws Exception {
  wayTooCold();
  assertEquals("HBchL", hw.getState());
}

@Test
public void turnOnCoolerAndBlowerIfTooHot() throws Exception {
  tooHot();
  assertEquals("hBChl", hw.getState());
}

public String getState() {
  String state = "";
  state += heater ? "H" : "h";
  state += blower ? "B" : "b";
  state += cooler ? "C" : "c";
  state += hiTempAlarm ? "H" : "h";
  state += loTempAlarm ? "L" : "l";
  return state;
}
```

Algo importante es el rendimeinto, y en el caso de arriba el programa
es de un sistema embebido de tiempo real, lo cual los recursos son
limitados en un equipo de esa naturaleza, por lo cual se deberia conciderar
usar StringBuffer en lugar de String.

Pero los test unitarios y el codigo de preoduccion son distintos y corren
en distintos equipos, por lo que esta concideracion pierde fuerza.

### Un assert por test

Esta regla pareceria muy purista, pero tiene sentido por que al ser uno
solo, significa una unica conclucion y por ende mas facil de entender,
y lo mismo podemos crear una API especifica para test que nos ayude
a tener varios estados contenidos en un solo resultado, tal como se
hizo en `Listing 9-5`.

Y para el caso de `Listing 9-1` podemos decir que es muy dificil hacerlo
en un solo assert, en dichos casos no habria problema en separar un
test en dos tests.

```java
public void testGetPageHierarchyAsXml() throws Exception {
  givenPages("PageOne", "PageOne.ChildOne", "PageTwo");
  whenRequestIsIssued("root", "type:pages");
  thenResponseShouldBeXML();
}
public void testGetPageHierarchyHasRightTags() throws Exception {
  givenPages("PageOne", "PageOne.ChildOne", "PageTwo");
  whenRequestIsIssued("root", "type:pages");
  thenResponseShouldContain("<name>PageOne</name>", "<name>PageTwo</name>", "<name>ChildOne</name>");
}
```

El problema de dividirlo de esta manera es que caemos en duplicacion y
podriamos eliminar la duplicacin usando un patron template, pero al ser
mucho lio, en este caso en especifico se prefiere tener mas de un assert
por test, pero alguien podria preferir tener duplicacion, siempre y cuando
uno no se minimise los asserts o la duplicacion no sea mucha, podemos
ignorar estas pequeñas manchas.

### F.I.R.S.T

Un tes debe de cumplir con este acronimo

- Fast: Los test deben de ejecutarse muchas veces durante el proceso
  de desarrollo, y si son lentos uno no quedra hacerlo, y descubrir
  problemas rapido no sera rapido.

- Independent: No deben de depender entre ellos, o uno no debe de poner
  las precondiciones para que se ejecute otro, ya que si lo hacen al fallar
  uno con dependencias, sera dificil hacer su diagnostico.

- Repeatable: Tanto en servidor como en laptop estos deben de funcionar igual,
  si no unos test funcionaran y otros no y esto sera dificil de interpretar.

- Self-Validating: En automatico deben decir si pasaron o no, de lo contrario
  sera un proceso manual y abierto a interpretaciones, por ejemplo, seria un error
  inrepretar un test por su por lectura de logs por medio de una persona.

- Timely: preimero crea el test y luego su implementacion, de lo contrario
  tarde o temprano habra codigo que no esta siendo testeado.

<!--
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-->

## 10. Clases (Repaso)

- Hay raras razones por las cuales una clase deba tener variables publicas,

- Para Definir el orden de declaracion de variables y metodos, publicos,
  estaticos, constantes recomendable siguir convencion estandart del leguaje.

- las clases deben ser pequeñas, y no hablamos de lineas, hablamos de
  responsabilidades

- Debemos ser capases de describir una clase sin usar palabras como
  "si esto entonces", "y", "o", "pero".

- SRP (Single Responsability Principe) las clases solo deben de tener
  una razon=responsabilidad por cual ser cambiadas. Este principio es
  uno de los mas importantes en POO. (Libro: Agile Software
  Development: Principles, Patterns, and Practices, Robert C. Martin)

- Hay la misma complejidad un codigo con muchas y pequeñas clases, que
  uno con grandes y pequeñas clases.
  - ¿Que prefieres?
    - Grandes cajones con muchas cosas
    - Pequeños cajones solo con cosas bien definidas

### Cohecion

- Las clases deberian de tener pocas variables de instancia,
y los metodos solo deben de manipular una o mas de estas, y entre mas
variables manipule la clase estaran mas coehidas a su clase.

#### Mantener cohecion crea mas clases

Imagina un gran metodo que quieres dividirlo en un metodo mas pequeño,
pero utiliza varias variables inicializadas en el alcance(scope) de esta
gran funcion, entonces.

**¿Pasamos estas varias variables como argumento a nuestro nuevo metodo?**, No,
por dios no, ya vimos eso en `Argumentos de Funciones`.

**¿Promobemos estas variables de funcion a variables de clase?**, depende del
contexto, pero lo mas seguro no, porque esto significa que estas nuevas
variables solo van a servir para comunicar pocos metodos entre si de la
clase, esto significa que estan pobremente cohehidas estas variables

**¿Creamos una nueva funcion?**, depende del contexto pero las seguro si,
es bueno por que esta nueva clase tiene una responsabilidad, o al menos
menos responsabilidades, y una cohecion mas alta.

**Un ejemplo de esto** podemos verlo en la transformacion del codigo
`10-6`, el cual el resultado pasa de 1 clase en una pagina, a 3 clases
en casi 3 paginas, y esto tiene sentido porque en un codigo refactorizado
sumas:

- Nombre mas significativos en variables
- = ... en funciones
- = ... en clases
- Espaciacion entre diferentes contextos/significados

Estos nombres auto comentan el codigo, no se re-escribio el codigo de cero,
se pasaron partes de este codigo a una parte que solo pudiera manipular
el contexto el cual esta tratando, incluso tenemos una clase `GeneradorDePrimos`
que por su naturalesa, es una clase %100 estatica, por ende no necesita
instanciarse, se creo solo para contener la logica en su propio espacio.

### Organizarse para el cambio

Cada vez que hacemos un cambio en un sistema, tenemos el riesgo de que
ya no funcione como lo hacia, y una buena organizacion y/o separacion de
responsabilidades hace que esta falta de funcionalidad sea identificable.

**Open-Close Principle (OCP)**: Modificaciones traen mayor probabilidad
de mal funcionamiento sobre lo que ya funciona, en cambio agregar asegura
que si algo va a mal-funcionar entonces esto es debido a lo que se agrego,
podemos ver ese ejemplo en el codigo `10-10` el cual requerimos de SRP
para lograrlo.

### Aislarse del cambio

En POO 101 aprendimos que las clases son implementaciones, mientras que
las clases abstractas son solo conceptos, en otras palabras, una clase
son detalles de funcionamiento, mientras que clases abstractas son contratos,
es decir, una clase es como hago determinada cosa y una clase abstracta
solo indica que es lo que debe pasar.

Que una clase dependa de detalles de otra clase es peligroso, ya que si
falla una falla una clase fallara la otra, y de esto salio **DIP**
(principio de inversion de dependencias), que en resumen dice, que nuestras
clases deben de depender de abstracciones y NO de implementaciones.

### TODO

TODO: Tener en cuenta la biografia del capitulo, para tener en cuenta
una base solida y profunda de POO.

<!--
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-->

## 11. Sistemas (Repaso)

La diferencia entre un sistema complejo y uno simple, es que el simple
puede ser administrado por una sola persona, mientras que el complejo
debe ser o solo puede ser administrado por multiples personas con distintas
especialidades.

### ¿Como construlles una ciudad?

¿Una sola persona podria manejar todos los detalles?, No, incluso administrar
una ciudad ya creada es dificil, pero aun asi (la mayoria del tiempo) las
ciudades funcionan.
Esto es debido a que estas han evolucionado con una abstraccion adecuada,
y los distinto componentes pueden funcionar, solo la dependencia e
independencia necesaria, sin tener que conocer los detalles de la gran
imagen.
Por ejemplo, existen sistemas de agua, electricidad, trafico, aplicacion
de la ley, desarrollo de software :O, que hacen lo necesario para que
la ciudad funcione correctamente, a traves de personas que conocen la
tanto la imagen general del proyecto sin preocuparse de los detalles,
como tambien otros pueden concentrarse en los detalles, sin preocuparse por cosas
de la gran imagen como interconecciones o aspectos de otros sistemas.

### Construccion separada de un sistema de su uso

En desarrollo de software al igual que un edificio hay dos estapas, la
estapa de construccion donde en dicho edificio hay gruas, las personas
dentro son constructores dentro de un mero cuadro de cemento, y cuando
es terminado, es una bonita construccion con personas viviendo ahi.

Un ejemplo seria la iniciacion perezosa, la cual en tiempo de desarrollo
no es un problema, pero en tiempo de ejecucion puede hacer una diferencia,

### Separacion del main, fabricadores e inyeccion de dependencias (TODO: Repasar estos)

Una estrategia para separar el proceso_de_construccion del tiempo_de_ejecucion
son los factories, el cual evitan que los modulos construyan/traigan sus
dependencias, en su lugar estas dependencias son creadas por algo que sabe
de este tema, y asi en lugar de que una clase cablee sus dependencias y
haga su trabajo lo cual rompe el SRP, incluimos un factory el cual le quita
la responsabilidad de crear y cablear sus dependencias.

La inyeccion de depedencias es que el objeto dependa de una abstraccion,
idealmente una interface (una clase 100% abstracta), y su implementacion
es inyectada a travez de inyeccion de dependencias, el cual dependiendo
del caso, por medio del constructor, de un setter o por argumento de
un metodo.
Y el IoC es una libreria que a travez de DI gestiona la creacion y cableado
de dependencias, un ejemplo claro de esto es Spring IoC.

### Escalar

Una ciudad no nace se hace, ya que esta primero fue un asentamiento, que
crecio a pueblo, para luego ya ser una ciudad, y con posibilidad de crecer
a una megapolis.
Es por eso que luego hay embotellamiento es una calle angosta, por que
tiempo atras no se pudo justificar construir una autopista en medio de
un pueblito, lo que hace la diferencia es que tan complicado es hacer mas
ancha esta calle, que va de hacer un camino alterno ha atravezar una cuadra.

Lo mismo el software hay que preocuparse de las necesidades actuales, lo
que hace el codigo limpio es tener en mente que puede creecer y que este
crecimiento sea lo menos doloroso posible.

### Preocupaciones principales y transversales

Persistencia (en archvo o RDBMS), cache, seguridad, manejo de errores,
encoding/decoding, etc. son preocupaciones transversales.

Para una aplicacion de banco nuestra preocupacion principal,
son los manejos de cuentra, tranferencias, retiros, depositos, etc. pero
tenemos la preocupacion transversal de guardar en una base de datos,
resguardar la seguridad a accesos de cuentas y operaciones, informar
de errores, entregar informacion de forma legible/manejable, etc.

#### Java Proxies

Los proxies son adecuados para situaciones simples, y son como reflejos
modificados de un objeto, creados por ejemplo usando `java.lang.reflect.*`,
y estos reflejos modificados son usados para satisfacer la procupacion
transversal, imitando el funcionamiento del objeto del cual a partir de
este se creo el proxy, por ejemplo.

```java
public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
  
  String methodName = method.getName();
  
  if (methodName.equals("getAccounts")) {
    bank.setAccounts(getAccountsFromDatabase());
    return bank.getAccounts();

  } else if (methodName.equals("setAccounts")) {
    bank.setAccounts((Collection<Account>) args[0]);
    setAccountsToDatabase(bank.getAccounts());
    return null;

  } else {
    ...
  }
}
```

Aqui podemos ver que podemos "invocar" `bank.getAccounts()` pero en
realidad se invocara `getAccountsFromDatabase`.

#### Analogia de AOP

Como del hecho de alguien corto un arbol con un cuchillo, esta idea
evoluciono al hacha, cierra, motocierra y tractor-cierra, pero desde aquel entonces al dia
de hoy la preocupacion principal es cortar un arbol, y la transversal es
como se cortara y que se necesita para cortarlo, lo cual puede depender del
contexto en el que nos encontremos.

lo mismo paso con los proxies, los cuales han evolucionado a AOP
(Aspects Oriented Programing), el cual sirve para esconder la preocupacion
transversal, y dicha preocupacion transversal puede ocurrir de distintas maneras.

en codigo seria algo asi, donde podemos ver de manera facil podemos cambiar
entre hacha y motocierra para cortar arboles, y en este caso la tarea de
como construir y de donde obtener el cortador de arboles lo estamos
delegando aun IoC.

```java
interface CortadorArbol {
  Collection<Tronco> cortarArbol(Arbol a);
  int obtenerAvance();
}

class Hacha implements CortadorArbol {
  ...
}

class MotoCierra implements CortadorArbol {
  ...
}

class AcerraderoModerno {

  @Wire("motocierra")
  private CortadorArbol cortador;

  public irPorMadera(Ejido ejido) {
    final arboles = ejido.obtenerArboles();
    Collection<Tronco> cortados = [];
    for(Arbol arbol : arboles) {
      final troncos = cortador.cortarArbol(arbol);
      cortados.add(troncos);
    }
    ...
  }

}
```

De esta forma nuestra preocupacion principal esta desacoplada de nuestra
preocupacion transversal, y hace mucho mas facil cambiar el escenario
y aun asi cumplir con la tarea enconmendada.

#### Pure Java AOP Frameworks

Por ejemplo, `Listing 11-5` ocultamos muchos detalles de preocupacion transversal
en simples etiquetas, no indicamos si es Postgres o MariaDB, si sera
en Json o Html, pero estamos indicando como se debe comportar esta
preocupacion transversal, sea cual sea esta.

```java
//Listing 11-5
@Entity
@Table(name = "BANKS")
public class Bank implements java.io.Serializable {
  
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private int id;

  @Embedded
  private Address address;

  @OneToMany(
    cascade = CascadeType.ALL,
    fetch = FetchType.EAGER,
    mappedBy="bank"
  )
  private Collection<Account> accounts = new ArrayList<Account>();

  public void addAccount(Account account) {
    account.setBank(this);
    accounts.add(account);
  }
  public Collection<Account> getAccounts() {
    return accounts;
  }
```

Incluso podemos quitar esas etiquetas con el uso de xml, y asi dejar
unicamente el POJO.

```xml
<beans>
  
  <bean id="appDataSource"
    class="org.apache.commons.dbcp.BasicDataSource"
    destroy-method="close"
    p:driverClassName="com.mysql.jdbc.Driver"
    p:url="jdbc:mysql://localhost:3306/mydb"
    p:username="me"
  />

  <bean id="bankDataAccessObject"
    class="com.example.banking.persistence.BankDataAccessObject"
    p:dataSource-ref="appDataSource"
  />

  <bean id="bank"
    class="com.example.banking.model.Bank"
    p:dataAccessObject-ref="bankDataAccessObject"
  />
  
</beans>
```

Y lo que ocurre en ambos casos a escondidas es lo mismo que el proxy,
donde aparentemente llamamos al metodo de nuestra preocupacion principal,
pero en realidad ocurre una llamada escalonada de metodos, que van
desde el metodo de la mas exterior de las preocupaciones transversales
hasta la interior que seria nuestra preocupacion principal.

![beans-and-aspects.oriented-programing](./img/beans-and-aspects.oriented-programing.png)

Por ejemplo, al llamar `getAccounts()` se llama primero `dao.findAllAcounts()`
que a su ves llama a `db.findAll()` y al final `bank.getAccount()`

### Los sistemas necesitan lenguajes de dominio especifico (DSL)

## 12. Emergencia

Segun Kent Kebeck podemos seguir 4 reglas para mantener lograr un buen
diseño sobre el camino, que en orden de importancia son;

- Corre todos los tests
- No contiene duplicacion
- Expresa la intencion del programador
- Minimisa el numero de metodos y clases

### Regla 1, Corre todos los tests

Primero y mas importante, un programa debe comportase como se espera, y
si no hay una manera simple de hacerlo, incluso el mejor diseño es cuestionable.

Testear funciones complejas o clases que hacen varias cosas es dificil,
asi que al hacer pruebas unitarias nos veremos forzados a escribir con SRP.
Y lo mismo pasa con el acomplamiento, ya que diversos componenetes acoplados son
dificil de testear, o un componente con poca cohesion es dificil de testear.

### Regla 2, Refactoriza y elimina duplicacion

Ya que tenemos los test automatizados, ahora estamos enpoderados para
poder hacer refactorizaciones sin el miedo de que estas rompan algo,
esto porque los test nos lo diran en tanto pase y donde paso.

### Regla 3, Expresividad

Todos hemos hecho un codigo dificil de entender, esto porque cuando estamos
escribiendo tenemos un alto nivel de comprencion, pero los que a un futuro
lo leeran no tendran el mismo nivel de comprencion.
En tanto funciona un codigo, no nos paramos a pensar en que sea facil de
leer para la siguiente persona, y lo mas seguro esa siguiente persona
seras tu.
Podemos apoyarnos en los patrones de diseño ya que varios de estos estan
pensados para aumentar la expresividad, como lo es el visitor o el command.

### Regla 4, Reduce cantidad de metodos y clases

Po ultimo y menos importante, a veces muchas clases significa dogmas,
por ejemplo, toda dependencia necesita una interface, o hay que separar
totalmente clases de datos y clases de comportamiento.

Debemos de entender que los principios, consejos y patrones propuestos,
son herramientas con sus fortalezas y debilidades, y hay que estar dispuesto
a no usar alguna ya que sus debilidades podran traer mas problemas.

Aun que sea la regla menos importante, es un hechos que pocos componentes
son mas faciles de entender, que tratar de entender muchos componentes.

## 10. Concurrencia

a
La concurrencia es separa el que del cuando, lo cual trae beneficios
tanto de rendimiento como de estructura, por ejemplo, para crear un reporte
cual depende de muchas peticiones http, en una ejecucion sincrona
hay que esperar por muchas cosas y por fin hacer algo, en cambio una
ejecucion asincrona hacemos diferentes cosas al mismo tiempo, y podemos
separar preocupaciones.

### Mitos e ideas falsas

- No siempre aumenta el rendemiento, solo cuando hay muchos tiempos de
  de epera, de lo contrario es trivial.
- El diseño de algoritmo pueden ser muy diferentes si aplicamos asincronia
  o sincronia.

### Desafios

Este codigo

```java
public class X {
  private int lastIdUsed;
  
  public int getNextId() {
      return ++lastIdUsed;
  }

}
```

Este codigo tribial con un valor inical de 10 y dos hilos se puede complicar

- hilo1 obtiene 10 actualiza a 11, hilo2 obtiene 10 actualiza a 11, resultado 11
- hilo1 obtiene 10 actualiza a 11, hilo2 obtiene 11 actualiza a 12, resultado 12
- hilo2 obtiene 10 actualiza a 11, hilo1 obtiene 11 actualiza a 12, resultado 12

Y esto se puede calcular a todos los posibles tiempos de ejecucion, los
cuales daria 12,870 tiempos, la cuales la mayoria estaran correctos, pero
unos de ellos no, y ese es el problema.

## Refinamiento sucecivo (saltado)

...

### JUnit Internals

