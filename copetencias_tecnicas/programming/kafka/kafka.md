# Kafka

- [Kafka](#kafka)
  - [Curso Introductorio de Kafka](#curso-introductorio-de-kafka)
    - [Introduccion](#introduccion)
      - [Que es Kafka?](#que-es-kafka)
      - [Que tipo de problemas resuelve Kafka?](#que-tipo-de-problemas-resuelve-kafka)
      - [Caracteristicas de Kafka?](#caracteristicas-de-kafka)
      - [Casos de uso?](#casos-de-uso)
    - [Teoria](#teoria)
      - [Que es un topic?](#que-es-un-topic)
      - [What and what are for partitions and offsets](#what-and-what-are-for-partitions-and-offsets)
      - [Some characteristics of partitions and offsets](#some-characteristics-of-partitions-and-offsets)
      - [08 Producers](#08-producers)
      - [What is a producer?](#what-is-a-producer)

## Curso Introductorio de Kafka

### Introduccion

#### Que es Kafka?

Kafka es un consumidor de informacion, el cual puede conectar a uno o varios emisores con un o varios receptores, donde un emisor puede ser una base de datos, un cliente web, etc. y el receptor un backed, logger, etc.

#### Que tipo de problemas resuelve Kafka?

Si tenemos 4 fuentes de info y 6 consumidores de info, para interconectarlos necesitamos 24 integraciones, y tenemos aun mas variables como

--Protocolo (tcp, http, ftp, jdbc ...)
--Formatos (xml, json, csv, protobuf ...)
--Evolución de schema/esquema

Solución

--Kafka seria una unica integracion donde en un solo punto los emisores y receptores se comunican en un mismo punto

#### Caracteristicas de Kafka?

-Open source
-Creado por: Linkeding y mantenido por Confluent, IBM, Cloudera
-Usado por: el 80% de grandes empresas como Twitter, Netflix, Uber, etc.
-horizontalmente escalable: Puede manejar cientos de brokers y millones de mensajes por segundo
-Performance: Puede tener una latencia menor de 10ms que puede ser considerado como tiempo real

#### Casos de uso?

SOLO USADO COMO SISTEMA DE TRANSPORTACION DE MULTIPLES Y GRANDES CANTIDADES DE INFORMACION

- Sistema de mensajeria

- Seguimiento de actividad

- Logs de varias aplicaciones

- Flujo de procesamiento (nuevo)

- De-acoplar dependencias de sistemas (micro-servicios pub/sub)

- Integracion con Spark, Flink, Hadoop, etc.

Casos reales

- Netflix: Recomendaciones

- Uber: Seguimiento taxis, usuarios, precios variables en tiempo real

- Linkedin: Prevencion de span, interacciones, recomendaciones de contactos

### Teoria

#### Que es un topic?

Es un stream particular de datos, como lo son logs, ventas, tweets, registro gps, etc.

No tienen restricción de esquema

Se pueden crear tantos topics como querramos solo que tengan un nombre unico

Pueden mandar cualquier tipo de formato (json, binario, archivos.txt, etc).

No se pueden consultar solo consumir

#### What and what are for partitions and offsets

A topic can divide in several partitions, and in each of these can send data, the difference is that each message will be tagged with a different secuence of Id, for example

            ┌─────
            │
            │   logs_1  [1,2,3,4,5]
            │
            │
Topic_logs  │   logs_2  [1,2,3,4,5,6,7,8]
            │
            │
            │   logs_3  [1,2,3]
            │
            └─────

#### Some characteristics of partitions and offsets

- You can have as many partitions as you want per  topic

- Once the data is written to a partition, it is inmutable (can not ber changed)

- Data is temporal: Data has a configurable time by default 1 week

- Offsets (kind of id) only have meaning for a specific partition
  - Example: offset 1 in partition 0 is not the same than offset 1 in partition 1

- Offsets are not reusable even if this have been deleted

- Order is guaranteed only within a partition (not across partitions)

- Data is asigned randomly to a partition unless a key is assigned

#### 08 Producers

#### What is a producer?

Is who writes data to topics in a partition

Producers know to with partition to write to
