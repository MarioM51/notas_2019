# Angular

- [Angular](#angular)
  - [Introduccion](#introduccion)
    - [requisitos para instalar angular?, Como instalar angular (cli)?, como crear proyecto?, y como iniciar proyecto](#requisitos-para-instalar-angular-como-instalar-angular-cli-como-crear-proyecto-y-como-iniciar-proyecto)
    - [Componentes (definición, creación y partes)](#componentes-definición-creación-y-partes)
      - [¿De que esta conformado un componente angular?;"](#de-que-esta-conformado-un-componente-angular)
      - [Como generar un componente con el cli de angular y que cosas crea?](#como-generar-un-componente-con-el-cli-de-angular-y-que-cosas-crea)
      - [¿Como crear un componente manualmente?](#como-crear-un-componente-manualmente)
      - [Como indicar el nombre de tag que representara nuestro componente angular?](#como-indicar-el-nombre-de-tag-que-representara-nuestro-componente-angular)
      - [Como usar un archivo html que represente la vista del componente?](#como-usar-un-archivo-html-que-represente-la-vista-del-componente)
      - [Como usar el template del componente angular  en el mismo archivo?](#como-usar-el-template-del-componente-angular--en-el-mismo-archivo)
      - [Como importar css en un componente angular?](#como-importar-css-en-un-componente-angular)
    - [Ciclo de vida](#ciclo-de-vida)
      - [Cuales son los ciclos de vida de un componente angular, y codigo ejemplo?](#cuales-son-los-ciclos-de-vida-de-un-componente-angular-y-codigo-ejemplo)
      - [Porque no es recomendable traer información del servidor (o parecido) en el constructor de un componente?](#porque-no-es-recomendable-traer-información-del-servidor-o-parecido-en-el-constructor-de-un-componente)
  - [Angular Book LANN4](#angular-book-lann4)
    - [Introduction](#introduction)
      - [Modules](#modules)
        - [Que es un modulo](#que-es-un-modulo)
        - [Como crear un modulo](#como-crear-un-modulo)
        - [Como agregar un modulo al main module](#como-agregar-un-modulo-al-main-module)
        - [Como usar componente dentro de modulo](#como-usar-componente-dentro-de-modulo)
    - [Componentes: Inter-comunicación](#componentes-inter-comunicación)
      - [Pasar valor de padre a hijo](#pasar-valor-de-padre-a-hijo)
      - [Pasar evento de hijo a padre (con emit)](#pasar-evento-de-hijo-a-padre-con-emit)
      - [Pasar evento con datos de hijo a padre (con emit)](#pasar-evento-con-datos-de-hijo-a-padre-con-emit)
      - [Pasar evento con datos de padre a hijo](#pasar-evento-con-datos-de-padre-a-hijo)
    - [DOM Manipulation](#dom-manipulation)
      - [Change clicked list item created with ngFor](#change-clicked-list-item-created-with-ngfor)
      - [Deteccion de cambios](#deteccion-de-cambios)
        - [ChangeDetectorRef: Caso de uso](#changedetectorref-caso-de-uso)
    - [Componentes: Ciclo de vida](#componentes-ciclo-de-vida)
      - [El binding se ejecuta dos veces](#el-binding-se-ejecuta-dos-veces)
      - [Ciclos de vida y sus usos](#ciclos-de-vida-y-sus-usos)
    - [Directives and Pipes](#directives-and-pipes)
      - [Que es un pipe](#que-es-un-pipe)
      - [Que es una directiva](#que-es-una-directiva)
      - [Ejemplo ngIf](#ejemplo-ngif)
      - [Ejemplo ngFor](#ejemplo-ngfor)
      - [ng-template](#ng-template)
      - [Ejemplo Pipe personalizada](#ejemplo-pipe-personalizada)
      - [Ejemplo Directiva personalizada](#ejemplo-directiva-personalizada)
    - [Services](#services)
      - [Como usar los services en Angular](#como-usar-los-services-en-angular)
      - [Como crear y usar un servicio](#como-crear-y-usar-un-servicio)
    - [Observables and RxJs](#observables-and-rxjs)
      - [Uso básico de observable](#uso-básico-de-observable)
      - [Crear key-logger con observables y RxJS](#crear-key-logger-con-observables-y-rxjs)
      - [Usar observable en template](#usar-observable-en-template)
    - [Comunicación HTTP](#comunicación-http)
      - [petición fetch a observable](#petición-fetch-a-observable)
      - [Petición Http usando la aproximación oficial de angular](#petición-http-usando-la-aproximación-oficial-de-angular)
      - [Autenticación y Autorización via http](#autenticación-y-autorización-via-http)
        - [Ejemplo Autenticación](#ejemplo-autenticación)
        - [Ejemplo Autorización con interceptores](#ejemplo-autorización-con-interceptores)
    - [Navegación con routing](#navegación-con-routing)
      - [Routing en el mismo modulo (explicativo)](#routing-en-el-mismo-modulo-explicativo)
      - [Routing dividido en módulos (comúnmente en producción)](#routing-dividido-en-módulos-comúnmente-en-producción)
      - [Navegación imperativa](#navegación-imperativa)
      - [Marcar enlace como activo o actual](#marcar-enlace-como-activo-o-actual)
      - [Pase de parámetros via url (url parameter)](#pase-de-parámetros-via-url-url-parameter)
      - [Re-usando componentes con child-routes](#re-usando-componentes-con-child-routes)
      - [Capturar parámetros url después de (?)](#capturar-parámetros-url-después-de-)
      - [Routing guard](#routing-guard)
        - [canActivate](#canactivate)
        - [canDeactivate](#candeactivate)
      - [Por el camino: routing](#por-el-camino-routing)
        - [Pasar objeto al navegar](#pasar-objeto-al-navegar)
        - [Uso de dos outlets en el mismo template](#uso-de-dos-outlets-en-el-mismo-template)
      - [Errores con routing](#errores-con-routing)
      - [Al entrar por primera vez el redirectTo no funciona y no se renderiza primer componente](#al-entrar-por-primera-vez-el-redirectto-no-funciona-y-no-se-renderiza-primer-componente)
    - [Formularios](#formularios)
      - [Formularios template-binding form](#formularios-template-binding-form)
      - [Formularios Reactivos](#formularios-reactivos)
      - [Formulario Feedback con clases de angular](#formulario-feedback-con-clases-de-angular)
      - [Validando inputs y mostar errores](#validando-inputs-y-mostar-errores)
      - [Crear validadores personalizados](#crear-validadores-personalizados)
    - [Cypress And Angular](#cypress-and-angular)
      - [Errors](#errors)
        - [Don't use TypeScript clases in tests (.cy.ts)](#dont-use-typescript-clases-in-tests-cyts)
    - [Mini Proyectos](#mini-proyectos)
      - [Scroll Infinito con 3 tabs=pestañas que conserva el scroll de cada una con navegacion](#scroll-infinito-con-3-tabspestañas-que-conserva-el-scroll-de-cada-una-con-navegacion)
      - [RcJs sincronizar listaUI infinita con subjects (BehaviorSubject)](#rcjs-sincronizar-listaui-infinita-con-subjects-behaviorsubject)

## Introduccion

### requisitos para instalar angular?, Como instalar angular (cli)?, como crear proyecto?, y como iniciar proyecto

<!-- Fuente: https://angular.io/guide/setup-local -->

1. Tener instalado node y npm

2. Instalar el cli de angular: `npm install -g @angular/cli`

3. Crear proyecto angular: `ng new my-app`

4. Iniciar proyecto: `ng serve --open` <!-- correr aplicación -->

<!--

#########################################

-->

----

<!--

#########################################

-->

### Componentes (definición, creación y partes)

#### ¿De que esta conformado un componente angular?;"

<!-- Fuente: https://angular.io/guide/component-overview -->

1. An HTML template that declares what renders on the page
2. A TypeScript class that defines behavior
3. A CSS selector that defines how the component is used in a template
4. Optionally, CSS styles applied to the template
"

#### Como generar un componente con el cli de angular y que cosas crea?

<!-- Fuente: https://angular.io/guide/component-overview#creating-a-component-using-the-angular-cli -->

`ng generate component <component-name>`

Generara

- A directory named after the component
- A component file, `<component-name>.component.ts`
- A template file, `<component-name>.component.html`
- A CSS file, `<component-name>.component.css`
- A testing specification file, `<component-name>.component.spec.ts`

<!-- mas opciones en https://angular.io/cli/generate#component-command -->

#### ¿Como crear un componente manualmente?

<!-- https://angular.io/guide/component-overview#creating-a-component-manually -->

#### Como indicar el nombre de tag que representara nuestro componente angular?

<!-- https://angular.io/guide/component-overview#creating-a-component-manually -->

```ts
  import { Component } from '@angular/core';
  ...
  @Component({
+   selector: 'employee',
  })
```

#### Como usar un archivo html que represente la vista del componente?

<!-- https://angular.io/guide/component-overview#creating-a-component-manually -->

```ts
  import { Component } from '@angular/core';
  ...
  @Component({
    ...
+   templateUrl: './component-overview.component.html',
    ...
  })
```

#### Como usar el template del componente angular  en el mismo archivo?

<!-- https://angular.io/guide/component-overview#creating-a-component-manually -->

```ts

  import { Component } from '@angular/core';
  ...
  @Component({
    ...
+   template: '<h1>Hello World!</h1>',
// If you want to add multiple lines
+   template: `
      <h1>Hello World!</h1>
      <p>This template definition spans multiple lines.</p>
    `
    ...
  })
```

Si quieres usar multiples lineas

```ts
@Component({
  
})
```

#### Como importar css en un componente angular?

<!-- https://angular.io/guide/component-overview#creating-a-component-manually -->

```ts
  import { Component } from '@angular/core';
  ...
  @Component({
    ...
+   styleUrls: ['./component-overview.component.css']
//o
+   styles: ['h1 { font-weight: normal; }']
    ...
  })
```

### Ciclo de vida

Fuente: [Link](https://angular.io/guide/lifecycle-hooks)

#### Cuales son los ciclos de vida de un componente angular, y codigo ejemplo?

[Lista de eventos/ciclos](https://angular.io/guide/lifecycle-hooks#lifecycle-event-sequence)

[Codigo completo](https://angular.io/generated/live-examples/lifecycle-hooks/stackblitz.html)

Ciclos: Create, Update, Delete

Codigo de ejemplo

```ts
  import { Component } from '@angular/core';
  import LoggerService from '../utils/LoggerService';

  @Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
  })
  export class AppComponent {
    public title: string = 'my-app';
    public nextId: number = 0;

    constructor() {}

    ngOnInit() {
+     this.logIt('OnInit');
    }

    logIt(msg: string) {
      LoggerService.log(`#${this.nextId++} ${msg}`);
    }
  }
```

#### Porque no es recomendable traer información del servidor (o parecido) en el constructor de un componente?

Deben ser baratos y seguros de construir, en cambio el `ngOnInit()` seria un buen lugar para hacerlo

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Angular Book LANN4

### Introduction

#### Modules

##### Que es un modulo

Igual que en programación OO, son componentes y funcionalidad que tiene
una razón de existir por si sola, por ejemplo, los módulos de stock,
shopping, pay-path, comments, etc.

Por ejemplo, el `main module` es el que se carga en el index.html

##### Como crear un modulo

<!-- crear modulo -->

```r
ng generate module products
```

##### Como agregar un modulo al main module

Cuando creamos un modulo este no se agrega de forma automática a nuestra
aplicación, asi que debemos de incluirlo de la sig. manera

```ts
  @NgModule({
      declarations: [
      AppComponent
    ],
    imports: [
      BrowserModule,
+     ProductsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
  })
```

##### Como usar componente dentro de modulo

1, crear modulo products (ProductListComponent vamos a crearlo adelante)

```ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './product-list/product-list.component';

@NgModule({
  declarations: [ProductListComponent],
  imports: [CommonModule],
  exports: [ProductListComponent],
})
export class ProductsModule {}

```

2, crear componente productsList

```ts
import { Component } from '@angular/core';

@Component({
  selector: 'product-list',
  template: '<p>{{name}}</p>',
  styles: ['p{ color: darkgreen; }'],
})
export class ProductListComponent {
  name = 'ProductList Working';
}
```

3, importar modulo al main

```ts
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ProductsModule } from '../products/products.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, ProductsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
```

4, Ya podemos usar el `ProductListComponent`, dentro de `AppModule`

```html
<p>{{name}}</p>
<product-list></product-list>
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Componentes: Inter-comunicación

#### Pasar valor de padre a hijo

Para eso utilizamos la anotacion `@Input`

1,Creamos el componente hijo

Recordar Que: el componente debe de estar agregado en el modulo, para
evitarnos este boilerplate, mejor usar el cli de angular para generar componente

```ts
import { Component, Input } from '@angular/core';

@Component({
  selector: 'product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent {
  @Input()
  public name: string = '';
}
```

```html
<p>Selected: {{name}}</p>
```

2,En componente padre usamos componente hijo, y le pasamos el valor

```ts
  import { Component } from '@angular/core';

  @Component({
    selector: 'product-list',

    template: `
      <ul>
        <li (click)="selected = 'Perro A'">Perro A</li>
        <li (click)="selected = 'Gato A'">Gato A</li>
        <li (click)="selected = 'Hamster A'">Hamster A</li>
        <li (click)="selected = 'Tortuga A'">Tortuga A</li>
      </ul>
+     <product-details [name]="selected"></product-details>
    `,

    styles: [`
        p {
          color: darkgreen;
        }
    `],
    
  })
  export class ProductListComponent {
    public selected: string = '';

    public selProduct() {}
  }
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Pasar evento de hijo a padre (con emit)

En este caso solo ejecutamos un método (no pasamos valores), asi que
debemos de aprovechar la variable que tenemos entre hijo y padre, para
saber cual producto estamos agregando al carrito.

Lo siguiente sera el resultado, el cual podemos observar que primero debemos
seleccionar un producto y luego agregarlo, si no seleccionamos nada, entonces
no estamos comunicando estado.

![01_component_event_child_to_parent](./img/01_component_event_child_to_parent.png)

1, En el componente hijo, emitimos un evento `addEvent` el cual el padre
podrá escuchar

```ts
    import { Component, Input, Output, EventEmitter } from '@angular/core';

    @Component({
      selector: 'product-details',

      template: `
        <div>
          <p>Selected: {{ name }}</p>
+         <button (click)="add()">Add2Car</button>
        </div>
      `,

      styles: [`
        div {
          border: 1px solid red;
          margin: 5px;
        }
      `],
    })
    export class ProductDetailsComponent {
      @Input()
      public name: string = '';

+     @Output()
+     public addEvent = new EventEmitter();

      public add() {
+       this.addEvent.emit();
      }
    }
```

2, En el padre, escuchamos el evento, notar que no enlazamos un estado entre
padre e hijo, usamos solo el estado del padre, el cual confiamos en que
ambos estados tengan el mismo valor

```ts
import { Component } from '@angular/core';

@Component({
  selector: 'product-list',

  template: `
    <div>
      <ul>
        <li (click)="selected = 'Perro A'">Perro A</li>
        <li (click)="selected = 'Gato A'">Gato A</li>
        <li (click)="selected = 'Hamster A'">Hamster A</li>
        <li (click)="selected = 'Tortuga A'">Tortuga A</li>
      </ul>
+     <product-details [name]="selected" (addEvent)="onAdd()"></product-details>
    </div>
  `,

  styles: [
    `
      div {
        border: 1px solid blue;
      }
    `,
  ],
})
export class ProductListComponent {
  public selected: string = '';

  onAdd() {
    window.alert('You just add: ' + this.selected);
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Pasar evento con datos de hijo a padre (con emit)

Es igual que pasar unicamente el evento, con la única diferencia que este
evento llevara dentro de el la info. que el hijo proporciona, y el padre
capturara esa info. a traves del evento que detecto.

1, hijo

```ts
    import { Component, Input, Output, EventEmitter } from '@angular/core';

    @Component({
      selector: 'product-details',

      template: `
        <div>
          <p>Selected: {{ name }}</p>
          <button (click)="add()">Add2Car</button>
        </div>
      `,

      styles: [
        `
          div {
            border: 1px solid red;
            margin: 5px;
          }
        `,
      ],
    })
    export class ProductDetailsComponent {
      @Input()
      public name: string = '';

      @Output()
+     public addEvent = new EventEmitter<string>();

      public add() {
+       this.addEvent.emit('-' + this.name + '-');
      }
    }
```

2, Padre

```ts
    import { Component } from '@angular/core';

    @Component({
      selector: 'product-list',

      template: `
        <div>
          <ul>
            <li (click)="selected = 'Perro A'">Perro A</li>
            <li (click)="selected = 'Gato A'">Gato A</li>
            <li (click)="selected = 'Hamster A'">Hamster A</li>
            <li (click)="selected = 'Tortuga A'">Tortuga A</li>
          </ul>
          <product-details
            [name]="selected"
+           (addEvent)="onAdd($event)"
          ></product-details>
        </div>
      `,

      styles: [
        `
          div {
            border: 1px solid blue;
          }
        `,
      ],
    })
    export class ProductListComponent {
      public selected: string = '';

+     onAdd(name: string) {
        window.alert('You just add: ' + name);
      }
    }

```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Pasar evento con datos de padre a hijo

**Resumen:** Se usa un subject en la clase padre y se lo pasamos como observable
al hijo

- [Source](https://stackoverflow.com/questions/44053227/how-to-emit-an-event-from-parent-to-child)

```ts
class ParentComponent {
  eventsSubject: Subject<void> = new Subject<void>();

  emitEventToChild() {
    this.eventsSubject.next();
  }
}
```

```html
<child-component [events]="eventsSubject.asObservable()"> </child-component>
```

```ts
class ChildComponent {
  private eventsSubscription: Subscription;

  @Input()
  events: Observable<void>;

  ngOnInit(){
    this.eventsSubscription = this.events.subscribe(() => doSomething());
  }

  ngOnDestroy() {
    this.eventsSubscription.unsubscribe();
  }
}
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### DOM Manipulation

#### Change clicked list item created with ngFor

**Resumen**: Debemos usar el evento, ya que este contiene el target, y
con el objeto Render2 propio de angular, podremos cambiar sus características
del elemento clikeado, en este ejemplo agregamos una clase.

1, Notar `$event`

```html
<article class="post-item" *ngFor="let p of (list$ | async)!" >
  <span (click)="onClickAddReaction($event, 'like', p)">
    <mat-icon like />{{p.likesCount}}
  </span>
</div>
```

2, Notar `addClass(e.target, 'liked');`

**Importante**: En este caso dentro de `.addReaction(`, modifico la cantidad
de likes el cual angular en automatico refleja el cambio en la UI.

```ts
    export class ListComponent {
      constructor(private renderer: Renderer2, private postsSrv: PostsService) {}

      onClickAddReaction(e: any, r: ReactionType, p: PostDto): void {
        this.postsSrv.addReaction(r, p);
        if (r == 'like') {
+         this.renderer.addClass(e.target, 'liked');
        }
      }
    }
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Deteccion de cambios

Fuente: [link](https://netbasal.com/a-comprehensive-guide-to-angular-onpush-change-detection-strategy-5bac493074a4)

**Resumen:** Tenemos el modo por default, que angular reflejara los cambios
en el template, ya sea porque una propiedad cambio o un ciclo de vida
termino (un checa siempre), pero a veces esto no es suficiente o da problemas
de rendimiento, por lo que podemos cambiar la estategia de deteccion de cambios
con:

```ts
    @Component({
      selector: 'xxx',
+     changeDetection: ChangeDetectionStrategy.OnPush,
```

En este caso corre en casos mas limitados como son:

- The Input reference changes.
- An event originated from the component or one of its children.
- We run change detection explicitly.

##### ChangeDetectorRef: Caso de uso

- [Fuente](https://stackoverflow.com/questions/56311396/angular-dom-not-updating-on-changes)

**tags:** no se refleja el cambio en el componente, no se reflejan los
cambios en el componente no muestra cambios,
no se muestran los cambios en el template.

**Resumen:** Pos razones que aun no entiendo {TODO} en un proyecto me paso
que los cambios en el componente no se reflejaban en el template, por
lo que lo solucione de la sig. manera.

**Escenario**: En el siguiente componente, tenemos este problema simulado de no
reflejar los cambios, donde si des-comentamos la linea `this.cdr.detectChanges();`
el problema de **solucionara**. lo simule con, un `timeout`, pintar una sub-propiedad
grande en este caso una imagen.

```ts
    @Component({
      selector: 'x',
      changeDetection: ChangeDetectionStrategy.OnPush,
      template: '<img [src]="p.img" width="100" height="100" />',
    })
    export class LoginFormComponent implements OnInit {
      
      constructor( private cdr: ChangeDetectorRef) {}

      public p = {
        img: '',
      };

      ngOnInit(): void {
        this.showImage();
      }

      private showImage() {
        setTimeout(() => {
          const img64 =
            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAIAAAAlC+aJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABcSURBVGhD7c8BCQAgEAAxE3z/Sp9KYxzCYAV27s7XBGoCNYGaQE2gJlATqAnUBGoCNYGaQE2gJlATqAnUBGoCNYGaQE2gJlATqAnUBGoCNYGaQE2gJlATqH0e2HnJnGG0ZIIhFAAAAABJRU5ErkJggg==';
          const imgF = this.dataURLtoFile(img64, 'key.gif');
          this._showImage(imgF);
        }, 1000);
      }

      private dataURLtoFile(dataurl: string, filename: string) {
        let arr = dataurl.split(',');
        let mime = (arr[0].match(/:(.*?);/) ?? [])[1];
        let bstr = atob(arr[arr.length - 1]);
        let n = bstr.length;
        let u8arr = new Uint8Array(n);
        while (n--) {
          u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, { type: mime });
      }

      private _showImage(img: File) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          this.p.img = e.target.result as string;
//Linea Importante
          //this.cdr.detectChanges();
        };
        reader.readAsDataURL(img);
      }
    }
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Componentes: Ciclo de vida

#### El binding se ejecuta dos veces

Si queremos ver mas detalladamente como Angular muestra una variable en
el template, podemos obtener el valor por un método que este muestre
un log, y asi saber cuantas veces se ejecuta este

```ts
  import { Component, Input, Output, EventEmitter } from '@angular/core';

  @Component({
    selector: 'product-details',

    template: `
      <div>
+       <p>Selected: {{ productName }}</p>
        <button (click)="add()">Add2Car</button>
      </div>
    `,

    styles: [
      `
        div {
          border: 1px solid red;
          margin: 5px;
        }
      `,
    ],
  })
  export class ProductDetailsComponent {
    @Input()
    public name: string = '';

    @Output()
    public addEvent = new EventEmitter<string>();

    public add() {
      this.addEvent.emit('-' + this.name + '-');
    }

+   get productName(): string {
      console.log('Get ' + this.name);
      return this.name;
    }
  }

```

Si ejecutamos, podremos ver que se ejecuta 2 veces, tanto al cargar como
al cambiar el valor usando el @Input.

Esto es por el tipo de detección de cambios es por default `changeDetection`,
el cual CREO es menos probable que un cambio no se vea reflejado, pero
podemos cambiarlo a `OnPush`.

```ts
    import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, } from '@angular/core';

    @Component({
      selector: 'product-details',
+     changeDetection: ChangeDetectionStrategy.OnPush,
      ...
```

Si ejecutamos de nuevo ya solo se ejecutara una ves.

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Ciclos de vida y sus usos

- [Fuente](https://angular.io/guide/lifecycle-hooks.)

+, Evitar el uso del constructor mas allá de inicializar variables, evitar
   aquí recoger información o uso de bindings

+, OnInit lo podemos usar para recoger información

```ts
import { ... OnInit  } from '@angular/core';

...
export class ProductDetailComponent implements OnInit

  ngOnInit(): void {
    console.log('Name is ${this.name} in the ngOnInit');
  }
```

+, ngOnDestroy() Podemos usarlo para limpiar timers and intervals,
   Unsubscribing from observable streams

+, ngOnChanges(), podemos usarlo para rastrear cambios `@Input()`, abajo podemos
   capturar el viejo y nuevo valor.

```ts
    import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, } from '@angular/core';

    @Component({
      selector: 'product-details',
      template: `<div><p>Selected: {{ name }}</p><button (click)="add()">Add2Car</button></div>`,
      styles: [`div { border: 1px solid red; margin: 5px; } `, ],
    })
+   export class ProductDetailsComponent implements OnChanges {
      
      @Input()
      public name: string = '';

      @Output() public addEvent = new EventEmitter<string>();

      public add() { this.addEvent.emit('-' + this.name + '-'); }

+     ngOnChanges(changes: SimpleChanges): void {
        const product = changes['name'];
        const oldValue = product.previousValue;
        const newValue = product.currentValue;
        console.log('Product changed from ' + oldValue + ' to ' + newValue);
      }
    }
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Directives and Pipes

#### Que es un pipe

Tubería: igual que en linux bash, estos transforman información

#### Que es una directiva

Nos permiten manejar funcionalidad ambigua, como cambiar apariencia o
comportamiento de elementos HTML o el DOM.

Pueden estar asociados a componentes, al dom, o atributos

Para crearlas/usarlas necesitamos el CommonModule

Ejemplo de directivas (accedemos a ellas usando *)

- *ngIf
- *ngFor
- *ngSwitch

#### Ejemplo ngIf

Abajo podemos agregar o eliminar un elemento del dom usando ngIf

```ts
import {Component,Input,Output,EventEmitter,OnChanges,SimpleChanges,} from '@angular/core';

@Component({
  selector: 'product-details',

  template: `
    <div *ngIf="name">
      <p>Selected: {{ name }}</p>
      <button (click)="add()">Add2Car</button>
    </div>
  `,

  styles: [`
      div {
        border: 1px solid red;
        margin: 5px;
      }`,
  ],
})
export class ProductDetailsComponent implements OnChanges {
  @Input()
  public name: string = '';

  @Output()
  public addEvent = new EventEmitter<string>();

  public add() {
    this.addEvent.emit('-' + this.name + '-');
  }

  ngOnChanges(changes: SimpleChanges): void {
    const product = changes['name'];
    const oldValue = product.previousValue;
    const newValue = product.currentValue;
    console.log('Product changed from ' + oldValue + ' to ' + newValue);
  }
}


//=====================================

import { Component } from '@angular/core';

@Component({
  selector: 'product-list',

  template: `
    <div>
      <ul>
        <li (click)="selected = 'Perro A'">Perro A</li>
        <li (click)="selected = 'Gato A'">Gato A</li>
        <li (click)="selected = 'Hamster A'">Hamster A</li>
        <li (click)="selected = 'Tortuga A'">Tortuga A</li>
      </ul>
      <product-details
        [name]="selected"
        (addEvent)="onAdd($event)"
      ></product-details>
    </div>
  `,

  styles: [
    `
      div {
        border: 1px solid blue;
      }
    `,
  ],
})
export class ProductListComponent {
  public selected: string = '';

  onAdd(name: string) {
    window.alert('You just add: ' + name);
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Ejemplo ngFor

```ts
  import { Component, OnInit } from '@angular/core';
  import Product from './Product';

  @Component({
    selector: 'product-list',

    template: `
      <div>
        <ul>
+         <li *ngFor="let p of products" (click)="selected = p">
            {{ p.name }}
          </li>
        </ul>
        <product-details [p]="selected" (addEvent)="onAdd($event)"></product-details>
      </div>
    `,

    styles: [` div { border: 1px solid blue;}` ],
  })
  export class ProductListComponent implements OnInit {
    public selected: Product | null = null;
    public products: Product[] = [];

    ngOnInit(): void {
      this.products.push(
        new Product('Perro', 100),
        new Product('Gato', 200),
        new Product('Oso', 300),
        new Product('Tortuga', 150),
        new Product('Tigre', 500),
        new Product('Hamster', 50)
      );
    }

    onAdd(name: string) { window.alert('You just add: ' + name); }
  }

```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### ng-template

Cuando aplicamos mas de un if sobre una variable, esto suele ser susceptible
a errores (algo del DRY {Don't Repeat Yourself}), por lo que en estos casos
se recomienda usar template

1, Uso de if (violación de DRY)

```html
  <div *ngIf="p">
    <p>Selected: {{ p.name }}</p>
    <p>Price: {{ p.price }}</p>
    <button (click)="add()">Add2Car</button>
  </div>
  <p *ngIf="!p">No product selected!</p>
```

1, Cambio a template, no viola DRY

- Nota: noSel se crea en el momento que ponemos #noSel

```html
+   <div *ngIf="p; else noSel">
      <p>Selected: {{ p.name }}</p>
      <p>Price: {{ p.price }}</p>
      <button (click)="add()">Add2Car</button>
    </div>
+   <ng-template #noSel>
      <p>No product selected!</p>
    </ng-template>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Ejemplo Pipe personalizada

1, Dentro de la carpeta donde queremos el pipe ejecutamos

```r
ng generate pipe sort
#Podremos ver como se crea un archivo y se actualiza nuestro modulo
```

2, Implementamos pipe

```ts
import { Pipe, PipeTransform } from '@angular/core';
import Product from './Product';

@Pipe({ name: 'pSort' })
export class SortPipe implements PipeTransform {
  public transform(products: Product[], ...type: string[]): Product[] {
    if (!products) {
      return [];
    }

    if (type[0] == 'price') {
      products = SortPipe.orderByPrice(products);
    }

    if (type[0] == 'name') {
      products = SortPipe.orderByName(products);
    }

    return products;
  }

  private static orderByName(products: Product[]): Product[] {
    products.sort((p1: Product, p2: Product) => {
      const smaller: number =
        p1?.name == p2?.name ? 0 : p1?.name > p2?.name ? 1 : -1;
      return smaller;
    });

    return products;
  }

  private static orderByPrice(products: Product[]): Product[] {
    products.sort((p1: Product, p2: Product) => {
      const smaller: number =
        p1?.price == p2?.price ? 0 : p1?.price > p2?.price ? 1 : -1;
      return smaller;
    });

    return products;
  }
}
```

3, Usamos pipe

```ts
    import { Component, OnInit } from '@angular/core';
    import Product from './Product';

    @Component({
      selector: 'product-list',

      template: `
        <div>
          OrderBy:
+         <select (change)="see($event)">
            <option>name</option>
            <option>price</option>
          </select>
          <ul>
+           <li *ngFor="let p of products | pSort : orderBy" (click)="selected = p">
              {{ p.name }} - {{ p.price }}
            </li>
          </ul>
          <product-details [p]="selected" (addEvent)="onAdd($event)"></product-details>
        </div>
      `,

      styles: [
        `
          div {
            border: 1px solid blue;
          }
        `,
      ],
    })
    export class ProductListComponent implements OnInit {
      public selected: Product | null = null;
      public products: Product[] = [];
      public orderBy: string = 'name';

      constructor() {}

      ngOnInit(): void {
        this.products.push( new Product('Perro', 100), new Product('Gato', 200), new Product('Oso', 300), new Product('Tortuga', 150), new Product('Tigre', 500), new Product('Hamster', 50), new Product('Aguila', 250), new Product('Zorro', 170) );
      }

      onAdd(name: string) {
        window.alert('You just add: ' + name);
      }

      public see(ev: any) {
+       this.orderBy = ev.target.value;
      }
    }
//=================

```

Resultado:

![02_create_and_use_pipe](./img/02_create_and_use_pipe.png)

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Ejemplo Directiva personalizada

1, Crear directiva con comando (dentro de la carpeta/modulo deseada)

```r
ng generate directive copyright
#Se debe crear archivo y ser agregado en el modulo, en la parte de declarations
```

2,Implementar directiva

```ts
import { Directive, ElementRef } from '@angular/core';
@Directive({ selector: '[appCopyright]' })
export class CopyrightDirective {
  constructor(el: ElementRef) {
    const currentYear = new Date().getFullYear();
    const targetEl: HTMLElement = el.nativeElement;
    targetEl.classList.add('copyright');
    targetEl.textContent = 'Copyright ©${currentYear} All Rights
    Reserved.';
  }
}
```

3,Usar directiva

```html
<p appCopyright>Lorem, ipsum dolor sit</p>
```

**Resultado:** Se agrega un copyright al párrafo.

TODO: Hacer directiva mas compleja

<!--

#########################################

-->

----

<!--

#########################################

-->

### Services

#### Como usar los services en Angular

A traves de DI para que los componentes solo estén conciertes de la
interface.

#### Como crear y usar un servicio

<!-- generar servicio, crear servicio -->

1, Usar el comando dentro del modulo deseado

```r
ng generate service products
#Notar que nuestro modulo no se actualizo
```

2, Implementar

- Importante notar que este @Injectable

```js
  import { Injectable } from '@angular/core';
  import Product from './Product';

  @Injectable({ providedIn: 'root' })
  export class ProductsService {
    constructor() {}

    public getProducts(): Product[] {
      return [ new Product('Perro', 100), new Product('Gato', 200), new Product('Oso', 300), new Product('Tortuga', 150), new Product('Tigre', 500), new Product('Hamster', 50), new Product('Aguila', 250), new Product('Zorro', 170) ];
    }

  }
```

3, Inyectar, unicamente incluyéndolo en el constructor, para que angular
pueda, inyectarlo debe ser dentro de otro elemento angular, como un
componente, service, directiva, etc.

```ts
    import { Component, OnInit } from '@angular/core';
    import Product from './Product';
+   import { ProductsService } from './products.service';

    @Component({
      selector: 'product-list',
      template: `
        <div>
          OrderBy: <select (change)="see($event)"><option>name</option><option>price</option></select>
          <ul><li *ngFor="let p of products | pSort : orderBy" (click)="selected = p"> {{ p.name }} - {{ p.price }} </li>
          </ul>
          <product-details [p]="selected" (addEvent)="onAdd($event)"></product-details>
          <p appCopyright>Lorem, ipsum</p>
        </div>
      `,
      styles: [`div { border: 1px solid blue; } `, ],
    })
    export class ProductListComponent implements OnInit {
      public selected: Product | null = null;
      public products: Product[] = [];
      public orderBy: string = 'name';

      constructor(
+       private srv:ProductsService
      ) {}

      ngOnInit(): void {
+       this.products = this.srv.getProducts();
      }

      onAdd(name: string) {
        window.alert('You just add: ' + name);
      }

      public see(ev: any) {
        this.orderBy = ev.target.value;
      }
    }

```

### Observables and RxJs

#### Uso básico de observable

```ts
import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'product-list',
  template: ` <h1>{{ title }}</h1> `,
})
export class ProductListComponent {
  public title: String = 'Codeado';
  public title$ = new Observable((obs) => {
    setInterval(() => {
      obs.next();
    }, 2000);
  });

  constructor() {
    this.title$.subscribe(this.setTitle);
  }

  private setTitle = () => {
    const timestamp = new Date().getSeconds();
    this.title = `Learning Angular (${timestamp})`;
  };
}
```

#### Crear key-logger con observables y RxJS

Los Observables ya se portan como observables, pero la ventaja de usar
un stream de RxJs, es que nos ofrece multiples métodos pre-fabricados.

Abajo creamos un filtro a un input para que admita solo valores numéricos.

```ts
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ProductsService } from './products.service';
import { Observable, filter, fromEvent, tap, map } from 'rxjs';

@Component({
  selector: 'product-list',
  template: `
    <input type="text" #keyContainer /> <br />
    You pressed: {{ keys }}
  `,
})
export class ProductListComponent implements OnInit {
  
  //equivalente a getElementById, parecido a getElementById
  @ViewChild('keyContainer', { static: true })
  public input: ElementRef | undefined;

  public keys = '';

  ngOnInit(): void {
    const logger$ = fromEvent<KeyboardEvent>(
      this.input?.nativeElement,
      'keyup'
    );
    logger$
      .pipe(
        map((evt) => evt.key.charCodeAt(0)),
        filter((code) => (code > 31 && (code < 48 || code > 57)) === false),
        tap((digit) => (this.keys += String.fromCharCode(digit)))
      )
      .subscribe();
  }
}
```

#### Usar observable en template

Usar un observable en lugar de aun arreglo normal, es que con el observable
es mas fácil manejar fuentes asíncronas, como lo es http

```ts
    import { Component, OnInit } from '@angular/core';
    import Product from './Product';
    import { ProductsService } from './products.service';
    import { Observable, of } from 'rxjs';

    @Component({
      selector: 'product-list',

      template: `
        <div>
          OrderBy:<select (change)="see($event)"><option>name</option> <option>price</option></select>
          <ul>
+           <li *ngFor="let p of (products$ | async)! | pSort : orderBy" (click)="selected = p">
              {{ p.name }} - {{ p.price }}
            </li>
          </ul>
          <product-details [p]="selected" (addEvent)="onAdd($event)" ></product-details>
          <p appCopyright>Lorem, ipsum dolor</p>
        </div>
      `,
    })
    export class ProductListComponent implements OnInit {
      public selected: Product | null = null;
      public products$: Observable<Product[]> = of([]);
      public orderBy: string = 'name';

      constructor(private srv: ProductsService) {}

//ref{a22cf4f9f9n}
      ngOnInit(): void {
+       this.products$ = this.srv.getProducts();
      }

      onAdd(name: string) {
        window.alert('You just add: ' + name);
      }

      public see(ev: any) {
        this.orderBy = ev.target.value;
      }
    }

```

### Comunicación HTTP

#### petición fetch a observable

Por default las peticiones fetch están basadas en promesas, pero como
vimos podemos facilitarnos el trabajo usando streams.

NOTA: Este solo es un ejemplo de usar fetch en angular, pero angular ya
implementa un modulo que hace esto de forma transparente.

IMPORTANTE: Este misma aproximación se puede aplicar para cualquier otro
tipo de promesa

```ts
const request$ = new Observable(observer => {
  fetch(url)
    .then(response => {
      return response.ok ? response.text() : '';
    })
    .then(result => {
      if (result) {
        observer.next(result);
        observer.complete();
      } else {
        observer.error('An error has occurred');
      }
    })
  ;
});
```

#### Petición Http usando la aproximación oficial de angular

1, Necesitamos importar a nuestro modulo, el modulo http

```ts
    import { HttpClientModule } from '@angular/common/http';

    @NgModule({
      declarations: [ AppComponent ],
      imports: [
        BrowserModule,
+       HttpClientModule,
        ProductsModule
      ],
      providers: [],
      bootstrap: [AppComponent]
    })
    export class AppModule { }
```

2, Ya podemos crear nuestra petición

- 2,1, Usamos DI para traer la instancia que hará peticiones por nosotros
- 2,2, El modulo http pode default de angular usa

**NOTAR**: que usamos `get` ya retorna un observable, usamos pipe para
trasformar de un any a instancias de producto.

```ts
    import { Injectable } from '@angular/core';
    import Product from './Product';
    import { Observable, map } from 'rxjs';
+   import { HttpClient } from '@angular/common/http';

    @Injectable({
      providedIn: 'root',
    })
    export class ProductsService {
      private static readonly productsUrl = 'https://fakestoreapi.com/products';

+     constructor(private http: HttpClient) {}

      public getProducts(): Observable<Product[]> {
        return this.http
          .get<Product[]>(ProductsService.productsUrl)
+         .pipe(map((products) => Product.anyListToProductList(products)));
      }

      public getProduct(id: number): Observable<Product> {
        const endpoint = ProductsService.productsUrl + '/' + id;
        return this.http
          .get<Product>(endpoint)
          .pipe(map((product) => Product.anyToProduct(product)));
      }

      public addProduct(name: string, price: number): Observable<Product> {
+       const options = { headers: new HttpHeaders({ Authorization: 'myAuthToken' }) };
+       const body = { title: name, price: price };
+       const req = this.http.post<Product>(ProductsService.productsUrl, body, options)
          .pipe(map((p) => Product.anyToProduct(p)));
        return req;
      }
    }
```

3, Este método lo asignamos a otro observable en la vista, y como ya vimos
en `ref{a22cf4f9f9n}` el template los acepta de forma sencilla.

**Recordar** que gracias a que estamos usando el pipe `async`, podemos
usar el observable de forma directa en nuestro template

```ts
import { Component, OnInit } from '@angular/core';
import Product from './Product';
import { ProductsService } from './products.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'product-list',

  template: `
    <div>
      OrderBy:
      <select (change)="see($event)"><option>name</option> <option>price</option></select>
      <ul>
+       <li *ngFor="let p of (products$ | async)! | pSort : orderBy" (click)="selected = p" >
          {{ p.title }} - {{ p.price }}
        </li>
      </ul>
      <product-details [p]="selected" (addEvent)="onAdd($event)" ></product-details>
    </div>
  `
})
export class ProductListComponent implements OnInit {
  public selected: Product | null = null;
  public products$: Observable<Product[]> = of([]);
  public orderBy: string = 'name';

  constructor(private srv: ProductsService) {}

  ngOnInit(): void {
    this.products$ = this.srv.getProducts();
  }

  onAdd(name: string) {
    window.alert('You just add: ' + name);
  }

  public see(ev: any) {
    this.orderBy = ev.target.value;
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Autenticación y Autorización via http

La autenticación en este caso es simple, solo necesitamos obtener el token
usando una petición post con las credenciales del usuario, y a traves de
un Observer podemos reflejar el estado en la UI.

En cambio la autorización, necesitamos crear un interceptor, para que
de forma transparente se agregue el token de forma transparente.

##### Ejemplo Autenticación

1, Como buena practica hacemos que seguridad tenga su propio modulo

```ts
//    ng generate module auth


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//mas adelante lo crearemos, y lo exportamos para que lo use el main
import { AuthComponent } from './auth.component';

@NgModule({
  declarations: [AuthComponent],
  imports: [CommonModule],
  exports: [AuthComponent],
})
export class AuthModule {}

```

2, Creamos servicio

```ts
//ng generate service auth

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private token = '';

  constructor(private http: HttpClient) {}

  login(): Observable<string> {
    const cred = { username: 'david_r', password: '3478*#54' };
    return this.http
      .post<string>('https://fakestoreapi.com/auth/login', cred)
      .pipe(tap((token) => (this.token = token)));
  }

  get isLoggedIn() {
    return this.token !== '';
  }

  logout() {
    this.token = '';
  }
}

```

3, Creamos componente con `--export` de una vez lo incluimos en `exports[]`,
ya este modulo estará dentro de otros modulo(s), en este caso el main

**Notar:** Como le hacemos aquí para quitar o poner el botón de login/logout
lo haremos de la misma manera en otros componentes.

```ts
//ng generate component auth --export

import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  template: `
    <button
      [hidden]="authService.isLoggedIn"
      (click)="authService.login().subscribe()"
    >
      Login
    </button>
    <button [hidden]="!authService.isLoggedIn" (click)="authService.logout()">
      Logout
    </button>
  `,

  styles: [''],
})
export class AuthComponent {
  public constructor(public authService: AuthService) {}
}

```

4, Mostramos el componente en nuestro main

4,1, Incluimos el modulo de auth en el modulo main

```ts
+   import { AuthModule } from '../auth/auth.module';

    @NgModule({
      declarations: [AppComponent],
      imports: [
        BrowserModule,
+       ProductsModule,
        AuthModule
      ],
      providers: [],  bootstrap: [AppComponent],
    })
    export class AppModule {}
```

4,2, Mostramos los botones de login/logout

```ts
  import { Component } from '@angular/core';

  @Component({
    selector: 'app-root',
    template: `
      <header>
        <h1>{{ name }}</h1>
+       <app-auth></app-auth>
      </header>
      <product-list></product-list>
    `,
    styles: [`
        header {
          display: flex; flex-wrap: wrap; align-content: space-between; align-items: center; justify-content: space-between;
        }
        h1 { margin-block-start: 0; margin-block-end: 0.2em; }
    `],
  })
  export class AppComponent {
    name = 'My Store';
  }

```

5, Mostramos u ocultamos botón de comprar, en función de si el usuario
esta logueado o no.

```ts
    import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, } from '@angular/core';
    import Product from './Product';
    import { ProductsService } from './products.service';
    import { Observable, of } from 'rxjs';
+   import { AuthService } from '../auth/auth.service';

    @Component({
      selector: 'product-details',

//mostrar objeto en observable, observable condicional observable
      template: `
        <div *ngIf="p$ | async as p; else noSel">
          <p>Selected: {{ p.id }}</p>
          <p>Title: {{ p.title }}</p>
          <p>Price: {{ p.price }}</p>
          <button (click)="add(p)" style="margin-right: 2rem;">Add2Car</button>
+         <button *ngIf="authService.isLoggedIn" (click)="add(p)">Comprar</button>
        </div>
        <ng-template #noSel>
          <p>No product selected!</p>
        </ng-template>
      `,

      styles: [
        `
          div {
            border: 1px solid red;
            margin: 5px;
          }
        `,
      ],
    })
    export class ProductDetailsComponent implements OnChanges {
      @Input()
      public id: number = 0;

      public p$: Observable<Product> | null = null;

      @Output()
      public addEvent = new EventEmitter<string>();

      public constructor(
        private srv: ProductsService,
+       public authService: AuthService
      ) {}

      public add(p: Product) {
        this.addEvent.emit('-' + p.title + '-');
      }

      ngOnChanges(changes: SimpleChanges): void {
        const product = changes['id'];
        //const oldValue = product.previousValue;
        const newId = product.currentValue as number;
        if (newId <= 0) {
          this.p$ = of<any>(null);
          return;
        }

        this.p$ = this.srv.getProduct(newId);
      }
    }
```

**Resultado**: Podremos ver como controlamos en tiempo real el estado
de la UI, en función si el usuario esta logueado,

<!--

#########################################

-->

----

<!--

#########################################

-->

##### Ejemplo Autorización con interceptores

La otro alternativa a interceptores seria agregar el header de auth,
cada vez que hagamos una petición que lo requiera, por lo que violaríamos DRY.

Los interceptores nos sirven para

- Agregar un header en cada petición, por ejemplo, el token
- Agregar indicador de carga
- logging

1, dentro de `auth/` Creamos interceptor: `ng generate interceptor auth`

**Importante**: es nuestra responsabilidad filtra cuales si y cuales
no se aplicara el interceptor, dentro del interceptor

```ts
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  
  constructor(public auth: AuthService) {}

  public intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
//  if(request.url...) {
//    return next.handle(request);
//  }

    const token = this.auth.getToken();
    const authReq = request.clone({
      setHeaders: { Authorization: token },
    });

    return next.handle(authReq);
  }
}

```

2, Lo incluimos en nuestro main, ya que se ejecutan en todas las peticiones,
de todos los módulos, es nuestra responsabilidad filtrar cuales si y cuales
no se aplicara el interceptor, dentro del interceptor

```ts
    import { NgModule } from '@angular/core';
    import { BrowserModule } from '@angular/platform-browser';

    import { AppComponent } from './app.component';
    import { ProductsModule } from '../products/products.module';
+   import { HTTP_INTERCEPTORS } from '@angular/common/http';
+   import { AuthInterceptor } from '../auth/auth.interceptor';
    import { AuthModule } from '../auth/auth.module';

    @NgModule({
      declarations: [AppComponent],
      imports: [BrowserModule, ProductsModule, AuthModule],
      providers: [
+       { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
      ],
      bootstrap: [AppComponent],
    })
    export class AppModule {}

```

**Resultado:** Se agregara el header `Authorization` en nuestras peticiones.

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Navegación con routing

#### Routing en el mismo modulo (explicativo)

0, Creamos una aplicación con routing

```r
ng new my-app --routing --style=css
```

1, Veremos que tenemos el archivo `app-routing.module.ts`

1,2, Incluimos las rutas con su respectivo componente

```ts
    import { NgModule } from '@angular/core';
+   import { RouterModule, Routes } from '@angular/router';

+   const routes: Routes = [
      { path: '', redirectTo: '/products', pathMatch: 'full' } // path vacio significa default
      { path: 'products', component: ProductListComponent },
      { path: '**', component: PageNotFoundComponent }
    ];

    @NgModule({
      imports: [RouterModule.forRoot(routes)],
      exports: [RouterModule]
    })

    export class AppRoutingModule { }
```

2, Nos debemos de asegurar que este importado en nuestro modulo main

```ts
+   import { AppRoutingModule } from './app-routing.module';
    import { AppComponent } from './app.component';
    @NgModule({
      declarations: [ AppComponent ],
      imports: [
        BrowserModule,
+       AppRoutingModule
      ],
      providers: [],
      bootstrap: [AppComponent]
    })
    export class AppModule { }
```

3, Usamos el componente propio de routing, en el cual se rendeizara
nuestro componente. Este comúnmente esta en nuestro componente main

```html
<ul>
  <li><a routerLink="/products">Products</a></li>
  <li><a routerLink="/car">Car</a></li>
</ul>
<router-outlet></router-outlet>
```

**Resultado:** Tenemos una navegación SPA

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Routing dividido en módulos (comúnmente en producción)

Es casi lo mismo, solo que definimos una clase para el routing y la incluimos
en nuestro sub-modulo, de esta forma al el sub-modulo, este lo hara junto
con su routing, aplicandoce al modulo main.

**Importante:** El routing tiene una política "El que llega primero gana",
asi que debemos definir primero las mas generales y luego las mas especificas.

1, Creamos clase de routing para productos

```ts
    import { NgModule } from '@angular/core';
    import { RouterModule, Routes } from '@angular/router';
    import { ProductListComponent } from './product-list/product-list.component';

//considerar que si no se muestra tambien es por que debe tener '' que indica el principal del modulo
    const routes: Routes = [{ path: 'products', component: ProductListComponent }];

    @NgModule({

//ERROR COMUN: no confundir 'forChild' (para sub-modulos) con 'forRoot' (para modulo principal)
+     imports: [RouterModule.forChild(routes)],
      
      exports: [RouterModule],
      
    })
    export class ProductsRoutingModule {}
```

2, Importamos nuestro routing dentro de nuestro modulo

```ts
    import { ProductsRoutingModule } from './products-routing.module';

    @NgModule({
      declarations: [...],
      imports: [
        CommonModule,
+       ProductsRoutingModule
      ],
      exports: [ProductListComponent],
    })
    export class ProductsModule {}
```

3, Nos aseguramos que nuestro sub-modulo esta importado en el main-module.

```ts
    import { AppRoutingModule } from './app-routing.module';

    @NgModule({
      declarations: [...],
      imports: [
+       AppRoutingModule,
      ],
      providers: [...],
      bootstrap: [...],
    })
    export class AppModule {}
```

4, Extra: Nuestro modulo principal puede tener sus rutas en un archivo
separado

```ts
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{ path: '', redirectTo: '/list-a', pathMatch: 'full' }];

@NgModule({

//ERROR COMUN: no confundir 'forRoot' (para modulo principal) con 'forChild' (para sub-modulos)
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule],

})
export class AppRoutingModule {}

```

**Resultado:** Nuestra conf. de routing debe de implementarse, a pesar
de haberla implementado en un sub-modulo.

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Navegación imperativa

```ts
import { Router } from '@angular/router';

constructor(private router: Router) { }

goHome() {
  this.router.navigate(['/']);
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Marcar enlace como activo o actual

1, Crear en styles.css

```css
.active {
  color: black;
}
```

2, Usar con

```html
<a routerLink="/products" routerLinkActive="active">Products</a>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Pase de parámetros via url (url parameter)

1, Declaramos ruta dinámica

```ts
    const routes: Routes = [
+     { path: 'products/:id', component: ProductDetailComponent },
    ];
```

2, Creamos enlace dinámico

```html
    <li *ngFor="let product of products | sort">
      <a 
+       [routerLink]="['/products', product.id]">
          {{product.name}}
      </a>
    </li>
```

3, Capturamos parámetro de URL

- `paramMap` es un observable al cual podemos reaccionar a el, en este
  caso, capturamos el valor y con este podemos cargar un producto deseado

```ts
    import { ActivatedRoute } from '@angular/router';
    import { switchMap } from 'rxjs';

    class ... implements OnInit {
      constructor(private activatedRoute: ActivatedRoute ) { }

      public ngOnInit(): void {
+       this.product$ = this.activatedRoute.paramMap.pipe(
          switchMap(params => {
              const urlParam = Number(params.get('id'));
              return this.productService.getProduct(urlParam)
          })
        );
      }
```

#### Re-usando componentes con child-routes

El problema con usar la aproximación que hemos estando viendo, es que
el elemento se destruye y crea cada vez que entramos o salimos de el,
lo cual puede traer problemas de performance.

```ts
const routes: Routes = [
  {
    path: 'products',
    component: ProductListComponent,
    children: [
+     { path: ':id', component: ProductDetailComponent },
    ]
  },
];
```

Y para que la ruta funcione correctamente usamos `./` para indicar
una ruta relativa.

```html
<a [routerLink]="['./', product.id]">{{product.name}}</a>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Capturar parámetros url después de (?)

Recordar que

- Query parameters = ?variable=value
- Url parameters = /product/1

```ts
ngOnInit(): void {
  this.route.queryParamMap.subscribe(params => {
    console.log(params.get('sortOrder'));
  });
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Routing guard

##### canActivate

Esta aprox. se usa para poder redirigir a un usuario antes de que toque
una ruta.

**Ejemplo:** Abajo implementamos para que redirija al root, si el usuario
no esta autenticado

1, Creamos la funcion

```ts
import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from './auth.service';

export const authGuard: CanActivateFn = () => {
  const authService = inject(AuthService);
  const router = inject(Router);
  if (authService.isLoggedIn) { return true; }
  return router.parseUrl('/');
};

```

2, La incluimos en nuestro routing

```ts
    const routes: Routes = [
      {
        path: 'cart',
        component: CartComponent,
+       canActivate: [authGuard]
      },
      //ruta por default, pagina 404, 
      { path: '**', component: PageNotFoundComponent }
    ];
```

##### canDeactivate

Con esto podemos prevenir que un usuario abandone una ruta, ej, si el
usuario tiene una tarea a terminar, entonces podemos con esto pedirle
que si esta seguro que quiere cambiar de ruta.

1, Creamos guard

```ts
import { CanDeactivateFn } from '@angular/router';
import { CartComponent } from './cart/cart.component';

export const checkoutGuard: CanDeactivateFn<CartComponent> = () => {
  const confirmation = confirm('Are you sure because ...?');
  return confirmation;
};
```

2, Lo incluimos en nuestro routing

```ts
    import { checkoutGuard } from './checkout.guard';

    const routes: Routes = [
      {
        path: 'cart',
        component: CartComponent,
        canActivate: [authGuard],
+       canDeactivate: [checkoutGuard]
      },
      { path: '**', component: PageNotFoundComponent }
    ];
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Por el camino: routing

##### Pasar objeto al navegar

- [Fuente](https://stackoverflow.com/questions/36835123/how-do-i-pass-data-to-angular-routed-components)

Angular 7.2.0 introduced new way of passing the data when navigating
between routed components:

```ts
@Component({
  template: `<a (click)="navigateWithState()">Go</a>`,
})
export class AppComponent  {
  constructor(public router: Router) {}
  navigateWithState() {
    this.router.navigateByUrl('/123', { state: { hello: 'world' } });
  }
}
```

Or:

```ts
@Component({
  selector: 'my-app',
  template: `
  <a routerLink="/details" [state]="{ hello: 'world' }">Go</a>`,
})
export class AppComponent  {}
```

To read the state, you can access window.history.state property after the navigation has finished:

```ts
export class PageComponent implements OnInit {
  state$: Observable<object>;

  constructor(public activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.state$ = this.activatedRoute.paramMap
      .pipe(map(() => window.history.state))
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

##### Uso de dos outlets en el mismo template

<!-- #{el7j1a} -->

**Importante:** El link debe de indicar en que outlet se va a mostrar el
componente, de otra forma marcara error
`Error: NG04002: Cannot match any routes. URL Segment: 'xxx'`

**Fuente:** [stackoverflow](https://stackoverflow.com/questions/51223222/angular-6-multiple-router-outlets)

a,Conf de router, notar el 'outlet'

```ts
const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'example1', component: ExampleComponent, outlet: 'example' }
]
```

b,Links: notar el 'outlets'

```html
<a [routerLink]="[{ outlets: { example: ['example1'] } }]">Example</a>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Errores con routing

#### Al entrar por primera vez el redirectTo no funciona y no se renderiza primer componente

**Descripción:** Al entrar por primera vez el redirect no funcionaba, el
componente solo se renderizado si daba click sobre el link, y la url del
browser no refleja los cambios del enlace presionado.

**Causa:** Por error puse en el modulo principal `[RouterModule.forChild(routes)]`,
mientras que era: `RouterModule.forRoot(routes)`.

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Formularios

#### Formularios template-binding form

Esta es la forma de antes, yl uso de formularios reactivos ya se a popularizado.

1,Necesitamos módulo de formularios

2,Enlazamos la variable con el campo del formulario con banana en la caja [()]

```ts
//1
      import { FormsModule } from '@angular/forms';

      @NgModule({
        declarations: [...],
        imports: [
+         FormsModule
        ],
        exports: [...]
      })

//2
      <form (ngSubmit)="onSubmit()">
        <input placeholder="New price" name="price" [(ngModel)]="product.price" />
        <button type="submit">Change</button>
      </form>

```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Formularios Reactivos

Este es la forma mas usada en angular, pero es relativamente nueva.

1, Importamos modulos

```ts
    import { FormsModule, ReactiveFormsModule } from '@angular/forms';

    @NgModule({
      declarations: [...],
      imports: [
+       FormsModule,
+       ReactiveFormsModule
      ],
      exports: [...]
    })

```

2, Creamos el objeto que representa nuestro formulario

```ts
    import { FormControl, FormGroup } from '@angular/forms';

    class ... {
      
      public productForm = new FormGroup({
        name: new FormControl('', { nonNullable: true }),
        price: new FormControl<number | undefined>(undefined, { nonNullable: true })
      });

    }
```

3, Creamos método para capturar evento

- Aquí podemos ver como podemos controlar el formulario, en este
  caso limpiar el formulario

```ts
    createProduct() {
      const name = this.name.value;
      const price = Number(this. price.value);
      this.productsService.addProduct(name, price)
        .subscribe(product => {
+         this.productForm.reset();
          this.added.emit(product);
        })
      ;
    }

    get name() { return this.productForm.controls.name }
    get price() { return this.productForm.controls.price }
```

4,Ligamos con template

```html
+   <form [formGroup]="productForm" (ngSubmit)="createProduct()">
      <div>
        <label for="name">Name</label>
+       <input id="name" formControlName="name" />
      </div>
      <div>
        <label for="price">Price</label>
        <input id="price" formControlName="price" />
      </div>
      <div><button type="submit">Create</button> </div>
    </form>
```

**Resultado:** Estamos usando ReactiveForms el cual nos da potencial,y
de una forma sencilla controlamos el formulario desde nuestra clase.

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Formulario Feedback con clases de angular

Angular de forma automática pone clases css a nuestros form-controls (inputs)
los cuales podemos saber lo sig.

- `ng-untouched`: we haven't interacted with it
- `ng-touched`: we have interacted with it
- `ng-dirty`: value with something
- `ng-pristine`: value empty
- `ng-valid`: valid value
- `ng-invalid`: value is not valid

Si ponemos estilos globales que apliquen estos, entonces veremos como
sin hacer nada estos se aplican.

```css
input.ng-touched {
  border: 3px solid lightblue;
}

input.ng-dirty.ng-valid {
  border: 2px solid green;
}

input.ng-dirty.ng-invalid {
  border: 2px solid red;
}
/*
<input id="name" formControlName="name" required />
...
<input id="price" type="number" formControlName="price" required />
*/
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Validando inputs y mostar errores

Validar

```ts
    import { FormControl, FormGroup, Validators } from '@angular/forms';

    productForm = new FormGroup({
      name: new FormControl('', {
      nonNullable: true,
+     validators: Validators.required
    }),
      price: new FormControl<number | undefined>(undefined, {
      nonNullable: true,

//multiples validaciones
+     validators: [Validators.required, Validators.min(1)]
    })
    });
```

Mostrar

```html
      <input id="name" formControlName="name" required />
+     <span *ngIf="name.touched && name.invalid">The name is not valid</span>

      <input id="price" formControlName="price" required />
+     <span *ngIf="price.touched && price.hasError('required')">The price is required</span>
+     <span *ngIf="price.touched && price.hasError('min')">The price should be greater than 1</span>
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Crear validadores personalizados

1, Creamos funcion que retorne null si es valido, o retorne un any algo
que nosotros entendamos de que trata

```ts
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function priceRangeValidator(): ValidatorFn {
  return (control: AbstractControl<number>): ValidationErrors | null => {
    const inRange = control.value > 1 && control.value < 10000;
    return inRange ? null : { outOfRange: true };
  };
}
```

2, Usamos

```ts
    productForm = new FormGroup({
      price: new FormControl<number | undefined>(undefined, {
        nonNullable: true,
        validators: [
          Validators.required,
+         priceRangeValidator()
        ]
      })
    });
```

3, Mostramos

```html
<span *ngIf="price.touched && price.hasError('outOfRange')">
  The price is out of range
</span>
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Cypress And Angular

#### Errors

##### Don't use TypeScript clases in tests (.cy.ts)

In my case when I tried to use a variable from a class

```ts
//Cuidado: al parecer importar javascript hace error de
import { AuthService } from 'src/app/core/services/auth.service';
//EL error no sale cuando hago el import si no cuando lo uso por ejemplo
AuthService.ENDPOINT_LOGIN
```

show me the nex error

```r
cypress the injectable XXX needs to be compiled using the JIT compiler,
but '@angular/compiler' is not available.
```

Solution: avoid import typescript classes in cypress tests

<!--

#########################################

-->

----

<!--

#########################################

-->

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

### Mini Proyectos

#### Scroll Infinito con 3 tabs=pestañas que conserva el scroll de cada una con navegacion

**Resumen:** Para lograrlo se necesito de `#{el7j1a}` dos outlets en
el mismo template, el cual al navegar se oculta y/o muestra el otro outlet,
asi evitando que se destruya el componente de lista

Video: [03_keep_scroll_position_in_navigation_and_3_tabs.webm](./img/03_keep_scroll_position_in_navigation_and_3_tabs.webm)

Codigo: ![03_keep_scroll_position_in_navigation_and_3_tabs.zip](./img/03_keep_scroll_position_in_navigation_and_3_tabs.zip)

a,src/app/app-routing.module.ts

```ts
    import { NgModule } from '@angular/core';
    import { RouterModule, Routes } from '@angular/router';
    import { ListComponent } from './list-posts/list/list.component';

    const routes: Routes = [
      { path: '', component: ListComponent },
      {
        path: 'post/:name',
        loadChildren: () =>
          import('./post-details/post-details.module').then(
            (m) => m.PostDetailsModule
          ),
+       outlet: 'sec',
      },
    ];

    @NgModule({
      imports: [RouterModule.forRoot(routes)],
      exports: [RouterModule],
    })
    export class AppRoutingModule {}
```

b, src/app/main/app.component.html

```html
    <main>

      <div [hidden]="sec.isActivated" >
        <router-outlet></router-outlet>
      </div>

      <div [hidden]="!sec.isActivated" >
<!-- importante el name -->
+       <router-outlet name="sec" #sec="outlet"></router-outlet>
      </div>

    </main>
```

c, src/app/list-posts/list/list.component.ts

```ts
import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import PostDto from 'src/app/01_model/Post.dto';
import { PostsService } from 'src/app/02_logic/posts.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit, AfterViewInit {
  private static readonly LOAD_ON = 800;

  public popular$ = of(new Array<PostDto>());
  public recent$ = of(new Array<PostDto>());
  public trend$ = of(new Array<PostDto>());

  private _canLoadMoreForScroll = true;
  private _onTab = 0;
  private _positions: Array<number> = [0, 0, 0];
  private _lastPosition = 0;
  private _mustShowTabs: boolean = true;
  private _lastTab: number = 0;

  //used for capture scroll position only when the scrolling stops
  private _scrollTimer: any = null;

  public constructor(
    private srv: PostsService,
    private router: Router,
    @Inject(DOCUMENT) private document: Document
  ) {}

  public ngOnInit(): void {
    this.popular$ = this.srv.findPopulares();
    window.addEventListener('scroll', this.scroll, true);
  }

  public ngAfterViewInit(): void {
    this._restoreScrollOnPopNavigation();
  }

  private scroll = ($event: any): void => {
    const newPosition = $event.srcElement.scrollingElement.scrollTop;

    //Avoid do something when go top because its first time clicking tab
    if (newPosition == 0) {
      return;
    }

    //only do something when
    if (this._scrollTimer !== null) {
      clearTimeout(this._scrollTimer);
    }
    this._scrollTimer = setTimeout(() => {
      this._lastPosition = newPosition;
      this.loadMoreOnScroll(newPosition);
    }, 150);

    //do something on scroll (this executes many times)
    this.showTabsMenuOnScroll(newPosition);
  };

  private _restoreScrollOnPopNavigation() {
    this.router.events.subscribe((event: any) => {
      if (event.navigationTrigger === 'popstate') {
        setTimeout(() => {
          window.scrollTo(0, this._positions[this._onTab]);
        }, 250);
      } else if (event.navigationTrigger === 'imperative') {
        this._positions[this._onTab] = this._lastPosition;
      }
    });
  }

  public onChangeTab(ev: any): void {
    this._onTab = ev.index;
    this._saveLastTabScrollPosition();
    this._restoreTabScrollPosition();
    this._lastTab = ev.index;
  }

  private _saveLastTabScrollPosition() {
    this._positions[this._lastTab] = this._lastPosition;
  }

  private _restoreTabScrollPosition() {
    switch (this._onTab) {
      case 0:
        setTimeout(() => {
          window.scrollTo(0, this._positions[0]);
        }, 150);
        break;

      case 1:
        this.trend$ = this.srv.findTrends();
        setTimeout(() => {
          window.scrollTo(0, this._positions[1]);
        }, 150);
        break;

      case 2:
        this.recent$ = this.srv.findRecent();
        setTimeout(() => {
          window.scrollTo(0, this._positions[2]);
        }, 150);
        break;
    }
  }

  private loadMoreOnScroll(newPosition: number) {
    const totalHeight = this.document.body.scrollHeight;
    const positionCorrected = totalHeight - (newPosition + 672);
    const heightTarget = positionCorrected < ListComponent.LOAD_ON;
    if (heightTarget && this._canLoadMoreForScroll) {
      this._canLoadMoreForScroll = false;
      switch (this._onTab) {
        case 0:
          this.findMorePopulares();
          break;

        case 1:
          this.findMoreTrends();
          break;

        case 2:
          this.findMoreRecent();
          break;
      }
    }
  }

  private showTabsMenuOnScroll(newPosition: number) {
    if (newPosition > 200 && this._mustShowTabs) {
      this._mustShowTabs = false;
      var cols = this.document.getElementsByClassName(
        'mat-mdc-tab-label-container'
      ) as any;
      for (let i = 0; i < cols.length; i++) {
        cols[i].style.position = 'fixed';
        cols[i].style.top = '0';
        cols[i].style.backgroundColor = '#212121';
        cols[i].style.width = '100%';
      }
    } else if (newPosition < 200 && !this._mustShowTabs) {
      this._mustShowTabs = true;
      var cols = this.document.getElementsByClassName(
        'mat-mdc-tab-label-container'
      ) as any;
      for (let i = 0; i < cols.length; i++) {
        cols[i].style.position = 'initial';
        cols[i].style.backgroundColor = '#303030';
        cols[i].style.width = '100%';
      }
    }
  }

  public findMorePopulares(): void {
    this.popular$ = this.srv.findPopulares(true);
    this._canLoadMoreForScroll = true;
  }

  public findMoreRecent(): void {
    this.recent$ = this.srv.findRecent(true);
    this._canLoadMoreForScroll = true;
  }

  public findMoreTrends(): void {
    this.trend$ = this.srv.findTrends(true);
    this._canLoadMoreForScroll = true;
  }
}
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### RcJs sincronizar listaUI infinita con subjects (BehaviorSubject)

**Resumen:** Es importante diferenciar entre Observer el cual es efímero,
el cual nos servira como una especie de async/await.
El Subject que son los datos en si, y la diferencia entre Subject y
BehaviorSubject, es que este ultimo recuerda su estado anterior.

- [Codigo](./img/04_03myrxjs.zip)

![04_sync_view_and_rxjs_subjects](./img/04_sync_view_and_rxjs_subjects.gif)

1, Creo el subject

```ts
class View {
  public subject: BehaviorSubject<Person[]>,
}
```

2, Sincronizo con la vista

```html
    <section  id="list"  >
      <h2>Lista {{list.filter.type}}</h2>
+     <article *ngFor="let p of list.subject.asObservable() | async">
        <p>{{p.id}} - {{p.name}}</p>
      </article>
      <button *ngIf="!list.loading" (click)="list.findMore()">More</button>
      <div *ngIf="list.loading">
        Loading...
      </div>

    </section>
```

3, Cuando quiero cargar mas, uso el subject (datos) ya mostrado, y uso
el recultado del Observer que regresa el service (peticion), para
actualizar el subject

**Recordatorio** En este caso uso un `BehaviorSubject` el cual guarda
el estado anterior, y por lo tanto tengo acceso a `getValue()`, y el
`next(` lo uso para actualizar el subject, y angular solo actualizara
la vista, ya que estay usando un observer que cree a partir del subject
en este caso en el html puse `.subject.asObservable()`.

```ts
    class X {
      public findMore() {
        this.loading = true;
        this.service.onLoadMore(this.filter).subscribe((found) => {
+         const onView = this.subject.getValue();
+         this.subject.next([...onView, ...found]);
          this.loading = false;
        });
      }
    }
```
