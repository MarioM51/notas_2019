# Angular NgRx

## Installation

We could use ngrx with out effects but this is not common, so we need to
install effects too

- cmd: `ng add @ngrx/store`
- cmd: `ng add @ngrx/effects`

Note: we can also install the browser plugin `Redux DevTools` that will help us with
an UI representation of our application state.

- Plugin [link](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd)

## Order to write new state managment

It's not mandatory bus is recomended to follow this order to help us to
implement a good design

1, Start alwas with