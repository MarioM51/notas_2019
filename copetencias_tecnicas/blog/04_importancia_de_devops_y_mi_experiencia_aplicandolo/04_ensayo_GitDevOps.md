# Importancia de DevOps y mi experiencia implementándolo en artesaniastlaxco.com

- [Importancia de DevOps y mi experiencia implementándolo en artesaniastlaxco.com](#importancia-de-devops-y-mi-experiencia-implementándolo-en-artesaniastlaxcocom)
  - [1. Introduccion](#1-introduccion)
  - [2. Desarrollo](#2-desarrollo)
    - [2.1. Problemas que arregla DevOps](#21-problemas-que-arregla-devops)
      - [2.1.1. Errar es de humanos](#211-errar-es-de-humanos)
      - [2.1.2. Discrepancias en el codigo](#212-discrepancias-en-el-codigo)
      - [2.2.3. Probar grandes cambios, en vez de probar pequeños cambios](#223-probar-grandes-cambios-en-vez-de-probar-pequeños-cambios)
    - [2.3. Como implemente GitOps en artesaniastlaxco.com](#23-como-implemente-gitops-en-artesaniastlaxcocom)
      - [2.3.1. Que herramientas use y observaciones de estas](#231-que-herramientas-use-y-observaciones-de-estas)
        - [2.3.1.1. Git vs sub-version](#2311-git-vs-sub-version)
        - [2.3.1.2. Github vs GitLab vs Gitea](#2312-github-vs-gitlab-vs-gitea)
        - [2.3.1.3. Drone vs Jenkins](#2313-drone-vs-jenkins)
        - [2.3.1.4. Docker vs a pelo](#2314-docker-vs-a-pelo)
        - [2.3.1.5. Linode vs AWS](#2315-linode-vs-aws)
        - [2.3.1.6. Porkbun](#2316-porkbun)
        - [2.3.1.7. Let's Encrypt y Certbot](#2317-lets-encrypt-y-certbot)
      - [2.3.2. Flujo de trabajo](#232-flujo-de-trabajo)
        - [2.3.2.1. Pasos para implementarlo](#2321-pasos-para-implementarlo)
        - [2.3.2.2. Pasos para usarlo](#2322-pasos-para-usarlo)
        - [2.3.2.4. GitOps en base de datos](#2324-gitops-en-base-de-datos)
        - [2.3.2.3. En caso de Rollback](#2323-en-caso-de-rollback)
    - [2.5. Trabajos Pendientes y Deseados](#25-trabajos-pendientes-y-deseados)
  - [3. Conclusion](#3-conclusion)

## 1. Introduccion

La intención de este ensayo, es en tener un recordatorio personal, de porque y como implementar GitOps,
y mi experiencia al implementarlo en artesaniastlaxco.com,
por lo que esto NO pretende ser una guía, mas bien, una aproximación, vistazo general y/o retrospectiva,
y porque mas vale una pálida tinta que una gran memoria.

En este proyecto que yo considero es de un tamaño medio-chico y un solo desarrollador, estaría de acuerdo que usar GitOps es sobre-ingeniería,
pero una de mis principales motivaciones en este proyecto, es poner a prueba mis habilidades, asi que me doy el lujo de ignorar la viabilidad del proyecto en términos de tiempo.

Hay varias formas de implementar GitOps, de hecho, antes de este lo hice usando Jenkins y GitLab, pero esto requiere un servidor de al menos 3gb de ram, y usando Gitea y Drone con 1gb de ram me sobra.

## 2. Desarrollo

### 2.1. Problemas que arregla DevOps

#### 2.1.1. Errar es de humanos

Parte de la filosofía de DevOps, es la reducción de la posibilidad de cometer errores, ya que el humano tarde o temprano va a cometerlo.
Asi que, si algo lo puede hacer una computadora, que lo haga la computadora, por que su probabilidad cometer un error es mucho menor.

Uno de los requisitos de DevOps es la **idempotencia**, que es la capacidad de obtener un mismo resultado, a pesar de hacerlo varias veces, algo que en lo que las computadoras son buenas.

#### 2.1.2. Discrepancias en el codigo

Algo que vi y he escuchado, es que incluso grandes empresas tienen su codigo des-sincronizado entre ambientes,
es decir, hay codigo en test que nunca va hacia producción,
o peor aun, hay codigo que esta en producción pero no esta en test, y esto pasa porque comúnmente al no usar DevOps, lo que se hace es, usar una computadora personal, donde se genera el ejecutable, y este se sube a producción.

Esto permite que se suba codigo que nunca se subió al repositorio central, y justificadamente podríamos decir que es culpa del programador, ya que no subió el codigo al repositorio central, pero ley de Murphy, si algo puede salir mal, eventualmente saldrá mal.

Una forma de imposibilitar este error es con DevOps, el cual dejamos la responsabilidad de generar el ejecutable a un servidor a partir del repositorio central, y este que lo suba a producción.

#### 2.2.3. Probar grandes cambios, en vez de probar pequeños cambios

Hay veces que se suben cambios solo por tenerlos guardados,
y esto provoca tener un desfase moderado o grande entre el codigo en el repositorio central y test o producción, lo cual va en contra de la filosofía de DevOps.

Y lo mismo, justificadamente podríamos decir que es culpa del programador, por no gatillar el despliegue del ejecutable, pero ley de Murphy.

Para imposibilitar este error, hacemos que cada vez que se suba codigo al repositorio central, se gatille el despliegue del ejecutable a producción o a test, o al ambiente previo correspondiente.

Y sonara raro pero, es por esto usar DevOps no es suficiente, asi que es mejor usar GitOps, o como a mi me gusta llamarle GitDevOps.

### 2.3. Como implemente GitOps en artesaniastlaxco.com

#### 2.3.1. Que herramientas use y observaciones de estas

Mi intención no es descubrir o explicar si una herramienta es mejor que la otra, solo una comparativa, ya que al final solo quiero lograr implementar GitDevOps, es mas si regalaran la ram, yo me hubiera quedado con Jenkins y Gitlab, por que era lo que yo usaba en un proyecto anterior.

##### 2.3.1.1. Git vs sub-version

Esta sera la única excepción, Git es simplemente mejor que sub-version.

Algo importante a tener en cuenta, al usar git junto con DevOps, es que debemos de usar multiples ramas.

En mi caso me estoy basando en el git-flow de Vincent Driessen, el cual en resumen, es tener una rama persistente por cada ambiente,
las cuales su contenido, es usado para construir el ejecutable de su ambiente que le corresponde,
y tener multiples ramas temporales, que son donde se implementan, y luego se combinan con las ramas de ambiente, en el gráfico de abajo tal vez se explica mejor.

![Git-flow](./img/01_gitflow.png)

También, por cada una de estas ramas de ambiente debemos, de poder diferenciar en que modo las ejecutamos, si en debug o e en release.

Es decir, si al estar haciendo pruebas de la rama producción en nuestro local, el ejecutable debe conectarse a la base de datos local, no a la de producción. Y cuando ejecutamos producción en el servidor productivo debe de conectase a la base de datos de producción.

Por ejemplo, Yo lo hago declarando una variable de entorno, por decir algo HOST="debug" en mi maquina local o HOST="release" en el servidor, de esta manera el ejecutable sabra diferenciar.

Esto con la finalidad de, usando el mismo codigo poder obtener diferentes comportamientos, según en la maquina que lo ejecutemos.
Ya que si no podemos lograr esto, entonces pasaremos un mal rato cambiando variables globales, tener conflictos al combinar ramas, o llegar a tener la idea compleja/errónea de tener ramas como test_local y test_release.

##### 2.3.1.2. Github vs GitLab vs Gitea

Github es de codigo cerrado y no tiene opción para self-hosting, lo cual me da mucha desconfianza, que la principal pagina de codigo abierto, sea de codigo cerrado y tenga una naturaleza centralizada, a pesar de la naturaleza des-centralizada de git.

GitLab, la ultima vez que levante GitLab en mi servidor, por solo tenerlo levantado me consumía 1.3gb de ram.

Gitea, no tiene ninguno de estos tres problemas, es de codigo abierto, tiene self-hosting, y solo consume 100 y pico megas de ram.

Algo que me gusto de Gitea, es que solo es un servicio de git,
no pretende ser un todo en uno como Github o GitLab, lo cual incluyen cosas que uno nunca va usar, pero Gitea lo compensa, pudiéndose conectase a servicios mas especializados usando OAuth y/o WebHooks, como lo es en mi caso Drone, pero hay otros, como Hugo, Slack, etc.

Lo que no me gusto de Gitea fue,

- Uno se confunde con Gogs, ya que Gitea es un fork de Gogs, pero entiendo es lo mismo, yo solo escogí Gitea por que su comunidad es mas activa.
  
- Es menos robusto/maduro que Gitlab/GitHub, pero como dije, a traves de OAuth o webHooks, podemos obtener incluso una robustez mayor, ya que cada servicio se concentra hacer lo suyo.
  
- Por lo tanto, se podría decir que Gitea es mas complejo, pero si ya te las arreglas para hacer self-hosting con o sin Docker, ya tienes la capacidad de crear algo robusto con este.

##### 2.3.1.3. Drone vs Jenkins

Drone me consume unos 25mb de ram y Jenkins me consumía 1gb, pero Jenkins es mucho mas maduro y robusto que Drone.

Un truco de seguridad, que me gusto de Drone, (y seguro igual se puede en Jenkins)
es la capacidad de usar secretos de una forma mas segura, el cual en mi caso uso archivos .ini para guardar contraseñas,
pero que estén ahi, es que una riesgo que se filtre este archivo, y por ende nuestros secretos.

Asi que para **imposibilitar este riesgo de seguridad** es, no tener las contraseñas de producción o test en el archivo .ini,
si no que cada vez que se haga un despliegue, hacer que Drone ponga las contraseñas de forma temporal en el .ini,
y ya una vez que se usaron se borran.

Drone en automático guarda las contraseñas de forma encriptada, y las des-encriptan al ponerlas en el archivo .ini, asi que aunque se filtren los encriptados, incluso un buen hacker pasara un mal rato.

##### 2.3.1.4. Docker vs a pelo

Yo tengo una relacion de amor odio con Docker, pero en resumen vale la pena atravesar por la complejidad extra de Docker, ya que sus ventajas son muchas, como lo es estabilidad, portabilidad, seguridad, etc.

Por ejemplo, un truco que me gusta de Docker, es su resolución de DNS automática entre contenedores, es decir, en lugar de usar IPs se pueden usar alias de contenedor para conectar aplicaciones entre si, por ejemplo, en lugar de decir que el backend se conecte a la IP 1.1.1.1, se le dice que se conecte a *my_database_app_x_test*.  
Con este simple truco (jaja), **imposibilitamos el error de** por accidente conectar erróneamente ambientes que no corresponden entre si, por ejemplo, conectar el backend de test con la bd de producción, lo cual puede causar un daño grave.

##### 2.3.1.5. Linode vs AWS

Linode es mas claro y económico, ya que en mi caso AWS, dicen ser mas económicos pero por tarifas flexibles e ignorancia o su alta complejidad, yo termine pagando de mas.

Lo que me gusta de Linode o en general las IaaS, es que creas tu servidor linux, y por medio de ssh lo configuras, asi que si sabes manejar linux sabes manejar IaaS.

En cambio las soluciones SaaS o PaaS, cada empresa tiene sus variantes, por lo que si sabes usar un SaaS de AWS, no necesariamente sabes usar el SaaS de Azure o alguna otra.

##### 2.3.1.6. Porkbun

leyendo experiencias de otros, es de los mas recomendados, por que son/tienen;

- Certificación ICANN: Es algo asi como un nombrador de dominios directo, ya que hay otros que son intermediarios.
- Claros: Hay otros sitios que dan ofertas de tales dominios, están baratos, pero tienen en letras pequeñas, que el segundo año ya cuesta el doble o mas, y aquí no eh visto que lo hagan.
- Privacidad: Hay formas de mandarle un comando `who is` a un dominio, y hay unos que entregan mucha información por default, mientras que Porkbun no da nada.

##### 2.3.1.7. Let's Encrypt y Certbot

Es gratis pero dura 3 meses, asi que use Certbot para cambiar el certificado https de forma automática.  
Y esta estrategia ya lo usan muchas paginas, y grandes como AWS, Cisco, etc.

#### 2.3.2. Flujo de trabajo

##### 2.3.2.1. Pasos para implementarlo

En la mayoría de herramientas, para saber como usarlas use la documentación oficial de la herramienta, y ni me esforzaré en dar detalles, ya que estos incluso cambian con el tiempo.

1, Con Linode, crear un VPS e instalar Docker.

![Linode](./img/02_linode.png)

2, Con Porkbun, configurar DNS para mandar el dominio al servidor Linode. (no fue el segundo paso pero asi me hubiera gustado, para ahorrarme trabajo)

- Nota: Porkbun solo dirige a artesaniastlaxco.com, sin el www, asi que tengo un servicio que escucha en artesaniastlaxco.com y redirige a www.artesaniastlaxco.com

![Porkbun](./img/03_porkbun.png)

3, Configurar HTTPS con Let's Encrypt y Certbot. para hacer esto necesitamos el nombre de dominio ya hecho.

- Ver la documentación oficial

![Let's Encrypt](./img/04_lets_encrypt.png)

4, Levante instancia de Gitea con docker, y por seguridad limite el acceso al repositorio, solo usando llaves ssh.

5, Levante instancia de Drone con docker, e instale un shell runner

- Me hubiera gustado mas un docker runner, pero para reducir la complejidad, lo hice asi, ya que necesitaba contenedores DinD.
- El runner es para Drone tenga acceso a la terminal.

![Gitea and Drone](./img/05_getea_drone.png)

6, Cree un makefile por cada capa (base de datos, backend, frontend), para realizar tareas como, compilar, desplegar, levantar, etc.

- Por ejemplo

```make
#bd/makefile
db_start:
  docker run --name $(container_name) --rm --detach \
    -v $(path_volume):/var/lib/postgresql/data \
    -p ${PORT}:5432 \
    --network $(NET) \
    -e POSTGRES_DB=$(NAME) \
    -e POSTGRES_USER=$(USER) \
    -e POSTGRES_PASSWORD=$(PASS) \
    postgres-alpine

#back/makefile
back_start:
  docker run --detach --rm --name $(name_container) \
    -e APP_HOST=remote \
    -v $(path_dist):/dist \
    -v $(path_certs):/certs \
    -w /dist \
    -p $(PORT):$(PORT) \
    --network $(NET) \
    alpine \
    ./$(exe_name)

#back/makefile
build_front:
  #Compile tailwind styles
  $(cmd_node_container) \
    ./node_modules/tailwindcss/lib/cli.js --minify \
      --config ./tailwind.config.cjs \
      --input ./src/input.css \
      --output $(path_build)/tailwin.css
```

7, Por cada capa cree un pipeline en forma de .drone.yml, el cual usa los pasos definidos en el makefile.

- Por ejemplo.

```yml
#bd/.drone.yml
kind: pipeline
type: exec
name: database-change

steps:
- name: migrate_up
  environment:
    USER:
      from_secret: USER
    PASS:
      from_secret: PASS
  commands:
  - echo "\r\nUSER=$USER\r\nPASS=$PASS" >> .ini
  - make dbStop
  - make dbStart
  - make dbMigrateUp
  - make dbRemoveSecrets

- name: migrate_down
  commands:
  - make dbMigrateDown
  when:
    event:
    - rollback
```

8, Crear rama producción y test, y subirlas a Gitea, con sus respectivas variables de conexión.

![Git branches](./img/06_git_branches.png)

9,  Conecte Drone y Gitea, para que Gitea le mande un evento de que ocurrió un git push, usando un webhook, y asi gatillar el pipeline.

10, Asegurar que ambas ramas se despliegan, en su ambiente correspondiente.

![07 Webhook Pipeline](./img/07_webhook_pipeline.png)

##### 2.3.2.2. Pasos para usarlo

Pasos para desplegar

1, Creo una rama a partir producción.

- por ejemplo, *f_login* (f de feature, leer el GitFlow mencionado)

![07 Webhook Pipeline](./img/08_using_1.png)

2, Hago los cambios correspondientes en la rama temporal, y al terminar hago commit en dicha rama.

- por ejemplo, en *f_login* hago el codigo que implementa el login, con sus respectivos commits (círculos)

![07 Webhook Pipeline](./img/08_using_2.png)

3, Me paso a la rama de ambiente, y traigo los cambios de la rama temporal a la rama de ambiente previo.

- Por ejemplo, me paso a la rama test, estando en test traigo los cambios de la rama *f_login*
- De hecho en el merge uso las banderas `--no-commit` y `--no-ff`, y usando el IDE, en mi caso vscode, veo que cambios se van a aplicar a la rama test.

![07 Webhook Pipeline](./img/08_using_3_merge.png)

4, Ya con los cambios de la rama temporal dentro de la rama de ambiente previo, hago el push a Gitea, el cual le avisara a Drone y se verán implementados los cambios en el ambiente previo.

- Por ejemplo, ya que la rama test tiene los cambios de la rama f_login, hago push a Gitea y este le notificara a Drone y este desplegara el nuevo ejecutable con el cual tiene el login implementado, en test.

![07 Webhook Pipeline](./img/08_using_4_deploy.png)

5, Se hacen ls pruebas en el ambiente previo, y se inspecciona si la implementación no tira el ambiente o haga que funcione mal, o si se decide que aun faltan mas cosas, etc.

6, Ya que el cambio se le da visto bueno en el ambiente previo, los cambios se pasan a producción, y se hace lo mismo, se usan las banderas `--no-commit` y `--no-ff`, para ver que están entrando solo los cambios esperados, se hace el commit, y al final el push a Gitea y Drone hará el resto, y listo, tenemos un cambio probado y desplegado en producción.

![07 Webhook Pipeline](./img/08_using_4_deploy_prod.png)

##### 2.3.2.4. GitOps en base de datos

Este es un tema extenso, el cual mejor hare otro articulo concentrándome en este.

Pero en resumen uso scripts con soda-fizz que tienen el DDL-SQL para hacer las migraciones, ya sea para arriba o para abajo, y drone tiene la posibilidad de levantar un evento deploy o rollback, el cual se pude diferenciar, si es deploy, ejecuta *script_a.up.fizz*, y en caso de ser un rollback, entonces ejecuta *script_a.down.fizz*, de hecho se pude ver arriba en el archivo `bd/.drone.yml`.

##### 2.3.2.3. En caso de Rollback

Hay despliegues que salen mal, y para dar un paso atrás, lo que yo uso es un, `git revert` el cual no borra los cambios, si no que, pasa lo hay en un paso anterior y lo pasa al frente.

```r
        revert
          |      
1 -> 2 -> 3 -> 2 -> 3b -> 4
      \       /
        -----
#En este caso "2" estaba funcionando bien,
#luego al desplegar "3" salio algo mal,
#se hizo revert y se despliega de nuevo lo que hay en "2",
#"3b" representa la corrección de "3",
#"4" representaría que se regresa al flujo normal de trabajo.
```

De esta forma queda registrado que se hizo un revert, y permitiendo ver que se hizo para corregirlo.
Y también re-usamos el pipeline de despliegue normal.

Si esto no funciona tenemos los respaldos. pero hacer un rollback usando respaldo es una tarea mas compleja, la cual solo recomendaría en casos de emergencia.

### 2.5. Trabajos Pendientes y Deseados

Pendientes

1. Subir o descargar respaldo en automático, ya que lo hago de forma manual.
2. Usar una app de android, para revisar la salud del servidor y/o cada capa, cada x tiempo.
3. Incluir la ejecución de test unitarios en el pipeline.
4. Mayores medidas de seguridad, ya que hay bots constantemente atacando el servidor.
5. Autorizaciones de desarrolladores, para que no todos puedan desplegar a producción.

Deseados

1. Uso de Kubernetes, para manejar un cluster de servidores.
2. Separación de codigo por módulos y/o usar git-sub-modules, para poder actualizarlos de forma automática, como lo hacen las imágenes de docker o librerías de node, por ejemplo, de v1.1.^, y en automático se actualize de 1.1.4 a 1.1.5.

## 3. Conclusion

Implementar GitOps es complejo, pero no necesariamente tanto (hablando de proyectos mediano-chicos), y si tomamos en cuenta sus beneficios, creo vale la pena dedicarle un momento para implementarlo.

Drone y Gitea, son una alternativa mas ligera a Gitlab y Jenkins, o mas independiente a Github-actions

Es recomendable tener ya listo el DNS y https configurados, antes de implementar DevOps.
