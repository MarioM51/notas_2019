# Importancia del codigo limpio y mi experiencia aplicándolo

## A. TDD

Algo que sin duda marca una diferencia entre desarrolladores, son los que
no aplican TDD y aquellos que lo hacen, y creo lo digo sin ningún sesgo,
ya que incluso yo no aplico TDD en toda la extension de la palabra, ¿asi
que me estoy dando un tiro en pie?, si y no, si porque estoy admitiendo
que no trabajo aplicando las herramientas necesarias, y no porque,
realmente pocos usan TDD o pocos de ellos lo aplican de una forma adecuada
(al menos lo que yo he visto entre compañeros de trabajo y de estudio),
y esta es una cruda verdad la cual pocos quieren hablar de esto, incluso
haciendo cita.

> Even without refactoring, writing good tests increases my effectiveness
> as a programmer.
> This was a surprise for me and is counterintuitive for most programmers.  
> /Traducción/  
> Incluso sin factorización, escribir buenos tests incrementa mi efectividad
> como programador. Esto fue una sorpresa para mi y es contraintuitivo para
> la mayoría de los programadores.  
> ̣- Martin Fowler

Esto quisiera haberlo entendido antes o que me lo hubieran enseñado en
la universidad, porque ese es mi pretexto y el de otras empresas para no
aplicar TDD, e incluso al principio es verdad que escribir
pruebas unitarias te hace mas lento, pero la verdad es que las pruebas
unitarias son la base de un buen software.

### A.1. Aplicaciones CRUDy y TDD

CRUD (Create, Read, Update, Delete) muchas aplicaciones en mayor o menor
medida son de este estilo, su trabajo es; crear, leer, actualizar y eliminar
cosas de una base de datos, y las aplicaciones que la mayoría de su
trabajo es hacer esto, se les llama aplicaciones CRUDy.  
Y en pruebas unitarias hay una regla de probar funcionalidad propia y
no externa, pero esto se contradice con las aplicaciones CRUDy, ya que
gran parte de su funcionalidad es depender de esta funcionalidad externa,
y esto me confunde por que en teoría ya estariamos callendo en pruebas
de integracion pero estas son caras. Y lo que realmente estamos testeando
es el estado de la BD, si tomamos en cuenta el codigo de abajo.

```ts
class UserDBDAO {
  public readAllUser(): []User {
    const resp = dbConn.query("SELECT * from users");
    const users = User.castList(resp);
    return resp;
  }

  public saveUser(newUser:User): User {
    const resp = dbConn.exec("INSERT INTO users VALUES(%v....", newUser....);
    newUser.id = resp.id;
    return newUser;
  }

}
```

¿Como probar que Al guardar?

1, Como saber que efectivamente se guardo, según un determinado esquema.

- Se que podemos falsear a `dbConn` para obtener un comportamiento predefinido,
  pero ¿como sabemos que ese comportamiento predefinido sera siempre mismo?
  ya que el esquema es algo cambiante y externo.

2, Como saber que efectivamente fallará de una determinada manera,
según un determinados registros.

- Lo mismo los registros es algo cambiante, entonces de que nos sirve
  decirle a un mock que se comporte de x manera, si este comportamiento
  puede variar con respecto a la dependencia externa,

Conclusion, en aplicaciones CRUDy no tenemos de otra que hacer pruebas
de integración, y creo que no hay que temer tanto por su costo, porque
ya contamos con docker, el cual de manera sencilla podemos predefinir
un esquema y unos registros predefinidos para obtener un comportamiento
esperado.
La observación es que estos tests de integración es importante que solo
prueben esta integración.
