# Renderizado

## Diferentes tipos de renderizado

El renderizado en el ámbito del desarrollo web, es la acción de ejecutar
el codigo para entregar una vista al usuario.

Por ejemplo, al decirle a alguien que dibuje un borrego,
se podría decir que "dibujar un borrego" es el codigo a ejecutar, y la persona que
dibuja este borrego es la computadora, y este dibujo puede variar, ya que
puede ser entregado a una persona que ve o a una que no, o incluso a otra
persona que lo va a terminar de hacer.

A continuación hablare de documento NO renderizado y documento renderizado, el
cual, el primero seria como el dibujo para la persona que si ve, mientras
que el segundo para la persona ciega.

Este es un tema complejo, y no por que sea difícil, si no porque existen
multiples aproximaciones, cada una con sus ventajas y desventajas según
el tipo y tamaño de la aplicación.

En el caso que presento en el punto **3**, puede ser logrados con
renderizado del lado del servidor, con herramientas como php, jsp,
asp, go, etc. y lo mismo para el punto **4**, solo que se agrega un
generador de codigo javascript como webpack o rollup.

Hay soluciones mas especializas para este tema, que facilitan este trabajo
a cambio de mitificarlo, las cuales detallare mas adelante.

### 4.3. SPAs con renderizado en el servidor

Esta solución trata de, entregar un documento u otro, según quien haga la petición,
por ejemplo, si quien hace la petición es el google bot, entonces se
entrega un documento ya renderizado, en caso de ser un navegador, se
entrega la un documento sin renderizado.

Por ejemplo para Nginx, su configuración luciría de las siguiente manera.

```r
location / {
    if ($http_user_agent ~ Googlebot) {
        rewrite ^/(.*)$ /forgooglebot break;
    }
    root /usr/share/nginx/app;
    try_files $uri $uri/ /index.html;
}
```

donde `/index.html` significaría un documento sin renderizado,
y `/forgooglebot` podría significar dos cosas.

Uno, un documento que no forme parte de la aplicación del cliente, como
el caso de un sitemap.xml, o incluso un documento .rdf.

dos, un pre-renderizado de nuestra SPA, el cual necesitaríamos headless
chrome como [prerender/prerender](https://github.com/prerender/prerender),
el cual es como tener un navegador que haga el trabajo del navegador del
cliente, pero del lado del servidor.

Ambas a mi parecer son buenas aproximaciones, pero en ambas se necesita
estar vigilando dos respuestas diferentes, del cliente y la del bot, y
me parecen una solución mas compleja y aun poco madura.

Esta parte la tengo como pendiente de probar, asi que no puedo
hablar mal o bien de esta aproximación, solo indicar que existe
esta posibilidad.

### 4.4. Uso de herramientas como NextJs y NuxtJs

Estas herramientas son un punto medio, que a diferencia de usar, por
decir algo php y jquery, en las cual en php renderizamos del lado del
servidor, y con javascript del lado del navegador. En estas en una misma
base de codigo en javascript/node, podemos decidir que se renderiza en el
servidor y que en el navegador.

Lo que ami no me gusta de estas herramientas es que uno queda obligado
a usar node, lo cual tiene un rendimiento pobre, a comparación de otros
lenguajes.
