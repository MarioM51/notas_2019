# Importancia de los datos estructurados y mi experiencia aplicando en artesaniastlaxco.com

## Tabla de contenido

- [Importancia de los datos estructurados y mi experiencia aplicando en artesaniastlaxco.com](#importancia-de-los-datos-estructurados-y-mi-experiencia-aplicando-en-artesaniastlaxcocom)
  - [Tabla de contenido](#tabla-de-contenido)
  - [1. Introduccion](#1-introduccion)
  - [2. Breve Recordatorio](#2-breve-recordatorio)
    - [2.1. Que problemas solucionan](#21-que-problemas-solucionan)
    - [2.2. Soluciones propuestas](#22-soluciones-propuestas)
    - [2.3. Cuales son las ventajas y desventajas de usarlos](#23-cuales-son-las-ventajas-y-desventajas-de-usarlos)
      - [2.3.1 Como lucirían estas características avanzadas de la web3.0](#231-como-lucirían-estas-características-avanzadas-de-la-web30)
    - [2.4. La historia de las impresoras](#24-la-historia-de-las-impresoras)
    - [2.5. Preguntas frecuentes](#25-preguntas-frecuentes)
  - [3. Como luciría el stack HTML/CSS/JS/JSON-LD](#3-como-luciría-el-stack-htmlcssjsjson-ld)
    - [3.1.1 Ventajas](#311-ventajas)
    - [3.1.2 Desventajas](#312-desventajas)
  - [4. Como luciría Svelte usando JSON-LD](#4-como-luciría-svelte-usando-json-ld)
    - [4.1. Ventajas](#41-ventajas)
    - [4.2. Desventajas](#42-desventajas)
  - [5. Mi experiencia](#5-mi-experiencia)
  - [Conclusion](#conclusion)

## 1. Introduccion

HTML es estructura, CSS estilos y Javascript es programación, por lo
que falta una parte muy importante para cualquier aplicación, y esta
parte son los datos, y a pesar de que ya existen los **datos estructurados**
para solucionar esta parte, y están en distintos vocabularios como RDfa,
micro-datos y JSON-LD, que ya han sido bien probados y usados por grandes
empresas, parece ser un tema poco conocido e incluso olvidando en el mundo
del desarrollo web, tanto por el común de sus desarrolladores como aquellos
que lo enseñan.

Yo he buscado sin éxito artículos que traten los datos estructurado de
una forma clara y practica, por lo cual tuve un mal rato entendiéndolos,
y ahora que los entiendo veo que es algo bastante sencillo.

Motivado por esto escribo este articulo, y mi intención no es es definir
una nueva forma de usar los datos estructurados, eso ya se invento, mis
intenciones son las siguientes.

1. Exponerlo de una forma fundamental que explique como pueden ser usarlos.
2. Dejar claro que es mas fácil de lo que parece.
3. Motivar a aprenderlos incluso en un nivel principiante, para un internet un poco mejor.
4. Exponer mi experiencia personal usándolos en artesaniastlaxco.com.
5. Explicar que debemos entregar documentos y luego aplicaciones, para no limitar su potencial.
6. Y por que no, encontrar alguien que corrija mi aproximación de usarlos.

## 2. Breve Recordatorio

### 2.1. Que problemas solucionan

Timothy John Berners-Lee (invertor del www), dice que la web 2.0 (la actual)
esta hecha para los humanos y no para las maquinas, lo cual trae [5 problemas](https://es.wikipedia.org/wiki/Web_sem%C3%A1ntica#Retos) (amplitud, vaguedad, incertidumbre, inconsistencia y engaño)
los cuales se tuvieron en mente para que los datos estructurados
solucionen estos 5 problemas.

Y lo que es peor, el SEO insuficiente ni siquiera esta mencionado en estos 5 problemas,
ya que esto es debido a una incompetencia técnica, es decir que, el abuso de
javascript en proyectos que requieren SEO, es un error que ni debería
de estar pasando, pero en cambio pasa.

Comúnmente las paginas web están conformadas por datos no estructurados,
y no estandarizados, lo que provoca que procesar los datos de una forma
externa, tal como lo hace el google bot u otras computadoras, sea difícil
o ineficiente.

Por ejemplo, si en html tenemos un párrafo o una lista, para el google bot no sera
mas que texto, y lo único que podrá hacer es ver palabras clave, las
cuales carecerán de contexto, y una forma comprobar esto es buscar en google
"playeras **sin** rayas negras" y nos aparecerán playeras **con** rayas negras.

Por el lado del json a pesar de estar estructurado sale igual o peor,
en primera, por que comúnmente se obtiene ejecutando el javascript en el navegador
para que se muestre como html, por lo tanto el google bot para obtener
la información debe ejecutar el javascript, lo cual no siempre lo hace
y/o lo condiciona, porque si multiplicamos ese costo de procesamiento por
los millones de documentos a indexar, es un costo enorme.

Dos, aunque el html ya se muestre en el documento, para que el google
bot lo lea directo, carece de un estándar, es decir, hay muchas
maneras expresar algo.

Por ejemplo, algunos consideraran que su producto tiene color
mientras que otros no, y las propiedades pueden significar diferentes
cosas, un precio en pesos no es el mismo un precio en dólares,
o también el idioma, unos usaran chino y otros ingles.

### 2.2. Soluciones propuestas

En este articulo nos concentraremos nada mas en el problema del SEO
insuficiente, el cual usar datos estructurados seria uno de los pasos
para solucionar este problema, pero también es uno de los primeros pasos
para solucionar los 5 problemas mencionados que tiene la actual web 2.0.

En el 2011 nació la organización schema.org la cual propone varios
esquemas definidos en los vocabularios RDFa, JSON-LD y micro-datos,
los cuales actualmente ya los usan por millones de sitios entre ellos;
Wikipedia, Amazon, IMDB, mercado libre y etc.

Otra, es renderizar del lado del servidor, ya que el internet nació
siendo conjunto de documentos, y por el aumento de funcionalidades en este, la web
evoluciono a que estos documentos sean gestionados por aplicaciones,
pero parece que estos documentos fueron sustituidos por aplicaciones,
como por ejemplo Twitter o Facebook,
lo cual trae varios problemas que podríamos evitarnos, siguiendo una evolución
que no borre pasos anteriores.

Asi que repasando, al publicar información de carácter publico, no hay
que olvidar que el internet es un conjunto de documentos que son gestionados por
aplicaciones. Y es por esto que necesitamos que el servidor nos entregué
este documento, y luego ya gestionamos este documento con una aplicación.

### 2.3. Cuales son las ventajas y desventajas de usarlos

Las principales serian que solucionarían los 5 problemas de la web2.0 antes mencionados,
también estaríamos usando estructuras estandarizadas, es decir las paginas tendrían información
definida de la misma manera, lo cual traería las siguientes ventajas.

- Los motores de búsqueda pueden reconocer los datos estructurados sin
  que nosotros hagamos nada, incluso mostrando nuestra información
  directamente en los resultados del motor de búsqueda.

- Nuestra pagina podría funcionar como una base de datos, para cualquier
  otro desarrollador o aplicación, y extraer datos de forma automática.

- Damos un primer paso hacia la web3.0, permitiendo características mas
  avanzadas, ya sea implementadas por nosotros o terceros, por ejemplo, grafos de
  conocimiento en RDF, y asi poder vincular nuestra información a otros sitios
  de propósito mas general, como lo es [Linked Open Data Cloud](https://lod-cloud.net)
  y por medio de este tipo de sitios se puedan hacer consultas SPARQL con ontologías y razonadores.

- Es compatible con la nueva propuesta popular de la web 3.0, de usar
  DAPPs con blockchain.

- Dejaríamos de romper el primer principio SOLID (responsabilidad única)
  ya que actualmente, combinamos lo que es la estructura y los datos, es decir,
  el html y los datos, lo cual a pesar de que no lo parezca complica mas
  las cosas.

Una de las desventajas es que tendríamos que dar los siguientes pasos
extra en nuestros proyectos.

1. Consultar los lineamientos de schema.org
2. Adaptar nuestros datos a estos lineamientos al mostrarlos en nuestro documento html.

#### 2.3.1 Como lucirían estas características avanzadas de la web3.0

**Errata** No soy un experto en web semántica o ciencia de datos, solo
lo he tocado su superficie, asi que puede que tenga mal interpretaciones,
me baso en mi experiencia, no en fuentes validadas.

Me parece importante mencionar que estos son temas avanzados, asi que hay que tener
cuidado al quererlos dominar, ya que es un tema extenso y de carácter
matemático, ya que incluye cosas como teoría de conjuntos, estadística, y lógica difusa,
por lo que recomiendo no meterse mucho en esto, al menos que quieras
irte a algo como ciencia de datos. Y como todo estándar no contempla
muchos casos específicos, y al no ser un tema muy extendido, tendrás un mal rato aplicándolos.

Pero por otro lado, se puede decir que solo es SQL, que también tiene un carácter matemático y
muchos no se molestan en entenderlo desde esa perspectiva, y aun asi lo manejamos, con la
diferencia de que en lugar de que la fuente de datos sea una programa
como PostgreSQL, puede ser un archivo html que contiene datos estructurados.

En este caso se ilustra como luciría la consulta SPARQL, para extraer
los productos que contienen la palabra "trompo" tanto en amazon, como
también en artesaniastlaxco al mismo tiempo.

```sql
-- No es una consulta real

--Consulta
PREFIX ex: <https://schema.org/Product/>
SELECT ?name ?price ?source
FROM <https://amazon.com/products>
FROM <https://artesaniastlaxco.com/productos>
WHERE {
  { 
    ?product ex:name ?name ;
             ex:price ?price ;
             ex:source "artesaniastlaxco.com" .
    FILTER (CONTAINS(LCASE(?name), "trompo"))
  }
  UNION
  { 
    ?product ex:name ?name ;
             ex:price ?price ;
             ex:source "amazon.com" .
    FILTER (CONTAINS(LCASE(?name), "trompo"))
  }
}

--Resultado
-------------------------------------------------------------
| name                 | price  | source                 |
=============================================================
| "Collar con trompo"  | 99.99  | "amazon.com"           |
| "Trompo de llavero"  | 30.00  | "artesaniastlaxco.com" |
| "Trompo de plástico" | 79.99  | "amazon.com"           |
| "Trompo de encino"   | 150.00 | "artesaniastlaxco.com" |
-------------------------------------------------------------
```

### 2.4. La historia de las impresoras

Algo que me gusta y me recordaron los datos estructurados, es que nos hace tener
conciencia, de que ahi afuera hay estándares y que están ahi por algo.

En los años 90s o antes, cada marca de impresoras tenia su propio
formato, unos eran ligeros y rápidos, otros completos y robustos, unos
(principalmente las marcas que crearon estos) defendían que X formato
era el mejor, y otros se esforzaban de mas y aprendían varios, o al menos
los mas populares, y por esto era un lio crear aplicaciones que fueran
compatibles con todas las impresoras.

Para resolver el problema se creo un estándar al cual los formatos
debían apegarse, y ya fuera por que les convenía o se veían obligados a hacerlo,
poco a poco fueron haciéndolo. Paso el tiempo y hoy por hoy, solo queda
el estándar, Por lo que hoy mandar a imprimir es algo fácil de lograr y que
quedo en segundo plano para el desarrollo de aplicaciones.

Y como desarrollador que ya tiene mas de 8 años viendo como, cada
mes sale X cosa que es mejor que Y cosa, y que X cosa ya se actualizo
y ahora es mejor que Y cosa, tengo el anhelo de poder ser componente a ojos
de las empresas con solo aprender los estándares, y claro que estos
tienen sus deficiencias, pero prefiero lidiar con las deficiencias de
una sola herramienta, que las ventajas y deficiencias de multiples
herramientas, pero esa es otra historia.

### 2.5. Preguntas frecuentes

1, ¿Donde puede conseguir los esquemas?

- Puedes buscar todo tipo de esquemas en [schema.org](https://schema.org/docs/schemas.html),
  pero igual puedes encontrarlos en [google](https://developers.google.com/search/docs/appearance/structured-data/search-gallery)
  pero este se basa en los de schema.org

2, ¿Donde y como puedo validar mis esquemas?

- Yo suelo usar 2 validadores online tanto el de schema.org como el de google
  - validator.schema.org
  - search.google.com/test/rich-results

4, ¿necesito saber de RDF, triple stores, SPARQL, OWL, etc. para usar datos estructurados?

- No, esos son temas mas avanzados, es como decir que necesitas saber
  correr para luego poder caminar, primero debes saber de datos estructurados,
  y luego ya podrías aprender estos temas.

## 3. Como luciría el stack HTML/CSS/JS/JSON-LD

Si ya tienes experiencia en el desarrollo web ya estas
familiarizado con los con la mayoría del stack, y no debes de
asustarte, por que los datos estructurados son mas fáciles que javascript,
y tampoco es algo de la vieja escuela.

Las siguientes propuestas son ejemplos simplificados, para centrarnos en
las diferentes formas en los que podríamos usar los datos estructurados,
y de esa forma evitarnos el ruido de otras herramientas.

Y como veremos en la ultima sección, podemos usar los datos estructurados
junto con herramientas robustas y/o modernas, como en mi caso Svelte y Golang.

Abajo pongo un ejemplo muy simple de como luciría este stack, lo cual
podemos ver lo simple que es la idea en realidad, y que en formas de
trabajar mas complejas, fundamentalmente harían lo mismo.

```html
<!DOCTYPE html>
<html>
<head>
  <title>HTML/CSS/JS/JSON-LD</title>
  <script id="product-info" type="application/ld+json">
  {
    "@type": "Product",
    "identifier": "2",
    "name": "Nombre de producto",
    "description": "Descripción de producto",
    "image": [
      "/public/img/1.png"
    ]
  }
  </script>
<style>
  html { background: #131516; color: white; }
  h3 { color: blue; }
</style>
</head>

<body>
  <h3 id="p-name"></h3>
  <p id="p-description"></p>
  <div id="p-images"></div>
</body>

<script type="text/javascript">
  let elmText = document.getElementById('product-info').innerText;
  let product = JSON.parse(elmText);

  document.getElementById('p-name').innerText = product.name;
  document.getElementById('p-description').innerText = product.description;

  let pImages = document.getElementById('p-images')
  for (const imgUrl of product.image) {
    let imgElm = document.createElement("img");
    imgElm.src = imgUrl;
    pImages.appendChild(imgElm);
  }
</script>

</html>
```

Como podemos ver aquí la única parte nueva seria es el `application/ld+json`
en la parte del head.

Importante mencionar que no hay problema en que la interface de usuario,
no utilize los datos estructurados, ya que es nuestra responsabilidad que
estas dos partes coincidan, ya sea lo hagamos de una u otra forma.

### 3.1.1 Ventajas

1, El crawler de google o cualquier otro bot detectara los datos estructurados,
sin la necesidad de ejecutar nuestro javascript, y nos dará todos los
beneficios mencionados.

2, Podemos separar lo que es el la parte del servidor y del cliente,
lo cual las aplicaciones de backend y frontend estarían mas des-acopladas.

3, El funcionamiento seria bastante parecido a lo que ya se hacen las
aplicaciones que usan APIs-REST, pero desde una aproximación de renderizado
del lado del servidor, el cual ya muchos proyectos antiguos funcionan
con esta aproximación.

### 3.1.2 Desventajas

1, Es mas trabajo. Pero es un trabajo extra parecido al de usar una rest-api,
pero con el paso extra de consultar el schema.org.

## 4. Como luciría Svelte usando JSON-LD

La idea general es usar una SPA (como siempre), solo que esta misma SPA
se va a usar en diferentes documentos, y lo único que va a variar es
el json-ld que se tendrá en el head.

Lo cual nuestros documentos html lucirían de la siguiente forma.

![Files](./img/svelte_jsonld_files.png)

index.html

```html
<!-- index.html -->
<!DOCTYPE html>
<head>
  <title>Home</title>
  <script type="application/ld+json">
    { "@context": "https://schema.org", "@type": "Restaurant", "name": "GreatFood" }
  </script>
</head>
<body>
  <div id="app"></div>
  <script type="module" src="/src/main.ts"></script>
</body>
</html>
```

menu.html

```html
<!-- menu.html -->
<!DOCTYPE html>
<head>
  <title>Menu</title>
  <script type="application/ld+json">
    { "@context":"https://schema.org", "@type":"Menu",
      "hasMenuSection":{ "@type":"MenuSection", "name":"Tacos", }
    }
  </script>
</head>
<body>
  <div id="app"></div>
  <script type="module" src="/src/main.ts"></script>
</body>
</html>
```

En tanto al codigo de la aplicación, seria el mismo que cualquier SPA.

App.svelte

```html
<script>
  import { Router, Route } from "svelte-navigator";
  import LazyRoute from "./ui_commons/LazyRoute.svelte";
  const Home = () => import("./pages/Home.svelte");
  const Menu = () => import("./pages/Menu.svelte");
</script>
<Router>
  <nav><a href="/">Home</a><a href="/menu.html">Menu</a></nav>
  <main>
    <LazyRoute path="/" component={Home}>Loading Home...</LazyRoute>
    <LazyRoute path="menu.html" component={Menu}>Loading...</LazyRoute>
    <Route><h3>404</h3></Route>
  </main>
</Router>
```

Home.svelte

```html
<script lang="ts">
    import Restaurant from "../model/Restaurant";
    const rest = Restaurant.fromJsonld();
</script>
<main>
  <h3>Restaurant</h3>
  <p>Restaurant name: {rest.name}</p>
</main>
```

Restaurant.ts y Menu.ts lucen casi igual, y en esta es donde tenemos
la lectura del json-ld del documento html.

```ts
export default class Menu {
  public name: string = "";

  public static fromJsonld(): Menu {
    const jsonld = JSON.parse((
      document.querySelector('script[type="application/ld+json"]') as HTMLElement)
      .innerText);
    const resp = new Menu(); // new Restaurant();
    resp.name = jsonld.hasMenuSection.name;
    //resp.name = jsonld.name;
    return resp;
  }
}
```

Menu.svelte

```html
<script lang="ts">
  import Menu from "../model/Menu";
  const menu = Menu.fromJsonld();
</script>
<main>
  <h3>Menu</h3>
  <p>Menu de: {menu.name}</p>
</main>
```

Como resultado tendríamos lo siguiente

![Svelte + JsonLD Result](./img/svelte_jsonld_result.png)

### 4.1. Ventajas

1, Tenemos todas las ventajas de usar SPAs como lo es, desarrollo por
componentes, integración con typescript, lazy load, navegación sin recarga,
y también las de usar datos estructurados.

2, Muchos desarrolladores ya están habituados a trabajar con SPAs,
por lo que la adopción de datos estructurados seria mas fácil.

### 4.2. Desventajas

1, Este tipo de herramientas no están pensadas para ser usadas como MPAs,
por lo que se tiene que saber un conocimiento mas avanzado de herramientas
como vite, rollup, etc, para generar el directorio dist de forma correcta.

- Una vez superado el problema de la compilación, el flujo de trabajo
  es mismo, por lo que creo vale la pena.

2, Se pierde la navegación sin recarga.

1. En lo personal, la característica de navegación sin recarga es un lujo,
   el cual el usuario no tiene problemas de estar sin esto.
2. El salto casi no se nota ya que el bundle queda guardado en el cache.
3. Esta recarga solo la tendríamos en rutas en las que queremos SEO
   con datos estructurados, otras partes no seria necesario.
4. Se puede arreglar, usando tanto api-rest-json como también json-ld,
   pero es un trabajo mas elaborado.

## 5. Mi experiencia

A rangos generales lo que yo hago en artesaniastlaxco.com, es lo mismo
que explique en el punto 4.

Pero hablando mas detalladamente, en lugar de entregar una SPA entrego
componentes web, pero por esta aproximación no me gusto, porque las
mejoras de rendimiento me parecen despreciables.

Asi que lo mas seguro **para el siguiente proyecto** lo haga usando una
SPA en donde a ojos de los bots es una MPA, pero a ojos de clientes es
una SPA, mas parecido a como lo muestro en el punto 4.

De ahi es un proyecto normal, el cual uso un backend escrito en golang
el cual entrego el documento html con los datos estructurados y la
aplicación en forma de componentes web, dichos datos se consultan una
base de datos en Postgres con un modelo relacional a la medida.

Algo que me decepciono de las consultas SPARQL, es que estas son mas
complicadas de usar de lo que deberían (a mi parecer), ya que uno tiene que
indicarle si son archivos que solo contienen los datos estructurados, o
si están embebidos en archivos html o xml, y si están en un formato json-ld o RDFa,
y que sus fuentes para aprenderlo suelen ser muy académicas.

A mi me gustaría que las consultas SPARQL fueran mas parecidas a SQL,
donde incluso el cli de Postgres puede ser interoperable con CockroachDB,
donde tras bambalinas deberían de conseguir los datos de forma automática,
sin importar que uno esta usando json-ld, RDFa o micro-data, ya sea que
este contenido en un html o no.

## Conclusion

A mi parecer los datos estructurados se han satanizado, por que comúnmente
se asocian con la web semántica, el cual es un tema complejo e incluso experimental,
como también por nuevas tendencias como las de las SPAs y API-REST que
dejan su uso en segundo plano, pero esto no significa que sean complejos
o que ya no sea posible usarlos.

Lo que he aprendido de esto es que como desarrollador web, es limitarse
a publicar datos estructurados, y no a consultarlos, al menos que nos
querremos especializar en el tema.

Pero superando estas barreras, creo que cuando se trata de información
de carácter publico, usar datos estructurados es una obligación, para
mejorar el internet, ya que aunque parezca que internet no tiene problemas,
la verdad es que los tiene varias, y es nuestro trabajo como desarrolladores
evitarlas y corregirlas.
