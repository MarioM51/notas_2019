# Web Security

## How Generate a Self Signed Certificate Using Keytool?

```r
keytool -genkeypair -alias sbip -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore sbip.p12 -validity 3650 -storepass myPassword
```

## Cryptography

### Cual es la diferencia entre par de llaves simétricas vs asimétricas

Simetrica: Es cuando usamos la misma llave tanto para firmar como para validar

Asimetrica: Cuando usamos una llave para firmar y otra para validar.

