<h1 style="text-align: center">Claud</h1>

# Amazon AWS: Curso Completo Arquitecto Soluciones Certificado

## Introduccion

### 4. Qué es la computación en la nube (cloud computing)?
Son computadoras que viajan en el cielo por obra de los espiritus chocarreros

- **Modelos de Cloud Computing**
  - **Infrastructure as a Service (IaaS)**: Proporciona una interface para la creacion y modificacion 
  de un equipo en caracteristicas de hardware, proporcionando el mayor control sobre los 
  recursos.   

  - **Plataform as a service (PaaS)**: Aqui uno ya no se preocupa por las caracteristicas del equipo, 
  mas bien ofrece una plataforma para dar de desplegar los servicios que uno a fabricado.    

  - **Software as a service (SaaS)**: Comunmente se refiren a aplicaciones de usuario final, donde 
  uno ya no administra, solo se tiene que preocupar por como utilizar el software en concreto, 
  por ejemplo, un servicio de correo electronico.   


- **Modelos de implementacion en la nube**
  - **All-in Cloud:** Esta toda la aplicacion esta implementada en la nube de modo que 
  toda la aplicacion se resguarda y ejecuta en la nube.   
  - **Hibrido:** Esto significa que se tiene una infraestructura privada como una en la nube 
  que lo mas comun ampliar la infraestructura. 



### 5. Ventajas y beneficios de la computación en la nube de AWS
- Gasto de capital (Capex) por gasto flexible: No se requiere de pagar una gran invercion inicial 
  solo se debe de pagar los recursos que se consumen.   
  
- Aprovecha las economias de escala masiva: Uno aprovecha la inercia de amazon en el caso que al 
  comprar infraestrucura a gran escala le sale mas barato y por consiguiente a uno.   
  
- Eliminar adivinar tus necesidades de capacidad: Solo podemos estimar la cantidad de recursos que 
  ocuparemos, lo cual es comun que nos quedemos con alguna limitacion o sobrante de potencia, y aws 
  nos permite empezar de tan abajo o arriba y escalar hacia arriba o hacia abajo como requiramos.   
  
- Aumenta la agilidad: AWS tiene un tiempo de respuesta que a nosotros nos costaria bastante adquirir y/o implementar.   

- Dejar de gastar dinero en mantener centros de datos: un centro de datos propio cuesta, y minimo a 
corto y medio plazo recortaremos costos.   

- Despliegue global en minutos.   

Todo lo que podemos tener en un data center lo podemos tener en AWS, por ejemplo tenemos el ejemplo 
de abajo.   
![equivalente_centro_de_datos_en_aws.PNG](./img/claud/aws/equivalente_centro_de_datos_en_aws.PNG)
Aqui podemos ver distintos elementos como balanceadores de cargam servidores, almacenamiento, 
base de datos, 



### 8. Laboratorio: Instalación de putty y puttygem SOLO para usuarios windows
<!-- Crear una maquina en aws gratis, Crear un servidor en aws, Crear servidor en AWS, Crear VPS AWS, Crear un host en AWS, Crear host en AWS, Crear host aws, crear instancia aws. crear instancias aws -->
<!-- conectarse a intancia aws, conectar a instancia aws, entrar a instancia aws, hacer conexion a instancia aws, conectarse a host aws, conectar a host aws, entrar a host aws, hacer conexion a host aws, conectarse a maquina aws, conectar a maquina aws, entrar a maquina aws, hacer conexion a maquina aws -->
Ver "Amazon AWS: Crea una máquina remota para pruebas y conectarnos" en DevOps.md   


### 14. Servicios gestionados Amazon AWS vs no gestionados
Tenemos dos tipos de servicios en amazon
- **No gestionado:** El escalado, tolerancia, a fallas y la disponibilidad 
son administradas por uno, como es el caso de las instancias EC2.

- **Gestionado:** El escalado, la tolerancia a fallas y disponibilidad entan 
incorporadas en el servicio, como es el caso de los S3.   


### 15. Laboratorio: Diferencia entre servicios gestionados vs no gestionados
**Objetivo:**   
  - Crear un NAT Gateway como servicio gestionado de amazon.   
  - Detalles de los amazon Machine Image (AMI).   
  - Detalles del ecosistema de appliances en aws.   
  - Diferencias entre los servicios administrados y no administrados.   



















































<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
----
<br /><br />
Temporal






----
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />