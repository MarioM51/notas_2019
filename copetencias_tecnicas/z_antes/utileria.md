# Utileria

- [Utileria](#utileria)
  - [Windows](#windows)
    - [Comandos](#comandos)
      - [Atajos](#atajos)
      - [Modificar atajos (Shortscuts)](#modificar-atajos-shortscuts)
      - [PowerShell](#powershell)
  - [POSTMAN](#postman)
    - [capturar valores en respuesta json](#capturar-valores-en-respuesta-json)
    - [Crear API MOCK](#crear-api-mock)
  - [Google Chrome](#google-chrome)
    - [Levantar servidor local con Chrome](#levantar-servidor-local-con-chrome)
    - [Forzar modo oscuro](#forzar-modo-oscuro)
  - [Google Sheets](#google-sheets)
    - [Rellenar porcentage de celda con colores](#rellenar-porcentage-de-celda-con-colores)
  - [Visual Studio Code](#visual-studio-code)
    - [Agregar regla vertical](#agregar-regla-vertical)
    - [Habilitar java](#habilitar-java)
  - [Draw.IO](#drawio)
    - [Mostrar ocultar contenido](#mostrar-ocultar-contenido)
  - [Reparacion](#reparacion)
  - [Chocolatey](#chocolatey)
  - [Redes](#redes)
    - [Ver servidor desde local network](#ver-servidor-desde-local-network)

## Windows

### Comandos

<!-- eliminar rapido, eliminar carpeta rapido, borrar rapido, borrar archivos rapido, borrar carpetas rapido-->
Eliminar archivos rapido: `RMDIR /Q/S foldername`

Pasar nombre de archivos a txt: `dir/b>lista.txt`

Conseder acceso a archivo en carpeta y sub carpetas: `icacls . /grant Subject51:(OI)(CI)F /T`

DNS local de windows: `c:\WINDOWS\system32\drivers\etc\hosts`

iniciar programa en cmd/windows background: `START /B program`
  
Ver el cache y archivos (file system) de Chrome: `file:///C:/Users/subject51/AppData/Local/Google/Chrome/User%20Data/Default`
  
Acttivar programas en windows como telnet, linux en windows etc.

- Buscar "Turn Windows features on or off" en menu inicio de windows o en panel de control ->
  programas y caracteristicas -> "Turn Windows features on or off"

La f es para que muestre los archivos: `tree /f`

#### Atajos

Sacar captura de pantalla: `Windows + Shift + S:`

#### Modificar atajos (Shortscuts)

Podemos hacerlo con [autohotkey](https://www.autohotkey.com/) el cual
podremos hacer scripts que se ejecutan a partir de shortscuts.

Ejemplo. Modificar el [Win + Ctrl + Right/Left] para cambiar de escritorio
a [Win + Ctrl + PagUp/pagDn]

0, Debemos haber instalado el programa

1, Crear dos archivos archivo xx.ahk

```js
<#<^PgUp::
Send, #^{Left}
return

<#<^PgDn::
Send, #^{Right}
return

>^Up::
Send, {Down}
return

>^Left::
Send, {Right}
return
```

2, Le damos doble click, y las teclas deberan estar modificadas

#### PowerShell

```powershell
[convert]::ToBase64String((Get-Content D:\Images\image1.jpg -Encoding byte))
```

<!-- 

.

.

.

-->

## POSTMAN

### capturar valores en respuesta json

Ejemplo capturar un valor de la respuesta y pasarlo a
una variable global.

```javascript
let token = pm.response.json().access_token;
pm.globals.set("token", token);
```

### Crear API MOCK

- [Fuente](https://www.youtube.com/watch?v=pAD11I3k9q0)

1. Creamos una coleccion
2. A esta le damos en la flecha que tiene al lado de l nombre de la coleccion
3. Seleccionamos pestaña Mocks
4. Le damos en agregar Mock lo nombramos y crear
5. Copiamos la URL y creamos una peticion con esta url/dominio mas la ruta
6. Dentro de la peticion creamos un ejemplo, con la peticion que queremos que nos retorne
7. guardamos ejemplo, regresamos guardamos peticion y le damos en "Send"
8. Nos deberia de dar la misma respuesta que le mandamos

<!-- 

.

.

.

-->

## Google Chrome

### Levantar servidor local con Chrome
<!-- iniciar servidor local con chrome, iniciar servidor con chrome,  servidor chrome servidor, -->

1. En chrome web store Instalar la extencion para Chrome "Web Server for Chrome"
ofrecido por "chromebeat.com" [link](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb)

2. En los marcadores ir a aplicaciones

3. Seleccionamos folder, le damos iniciar, entramos en la url que nos da.

![hgf](./img/utileria/01_levantar_servidor_local_chrome.gif)

----

### Forzar modo oscuro

Podemos poner un modo escuro con la extension "Dark Reader"

<!-- 

.

.

.

-->

## Google Sheets

### Rellenar porcentage de celda con colores

- [Fuente](https://www.benlcollins.com/spreadsheets/sparklines-in-google-sheets/)

``` json
=SPARKLINE(Q211:R211;{"charttype"\"bar";"color1"\"green";"color2"\"#bb2222";"max"\24})
```

![d](./img/utileria/rellenar_porcentaje_de_celda_google_sheet.PNG)

<!-- 

.

.

.

-->

## Visual Studio Code

### Agregar regla vertical

Vamos a menu File → Preferences → Settings y agregamos

```json
"editor.rulers": [80,120]

//para editar color
"workbench.colorCustomizations": {
    "editorRuler.foreground": "#ff4081"
}
```

### Habilitar java

A. Usar la terminal para poner los comandos para compilar y correr,
por ejemplo

```bash
> javac Archivo.java

> java Archivo
salida de nuestro programa
```

B. Agregar una configuracion e instalar puglin para facilitarlo

1. Instalar el "Java Extension Pack" de "Microsoft"

2. En un archivo .java abierto ie a
`Terminal->Configure Default Build Task...->Create from template->others->`

3. Se nos abrira una pestaña con el archivo `task.json`, en el cual
   pegamos el sigiente configuracion/json. **Nota:** Acegurarse que las
   rutas existen ya que podemos tener una version o ruta diferente de
   nuestro jvm y jdk.

```json
"tasks": [
    {
        "label": "javac",
        "command": "java",
        "windows":{
            "command": "C:\\Program Files\\Java\\jdk1.8.0_201\\bin\\javac.exe"
        },
        "args": [ "${file}" ]
    },
    {
        "label": "runjava",
        "command": "java",
        "windows":{
            "command": "C:\\Program Files\\Java\\jdk1.8.0_201\\bin\\java.exe"
        },
        "args": [
            "${fileBasenameNoExtension}"
        ]
    }
]
```

**Resultado:** Tenemos 2 opciones de correr java la que nos da la
extencion donde nos aparecera un boton arriba del main para correr
el programa o entrar a `Terminal->Run Task` y seleccionar una de las
acciones que agregamos.

<!-- 

.

.

.

-->

## Draw.IO

### Mostrar ocultar contenido

Fuente: drawio-app.com/interactive-diagrams-with-custom-links-and-actions/

<!-- 

.

.

.

-->

## Reparacion

Fuente: [hirensbootcd](https://www.hirensbootcd.org/download/)

Para meterlo dentro de una usb usamos la herramienta iso2usb

## Chocolatey

Este es un gestor de paquetes como apt en linux el cual nos permite
instalar programas de linux en windows

instalar programa de linux en windows

```r
choco install XXX
```

## Redes

### Ver servidor desde local network

**Problema:** En el caso de Gin tuve el problema de que me rechasaba la conexion,
cuando trataba de conectar a mi localhost desde mi telefono, a pesar
de que podia hacer ping del cell a lap y lap a cell.

**Causa:** Levante el servicio para que escuchara desde `router.Run("localhost:80")`
lo cual al tener una peticion de fuera la ignoraba.

**Solucion:** Cambie el desde donde queria que escuchara, que es la IP
asignada en mi local network en mi caso `router.Run("192.168.1.81:80")`
