
# Plan Proyectos

## Carrito de compras

<!-- Resulto que piden licencia incluso para verciones anteriores
1. Diseño, Crear wireframe con Balsamiq
  1.1. Descargo balsamiq: https://balsamiq.com/wireframes/desktop/archives/?prefix=mockups-desktop/2.2/ al parecer 
  el 2.2.28 (ya que >3v es de pago y <2.x es gratuito) es el mas reciente para windows "E:\Progamas\Balsamiq_2.2.28.exe".   
-->

1. Diseño, Me voy a vazar en el diseño de Udemy
  1.1. Mejor usar lapiz y luego sacarle fotos para crear un proto con "Marvel - Prototipos de apps"
  
2. Front, angular y bootstrap

3. Back, Spring con micro-servicios

4. Base de Datos, procedimientos almacenados

5. Operaciones, Para despliegue uso de develop, test y produccion en docker para desplegar con jenkins

6. Mobil, Flutter

## Tienda2

1. Analisis
   - Casos de uso / BackLog
   - Diagrama de contexto
   - Happy-Paths
   - Dibujos a Lapiz de UI/UX bajo nivel
   - Pasar dibujos a Digital UI/UX medio nivel
   - Crear prototipo con Marvel
  
2. Diseño
    - Plan de desarrollo / Modulos
    - Diseño E/R
    - Diseño OOP

3. Desarrollo
   - Primero crear los json para poder empezar indistivamente del
    front o del back.
   - Crear API-Rest en Spring
   <!-- No se usara .md, file-system para objeter informacion solamente 
   la IPI para descargar informacion y multimedia -->
   - Crear Slugs para obtener la info de API.
   - Crear Front en React-Gatsby usando SASS

4. Implementacion
   - Poner aplicacion en docker Spring-React/DB y crear docker-compose
   - Subir en Heroku
