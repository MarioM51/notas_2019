
# Concepcion
+ Analisis de concepcion, motivaciones, objetivos, problemas en un powerpoint, 
  para no hacer texto de mas.
	
+ Identificar autores, que personas lo van a usar.
	
+ Creacion de pila de producto general y prototipo de bajo nivel.
	
+ Creacion de presupuesto, 
    - hacerlo en horas PENDIENTE("Buscar el proceso de calculo de horas")
		
+ Creacion de contrato 
    - Tenemos un ejemplo en: [Contrato NavarQ](./img/workflow/Ejemplo_de_contrato.pdf) <!-- file://E:\cursos\_2019\img\workflow/Ejemplo_de_contrato.pdf -->

+ Creacion de maqueta (prototipo de alto nivel) para aumentar el impacto en 
  el cliente de propuesta de solucion, es necesario una .
	
+ Propuesta de solucion y realizacion de la venta exponiendo maqueta.


# Analisis

## Comprencion del problema o meta
+ Analisis del problema: ¿Por que quieren y que impulzo la busqueda del producto o solucion?
  - Declaracion del problema u objetivo,   
  - diagrama espina pescado,   
  - Diagrama de caso de uso  *Buscar en Babok "Figura 9-11: Diagrama de caso de uso"*
  - Diagrama de contexto, Segun [libro BABOK](./img/workflow/BABOK_v2.pdf), *Buscar "Diagrama 9-13: Diagrama de contexto (Notación Yourdon)".*    <!-- file://E:\cursos\_2019\img\workflow/BABOK_v2.pdf -->) ,   
  - Diagrama de secuencia, *Buscar "Figura 9-14: Diagrama de secuencia (UML)"*
		
+ Declaracion de objetivos: ¿Que hara el producto y como los veneficiara?.   
  - ojetivos de negocio en general.   
  - objetivo del proyecto y como ayuda al objetivo general.   
	
+ Definicion del alcance:   
  - contestar a ¿como sabemos que hemos acabado en las historias de usuario?.   
  - uso de Diagrama de descomposición funcional. *Buscar Figura 9-6: Diagrama de descomposición funcional*.   
  - Matriz de Trazabilidad, enlistar que cosas se necesitan para lgrar los objetivos (Trazar).   
		
+ Lluvia de ideas: creacion de prototipos.   
  - diagrama de actividades.   
  - Pila de producto.   
  - prototipo UI bajo nivel.   
  - refinamiento EDT.   
	
+ lluvia de ideas.   
  - Para la comunicacion para llevar a cabo la elicitacion en la Empresa, y para confirmar 
    meta de Sprint_1.   


### Pasos tomados
1. Que todo el equipo entienda el problema por medio de un diagrama espina pescado fisico 
que todos tomen, enlistar los problemas y sacar la declaracion del problema. apoyandoce de 
los casos de uso en fisico y diagrama de contexto, usando una tematica de lluvia de ideas.   

2. Definir los objetivos de negocio y luego definir las metas para alcanzar/realizar dicho 
producto o solucion. hacerlo en word ni pex y con ayuda del EDT para trazar.   

3. Definir el alcance contestando a ¿como sabemos que acabamos?.   

4. Empezar a hacer la pila de producto con la ayuda del EDT trazado, y los objetivos a cada 
uno de estos incluyebdole la columna como comprobarlo.   

5. Hacer una reunion de lluvia de ideas con el uso de casos de uso e historias de uso en 
fisico y con hojas de papel.   

	6) Empezar el diseño (6.X en paralelo).   
	  6.1) Definir reglas/enunciado de negocio enfocandoce en entidades y relaciones 
         (seguir libro) para luego tambien empezar el diagrama E/R.   
	  6.2) Empezar a realizar la UI vasandoce en la columna como comprobarlo. Primero 
         en papel y luego pasarlo a powerPoint con la extencion de storiboard.   
    
	7) Del diagrama E/R sacar el diagrama logico y hacer tests a la BD que se genere 
     del modelo logico.   
  
	8) Del prototipo de UI empezar a hacer procedimientos almacenados que se requieran 
     a menudo y que sean complejos, contestando a preguntas como ¿como agrego un ...?, 
     o ¿en la vista X que datos necesito y cuales son sus parametros de entrada salida?.   

X) Hacer una seleccion de herramientas de desarrollo.   




Resutltado.   

1) documento con def. de problema y enlistado de estos, diagrama espina pescado, 
objetivos para llegar a la solucion, objetivos generales (desde el punto de vista 
del cliente), alcance-delimitacion, EDT, Diagrama de contexto.   

2) Prototipos: diagrama de atividades, diagrama UI bajo nivel.   

4) Lista de requerimientos listos para ser empaquetados para precentacion a los stakeholders.   

3) Documento con dudas que ahigan surgido para los stakeholders.   
