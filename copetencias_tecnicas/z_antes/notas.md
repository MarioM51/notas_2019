# Notas

## Paginas

+ Paginas de cursos
  + udemy.com  
  + coursera.org  
  + miriadax.net  

+ Paginas de referencia
  + [Acordeon de markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#table-of-contents)  
  + [Acordeon de Git](https://www.keycdn.com/blog/git-cheat-sheet)  

## Ser creativo no es ser original.Austin Kleon, escritor y artista

1.nada surge de la nada, muchas cosas son una recopilacion de varias cosas
2.Uno cree que debe de saber quien es para luego hacer cosas, pero creo
   que uno primero hace cosas y luego sabes quien eres.
3.haz un servicio/producto que quisieras que existiera.
4.el trabajo creativo no siempre empiza por la mente.tambien empieza
   por la mente.
5.los hobbies / proyectos secundarios son importantes ya que puede
   dar ideas para tu proyecto principal o puede que este despegue mas que
   el otro.
6.Crea algo bueno y luego compartelo.
7.la geografia ya no nos limita.
8.Se aburrido, para hacer algo debes de sentarte en el mismo lugar
   para hacer lo mismo cada dia.
9.Sacrificios, al final debes de dejar de hacer ciertas cosas para
   que  logres tus objetivos.
10.Aburrirse es importante para asociar cosas que estando entretenido
    no se puede.
11.Las listas pueden ser como la lista de canciones de un album, en
    las cuales sirven para guiar lo que quieres comunicar, pero igual
    sirven para que puedas saltar al punto que a uno/ellos le interesa
    e incluso pueden nacer ideas entre los puntos.

## Mis Vacaciones en Huatulco

Resumen

+ Fueron 4 dias incluyendo los que tome el avion, osea 2 completos alla.
+ 3 o 4 dias completos ubiera estado mejor, consiguiendo un hotel mas barato.

### Viernes

+ Compre los boletos en viva-aerobus solo el de ida un dia antes lo que me costo como 1300 el boleto.
  + Me hubiera ahorrado mas al comprarlo con mas anticipacion ya que los llegue a ver en 700.
  
+ Solo me lleve mi morral con 2 mudas de ropa 3 con la que lleva puesta, sin toalla.
  + Siento que solo un muda hubiera sido suficiente, la toalla use la del hotel.
  + Me lleve 2 sudaderes lo cual en el mar fue una burrada, por mucho la livina ubiera bastado.

### Sabado

+ Al llegar al aeropuerto al salir tome la combi que ahi paso, y me baje en una plasa comercial.

+ de ahi camine a un chedragui en donde encontre hotel, primero me hospede en el XXX.

### Domingo

+ Me sali del hotel para ir a la playa de la cruz cerca del mercado de artesanias y ropa.

+ Ahi me acerque a un modulo de informacion turistica donde me recomendaron el hotel y los recorridos.

+ Fui al recorrido de las bahias, y de las tortugas y cocodrilos.

+ En la noche fui a la iguesia de la cruzesita.

+ compre morral grande, y cosillas.

+ Fui a bar, donde una chica me dejo un chupeton >:Z

### Lunes

+ Fui al recorrido de las cascadas, donde en parada.

+ compre mezcal de maracuya, chocolate, vaso, chanclas para nadar, segunporque se chispan.
  + Ubiera comprado chocolate de barra y chanclas tipo tenis.

+ Fui al temascal.

### Martes

+ Fui al recorrido de las motosicletas y de las lanchas, se me olvido llevar el protector aqui.

+ Para ir al aeropuerto tome micro en ahi por el centro comercial cerca del centro.

+ Tome el el vuelo de regreso y tome un apizaco y luego un taxi a tlaxco.

## Resumen Videos

### Como luchar contra el envejecimiento

Las moleculas tienen 2 modos, 1.crecimiento y reporoduccion y 2 reparacion y proteccion
y al estar en el 2, vivimos mas y lo hacemos con lo siguiente

+ Ayunos intermitentes restriccion calorica  

+ evitar las proteinas

+ ejercicio HIIT (Hight Intensity intervels time)

+ incomodamente frio o caliente

### Acronimos de tecnologia

+ Modos en los que se construlle/muestra una aplicacion web
  + **SSR** = server-side rendered
  + **CSR** = client-side rendering
  + **SSG** = Static Site Generators
