# Objetivos

+ Casos de Uso
  + Crear casos de uso
  + crear prototipo de bajo nivel
  + diseño de la base de datos
  + division de etapas
  + Rollos tecnico
    + 1 crear front-office con Gatsby
    + 2 Crear back-office con android
    + 3 Crear back-end con Spring

## Cursos Youtube/gratis

+ [Dibujos cartoon](https://www.youtube.com/playlist?list=PL8pOhpBIzCj4tSqh9cXl0hZpuxuHjJDsf)
+ [Yoga](https://www.youtube.com/watch?v=dgCECIosrYE&list=PLNH7cFJ42PKgKMSX96VzgrR_S4GUmTfgQ&index=2&t=0s)
+ [Curso ingles](https://www.youtube.com/playlist?list=PL93iwVYt7GG7OMmJZclmGo0ekKOzTi5o-)
+ Letering a mano
+ Animales geometricos
