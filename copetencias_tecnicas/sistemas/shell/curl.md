# Curl

## Hacer peticion post

```r
curl -i -X POST --data '{"title":"Uno","price":20}' http://localhost:8080/products
```

## Incluir cookies

```r
curl --cookie "key1=val1;key2=val2;..." 
```

## Incluir headers

--header "Accept: text/javascript" --header "X-Test: hello"