# Shell & Shell-Script

- [Shell \& Shell-Script](#shell--shell-script)
  - [falta](#falta)
  - [Sección 4: Comandos del sistema de ficheros](#sección-4-comandos-del-sistema-de-ficheros)
    - [18. Uso de comodines: la interrogación](#18-uso-de-comodines-la-interrogación)
    - [Sección 6: Búsquedas en el sistema de ficheros con FIND](#sección-6-búsquedas-en-el-sistema-de-ficheros-con-find)
  - [======== APARTADO 2, MAHEJO DE TEXTO ====](#-apartado-2-mahejo-de-texto-)
    - [35. Enviar texto a la salida estándar. Comando ECHO](#35-enviar-texto-a-la-salida-estándar-comando-echo)
    - [36. Mostrar contenido de ficheros de texto. CAT, MORE y LESS](#36-mostrar-contenido-de-ficheros-de-texto-cat-more-y-less)
    - [37. Seleccionar primeras o últimas lineas: HEAD y TAIL](#37-seleccionar-primeras-o-últimas-lineas-head-y-tail)
    - [38. Seleccionar partes de una linea: CUT](#38-seleccionar-partes-de-una-linea-cut)
    - [39. Filtrar sólo las lineas que contengan un patrón: GREP](#39-filtrar-sólo-las-lineas-que-contengan-un-patrón-grep)
    - [Tarea](#tarea)
  - [Sección 10: Acciones sobre texto](#sección-10-acciones-sobre-texto)
    - [41. Editores de texto: nano](#41-editores-de-texto-nano)
    - [41. Editores de texto: vi](#41-editores-de-texto-vi)
    - [43. Ordenar y procesar repeticiones: SORT y UNIQ](#43-ordenar-y-procesar-repeticiones-sort-y-uniq)
    - [44. Contar e invertir: WC y REV](#44-contar-e-invertir-wc-y-rev)
  - [Sección 11: Tuberías y redirecciones](#sección-11-tuberías-y-redirecciones)
    - [46. Redirecciones](#46-redirecciones)
    - [47. tuberias](#47-tuberias)
    - [Retos](#retos)
  - [Sección 12: Comandos avanzados: TR y SED](#sección-12-comandos-avanzados-tr-y-sed)
    - [49. Sustitucion de caracteres](#49-sustitucion-de-caracteres)
    - [50. Ejemplos con TR](#50-ejemplos-con-tr)
    - [51. Editor de flujo de texto: SED](#51-editor-de-flujo-de-texto-sed)
    - [52. Ejemplos con SED](#52-ejemplos-con-sed)
    - [Tarea seccion 12](#tarea-seccion-12)
  - [Sección 13: Expresiones Regulares (Regex)](#sección-13-expresiones-regulares-regex)
    - [56. Exclusión y Rangos](#56-exclusión-y-rangos)
    - [57. Repeticiones](#57-repeticiones)
    - [58. OR, uso de paréntesis, abreviaturas y límites de palabra](#58-or-uso-de-paréntesis-abreviaturas-y-límites-de-palabra)
    - [59. Referencias](#59-referencias)
  - [======== APARTADO 3 PROGRAMAR SHELL SCRIPT ====](#-apartado-3-programar-shell-script-)
    - [62. Uso de los parámetros](#62-uso-de-los-parámetros)
    - [63. Lectura y escritura de variables](#63-lectura-y-escritura-de-variables)
    - [64. Variables del sistema](#64-variables-del-sistema)
    - [65. Arrays, arreglos](#65-arrays-arreglos)
    - [67. Leer información con READ](#67-leer-información-con-read)
    - [Sección 17: Operaciones](#sección-17-operaciones)
    - [70. Operaciones Lógicas. Comando Test](#70-operaciones-lógicas-comando-test)
    - [72. Test: comprobación sobre ficheros y else](#72-test-comprobación-sobre-ficheros-y-else)
    - [73. Comprobación de cadenas con Expresiones Regulares](#73-comprobación-de-cadenas-con-expresiones-regulares)
    - [74. Operaciones con el contenido de una Variable](#74-operaciones-con-el-contenido-de-una-variable)
    - [Tareas seccion 14](#tareas-seccion-14)
  - [Sección 18: Estructuras condicionales](#sección-18-estructuras-condicionales)
    - [Condicional anidado y uso de AND y OR](#condicional-anidado-y-uso-de-and-y-or)
    - [78. Sentencia CASE](#78-sentencia-case)
    - [80. Sustitucion o expasion de comandos](#80-sustitucion-o-expasion-de-comandos)
    - [81. Uso de AND y OR para ejecutar comandos dependiendo del otro](#81-uso-de-and-y-or-para-ejecutar-comandos-dependiendo-del-otro)
    - [83. Depurar un Script](#83-depurar-un-script)
    - [Tareas seccion 18](#tareas-seccion-18)
  - [Sección 19: Bucles](#sección-19-bucles)
    - [85. while y for](#85-while-y-for)
    - [86. Modificando iteraciones: Break y Continue](#86-modificando-iteraciones-break-y-continue)
    - [87. PENDIENTE Ejemplo Renombrar Ficheros ejemplo real](#87-pendiente-ejemplo-renombrar-ficheros-ejemplo-real)
    - [Tarea Seccion 19](#tarea-seccion-19)
  - [Sección 20: Funciones](#sección-20-funciones)
    - [90. Ejemplo Avanzado. mostrar información de usuarios](#90-ejemplo-avanzado-mostrar-información-de-usuarios)
    - [Tarea, Seccion 20](#tarea-seccion-20)
  - [Sección 21: Conocimientos útil](#sección-21-conocimientos-útil)
    - [92. Compactar y comprimir ficheros y directorios: TAR](#92-compactar-y-comprimir-ficheros-y-directorios-tar)
    - [93. TAR, ejemplos de uso](#93-tar-ejemplos-de-uso)
    - [94. Programar tareas: CRON](#94-programar-tareas-cron)
    - [95. Ejemplo CRON y TAR: programar copias de seguridad](#95-ejemplo-cron-y-tar-programar-copias-de-seguridad)
    - [96. Introducir texto en programas interactivos](#96-introducir-texto-en-programas-interactivos)
    - [97. El Separador Interno entre Campos. $IFS](#97-el-separador-interno-entre-campos-ifs)
  - [Mis notas](#mis-notas)
    - [Buscar en logs](#buscar-en-logs)
    - [Uso de SCP](#uso-de-scp)
    - [Registrar temperatura](#registrar-temperatura)
  - [Seguridad](#seguridad)
    - [ssh](#ssh)
  - [Por el camino](#por-el-camino)
    - [Errores inesperados al ejecutar script](#errores-inesperados-al-ejecutar-script)
    - [Historial de comandos](#historial-de-comandos)
    - [Crear panel de control y/o vigilar procesos archivos que nos interesa](#crear-panel-de-control-yo-vigilar-procesos-archivos-que-nos-interesa)
    - [Script para al cambiar jar detener el anterior y ejecutar el nuevo](#script-para-al-cambiar-jar-detener-el-anterior-y-ejecutar-el-nuevo)
    - [Cambiar zona horaria](#cambiar-zona-horaria)
    - [Explorar estructura de folders](#explorar-estructura-de-folders)
    - [Uso de apt](#uso-de-apt)
    - [Checaar puertos](#checaar-puertos)
    - [obtener ruta del script en ejecucion](#obtener-ruta-del-script-en-ejecucion)
    - [instalar cliente postgres](#instalar-cliente-postgres)
    - [Crear folders de prueba](#crear-folders-de-prueba)
    - [Detectar sistema operativo en bash script](#detectar-sistema-operativo-en-bash-script)
    - [Mover solo contenido de carpeta](#mover-solo-contenido-de-carpeta)
    - [Asignar una IP estatica](#asignar-una-ip-estatica)
    - [Comparar dos archivos de texto](#comparar-dos-archivos-de-texto)
    - [Uso de ps (ver procesos)](#uso-de-ps-ver-procesos)
    - [Generar String aleatorio](#generar-string-aleatorio)
    - [Uso de variable de entorno](#uso-de-variable-de-entorno)
    - [Incrementar o crear la memora swap](#incrementar-o-crear-la-memora-swap)
    - [Esperar hasta podese conectar a una IP y puerto](#esperar-hasta-podese-conectar-a-una-ip-y-puerto)
    - [Esperar hasta encontar algo en los logs](#esperar-hasta-encontar-algo-en-los-logs)
    - [Importar variables de .env](#importar-variables-de-env)
    - [Apariencia de la terminal](#apariencia-de-la-terminal)
    - [Descargar archivo usando url](#descargar-archivo-usando-url)
    - [DNS](#dns)
      - [Checar respuesta de DNS en consola](#checar-respuesta-de-dns-en-consola)
      - ["Redirigir" ej.com a www.ej.com](#redirigir-ejcom-a-wwwejcom)
      - [SRV record](#srv-record)

## falta

- Buscar archivos por fecha de modificacion y permisos
- aptitude y synaptic

## Sección 4: Comandos del sistema de ficheros

### 18. Uso de comodines: la interrogación

Tenemos el uso de comodin `*` el cual indica cualquier caracter y
cualquier cantidad de caracteres, lo cual habeces podira llegar a
ser muy generico.

**Problematica** Tenemos una lista de archivos unos separados solo
por año y otros seprados por año y mes, y solo queremos los fiecheros
que estan separados por mes.

```bash
> ls
2017-01.txt 2017-02.txt 2017.txt 2018-01.txt 2018-02.txt 2018.txt 2019.txt 2020-01.txt

> ls 201?.txt
2017.txt 2018.txt 2019.txt
```

----

----

----

### Sección 6: Búsquedas en el sistema de ficheros con FIND

El comando `find` nos sirve para encontrar archivos y nos da
diferentes el cual puede ser muy poderoso.

- `-name` podemos usar patrones a buscar y usamos `-iname` para que
  no distinga entre mayus y minus.
- `-type` podemos filtrar por directorio(d), fichero(f), elnaces (i)
- `-size`: Especificamos el tamaño `+` mas de, `-` memos de, `c` bytes
- `-perm`: podemos buscar por permiso

<!-- buscar archivos por su nombre, buscar por nombre de archivo, buscar nombre de archivo-->

Ejemplo:

```bash
#Listar los archivos que terminen en sh sin distinguir mayus/minus en sh
> find /home/mario -iname "*.sh"

# Listar los directorios que contenga una i, buscar diectorios por nombre
# bucar folders, buscar carpetas, buscar por nombre de carpeta
> find . -iname "*i*" -type d

#Listar los que tengan mas de 2M
> find . -size +2M

# Listar los que pesen menos de 1byte=c, para kilobyte=k
> find . -size -1c
```

----

----

----

## ======== APARTADO 2, MAHEJO DE TEXTO ====

- La gran mayoria de configuraciones e informacion que maneja linux se
  guarda en ficheros de texto
- Los registros de actividad (logs) tambien en formato de texto, los
  cuales suelen ser muy grandes, y necesitamos encontrar o quedarnos
  solo con aquellos logs que nos interesan.
- Cualquier programa en modo consola que muestra resultados en el
  monitor lo hace mendiante texto, el cual podemos redirigir y combinar
  para obtener lo que queremos
- Nos sirve para modificar cualquier tipo de codigo fuente o de base
  de datos: HTML, JS, JSON, SQL, csv

Por ejemplo: Al consultar un usuario en `/etc/passwd`, si imrpimimos
toso el archivo nos aparecera mucha informacion.

----

### 35. Enviar texto a la salida estándar. Comando ECHO

Veremos diversor comandos paramostrar texto en pantalla

Comando **echo** este repite lo que le indicamos, lo interesante es
usarlo para que interprete caracteres especiales con barra-invertida
como.

- Es recomendable el uso de comillas dobles
- Si usamos comillas simples ignorara sustituir las variables para mostrar

```sh
> echo -e "Salto de linea \n segundo renglon"
< Salto de linea
<  segundo renglon

> echo "Salto de linea \n segundo renglon"
< Salto de linea \n segundo renglon

# hacer operaciones matematicas
> echo "Multiplicacion $((25*4))"
< Multiplicacion 100

# mostrar variables de sistema
> echo "$USER - $PATH"
< mario - /usr/local/sbin:/usr/local/bin:/usr/sbin

# mostrar caracteres especiales
> man echo
\\     backslash
\n     new line
\v     vertical tab
```

----

### 36. Mostrar contenido de ficheros de texto. CAT, MORE y LESS

**CAT** con este comando mostramos el contenido de un archivo

- con `-n` podemos mostrar el numero de cada linea del archivo
- El problema con cat es que si tenemos muchas lineas, no nos alcansara
  la pantalla y no podremos ver el archivo

```sh
cat /etc/network/interfaces
configuraciones de red...

> cat -n /etc/issue
< 1  Ubuntu 18.04.2 LTS \n \l
< 2
```

**MORE** Lo que hara es parar la pantalla para que nosotros lo avanzemos
a nuestro propio ritmo.

- para avanzar linea por linea usamos `enter`
- para avanzar por pantalla usamos `space`
- Pulsando `h` nos mostrara diferentes opciones que podemos hacer

```sh
more /var/log/syslog
mucho-texto...
```

**LESS** hace lo mismo y funciona igual que `more` pero es mas potente

- ventajas
  - al salir oculta el texto para tener un shell mas limpio
  - optimizado para muchas muchas lineas de texto

- con `:300` podemos avanzar 300 lineas o las que queramos

```sh
less /var/log/syslog
```

----

### 37. Seleccionar primeras o últimas lineas: HEAD y TAIL

Los archivos de logs siempre el nuevo contenido se agrega hasta abajo
por lo que nos interezara mostrar las ultimas lineas, y en otros casos
mostrar las primeras.

**TAIL** nos mostrara las ultimas lineas de un archivo, por defecto
las ultimas 10 lineas

**HEAD** Nos mostrara las primeras lineas

- `-n 100`  nos mostrara las 111 ultimas/primeras lineas
- `-100` lo mismo pero nos ahorramos la letra del argumento
- `tail argumentos *.log` nos mostrara las ultimas lineas de todos
  aquellos archivos dentro de la carpeta que terminen en .log

```sh
tail /var/log/syslog
head -n 1 /var/log/syslog # mostrar la primera linea nadamas
tail -100 /var/log/syslog # nos mostrara las ultimas 100

# mostrar las ultimas lineas de todos los archivos que terminen en .log
tail -n 10 /var/log/*.log

# no mostrara el nombre de cada archivo
tail -n 1 -q /var/log/*.log
```

----

### 38. Seleccionar partes de una linea: CUT

<!-- corte vertical, cortar verticalmente, uso de cut -->

**CUT** Lo que hara es un "corte" vertical en cada linea del archivo,
lo cual puede ser util si tenemos un archivo bien definido verticalmente

- `-c [inicio]-[fin]` Mustra desde el x caracter hasta el x caracter
- `-c [inicio],[fin]` Muestra el x caracter y el x caracter

```sh
> cut -c1-10 /etc/passwd
root:x:0:0
daemon:x:1
bin:x:2:2:
sys:x:3:3:
sync:x:4:6
...

> cut -c1,10 /etc/passwd
r0
d1
b:
s:
s6
...

# podemos combinar rangos y caracteres con
> cut -c1-3,6-7,10 /etc/passwd
roox:0
daen:1
bin:2:
sys:3:
synx:6
```

El argumento `-d` podemos indecar el caracter por el cual va a cortar
verticalmente, por ejemplo para un .cvs que esta separado por comas
podremos obtener la informacion de este de una forma mas organizada.  
Y una ves que lo tengamos separado con `-f` podemos indicar cuales de
ellos queremos.

<!-- Selecionar campo, dividir informacion por delimitador-->

**Ejemplo** ver los usuarios que tenemos en linux

```sh
# (-d ":") en el archivo separamos los campos por ":"
# (-f 1) solo queremos el primer campo
> cut -d ":" -f 1 /etc/passwd
root
daemon
bin
sys
sync
```

Del mismo modo que escogemos los caracteres, podemos escoger los campos

**Ejemplo:** Podemos escojer el campo 1 y 3, y del 4 al 6

```sh
> cut -d":" -f1,3,5-7 /etc/passwd
daemon:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:2:bin:/bin:/usr/sbin/nologin
sys:3:sys:/dev:/usr/sbin/nologin
```

----

### 39. Filtrar sólo las lineas que contengan un patrón: GREP

**GREP** Nos sirve para mostrar solo las lineas que cumplen con un
patron lo cual suele ser muy util, .

- `-v` mostrar las que NO coinciden con el patron
- `-l` mostrar solo el nombre del archivo donde se encontro el patron
- `-w` El patron debe ser una palabra independente, rodeada de espacios
  o por caracteres especiales
- `-n` Indica el numero de linea
- `-i` No distingue entre mayusculas y minusculas
- `-c` No nos muestra la linea solo nos muestra la cantidad que cumplen
  con el patron
- `-r` Busca en los archivos de forma recursiva directorio/sub-directorio
  
**Ejemplos:**

<!-- buscar archivos que contengan una palabra, buscar archivos por su contenido, encontrar archivos que contengan una palabra, encontrar archivos por su contenido, -->
<!-- grep ocultar lineas de un archivo con grep, grep quitar lineas de un archivo con grep, grep quitar lineas de un archivo a mostrar con grep, grep eliminar lineas de un archivo con grep, -->
<!-- filtrar lineas de una salida, filtrar salida de un programa  -->

```sh
# Buscar si existe un usuario, comprobar si existe un usuario, ver si existe un usuario, existe usuario
> grep "root" /etc/passwd
root:x:0:0:root:/root:/bin/bash

# Buscar los usuarios con el login habilitado, para eso quitamos
# las lineas que contengan "nologin" ya que eso indica el tienen
# el login desabilitado
> grep -v "nologin" /etc/passwd
sync:x:4:65534:sync:/bin:/bin/sync
lxd:x:105:65534::/var/lib/lxd/:/bin/false

# Mostrar los archivos donde tenga x palabra
> grep "error" -l /var/log/*.log
/var/log/bootstrap.log
/var/log/dpkg.log

# Mostrar las lineas donde tenga x palabra
> grep "error" /var/log/*.log
/var/log/bootstrap.log:Selecting previously unselected package libgpg-error0:amd64.
/var/log/dpkg.log:2019-02-14 09:49:37 install libgpg-error0:amd64 <none> 1.27-6

# buscar los grupos con el id 100, el problema es que me mostrara las
# lineas que tengan la coincidencia como el 1000 o 0100, solucion -w
> grep -w "100" /etc/passwd
systemd-network:x:100:102:systemd Network Management

# mostrar la linea donde encontro la coincidencia e ignorar case-sensitive
> grep -n -i "system" /etc/passwd
17:gnats:x:41:41:Gnats Bug-Reporting System...
19:systemd-network:x:100:102:systemd Networ...
20:systemd-resolve:x:101:103:systemd Resolv...

# mostrar el numero que se encontro dicha coincidencia, en x directorio
# y subdirectorios
> grep -c -r "error" /var/log/
/var/log/apt/history.log:2
/var/log/apt/eipp.log.xz:10
/var/log/journal/f95e4bf6a0e146a59/system.journal:1

# Mostrar 12 lineas antess y 10 lineas despues de donde encontro
> grep -B12 -A10 "error" /var/log/log.log


```

----

### Tarea

![01-reto-manejo-de-texto](./img/01-reto-manejo-de-texto.png)

----

----

----

## Sección 10: Acciones sobre texto

### 41. Editores de texto: nano

Es un editor de texto secillo y parecido a los que tienen UI.

- `ctrl+G`: Ayuda
- `ctrl+O`: Escribir el fichero
- `ctrl+W`: Buscar cadena
- `ctrl+_`: Ir a una linea den concreto

----

### 41. Editores de texto: vi

Es un editor de texto muy potente pero suele ser mas dificil de usar.

- `Esc` - Tiene dos modos de funcionar comando o editor y con esta
  tecla cambiamos de modo.
- Cuando queremos empesar a escribir pultamos `i` (debe de mostrarse
  una ==INSERT== en la parte inferior) y salimos con 'Esc'.
- `/palabra_a_buscar` - en modo comando buscamos una palabra con barra.
- `:q!` - fuerza la salida del programa, aun que no guardemos cambios.
- `:w` - Guardamos los cambios.
- `:wq` - Guardamos los cambios y salir.

----

### 43. Ordenar y procesar repeticiones: SORT y UNIQ

**SORT** Lo podemos ejecutar sobre un archivo para ordenar sus lineas,
por defecto lo hace en modo alfabetico ascendente.

- No modifica solo muestra

```sh
> sort /etc/passwd
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
games:x:5:60:games:/usr/games:/usr/sbin

> sort /etc/passwd
# ordenado de forma inversa

#Ordenar por orden numerico, ordenar numeros
> sort -n prueba.txt

# ordenar en magnitudas humanas
> sort -h prueba.txt

```

**UNIQ** eliminar lineas repetidas que esten juntas

```sh
> cat prueba.txt
ana
alberto
beto
beto
ana
ana
alberto
beto
carlos


> uniq prueba.txt
ana
alberto
beto
ana
alberto
beto
carlos

> uniq -ic prueba.txt # ignora mayus y minus y dice las repeticiones
```

**AWK** Eliminar lineas repetidas, sin importar que tan distanciadas esten,
eliminar lineas que se repiten

```r
#Ejemplo
awk '!a[$0]++' prueba.txt

#Ejemplo real, donde en los logs de un servidor se deja solo la seccion
#de las ips y se eliminan las repetidas, y se ignorar las lineas que
#digan no-www y se quitan los espacios en blanco
cat /path/server.log | cut -d "|" -f4 | grep -v "no-www to www" | tr -s " " | awk '!a[$0]++'
```

----

### 44. Contar e invertir: WC y REV

<!-- 
contar numero de lineas, contar lineas de archivo, mostrar numero de lineas de archivo
cuantas lineas tiene un archivo
-->

**WC** Nos muestra la cantidad de lineas, palabras, caracteres
respectivamentez

- `-l`: Solo las lineas
- `-w`: Solo las palabras
- `-m`: Solo los caracteres

```sh
> wc prueba.txt
9 6 50 prueba.txt
```

**REV** Invertira el orden de los caracteres la linea.  
**Por ejemplo**: Extraer el ultimo directorio de una lista de
directorios guardada en un archivo de texto.

<!-- voltear caracteres, invertir caracteres, alrevez, al revez -->

1. Invertimos el orden de los caracteres con rev
2. Dividimos por "/" y obtenemos el primer campo con cut
3. le damos otra ves la buelta con el comando rev

```sh
> cat prueba
/etc/perl/xml
/etc/perl/xml/sac
/etc/avahi
/etc/avahi/services
/etc/avahi/services/workers
/etc/avahi/services/workers/utils

> rev prueba.txt | cut -d"/" -f1 | rev
xml
sac
avahi
services
workers
utils
```

![01-reto-manejo-de-texto-2](./img/01-reto-manejo-de-texto-2.png)

----

----

----

## Sección 11: Tuberías y redirecciones

### 46. Redirecciones

Las **redirecciones** nos permiten modificar la salida estandar de un
programa que la mayoria por defecto es a la pantalla, por ejemlo, si
no queremos que se guarde un texto en un archivo de texto, podremos
usar las redirecciones.

<!-- echo con salto de linea -->

```sh
# ">" creara o sustituira un archivo con lo indicado
> echo "hola /n tuberias desde aqui" > salida.txt
> cat salida.txt
hola
 tuberias desde aqui

# ">>" Añadira el nuevo texto al final
> echo "Saludos desde abajo" >> salida.txt
> cat salida.txt
hola
 tuberias desde aqui
Saludos desde abajo

# usamos "2> o 2>>" para redirigir los errores nadamas, redirigir errores
> grep -c -r "error" /var/log/ 2> errores.txt # no muestra errores
/var/log/bootstrap.log:4
/var/log/dpkg.log:16
/var/log/unattended-upgrades-shutdown.log:0
> cat errores.txt # los errores se guardan aqui
grep: curtin-install.log: Permission denied
grep: install-cfg.yaml: Permission denied
grep: tallylog: Permission denied
grep: /var/log/btmp: Permission denied

# usamos "&> o &>>" para redirigir los mensajes y errores
```

----

### 47. tuberias

Las **tuberias** nos permiten multiplicar el potencial de los comandos,
ya que podemos mandar el resultado de un comando a otro.  

<!-- mostrar espacio ocupado por carpeta, mostar peso de carpeta-->
```sh
> echo "Hola Mundo" | wc
1       2      11

# mostrar los usurios que empiezen por m
>  grep ^m /etc/passwd | cut -d":" -f1
man
mail
messagebus
mario

# no imprimir errores, no mostrar errores, mandar a null, mandar error a null

# mostrar la carpeta que mayor espacio ocupa en el sistema
# - "2> /dev/null" enviara los errores a ningun lado
# - "-hr" ordena datos humanos de mayor a menor
> du  -sh /* 2> /dev/null | sort -h | tail -n1
2.1G    /swap.img
```

Tambien podemos redirigir la entrada de texto (por defecto el teclado)
usando `"<"` para que hagarre la informacion de otro lado, por ejemplo,
un fichero.

```sh
# ejecutara mysql todos los comandos contenidos en el archivo
mysql < db_backupt.sql
```

----

### Retos

![03-reto-tuberias-y-redirecciones](./img/03-reto-tuberias-y-redirecciones.PNG)

----

----

----

## Sección 12: Comandos avanzados: TR y SED

Usaremos estos camandos para hacer una combinacion mas avanzada de
tuberias y redirecciones.

----

### 49. Sustitucion de caracteres

**TR** Lo que hace el comando remplazar o eliminar caractreres que lee
desde la entrada

![04-comando-tr](./img/04-comando-tr.gif)

- `-d` Borrar el patron que le indicamos
- `-s` Eliminar los caracteres repetidos
- `-c` Se veran afectados todos los caracteres menos los indicados
- Puede funcionar con expreciones regulares, por ejempo
  - Podemos incluir rangos por ejemplo `a` a la `z` con `a-z`
  - `[a*3]` Si encuentra 3 a seguidas las sustitulle

![05-clases-expreciones-regulares](./img/05-clases-expreciones-regulares.PNG)

```sh
> echo "Esto es una frace, de varias palabras para un parrafo largo: 15" > prueba.txt

# Clases regulares

# sustituir numeros de un texto
> tr [:digit:] X < prueba.txt
Esto es una frace, de varias palabras para un parrafo largo: XX

# sustituir caracteres, sustituir texto, sustituir letras
> tr [:alpha:] X < prueba.txt
XXXX XX XXX XXXXX, XX XXXXXX XXXXXXXX XXXX XX XXXXXXX XXXX: 15

# quitar los espacios, sustituir espacios
> tr [:space:] - < prueba.txt
Esto-es-una-frace,-de-varias-palabras-para-un-parrafo-largo:-15

# Argumentos, Eliminar la letra a
> tr -d a < prueba.txt
Esto es un frce, de vris plbrs pr un prrfo lrgo: 15

# eliminar la r que esten repetidas, quitar caracteres repqtidos,
# quitar repetidos, quitar patrones repetidos
> tr -s r < prueba.txt
... parafo largo: 15

# pasar de mayusculas a minisculas, convertir a minusculas
# pasar de mayusculas a minusculas, lo hacemos al inverso la ultima p
> echo "Texto CoN MaYusCuLaS" | tr '[:upper:]' '[:lower:]'

echo "Texto CoN MaYusCuLaS" | tr '[:lower:]' '[:upper:]'
```

----

### 50. Ejemplos con TR

<!-- uso de tr, como usar tr -->

**Problema:** Queremos tomar el grupo al que le pertenese un elemento
de la siguiente columna, Si nos fijamos podemos tener un problema, con
cut, ya que no captura el root por que **tiene 2 espacios**

<!-- separar campos, separar por caracter-->

```sh
> ls -l
-rw-rw-r-- 1 mario mario 213 May 26 00:12 errores.txt
-rw-r--r-- 1 root  root   47 May 26 04:31 f_de_root.txt
-rw-rw-r-- 1 mario mario  63 May 26 04:18 prueba.txt

> ls -l | cut -d" " -f4
mario

mario
mario
```

**Solucion:** Le quitamos los 2 espacios con tr como ya vimos
<!-- obtener solo nombre de archivo, sacar solo el nombre de archivo-->
```sh
> ls -l | tr -s " " | cut -d" " -f4
mario
root
mario
mario
```

----

### 51. Editor de flujo de texto: SED

`sed` modifica linea a linea segun un script especificado, su sintaxis
es la siguiente.

```sh
sed [-n] [-e "script"] [-f archivo] archivo1 archivo2
```

- `-n` Indicamos que suprima la salida estandart, `-p` indicamos que
  sea por pantalla
- `-e` Indica el script
- `-f` Indica que las ordenes seran tomadas de un fichero
- `-r` Se utilizan expreciones regulares extendidas

![06-comando_sed_ejemplo.PNG](./img/06-comando_sed_ejemplo.PNG)

- La ubicacion la indicamos por lineas, arriba le indicamos que 2 a 5
  seran afectadas, tambien filas especificas, por ejemplo `5-9`,
  tambien podemos con `$` especificar la ultima linea.
- Instrucciones.
- `i\`: Insertar linea antes de la linea actual.
- `a\`: Insertar linea despues de la linea actual.
- `c\`: Sustituye la linea actual por la esspecificada a continuacion.
- `d`: Borrar linea actual.
- `p`: Imprimir linea actual en salida estandart.
- `s`: Sustituir cadena en linea actual.
- `q`: Termina el proceso cuando alcanza la linea especificada.

### 52. Ejemplos con SED

Eliminar Lineas

```sh
# eliminar rango de lineas
> sed -e "1,3d" texto_largo.txt
#nos mostrara el texto modificado

# Elimina la linea que tenga dicha palabra
> sed -e "/palabra/d" texto_largo.txt

# Elimina la linea que tenga dicha palabra y 5 delante de ella
# tambien se puede "2,/palabra/d" para eliminar las que estan antes
> sed -e "/palabra/,2d" texto_largo.txt

# podemos ver que linea se borra con cat
> cat -n texto_largo.txt | sed -e '10,/tincidunt/d'

8  ...
9  ...
20 ... # Podemos ver que el enumerado se corta
21 ...
```

Agregar texto

```sh
# insertar una linea antes de la indicada
> sed -e '3i\HOLA' texto_largo.txt

# Sustituir la linea indicada
> sed -e '1c\##################' texto_largo.txt

# podemos mostrar un texto especifico incluso invertirlo
# ejemplo quitar de un texto un parrafo
> cat -n texto_largo.txt | sed -ne '6,9!p'
4   ...
5     # podemos ver todas las lineas exepto las qe indicamos
11  ...
```

Sustituir palabras

```sh
sed -e 's/PALABRA-O-EXPRECION-REGULAR/REMPLAZO/' texto_largo.txt
```

----

### Tarea seccion 12

![07-manejo-de-texto-3](./img/07-manejo-de-texto-3.PNG)

----

----

----

----

## Sección 13: Expresiones Regulares (Regex)

Comunmente usaremos las expreciones regulares con `grep` y el `sed`
en los cuales expresarnos muy especificamente para encontrar cualquier
tipo de patron.

- Caracteres especiales
- `^`: Inicio de linea
- `$`: Fin de Linea
- `.`: Cualquier caracter (comodin), si queremos quitar el significado
  especial tendremos que poner una contra-barra "\".
- `[abc]`: Podemos indicar cualquier caracter, e indica que debe de
  haber alguno de ellos. **Por ejemplo:** `c[aei]s[ao]` pude ver como
  valido a los sig casos "casa", "caso", "ceso".

**Error:** No se encuentra el archivo spanish.  
**Causa:** No tiene instalado el idioma el sistema.  
**Solucion:**

```sh
sudo apt-get install -y wspanish
sudo apt-get install -y wamerican # opcional
```

<!-- encontrar palabras, buscar palabras, buscar en diccionario-->

Ejemplos uso expreciones regulares.

- descargamos [08_datos.csv](./img/08_datos.csv)

```sh
#filtrame las ultimas palabras que empiezen con una a
> grep ^a /usr/share/dict/spanish | tail
azumbre
azur
azurita
...x10

#palabras que terminen en x
> grep x$ /usr/share/dict/spanish | tail

# palabras que cumplan con el sig. patron
> grep c[aei]s[ao] /usr/share/dict/spanish | tail
rescisoria
rescisorio
...
# Como vemos nos da las palabras que cumplen ese patron en cualquier
# parte de la palabra

# Palabras que cumplan con el patron de INICIO A FIN
> grep ^c[aei]s[ao]$ /usr/share/dict/spanish | tail
casa
caso
ceso

# uso de comodin, De un conjunto de datos de personas seleccionar lo
# que tengan  entre 20 y 29 años
> cat datos.csv
Daquan|In@infaucibus.ca|(770) 786-9698|Nec Associates|2930|cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut|18
...x100
> cut -d"|" -f1,7 datos.csv | grep 2.$ # tenemos la edad al final
Lani|28
Jescie|29
Delilah|28...

# uso de escape de caracter especial asi evitamos que interprete el
# punto como comodin
grep "\.$" texto_largo.txt
```

----

### 56. Exclusión y Rangos

Exclusion: `[^x]` asi indicamos que queremos patrones que no tengan la
letra x u cualquier otro caracter, **por ejemplo**, `c[^aei]s[^ao]` el
cual coincidira con: "cose", "cusi"

```sh
> grep "^c[^io]s[^eiou]$" /usr/share/dict/spanish | tail
casa
```

**Rango:** `[a-z]` Estamos indicando que sean de la a a la z
minusculas, siqueremos que sean numeros serian `[1-7]`

### 57. Repeticiones

podemos indicar la repecion de algun conjunto o patron

- `X*`: El asterisco concuerda con cero o mas repeticiones de la
  exprecion regular que le precede.
- `X?`: Indica 0 o 1 repeticion que le precede al caracter, osea,
  puede estar o no estar.
- `X+`: indica 1 o mas repeticiones, osea, obligatorio que este y se
  puede repetir mas veces.
- `X{n}`: Indica n repeticiones exacatas del caracter.
- `X{n,}`: indica n o mas repeticionde del caracter, osea, ahi un
  minimo de numero repeticiones, pero no maximo de repeticiones.
- `X{,n}`: Indica cero repeticiones a n repeticiones.
- `X{n,m}` Indica de n a m repeticiones del caracter.

**Nota:** Tenemos que usar `grep -E` y `sed -r` para usar estas, en
los comandos

```sh
# palabras que tengan 8 veces las letras entre la h y la q
> grep --color -E '[h-q]{8}' /usr/share/dict/spanish
frijolillo
homonimia
homónimo...

# Palabras con exactamente 3 letras, y que empiezen con d, c, q
> grep -E '^[dcq][[:alpha:]]{2}$' /usr/share/dict/spanish | tail

# palabras entre 19 y 21 letras
> grep --color -E '[[:alpha:]]{19,21}$' /usr/share/dict/spanish | tail
```

----

### 58. OR, uso de paréntesis, abreviaturas y límites de palabra

- `OR` lo podemos indicar con `|` podemos indicar que se cumpla una
  exprecion regular o la otra.
  - Ejemplo: palabras que empiezen con a y terminen con t o que
    empiezen por e y terminen en t: `a.*t$|^e.*t$`

```sh
grep -E '^a.*t$|^e.*x$' /usr/share/dict/spanish
```

- `(exprecion)`: Asi podemos poner una exprecion como un conjunto y
  asi poder usar un conjunto de expreciones.

```sh
# palabra donde tiene que repetirce dos veces la letra c seguida de
# alguna vocal esto estando junto
> grep -E '(c[aeiou]){2}' /usr/share/dict/spanish
ceción
cecografía
certificación
chancaca
chancuco
cica
cicalar
```

<!-- inicio de palabra, inicio palabra-->
**LIMITES DE PALABRA**: Son utiles para definir que queremos que esta
sea una palabra, y no una parte de ella, o tambien para indicar que esta
al inicio o al final de la palabra.

- `\<`: Inicio de la palabra
- `\>`: Fin de la palabra
- `\b`: Limite de palabra (inicio o fin)
- `\B`: Lo contrario, (no-inicio, no-final)

```sh
# palabras que inicien con "lar"
> grep -E '\<lar[[:alpha:]]*' /usr/share/dict/spanish
larga
larije
larva

# Poner al inicio y al final de cada palabra un caracter
sed -r 's/\b/|/g' texto_largo.txt
|Lorem| |ipsum| |dolor| |sit| |amet|, |consectetur| ...
|vel| |ante| |eget| |tempor|. |Nulla| |facilisi|. ...
|Maecenas| |tempus| |placerat| |nibh|, |at| |egestas| ...
```

----

### 59. Referencias

Podemos hacer algo mas complejo como condicionales, como es el de,
encontrar una no-vocal rodeada por la misma vocal

```sh
> grep -E '^([[:alpha:]])l\1$' /usr/share/dict/spanish
ala
ele
```

----

----

----

## ======== APARTADO 3 PROGRAMAR SHELL SCRIPT ====

Con los Scripts podremos crer un conjunto de comandos, para hacer
programas que automatizan un proceso, y tenemos mayores opciones como
lo es.

- debe tener permisos para poder ejecutarse y quien puede hacerlo,
  lo podemos hacer con `chmod +x mi-script.sh`
- La primera linea indicamos donde esta el bash con el que sera interp
- debe de tener una extencion `.sh`

```bash
> cat mi-script.sh
#!/bin/bash
SALUDO="Hola bash desde consola";
echo "$SALUDO";

> sh mi-script.sh
Hola bash desde consola
```

----

### 62. Uso de los parámetros

Sin definir nada tendremos disponibles diferentes parametros.

- `$n`: La informacion de un parametro en concreto, siendo n el numero
  del parametro, **Por ejemplo** `$1` sera el primer parametro que le
  pasamos al ejecutar el script.
- `$*`: Contiene todos los parametros, que se precentan en una sola
  cadena de caracteres.
- `@`: Todos los parametros pero en una lista.
- `#`: Es el numero de parametros con el que se ha llamado al script.

```sh
#!/bin/bash
echo El primer parametro es: $1
echo La cantidad de parametros pasados son: $#
echo Todos los parametros son: $*

--- Ejecucion

> sh mi-script.sh primerA segundoB
El primer parametro es: primerA
La cantidad de parametros pasados son: 2
Todos los parametros son: primerA segundoB
```

----

### 63. Lectura y escritura de variables

- Es importante que no tenga espacios blancos el igual por ejemplo
  `miVar = "algo"` causaria error, lo correcto seria `miVar="algo"`
- Tenemos dos formas de acceder al valor de la variable.
  - `echo $variable`
  - `echo ${variable}`, es util para que no se mescle con otros codigos

```sh
#!/bin/bash
var1="Hola"
var2="Mario"
echo $var1 ${var2}

--- Ejecucion

> sh mi-script.sh
Hola Mario
```

----

### 64. Variables del sistema

Tenemos variables de sistema que podemos acceder desde nuestro script

VARIABLES PREDEFINIDAS

- `$0`: Nombre del script
- `$$`: PID del proceso asignado al script en ejecucion
- `$?`: Resultado devuelto por el ultimo proceso ejecutado

VARIABLES DE ENTORNO

- `$PATH`: Es la lista de directorios donde va a ir a buscar los
  comandos que mandamos desde bash-shell
- `printenv`: Con este comando podemos ver todas las variables de
  entorno disponibles en el sistema

ejemplo

```sh
#!/bin/bash
echo Nombre del script: $0
echo Usurio que ejecuto el scriptn: $USER
echo PID asignado a este script: $$

--- Ejecucion

> sh mi-script.sh primerA segundoB

```

----

### 65. Arrays, arreglos

Arreglo es un conjuto de variables que podemos hacceder a ellas por
medio de un index

- Declaracion: `miarray=(dato1,dato2,dato3)`
- Mostrar todos los elementos `${miarray[*]}` o `${miarray[@]}`
- Mostrar la cantidad de elementos `${#miarray[*]}`
- Mostrar los indices: `${!#miarray[*]}`
- Eliminar alguna posicion: `unset miarray[n]`, si no se pone cual
  indice se eliminaran todos

```sh
#!/bin/bash
miarray=("dato1" "dato2" "dato3")
echo "Elemento indice 1 del arreglo:" ${miarray[1]}
echo "Total de elementos en el arreglo:" ${#miarray[*]}
echo "Todos los elementos del arreglo:" ${miarray[*]}
echo "Indices del arreglo:" ${!miarray[*]}

# Recorrer todo el arreglo
for i in "${miarray[@]}"
do
   echo "$i"
done

---- Ejecuccion
Elemento indice 1 del arreglo: dato2
Total de elementos en el arreglo: 3
Todos los elementos del arreglo: dato1 dato2 dato3
Indices del arreglo: 0 1 2
dato1
dato2
dato3
```

> **Error**: Syntax error: "(" unexpected .  
> **Descripcion**: Tenemos bien la sintaxis pero aun asi nos muestra
> este error al ejecutarlo.
> **Causa**: Estamos ejecutando el script `sh miScript.sh` y con sh no
> tiene soporte para los arreglos.
> **Solucion**: Ejecutar el script asi: `./miScript.sh`

----

### 67. Leer información con READ

COn este podemos indicar que el usuario introdusca algo para guardarlo
dentro de una variable

<!-- pedir entrada por teclado, entrada de texto, pedir texto, introducir texto,  -->

```sh
#!/bin/bash
echo "Escribe algo: "
read var1;
echo "Dato introducido: ${var1}";
read -p "Escribe numbre y primer apellidos: " var1 var2
echo "Tu nombre es: ${var1} y apellido es ${var2}";

---- Ejecucion
Escribe algo:
algo
Dato introducido: algo
Escribe numbre y primer apellidos: Mario Marquez
Tu nombre es: Mario y apellido es Marquez
```

----

### Sección 17: Operaciones

- Operaciones igual que una calcu: `echo $((1+1))`
- Mostrar numero aleatorio entre 0 y 10: `echo $RANDOM%10`

----

### 70. Operaciones Lógicas. Comando Test

- Fuente: `man test`

Tenemos dos formas de hacer indicar que se haga una operacion logica.  

- `test 10 -gt 20`
- `[ 10 -gt 20 ]`

Tenemos diferentes operadores.  

- `[-d /etc]`: Comprueba si es un archivo o directorio
- `[1 -eq 1]`: iguales
- `[1 -ge 1]`: Mayor o igual
- `[1 -gt 1]`: mayor que
- `[1 -le 1]`: menor o igual que
- `[1 -lt 1]`: menor que
- `[1 -ne 1]`: diferente

Cadenas de texto.  

- `cadena1 = cadena2`: Es importante dejar espacios, ya que si no lo
  hacemos seria una asignacion y no una comparacion
- `cadena1 > cadena2`: es mayor que
- `cadena1 < cadena2`: Es menor que
- `cadena1 != cadena2`: Diferente
- `-n cadena2`: El tamaño de la cedena es mayor a 0

Uso de expreciones OR y AND.  

<!-- negar condicional, uso de condicionar, usar negacion, negacion if negacion, negar if -->

- `!exprecion`: Negacion (ej. `if ! [ -f $JAR_TO_BACKUP ]` o `if ! [[ $? -eq 0 ]]; then` )
- `exprecion1 -a exprecion2`: AND
- `exprecion1 -o exprecion2`: OR

```sh
#!/bin/bash
# if sintaxis if
# if CONDICION; then
#  comandos
# fi

# Ejemplo simple, compatacion de numeros
if [ 1 -ge 1 ]; then # CUIDADO necesita espacios, no tocar corchetes
  echo "Los numeros son iguales"
fi

# comparacion de cadenas
v1="var";
v2="var";
if [ $v1 = $v2 ];
  then echo "Las cadenas son iguales";
  else echo "Las cadenas son diferentes"
fi

# Uso de and, mayor y menor que
num=7;
if [ $num -ge 5 -a $num -le 10 ]; then
  echo "El numero esta entre 5 y 10"
fi
```

Condicional en una linea, condicional en shell condicional

```r
[[ -f "file.x" ]] && echo "True" || echo "False"
```

----

### 72. Test: comprobación sobre ficheros y else

<!-- comprobar que existe el archivo, existe archivo, comprobar que existe el archivo, existe fichero -->
<!-- comprobar que existe el directorio, existe directorio, comprobaciones con archivos, comprobaciones archivos -->

- `-e RUTA`: existe, sea lo que sea?
- `-d RUTA`: existe y es un directorio?
- `-f RUTA`: existe y es un fichero?
- `-r RUTA`: existe y se puede leer
- `-w RUTA`: existe y se puede escribir
- `-x RUTA`: existe y es ejecutable
- `-s RUTA`: existe y su tamaño es mayor a cero

Ejemplo

```sh
#!/bin/bash
read -p "Introduce una ruta: " ruta;

if [ -e $ruta ];
  then echo "Este elemento existe";
  else echo "No existe este elemento";
fi

if [ -d $ruta ];
  then echo "Es un directorio";
  else echo "No es un directorio";
fi

if [ -x $ruta ];
  then echo "Es ejecutable";
  else echo "NO se puede ejecutar";
fi
```

### 73. Comprobación de cadenas con Expresiones Regulares

<!--
regex condicional, condicional con regex, condicional con exprecion regular
condicional regex, condicional exprecion regular exprecion regular en conficional
-->

- Sintaxis1: `[[ cadena =~ regex ]]`
- Sintaxis2: `[[ $var == *texto* ]]`

Recordar que tenemos la variable `$?` donde se guarda el resultado de
la ultima operacion.

Ejemplo.

```sh
> [[ casa =~ cas ]] # sin comillas la exprecion regular
> echo $?
0 # 0 cirgifica que se cumplio

# introducir un s o si, y no vale si empiezan por estas
> [[ "s" =~ ^(s|S|si|Si|SI)$ ]]; echo $? # 0 = si se cumplio
> [[ "silla" =~ ^(s|S|si|Si|SI)$ ]]; echo $? # 1 = no se cumplio

# que contenga la cadena test, por ejemplo "xxxtest", "testxxx" "xxxtestxxx"
if [[ $var == *test* ]]; then
  # . . . .
fi
```

----

### 74. Operaciones con el contenido de una Variable

BASH es capas de realizar operaciones sobre el contenido de una
variables sin recurrir a otros comandos, por ejemplo.

`${#var}` Devuelve la longitud de una cadena que contiene $var.

```sh
a="Mi cadena"; echo ${#a}; # salida: 9
```

`${var:posicion}` De la pocicion indicada hasta al final de la cadena

```sh
a="Mi cadena"; echo ${a:3}; # salida: cadena
```

`${var:posicion:logitud}`: Desde la pocicion indicada, hasta la
longitud indicada, osea, de la pocicion indicada, mas la cantidad de
caracteres indicados

```sh
a="Mi cadena"; echo ${a:1:3}; # salida: i c
```

`${var#patron}`: Elimina desde el inicio, la parte mas corta que
coincida con el patron, si se usa `##` elimina la parte mas larga

```sh
a="Mi cadena para pruebas con"; echo ${a#*cadena}; # para pruebas con
a="192.168.1.158"; echo ${a##*1};                  # 58
```

`${var%patron}`: Igual, pero elimina desde el final del var, tambien
podemos usar `%%` para eliminar desde el final

```sh
a="Mi cadena para pruebas con"; echo ${a%para*}; # Mi cadena
```

`${var/patron/sustitucion}`: Sustituye la primera ocurrencia que
coincida con patron por sustitucuon. Si usamos `//` sustituye todas
las ocurrencias

```sh
a="Mi cadena cadena"; echo ${a/cadena/texto}; # Mi texto cadena
a="Mi cadena cadena"; echo ${a//cadena/texto}; # Mi texto texto

a="Los perros corredores"; echo ${a//os|es/as};
```

### Tareas seccion 14

![08-reto-seccion-14-scripts-basicos](./img/08-reto-seccion-14-scripts-basicos.PNG)

----

----

----

## Sección 18: Estructuras condicionales

### Condicional anidado y uso de AND y OR

Lo podemos hacer como como cualquier otro lenguaje condicional, como
tambien tenemos el if-elseif

```sh
# CUIDADO puede haber error de sintaxis con "sh x.sh" y no con "bash x.sh" condicionales anidados
if [ "$CONFIGM" != "YES" ]
  then
    echo "cancelado"
    exit 0;
fi

# Condicional anidados
if [ 1 != 2 ]; then
  if [ 1 = 1 ]; then
    echo "Condicional anidado";
  fi
fi
# Escrucura else-if-else
if [ 1 != 1 ]; then
  echo "bloque if"
elif [ 1 = 1 ]; then
  echo "bloque else-if";
fi

if [[ ( $A == *test* ) || ( $B == *test* ) ]]; then
  echo "Checar si la variable A ò B contienen test"
fi

if [ 1 = 1 -a 1 = 1 -a 1 = 1 ]; then
  echo "Bloque and-and"
fi;

if [ 1 = 1 -o 1 = 1 -o 1 = 2 ]; then
  echo "Bloque or or";
fi

# ternario
a=$([ "$b" == 5 ] && echo "$c" || echo "$d")
```

----

### 78. Sentencia CASE

```bash
#!/bin/bash
read -p "Introduce tu calificacion 1 al 10: " valor;
case $valor in
  [012345]) # podemos hacer uso de expreciones
    echo "Reprobado";;
  6)
    echo "De pansazo";;
  7)
    echo "Pasaste ok";;
  8)
    echo "bien";;
  9|10) # aqui si hacemos [910] lo tomara como 9, 1, y 0
    echo "sobre-saliente";;
  *) #valor por default
    echo "Nota incorrecta";;
esac
```

----

### 80. Sustitucion o expasion de comandos

Esto permite ejecutar un comando e insertar un valor devuelto por este,
en cualquier otro lugar de otra instruccion, para eso usamos `$()` o
entre comillas invertidas ``

Ejemplos basicos:

```bash
echo "Hoy es: $(date)" # Hoy es: Sat May 30 02:32:50
echo "El directorio actual ocupa: `du -sh .`"

# obtener fecha para archivo, guardar fecha en variable uso de fechas, decharacion defechas
DATE="$(date +"%y%m%d-%H%M%S")"

```

----

### 81. Uso de AND y OR para ejecutar comandos dependiendo del otro

Con estas podemos ejecutar o no un comando dependiendo del resultado
del comando anterior

```bash
# Si falla el primero se ejecutara el segundo, y si se ejecuta
# correctamente el primero NO se ejecutara el segundo
> id pancho &> /dev/null || echo "Usuario pancho no existe"
Usuario pancho no existe

# Solo se ejecuta el segundo comando si el primer comando salio bien.
ping -c1 127.0.0.1 &> /dev/null && echo "Local activo";
```

----

### 83. Depurar un Script
<!-- aser debbug, debug-->

Usamos el argumento x en bash

```bash
/bin/bash -x ./mi-script.sh
```

Tambien lo podemos hacer en la primera linea

```bash
#!/bin/bash -xv
instrucciones...
```

----

### Tareas seccion 18

1. Que diga si se han introducido parámetros o no.  
2. Que genere un número aleatorio y pida al usuario otras dos cifras
   distintas. El script dirá si el número generado es divisible por
   las dos cifras, por una o por ninguna.
3. Que pida la fecha de nacimiento del usuario y le diga cuántos años
   tiene. Para saber la fecha actual se utilizará el comando date
   (consultar las páginas man para conocer el formato de los datos de
   salida)
4. Que pida un ruta y después ofrezca un menú con las siguientes
   opciones:
   - Comprobar si es un directorio
   - Crear el directorio
   - Listar su contenido
5. Dependiendo de la tecla pulsada se realizará una opción u otra
   (usar la estructura CASE)
6. Que pida la posición en la que ha quedado un corredor en un evento y que en función del puesto imprima por pantalla los siguientes mensajes:
   - Si la posición es 1 => “Medalla de oro”
   - Si es 2 => “Medalla de plata”
   - Si es 3 => “Medalla de bronce”
   - Entre el 4 y el 9 => “Tienes un diploma”
   - Entre el 10 y el 19 => “Has quedado muy bien”
   - En otro caso => “Gracias por participar”

----

----

----

## Sección 19: Bucles

### 85. while y for

Sintaxis while

```sh
#!/bin/bash
while CONDICION; do
  INSTRUCCIONES
done

# ejemplo
cout=0;
while [ $cout -lt 5 ]; do
  echo "El contador es: $cout";
  cout=$(($cout+1));
done
```

Sintaxis for

```bash
for VARIABLE in LISTA; do
  INSTRUCCIONES
done

#Ejemplo
#!/bin/bash
for i in $(seq 5 10); do          # imprimira 5 al 10
  echo "Iteracion actual: $i";
done

for doc_txt in $(ls *.txt); do # mostrara los .txt del dir actual
  echo "Archivo de texto: $doc_txt";
done
```

----

### 86. Modificando iteraciones: Break y Continue

Funciona igual que cualquier otro lenguage de programacion

```sh
#!/bin/bash
for i in $(seq 5 10); do
  if [ $i -eq 7 ]; then continue; fi # no se mostrara el 7
  echo "Iteracion actual: $i";
done

for doc_txt in $(ls *.txt); do # Se detendra en el archivo prueba.txt
  if [ $doc_txt = "prueba.txt" ]; then break; fi
  echo "Archivo de texto: $doc_txt";
done
```

Ejemplo: Ver que archivos estan vacios

<!-- recorrer archivos en loop, iterar archivos en loop -->

```sh
for fichero in $(ls .); do # Se detendra en el archivo prueba.txt
  if [ $(du ./$fichero | cut -f1) -eq 0 ]; then
    echo "fichero vacio: $fichero";
  fi
done
```

Ejemplo: Recorer directorios de un directorio: Si queremos recorrer
los directorios dentro de un directorio evitando los archivos, podemos
hacer lo siguiente

<!-- hacer loop sobre carpetas, hacer loop sobre directorios, recorrer un directorio -->

```bash
# Directorio completo
#  ./
#    folder1/
#    folder2/
#    folder3/
#    archivo1.x
#    archivo2.x
#    archivo3.x
for dir in $(ls -d */); do
  NUM_OF_DIR=$(($NUM_OF_DIR+1))
  echo ${dir};
done
echo "Numero de directorios: $NUM_OF_DIR"
# Salida
#folder1/
#folder2/
#folder3/
#Numero de directorios: 3
```

----

### 87. PENDIENTE Ejemplo Renombrar Ficheros ejemplo real

**Problema:** Cuando tenemos una lista de archivos que estan ordenados,
por ejemplo: `e01.md, e02.md, e03.md, e04.md, e05.md` y queremo meter
un nuevo archivo `e03.md` y que el archivo actual `e03.md` pase a ser
el `e03.md` y que todos los demas siguan la numeracion por ejemplo que
el `e05.md` pase a ser el `e06.md`, **EN RESUMEN**, hacer una incercion
de archivos en medio y concervar la numeracion.

```bash
#!/bin/bash
OLDIFS=$IFS;
i=0;
for n in *.md; do
  IFS="-" # creo IFS es una variable de sistema&entorno
  partes=($n);
  
  echo "${$partes[0]}, ${$partes[1]}, ${$partes[2]}"
  i=$(($i+1))
done
IFS=$OLDIFS
```

Mi carpeta y quiero meter `e03 - Ejemplos de b1.md` y que
`e03 - Ejemplos de c.md` pase a ser `e04 - Ejemplos de c.md` y los
demas sigan su numeracion

```r
echo "contenido" > "e01-c01 - Ejemplos de a.md"
e01-c02 - Ejemplos de b.md
e01-c03 - Ejemplos de c.md
e01-c04 - Ejemplos de d.md
e01-c05 - Ejemplos de e.md
```
<!-- PENDIENTE AQUI-->
Ejecutamos el script en la carpeta actual...

----

### Tarea Seccion 19

1. Que pida números y diga sin son pares o impares, hasta que
introduzca el 0 que hará que el programa termine

2. Que adivine el PID del script, diciendo en cada momento si es
mayor o menor que el número introducido. Cuando se acierte terminará
y mostrará cuantos intentos se han necesitado

3. Que diga qué ordenadores de tu red de clase C están conectados. Es
decir, hará ping desde la 192.168.X.1 hasta la 192.168.X.254 siendo X
un número que indica la subred en la que te encuentras.

----

----

----

## Sección 20: Funciones

- Sintaxis

```bash
function nombre {
  INSTRUCCIONES
}

#!/bin/bash
function con_parametroe {
  echo "Hola mundo soy: $1"; # se hace igual que los scrips
}

con_parametroe mario # dentro del mismo script
```

### 90. Ejemplo Avanzado. mostrar información de usuarios

**Objetivo:** Vamos a crear una funcion que nos listara los usuarios y
nos mostrara informacion como

- Espacio usado por su directorio peroonal
- Si esta actualmente logueado, y si no cual fue la ultima vez que lo
  hizo

A. agregar un grupo: `addgroup alumnos`

<!-- -d=directorio-personal, -m=crea-el-dir-si-no-existe-->
B. agregar usuarios: `useradd -d /home/pepe -m -g alumnos pepe`

B-1. Agregar contraseña a usuario: `passwd pepe`

C. Ver usuarios creados: `tail /etc/passwd`

```r
pepe:x:1001:1001::/home/pepe:/bin/sh
ana:x:1002:1001::/home/ana:/bin/sh
juan:x:1003:1001::/home/juan:/bin/sh
liz:x:1004:1001::/home/liz:/bin/sh
```

D. Ver id grupo: `tail /etc/group`

<!-- crear archivo vacio, crear archivo de relleno, archivo relleno, crear archivos vacios, crear archivos dummy -->
<!-- of: Donde se creara, bs: que tamaño, count: Cuantos archivos-->
<!-- usar expreciones regulares en grep, usamos egrep, expreciones regulares en grep-->
C. Crear archivos de x tamaño: `dd if=/dev/zero of=/home/pepe/fichero bs=9M count=1`

```bash
#!/bin/bash
function get_user_info {
  IFS=":" # cambiamos el separador default " " (espacio) por ":"
  user=($1);  # se asignara un arreglo con separacion por ":"
  echo ${user[0]};
  echo "Espacio usado por su directorio ${user[5]}:" $(du -sh ${user[5]} | cut -f1);
  
  # who saber quien esta en el sistema, saber quien esta logueado en el sistema
  # \b separador de palabra
  
  if $(who | egrep "^${user[0]}\b" &> /dev/null); then
    echo "Actualmente logueado";
  else
    # saber ultimo login de usuario, fecha ultimo login
    # -s> eliminar repeticiondes del lo indicado
    acceso=$(grep opened.*${user[0]} /var/log/auth.log | tail -n1 | tr -s " " | cut -d" " -f1-3);
    if [ -z "$acceso" ]; then
      echo "Nunca se ha loguado";
    else
      echo "Ultimo acceso: $acceso";
    fi
  fi;

  echo -e "\n"
  IFS=" "; # regrefsmos al default pa no tener pex con sig. comandos
           # ya que se podrian ver afectados
}

read -p "Introdusca el nombre del grupo: " grupo;
# filtramos las lineas que contengan al inicio esa palabra usando una
# regex en el archivo donde estan los id de los grupo
gid=$(egrep ^$grupo /etc/group | cut -d":" -f3)

# \w palabra repetida una o mas veces
usuarios=$(egrep "^\w+:x:\w+:$gid:" /etc/passwd)

# -z: esta vacia la cadena?, ver si una cadena esta vacia, comprobar si una cadena es vacia
if [ -z "$usuarios" ]; then
  echo "No se han encontrado usuarios"; exit 1;
fi

for u in $usuarios; do
 get_user_info $u                # usamos la funcion info que creamos nosotros
done;
```

**Resultado:**> Al introducir el nombre del grupo, podemos ver sus
usuario, al igual que su informacion de, A) espacio ocupado en dir
B) Si esta logueado actualmente, C) si no esta loguado cuando fue
su ultimo acceso, y D) Avizar si nunca se ha logueado

![09-script-info-usuarios](./img/09-script-info-usuarios.PNG)

----

### Tarea, Seccion 20

![10-tarea-funciones](./img/10-tarea-funciones.PNG)

----

----

----

## Sección 21: Conocimientos útil

### 92. Compactar y comprimir ficheros y directorios: TAR

Sirve para compactar varios ficheros y diferectorios en 1 solo archivo.

- Sintaxis: `tar [opciones] destino.tar datos`
- Otras opciones importantes.
  - `-c`: Compacta, create
  - `-x`: extrae
  - `-f`: Escribe o lee un fichero
  - `-z`: Comprime o descomprime con gunzip
  - `j`: Comprime o descomprime con bzip2
  - `-P`: Utiliza rutas anbsolutas al descomprimir (Cuidado)
  - `-p`: Perserva los permisos de los ficheros originales
  - `-r`: Añade elementos a un fichero ya compatcado.
  - `-t`: mustra la informacion que contiene un fichero tar

Por ejemplo.

<!-- uso de tar -->

```sh
# Comprimir toda la carpeta /etc/ y /var/
> tar -cf fichero.tar /etc/  /var/

# Extraer un fichero tar
> tar -xf fichero.tar
```

Una forma mas estandart de comprimir archivos es usando gzip, el cual
fuenciona igual solo agregamos en los argumentos una `z` en medio

```bash
# comprimir
> tar -czf fichero.tar.gz /etc/  /var/
# extraer
> tar -xzf fichero.tar.gz
```

**IMPORTANTE**: tar puede descomprimir respetando las rutas absolutas lo
que puede ser PELIGROSO, ya que podemos sobre escribir algo importante
por lo que usamos la opcion `-C /some/path .` (el punto es importante)
el cual el comprimido tendra de forma explisita que debe descomprimir
el `.` lo que significa la carpeta actual.

```r
> tar -czf /some/path/some_file.tar.gz -C /some/path .
```

Tambien podemos hacer diferentes tipos de copias de seguridad, con
diferentes opciones

- **Totales**: Se copian todos los archivos
- **Diferenciales**: Se copian solo los que se hayan modificado desde una
  fecha indicada. Usando la opcion `N` segudo de la fecha
- **Incrementales**: Se cpopian solo los archivo que se han modificado
  desde la ultima copia. Se utiliza la opcion -g seguido de la ruta del
  fichero de registro

----

### 93. TAR, ejemplos de uso

```bash
# no tendra ninguna comprecion en el tamanio
> tar -cf copia.tar ana/ mario/

# los archivos pesaran menos
> tar -czf copia.tar.gz ana/ mario/

# des-empaquetar
> tar -xf copia.tar

# descomprimir, con "v" para que nos muestre que esta
# descomprimiendo, igual funciona para descomprimir
> tar -xzvf copia.tar.gz
```

----

### 94. Programar tareas: CRON

Para automatizar tareas este es muy importante, que es un demonio
que se ejecuta al iniciar el sistema, para iniciar cuando se van a
ejecutar tareas repetitivas

```bash
#Podemos ver las carpetas del cron en
> ls /etc/cron* -ld
/etc/cron.d
/etc/cron.daily
/etc/cron.hourly
/etc/cron.monthly
/etc/crontab
/etc/cron.weekly
```

Los cuales su nombre nos indica cada cuanto se ejecutan
los scripts que estan dentro, y podemos nosotros agregar un script
propio para que se ejecute cada x tiempo.

Si no nos acomoda ningana de las carpetas podemos **Personalizar**
el momento exacto en el que queremos ejecutar nuestro script.
podemos hacer lo siguiente.

```bash
> crontab -l # ver crons creados

> crontab -e # crear cron
# nos aparecera como editar un archivo, comentado con ayuda, donde
# hay ejemplo: tipo  0 5 * * 1 tar -zcf /backups/home.tgz /home/
```

- Sintaxis Cron: `[cuando] [comando] ò [cuando] [script]`

- Donde `0 5 * * 1`
  - primero: es minuto, ej. 0=hora en punto
  - segundo: Es la hora, ej. 5=5am
  - tercero: El dia del mes, ej. \*=todos los dias, 1=primero de cada mes
  - cuarto: mes, 1=enero
  - quinto: dow(day of week) 1=lunes
  - sexto: el comando o script que queremos ejecutar

----

### 95. Ejemplo CRON y TAR: programar copias de seguridad

Tenemos diferentes tipos de respaldo las cuales combinaremos segun
nuestras necesidates.

- **Respaldo total:** Garantiza todos los datos y es comoda por que
  solo se maneja un fichero, pero ocupa demaciado espacio
- **Respaldo diferencial:** En un archivo se va a agregando los
  archivos modificados, por ejemplo, cada semana, donde tendremos
  cada semana informacion repetida.
- **Respaldo incremental**: Solo se copian los archivos que se han
  modificado, pero por cada respaldo se crea un nuevo archivo, solo
  con los archivos modificados, por lo que con el tiempo, puede
  llegar a ser incomo manejar el respaldo.
  
Lo mejor es combinar las estrategias, por ejemplo, hacer una copia
total cada mes, hacer una incromental cada semana, y una diferencial
toda la semana.

**Objetivo:** Vamos a programar un respaldo que combinando las
estrategias como indicamos en el parrafo anterior.

<!-- ejemplo fechas, date ejemplo, uso de date, usar date-->

- `date +%F` nos dara la fecha en el sig. formato. `2018-06-21` el
cual nos sera util, para ordenar archivos.

- `date +%Y-%m-%d--%H-%M-%S` nos dara los siguiente: `2020-09-29--05-36-56`

- `date +anio%Ymes%mdia%dhora%Hmin%Mseg%S` Nos data `anio2020mes09dia29hora05min38seg44`

A. Creamos cron `crontab -e`

```bash
# al minuto cero, a las 11pm, mes 1, dia 1, dow cualquiera, (respaldo cada año)
0 23 1 1 * tar -czf /var/respaldos/completo_$(date +\%F).tar.gz /home

# a las 11:00pm, todos los meses, todos los dias, el sabado=6 (respaldo mensual)
0 23 * * 6 tar -czf /var/respaldos/diferencial_$(date +\%F).tar.gz /home/ -N $(date +%Y-%m)-01

# a las 11:00pm, todos los dias del mes, de todos los meses,
# entre semana y los domingos, por que el sabado se hace el diferencial
0 23 * * 1-5,7 tar -czf /var/respaldos/incremental_$(date +\%F).tar.gz /home/ -g /var/respaldos/backup.logs
```

----

### 96. Introducir texto en programas interactivos

Cuando hacemos un `cat > file` la terminal se **queda esperando**
de que nosotros le introduscamos texto, para hacer esto lo hacemos
usando `EOF` para simular la entrada interactiva desde un script.

Ejemplo.

<!-- comando para crear archivo de varias lineas -->

```bash
#!/bin/bash
cat > file.txt <<EOF
alguna frace la que tenga
dos parrafos por que yo quiero
EOF
```

Si ejecutamos este script veremos como se crea el archivo con el
texto que indicamos.

En aplicaciones practicas lo podemos usar en.

- conectar a ftp por `ftp -n 192.x.x.x`, el cual luego nos pedira
usuario y contraseña, para luego los comandos necesarios para subir
o descargar archivos.

```bash
#!/bin/bash
ftp -n 192.x.x.x <<EOF
user miUsuario miPassword
lcd /var/respaldos # me muevo de carpeta
mput file-a-subir.tar # subimos archivo
bye # salimos
EOF
```

Si ejecutamos se subira el archivo file-a-subir.tar al servidor FTP
que indicamos, igual podemos indicar `*.tar` para subir todos.

**Nota:** Si queremos añadir espacios al inicio, para que se vea mejor
**no se podra solo tabulaciones** y tendremos que agregarle un guion
para que los elimine, ejemplo.

```bash
#!/bin/bash
cat > file.txt <<-EOF # IMPORTANTE EL GUION
  alguna frace la que tenga
  dos parrafos por que yo quiero
    tercer parrafo
  cuarto linea de texto
    quienta linea
EOF
```

----

### 97. El Separador Interno entre Campos. $IFS

Linux por defecto usa el espacio " " para separar los datos, por
ejemplo cuando menetemos un ejemplo "comando dato1 dato2" linux esta
programado para separar los dato1 y dato2 por el espacio, ya que es
el que esta por defecto.

Pero para nuestros scripts podremos modificar este para hacer nuestro
trabajo mas facil, como **ya lo hemos hecho** en scripts anteriores,
por ejemplo.

```bash
#!/bin/bash
entrada="nombre apellido1 apellido2" # tambien funcionara con tab y salto de linea
for e in $entrada; do
  echo $e
done

---- Ejecucion
> ./script-separador-IFS.sh
nombre
apellido1
apellido2
```

Vemos que ha separado los campos, pero habra veces que este no sera
nuestro caso, poe ejemplo, tendremos los datos deparados por comas
o dos puntos o etc, lo cual

**Recomendacion:** para no meternos en lios con futuros comandos
usariamos creariamos una varible `OLDIFS` para cuando terminemos
de usarlo, lo re-establescamos al default.

```bash
#!/bin/bash
OLDIFS=$IFS; # +
IFS=":";     # +
entrada="nombre:apellido1:apellido2" # tambien funcionara con tab y salto de linea
for e in $entrada; do
  echo $e
done
IFS=$OLDIFS; # +
```

Nos dara el mismo resultado.

----

----

----

## Mis notas

### Buscar en logs

<!-- mostrar logs -->
Ver los logs en vivo: ``` tail -f path/files* ```

Buscar en los logs en varios archivos en vivo y separacion

```bas
tail -f path*/files* | grep -B 4 -A 400 "TEXTO_A_BUSCAR" --group-separator=$'\n#-SEPARADOR-#'
```

<!-- Repetir comando cada x tiempo sin bash, -->

<!-- checar uso de ram, consumo de ram, ver cuanto consume de ram, estatus ram -->

Buscar en multiples logs cada x segundos, por ejemplo ver que instancias reiniciaron cada 10 segundos buscando e-busisnes

```r
# estructura basica
while true; do xxxxxxxx  sleep 10; done;
# Ejemplo busqueda en texto
while true; do now=$(date); cut -d{caracter} -f1 ruta/archivos* | grep "TEXTO_A_BUSCAR";  sleep 10; echo '#Time' $now ' ####';done;
# Ejemplo checar recursos
while true; do free -t | awk 'NR == 2 {printf("Current Memory Utilization is : %.2f% \n"), $3/$2*100}'; sleep 1; done;
```
  
Igual que arriba pero solo una vez por ejemplo

```r
cut -d[caracter] -f1 rutas*/file | grep e-bus | grep "TEXTO_A_BUSCAR"
```

Buscar en las trazas y mostrar la linea, lineas adelante o lineas adelante atras

```r
grep -A 100 "TEXTO_A_BUSCAR" instancias*/files* --group-separator=$'\nSEPARADOR'

grep -B 50 -A 100 "XXX" paths*/files* --group-separator=$'SEPARADOR'

grep -Rn "XXX" paths*/SystemOut.log

tail -200 path/file.log #Ver ultimas X lineas de Logs

```

----

### Uso de SCP

Descargar archivos: `scp -r -P22 usuario@x.x.x.X:/ruta_host/ X:\ruta_host/`

Subir archivos, en este ejemplo subimos de windows a linux

```r
# subir un archivo
scp -r -P22 W:\path\file.class user@x.x.x.x:/path_host/

# Subir carpeta
scp -r -P22 nombre_carpeta user@x.x.x.x:/path_host/
```

----

### Registrar temperatura

Usamos el comando `sensors` que lo podems}os instalar con ``

```r
while true; do sensors | grep "temp1:" | tr -s " " | cut -d" " -f2,3 | sed -e 's/(high/,/' | sed -e 's/(crit//' | tr "\n" " " | sed -e '1a\n' | sed -e '1i\n' | grep + | tr -d " " &>> temperaturas.log; sleep 1; done;
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

## Seguridad

### ssh

Ver logs de intento de inicio de sesion, checar logs de inento de logins,

/var/log/auth.log

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

## Por el camino

<!-- 
Ver los procesos en ejecucion, ver los programas en ejecucion, ver PID de programa
ver PID de proceso, listar procesos corriendo, ver qu esta corriendo
-->

- Fuente: [Link](https://www.hostinger.com/tutorials/vps/how-to-manage-processes-in-linux-using-command-line)

- Listar procesos en ejecucion: `ps`

- Lista de procesos con menu de opciones: `top`

### Errores inesperados al ejecutar script

> Error: "unexpected end of file"  
> Solucion: `dos2unix file`

### Historial de comandos

- Fuente: [link](https://askubuntu.com/questions/624848/view-history-of-commands-ran-in-terminal)

Podemos hacer uso de las dos siguientes opciones, dependendo de la
distribucion algunas serviran algunas otras no

```bash
> cat ~/.bash_history
> history
```

### Crear panel de control y/o vigilar procesos archivos que nos interesa

Podemos tener una especie de panel de control, o vigilar los procesos
que nos interesan, lo podemos hacer por medio de un comando `while` que
se repita cada determinado tiempo y que limpie lo anterior.

Por ejemplo, el siguiente comando podemos ver la sig. info

- Los arvhivos ver si se agregan o se eliminan en "tiempo real"
- El numero de procesos java que tenemos
- Los programas java que estamos ejecutando

```r
while true; do ls -l; ps | grep "java" | wc -l; ps | grep "$(ls *.jar)" | grep -v "grep" | grep -v ".sh"; sleep 1; clear; done;
```

### Script para al cambiar jar detener el anterior y ejecutar el nuevo

<!-- PENDIENTE: Pasar lista a texto buscable -->

1. obtener pid de proceso
2. limpiar string
   1. eliminar letras de numero
   2. eliminar espacios en blanco al inicio y final
3. cerrar aplicacion correctamente

<!-- 

"trim()" quitar espacios, limpiar espacios, borrar espacios, eliminar espacios

-->

```bash
#!/bin/bash

logwatch="watch-file.log"
logApp="app.log"
now="$(date +"%m-%d-%y--%T")"
# my-app.jar
jarname=$1

# el comando ps retorna el PID con unos espacios al inicio asi que esta
# funcion nos ayudara a hacer trim para que no nos afecten estos espacios
trim() {
    local var="$*"
    var="${var#"${var%%[![:space:]]*}"}"
    var="${var%"${var##*[![:space:]]}"}"
    printf '%s' "$var"
}

# obtenemos el PID a travez del numbre del jar que se esta ejecutando
# en la parte de "filtrado="$(echo..." ahi casos en el que obtenemos
# el PID pegado al nombre de usuario por ejemplo "123456root" y con este
# estamos dejando solo los caracteres numericos
# ======================
# 'tr -s " "': quitar espacios tuberia quitar espacios
function getPID {
  pid_java="$(ps | grep "$jarname" | grep -v "grep" | grep -v "watchjar.sh" | head -n1 | tr -s " " | xargs | cut -d " " -f 1)"
  filtrado="$(echo $pid_java | tr [:alpha:] ' ')"
  pidFiltrado="$(trim $filtrado)"
  if [ -z "$pidFiltrado" ];
    then echo "" > /dev/null
    else echo "$pidFiltrado" # retornamos el PID
  fi
}

function myPrint {
  message=$1
  echo "$now $message" >> $logwatch
}

# comprobar si una cadena esta vacia, ver si cadena es vacia, checar si una cadena es vacia
# checar si una cadena esta vacia.
if [ -z "$jarname" ];
  then
    echo "falto nombre del jar a ejecutar"
    exit
fi

# Comprobamos si existen archivos jar, y para eviar problemas enviamos
# el posible mensaje de no encontro al vacio
a="$(ls *.jar 2> /dev/null)"
if [ -z "$a" ];
  then
    echo "No se encontro ningun jar"
    exit
fi

myPrint "ejecutando $jarname"
java -jar $jarname >> $logApp &

while true; do
    sum1="$(md5sum "$(ls *.jar)")"
    sleep 2
    sum2="$(md5sum "$(ls *.jar)")"
    if [ "$sum1" != "$sum2" ];
      then
        myPrint "$now Cambio ejecutable"
        pid="$(getPID)"
        if [ -z "$pid" ];
          then
            myPrint "No se encontro PID de aplicacion java en ejecucion"
          else
            # detenemos
            myPrint "$now Terminando ejecutable $pid..."
            kill $pid
            sleep 5
            # iniciamos de nuevo
            existeLog="$(ls -l | grep "app.log$")"
            if [ -z "$existeLog" ];
              then
                myPrint "No existe app.log"
              else
                killed="$(grep "Shutdown completed" $logApp)"
                if [ -z "$killed" ];
                  then
                    # myPrint "$now El proceso no se a eliminado"
                    echo "" > /dev/null
                  else
                    myPrint "$now Termino ejecutable, iniciando nuevo jar "
                    mv app.log "$logApp-$now"
                    java -jar $jarname >> $logApp &
                fi
            fi
        fi
      else
        # myPrint "$now No a cambiado el archivo"
        echo "" > /dev/null
    fi
done
```

----

### Cambiar zona horaria

<!-- cambiar fecha -->

Para ubuntu y parecidos

- Fuente: [Link](https://itsfoss.com/change-timezone-ubuntu/)

- Podemos ver las zonas disponebles con `timedatectl list-timezones`

- Asignamos una con: `sudo timedatectl set-timezone America/Mexico_City`

para verciones alpine tipo contenedores de docker

- Fuente: [Link](https://wiki.alpinelinux.org/wiki/Setting_the_timezone)

<!-- instalar en linux basico, instalar sin apt-get, alternativa a apt-get-->

1. Descargamos zonas horarias: `apk add tzdata`
2. Buscamos la nuestra: `ls /usr/share/zoneinfo`
3. Suponemos que es **America/Mexico_City** copiamos: `cp /usr/share/zoneinfo/America/Mexico_City /etc/localtime`
4. cambiamos la zona `echo "America/Mexico_City" >  /etc/timezone`
5. Comprobamos: `date`
6. Podemos quitar las demas zonas: `apk del tzdata`

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Explorar estructura de folders

<!-- 
listar contenido de carpetas, listar contenido de folders, listar contenido de directorios
ver estructura de carpetas, ver estructura de folders, ver estructura de directorios
tree, alternativas a tree, arbol de archivos, arbol de carpetas, estructura de archivos
-->

- Fuente: [Link](https://www.cyberciti.biz/faq/how-to-show-recursive-directory-listing-on-linux-or-unix/)

tenemos varias formas de mostrar la estructura de una carpeta

 ```r
# =========================================
> du -a pagina/
4       pagina/dos/index.html
8       pagina/dos
4       pagina/index.html
16      pagina/
# =========================================
> find . -print
.
./pagina
./pagina/dos
./pagina/dos/index.html
./pagina/index.html
# =========================================
> find ./pagina -print
./pagina
./pagina/dos
./pagina/dos/index.html
./pagina/index.html
# =========================================
> tree .
├───.3T
│   └───robo-3t
│       └───1.3.1
│           └───cache
├───.android
│   ├───avd
│   ├───breakpad
│   ├───build-cache
│   │   └───3.5.1
│   ├───cache
│   ├───metrics
│   │   └───spool
│   └───studio
│       └───installer
# =========================================
 ```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Uso de apt

<!-- desinstalar programas, quitar programa,  -->

Desinstalar paquetes: `apt-get remove nombre_paquete`

> **Error** Unable to locate package  
> **Descripcion:** cuando intentamos instalar un paquete nos muestra
> este error y no se instala el paquete
> **Causas:** 1, no tenemos actualizado nuestro repositorio. 2, tenemos
> mal configurado nuestro repo y necesitamos agregar fuentes.
> **Solucion1:** Actializar repositorio `apt-get update`

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Checaar puertos

<!-- 
mostrar puertos activos, listar puertos activos, ver puertos activos 
-->

Podemos ver que puertos estan escuchando nuestro **servidor en el que estamos**
y poder filtrar con grep aquellos servicios que nos interesa

```r
netstat -tlpn | grep ftp
OR
ss -tlpn | grep ftp
```

mostrar el PID que esta ocupando X puerto

```r
sudo ss -lptn 'sport = :80'
```

<!-- 
checar comunicacion. revizar comunicacion, comprobar comunicacion
asegurar comunicacion, verificar comunicacion. ver comunicacion

checar puertos. revizar puertos, comprobar puertos, asegurar puertos,
verificar puertos. ver puertos
-->

- Fuente: [Link](https://linoxide.com/linux-how-to/install-use-netcat-command-linux/)

podemos igual checar los puertos de un **servidor remoto** desde un
equipo cliente.

1, Instalar netcat: `apt install netcat`

2, Ver comunicacion de un puerto: `nc -vn 192.168.40.146 2424`

3, Comunicacion en de x a y puerto: `nc -vnz -w 1 192.168.40.146 20-25`

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### obtener ruta del script en ejecucion

<!-- 
obtener la ruta del script, ejecutar los comandos del script en su ruta
trabajar en la ruta del script, usar la ruta del script,
conseguir ruta del script em ejecucion
-->

```r
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### instalar cliente postgres

1, Agregar link a repositorio

```r
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" |sudo tee  /etc/apt/sources.list.d/pgdg.list
```

2, Instalar Cliente postgres: `sudo apt update && sudo apt -y install postgresql-client-12`

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Crear folders de prueba

<!-- crear directorios de pruebas, crear archivos dummy, crear diferectorios de test -->

```r
mkdir -p test/subdir && echo "raiz" > raiz.txt && echo "subDir" > test/algo1.txt && echo "SubSubDir1" > test/subdir/sub.log
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Detectar sistema operativo en bash script

<!-- 
detectar sistema operativo, ver sistema operativo, saber el sistema operativo
detectar so, ver so, saber so, detectar s.o., ver s.o., saber s.o.
-->

- Fuente: [Link](https://stackoverflow.com/questions/394230/how-to-detect-the-os-from-a-bash-script)

Puede que nuestros queramos ejecutar nuestro script en diferentes S.O.
y este script no podra funcionar correctamente en diferentes, S.O. por
lo que podemos agregar diferentes bloques para un SO en espesifico.

Tenemos la variable global `$OSTYPE` que tiene el nombre del s.o.

```sh
case "$OSTYPE" in
  solaris*) echo "SOLARIS" ;;
  darwin*)  echo "OSX" ;;
  linux*)   echo "LINUX" ;;
  bsd*)     echo "BSD" ;;
  msys*)    echo "WINDOWS" ;;
  *)        echo "unknown: $OSTYPE" ;;
esac
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Mover solo contenido de carpeta

<!-- 
mover solo archivos de carpeta, mover archivos de carpeta, mover solo los archivos de carpeta, mover los archivos de carpeta, mover contenido de carpeta
mover solo ficheros de carpeta, mover ficheros de carpeta, mover solo los ficheros de carpeta, mover los ficheros de carpeta,
-->

Si queremos mover el contenido de una carpeta a otra sin mover la
carpeta solo el contenido entonces tenemos la sig. opcion

```r
mv -v ./dir_con_contenido_que_nos_insteresa/* .
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Asignar una IP estatica

En Ubuntu

- Fuente: [Link](http://itprohelper.com/setting-up-static-ip-on-ubuntu-server-20-04-lts/)

1, Ver que interface usar `ip a`

En mi caso imprimio lo siguiente, y me di qcuenta que es la `enp0s3` ya
que es la que se usa para conectarme cpn mi equipo

```r
2: enp0s3: <BROADCAST...> mtu ...
    link/ether ...:fc brd ...
    inet 192.168.1.80/24 brd 192.168.1.255 scope global dynamic enp0s3
```

2, Asegurar que no tenemos algo ya configurado:

Este archivo; `/etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg`
debe de tener nanamas`etwork: {config: disabled}`

3, Configuramos: `sudo nano /etc/netplan/00-installer-config.yaml`

<!-- Copie el gateway del ipconfig de mi maquina -->

```yml
network:
  version: 2
  ethernets:
    enp0s3:
      addresses: [192.168.1.80/24]
      dhcp4: false
      gateway4: 192.168.1.254
      nameservers:
        addresses: [192.168.1.254, 8.8.8.8]
```

4, Aplicamos temporalmente: `sudo netplan try`

5, Ya que vimos que no ahi un error aplicamos permanentemente: `sudo netplan apply`

6, Comprobamos, aun nos debeveremos poder conectar por ssh incluso si
reiniciamos el equipo.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Comparar dos archivos de texto

<!-- comparar dos archivos, comparar 2 achivos, comparar linea por linea, ver si dos archivos son iguales
vir si dos archivos son identicos, comparar texto, -->

Fuente: [uso de cmp](https://stackoverflow.com/questions/12900538/fastest-way-to-tell-if-two-files-have-the-same-contents-in-unix-linux)

Tenemos dos formas de comparar archivos por medio de su md5 o compaarlos
linea por linea

A, Comparar linea por linea

```r
cmp file1.x file2.x
cmp --silent file1 file2 || echo "files are different"
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Uso de ps (ver procesos)

<!--
ver procesos activos, ver programas activos, listar procesos activos, listar programas activos
mostrar procesos activos, mostrar programas activos, mostrar los procesos activos, mostrar los programas activos,
como usar ps, uso ps, ver todos los procesos activos
-->

- Nota: Tambien tenemos el comando `htop` el cual nos ha una GUI mas
  amigable para ver los procesos y recursos.

ps nos sirve para enseñar los procesos activos, pero hay que tener
cuidado ya que por defecto no muestra todos los procesos, por ejemplo.

```r
# Muestra los procesos activos
> ps
 PID TTY          TIME CMD
1396 pts/0    00:00:00 bash
1568 pts/0    00:00:00 ps

# mustra los procesos activos y los detalles de este
> ps l
F   UID     PID    PPID PRI  NI    VSZ   RSS WCHAN  STAT TTY        TIME COMMAND
4     0    1396       0  20   0   5748  3592 do_wai Ss   pts/0      0:00 bash
0     0    1571    1396  20   0   9304  1216 -      R+   pts/0      0:00 ps l

# muestra tolos los procesos
ps l -e
F   UID     PID    PPID PRI  NI    VSZ   RSS WCHAN  STAT TTY        TIME COMMAND
4     0       1       0  20   0   4076     0 hrtime Ss   ?          0:03 tail -f /dev/nul
4     0     180       1  20   0      0     0 -      Z    ?          0:27 [java] <defunct>
1     0     181       1  20   0      0     0 -      Z    ?          0:00 [bash] <defunct>
0     0    1359       1  20   0 2598468 164184 futex_ Sl ?          0:41 java -jar -Dspring.profiles.active=test my.jar
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Generar String aleatorio

<!-- generar palabra aleatoria, generar string random string, cadena aleatoria -->

Con el siguiente comando podemos generar un string random, donde

- `/dev/urandom` programa que generara el aleatorio
- `tr -dc A-Za-z0-9` los caracteres que queremos en este caso minus,
  mayus y numeros

```r
# comando
> head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo ''

# script
# cUIDADO: La variable $RANDOM existe y tiene un valor estatico, por lo que no
# podemos usarlo como nombre de variable
RANDOM_NAME=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 5)
echo "$RANDOM_NAME"
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Uso de variable de entorno

Para crear variables que posamos usar eun nuestros scrips o comandos,
podemos crearla de la sig. manera

```r
> export HOST="test"
> echo $HOST
< test
```

Eliminar variable de entorno, borrar variable de entorno

```r
unset HOST
```

Para que sea permanente agregamos el export en `~/.bashrc`

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Incrementar o crear la memora swap

<!-- aumentar la memoria swap, añadir memoria swap, -->

- Fuente: [askubuntu](https://askubuntu.com/questions/927854/how-do-i-increase-the-size-of-swapfile-without-removing-it-in-the-terminal)

```r
#Check if there is any swap memory currently
sudo swapon -s

# First disable swap file:
sudo swapoff /swapfile

# Now let's increase the size of swap file:
sudo dd if=/dev/zero of=/swapfile bs=1M count=1024 oflag=append conv=notrunc
# The above command will append 1GiB of zero bytes at the end of your swap file.

# Setup the file as a "swap file":
sudo mkswap /swapfile

# enable swaping:
sudo swapon /swapfile

#Check that it's ok
sudo swapon -s
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Esperar hasta podese conectar a una IP y puerto

**Objetivo:** Esperar hasta que se pueda conectar a una IP y puerto que
definamos

```bash
function wait_until_can_connect_to_ip_1_port_2 {
  COUNT=1;
  TIME_OUT=40;
  until nc -z $1 $2
  do
    echo "waiting for $1:$2 $COUNT/$TIME_OUT ...";
    COUNT=$(($COUNT+1));
    if [ $COUNT -gt $TIME_OUT ]; then
      echo "time out";
      break;
    fi
    sleep 1;
  done
}

wait_until_can_connect_to_ip_1_port_2 127.0.0.1 80
echo "fin"

waiting for 127.0.0.1:80 ... 1/X
waiting for 127.0.0.1:80 ... 2/X
waiting for 127.0.0.1:80 ... 3/X
fin
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Esperar hasta encontar algo en los logs

Fuente: [stackoverflow.com](https://stackoverflow.com/questions/25959870/how-to-wait-till-a-particular-line-appears-in-a-file)

```sh
function wait_until_container_1_logs_2 {
  print_debug "waiting for '$2' in logs of $1..."
  ( docker logs -f $1 & ) | grep -q "$2";
}

function wait_until_file_1_logs_2 {
  print_debug "waiting for '$2' in logs of $1..."
  ( tail -f $1 & ) | grep -q "$2";
}

wait_until_container_1_logs_2 some_contaier_name "something to search"
wait_until_file_1_logs_2 some.log "something to search"
echo fin

# RESULTADO
waiting for 'something to search' in logs of some_contaier_name...
waiting for 'something to search' in logs of some_contaier_name...
waiting for 'something to search' in logs of some_contaier_name...
fin
```

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### Importar variables de .env

<!-- usar .env -->

**Objetivo:** De un archivo de variables .env, vamos a usarlo en un script
bash, para poder usar sus variables en el script.

Fuente: [github.com/mihow](https://gist.github.com/mihow/9c7f559807069a03e302605691f85572)

A, Archivo .env

```r
ENV="XXX"
IP="X.X.X.X"
```

B, archivo .sh

```bash
set -o allexport; source some_path/.env; set +o allexport;
echo $ENV;
echo $IP;
#Resultado
XXX
X.X.X.X
```

B,2, Da lo mismo hacerlo por separado

```bash
#!/bin/bash
set -e 
source .env
echo $ENV;
...
```

### Apariencia de la terminal

a, Cambiar como se ve la linea que aparace al inicio

```js
export PS1="\[\]\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\] (\!)[\$(date +%k:%M:%S)]\n\$> "
```

### Descargar archivo usando url

<!-- descargar de link, descargar-->

Usando curl

```r
curl http://x.com/file.zip --output path/file.zip
```

Usando wget

```r
wget -P /path/host http://x.com/file.zip
```

### DNS

#### Checar respuesta de DNS en consola

```r
nslookup www.example.com

dig www.example.com
```

#### "Redirigir" ej.com a www.ej.com

**Importante:** realmente no se redirige se tienen dos formas de dominio,
es decir que no se compartiran datos entre ej.com y www.ej.com,
se recomienda solo usar el www, por temas de SEO y estabilidad, por lo
que la **solucion** seria implementar del lado del servidor, que cuando
se detecte una peticion no-www redirigirla a una www

Para esto se usa un registro tipo alias, en mi caso que tengo mi dominio
en `porkbun.com` lo configure de la siguiente manera.

```r
type: A
host: www.artesaniastlaxco.com
answer: 192.53.166.42
ttl: 600
-------------
type: ALIAS
host: artesaniastlaxco.com
answer: www.artesaniastlaxco.com
ttl: 600
```

#### SRV record

Este tipo de DNS no sirve para crear por ejemplo gitea.example.com para
hacer una peticion a www.example.com:8080 o 1.2.3.4:8080, ya que el
navegador no soporta este tipo de dns, lo soportan solo programas que
hace un tipo especial de peticion en busca del srv, por ejemplo, minecraft
