<center>
<h1> DevOps, Operaciones Sistemas, Linux, Bash</h1>
</center>




Conectarse a host remoto o virtual

```ssh mario@192.168.1.100 ```


<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
___
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<!-- 
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################

############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################

############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
-->








































































































































































# Actividades


### Instalar FTP ubuntu server

Fuente: https://devanswers.co/installing-ftp-server-vsftpd-ubuntu-18-04/

1. instalar ftp `sudo apt update && sudo apt install vsftpd`

1.2. ver estatus `sudo service vsftpd status`

1. Configuramos firewall por seguridad

```shell
# abrimos puertos para permitir ssh y ftp
sudo ufw allow OpenSSH
sudo ufw allow 20/tcp
sudo ufw allow 21/tcp
sudo ufw allow 40000:50000/tcp

#habilitamos
sudo ufw enable

# vemos estatus
sudo ufw status
```

3.1. Creamos usuario para ftp `sudo adduser clienteapp`

3.2. Prohibimos el acceso de nuestro usuario root a ftp por seguridad

```bash
sudo nano /etc/ssh/sshd_config
# agregamos
DenyUsers mario
```

<!-- reiniciar ssh -->
3.3. Reiniciamos ``` sudo service sshd restart ```

5. Editamos el archivo ``` sudo nano /etc/vsftpd.conf ``` 

	5.1 Creamos respaldo de la configuracion de ftp> ``` sudo cp /etc/vsftpd.conf /etc/vsftpd.conf.bak ```

	5.2. buscamos (ctrl+w) *write_enable=YES* y descomentamos

	5.3. descomentamos *chroot_local_user=YES* para evitae que los usuarios se salgan de su directorio

	5.4. descomentamos *local_umask=022* para que los archivos subidos se les asignen los permisos correctamente

	5.5. Agregamos la sig. linea *force_dot_files=YES* al final para que se muestren los archivos que empiezan con .

	5.6. Agregamos permisos de escritura en el root *allow_writeable_chroot=YES*

	5.7. Agregamos las siguientes
```
pasv_min_port=40000
pasv_max_port=50000
```

	5.8. Guardamos 



<br /><br /><br />

7. Agregamos carpeta para subir archivos, en este caso para subir web

	7.1. Creamos la carpeta: ```sudo mkdir /var/cliesntes_app```

	7.2. para que al entrar el usr por ftp inicie en esta carpeta ```sudo usermod -d /var/cliesntes_app clienteapp```

	7.2. le asignamos permisos a nuestro usuario ```sudo chown clientesapp:clientesapp -R /var/cliesnte_app ```

	7.3. le asignamos permisos a nuestro usuario ```sudo chmod a-w -R /var/cliesntes_app```

	7.3. ```sudo chmod 755 -R /var/cliesntes_app```


	7.4. reiniciamos ftp ``` sudo systemctl restart vsftpd ```



8. Podemos ver los log en ```sudo tail -f /var/log/vsftpd.log```

### Instalar Java en linux ubuntu OpenJDK

Instalar openJDK en ubuntu: `sudo apt-get install openjdk-8-jre`

parar el proceso sin matarlo: `kill -STOP {pid}`

### Instalar mysql linux ubuntu server

Fuente: https://support.rackspace.com/how-to/installing-mysql-server-on-ubuntu/   
Instalar mysql-server: ```sudo apt-get install mysql-server```   

iniciar ```sudo systemctl start mysql```   

iniciar proceso de inicio de configuracion: ```mysql_secure_installation utility```    

reiniciar ```service mysql stop```  y ```service mysql start```   

<!-- iniciar mysql con el systema, iniciar mysql con el inicio del sistema-->
Iniciar mysql en el inicio de sistema```sudo systemctl enable mysql```

vemos la pass por default en ```sudo cat /etc/mysql/debian.cnf```   

actualizar pass: ```UPDATE mysql.user SET authentication_string = PASSWORD('password') WHERE User = 'root';```   

iniciar session se usa sudo para root ```sudo mysql -u root -p```   

problemas para iniciar con root: https://askubuntu.com/questions/766334/cant-login-as-mysql-user-root-from-normal-user-account-in-ubuntu-16-04   




### Instalar y levantar Node con express en linux ubuntu server
1. instalar
```
sudo apt install nodejs 
sudo apt install npm
```




## PRACTICA: Levantar spring boot y angular en servidor y darle trafico con jmeter

1. Preparar para deploy en ubunto server aplicacion java con spring boot

	1.1. Preparamos servidor ftp, ver en DevOps "Instalar FTP ubuntu"


2. levantar proyecto back clientes en ubuntu

	2.1. Instalar java en servidor ver en Devops *Instalar Java en linux ubuntu OpenJDK*

	2.2. Instalar mysql y crear la bd, ver en Devops *Instalar mysql linux ubuntu server*, no olvidar el mysql_secure_installation   

	2.3. Subimos el jar por ftp y ejecutamos jar de spring boot, viendo que tengamos los permismos chmod 775 y chown   
		2.3.1. Ejecutar jar en segundo plano:  ```nohup java -jar back/clientesappback-0.0.1.jar > back/clientes-app.log &```    

	2.4. Podemos ver los procesos por usuario ```htop --user=clienteapp``` y para matar el proceso se da espacio  
		 sobre el proceso, precionar k y seleccionar kill   


3. Levantar proyecto front con angular

	3.1. Instalar express y compress, hacer script en mi caso el segundo Ver en Java.md 
		 *190. Realizando el despliegue de nuestra app angular con NodeJS y Express* 

	3.2. Ejecutar node express en segundo plano:  ```nohup node front/server.js > front/clientes_app_front.log & ```    

	3.3. **Cuidado:** COn las rutas en el server.js se pone al pedo   


4. Crear trafico con jmeter: file://E:\cursos\_2019\img\dev_ops\jmeter\prueba_clientes_app.gif
	4.1. Enviar peticion de Login y capturar token
	4.2. uso del CRUD: [script](./img/dev_ops/jmeter/prueba_clientes_app.jmx)
	![script](./img/dev_ops/jmeter/prueba_clientes_app.gif)



<br /><br /><br />
### Descargar archivos de servidor remoto con scp
<!-- uso de scp uso, como usar scp usar scp comando scp, descargar carpeta de servidor remoto  -->
```
scp -r mario@192.168.1.74:/home/mario/docker {dir_local}
```


<br /><br /><br />
<!-- mostrar informacion del sistema operativo, mostrar informacion del so, mostrar detalles del sistema operativo -->
<!-- ver informacion del sistema operativo, ver informacion del so, ver detalles del sistema operativo -->
### Ver detalles del Sistema operativo

<!-- ver nombre del distro, ver nombre de distribucion,  mostrar nombre del distro, mostrar nombre de distribucion ver distro, ver distribucion,  mostrar distro, mostrar distribucion ver detalles so, mostrar detalles so, ver detalles de so, mostrar detalles de so, ver nombre del systema operativo, ver nombre del so, nombre so, nombre del so, ver so nombre  -->
- para ver el nombre de la ditribuccion 
```
cat /etc/os-release
```



### Instalar Docker en Debian 9

- Actualizamos: ``` apt update ```

- instlamos utilerias
```
apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```

- agregamos repositorio de donde instalar docker
```
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
```

- agregamos llave del nuevo repositorio
```
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - 
```

- Actualizamoc con el nuevo repo ``` apt update ```

- aseguramoss que instalara docker del nuevo repo que agregamos
```
apt-cache policy docker-ce
```

- instalar docker: ``` apt install -y docker-ce ```

- Iniciar el sistema con docker: ``` systemctl enable docker ```


- instalar docker compose
```
curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose 
```

- agregar permisos
```
chmod +x /usr/local/bin/docker-compose
```

- Comprobar instalacion
```
docker-compose -v 
```

### Instalar Drone en Linux a linea de comandos

Fuente: [drone.io](https://docs.drone.io/runner/exec/installation/linux/#download)

DRONE_RPC_PROTO=http
DRONE_RPC_HOST=139.144.231.244
DRONE_RPC_SECRET=el_secreto_of_drone

DRONE_LOG_FILE=/var/log/drone-runner-exec/log.txt






<!-- 
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################

############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################

############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
-->






















































<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
___

# PENDIENTE 
1.  Un docker-compose.yml con Jenkins con utilerias como respaldar bd a aws etc solo 
    construllendolo sin preocuparme por detalles.   
    
2.  Un docker-compose.yml que de la posibilidad con jenkins que codigo desplegar, en que 
    lugar de aws desplegarlo y consolo dando push a master se haga el despliege.   

3. Pasar mi host de gitlab a uno independiente.   





