# NGINX

- [NGINX](#nginx)
  - [Antes de empezar](#antes-de-empezar)
  - [Introduccion](#introduccion)
    - [Directivas](#directivas)
    - [Redirecciones](#redirecciones)
      - [De una ruta a una carpeta](#de-una-ruta-a-una-carpeta)
      - [De una ruta a otro puerto](#de-una-ruta-a-otro-puerto)

pronunciado enyinéx y surgió para alto rendimiento en contraposición
de apache entre sus características están

- Servidor de archivos estáticos
- Proxy inverso con opciones de cache
- balanceo de carga
- tolerancia a fallos
- soporte de http y https (ssl)
- Soporta FastCGI
- Servidores virtuales basados en nombre y/o dirección IP.
- Soporte de autenticacion
- Compatible con IPV6
- compresión gzip
- habilitado para soportar 10,000 conexiones simultaneas

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

## Antes de empezar

Seguire el curso con Docker por lo que el comando para levantarlo y entrar
a este son los siguientes.

A, Para levantarlo: `docker run -d -p 80:80 --name my_ngnix nginx:1.18-alpine`

B, para entrar a este: `docker exec -ti my_ngnix sh`

<!-- levantar con configuracion, cambiar configuracion -->

C, Para levantarlo con configuracion personalizada, podemos agregar
[este](img/01-basic-server.conf) archivo, donde en donde ejecutamos el
comando debemos de tener `nginx.conf` el archivo que mensionamos.

Ejemplo.

- Linux: `docker run -d -p 80:80 --name my_ngnix -v .:/etc/nginx/nginx.conf nginx:1.18-alpine`
- Windows (GitBash): `docker run -d -p 80:80 --name my_ngnix -v //e/cursos/0activ/sistema/ngnix/uno/nginx.conf:/etc/nginx/nginx.conf nginx:1.18-alpine`

D, Levantarlo con una carpeta de contenido diferente.

```r
docker run -d -p 80:80 --name my_ngnix \
# cambiamos configuracion
  -v //e/path/nginx.conf:/etc/nginx/nginx.conf \
# cambiamos contenido
  -v //e/path/user-site/:/usr/share/nginx/html \
  nginx:1.18-alpine
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

## Introduccion

- Fuente: [Documentacion Oficial](https://nginx.org/en/docs/beginners_guide.html)
- Mas: [Documentacion Oficial](https://nginx.org/en/docs/http/ngx_http_core_module.html)

A, instalacion: `apt-get install nginx`

B, Directorio/configuraciones: `/etc/nginx/nginx.conf`

- Nota: Tenemos en nginx.conf unas directivas `include` que concatenan
  nuestro archivo principal

C, Directorio/configuraciones: `/etc/nginx/conf.d/default.conf`

- En este archivo tenemos nuestro server configurado, ej. el puerto,
  carpeta que entregara, etc.

<!-- ver peticiones, -->

D, Directorio de logs: `/var/log/nginx/` y en `/var/log/nginx/access.log`
podemos ver los accesos

E, Hacer pruebas de configuracion (ej. sintaxis): `sudo nginx -t`

F, Ejecutable principal: `/usr/sbin/nginx`, por default se agrega al path

G, Mandar señales `nginx -s signal`

- stop — fast shutdown
- quit — graceful shutdown
- reload — reloading the configuration file
- reopen — reopening the log files

H, El `nginx.conf` se divide en 3 partes, las cuales veremos mas adelante

```r
# DIRECTIVAS DE main
user  nginx;
worker_processes  auto;
...

events {
  # DIRECTIVAS DE EVENTOS
  ...
}

http {
  # DIRECTIVAS DE HTTP
  ...
}
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### Directivas

Fuente: [Documentacion oficial](https://nginx.org/en/docs/ngx_core_module.html)

- **Como leer la docu:** En cada explicacion de directiva tenemos un
  `Context: XXXX` donde en las X define events, http, server, y location
  las cuales son las cuatro directivas que tenmos

Estas son las que estan en el primer nivel (no estan dentro) de ningun
bloque, y tenemos las siguietes

- Nota: Algunas etiquetas pueden usarse en distintos sitios (checar docu)

```r
user  nginx;                              # Usuario que ocupara el proceso
worker_processes  auto;                   # hilos que ocupara
error_log  /var/log/nginx/error.log warn; # donde escribir los logs de errores y el nivel a mostrar
pid        /var/run/nginx.pid;            # donde escribir el pid del proceso
env PERL5LIB=/data/site/modules;          # definicion de variables

events {
  worker_connections  1024;     # Cantidad de conexiones simultaneas
  debug_connection 127.0.0.1;   # los logs de esta IP iran a otro archivo
}

# Configuraciones cuando ocurre una peticion http
http {
  access_log  /var/log/nginx/access.log  main; # escritura de logs del los accessos
  keepalive_timeout  65; # 65s despues de inactitividad la conexion se cierra
  #gzip  on;             # Comprecion
  keepalive_requests 100 # Al cumplirse 100 peticiones se cerrara y aabrira una nueva conexion
  send_timeout 60s;      # timeout al no responder una peticion
}
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### Redirecciones

<!-- cambiar en funcion de la rutam cambiar de carpeta, usar multiples carpetas, uso de carpetas, uso de varias carpetas-->

#### De una ruta a una carpeta

 ejemplo `ej.com/admin` -> `/usr/share/nginx/admin-site`

- Archivo completo [aqui](./img/02-cambiar-de-carpeta-en-funcion-de-la-ruta.conf)

```r
...
events {...}
http {
  ...
  server {
    listen 80; listen [::]:80; server_name localhost;
    root   /usr/share/nginx/html;
    index  index.html index.htm;
# REDIRECCION
# CUIDADO: CREO ES OBLIGATORIO EL ULTIMO /
    location /admin {
      alias   /usr/share/nginx/admin-site/;
    }
  }
}
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

#### De una ruta a otro puerto

**Objetivo:** Tendremos 2 servidores en diferentes puertos, y cuando
entremos a una ruta en espesifico, nos re-dirigira al segundo servidor
que esta en otra IP

A, Para levantar.

- No olvidar abrir el segundo puerto en docker

```r
docker run -d -p 80:80 -p 81:81 --name my_ngnix \
  -v //e/03.../nginx.conf:/etc/nginx/nginx.conf \
  -v //e/03.../serv1/:/usr/share/nginx/serv1 \
  -v //e/03.../serv2/:/usr/share/nginx/serv2 \
  nginx:1.18-alpine
```

B, Archivo de configuracion

```r
user              nginx;
worker_processes  auto;
error_log         /var/log/nginx/error.log warn;
pid               /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
  include           /etc/nginx/mime.types;
  default_type      application/octet-stream;
  log_format  main  '$remote_addr Status: $status Bytes: $body_bytes_sent';
  access_log        /var/log/nginx/access.log  main;
  sendfile          on;
  keepalive_timeout 65;
  gzip  on;

  server {
    listen      80;
    listen      [::]:80;
    server_name localhost;
    root   /usr/share/nginx/serv1;
    index  index.html index.htm;
    location /serv2 {
      rewrite ^ http://$http_host:81/ permanent;
    }
  }

  server {
    listen      81;
    listen      [::]:81;
    server_name localhost;
    root   /usr/share/nginx/serv2;
    index  index.html index.htm;
    location /serv1 {
      rewrite ^ http://$http_host:80/ permanent;
    }
  }
}
```
