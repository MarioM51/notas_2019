# JENKINS

## Indice

- [JENKINS](#jenkins)
  - [Indice](#indice)
  - [PENDIENTE](#pendiente)
  - [Introduccion](#introduccion)
    - [Instalacion de Docker](#instalacion-de-docker)
    - [Instalar Docker Compose](#instalar-docker-compose)
    - [Instalacion De Jenkins](#instalacion-de-jenkins)
    - [Como Trabajar Con Docker Y Jenkins](#como-trabajar-con-docker-y-jenkins)
  - [Primeros pasos en Jenkins](#primeros-pasos-en-jenkins)
    - [Intruduccion A La Ui De Jenkins](#intruduccion-a-la-ui-de-jenkins)
    - [Primer Jobs](#primer-jobs)
    - [Juega Un Poco Mas Con Tu Primer Job](#juega-un-poco-mas-con-tu-primer-job)
    - [Redirige El Output De Tu Primer Job](#redirige-el-output-de-tu-primer-job)
    - [Crea Un Script Con Tu Primer Job](#crea-un-script-con-tu-primer-job)
    - [Agrega Parametros A Tu Primer Job](#agrega-parametros-a-tu-primer-job)
    - [17. Incluye parámetros a tu Job para ejecutar un script](#17-incluye-parámetros-a-tu-job-para-ejecutar-un-script)
  - [Sección 3: Jenkins & Docker](#sección-3-jenkins--docker)
    - [19. Docker + Jenkins - SSH - I](#19-docker--jenkins---ssh---i)
    - [22. Aprende a instalar plugins (SSH Plugin)](#22-aprende-a-instalar-plugins-ssh-plugin)
    - [23. Integra Jenkins con tu server SSH](#23-integra-jenkins-con-tu-server-ssh)
    - [24. Ejecuta tu primer Job en un host remoto vía SSH](#24-ejecuta-tu-primer-job-en-un-host-remoto-vía-ssh)
  - [Sección 4: Jenkins & AWS](#sección-4-jenkins--aws)
    - [26. Introducción: MySQL + AWS + Shell Script + Jenkins](#26-introducción-mysql--aws--shell-script--jenkins)
    - [27. Instalar MySQL en Docker](#27-instalar-mysql-en-docker)
    - [28. Instala MySQL Client y AWS CLI](#28-instala-mysql-client-y-aws-cli)
    - [29.30.31. Crea una DB sencilla en MySQL. Crea un Bucket en S3. Crea un usuario IAM en AWS para autenticación](#293031-crea-una-db-sencilla-en-mysql-crea-un-bucket-en-s3-crea-un-usuario-iam-en-aws-para-autenticación)
    - [32. Aprende a tomar un dump de una DB y a subirlo a AWS S3 manualmente](#32-aprende-a-tomar-un-dump-de-una-db-y-a-subirlo-a-aws-s3-manualmente)
    - [33.34. Crea un script para automatizar el export de la DB. Integra tu script con AWS (AWS CLI)](#3334-crea-un-script-para-automatizar-el-export-de-la-db-integra-tu-script-con-aws-aws-cli)
    - [35. Aprende a manejar data sensible en Jenkins (Contraseñas, keys)](#35-aprende-a-manejar-data-sensible-en-jenkins-contraseñas-keys)
    - [36. Crea un Jenkins Job para subir tu DB a AWS (Parámetros + Shell Scripting)](#36-crea-un-jenkins-job-para-subir-tu-db-a-aws-parámetros--shell-scripting)
  - [Sección 5: Jenkins & Ansible](#sección-5-jenkins--ansible)
    - [43. Instala Ansible en Jenkins](#43-instala-ansible-en-jenkins)
    - [44. Asegúrate de que Jenkins tenga las llaves SSH](#44-asegúrate-de-que-jenkins-tenga-las-llaves-ssh)
    - [45.65. Crea un archivo de inventario sencillo: Docker + Ansible + Jenkins / Permite que los usuarios se registren en Jenkins](#4565-crea-un-archivo-de-inventario-sencillo-docker--ansible--jenkins--permite-que-los-usuarios-se-registren-en-jenkins)
  - [Sección 6: Jenkins & Security](#sección-6-jenkins--security)
    - [66. Instala un Plugin de seguridad](#66-instala-un-plugin-de-seguridad)
    - [67. Crear usuario en Jenkin](#67-crear-usuario-en-jenkin)
    - [68.69. Crea un rol global de solo lectura - Asigna el rol anterior a un usuario en Jenkins](#6869-crea-un-rol-global-de-solo-lectura---asigna-el-rol-anterior-a-un-usuario-en-jenkins)
    - [71. Restringe el acceso a determinados Jobs con roles de proyectos](#71-restringe-el-acceso-a-determinados-jobs-con-roles-de-proyectos)
  - [Sección 7: Jenkins Tips & Tricks](#sección-7-jenkins-tips--tricks)
    - [73. Variables de entorno disponibles](#73-variables-de-entorno-disponibles)
    - [74. Crea tus variables de Entorno](#74-crea-tus-variables-de-entorno)
    - [76. Ejecuta Jobs con Cron](#76-ejecuta-jobs-con-cron)
    - [77. Crea un usuario con permiso para ejecutar los Jobs 78. Gatilla Jobs con curl (Sin parámetros)](#77-crea-un-usuario-con-permiso-para-ejecutar-los-jobs-78-gatilla-jobs-con-curl-sin-parámetros)
  - [Sección 8: Jenkins & Email](#sección-8-jenkins--email)
    - [82. Configura el envío de notificaciones con AWS SES](#82-configura-el-envío-de-notificaciones-con-aws-ses)
    - [83. Configura el envío de notificaciones con Gmail](#83-configura-el-envío-de-notificaciones-con-gmail)
  - [Sección 9: Jenkins & Maven](#sección-9-jenkins--maven)
    - [88. Aprende a hacer pull desde un repo en Git/GitHub](#88-aprende-a-hacer-pull-desde-un-repo-en-gitgithub)
    - [89. Construye el código con Maven](#89-construye-el-código-con-maven)
    - [90. Realiza tests en tu código](#90-realiza-tests-en-tu-código)
    - [91. Despliega tu JAR localmente](#91-despliega-tu-jar-localmente)
    - [92. Archiva el resultado de tus tests en Jenkins](#92-archiva-el-resultado-de-tus-tests-en-jenkins)
    - [93. Archiva artifacts en tu Job](#93-archiva-artifacts-en-tu-job)
    - [94. Envía notificaciones del estado de tus Jobs vía mail](#94-envía-notificaciones-del-estado-de-tus-jobs-vía-mail)
  - [Sección 10: Jenkins & GIT](#sección-10-jenkins--git)
    - [97. Crea un repo en GIT](#97-crea-un-repo-en-git)
    - [101.102. Git Hooks . Realiza un cambio al repo y gatilla el Job de Jenkins](#101102-git-hooks--realiza-un-cambio-al-repo-y-gatilla-el-job-de-jenkins)
  - [Sección 11: Jenkins & DSL](#sección-11-jenkins--dsl)
    - [105. Instalación de plugin DSL](#105-instalación-de-plugin-dsl)
    - [106..113 Seed Job - SCM Integracion git -  Triggers (CRON) - Steps - Mailer](#106113-seed-job---scm-integracion-git----triggers-cron---steps---mailer)
    - [115. Construye el Job de Maven en DSL](#115-construye-el-job-de-maven-en-dsl)
  - [Sección 12: CI/CD - Definiciones](#sección-12-cicd---definiciones)
  - [Sección 13: Jenkins Pipeline - Jenkinsfile](#sección-13-jenkins-pipeline---jenkinsfile)
    - [125. Introducción a Jenkinsfile](#125-introducción-a-jenkinsfile)
    - [126. Instala el plugin de Pipeline](#126-instala-el-plugin-de-pipeline)
    - [127. Crea tu primer Pipeline](#127-crea-tu-primer-pipeline)
    - [128. Agrega más steps a tu pipeline](#128-agrega-más-steps-a-tu-pipeline)
    - [129. Retry](#129-retry)
    - [130. Timeouts](#130-timeouts)
    - [131. Post actions](#131-post-actions)
    - [132. Variables de entorno](#132-variables-de-entorno)
    - [133. Credentials](#133-credentials)
  - [Sección 14: CI/CD + Jenkins Pipeline + Docker + Maven](#sección-14-cicd--jenkins-pipeline--docker--maven)
    - [137. Define los stages de tu pipeline](#137-define-los-stages-de-tu-pipeline)
    - [138. Build: Genera el JAR de tu app Maven usando Docker](#138-build-genera-el-jar-de-tu-app-maven-usando-docker)
    - [139. Build: Crea un script para automatizar la creación del JAR con Docker](#139-build-crea-un-script-para-automatizar-la-creación-del-jar-con-docker)
    - [140. Build: Crea un Dockerfile y construye una imagen con el JAR](#140-build-crea-un-dockerfile-y-construye-una-imagen-con-el-jar)
    - [141. Build: Crea un docker-compose para automatizar la construcción de la imagen](#141-build-crea-un-docker-compose-para-automatizar-la-construcción-de-la-imagen)
    - [142. Build: Crea un script para automatizar la creación de la imagen con compose](#142-build-crea-un-script-para-automatizar-la-creación-de-la-imagen-con-compose)
    - [143. Build: Agrega tus scripts a tu Jenkinsfile](#143-build-agrega-tus-scripts-a-tu-jenkinsfile)
    - [146. Test: Aprende a realizar tests desde Docker con Maven](#146-test-aprende-a-realizar-tests-desde-docker-con-maven)
    - [Amazon AWS: Crea una máquina remota para pruebas y conectarnos](#amazon-aws-crea-una-máquina-remota-para-pruebas-y-conectarnos)
    - [Amazon AWS: Configurar domino en Amazon (AWS)](#amazon-aws-configurar-domino-en-amazon-aws)
    - [149. Push: Aprende a crear un registry en Docker (Sin autenticación)](#149-push-aprende-a-crear-un-registry-en-docker-sin-autenticación)
    - [150. Push: Aprende a subir y bajar imágenes a un registry en Docker](#150-push-aprende-a-subir-y-bajar-imágenes-a-un-registry-en-docker)
    - [151. Push: Crea certificados SSL para tu registry con autenticación](#151-push-crea-certificados-ssl-para-tu-registry-con-autenticación)
    - [152.153 Push: Aprende a crear un registry en Docker (Con autenticación) . Aprende a subir y bajar imágenes a un registry con autenticación](#152153-push-aprende-a-crear-un-registry-en-docker-con-autenticación--aprende-a-subir-y-bajar-imágenes-a-un-registry-con-autenticación)
    - [154.155. Push: Automatiza el proceso de subir y bajar imágenes a un registry - Valida el funcionamiento de tu script de push (Sin Auth)](#154155-push-automatiza-el-proceso-de-subir-y-bajar-imágenes-a-un-registry---valida-el-funcionamiento-de-tu-script-de-push-sin-auth)
    - [156. Push: Agrega el script de push a tu Jenkinsfile](#156-push-agrega-el-script-de-push-a-tu-jenkinsfile)
    - [154.155. Push: Automatiza el proceso de subir y bajar imágenes a un registry - Valida el funcionamiento de tu script de push (Con Auth)](#154155-push-automatiza-el-proceso-de-subir-y-bajar-imágenes-a-un-registry---valida-el-funcionamiento-de-tu-script-de-push-con-auth)
    - [157. Deploy: Transfiere las variables de entorno a la máquina remota](#157-deploy-transfiere-las-variables-de-entorno-a-la-máquina-remota)
    - [158. Deploy: Despliega tu aplicación manualmente](#158-deploy-despliega-tu-aplicación-manualmente)
    - [159. Deploy: Crea un script para automatizar el despliegue de tu aplicación](#159-deploy-crea-un-script-para-automatizar-el-despliegue-de-tu-aplicación)
    - [160. Deploy: Ejecuta el script de publish en la máquina remota](#160-deploy-ejecuta-el-script-de-publish-en-la-máquina-remota)
    - [161. Deploy: Agrega el script de deploy al Jenkinsfile](#161-deploy-agrega-el-script-de-deploy-al-jenkinsfile)
    - [162. Crea un repo en GIT para el nuevo proyecto](#162-crea-un-repo-en-git-para-el-nuevo-proyecto)
    - [163. Crea el pipeline en Jenkins](#163-crea-el-pipeline-en-jenkins)
    - [165. Agrega la contraseña del registry al Jenkinsfile](#165-agrega-la-contraseña-del-registry-al-jenkinsfile)
    - [166. Verifica el host ssh en el contenedor de Jenkins](#166-verifica-el-host-ssh-en-el-contenedor-de-jenkins)
    - [168. Agrega post actions a tu Jenkinsfile](#168-agrega-post-actions-a-tu-jenkinsfile)
    - [170. Agrega un Git Hook al repo para gatillar el Job automáticamente](#170-agrega-un-git-hook-al-repo-para-gatillar-el-job-automáticamente)
    - [171. Inicia el proceso de CI/CD solo con un commit a GIT](#171-inicia-el-proceso-de-cicd-solo-con-un-commit-a-git)
  - [RESUMEN: Crear pipeline DevOps](#resumen-crear-pipeline-devops)
  - [FIN Curso jenkins de principiante a experto](#fin-curso-jenkins-de-principiante-a-experto)
  - [Cosas que me voy encontrando](#cosas-que-me-voy-encontrando)
    - [Errores](#errores)
    - [Crear contenedor de jenkins con https (Sin reverse proxy)](#crear-contenedor-de-jenkins-con-https-sin-reverse-proxy)

## PENDIENTE

- Saber como cambiar de JDK para construir un proyecto con un JDK
  diferente al de Jenkins, por ejemplo, queria construir un proyecto
  con java11 pero jenkins tenia java8, y hay manera de agregar un JDK
  para que se use ese en el job, pero no pude :(

## Introduccion

**¿Que es Jenkins?** Es una herramienta/Servidor de automatizacion de
procesos, workflows, etc. como son los backups, subida a prod, tests
automaticos, y muchos mas que tenga que ver con automatizacion, lo cual
esta muy de la mano con Dev/Ops y/o integracion continua.

**DevOps:** Developer/Operaciones es reducir la brecha entre los de
operaciones y los desarrolladores, en el cual un desarrollador que no
sepa de operaciones, pueda con un click reiniciar, o subir a produccion,
o retornar etc, sin que realmente sepa como se hace este proceso

Jenkins tiene muchos plugins como de maven, git, etc, que facilitan
mucho el trabajo

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Instalacion de Docker

Docker nos ayuda a crear contenedores de servicios.

1, Descargamos virtualbox.

2, Descargamos Ubuntu Server y lo instalamos en el virtualbox.

  2.1. **Recomendacion:** Luego el instalador de Linux trae
       la opcion de instalar paquetes, mejor **altarnos esta opcion**
       ya que es mejor hacer la instalacion segun la documentacion
       oficial y con la version mas resiente/estable.

  2.2. Para poder copiar y pegar comandos podemos conectarnos
       desde ssh a la maquina virtual, el cual debemos poner la
       network de la maquina en **modo bridge** para podernos comunicar
       con ella en el mismo ambito de red.
        - **Manual:** [link](https://www.linuxbabe.com/virtualbox/a-pretty-good-introduction-to-virtualbox-bridged-networking-mode)

3, Instalamos Docker (en mi caso Ubuntu Server) con los siguiente comandos.

- **Oficial:** [Dcumentacion Oficial](https://docs.docker.com/engine/install/ubuntu/)

- **Udemy:** [Manual de Udemy](./img/install_docker.txt) <!-- file://E:\cursos\_2019\img\Jenkins\install_docker.txt -->

- Actualiza los repos. `sudo apt-get update`

- Instala utilidades: `sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y`

- Agregar el gpg: `curl -fsSL htps://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

- Agregar el repo: `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`

- Actualizar de nuevo. `sudo apt-get update`

- Instalar docker: `sudo apt-get install docker-ce`

- Iniciarlo con el sistema: `sudo systemctl enable docker`

4, **Iniciamos docker**: `sudo systemctl start docker`

  4.1. Corremos docker, correr docker `sudo systemctl enable docker`

  4.2. Usamos docker, este es para ver los servicios actuales: `sudo docker ps`

> **Error**: Got permission denied while trying to connect to the Docker daemon
> **Causa**: El usuario de docker necesita permisos
> **Solucion**: Agregamos nuestro usuario al grupo de docker: `bash sudo usermod -aG docker mario`
> Al terminar nos logueamos de buevo para notar los cambios
> 1, Nota: podemos ver que usuario estamos usando con whoami

**Resultado:** Tenesmo instalado y funcionando docker

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Instalar Docker Compose

<!-- instalacion docker compose, instalacion docker-compose, instalar docker-compose, instalacion de docker-compose, instalacion de docker compose-->

Lo usaremos para facilitar el trabajo

1, Descargamos archivo de github y lo colocamos en /docker-compose

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

2, Agregamos permisos a la carpeta

```bash
sudo chmod +x /usr/local/bin/docker-compose
```

3, Ejecutamos docker compose y deberia de darnos la version

```bash
docker-compose -v
```

**Resultado:** Ya estamos tenemos instalado docker y Docker-compose,
FALTA instalacion de jenkins

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Instalacion De Jenkins

<!-- instalar jenkins-->

NOTA: la imagen de **jenkinsci/jenkins** esta deprecada, asi que en vez de
usar, deberas usar esta: **jenkins/jenkins**.

Docker usa imagenes que son basicamente plantillas de servicios

1. Vamos a Docker hub, que es el lugar donde se guardan todo tipo de

imagenes.

- [imagen oficial de jenkins](https://hub.docker.com/r/jenkins/jenkins/)
  
  1.1. ahi tendremos un comando el cual ejecutamos como esta, lo que
        hara sera descargar la imagen.
      Terminara con un **"Status: Downloaded newer image for jenkins/jenkins:latest"**.
    ``` docker pull jenkins/jenkins ```.
  1.2. al terminar tendremos la imagen agregada, la cual podremos
  ver las imagenes que tenemos con.

```sudo docker images```

1. Creamos una carpeta y le damos pemisos al usuario de jenkins sobre esta
Nota: El proceso de jenkins es el 1000 por eso el comando.
  `mkdir jenkins & chown 1000 jenkins`

3, Creamos la el .yml de docker: `sudo nano /home/mario/jenkins/docker-compose.yml`

  3.1. Y le agregamos el siguiente contenido.

```yml
version: '3'
services:
    jenkins:
        container_name: jenkins
        image: jenkins/jenkins
        ports:
            - "8080:8080"
        volumes:
            - /home/mario/jenkins/jenkins_home:/var/jenkins_home
        networks:
            - net
networks:
    net:
```

**3.2.** Creamos la carpeta, que definimos en el .yml

```bash
mkdir /home/mario/jenkins/jenkins_home
```

1. Construimos el servicio

```bash
 docker-compose up -d
```

**4.1.** termina con un "Creating jenkins ... done"

1. Comprobamos que el contenedor este corriendo con el nombre de jenkins/jenkins

```log
 sudo docker ps
CONTAINER ID  IMAGE            COMMAND                  CREATED        STATUS        PORTS                               NAMES
a563a2456d66  jenkins/jenkins  "/sbin/tini -- /usr/…"   9 seconds ago  Up 6 seconds  0.0.0.0:8080->8080/tcp, 50000/tcp   jenkins
```

Nota: podemos ver que esta haciendo jenkins con, ver logs de jenkins en docker

<!-- ver logs de jenkins, logs jenkins logs, mostrar logs de jenkins -->

```bash
 docker logs -f jenkins
```

1. Entramos a jenkins desde el navegador `http://{{IP_HOST}}:8080/`

  6,1. nos indicara una ruta que contiene la pass, en mi caso

```bash
 cat /home/mario/jenkins/jenkins_home/secrets/initialAdminPassword
```

1. al ingresar la pass nos dara 2- opciones de "instalar plugins sugerios"
o de "seleccionar plugins a instalar" como no sabemos seleccionamos la 1

  7.1. Nos pedira crear el usuario admin
  7.2. Nos loeamos y podremos ver el panel de jenkins

<!-- entrar a junkins -->

1. Comprobar que el servicio este activo

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Como Trabajar Con Docker Y Jenkins

hasta este punto ya tenemos instalado y corriendo Jenkins

1. Detener el servicio de Jenkins, el cual no borrara nada solo
detendra el servico.

  1.1. vamos a donde tenemos nuestro .yml y corremos

```bash
  cd /home/mario/jenkins && docker-compose stop
  cd /home/mario/jenkins && docker-compose down
```

1. Iniciar el servicio, iniciar servicio, levantar servicio
  2.1. En caso de haberle dado un down primero ejecutamos, ya que nos podria dar un
       Error: "No containers to start"

```bash
  cd /home/mario/jenkins && docker-compose up -d ||| <- Recomendado puede crear e iniciar segun el caso
```

2.2. Si solo le dimos un stop

```bash
  cd /home/mario/jenkins && docker-compose start
```

3, Reiniciar el servicio

```bash
  cd /home/mario/jenkins && docker-compose restart
```

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Primeros pasos en Jenkins

### Intruduccion A La Ui De Jenkins

Tareas o Jobs: Son los pasos que definimos para automatizar las tareas
Personas: Los usuarios que pueden logearse
Historial: Las ejecuciones de los jobs y su estatus
etc.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Primer Jobs

Las tareas comunmente se hacen remotamente, osea que el
servidor dew jenkins se conecta a un host y este ejecuta la
tarea.

Para aprender vamos a ejecuar los jobs en la misma maquina en la que
esta jenkins instalado (por lo mientras).

**TAGS;** *consola de jenkins, consola jenkins consola, jenkins shell, jenkin shell, jenkin shell*

1. Crear una tarea.
  1.1. Vamos a nueva tarea, le damos nombre y damos click sobre
  "crear un proyecto de estilo libre" y sobre ok.

  1.2. Vamos a ejecutar  en el select ponemos "Ejecutar linea de comandos (shell)"
        Que lo que hara sera ejecuatar comandos linux, pero sobre la
      shell de docker, la cual podemos acceder a shell de docker con,

```bash
  docker exec -ti jenkins bash
  jenkins@ad40e110282b:/$  <- Cambia a este shell
```

1.3. Agregamos en el textarea un `bash echo "hola"` y le damos guardar

![alt text](./img/panel_edicion_tarea.PNG "panel_edicion_tarea")

1.4. Le damos sobre Construir ahora.

1.5. Esto ejecutara el job y se mostrara un numero en la parte izq
  podemos ver un menu desplegable y darle sobre "Console output"
  y aqui se nos mostrara la salida, que en este caso fue un hola

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Juega Un Poco Mas Con Tu Primer Job

Vamos a editar nuestro primer Job., para eso le damos clic sobre
configurar

```bash
  echo "fecha: $(date)"
```

uso de date, dentro de la consola de jenkins

```r
Comando   | Salida
date +%F  | 2019-05-01
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Redirige El Output De Tu Primer Job

Uso de variables en el shell de jenkins

```bash
NOMBRE = "Mario"
echo "Hola $NOMBRE fecha: $(date)"
```

-> **SALIDA:** Hola Mario fecha: Wed May  1. 14:47:08 UTC 2019.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Crea Un Script Con Tu Primer Job

Vamos a crear un Script y ejecutarlo desde el bash de Jenkins

1, Creamos un Script con dos variables sin definir

```bash
cd /home/mario/jenkins
nano miScript.sh
```

contenido

```bash
 #!/bin/bash
 echo "Hola $NOMBRE $APELLIDO"
```

2, Lo ejecutamos, el cual solo va a sar "Hola" ya que no tenemos las variables definidas

```bash
> sh miScript.sh
< Hola
```

3, Copiamos el script dentro del contenedor de jenkins

```bash
> docker cp /home/mario/jenkins/miScript.sh jenkins:/opt
```

4, Nos pasamos a la consola de docker jenkins y buscamos el archivo
que acabasmo de pasar, el cual deberiamos de ver

```bash
  docker exec -ti jenkins bash
  ls -l /opt
```

5, Ejecutamos el script dentro del conenedor de docker

```bash
sh /opt/miScript.sh
```

6, dentro de la Pagina de Jenkins configuramos nuestro job y en el bash
le ponemos la ruta del script a ejecutar

```r
NOMBRE="Mario"
APELLIDO="Marquez"
sh /opt/miScript.sh
```

7, Construimos/ejecutamos y podemos ver la ejecucion de nuestro
script pero no vamos a poder ver asignados ni el nombre ni el app

```bash
Hola -> Salida
```

**Error:** Permission denied.
**Solucion:** Le agregamos permisos de ejecucion `bash chmod +x /opt/miScript.sh`
**Error:** No se muestra nombre ni app.
**Causa:** Esto ocurre algo asi por ambito o scope
**Soluccion:** Les pasamos las variables como parametros y las capturamos en el script
  en la parte de ejecutar de Jenkins lo ponemos de la siguiente manera

```bash
NOMBRE="Mario"
APP="Marquez"
/opt/miScript.sh $NOMBRE $APP
```

1. Modificamos el Script para que tome los parametros que nosotros le pasamos
tomar parametros de script, pasarle parametros a script.

```bash
#!/bin/bash
NOMBRE=$1
APELLIDO=$2
echo "Hola $NOMBRE $APELLIDO"
```

9, Ejecutamos el Script de nuevo., ahora podremos ver vien la salida con `Hola Mario Marquez`

![alt text](./img/script_parametros_entrada_ejec_script_desde_jenkins_output.PNG "script_parametros_entrada_ejec_script_desde_jenkins_output")

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Agrega Parametros A Tu Primer Job

Podemos agregar parametros desde Jenkins, para no estar modifcando la area de shell/ejecutar
![alt text](./img/3_parametros_desde_jenkins.PNG)
la ventaja de esta forma esque antes de ejecutarse nos preguntara el paramero para ejecutarlo.
y lo llamamos tal como si de definieran las variables arriba.

[Script](./img/script.sh)

```bash
/opt/miScript.sh $NOMBRE $APP
```

Tenemos otros tipos de parametros como son

```r
Eleccion,     ejecucion
cadena,       fichero
Contrasena.   texto
boleano
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 17. Incluye parámetros a tu Job para ejecutar un script

1, Cambiamos el script ``` nano ~/jenkins/miScript.sh ```

<!-- condicionales bash condicional bash condicionales en bash condicional en bash -->

```bash
#!/bin/bash
NOMBRE=$1
APELLIDO=$2
MOSTRAR=$3
if [ "$MOSTRAR" = "true" ]; then
  echo "Hola $NOMBRE $APELLIDO"
else
  echo "para mostrar el nombre selecciona la casilla de mostrar"
fi
```

2, Pasamos el script al contenedor ``` sudo docker cp /home/mario/jenkins/miScript.sh jenkins:/opt ``` .

3, Agregamos una variable desde la UI de Jenkins y en ejecutar agregamos la nueva variable.

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 3: Jenkins & Docker

### 19. Docker + Jenkins - SSH - I

Podemos ejecutar Jobs en en un host distinto al que tenemos instalado jenkins,
osea ejecutar un job en un host remoto

1, En **Docker host** creamos contenedor que nos servira como host remoto.

  1.1.  Creamos una carpeta centos o como sea y nos metemos ahi

  1.2. Creamos llave, y si nos fijamos se crearon 2 archivos

```bash
ssh-keygen -f remote-key
```

  1.3. Agregamos dockerfile.  ```cd docker && nano dockerfile```

<!-- levantar contenedor ssh, iniciar contenedor ssh, crear contenedor ssh con centos ssh, -->

```dockerfile
FROM centos:7

RUN yum -y install openssh-server

RUN useradd remote_user && \
    echo "remote_user" | passwd remote_user --stdin && \
    mkdir /home/remote_user/.ssh && \
    chmod 700 /home/remote_user/.ssh

COPY remote-key.pub /home/remote_user/.ssh/authorized_keys

RUN chown remote_user:remote_user -R /home/remote_user/ && \
    chmod 600 /home/remote_user/.ssh/authorized_keys

RUN /usr/sbin/sshd-keygen > /dev/null 2>&1

CMD /usr/sbin/sshd -D
```

**Error:** returned a non-zero code: 127, code 127.
**Descripcion:** Al tratar de construir la imagen marco este error.
**Causa:** el codigo 127 significa que algun **comando no fue encontrado** en mi caso passwd no lo encontraba.
**Solucion 1:** Instalar el comando/programa que falta en mi caso **yum -y install passwd**
**Solucion 2:** En mi caso tenia **FROM centos** que descargo la ultima (la 8) que al parecer no tiene esos
progrmas asi que lo cambie a la  **FROM centos:7**

2, Modificamos el docker-compose.yml para levantar los 2 host juntos en un paso.

```dockerfile
version: '3'
services:
    jenkins:
      container_name: jenkins
      image: jenkins/jenkins
      ports:
        - "8080:8080"
      volumes:
        - /home/mario/docker/jenkins/jenkins_home:/var/jenkins_home
      networks:
        - net
    remote_host:
      container_name: remote_host_name
      image: remote_host_image
      build:
        context: centos/
      networks:
        - net
networks:
    net:
```
<!-- levantar docker-compose.yml (el segundo) -->
2.1. Contruimos ``` sudo docker-compose build ``` y levantamos ``` sudo docker-compose up -d ```

**Resultado** Ya tenemos iniciados dos contenedores ```sudo docker ps```

```log
CONTAINER ID   IMAGE               COMMAND                  CREATED         STATUS         PORTS                               NAMES
4c395476ed0b   remote_host_image   "/bin/sh -c '/usr/sb…"   27 seconds ago  Up 21 seconds                                      remote_host_name
a563a2456d66   jenkins/jenkins     "/sbin/tini -- /usr/…"   3 hours ago     Up 3 hours     0.0.0.0:8080->8080/tcp, 50000/tcp   jenkins
```

3, Nos conectamos al contenedor desde Jenkins.

  3,1. Entrar al contenedor de jenkins `sudo docker exec -ti jenkins bash`
  3,2. Hacemos ping `ping remote_host_name`
  3,3. Entramos desde ssh en Jenkins a remote_host_name `ssh remote_user@remote_host_name`

**Deberiamos haber poder entrado con la contraseña que definimos:**

1. Entrar con la llave publica (sin contraseña)
4.1. Copiamos la llave privada (remote-key sin el .pub) al contendor de jenkins `docker cp remote-key jenkins:/home/remote-key` y nos conectamos al contenedor jenkins
4.2. En la misma carpeta donde compiamos la llave ejecutamos `ssh -i remote-key remote_user@remote_host_name`
**Deberiamos de haber podido entrar sin contraseña lo cual vamos a hacer esto desde la UI de jenkins**

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 22. Aprende a instalar plugins (SSH Plugin)

<!-- agregar plugins, añadir plugins, en Jenkins-->
1. Vamos a, UI de Jenkins -> Administrar Jenkins -> Administrar plugins -> Todos los plugins y buscamos "ssh"
e instalamos el que dice solo ssh "SSH This plugin executes shell commands remotely using SSH protocol." y le damos
intalar sin reiniciar.

1.2. Al terminar deberemos de verlo en la mista parte pero en "plugins intalados"

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 23. Integra Jenkins con tu server SSH

<!-- añadir ssh, configurar ssh -->
1. agregar llave ssh a Jenkins.
  1.1 Vamos a, Jenkins UI -> Credenciales -> jenkins -> Global credencials -> Add Credencials.
![4_agrgar_llave_ssh_desde_UI.PNG](./img/4_agrgar_llave_ssh_desde_UI.PNG)

2. vamos a, Jenkins UI -> Administrar Jenkins -> bajamos a "SSH remote hosts"  -> Añadir.
![5_agregar_conexion_a_plugin_ssh.PNG](./img/5_agregar_conexion_a_plugin_ssh.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 24. Ejecuta tu primer Job en un host remoto vía SSH

1, Agregamos nuava tarea -> añadir un nuevo paso -> Execute shell script on remote host using ssh

2, Seleccionamos el host en donde se ejecutaran estos en este caso solo tenemos uno

```host
Site: remote_user@remote_host_name:22
Command: echo "Hola desde jenkins" > /tmp/test.txt
```

3, damos sobre contruir ahora y debe de dar ok *azul*

4, Si vamos al directorio /tmp veremos que esta el archivo creado.

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 4: Jenkins & AWS

### 26. Introducción: MySQL + AWS + Shell Script + Jenkins

Vamos a crear un Job que hara un respaldo de una base de datos y lo guardara en una
instancia de almacenamiento de AWS

### 27. Instalar MySQL en Docker

1. En nuestro docker-compose.yml agregamos un contenedor de MySQL

```yml
version: '3'
services:
    jenkins:
      container_name: jenkins
      image: jenkins/jenkins
      ports:
        - "8080:8080"
      volumes:
        - /home/mario/docker/jenkins/jenkins_home:/var/jenkins_home
      networks:
        - net
    remote_host:
      container_name: remote_host_name
      image: remote_host_image
      build:
        context: centos/
      networks:
        - net
    database_host:
      container_name: database_host
      image: mysql:5.7
      environment:
        - "MYSQL_ROOT_PASSWORD=root"
      volumes:
        - /home/mario/docker/mysql/data:/var/lib/mysql
      networks:
        - net

networks:
    net:
```

1.1. levantar docker-compose.yml

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 28. Instala MySQL Client y AWS CLI

1. Instalamos a nuestro remote_host.
1.1. mysql para que nos podamos conectar a este y hacer el respaldo "export".
1.2. epel-release python-pip para poder tener el comando pip.
1.3. desde pib install el CLI de AWS para poder subir el respaldo a una instancia S3.

```dockerfile
FROM centos:7

RUN yum -y install openssh-server

RUN useradd remote_user && \
    echo "remote_user" | passwd remote_user --stdin && \
    mkdir /home/remote_user/.ssh && \
    chmod 700 /home/remote_user/.ssh

COPY remote-key.pub /home/remote_user/.ssh/authorized_keys

RUN chown remote_user:remote_user -R /home/remote_user/ && \
    chmod 600 /home/remote_user/.ssh/authorized_keys

RUN /usr/sbin/sshd-keygen > /dev/null 2>&1

RUN yum -y install mysql epel-release && \
    yum -y install python-pip && \
    pip install --upgrade pip && \
    pip install awscli

CMD /usr/sbin/sshd -D
```

2, Hacemos un build y levantamos ``` docker-compose build ```, - ``` docker-compose up -d ```

**Resultado:** En nuestro remote_hosr deberiamos de tener los comando ```aws y mysql```  dispobibles

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 29.30.31. Crea una DB sencilla en MySQL. Crea un Bucket en S3. Crea un usuario IAM en AWS para autenticación

**Objetivo:** Crearemos una BD y un respaldo de esta y subiremos el respaldo a una Bucket S3 de amazon.

1. Nos conectamos a remote_host, y nos conectamos a mysql, y creamos una db

```sql
CREATE DATABASE test_db;
USE test_db;
CREATE TABLE info(nombre VARCHAR(20), edad INT(2));
INSERT INTO info(nombre, edad) VALUES("Mario", 26);
INSERT INTO info(nombre, edad) VALUES("Neey", 24);
```

2, Creamos una cuenta en AWS y una Bucket S3 en mi caso se llama "mario51s3test".

![6_cuenta_de_AWS_y_bucket_s3.PNG](./img/6_cuenta_de_AWS_y_bucket_s3.PNG)

Aqui es donde subiremos nuestros respaldos

1. Vamos a la web de amazon -> Servicios -> iam -> usuarios -> add user
3.1. le damos nombre y marcamos la casilla "Programmatic access" y Creamos usuario

4, Ya creado podremos dercargar el .csv y lo descargamos.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 32. Aprende a tomar un dump de una DB y a subirlo a AWS S3 manualmente

**Objetivo:** Haremos el proceso de forma manual para luego subirlo de forma
automatica con Jenkins.

1. Crear respaldo.
  1.1. Entramos desde nuestro dockerHub al contenedor remoto ``` sudo docker exec -ti remote_host_name bash ```
  1.2. Creamos el respaldo (recordar que tenemos la bd en "database_host") ``` mysqldump -u root -h database_host -proot test_db > /tmp/test_db.backup.sql ```

2. Subirlo a AWS
  2.1. [configurar acceso](https://docs.aws.amazon.com/es_es/cli/latest/userguide/cli-chap-configure.html) como
  vemos en el link haremos la forma de las variables

```bash
export AWS_ACCESS_KEY_ID=[primer_campo_de_.csv]
export AWS_SECRET_ACCESS_KEY=[segundo_campo_de_.csv]
```

  2.2. Subimos el archivo a aws ``` aws s3 cp /tmp/test_db.backup.sql s3://mario51s3test ```

Ya deberiamos de poder ver nuestro archivo desde la web de aws

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 33.34. Crea un script para automatizar el export de la DB. Integra tu script con AWS (AWS CLI)

1. Creamos script que creara y subira a aws nuestro respaldo, para luego ejecutarlo desde Jenkins.
  1.1. Usamos variables que le pasaremos desde jenkins para que sea desacoplado el script.
  1.1. Usaremos ``` && \ ``` para que si falla no ejecute el siguiente.

```bash
#!/bin/bash

DB_HOST=$1
DB_PASS=$2
DB_NAME=$3

AWS_ACCESS=$4
AWS_PASS=$5
AWS_BUCKET_NAME=$6

DATE=$(date +%Y-%m-%d--%H:%M)

mysqldump -u root -h $DB_HOST -p$DB_PASS $DB_NAME > /tmp/test_db_$DATE.backup.sql && \
export AWS_ACCESS_KEY_ID=$AWS_ACCESS  && \
export AWS_SECRET_ACCESS_KEY=$AWS_PASS  && \
aws s3 cp /tmp/test_db_$DATE.backup.sql s3://$AWS_BUCKET_NAME
```
<!-- uso de vi, como usar vi editor vi, uso vi, comados vi https://www.thegeekdiary.com/basic-vi-commands-cheat-sheet/ -->

1. Lo guardamos y le damos permisos de ejecucion `chmod +x /tmp/script.sh`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 35. Aprende a manejar data sensible en Jenkins (Contraseñas, keys)

Para esto Jenkins nos da la posibilidad de guardar contraseñas que podremos llamar de forma
segura a nuestros Jobs

1. Jenkins IU -> Credencials -> Jenkins -> Global credencials -> Add Credencials -> Secret Text

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 36. Crea un Jenkins Job para subir tu DB a AWS (Parámetros + Shell Scripting)

1, Jenkins UI -> Nueva Tarea -> estilo libre

2, buscar "Esta ejecución debe parametrizarse" y agregamos los parametros no sensibles

3, buscar "Use secret text(s) or file(s)" y agregamos los parametros NO sensibles, pass de usr de la bd y pass de aws

4, buscar "Ejecutar" -> Execute shell script on remote host using ssh -> marcamos "Hide command from console output"

```bash
/tmp/script.sh $DB_HOST $DB_PASS $DB_NAME $AWS_ACCESS $AWS_PASS $AWS_BUCKET_NAME
```

5, Guardamos y corremos el Job,

**Resultado:**

![7_script_subir_respaldo_a_aws_de_mysql](./img/7_script_subir_respaldo_a_aws_de_mysql.gif)

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 5: Jenkins & Ansible

Ansible es una herramienta de Red Had que para los de operaciones es una herramienta importante
la cual es tan o mas poderosa que el shell scripting (los .sh en bash que escribimos) ya que este
usa python el cual podemos acceder a todos sus modulos lo cual hace mas facil y poderosas las cosas.

### 43. Instala Ansible en Jenkins

1. Creamos una imagen de Jenkins pero le agregamos una instalacion de Ansible

```dockerfile
FROM jenkins/jenkins

USER root

RUN curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py" && python get-pip.py

RUN pip install -U ansible

USER jenkins
```

2, Modificamos nuestro docker compose

```yml
version: '3'
services:
    jenkins:
      container_name: jenkins-ansible
      build:
        context: ./jenkins-ansible
      ports:
        - "8080:8080"
      volumes:
        - /home/mario/docker/jenkins/jenkins_home:/var/jenkins_home
      networks:
        - net
    remote_host:
      container_name: remote_host_name
      image: remote_host_image
      build:
        context: centos/
      networks:
        - net
    database_host:
      container_name: database_host
      image: mysql:5.7
      environment:
        - "MYSQL_ROOT_PASSWORD=root"
      volumes:
        - /home/mario/docker/mysql/data:/var/lib/mysql
      networks:
        - net

networks:
    net:
```

3, construimos `docker-compose build` e iniciarmos `docker-compose up -d`

**Resultado** Si entramos al contenedor de jenkins-ansible podremos ejecuat el comando "ansible"

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 44. Asegúrate de que Jenkins tenga las llaves SSH

1. Copiamos la llave privada ssh de nuestro docker hub al contenedor de jenkins

```bash
cp centos/remote-key jenkins/jenkins_home/ansible/remote-key
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 45.65. Crea un archivo de inventario sencillo: Docker + Ansible + Jenkins / Permite que los usuarios se registren en Jenkins

- En Jenkins UI -> administrar configuracion global de seguridad
  - casilla "Activar seguridad" : podremos desactivar/activar el login de Jenkins
  - casilla "Permitir que los usuarios se registren" : **No es buana idea** ya que podria ser un problema de seguridad

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 6: Jenkins & Security

### 66. Instala un Plugin de seguridad

- El plugin se llama **Role-based Authorization Strategy** descripcion "Enables user authorization using a Role-Based strategy.
Roles can be defined globally or for particular jobs or nodes selected by regular expressions"
  - Al terminar la instalacion aparecera en Configuración global de la seguridad una casilla **Role-Based Strategy** que no
  estaba antes, la cual marcamos.
  - Al marcar la casilla delegara el menajo de roles a este plugin y nos aparecera uno menu "Configuración global de la seguridad" llamado
  **Manage and Assign Roles**.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 67. Crear usuario en Jenkin

Podemos hacerlo en Administrar Jenkins -> Usuarios -> Crear un usuario

### 68.69. Crea un rol global de solo lectura - Asigna el rol anterior a un usuario en Jenkins

En Administrar Jenkins -> Manage and Assign Roles -> Manage Roles, podremos crear roles

1. Ejemplo vamos a crear un usuario que solo podra ver los Jobs
  1.1. Cremoas un rol solo con el permiso de Global/Read que es el basico para poder entrar.
  1.2. Le damos el permiso de Tareas/read.

2. Si nos logeamos con usuario creado y con el rol que creamos solo podra ver las tares pero no ejecutarlas ni nada.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 71. Restringe el acceso a determinados Jobs con roles de proyectos

Para esto debemos de quitar el acceso a la lectura de tareas al rol, solo dejarle el de vision
general, de la siguiente manera, donde inicamos que solo podra ver y contruir las tareas que
complan con un determinado patron.
![8_permisos_para_ver_ejecutar_x_tareas](./img/8_permisos_para_ver_ejecutar_x_tareas.PNG)

<!--

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 7: Jenkins Tips & Tricks

### 73. Variables de entorno disponibles

Si en Google buscamos "jenkins environment variables" encontraremos este [link](https://wiki.jenkins.io/display/JENKINS/Building+a+software+project)
o en local `http://192.168.1.74:8080/env-vars.html/` en local el cual tendremos toda una lista de variables que podremos usar en nuestro
jenkins y ya existen

1. Ejecutamos este codigo desde una tarea de jenkins

```r
echo JAVA_HOME: $JAVA_HOME
echo WORKSPACE: $WORKSPACE
echo GIT_URL: $GIT_URL
echo JENKINS_URL: $JENKINS_URL
```

Nos data la siguiente salida

```r
JAVA_HOME: /usr/local/openjdk-8
WORKSPACE: /var/jenkins_home/workspace/var-env
GIT_URL:
JENKINS_URL: http://192.168.1.74:8080/
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 74. Crea tus variables de Entorno

Podemos crear nuestras variables

1. vamos a, Jenkins UI -> Configurar el Sistema -> casilla "Variables de entorno"

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 76. Ejecuta Jobs con Cron

Podemos crear un cron facilmente en la pagina de **crontab.guru**

1. Vamos a la tarea que queremos aplicarle el Cron

2. Vamos a Configugurar -> buscamos "Ejecutar periodicamente"

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 77. Crea un usuario con permiso para ejecutar los Jobs 78. Gatilla Jobs con curl (Sin parámetros)

<!-- 
correr job desde url, ejecutar job desde url, iniciar job desde url, correr con url, ejecutar con url, 
iniciar con url, correr con una url, ejecutar con una url, iniciar con una url, utilizar url para correr job,
utilizar http para correr job,  correr job desde en enlace, ejecutar job desde enlace, iniciar job desde enlace, 
correr con un enlace, ejecutar con enlace, correr job desde http
-->

Podemos crear un script que ejecute algun job que agregamos en jenkins asi
por ejemplo cada vez que un usuario haga X cosa el Job se ejecutara.
Para eso haremos uso de cURL para ejecutar el Job y debemos de saber que jenkins tiene **CSRF Protection**
previene ataques de estres por peticiones.

```r
crumb=$(curl -u "neey:neey" -s 'http://192.168.1.74:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')
curl -u "neey:neey" -H "$crumb" -X POST http://192.168.1.74:8080/job/Variables%20Dos/build?delay=0sec
curl -u "neey:neey" -H "$crumb" -X POST  http://192.168.1.74:8080/job/backup-db-to-aws/buildWithParameters?DB_HOST=db_host&DB_NAME=testdb&BUCKET_NAME=jenkins-udemy
```

**PENDIENTE** Para ejecutarlo desactive CSRF pero esto podria significar un riesgo.

<!-- crear-token-ID:edcvbh87n -->

**Solucion-alter** Podemos generar un token y utilizar este para correr
nuestros jobs [Fuente](https://stackoverflow.com/a/63191483)

1. Generar token en `Accounts icon -> configure -> API Token -> Add New token`
2. Enviar peticion `curl -v -X POST <DOMAIN>/job/<JOB>/build --user mario:<TOKEN>`

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 8: Jenkins & Email

No siempre podemos estar pendientes para saber cuando un Job empieza a fallar,
para esto podemos hacer uso de jenkins/mailer, para que en tanto ocurra un x evento
nos notifique por correo.

Por defecto ya esta instalado el plugin si no, lo instalamos como cualquier otro.

### 82. Configura el envío de notificaciones con AWS SES

AWS SES es un servicio de notificaciones

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 83. Configura el envío de notificaciones con Gmail

<!-- enviar correo por jenkins, configurar el envio de correos en jenkins, mandar correo por jenkins envio de correo -->
1, Desactivamos la proteccion de aplicaciones de gmail

  1.1. Buscamos en google "let less secure apps access your account gmail" o vamos a este [link](https://devanswers.co/allow-less-secure-apps-access-gmail-account/)

  1.2. Por ahi ahi un link que podemos que nos llevara a un switch "Permitir el acceso de aplicaciones poco seguras:" lo activamos

2, Vamos a Administrar Jenkins.

- en "System Admin e-mail address" ponemos nuestro correo.
- en "Servidor de correo saliente (SMTP)" ponemos.
- habilitamos la casilla "Use SMTP Authentication".
- Nombre de usuario: nuestro correo de gmail.
- Contraseña: nuestra contraseña de gmail.
- Puerto de SMTP: 465.

3,  Activamos la casilla "Probar la configuración enviando un correo de prueba" y le damos probar configuracion.

**Resultado:** Nos ddebio de llegar un correo a nuestra cuenta de gmail

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 9: Jenkins & Maven

El plugin se llama **Maven Integration plugin** y como requisito necesitamos el
"Git plugin" en mi caso solo se llama **Git**

### 88. Aprende a hacer pull desde un repo en Git/GitHub

A. Creamos Job

1. un nuevo  y en Configurar el origen del código fuente seleccionamos Git

  1.1. Como ejemplo le ponemos la sig url: `https://github.com/jenkins-docs/simple-java-maven-app` que es
  un repo de un proyecto maven que nos da el equipo de jenkins.

![9_hola_git_jenkins.PNG](./img/9_hola_git_jenkins.PNG)

B. Le damos construir y automaticamente se descargara el repositorio, en la carpata
que tengamos el workspace mas nombre del Job en este caso /var/jenkins_home/workspace/maven-hola.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 89. Construye el código con Maven

1. Connfiguramos maven
  1.1. vamos a, Jenkins UI -> Administrar Jenkins -> Global Tool Configuration -> buscamos "Maven"  -> Añadir maven.
  1.2. Le damos el nombre que querramos y seleccionamos la version y guardamos cambios.

2. Vamos a nuestro proyecto que queremos construir
  2.1. Dentrode la configuracion de la tarea vamos a "Ejecutar" -> "Ejecutar tareas maven de nivel superior"
  2.3. Seleccionamos el maven que añadimos apenas
  2.4. En Goles le damos el comando maven `-B -DskipTests clean package` en este caso para que construlla sin ejecutar tests

3. Guardamos y ejecutamos.

**Resultado:** La primera vez que ejecutamos descargara maven y luego construira el proyecto dando el jar en mi
caso lo entrego aqui */var/jenkins_home/workspace/maven-hola/target/my-app-1.0-SNAPSHOT.jar*

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 90. Realiza tests en tu código

Para esto agregamos un nuestra tarea un nuevo paso de Maven donde en Goals
solo le ponemos `test`

![15-ejecutar-test-maven](./img/20-ejecutar-test-maven.PNG)

**Resultado:** Lo mismo que arriba pero ahora se ejecutan los test que vienen
en el repo.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 91. Despliega tu JAR localmente

Para eso en la cinfiguracion de nuestro job en ejecutar agegamos un "Ejecutar linea de comandos" con

```bash
echo ***********************
echo Desplegando App.jar

java -jar /var/jenkins_home/workspace/maven-hola/target/my-app-1.0-SNAPSHOT.jar

echo termino proceso de desplegado
echo ***********************
```

**Resultado:** Al ejecutar el Job este 1) descarga/actualiza el codigo de git, 2) construlle el
.jar, 3) desppliega en el mismo servidor, en este caso solo muestra un Hello Word.

![10_descargar_compilar_pruebas_desplegar_proyecto_maven](./img/10_descargar_compilar_pruebas_desplegar_proyecto_maven.gif)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 92. Archiva el resultado de tus tests en Jenkins

El resultado de los test se guardan en {nombre_maven}/target/surefire-reports/** en mi caso
/var/jenkins_home/workspace/maven-hola/target/surefire-reports/, a los cuales usaremos en.

1. Configuracion del Job -> `Acciones para ejecutar después` -> Publicar los resultados de test Junit

  1.1. Donde dice `Ficheros XML con los informes de tests`

   1.1.1. agregamos `target/surefire-reports`

   1.1.2. En otro proyecto me dio lata y use `**/surefire-reports/*.xml`

  1.2. Guardamos ejecutamos y al terminar en nuestro home de Job veremos que se agrega
  un nuevo **Últimos resultados de tests**

![11_mostrar_grafica_test_jenkins](./img/11_mostrar_grafica_test_jenkins.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 93. Archiva artifacts en tu Job

Podemos capturar el ultimo Jar que se capturo con exito y lo podremos
mostrar desde jenkins

1. En nuestro Job agregamos en "Acciones para ejecutar despues" -> ***guardar los archivos generados**

2. Ficheros para guardar: `target/*.jar`

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 94. Envía notificaciones del estado de tus Jobs vía mail

Vamos a enviar un correo en caso de que falle.

1. Vamos a la configuracion de nuestro Job -> Acciones para ejecutar despues -> Notificacion por correo
1.1. Y ponemos el correo destino marcando las dos casillas.

2. Modificamos algo para que falle intencionalmente el Job y comprobar que llega el correo.

**Resultado:** Cuando falla nuestro Job nos llega un correo indicando que fallo x tarea.

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 10: Jenkins & GIT

**Objetivo:** Vamos a crear un contenedor con un servicio de docker

1. Buscamos en Google "gitlab ce Docker" y en la documentacion - [run-the-image](https://docs.gitlab.com/omnibus/docker/#run-the-image)
tendremos los datos para poder crear nuestro contenedor.

<!-- credenciales de gitlab al iniciar contenedor root:12345678, usuario y contraseña de gitlab, usuario de gitlab y contraseña de gitlab-->
**Nota:** Solo agregamos la parte de gitlab, y CUIDADO descarga como 1gb para tener
la imagen, y tarda como 30min en levantar, le puse contraseña del 1 al 8.

```yml
version: '3'
services:
    jenkins:
      container_name: jenkins-ansible
      build:
        context: ./jenkins-ansible
      ports:
        - "8080:8080"
      volumes:
        - /home/mario/docker/jenkins/jenkins_home:/var/jenkins_home
      networks:
        - net
    remote_host:
      container_name: remote_host_name
      image: remote_host_image
      build:
        context: centos/
      networks:
        - net
    database_host:
      container_name: database_host
      image: mysql:5.7
      environment:
        - "MYSQL_ROOT_PASSWORD=root"
      volumes:
        - /home/mario/docker/mysql/data:/var/lib/mysql
      networks:
        - net

    gitlab:
      container_name: gitlab_host
      hostname: gitlab.example.com
      ports:
        - "443:443"
        - "80:80"
      restart: always
      volumes:
        - "/srv/gitlab/config:/etc/gitlab"
        - "/srv/gitlab/logs:/var/log/gitlab"
        - "/srv/gitlab/data:/var/opt/gitlab"
      image: gitlab/gitlab-ce:latest
      networks:
        - net

networks:
    net:
```

**Resultado** Tendremos gitlab en nuestro local con el uso de la imagen oficial sin
complicaciones.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 97. Crea un repo en GIT

1, en nuestro gitLab UI Creamos un grupo y un proyecto

2, Creamos usuarios Vamos a Admin Area (en la parte de arriba ahi una llave/perico) -> users -> new user

3, Vamos a proyectos y agregamos el usuarios como desarrollador

4, Agregamos un proyecto maven al repo y lo subimos a nuestro gitlab

> **Error:** failed to push some refs to.
> **Descripcion:** A la hora de hacer un push con nuestro usuario Developer nos muestra este error.
> **Causa:** El rol de Developer no tiene permisos de crear una rama.
> **Solucion:** A nuestro usuario le damos un rol arriba "maintainer" o creamos una rama con otro usuario.

5, Creamos una credencial sobre la llave naranga -> credencial de usuario/contraseña -> ponemos la contraseña
del usuario que creamos

6, A nuestra tarea de maven le cambiamos la url por la de nuestro proyecto en gitlab en mi caso
`http://192.168.1.74/my_repo_one/hola-maven.git`.

**Resultado:** Con el mismo Job de de maven descargar/contruir/probar/desplegar funciona pero
ahora desde nuestra propia instancia de GitLab

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 101.102. Git Hooks . Realiza un cambio al repo y gatilla el Job de Jenkins

uno de los pilares de DevOps es que los Jobs de jenkins estan enlazados con un repositorio de Git,
es decir que cuando hacemos un push al repositorio se ejecutaria un Job, y para esto nos sirven los Hooks.

<!-- agregar hooks a gitlab, ruta hooks ruta, ubicacion hooks ubicacion -->
1. Agregar Hook personalizado en nuestro *contenedor de gitlab*.

  1.1. Entramos al contenedor a la ruta en mi caso en la primer ruta

- `/var/opt/gitlab/git-data/repositories/@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.git`

- igual deberian esta en /var/opt/gitlab/git-data/repositories/

**NOTA** Para no tener la ruta hashed/6b/86/6b86b273... que no se entiende podemos correr el
siguiete comando para que lo vemoas con una estructura mejor.
En mi caso solo tenia un proyecto con el id 1 (lo podmeos ver en los detalles en UI de GitLab),

```r
sudo gitlab-rake gitlab:storage:rollback_to_legacy ID_FROM=1 ID_TO=20
```

**Resultado** Al terminar el comando tenemos una ruta mas entendible en
mi caso /var/opt/gitlab/git-data/repositories/my_repo_one/hola-maven.git/

1. En esta ultima ruta agregamos una nueva carpeta **custom_hooks** y dentro un **post-receive**
que se refiere a que hara GitLab despues de resivir algo lo cual le ponemos el siguietne Script.
  2.1. Lo que hara es que si la rama que esta llegando es manster entonces consumimos
  el servicio para arrancar nuestra.

```bash
#!/bin/bash
# Get branch name from ref head

if ! [ -t 0 ]; then
  read -a ref
fi
IFS='/' read -ra REF <<< "${ref[2]}"
branch="${REF[2]}"

if [ $branch == "master" ]; then
#  crumb=$(curl -u "maro:mario" -s 'http://jenkins:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')
#  curl -u "mario:mario" -H "$crumb" -X POST http://192.168.1.74:8080/job/maven-hola/build
   curl -u "mario:mario" -X POST http://192.168.1.74:8080/job/maven-hola/build

  if [ $? -eq 0 ] ; then
    echo "*** Ok"
  else
    echo "*** Error"
  fi
fi
```

2.2. Le damos permismos de ejecucion: `chmod +x post-receive` y `chown git:git custom_hooks -R`

**Hasta este punto** ya deberia de estar listo el hecho de hacer un push y que se gatille nuestro Job

1. Vamos a hacer un cambio y a subirlo

<!-- mostrar archivos con x cadena, , mostrar archivos que contengan x cadena, buscar archivos con grep para encontrar archivos, buscar archivos con grep para buscar archivos
grep encontrar archivo
-->

**Resultado** Al hacer Push se ejecuta Tarea de Jenkins que descarga, compila, prueba y despliega.
![12_descargar_compilar_pruebas_desplegar_proyecto_maven_haciendo_push.gif](./img/12_descargar_compilar_pruebas_desplegar_proyecto_maven_haciendo_push.gif)

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 11: Jenkins & DSL

Doc: `https://jenkinsci.github.io/job-dsl-plugin/#plugin/git`

Hasta ahora hemos estando creando los Jobs de forma manual desde la
UI-web de jenkins, pero que pasaria si quisieramos crear los Jobs de
forma automatica.
Aqui entra DSL que es un Plugin de Jenkins donde nos ayuda a crear los
Jobs de forma automatica,

### 105. Instalación de plugin DSL

Se llama "Job DSL"

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 106..113 Seed Job - SCM Integracion git -  Triggers (CRON) - Steps - Mailer

La forma en los que se crean los Jobs en DSL es es a travez de un
script que al ejecutarse se creara nuestro Job.
Este codigo que construlle el Job le llamamos Seed.

**Objetivo:** Ejemplo crear Job con DSL.

1. Creamos un Job y en ejecutar seleccionamos **Process Job DSLs**.
2. Seleccionamos "Use the provided DSL script" y en "DSL Script".

```r
job('nombre_job') {
  description('Descripcion del Job')
  parameters {
    stringParam('planet', defaultValue='earth')
    booleanParam('isSometing', true)
    choiceParam('OPTION', ['option 1', 'option 2', 'option 3' ])
  }
  scm { git('https://github.com/jenkins-docs/simple-java-maven-app', 'master') }

  triggers{ cron('H 5 * * 7') }

  //son como las opciones a ejecutar en este caso un comando bash
  steps{ shell("echo 'hello from DSL'") }

  //envio de notificacion por mail
  publishers { mailer('exmple@mail.com', true, true) }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 115. Construye el Job de Maven en DSL

El mismo Job que creamos de maven descarga/construlle/prueba/despliega pero
creado desde DSL

```r
job('maven_dsl') {

    description('Maven dsl project')

    scm {
        git('https://github.com/jenkins-docs/simple-java-maven-app', 'master', {node -> node / 'extensions' << '' })
    }

    steps {
        maven {
            mavenInstallation('jenkins-maven')
            goals('-B -DskipTests clean package')
        }
        maven {
            mavenInstallation('jenkins-maven')
            goals('test')
        }
        shell('''
            echo ********************
            echo "Desplegando el jar"
            echo ********************

            java -jar /var/jenkins_home/workspace/maven-job/target/my-app-1.0-SNAPSHOT.jar
        ''')
    }

    publishers {
        archiveArtifacts('target/*.jar')
        archiveJunit('target/surefire-reports/*.xml')
        mailer('ricardo.andre.gonzalez07@gmail.com', true, true)
    }
}
```

**PENDIENTE** hacer videos

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 12: CI/CD - Definiciones

Casi siempre vemos a CI (Continuous Integration) con CD (Continuous Delivery) ya
que son parte de los mismo solo que en etapas diferentes

- CI: Es lo que hemos venido haciendo que es al hacer push se hacen pruebas
unitarias, de integracion y funcionales.

- CD: Este es cuando se despliega a un entorno previo y se hacen pruebas de
aceptacion.

- **CD (Continuous Deployment)**: Cuando las pruebas de aceptacion pasaron en los
ambientes previos entonces ahora se despliega en produccion, proceso el cual igual
puede ser hecho con Jenkins.

![13_diagrama_proceso_CI-CD.PNG](./img/13_diagrama_proceso_CI-CD.PNG)

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 13: Jenkins Pipeline - Jenkinsfile

**DOC:** `https://jenkins.io/doc/book/pipeline/`

Un Pipeline se traduce literalmente como tuberia, y se refiere al workflow (diferentes etapas) que pasa de la
etapa de desarrollo a la de produccion, el cual nos dan el siguiente ejemplo el equipo de Jenkins.

![13_diagrama_proceso_CI-CD_team_jenkins](./img/13_diagrama_proceso_CI-CD_team_jenkins.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 125. Introducción a Jenkinsfile

<!-- etapas de jenkins -->

Jenkinsfile es una herramienta que nos permite crear nuestro pipelines que hablabamos arriba, este ya
lo hicimos con nuestro Job descarga/compila/prueba/despliega pero no lo hicimos de forma declarativa,
el cual lo hariamos con este Jenkinsfile el cual luce de la siguiente manera.

![14_ejemplo_jenkinsfile](./img/14_ejemplo_jenkinsfile.PNG)

Estos Scripts pueden ser escritos en diversos lenguajes como groovy, python etc para usar la sinercia
que ya traen pero nosotros usaremos Declarative Pipeline que es el DSL de jenkins para comprenderlo
mas facilmente.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 126. Instala el plugin de Pipeline

Se llama "Pipeline" y ay viene por defecto instalado pero en caso lo instalamos
sin mayor misterio.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 127. Crea tu primer Pipeline

Vamos a crear un Job pero ahora en lugar de escojer "estilo libre" escogeremos "Pipeline"

1. Veremos opciones muy parecidas en el cual podremos poner el Script directamente en el formulario
o desdecargarlo desde un repositorio y ejecutarlo desde un archivo.

1.1. En este caso lo pondremos como Pipeline -> Definition: Pipeline script -> y en Script lo siguiente

```r
pipeline {
  agent any
  stages {
    stage('Build') {
      steps {
        echo 'Build start...'
      }
    }
    stage('Test') {
      steps {
        echo 'Test start...'
      }
    }
    stage('Deploy') {
      steps {
        echo 'Deploy start...'
      }
    }
  }
}
```

**Resultado:** Se ejecutara el Job pero nos dara una grafica de como
va cada etapa.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 128. Agrega más steps a tu pipeline

<!--
pipeline con multiples comados en pipeline, pipeline con varios comandos en pipeline
pipeline multiples comandos, pipeline varios comandos
-->

Dentro de los Stages podemos ejecutar diferentes pasos con comillas simples (no es como la que usamos en markdown)

```r
stage('Test') {
  steps {
    sh ' echo "Primer paso" '
    sh '''
        echo "Segundo Paso"
        echo "Segundo Paso seccion A"
    '''
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 129. Retry

Podemos hacer que una tarea se ejecute mas de una vez si esta falla, abajo vemos un shell
que esta mal pero este se va a ejecutar 3 veces.

```r
stage('Test') {
  steps {
    retry(3) {
      sh ' falla_a_proposito '
    }
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 130. Timeouts

Podemos indicar que a partir de X tiempo falle la tarea para que no quede trabado el

```r
stage('Test') {
  steps {
    timeout(time: 3, unit:'SECONDS') {
      sh 'sleep 5'
    }
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 131. Post actions

Es la accion a tomar cuando termine una etapa.

```r
stage('Prueba') {
  steps {
//  sh ' echo "Fail."; exit 1 '
    sh ' echo "Success."; exit 0 '
  }
  post {
    // se ejecuta con exit 1
    // ej. independientemente de que falle o salga bien guardaremos los tests o aremos respaldo
    always   { echo 'Siempre me voy a ejecutar'}

    success  { echo 'Solo me ejecuto si nada falla'}

    // se ejecuta con exit 1
    failure  { echo 'Solo me ejecuto si el build falla'}

    unstable { echo 'Solo me ejecuto si se marca como inestable'}

    // ej. se estaba construyendo bien pero fallo se ejecuta, y si falla o sale bien mas de 2 veces no se ejecutara, osea solo si cambia
    changed  { echo 'El pipeline paso de estar fallando a seguir correctamente o viceversa'}
  }
}
```

No es necesario definir todos solo los que necesitemos.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 132. Variables de entorno

pipeline jenkins

```r
environment {
  NOMBRE = 'Mario'
  APP = 'Marquez'
}
stages {
  stage('Test') {
    steps { sh ' echo nombre: $NOMBRE apellido: $APP  ' }
}}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 133. Credentials

pipeline jenkins

```r
environment {
  SECRETO = credentials('NOMBRE_ID_CREDENCIAL_GUARDADA')
}
stages {
  stage('Test') {
    steps { sh ' echo $SECRETO' }
}}
```

**Resultado:** Solo veremos asteriscos en la salida ya que Jenkins sabe
que es data sensible.

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 14: CI/CD + Jenkins Pipeline + Docker + Maven

1, Modificamos el context de jenkins del yml para construir una imagen personalizada de jenkins.

<!-- levantar contenedor gitlab, iniciar contenedor gitlab, levantar gitlab, iniciar gitlab imagen gitlab  -->

```r
version: '3'
services:

    jenkins:
      container_name: jenkins-ansible
      build:
        context: ./jenkins-ansible-docker
      ports:
        - "8080:8080"
      volumes:
        - /home/mario/docker/jenkins/jenkins_home:/var/jenkins_home
        - /var/run/docker.sock:/var/run/docker.sock
      networks:
        - net
    remote_host:
      container_name: remote_host_name
      image: remote_host_image
      build:
        context: centos/
      networks:
        - net

    database_host:
      container_name: database_host
      image: mysql:5.7
      environment:
        - "MYSQL_ROOT_PASSWORD=root"
      volumes:
        - /home/mario/docker/mysql/data:/var/lib/mysql
      networks:
        - net

    gitlab:
      container_name: gitlab_host
      hostname: gitlab.example.com
      ports:
        - "443:443"
        - "80:80"
      restart: always
      volumes:
        - "/srv/gitlab/config:/etc/gitlab"
        - "/srv/gitlab/logs:/var/log/gitlab"
        - "/srv/gitlab/data:/var/opt/gitlab"
      environment:
        - "DOCKER_DRIVER=overlay"
      image: gitlab/gitlab-ce:latest
      networks:
        - net
networks:
    net:
```
<!-- credenciales gitlab "root:12345678" accessos gitlab usuario gitlab login gitlab, -->

2, Modificamos el dockerfile para tener jenkins + ansible + docker + docker-compose

```dockerfile
FROM jenkins/jenkins

USER root

# Instala ansible
RUN curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py" && python get-pip.py && \
    pip install -U ansible

#instalar Docker
RUN apt update && \
    apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    apt update && \
    apt-cache policy docker-ce && \
    apt install -y docker-ce && \
    systemctl enable docker

# Instalar docker compose
RUN curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose && \
    usermod -aG docker jenkins

USER jenkins
```

3, Constrimos ```sudo docker-compose build``` y levantamos ```sudo docker-compose up -d```

**Resultado** Al entrar al contenedor de jenkins ```sudo docker exec -ti jenkins-ansible bash``` podremos
ejecutar ```sudo docker exec -ti jenkins-ansible bash``` y veremos los contenedores incluso el del mismo jenkins.

Esto es por que estamos compartiendo el **docker.sock** en nuestro .yml.

```r
CONTAINER ID    IMAGE              COMMAND        CREATED    STATUS    PORTS                               NAMES
1f761cd486ee    docker_jenkins     "/sbin/tin…"   sometime   Up        0.0.0.0:8080->8080/tcp, 50000/tcp   jenkins-ansible
3a9c76b2beed    remote_host_image  "/bin/sh -…"   sometime   Up                                            remote_host_name
3f756a61eb7d    mysql:5.7          "docker-en…"   sometime   Up        3306/tcp, 33060/tcp                 database_host
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 137. Define los stages de tu pipeline

Empezaremos a crear nuestro stages desde lo basico.
<!-- archivo basico de jenkisfile basico, archivo jenkisfile basico  -->

```r
pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                //
            }
        }
        stage('Test') {
            steps {
                //
            }
        }
        stage('Deploy') {
            steps {
                //
            }
        }
    }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 138. Build: Genera el JAR de tu app Maven usando Docker

**Objetivo:** Preparar el comando que construira el .jar dentro de un contenedor y lo guardara usando
volumentes dontro de nuestro docker host

1. Cremos una carpeta llamada pipeline donde tendremos lo neceario para que este

  1.1. Dentro de este pegamos en una carpeta el codigo java (proyecto maven)

  1.2. Creamos una carpeta llamada build dentro de jenkins que es donde guardaremos el .jar

<!--  ver estructura de archivos, mostrar estructura de archivos, ver arbol de archivos, ver arbol de archivos  ver estructura de directorios, mostrar estructura de directorios, ver arbol de directorios, ver arbol de directorios
L = Level, cantidad de directorio que queremos ver, por defecto todos -d = Direcotirios que queremos ver -->

```r
tree -a pipeline -L 3 -d pipeline
pipeline
├── build        # donde guardaremos el .jar
└── my-maven-app # codigo java
```

<!-- generar jar, construir jar,  -->

1. Generamos el Jar en una sola linea

```r
sudo docker run --rm -v /root/.m2:/root/.m2 -v /home/mario/docker/pipeline/my-maven-app:/app -w /app maven:3-alpine mvn -B -DskipTests clean package

--rm                     # al final destrulle el contenedor
-v /root/.m2:/root/.m2   # aqui guarda las dependencias asi que las guardamos en un volumen pa no descargarlas cada vez
-v /home/...my-maven-app...:/app  # aqui guarda el .jar lo guardamos en el docker host
-w /app                  # indicamos que queremos estar el el directorio /app
maven:3-alpine           # indicamos que usaremos una imagen de maven version 3-alpine
mvn -B -DskipTests clean package  # ejecutamos el comando maven que contruira el .jar
```

1.A, Lo mismo pero con windows

```r
docker run --rm -v //c/Users/subject51/.m2://root/.m2 -v //e/cursos/0activ/sistema/docker/node/dos/app/://app -w //app maven:3-openjdk-11-slim mvn -B -DskipTests clean package
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 139. Build: Crea un script para automatizar la creación del JAR con Docker

**Objetivo:** El comando de arriba vamos a meterlo dentro de un script para poder
correr comandos en esta imagen solo pasando los parametros de maven.

1, Creamos el Script y lo guardamos en **/build/docker-mvn.sh**

1,1, Sera lo msimo del comando de arriba pero de lugar del comando de maven le pasamo el parametro que llegue.

```bash
!#/bin/bash
echo "###################"
echo "* Building jar* "
echo "###################"

docker run --rm -v /root/.m2:/root/.m2 -v /home/mario/docker/pipeline/my-maven-app:/app -w /app maven:3-alpine "$@"
```

2, Ejecutamos el script para construir el .jar

```r
sudo ./pipeline/build/docker-mvn.sh mvn -B -DskipTests clean package
```

**Resultado:** Debe de ejecutarse el comando maven correctamente y descargar las dependencias solo
la primera vez y al terminar podremos ver  nuestro .jar en la carpeta de **pipeline/my-maven-app/target/**

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 140. Build: Crea un Dockerfile y construye una imagen con el JAR

**Objetivo** Crear una imagen que tomara un .jar que este en la carpta build y
lo desplegara, Que la utilizaremos en X

1, Creamos "dockerfile-java" dentro de build

```r
FROM openjdk:8-jre-alpine

RUN mkdir /app

COPY .*jar /app/app.jar

CMD java -jar /app/app.jar
```

2, construir imagen

```r
/docker/pipeline/build$ sudo docker build -f dockerfile-java -t test .
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 141. Build: Crea un docker-compose para automatizar la construcción de la imagen

1, Creamos nuestro docker-compose pero esste sera especifico para el build asi que
le llamamos **docker-compose-build.yml** dentro de la carpeta de build

```r
version: '3'
services:
  app:
    image: 'app:$BUILD_TAG'
    build:
      context: .
      dockerfile: dockerfile-java
```

> **Error:** The X variable is not set. Defaulting to a blank string invalid reference format
> **Descripcion:** A la hora de construir por docker-compose da error.
> **Solucion:** Crear un archivo llamado .env donde ponemos las variables de entorno.
> **Fuente** [Link](https://stackoverflow.com/questions/48607848/env-file-is-ignored-in-docker-compose-and-cause-the-variable-is-not-set-wa)

2, Creamos archivo de variables ".env", pero recordar que esta variable la tomara del contenedor de Jenkins.

```r
BUILD_TAG=2
```

3, Construimos nuestra imagen pero desde docker-compose

```r
docker-compose -f docker-compose-build.yml build
```

**Resultado** Podemos crear una imagen con composer en el que cada vez que se construlla
se generara una nueva imagen por que toma la variable de jenkins BUILD_TAG que es una variable
que va incrementando jenkins de forma automatica

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 142. Build: Crea un script para automatizar la creación de la imagen con compose

**objetivo** vamo a hacer un script que copie el jar que se genero y lo peque en build y
luego construlla la imagen.

1, en la carpeta build/ creamos un script **build.sh**

```bash
#!/bin/bash

cp -f /home/mario/docker/pipeline/my-maven-app/target/*.jar /home/mario/docker/pipeline/build/

echo "###################- BUILDING IMAGE -####################"

cd /home/mario/docker/pipeline/build/ && docker-compose -f docker-compose-build.yml build --no-cache
```

2, dar permisos de ejecucion ```chmod +x /home/mario/docker/pipeline/build/build.sh``` y Ejecutamos

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 143. Build: Agrega tus scripts a tu Jenkinsfile

- Ya tenemos los script.

  - `build/docker-mvn.sh`, para crear el .jar en la carpeta target del proyecto maven.

  - `build/dockerfile-java`, para definir la imagen con el jar que se copio en build.

  - `build/docker-compose-build.yml`, para definir la creacion la imagen y le pondra un version autoincrementar po una var de jenkins.

  - `build/build.sh`, copiar el .jar de target/ a build/ y luego construir la imagen con docker-compose.

**Objetovo** uniremos nuestros scripts y definiciones en nuestro **jenkinsfile** para hacer el stage de build

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 146. Test: Aprende a realizar tests desde Docker con Maven

**Objetovo** usaremos el mismo script de docker-mvn para correr nuestras pruebas.

```r
pipeline {
  agent any
  stages {
    stage('Build') {
        steps {
            sh '''
          # GENERAMOS EL JAR
              sudo ~/docker/pipeline/build/docker-mvn.sh mvn -B -DskipTests clean package

          # GENERAMOS LA IMAGEN CON EL JAR
              sudo ~/docker/pipeline/build/build.sh
            '''
        }
    }
    stage('Test') {
      steps {
        sudo ~/docker/pipeline/build/docker-mvn.sh mvn test
      }
    }
    stage('Deploy') {  steps {  } }
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Amazon AWS: Crea una máquina remota para pruebas y conectarnos

<!-- Crear una maquina en aws gratis, Crear un servidor en aws, Crear servidor en AWS, Crear VPS AWS, Crear un host en AWS, Crear host en AWS, Crear host aws, crear instancia aws. crear instancias aws -->
<!-- conectarse a intancia aws, conectar a instancia aws, entrar a instancia aws, hacer conexion a instancia aws, conectarse a host aws, conectar a host aws, entrar a host aws, hacer conexion a host aws, conectarse a maquina aws, conectar a maquina aws, entrar a maquina aws, hacer conexion a maquina aws -->

- Fuente: [Link](https://www.youtube.com/watch?v=3ILMaRB-QT0)

- Consola web de amazon: [Link](https://us-east-2.console.aws.amazon.com/)

1, Generar llave publica/privada.

- 1.1. En el menu izq de web-ui de aws entramos a "Key Pairs".

- 1.2. click en **Create key pair** y le damos nombre.

- 1.3. Se nos descargara un archivo .pem (clave privada) la guardamos en un lugar seguro.

- 1.4. Y cuando arranquemos una instancia (mas adelante lo hacemos) nos preguntara que clave
  usaremos, y le indicamos el nombre de la que acabamos de descargar.

2, Convertimos la llave .pem a una que reconosca putty (Solo para windows).

- 2.1. Entramos a `https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html`

- 2.2. descargamos puttygen para convertir nuestra llave .pem a una que reconosca putty

- 2.3. abrimos puttygen y (load) cargamos el .pem que descargamos.

- 2.4. Le damos sobre "save private key" y nos guardara un archivo .ppk  

3, Crear instancia EC2.

- 3.1. En la barra superior le damos a Services -> bajo Compute -> seleccionamos EC2

![15_crear_host_aws_ec2](./img/15_crear_host_aws_ec2.PNG)

- 3.2. Le damos sobre "Launch Instance".  

- 3.3. Seleccionamos el SO "Ubuntu Server 18.04 LTS (HVM), SSD Volume Type".

- 3.4. escojemos la version free y damos Next.

- 3.5. Dejamos la configuracion por defecto.

- 3.6. En "Add Storage" podemos darle un meximo de 30gb.

- 3.7. en "5. Add Tags" le damos un tag "name: ubuntu-docker" no es obligatorio.

- 3.8. En "6. Configure Security Group" tiene solo que se pueda ssh, nosotros le agregamos
  el http y https, y le damos en "Next" o "Review and launch".

- 3.9. Revisamo que este correcto y le damos en lanzar.

- 3.10. **Nos pedira crear o seleccionar una llave ssh**, le damos crear una nueva, le damos nombre y descargmos.

- 3.11. Le damos iniciar instancia, debe de terminar con un "Your instances are now launching" y le
  damos "view instances" el cual debe de estar como running.

4, Conectarse via putty: Al parecer no es recomendable usar el ssh de windows, y la [doc oficial](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html)
solo dice como hacerlo con putty.
  3.1. Descargar [putty](https://www.putty.org/)

  <!-- usuario de aws, usuario aws usuario, accesos aws accesos, credenciales aws credenciales-->
- 4.2. Por defecto el nombre de usuario es el mismo que su S.O. en mi caso el nombre de usuario
  es "ubuntu", por que escoji ubuntu como SO.

- 4.3. Como ruta ponemos la "Public DNS (IPv4)" que nos da aws en la descripcion de la instancia

- 4.4. En putty vamos a panel izq ssh -> auth -> browse y seleccionamos el arcchivo .ppk

**Resultado:** Deberiamos de poder entrar a nuestra instancia de aws sin problemas y sin
pedirnos contraseña.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### Amazon AWS: Configurar domino en Amazon (AWS)

<!-- registrar dominio registrar dns en AWS -->

- **Fuente:** [Youtube](https://www.youtube.com/watch?v=HwZ3wNaM69s)
- **Fuente-oficial:** [Doc-amazon](https://aws.amazon.com/es/getting-started/tutorials/get-a-domain/)

1, Vamos a [Link](https://console.aws.amazon.com/route53/home?region=us-east-2#DomainListing) -> "Register Domain"

2, Buscamos un dominio y lo creamos, registrarlo tiene un costo de $250 por año, el cual nos enviaran
correo indicando que el dominio ya esta registrado.

3, Vamos a [Link](https://console.aws.amazon.com/route53/home?region=us-east-2#hosted-zones:) -> create hosted zone y
le damos el nombre del dominio que creamos (en mi caso mario51.com)

4, Click sobre el hosted zone que acabamos de crear y damos sobre "create recod set" le asignamos -> name: "www",
type: "Ipv4", value:"ip piblica de instancia" -> create

5, Para que entre aunque el usuario no le ponga el www, "create recod set" -> type: "IPv4" alias: "yes" -> sleccionamos
el que creamos apenas en mo caso "www.mario51.com" -> create

> **Error:** No se estaban resolviendo el dominio, no se resuelve dominio no funciona.  
> **Descripcion:** Entraba por medio de IP estatica o una elastica de aws que le asigne a la instancia y
> si entregaba contenido, pero a la hora de querer entrar por nombre de dominio daba error de dominio
> no encontrado.  
> **Causa:** Tenia distinta la configutrancion entre la configuracion de domino, y la hosted zone.  
> **Fuente:** [Link](https://forums.aws.amazon.com/thread.jspa?threadID=175608)  
> **Solucion:** Las dos conf de abajo deben estar iguales.

![Error_conf](./img/16_error_conf_dns_aws.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 149. Push: Aprende a crear un registry en Docker (Sin autenticación)

El registry en una maquina en la cual podremos guardar las imagenes que generemos.

Este contenedor guardara nuestras imagenes en **/var/lib/registry/** lo cual
generaemos un volumen aqui para no perderlas.

por defecto usa el puerto 5000 pero cualquier otro no ahi pex.

```r
docker run -d -p 5000:5000 -v ~/docker/pipeline/registry-images:/var/lib/registry/ --name registry registry:2
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 150. Push: Aprende a subir y bajar imágenes a un registry en Docker

1, Descargamos la imagen de hello-word del docker hub oficial, ya que es la mas liviana

```r
docker pull hello-world
```

2, **Subir imagen a local**

  2.1. Tageamos la imagen (como un clon de esta) que usaremos para subiremos
  donde definimos a-donde-se-subira/nombre, por defecto se sube al oficial de
  docker.

```r
docker tag hello-world localhost:5000/hello-world-ejemplo
```

  2.2. Subimos la imagen a local

```r
sudo docker push localhost:5000/hello-world-ejemplo
```

  2.3. Comprobamos, recordemos que hicimos un volumen
<!-- ver imagenes en registry, ver imagenes guardadas en registry,  ver imagenes subidas en registry -->
```r
ll ~/docker/pipeline/registry-images/docker/registry/v2/repositories/
```

**Resultado:**  deberiamos de ver en el directorio "hello-world-ejemplo/" que
es nuestra imagen que tageamos, dento de nuestro contenedor en el docker hub local.

1. Configuramos para **subir imagen a host remoto**.
  3.2. En el host desde el cual vamos a subir en mi caso el local cambiamos la configuracion del push

```r
sudo nano /lib/systemd/system/docker.service
```

  3.2. Buscamos la siguiente linea y le agregamos nuestro host remoto, y Guardamos

```r
# ORIGINAL ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock --insecure-registry ec2-18-191-218-166.us-east-2.compute.amazonaws.com:5043
```

  3.3. preparar cambios y reiniciar.

```r
sudo systemctl daemon-reload
sudo systemctl restart docker
```

<!-- crear contenedor registry sin autenticación, crear registry, crear servicio registry iniciar contenedor registry sin autenticación, iniciar registry, iniciar servicio registry sin autenticación-->

4, Levantamos servicio de registry a nuestro host remoto

  4.1. Entramos a nuestro host reomoto e iniciar un contenedor de registry.

```r
docker run -d -p 5043:5000  -v ~/docker/pipeline/registry-images:/var/lib/registry/ --name registry registry:2
```

  4.1. Agregamos reglas para acceso a registry en aws
![15_host_aws_reglas_seguridad_para_docker_registry](./img/15_host_aws_reglas_seguridad_para_docker_registry.PNG)

  4.3. Comprobamos que podemos hacer un telnet de nuestro host local a host remoto en el puerto 5043

```r
> telnet ec2-18-191-218-166.us-east-2.compute.amazonaws.com 5043
< Trying 18.191.218.166...
< Connected to ec2-18-191-218-166.us-east-2.compute.amazonaws.com.
```

1. Subimos imagen a nuestro host remoto
  5.1. Agregamos tag para indicar que subiremos la imagen a este registry

```r
sudo docker tag hello-world-ejemplo  ec2-18-191-218-166.us-east-2.compute.amazonaws.com:5043/hello-world-ejemplo
```

  5.2. Subimos la imagen

```r
docker push ec2-18-191-218-166.us-east-2.compute.amazonaws.com:5043/hello-world-ejemplo
```

2.3. Comprobamos, recordemos que hicimos un volumen
<!-- ver imagenes en registry, ver imagenes guardadas en registry,  ver imagenes subidas en registry -->

```r
ll ~/docker/pipeline/registry-images/docker/registry/v2/repositories/
```

**Resultado:** Hemos creado en un contenedor con el servicio de registry de docker
y subido una imagen de nuestro local a un host remoto de aws.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 151. Push: Crea certificados SSL para tu registry con autenticación

<!-- crear certificado ssl, generar certificado, -->

Fuente: [Tutorial](https://www.learndevopsnow.tech/docker-registry-con-basic-auth/)

**Cuidado:** 1) no respaldar toda la carpeta /etc/ ya que me causo rollos, por algun motivo
no deja crear el contenedor por que no existia /etc/letsencrypt pero al final si me dejo.

por seguridad vamos a agregar un login a nuestro registry remoto, lo cual vamos a necesitar
de un certificado ssl.

1. Creamos archivo nginx

```conf
events {
  worker_connections  512;  ## Default: 1024
}
http {
  server {
    listen       80;
    # Reemplaza según tus registros DNS
    server_name  www.mario51.com mario51.com;

    location ^~ /.well-known/acme-challenge/ {
       default_type "text/plain";
       root /mnt;
    }
  }
}
```

2, Creamos contenedor nginx

```r
docker run --rm -v /home/ubuntu/docker/certs/conf/nginx.conf:/etc/nginx/nginx.conf:ro -v /home/ubuntu/docker/certs/letsencrypt/:/etc/letsencrypt -p 80:80 -ti nginx:alpine sh
```

3, Iniciar nginx, Iniciar servicio nginx ``` nginx ```

4, Agregamos certbot ```apk add --update certbot```

5, Creamos sertificado y nos salimos del contenedor el cual se eliminara el contenedor al
salirnos por la bandera "--rm"

```r
certbot certonly --email mario51marquez@gmail.com --agree-tos --non-interactive --webroot -w "/mnt" -d www.mario51.com -d mario51.com
```

6, comprobamos que tengamos los certificados en nuestro docker-host ``` sudo ls -l ~/docker/certs/letsencrypt/live/ ```
el cual debe de tener el mismo nombre que nuestro dominio.

**Resultado:** Tenemos un certificado valido creado gratuito y valido por 3 meses el cual deberemos
de renovar y siempre sera gratuito.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 152.153 Push: Aprende a crear un registry en Docker (Con autenticación) . Aprende a subir y bajar imágenes a un registry con autenticación

**Fuente:** La misma que el anteiror empesando de "Creando el Registry".

<!-- crear dos carpetas al mismo tiempo, crear 2 carpetas al mismo tiempo, crear dos directorios al mismo tiempo, crear 2 directorios al mismo tiempo, crear dos carpetas con un comando, crear 2 carpetas con un comando, crear dos directorios con un comando, crear 2 directorios con un comando, crear dos carpetas con un solo comando, crear 2 carpetas con un solo comando, crear dos directorios con un solo comando, crear 2 directorios con un solo comando -->
1, Creamos directorio ``` mkdir ~/docker/certs/registry/{auth,data} -p && cd ~/docker/certs/registry/ ```

2, Copiamos nuestro certificado a las carpetas que apenas creamos

```bash
cp ~/docker/certs/letsencrypt/live/www.mario51.com/fullchain.pem ~/docker/certs/registry/auth
cp ~/docker/certs/letsencrypt/live/www.mario51.com/privkey.pem ~/docker/certs/registry/auth
```

3, Creamos un usuario de prueba, el cual guardaremos en un archivo, en forma de "user1:xxx.xxx.xxx."

```shell
docker run --entrypoint htpasswd registry:2 -Bbn user1 pass1 > ~/docker/certs/registry/auth/htpasswd
```

**Error:** Permisos denegados, requiere permisos por todos lados y no
deja ejecutar el comando, **Solucion** se guarda el comando en en script
y se ejecuta con sudo

4, Creamos la configuracion - en con - ``` sudo nano ~/docker/certs/registry/auth/nginx.conf ```

```shell
events {
    worker_connections  1024;
}

http {

  upstream docker-registry {
    server registry:5000;
  }

  ## Set a variable to help us decide if we need to add the
  ## 'Docker-Distribution-Api-Version' header.
  ## The registry always sets this header.
  ## In the case of nginx performing auth, the header is unset
  ## since nginx is auth-ing before proxying.
  map $upstream_http_docker_distribution_api_version $docker_distribution_api_version {
    '' 'registry/2.0';
  }

  server {
    listen 443 ssl;
    server_name mario51.com;

    # SSL
    ssl_certificate /etc/nginx/conf.d/fullchain.pem;
    ssl_certificate_key /etc/nginx/conf.d/privkey.pem;

    # Recommendations from https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html
    ssl_protocols TLSv1.1 TLSv1.2;
    ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:SSL:10m;

    # disable any limits to avoid HTTP 413 for large image uploads
    client_max_body_size 0;

    # required to avoid HTTP 411: see Issue #1486 (https://github.com/moby/moby/issues/1486)
    chunked_transfer_encoding on;

    location /v2/ {
      # Do not allow connections from docker 1.5 and earlier
      # docker pre-1.6.0 did not properly set the user agent on ping, catch "Go *" user agents
      if ($http_user_agent ~ "^(docker\/1\.(3|4|5(?!\.[0-9]-dev))|Go ).*$" ) {
        return 404;
      }

      # To add basic authentication to v2 use auth_basic setting.
      auth_basic "Registry realm";
      auth_basic_user_file /etc/nginx/conf.d/htpasswd;

      ## If $docker_distribution_api_version is empty, the header is not added.
      ## See the map directive above where this variable is defined.
      add_header 'Docker-Distribution-Api-Version' $docker_distribution_api_version always;

      proxy_pass                          http://docker-registry;
      proxy_set_header  Host              $http_host;   # required for docker client's sake
      proxy_set_header  X-Real-IP         $remote_addr; # pass on real client's IP
      proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
      proxy_set_header  X-Forwarded-Proto $scheme;
      proxy_read_timeout                  900;
    }
  }
}
```

<!-- crear contenedor registry CON autenticación, crear registry, crear servicio registry iniciar contenedor registry con autenticación, iniciar registry, iniciar servicio registry con autenticación inicar registry en aws, crear registry en AWS, inicar servicio registry en aws, crear servicio registry en aws, inicar contenedor registry en aws, crear contenedor registry en aws -->
5, Creamos **docker-compose.yml** para definir nuestro servicios ``` nano ~/docker/certs/registry/docker-compose.yml ```

```dockerfile
version: '3'
services:
  nginx:
    image: "nginx:alpine"
    ports:
      - 5043:443 # Usa el puerto que prefieras para exponer tu servicio
    networks:
      - net
    volumes:
      - ./auth:/etc/nginx/conf.d # Mapeamos los certificados
      - ./auth/nginx.conf:/etc/nginx/nginx.conf:ro # Mapeamos la config de nginx

  registry:
    image: registry:2
    ports:
      - 127.0.0.1:5000:5000
    volumes:
      - ./data:/var/lib/registry # Persistimos la data del registry
    networks:
      - net
networks:
  net:
```

**Nota** Fijarse que si tenga los archivos como se señalan ahi

6, Levantamos `cd ~/docker/certs/registry/ && sudo docker-compose up -d`

<!-- entrar a registrer aws, login registrer aws, entrar a registrer de aws, loguearse en registr de aws, login en registrer -->
7, Probamos

  7.1. Vamos a nuestra maquina local
  7.2. Tageamos la imagen para subir a nuestro remoto `sudo docker tag hello-world-ejemplo:latest mario51.com:5043/hello-world-ejemplo:latest`
  7.3. subimos la imagen que teniamos de prueba `sudo docker push mario51.com:5043/hello-world-ejemplo:latest`
  7.4. **Debe dar ERROR** no basic auth credential, significa que la seg esta funcionando.
  7.5. Nos logeamos `docker login -u user1 -p pass1 mario51.com:5043` debe de darnos un **Login Succeeded**.
  7.6. Repetimos la subida (el push de apenas pusimos).

**RESULTADO:** Creamos un registry con https con usuario y contraseña para poder subir, y desde
otro host nos logueamos y subimos la imagen.

![17_subir_a_registry_en_aws_con_https.PNG](./img/17_subir_a_registry_en_aws_con_https.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 154.155. Push: Automatiza el proceso de subir y bajar imágenes a un registry - Valida el funcionamiento de tu script de push (Sin Auth)

1, Creamos el script encargado de subir la imagen generada a nustro registry

<!-- ver si una variable existe, ver si una variable esta asignada, ver si una variable esta vacias, condicional existe variable -->
<!-- checar si una variable existe, checar si una variable esta asignada, checar si una variable esta vacias, -->
<!-- estructura script estructura, script ejemplo script,  estructura de script, ejemplo de script -->

```bash
#!/bin/bash

echo "####################- subiendo imagen a registry -########################"

REGISTRY="localhost:5000"
IMAGE="app"

if [ -z "$BUILD_TAG" ]; then
  echo -e "\e[33mTesting... set BUILD_TAG = 2"
  BUILD_TAG="2"
fi

echo "$IMAGE":"$BUILD_TAG" "$REGISTRY"/"$IMAGE":"$BUILD_TAG"

docker tag "$IMAGE":"$BUILD_TAG" "$REGISTRY"/"$IMAGE":"$BUILD_TAG" && \
docker push "$REGISTRY"/"$IMAGE":"$BUILD_TAG"
```

2, Le damos permisos de ejecucion ```chmod +x pipeline/push/push.sh```.

3, Probamos ```sudo sh pipeline/push/push.sh```.

**Resultado:** Al ejecutar el script se subira nuestra imagen recien creada a
nuestro registry, el cual usaremos para nuestro pipeline.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 156. Push: Agrega el script de push a tu Jenkinsfile

Agregamos la ejecucion de nuestro script de subir al registry host.

```dockerfile
pipeline {
  agent any
  stages {
    stage('Build') {
        steps {
            sh '''
# GENERAMOS EL JAR
              sudo ~/docker/pipeline/build/docker-mvn.sh mvn -B -DskipTests clean package

# GENERAMOS LA IMAGEN CON EL JAR
              sudo ~/docker/pipeline/build/build.sh
            '''
        }
    }

    stage('Test') {
      steps {
        sh '''
          sudo ~/docker/pipeline/build/docker-mvn.sh mvn test
        '''
      }
    }

    stage('Push') {
      steps {
        sh '''
          sudo sh ~/docker/pipeline/push/push.sh
        '''
      }
    }

    stage('Deploy') {  steps {  } }
  }
}

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 154.155. Push: Automatiza el proceso de subir y bajar imágenes a un registry - Valida el funcionamiento de tu script de push (Con Auth)

1, En host-local creamos el script (~/docker/pipeline/push/push.sh) encargado de subir la imagen generada a nustro registry

<!-- ver si una variable existe, ver si una variable esta asignada, ver si una variable esta vacias, condicional existe variable -->
<!-- checar si una variable existe, checar si una variable esta asignada, checar si una variable esta vacias, -->
<!-- estructura script estructura, script ejemplo script,  estructura de script, ejemplo de script -->

```bash
#!/bin/bash

echo "####################- subiendo imagen a registry -########################"

USER=user1
PASSWORD=$AWS_REGISTRY_PASS

REGISTRY="mario51.com:5043"
IMAGE="app"

# AGREGAMOS VARIBLE DE TEST
if [ -z "$BUILD_TAG" ]; then
  echo -e "\e[33mTesting... set BUILD_TAG = 2"
  BUILD_TAG="2"
fi

if [ -z "$PASSWORD" ]; then
  echo -e "\e[33mTesting... set PASSWORD = pass1"
  PASSWORD="pass1"
fi

echo -e "\e[34m$IMAGE":"$BUILD_TAG" "$REGISTRY"/"$IMAGE":"$BUILD_TAG"
echo -e "\e[97m"

docker login -u $USER -p $PASSWORD $REGISTRY && \
  docker tag "$IMAGE":"$BUILD_TAG" "$REGISTRY"/"$IMAGE":"$BUILD_TAG" && \
  docker push "$REGISTRY"/"$IMAGE":"$BUILD_TAG"
```

2, Le damos permisos de ejecucion ```chmod +x ~/docker/pipeline/push/push.sh```.

3, Probamos ``` sudo sh ~/docker/pipeline/push/push.sh ```.

**Resultado:** Al ejecutar ~/docker/pipeline/push/push.sh se subira nuestra imagen recien creada a
nuestro registry, la cual sera la que desplegemos, en este caso app es lo que construimos con maven.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 157. Deploy: Transfiere las variables de entorno a la máquina remota

**Objetivo:** Le enviaremos las variables para que el host-remoto sepa cual es la
imagen actual que jenkins construyo, y con esta info sepa cual imagen desplegar.

1, Creamos un script (~/docker/pipeline/push/send_info_file.sh) que pondra el nombre de la imagen (siempre sera app),
el build_tag y la contraseña, pero en mi caso no voy a usar la contraseña.

```bash
#!/bin/bash
if [ -z "$BUILD_TAG" ]; then
  echo "Testing... set BUILD_TAG = 3"
  BUILD_TAG="3"
fi

if [ -z "$PASS" ]; then
  echo "Testing... set PASS = pass1"
  PASS="pass1"
fi

echo app > /tmp/.auth
echo $BUILD_TAG >> /tmp/.auth
echo $PASS >> /tmp/.auth

# PARA SERVIDOR REMOTO
scp -i /home/mario/aws/key-ubuntu-docker.pem /tmp/.auth ubuntu@mario51.com:/tmp/.auth

# PARA CONTENEDOR LOCAL
# sudo docker cp /tmp/.auth registry:/tmp/.auth
```

**Error:** "It is required that your private key files are NOT accessible by others", "Load key "xxx.pem": bad permissions"
**Descripcion:** A la hora de querer un archivo con el comando scp muestra este error.
**Solucion:** Cambiar los permisos de la llave por: ```chmod 400 mykey.pem```

2, Comprobamos que este nuestro archivo (en este caso dentro de contenedor) en nuestro registry host, en mi caso local.

```bash
docker exec registry sh -c "cat /tmp/.auth"
```

**Resultado:** Le mandamos al host remoto el nombre de la imagen que en este sera siempre el mismo, el
BUILD_TAG que es el numero de compoilacion actual de jenkins, y la contraseña

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 158. Deploy: Despliega tu aplicación manualmente

**Objetivo:** En host-remoto vamos a desplegar la imagen que recien subimos usando
la informacion de .auth que igual apenas subimos, para saber que imagen desplegar.

1. crear variables de entorno a partir de archivo .auth

```bash
# salida "app"
export IMAGE=$(sed -n '1p' /tmp/.auth)

# salida "2"
export TAG=$(sed -n '2p' /tmp/.auth)

export REGISTRY=mario51.com:5043
```

2, En host-remoto creamos el yml

```dockerfile
version: '3'
services:
  app-prod:
    image: '$REGISTRY/$IMAGE:$TAG'
    container_name: app-prod-2
```

4, hacemos login en registrer Construimos ``` cd ~/docker/pipeline/deploy/ && docker-compose up -d ```

**Resultado:** hicimos deploy la imagen que subimos y sabemos cual fue la ultima
que se subio por medio del archivo .auth, **Falta** hacerlo de forma auto.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 159. Deploy: Crea un script para automatizar el despliegue de tu aplicación

1, En local-host creamos "~/docker/pipeline/deploy/publish.sh" que subiremos a nuestro
host remoto para ejecutarlo ahi.

```bash
#!/bin/bash

export REGISTRY=mario51.com:5043
export IMAGE=$(sed -n '1p' /tmp/.auth)
export TAG=$(sed -n '2p' /tmp/.auth)
export PASS=$(sed -n '3p' /tmp/.auth)

docker login -u user1 -p $PASS $REGISTRY && \
cd ~/docker/pipeline/deploy/ && docker-compose up -d
```

2, Agregar permisos de ejecucion ``` chmod +x ~/docker/pipeline/deploy/publish.sh ```

3, Agregamos en deploy.sh la linea para subirlo a host-remoto.

```bash
#!/bin/bash

scp -i /home/mario/aws/key-ubuntu-docker.pem /tmp/.auth ubuntu@mario51.com:/tmp/.auth

scp -i /home/mario/aws/key-ubuntu-docker.pem ~/docker/pipeline/deploy/publish.sh ubuntu@mario51.com:/tmp/publish.sh

```

Nota: Debe de estar arriba de jenkinsfile ya que desde jenkins ejecutaremos el script para subirlo.

4, Ejecutamos ``` sh ~/docker/pipeline/deploy/deploy.sh ``` el cual debe de subirse los archivos .auth y publish.sh

**Resultado** Creamos script para hacer deploy de la imagen en turno y
agregamos en deploy.sh que se suba este a remote-host luego ejecutarlo ahi.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 160. Deploy: Ejecuta el script de publish en la máquina remota

Arriba subimos publish.sh que hara deploy de nuestra imagen en turno

1, En remote-host nos aseguramos que no corra ningun contenedor de "app"

2, En local-host Modificamos deploy.sh para mandar a ejecutar publish.sh que lo hacemos con ssh

```bash
#!/bin/bash

scp -i /home/mario/aws/key-ubuntu-docker.pem /tmp/.auth ubuntu@mario51.com:/tmp/.auth

scp -i /home/mario/aws/key-ubuntu-docker.pem ~/docker/pipeline/deploy/publish.sh ubuntu@mario51.com:/tmp/publish.sh

ssh -i /home/mario/aws/key-ubuntu-docker.pem ubuntu@mario51.com sh /tmp/publish.sh

```

3, Ejecutamos en local-host deploy.sh

**Resultado** debe de desplegarse la imagen en turno en nustro remote-host

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 161. Deploy: Agrega el script de deploy al Jenkinsfile

1. En local-host modificamos nuestro jenkinsfile

```dockerfile
pipeline {
  agent any
  stages {
    stage('Build') {
      steps {
        sh '''
          sudo sh /home/mario/docker/pipeline/jenkins/build/docker-mvn.sh mvn -B -DskipTests clean package
          sudo sh /home/mario/docker/pipeline/jenkins/build/build.sh
        '''
      }
    }

    stage('Test') {
      steps {
        sh '''
          sudo sh /home/mario/docker/pipeline/jenkins/build/docker-mvn.sh mvn test
        '''
      }
    }

    stage('Push') {
      steps {
        sh '''
          sudo sh /home/mario/docker/pipeline/jenkins/push/push.sh
        '''
      }
    }

    stage('Deploy') {
      steps {
        sh '''
          sudo sh /home/mario/docker/pipeline/jenkins/deploy/deploy.sh
        '''
      }
    }
  }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 162. Crea un repo en GIT para el nuevo proyecto

1, Entrar a gitlab, login gitlab "root:12345678"

2, crear repositorio en gitlab en mi caso con nombre "pipeline-maven"
2.1. Agregar usuario gitlab, en mi caso mario como maintainer

3, En nuestro codigo que venimos trabajando agregar url de gitlab a nuestro proyecto en
la carpeta.

**Recordatorio:** Todos los scripts dentro del jenkinsfile se van a ajecutar dentro
del copntenedor de jenkins asi que debemos de asegurarnos que tengamos lo necesario en
dicho contenedor.

4, Copiamos la llave para entrar a nuestro host remoto, dentro de nuestro conenedor de jenkins
4.1. En ~/docker/pipeline/jenkins/deploy/deploy.sh cambiamos la ruta del .pem por el que acabamos de crear.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 163. Crea el pipeline en Jenkins

**Objetivo**: Ya creamos los scripts y el jenkinsfile ahora vamoa a crear el job que ejecutara
este jenkinsfile

1, Vamos a jenkins, new job -> nombre: "pipeline-maven" tipe: "pipeline" -> Ok.

- 1.1. En la parte de pipeline le ponemos en Definition: "Pipeline script from SCM", SCM: Git.

- 1.2. En "Script Path": le dejamos "Jenkinsfile" ya que en este caso lo tenemos en la rais del repo -> Crear.

2, Ejecutamos (Contruimos) la tarea

**Resultado** Va a fallar pero lo importante en notar es que se va a crear una carpeta en *~/docker/jenkins/jenkins_home/workspace/*
por que ahi definimos el volumen.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 165. Agrega la contraseña del registry al Jenkinsfile

1. Vamos a jenkins UI -> Credencials -> jenkins (abajo de donde cide "Stores scoped to Jenkins") -> seguimos
-> Agregar credencial y le amos el mismo nombre que en el script "AWS_REGISTRY_PASS"

2. Agregamos la variable en nuestro *enviroment*

```dockerfile
pipeline {
  agent any
  environment {
    AWS_REGISTRY_PASS = credentials('AWS_REGISTRY_PASS')
  }
  stages { ... }
}
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 166. Verifica el host ssh en el contenedor de Jenkins

Recordemos que tenemos un comando ssh en uno de nuestros scripts el cual ejecutara nuestros
el cual **pedira confirmacion la primera** vez que lo hagamos el cual parara el flujo
automotico, podriamos ponerle una bandera para que siempre ignore esto, pero no es una buena
practica de seguridad, asi que entraremos al contenedor y ejecutamos el comando.

1. Entrar al contenedor de jenkins ``` sudo docker exec -ti jenkins-ansible bash ```

2. Ejecutamos un comado quen usa el ssh en este caso ``` ssh -i /opt/key-ubuntu-docker.pem ubuntu@mario51.com sh /tmp/publish.sh ```
  2.1. Nos preguntara que si queremos agregar el host a la lista de confianza, le damos que si.

**Resultado:** Tendremos el host agregado a la lista de confianza y ya no nos lo pedira
cuando lo ejecutemos desde jenkins.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 168. Agrega post actions a tu Jenkinsfile

**Objetivo:** Endicar en jenkinsfile que.

  1. Al terminar de construir se guarde el .jar en jenkis para disponerlo en la UI de jenkins.
  2. Al terminar de ejecutar los test guardaremos siempre el informe de estas.

```dockerfile
pipeline {
  agent any
  environment {
    AWS_REGISTRY_PASS = credentials('AWS_REGISTRY_PASS')
  }
  stages {
    stage('Build') {
  
      post {
        success {
          archiveArtifacts artifacts: 'my-maven-app/target/*.jar', fingerprint: true
        }
      }
    }
    stage('Test') {
      steps { ... }
      post {
        always {
          junit 'my-maven-app/target/surefire-reports/*.xml'
        }
      }
    }
    stage('Push') {...}
    stage('Deploy') {...}
  }
}
```

**Resultado:** Al ejecutar el pipeline desde jenkins debemos de tener los
resultados de los tests y el .jar generado disponibles en la ui de jenkins,
todo haber funcionado bien (el despliege en prod).

![18_jenkinsfile_guardar_resultado_test_y_jar.PNG](./img/18_jenkinsfile_guardar_resultado_test_y_jar.PNG)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 170. Agrega un Git Hook al repo para gatillar el Job automáticamente

**Objetivo** Hasta ahora ya tenemos ejecutando correctamente nuestro pipeline, pero
lo ejecutamos manualmente desde la UI de jenkins, y la idea es que este ocurra cada
ver que hacemos un commit, esto lo hacemos a traves de un hook.

1, Entramos a nuestro jenkins-host y vamos a la ruta en mi caso "/var/opt/gitlab/git-data/repositories/root/maven-pipeline.git"

2, agregamos la carpeta custom_hooks/ y dentro el post-receive (igual como se hace en "agregar hooks a gitlab") y
le ponemos lo siguiente el cual es lo mismo que "agregar hooks a gitlab" solo cambia la ruta.

```bash
#!/bin/bash
# Get branch name from ref head

if ! [ -t 0 ]; then
  read -a ref
fi
IFS='/' read -ra REF <<< "${ref[2]}"
branch="${REF[2]}"

if [ $branch == "master" ]; then
  curl -u "mario:mario" -X POST http://192.168.1.73:8080/job/pipeline-maven/build
  if [ $? -eq 0 ] ; then
    echo "*** Ok"
  else
    echo "*** Error"
  fi
fi
```

3, Le agregamos permisos de ejecucion y el el usuario como git ```chown git:git -R custom_hooks```.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% divicion titulo

-->

### 171. Inicia el proceso de CI/CD solo con un commit a GIT

Anteriormente creamos un custom_hook que a la hora de recibir un push a master
este ejecutara el nuestra tarea de jenkins que hara todo nuestro pipeline.

1, Hacemos un cambio en nuestro codigo, en mi caso el codigo solo manda un "hola mundo"
<!-- buscar archivos con grep buscar archivos, filtrar archivos con grep filtrar archivos, listrar archivos con grep listar archivos, buscar archivos con x palabra con grep buscar archivos, filtrar archivos con x palabra con grep filtrar archivos, listrar archivos con x palabra con grep listar archivos, -->

```bash
grep -R World
```

![19_grep_para_buscar_archivos.PNG](./img/19_grep_para_buscar_archivos.PNG)

2, Hacemos push y deberia de desplegarse nuestra aplicacion.

## RESUMEN: Crear pipeline DevOps

- Resumen
  - En el Job tenemos que al iniciar se descarge el codigo en /pipeline/my-maven-app
  - Se genera una imagen maven para generar el jar usando un volumen en la direccion del codigo
    y al terminar de generar se guarda nuestro .jar en /pipeline/my-maven-app/target/
  - Luego se crea una imagen con el jar recien y como tag le ponemos el numero del job que tiene jenkins.
  - Se sube la imagen a un registry de docker usando usr>pass el cual necesita https.
  - Se sube a un host-remoto un archivo con el nombre de la imagen y dl tag para que sepa cual desplegar.
  - A partir del archivo subido se crear variables de entorno para que las use un .yml use la imagen en turno a ser desplegada.
  - se despliega la imagen en PROD.

PENDIENTE

## FIN Curso jenkins de principiante a experto

El fin

<!-- 

-->
  
## Cosas que me voy encontrando

### Errores

----

> **Error**: Invalid agent type "docker" specified
> **Descripcion:** Al querer ejecutar un job de tipo pipeline con el
> siguiente codigo me muestra este error.

```jenkinsfile
pipeline {
  agent {
    docker { 
      image 'node:lts'
    }
  }
...
```

> **Causa:** No tengo instalados los plugins para usar docker dentro
> de jenkins
> **Solucion**: Instalar los plugins `Docker plugin` y `Docker Pipeline`

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

> **Error;** docker: Permission denied
> **Fuente:** [Link](https://stackoverflow.com/questions/44444099/how-to-solve-docker-permission-error-when-trigger-by-jenkins)
> **Descripcion:** Al querer ejecutar un job de tipo pipeline con el
> siguiente codigo me muestra este error.

```jenkinsfile
pipeline {
  agent {
    docker {
      image 'node:lts'
    }
  }
...
```

> **Causa:** Tenemos una falta de permisos, en el cual los tiene el
> grupo docker, por lo que debemos de agregar a este grupo el usuario
> que esta usando jenkins para ejecutar el job
> **Solucion-1:** Elevar los permisos de jenkins `sudo usermod -aG docker jenkins`
> **Solucion-2:** Al crear el contenedor `docker run -d -u root ... jenkinsci/blueocean`

----

<!-- Agregar jdk a jenkins -->

**Error:** No se puede agregar un nuevo jdk a jenkins
**Solucion:** Checar fuente: [stackoverflow](https://stackoverflow.com/questions/56960855/using-openjdk-12-in-jenkins-docker-version)

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!-- 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Crear contenedor de jenkins con https (Sin reverse proxy)

Fuentes:

- [Setup secured Jenkins master with docker](https://stackoverflow.com/questions/29755014/setup-secured-jenkins-master-with-docker)
- [DockerHub](https://hub.docker.com/_/jenkins/)

**CUIDADO:** Podemos resivir un `ERR_INVALID_HTTP_RESPONSE` si ya hacemos
la peticion a http, pero si camiamos a **https://** podremos ver una
respuesta, aunque podemos resivir un `ERR_CERT_AUTHORITY_INVALID` el
cual podemos solucionarlo pero ese es un tema en [TODO]().

1, Crear certificado: `keytool -genkey -keyalg RSA -alias selfsigned -keystore jenkins_key.jks -storepass <PASS> -keysize 2048`

2, Crear DockerFile que incluye el certificado

```dockerfile
  FROM jenkins/jenkins:jdk11
  USER root
  RUN apt install...
# Agregamos el certificado al contenedor (no es obligatoria dicha ruta)
+ COPY ./certs/jenkins_key.jks /var/lib/jenkins/key/jenkins_key.jks
# Agregamos variables para configurar el https entre ellos el sertificado, su pass, y puerto
+ ENV JENKINS_OPTS --httpPort=-1 --httpsPort=9090 --httpsKeyStore=/var/lib/jenkins/key/jenkins_key.jks --httpsKeyStorePassword=<PASS>
# Exponemos el puerto abierto del https por jenkinsfile
+ EXPOSE 9090
```

3, Creamos el docker-file

```yml
version: "3.3"

networks:
  cdmt_net_prod:
    external: true

services:
  jenkins:
    container_name: cdmt_jenkins
# Usamos el dockerfile que creamos
+   build:
      context: .
      dockerfile: jenkins_with_docker.dockerfile
    privileged: true
    user: root
    restart: on-failure
    ports:
      - "9090:9090"
    volumes:
      - ./jenkins_home:/var/jenkins_home
    networks:
      cdmt_net_prod:
        ipv4_address: 166.10.0.11
```