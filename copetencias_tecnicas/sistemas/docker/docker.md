# Docker

- [Docker](#docker)
  - [Accessos Rapidos](#accessos-rapidos)
  - [PENDIENTE](#pendiente)
  - [Instroduccion](#instroduccion)
    - [14. Imagenes oficiales - I](#14-imagenes-oficiales---i)
  - [Sección 3: Docker Images](#sección-3-docker-images)
    - [16. Creando nuestra primera imagen](#16-creando-nuestra-primera-imagen)
    - [17. Probando la funcionalidad de nuestra primera imagen](#17-probando-la-funcionalidad-de-nuestra-primera-imagen)
    - [18. Introducción al Dockerfle](#18-introducción-al-dockerfle)
    - [19. Dockerfile - From/ Run / Copy / Add](#19-dockerfile---from-run--copy--add)
    - [20. Dockerfile - Env / WorkDir / Expose](#20-dockerfile---env--workdir--expose)
    - [21. Dockerfile - Label / User / Volume](#21-dockerfile---label--user--volume)
    - [22. Dockerfile - CMD / dockerignore](#22-dockerfile---cmd--dockerignore)
    - [24. Dockerfile - Buenas prácticas](#24-dockerfile---buenas-prácticas)
    - [25. Dockerfile - Construyendo una imagen Apache + PHP + TLS/SSL - I](#25-dockerfile---construyendo-una-imagen-apache--php--tlsssl---i)
    - [26. Dockerfile - Construyendo una imagen Apache + PHP + TLS/SSL - II](#26-dockerfile---construyendo-una-imagen-apache--php--tlsssl---ii)
    - [27. Eliminar imágenes](#27-eliminar-imágenes)
    - [28. Cambiar el nombre del Dockerfile](#28-cambiar-el-nombre-del-dockerfile)
    - [29. Dangling images](#29-dangling-images)
    - [30. Crea tu propia imagen con Nginx y PHP-FPM](#30-crea-tu-propia-imagen-con-nginx-y-php-fpm)
    - [31. Tip: Multi-Stage-Build > Aprende a construir apps Java con maven y similares!_](#31-tip-multi-stage-build--aprende-a-construir-apps-java-con-maven-y-similares_)
  - [Sección 4: Docker Containers](#sección-4-docker-containers)
    - [35. Listar / Mapear puertos](#35-listar--mapear-puertos)
    - [36. Renombrar / Iniciar / Reiniciar / Detener](#36-renombrar--iniciar--reiniciar--detener)
    - [37. Variables de entorno](#37-variables-de-entorno)
    - [39. Crear un contenedor MySQL](#39-crear-un-contenedor-mysql)
    - [40. Crear un contenedor Mongo](#40-crear-un-contenedor-mongo)
    - [41, Crear un contenedor Tomcat](#41-crear-un-contenedor-tomcat)
    - [42, Crear un contenedor Postgres](#42-crear-un-contenedor-postgres)
    - [43, Crear un contenedor nginx](#43-crear-un-contenedor-nginx)
    - [43, Crear un contenedor jenkins](#43-crear-un-contenedor-jenkins)
    - [44. Administrar usuarios](#44-administrar-usuarios)
    - [45. Limitar recursos a un contenedor](#45-limitar-recursos-a-un-contenedor)
    - [46. Copiar archivos contenedor](#46-copiar-archivos-contenedor)
    - [47. Convierte un contenedor en una imagen](#47-convierte-un-contenedor-en-una-imagen)
    - [48. Sobreescribe el CMD de una imagen sin un Dockerfile](#48-sobreescribe-el-cmd-de-una-imagen-sin-un-dockerfile)
    - [49. destruir contenedores automáticamente](#49-destruir-contenedores-automáticamente)
    - [50. Cambiar el Document Root de Docker](#50-cambiar-el-document-root-de-docker)
  - [Sección 5: Docker Volumes](#sección-5-docker-volumes)
    - [52. Introduccion - Volumenes](#52-introduccion---volumenes)
    - [54. Volúmenes de host - Caso práctico MySQL](#54-volúmenes-de-host---caso-práctico-mysql)
    - [55. Volúmenes anónimos - Caso práctico MySQL](#55-volúmenes-anónimos---caso-práctico-mysql)
    - [57. Volúmenes nombrados - Caso práctico MySQL](#57-volúmenes-nombrados---caso-práctico-mysql)
    - [57. Volúmenes nombrados: Creacion y uso de respldos con .tar](#57-volúmenes-nombrados-creacion-y-uso-de-respldos-con-tar)
    - [58. Dangling volumes](#58-dangling-volumes)
    - [59. Persistiendo data en MongoDB](#59-persistiendo-data-en-mongodb)
    - [60. Persistiendo data en Jenkins](#60-persistiendo-data-en-jenkins)
    - [61. Persistiendo logs en Nginx](#61-persistiendo-logs-en-nginx)
    - [62. Comparte volúmenes entre uno o más contenedores](#62-comparte-volúmenes-entre-uno-o-más-contenedores)
  - [Sección 6: Docker Network](#sección-6-docker-network)
    - [65. ¿Cuál es la red por defecto?_](#65-cuál-es-la-red-por-defecto_)
    - [66. Crear una red definida por el usuario](#66-crear-una-red-definida-por-el-usuario)
    - [68. Agregar contenedores a una red distinta a la por defecto](#68-agregar-contenedores-a-una-red-distinta-a-la-por-defecto)
    - [69. Conectar contenedores en la misma red](#69-conectar-contenedores-en-la-misma-red)
    - [70. Conectar contenedores en distintas redes](#70-conectar-contenedores-en-distintas-redes)
    - [71. Eliminar redes](#71-eliminar-redes)
    - [71. Asignar IP a un contenedor](#71-asignar-ip-a-un-contenedor)
    - [73. La red de Host](#73-la-red-de-host)
    - [74. La red None](#74-la-red-none)
    - [75. CORS en Docker](#75-cors-en-docker)
  - [Sección 7: Docker Compose](#sección-7-docker-compose)
    - [78. Instalación](#78-instalación)
    - [79. Primeros pasos en Compose](#79-primeros-pasos-en-compose)
    - [80. Variables de entorno en Compose](#80-variables-de-entorno-en-compose)
    - [81. Volúmenes en Compose](#81-volúmenes-en-compose)
    - [82. Redes en Compose](#82-redes-en-compose)
    - [83. Construye imágenes en Compose](#83-construye-imágenes-en-compose)
    - [84. Sobreescribe el CMD de un contenedor con Compose](#84-sobreescribe-el-cmd-de-un-contenedor-con-compose)
    - [85. Limitar recursos en contenedores (Compose v2)](#85-limitar-recursos-en-contenedores-compose-v2)
    - [86. Politica de reinicio de contenedores](#86-politica-de-reinicio-de-contenedores)
    - [87. Personaliza el nombre de tu proyecto en Compos](#87-personaliza-el-nombre-de-tu-proyecto-en-compos)
    - [90. Instalando WordPress + MySQL](#90-instalando-wordpress--mysql)
    - [91. Instalando Drupal + PostgreSQL](#91-instalando-drupal--postgresql)
    - [92. Instalando PrestaShop + MySQL](#92-instalando-prestashop--mysql)
    - [93. Instalando Joomla + MySQL](#93-instalando-joomla--mysql)
    - [94. Instalando Reaction Ecommerce - NodeJS + MongoDB](#94-instalando-reaction-ecommerce---nodejs--mongodb)
    - [95. Instalando Guacamole](#95-instalando-guacamole)
    - [97. Instala Zabbix con Compose](#97-instala-zabbix-con-compose)
  - [Sección 8: Docker Registry](#sección-8-docker-registry)
    - [99. Crea tu propio Docker Registry](#99-crea-tu-propio-docker-registry)
    - [100. Sube tus imágenes!_](#100-sube-tus-imágenes_)
    - [101. Comparte las imágenes en tu red](#101-comparte-las-imágenes-en-tu-red)
  - [CONFIGURACION DE PROXY, CONFIGURAR ACCESSO A LA INTRANET, AGREGAR UN PROXY, USO DE ARCHIVO .PAC](#configuracion-de-proxy-configurar-accesso-a-la-intranet-agregar-un-proxy-uso-de-archivo-pac)
  - [Errores](#errores)
    - [dockercompose TypeError You must specify a directory to build in path](#dockercompose-typeerror-you-must-specify-a-directory-to-build-in-path)
    - [/bin/sh: apt-get: not found](#binsh-apt-get-not-found)
  - [Por el camino](#por-el-camino)
    - [Construir Jar](#construir-jar)
    - [Construir gatsby con docker NO RECOMENDADO](#construir-gatsby-con-docker-no-recomendado)
    - [Construir gatsby con docker RECOMENDADO](#construir-gatsby-con-docker-recomendado)

## Accessos Rapidos

- [docu volumenes](https://docs.docker.com/storage/volumes/)

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## PENDIENTE

1. Un docker-compose.yml con Jenkins con utilerias como respaldar bd a aws etc solo
   construllendolo sin preocuparme por detalles.
2. Un docker-compose.yml que de la posibilidad con jenkins que codigo desplegar, en que
   lugar de aws desplegarlo y consolo dando push a master se haga el despliege.

3. Pasar mi host de gitlab a uno independiente.

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## Instroduccion

<!-- instalar docker -->

En "jenkins.md" Ver "Instalacion de Docker"

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 14. Imagenes oficiales - I

Estos son contenedores oficiales, por ejemplo la imagen ofcial de mongo hub.docker.com/_/mongo
en la cual podremos ver el comando docker para descargarla `docker pull mongo` o si queremos definir
la vercion especifica podemos usar `docker pull mongo:3.6.5-jessie`.

<!-- listar imagenes, ver imagenes, ver lista de imagenes  -->

Ya instalada podemos ver la lista de imagenes con `docker images`

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 3: Docker Images

### 16. Creando nuestra primera imagen

Tendremos escenarios por ejemplo donde querramos tener imagenes con php y
mysql, pero estas no la encontramos de manera oficial.

A. Creamos el script, con el nombre de "dockerfile" asecas,

```dockerfile
# CASARA ERROR, VER EL CORREGIDO EN buscando "[#xdr56tyghbnmk#]" son comillas
# sistema operativo que tengamos
FROM ubuntu

# instalamos herramientas
RUN apt-get update
RUN apt-get install -y apache2 && apt-get clean
```

**IMPORTANTE:** La imagen no se quedara corriendo, esto es por que nos
falta CMD, que se explica mas abajo.

**Nota:** Al parecer es importante el `apt-get update` ya que sin
este da errores.

**Nota:** Igual es importante el ```-y``` para que a todo lo que pregunte
diga que si .

B. Construimos

```r
docker build --tag nombre_imagen_aguardar donde_esta_script
ej.
docker build --tag ubuntu-apache2 .
```

C. Si construimos otra imagen igual este ya no descargara de
nuevo el archivo, usara el cache.

```r
docker build --tag ubuntu-apache2:upsi .
```

<!-- ver historial  -->

D. Ver el historial de docker

```r
sudo docker history -H ubuntu-apache2:latest --no-trunc
```

![1_history](./img/1_history.png)

**Resultado:** Creamos una imagen ubuntu con apache la cual podremos
convertir a contenedor para asi poderla ejecutar mas adelante,.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 17. Probando la funcionalidad de nuestra primera imagen

A, Ver todos los contenedores que tenemos `sudo docker ps -a`

> Ver los contenedores corriendo en docker: `sudo docker ps`  
<!-- mostrar solo ciertos campos, filtrar las columnas a mostrar, filtrar campos, filtrar columnas -->
> Limitar la informacion que nos muestra: `docker ps -a --format "table {{.Names}}\t{{.Ports}}\t{{.Status}}"`
> Ver todos los contenedores tanto corriendo o que se detubieron o murieron: `sudo docker ps -a`  
> Eliminar contenedor de docker *corriendo* se ve el nombre en name del ps: `sudo docker rm -f nombre_imagen`  
> Eliminar contenedor de docker con todo y volumen: `sudo docker rm -fv nombre_imagen`  
> Eliminar todos los contenedoes:  `sudo docker ps -q | xargs docker rm -f`  

B. crear un conetenedor apartir de la imagen que creamos

<!-- crear un conetenedor apartir de dockerfile, crear contenedor con dockerfile, dockerfile crear contenedor, crear contenedor de dockerfile -->

```r
docker run -d ubuntu-apache2
```

C. Vemos las imagenes corriendo en docker y no vemos ninguna esto
significa que se murio nuestro contenedor, y lo podemos ver en el
status que dio un estatus **Exited**.
Esto occurrio por que necesitamos agregar una capa llamada **cmd** que
es un acronimo de command y si vemos el log vemos que usa el bash.

```R
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                        PORTS               NAMES
3dedb6ceb64c        ubuntu-apache2      "/bin/bash"              2 minutes ago       Exited (0) 2 minutes ago                          cool_grothendieck
```

C. Corregimos script agregando el cmd <!-- [#xdr56tyghbnmk#] -->

```dockerfile
# sistema operativo que tengamos
FROM ubuntu

# Para que no interactue con nosostros
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

# instalamos herramientas
RUN apt-get update
RUN apt-get install -y apache2 && apt-get clean

#agregamos cmd
CMD apachectl -DFOREGROUND
```

D. Reconstruimos la imagen y vemos que paso. Y veremos que el cmd lo
esta sustituyendo del bash por el DFOREGROUND

```bash
> sudo docker build -t ubuntu-apache2:con-cmd .
> sudo docker history -H ubuntu-apache2:con-cmd --no-trunc
```

E. Corremos imagen o creamos contenedor ``` sudo docker run -d ubuntu-apache2:con-cmd ```

F. Ahora si vemos si estara corriendo nuestro contenedor

```r
$ sudo docker ps
CONTAINER ID  IMAGE                    COMMAND                  CREATED         STATUS         PORTS  NAMES
9da0318ded3c  ubuntu-apache2:con-cmd   "/bin/sh -c 'apachec…"   46 seconds ago  Up 41 seconds         modest_lehmann
```

G. Para saber si nuestro apache esta funcionando
lo exponemos en un puerto.
`sudo docker run -d --name mi_apache -p 80:80 ubuntu-apache2:con-cmd`

**Resultado:** Si entramos desde el navegador por el puerto 80 podremos
ver la pagina de inicia de nuestro apache que expusimos.

![2_mi_prim_contenedor_apache](./img/2_mi_prim_contenedor_apache.png)

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 18. Introducción al Dockerfle

- FROM: Indicamos que SO empezar o desde que imagen vasarnos.

- RUN: Instrucciones ya sea instalacion u cualquier otro comando,
  como creacion de archivos, usuarios conf de permisos.

- COPY/ADD: Agregamos archivos de nuestra maquina al contenedor.

- ENV: Sirve para agregar variables de entorno.

- WORKDIR: Directorio de trajago.

- EXPOSE: para definir la exposicion de puertos.

- LABEL: No es mas que una etiqueta que podemos poner en cualquier
  lugar del dockerfile, ayuda a dar metadata como version, descipcion, etc.

- USER: Cambiar de usuario para que los RUNs sean a travez de x usuario,
por defecto esta el root

- VOLUMEN: Cambio de donde se guarda la informacion.

<!-- ejecutar al terminar de crear imagen, correr comando cuando termine de crearse el imagen, 
al terminar de crear imagen, cuando termine creacion del imagen, cuando la imagenes este creada ejecutar comando-->

- CMD: Este ejecutara el comando que le indiquemos, cuando la imagen
  se termine de crear, asta ahora lo hemos usado para correr directamente
  un comando que mantenga vivo la imagen pero se puede cualquier otra
  cosa.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 19. Dockerfile - From/ Run / Copy / Add

**Objetivo:** Crearemos una imagen que use estos tres argumentos,
usando copy para copiar una pagina estatica a la carpeta de apache,
algo asi para que automatizar este paso.

Estructura: `COPY origen destino`

A. Creamos el script si vemos debemos de tener una carpeta
llamada mi_html en el mismo lugar donde tenemos el script

```dockerfile
# sistema operativo que tengamos
FROM ubuntu

# instalamos herramientas
RUN apt-get update
RUN apt-get install -y apache2 && apt-get clean

#copiamos codigo
COPY mi_html /var/www/html

#agregamos cmd
CMD apachectl -DFOREGROUND
```

B. Contruimos nuestro contenedor y comprobamos que este la imagen

```r
docker build --tag apache-mihtml .

sudo docker images
```

C. Iniciamos el contenedor

```r
sudo docker run -d --name mi_apache_html -p 80:80 apache-mihtml
```

Nota: **ADD** puede hacer lo mismo que COPY no es recomendable ya que
ADD es mas pensado para copiar codigo de un URL de internet que podra
copiarlo desde aqui.

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 20. Dockerfile - Env / WorkDir / Expose

**Objetivo:**

1. Con ENV Crearemos una variable que contenga un html
y esta guardarlo por un echo a un archivo que podremos ver
desde nuestro contenedor.

2. con WORKDIR cambiaremos el dir donde estamos trabajando
el cual ya no tendremos que especificaar la ruta completa de
donde estan nuestro html.

**Nota:** No usaremos EXPOSE por el tiempo, pero podriamos
configurar apache para que de su servicio en el puerto
8080 y desde el dockerfile decirle que exponga el 8080.

Estructura

```r
ENV nombre_variable valor
```

A. Creamos Script

```dockerfile
# sistema operativo que tengamos
FROM ubuntu

# instalamos herramientas
RUN apt-get update
RUN apt-get install -y apache2 && apt-get clean

#cambiamos de directorio detrabajo
WORKDIR /var/www/

#copiamos codigo
COPY mi_html html/

ENV mi_var <h1>Hola_variables</h1>
RUN echo "$mi_var" > html/variables.html

#agregamos cmd
CMD apachectl -DFOREGROUND
```

B. Creamos imagen y corremos

```r
sudo docker build --tag apache-mihtml .

sudo docker run -d --name apache-var -p 80:80 apache-mihtml
```

**Resultado:** Agregamos el valor de una variable de
entorno a un html desde un cat

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 21. Dockerfile - Label / User / Volume

**Objetivo:** Crearemos un usuario y con USER cambiaremos al este
usuario creado, y pasaremos a un html que usuario estaba primero (root)
y cual despues.

- Creamos Script

```dockerfile
FROM ubuntu
RUN apt-get update
RUN apt-get install -y apache2 && apt-get clean
WORKDIR /var/www/

#CREAMOS USUARIO
RUN useradd mario_ctner
RUN echo "<h1>Usuarios</h1> <h2>Usuario actual 1: $(whoami) </h2>" > html/index.html

# CAMBIAMOS AL USUARIO CREADO
RUN chown mario_ctner -R html/
USER mario_ctner
RUN echo "<h2>Usuario actual 2: $(whoami) </h2>" >> html/index.html

#CAMBIAMOS A USUARIO ROOT POR LIOS DE PERMISOS
USER root
RUN chown root -R html/
RUN echo "<h2>Usuario actual 3: $(whoami) </h2>" >> html/index.html

CMD apachectl -DFOREGROUND
```

2, Creamos imagen y corremos

```r
sudo docker build --tag apache-mihtml .

sudo docker run -d --name apache-var -p 80:80 apache-mihtml
```

**Resultado:** ![3_user](./img/3_user.png)

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 22. Dockerfile - CMD / dockerignore

dockerignore: Podemos ignorar archivos en un COPY

<!--

######################################### divicion titulo

-->

----

<!--

######################################### divicion titulo

-->

### 24. Dockerfile - Buenas prácticas

Las imagenes deben de contener las siguientes caracteristicas
para considerar que tienen buenas practicas.

- **Efimeros:** Pueden destruirse con gran facilidad.

- **Un Servicio por contenedor:** Tipo principio de
una sola responsabilidad por contenedor.

- **Build Context -> Dockerignore**: No tener archivos
pesados si no son absolutamente necesarios.

- **Pocas Capas**: para que sea facil de comprender y de
reutilizar.

- **Multilinea "\"** Para que sea legible facilmente.

- **Varios argumentos en una sola capa:** Podemos pasar
varios argumentos en una sola capa en vez de distintas
instrucciones en varias capas, para conservar la idea
de pocas capas

- **No instalar paquetes inecesarios:** En teoria no deberian
tener cosas como editores de texto (nano) y otras cosas por
que en teoria solo se encargan del servicio.

- **Labels:** Para que tenga Tags y sea mas descriptva la imagen
a lo largo del tiempo

<!--

-->

**Objetivo**: Veremos un ejemplo de pocas capas, multilinea, y varios argumentos.

1, **Mala practica** Aqui tenemos 4 capas pero podriamos
hacerlo en 2 capas solamente.

```dockerfile
FROM nginx

RUN echo "1" >> /usr/share/nginx/html/index.html

RUN echo "2" >> /usr/share/nginx/html/index.html

RUN echo "3" >> /usr/share/nginx/html/index.html
```

2, **Correccion/Buena practica/Pocas capas**: Como podemos ver estamos
cumpliendo con la practica de tener pocas capas. pero
cuesta algo de trabajo leerlo

```dockerfile
FROM nginx

RUN echo "1" >> /usr/share/nginx/html/index.html && echo "2" >> /usr/share/nginx/html/index.html && echo "3" >> /usr/share/nginx/html/index.html
```

3, **Correccion/Buena practica/Multilinea**: Para cumplir
con el multilineas para su facil lectura agregamos el
backslash y podemos ver que es mucho mas facil de leer.

```dockerfile
FROM nginx

RUN echo "1" >> /usr/share/nginx/html/index.html && \
  echo "2" >> /usr/share/nginx/html/index.html && \
  echo "3" >> /usr/share/nginx/html/index.html
```

4, **Correccion/Buena practica/Multilinea**: Para cumplir con
la practica de "Varios argumentos" podemos que estamos usando
la misma ruta varias veces y si queremos cambiarla deberemos de
hacerlo en los tres.

```dockerfile
FROM nginx

ENV path_index /usr/share/nginx/html/index.html

RUN echo "1" >> $path_index && \
  echo "2" >> $path_index && \
  echo "3" >> $path_index
```

5, Creamos imagen y corremos

```r
sudo docker build --tag nginx-bn .
sudo docker run -d --name nginx-bn -p 80:80 nginx-bn
```

**Resultado:** Estamos levantando nginx y agregamos
texto al html desde comandos usando buenas practicas

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 25. Dockerfile - Construyendo una imagen Apache + PHP + TLS/SSL - I

RUN apt-get update
RUN apt-get install -y apache2 && apt-get clean

```dockerfile
FROM ubuntu

ARG DEBIAN_FRONTEND=noninteractive

#instalacion
RUN apt-get update && \
    apt-get install -y apache2 php php-cli php-common && \
    apt-get clean

#comprobamos php
RUN echo "<?php phpinfo(); ?>" > /var/www/html/index.php

CMD apachectl -DFOREGROUND
```

2, Creamos imagen y corremos

```r
sudo docker build --tag apache-php .
sudo docker run -d --name apache-php -p 80:80 apache-php
```

**Resultado:** Tenemos una imagen/contenedor con apache
que puede ejecutar php.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 26. Dockerfile - Construyendo una imagen Apache + PHP + TLS/SSL - II

Ya tenemos el apache-php nos falta agregar una seguridad https

**PENDIENTE**: Hacerlo un ubuntu/debian ya que no pude :( la conf es
diferente creo me falto activar un modulo [Link](https://stackoverflow.com/questions/52767424/docker-apache-installation-fails)

```dockerfile
FROM ubuntu

ARG DEBIAN_FRONTEND=noninteractive

#instalacion
RUN apt-get update && \
    apt-get install -y apache2 php php-cli php-common && \
    apt-get clean

#comprobamos php
RUN echo "<?php phpinfo(); ?>" > /var/www/html/index.php && \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf

#pasamos configuracion
COPY ssl.conf .
RUN cat ssl.conf > /etc/apache2/sites-available/000-default.conf
COPY misitio.crt /etc/apache2/ssl/ssl.crt
COPY misitio.key /etc/apache2/ssl/ssl.key

# en la conf definimos que el https sera el puerto 443
EXPOSE 443

CMD apachectl -DFOREGROUND
```

**Nota:** Lo hago con centos mientras ya que con ubuntu no me
salio.

```dockerfile
FROM centos:7

ARG DEBIAN_FRONTEND=noninteractive

#instalacion
RUN yum -y install \
    httpd php php-cli php-common mod_ssl openssl

#comprobamos php
RUN echo "<?php phpinfo(); ?>" > /var/www/html/index.php

#pasamos configuracion
COPY ssl.conf /etc/httpd/conf.d/default.conf

COPY misitio.crt /etc/apache2/ssl/ssl.crt
COPY misitio.key /etc/apache2/ssl/ssl.key

# en la conf definimos que el https sera el puerto 443
EXPOSE 443

CMD apachectl -DFOREGROUND
```

1, Creamos el certificado

```r
# todo por defecto exepto hostname
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout misitio.key -out misitio.crt
```

2, Creamos archivo de conf de apache y el ssl

```xml
<VirtualHost *:443>
    ServerName localhost
  DocumentRoot /var/www/html
  SSLEngine On

  SSLCertificateFile    /etc/apache2/ssl/ssl.crt
  SSLCertificateKeyFile /etc/apache2/ssl/ssl.key
</VirtualHost>
```

**Resultado:** ![4_https.PNG](./img/4_https.PNG)

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 27. Eliminar imágenes

```r
docker rmi nombre_o_id_imagen
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 28. Cambiar el nombre del Dockerfile

Si por x razones nuestro archivo de imagen se llama distinto a Dockefile
podemos hacerlo con un nuevo flag "f"

<!-- 
crear imangen de dockerfile, crear contenedor de dockerfile, levantar dockerfile
ejecutar dockerfile, correr dockerfile, usar dockerfile, construir dockerfile
-->

```r
docker build -t nombre_imagen_resultante -f mi_dockerfile_con_nombre_distinto .
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 29. Dangling images

En español seria como imagen huerfana y las podemos distinguir
por que tiene `<none>` en la parte de repository y/o en tags.

Esto ocurre cuando creamos una imagen con el mismo nombre y tag
que una que tenemos, lo que pasa es que no se sustituye
mas bien la deja en `<none>` y se crea una del mismo nombre.

```r
sudo docker images
REPOSITORY   TAG     IMAGE ID      CREATED         SIZE
apache-php   latest  1893479d054a  12 minutes ago  325MB
<none>       <none>  da798e56a57a  27 minutes ago  208MB
<none>       <none>  c0db3cffd906  30 minutes ago  208MB
```

Comunmente deberiamos de eliminar estas imagenes, pero docker
no lo hace por si las llegas a necesitar, pero lo mas **recomendable**
es evitar esto definiendo tags.

Eliminar imagenes huerfanas, podriamos hacerlo una por una a travez <!-- eliminar imagenes huerfanas, eliminar imagen huerfana -->
de su id pero que flojera.

```r
# filtrar imagenes
docker images -f dangling=true

#eliminar no se pudo en ubuntu
docker images -f dangling=true -q | xargs docker rmi

#eliminar imagen
sudo docker rmi 0f3e07c0138f
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 30. Crea tu propia imagen con Nginx y PHP-FPM

1. guardamos el repo de ngninx como ngninx.conf (buscar: "ngninx repo file")

```conf
[nginx-stable]
name=nginx repo
baseurl=http://nginx.org/packages/centos/7/$basearch/
gpgcheck=0
enabled=1
```

1. Creamos nuestro docke file

```r
FROM centos:7

COPY nginx.repo /etc/yum.repos.d/nginx.repo

RUN \
  yum -y install nginx --enablerepo=nginx  &&   \
  yum -y install https://centos7.iuscommunity.org/ius-release.rpm  && \
  yum -y install  php71u-fpm   \
          php71u-cli           \
          php71u-mysqlnd       \
          php71u-soap          \
          php71u-xml           \
          php71u-zip           \
          php71u-json          \
          php71u-mcrypt        \
          php71u-mbstring      \
          php71u-gd            \
          --enablerepo-ius && yum clean all

EXPOSE 80 443

VOLUME /var/www/html /var/log/nginx /var/log/php-fpm /var/lib/php-fpm

COPY nginx.conf /etc/nginx/conf.d/default.conf

COPY ./bin/start.sh/ /start.sh
```

Creamos archivo de conf

```r
server {
  listen 80;
  server_name .example.co.uk.dev;

  access_log /usr/local/var/log/nginx/example.co.uk-access.log;
  error_log  /usr/local/var/log/nginx/example.co.uk-error.log error;

  root /var/www/example.co.uk/public;
  index index.php index.html;

  location / {
    try_files $uri $uri/ /index.php?$query_string;
  }

  location ~ \.php {
    fastcgi_index index.php;
    fastcgi_pass unix:/usr/local/var/run/php-fpm.socket;

    include fastcgi_params;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_param PATH_INFO $fastcgi_path_info;
    fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
  }

  # Prevents caching of css/less/js/images, only use this in development
  location ~* \.(css|less|js|jpg|png|gif)$ {
    add_header Cache-Control "no-cache, no-store, must-revalidate";
    add_header Pragma "no-cache";
    expires 0;
  }
}
```

PENDIENTE...

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 31. Tip: Multi-Stage-Build > Aprende a construir apps Java con maven y similares!_

Podemos utilizar varios FROM en un Dockerfile donde el que cuenta es el ultimo
donde podemos contruir un jar con una imagen maven y el jar resultante pasarlo
a otra imagen

1, De un proyecto maven cualquiera pasarlo a nuestro host
en mi caso [jena-a/](./img/5_proyecto_maven_jena.a.zip) o file://E:\cursos\_2019\img\dev_ops\docker\5_proyecto_maven_jena.a.zip

```dockerfile
FROM maven:3.5-alpine

COPY jena-a/ jena-a/

RUN cd jena-a/ && mvn package
#FROM openjdk:8-alpine
```

2, Creamos imagen y corremos: `sudo docker build --tag maven-jena-1 .`

Al terminar de onstruir nos dira donde construyo el .jar
de nuestra app en mi caso.

```r
*[INFO] Building jar: /jena-a/target/jena.a-0.0.1.jar*
```

3, Si vemos el peso de nuestra imagen `sudo docker images` veremos
que pesa 134mb nuestra imagen de jena, pero si miramos la de maven
veremos que pesa 119mb, lo que esta diferencia de 15mb es por las dependencias
que se descargaron del proyecto.

```r
REPOSITORY     TAG         IMAGE ID        CREATED          SIZE
maven-jena-1   latest      f459e3d36b71    3 minutes ago    134MB
maven          3.5-alpine  fb4bb0d89941    11 months ago    119MB
```

**Objetivo:** En este caso lo unico que necesitamos para desplegar nuestra
app es el .jar generado, no necesitamos nada de maven para eso, asi que
**haremos Multi-Stage-Build** otro FROM dentro del dockerfile, para pasar
el .jar resultante a nuestra imagen de despliege, y asi nos ahorraremos el
peso de maven.

4, Pasamos el .jar de la primera imagen a la segunda.

  4,1, Para eso necesitaremos de un alias

```dockerfile
FROM maven:3.5-alpine as maven-builder

COPY jena-a/ jena-a/

RUN cd jena-a/ && mvn package

# Imagen de deespliegue
FROM openjdk:8-alpine

COPY --from=maven-builder /jena-a/target/jena.a-0.0.1.jar /opt/app.jar
COPY --from=maven-builder /jena-a/target/dependency-jars/ /opt/dependency-jars/

CMD java -jar /opt/app.jar
```

5, Corremos: `sudo docker run -d --name maven-jena-1 -p 80:80 maven-jena-2`

<!-- ver logs contenedor, ver logs de contenedor, mostrar logs contenedor, mostrar logs de contenedor-->
**Resultado:** Si vemos los logs del contenedor ```docker logs -f contenedor``` veremos
la ejecucion del programa, en este caso no se mantiene vivo el contenedor ya que
el .jar no se mantiene.

Igual deberia de pesar ~100mb de maven y ~100mb de openJdk pero solo pesa 112
lo cual nos ayudo el multi-stage.

![jena-a/](./img/5_proyecto_maven_jena.a.png)

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 4: Docker Containers

- El contenedor es una **instancia temporal** de una imagen, por lo
que no se recomienda nunca hacer un cambio en los contenedores
ya que estos son temporales.
- Los contenedores a difenrencia de las imagenes son de **escritura y lectura**.
- Podemos crear crear varias imagenes de una sola imagen.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 35. Listar / Mapear puertos

Aunque dentro del contenedor exponga puertos para disponer de
algun servicio si no lo exibimos como parametro no lo encontraremos.

**por ejemplo**, tenemos la imagen oficial de jenkins `docker pull jenkins`
expone para su servicio a travez de un puerto, y si lo corremos de eta manera
`docker run -d jenkins` al ver nuestras imagenes activas `docker ps`
veremos que tiene el puerto 8080, pero al querer entrar desde el navegador no
veremos nada. Pero ahora corremos con la bandera -p `docker run -d -p 8080:8080 jenkins`
donde es **puerto_host:puerto_contenedor** ahora si podremos ver una instancia de
Jenkins, la cual no es obligatorio que tenga el mismo puerto que el contenedor
por ejemplo podriamos hacer un `docker run -d -p 8191:8080 jenkins` y podrian
correr ambos jenkins sin problema.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 36. Renombrar / Iniciar / Reiniciar / Detener

**cambiar nombre contenedor** Cambiar el nombre a contenedor, renombrar contenedor
el cual podemos hacer incluso si esta activo `docker rename nombre_actual nombre_aCambiar`.

<!-- Detenener contenedor sin eliminarlo, parar contenedor sin eliminar, apagar contenedor -->
**Detener contenedor:** Podemos hacerlo a travez de `docker stop nombre_o_id`

<!-- Iniciar contenedor detenido, arrancar contenedor detenido -->
**Iniciar Contenedor:** `docker start nombre_o_id`

**Reiniciar Contenedor:** `docker restart nombre_o_id`

<!--

entrar a contenedor por terminal, abrir terminal de contenedor, conectarse a contenedor, conectar a contenedor, abrir contenedor en terminal
hacer ping entre contenedores, ejecutar ping en contenedores

-->

**Inicar terminal de contenedor:** la t es de terminal y la es de interactiva:

```r
docker exec -ti nombre_o_id bash
```

**Ejecutar comando en contenedor:**

```r
docker exec host_uno bash -c "ping ip_host_dos"
```

<!-- 
entrar a contenedor definiendo usuario,  entrar a contenedor con x usuario, entrar a contenedor con usuario
-->

En el caso de jenkins si vemos el usuario `whoami` veremos que entro
como el usuario de jenkins, pero si queremos **entrar con otro usuario** lo
haremos con `docker exec -ti -u root nombre_o_id bash`

Nos sirve por ejemplo para ver informacion o hacer cambios necesarios por ejemplo
ver la contraseña por default/temporal de jenkins que esta en /var/jenkins_home/secrets/initialAdminPassword

Iniciar contenedor estructura

<!-- estructura levantar contenedor estructura, estructura iniciar contenedor estructura -->

```r
sudo docker run -dti -e "variable=valor" --name nombre_contenedor nombre_imagen comando
```

<!--
iniciar contenedor centos y mantenerlo vivo, iniciar contenedor basico, iniciar contenedor sin dockerfile y mentenerlo vivo contenedor
iniciar sin dockerfile, levantar contenedor centos y mantener vivo contenedor, mantener vivo el contenedor
-->

Iniciar contenedor y mantenerlo vivo: `docker run -td centos`

Tambien podemos agregar el siguiente comando en nuestro dockerfile: `CMD tail -f /dev/null`

<!-- crear y mantener -->

RESUMEN: Al crearse se ingresara a el y al salir de el se eliminara.

- `docker run --name <CONTAINER_NAME> --rm -ti <IMAGE_NAME> bash`

EXTENDIDO: Podemos crear un contenedor solo para unos cuantos comandos.

- Crearlo: `docker run -td --name <CONTAINER_NAME> <IMAGE_NAME> sh`
- Entrar a el: `docker exec -ti <CONTAINER_NAME> sh`
- Eliminarlo: `docker rm -fv <CONTAINER_NAME>` <!-- cuidado igual elimina volumen-->

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 37. Variables de entorno

Ya sabemos cmo crear una variable desde el Dockerfile ```ENV nombre valor``` es decir,
creamos la varible en laimagen, pero igual podemos crearla desde el comando, es decir,
que creamos la variable en el contenedor o crear variable de contenedor, crear
varible a nivel de contenedor, crear variable en contennedor, ejemplo.
<!-- Diferencia entre variabe de imagen y de contenedor -->

```r
# Corremos y creamos variable
sudo docker run -dti -e "test=algo" --name copy-env-2 copy-env

# entramos a la terminal
sudo docker exec -ti copy-env-2 bash

# vamos variable que creamos

echo $test
algo
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 39. Crear un contenedor MySQL

1, descargamos imagen oficial de mysql: `docker pull mysql`

2, Iniciamos el servicio definiciando la pass de root
desde una varible, la cual podemos ver desde la documentacion
de docker hub

```r
docker run -d --name my-mysql -e MYSQL_ROOT_PASSWORD=root -d mysql

#ver logs
sudo docker logs -f my-mysql
```

**Nota**: Si nos fijamos al iniciar el contenedor no expusimos
un puerto para acceder al servicio de forma externa solo de forma
interna, entonces **¿como accedemos de forma intenrna o el mismo host a mysql sin haber definido el puerto?**  

<!-- Saber cual es la ip de un contenedor, saber la ip de contenedor, ver la ip de contenedor -->
3, Inspeccionamos el contenedor para saber cual es su ip

```r
docker inspect my-mysql

# nos dara algo como esto
"Networks": {
  "bridge": {
  ...
  "IPAddress": "172.17.0.2",
  ...
  "MacAddress": "02:42:ac:11:00:02",
  }
}
```

4, Nos conectamos desde la IP del contenedor: `mysql -u root -h 172.17.0.2 -p root`

**Nota:** Ahora si queremos exponer el servicio a de forma externa
deberemos de agregar los puertos, pero ahora vamos a usar mas variables
para crear una base de datos y un usuario

```r
docker run -d -p 3333:3306 --name my-mysql -e "MYSQL_ROOT_PASSWORD=root" -e "MYSQL_DATABASE=my-db" -e "MYSQL_USER=mario" -e "MYSQL_PASSWORD=mario" mysql
```

5, Ahora podemos iniciar desde fuera a mysql con el usuario root con
pass mario y una bd llamada my-db

<!-- entrar a mysql desde host remoto, entramos a mysql-->

```r
C:\Users\subject51>mysql -u root -p -h 192.168.1.68 --port 3333
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 40. Crear un contenedor Mongo

1. Descargamos imagen: `docker pull mongo`

2. Iniciamos contenendor: `docker run -d --name my-mongo -p 27017:27017 mongo`

<!-- Ver estaditicas contenedor, ver estadisticas de contenedor estadisticas-->
Ver cuanta memoria consume un contenedor, ver estadisticas de un contenedor,
ver consumo de recursos de contenedor.

`docker stats my-mongo`

![stats](./img/6_stats.PNG)

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 41, Crear un contenedor Tomcat

ver la doc de docjer hub

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 42, Crear un contenedor Postgres

ver la doc de docjer hub

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 43, Crear un contenedor nginx

ver la doc de docjer hub: [docker.com](https://hub.docker.com/_/nginx)

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 43, Crear un contenedor jenkins

ver la doc de docjer hub

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 44. Administrar usuarios

Podmeos determinar con que usuario entrar ya que si recordamos
poe defecto va entrar por defecto el usuario root, pero si definimos
el dockerfile de la siguietne manera

```dockerfile
FROM centos

ENV prueba 1234

RUN useradd mario

USER mario
```

al entrar al contenedor `exec` veremos que el usuario con
el que entrara sera mario

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 45. Limitar recursos a un contenedor

Lo podemos hacer cuando arrancamos el contenedor, ya que por defecto
tomara todos los recursos que tenga la maquina.

1, Limitar el uso de ram

```r
Eje.1
docker run -d -m "5gb" --name nombre_contenedor nombre_imagen

Eje.2
docker run -d -m "500mb" --name nombre_contenedor nombre_imagen
```

2, **Limitar CPU**: Para empezar podemos saber el uso de nuestro
CPu con el siguinte comando, el cual muestra en mi caso que tengo
solo un CPU, pero podria mostrar 4 u mas CPUs que tenga la maquina.

```r
grep "model name" /proc/cpuinfo
model name      : Intel(R) Core(TM) i3-2370M CPU @ 2.40GHz
```

Limitamos, en este caso estamos disiendo que solo ocupe el CPU 0, 1 y 2 en
caso de tener mas de uno.

```r
docker run -d --cpuset-cpus 0-2 --name nombre_contenedor nombre_imagen
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 46. Copiar archivos contenedor

<!--
copiar archivo a contenedor, Copiar archivo de host a contenedor,Copiar archivo de contenedor a host, Copiar de host a contenedor, Copiar de contenedor a host pasar archivo de host a contenedor, pasar archivo de contenedor a host,
copiar carpeta a contenedor, Copiar carpeta de host a contenedor, copiar archivos a contenedor
Copiar carpeta de contenedor a host, pasar carpeta de contenedor a host,
-->

Estructura

```r
# copiar archivo
docker cp archivo_host nombre_contenedor:/ruta_a/copiar

# copiar carpeta
docker cp /folder_to_copy/. nombre_contenedor:/path/new_folder
```

**Objetivo:** Supongamos que queremos copiar a un contenendor
de mysql ya creado un archivo a su directorio /home, el cual tenemos el
siguinte contenedor corriendo.

```r
CONTAINER ID   IMAGE   COMMAND     CREATED    STATUS     PORTS       NAMES
ed4859621e79   mysql   "docker…"   3min ago   Up 3min    3333:3306   my-mysql
```

1. Copiar de host a contenedor: `docker cp hola.txt my-mysql:/home`

2. Copiar de contenedor a host, para esto solo lo hacemos al
revez

```r
docker cp my-mysql:/var/log/dpkg.log logs_mysql.log
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 47. Convierte un contenedor en una imagen

Podemos a partir de un contenedor que ya sufrio cambios o
se le agrego datos convertirlo a una imagen con esos cambios
y datos, pero recordemos que los contenedores deberian de ser
efimeros, lo cual **no es una buena practica**
para guardar los datos generados o la configuracion personalizada.

```r
docker commit nombre_contenedor nombre_imagen_resultante
```

**Importante** Los datos que esten dentro de **VOLUMEN ruta/volumen/** no
se les guardaran los datos.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 48. Sobreescribe el CMD de una imagen sin un Dockerfile

Por defecto el CMD es "/bin/bash" pero si queremos cambiarlo a la
hora de levantar el contennedor podemos hacer lo sig.

```r
docker run -dti nombre_imagen comando
```

Ejemplo queremos iniciar un servidor html con phyton
podemos hacerlo con el siguiente comando, donde la parte de
centos ya conocemos, pero en adelante lo que empieza con phyton
ya estamos sobre-escribiendo lo que es el CMD.

```r
docker run -d -p 8080:8080 centos python -m SimpleHTTPServer 8080
#creara
CONTAINER ID  IMAGE   COMMAND                 PORTS
a901ac5dad13  centos  "python -m SimpleHTT…"  0.0.0.0:8080->8080/tcp
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 49. destruir contenedores automáticamente

<!-- eliminar contenedor cuando termine de ejecutarse -->

Para esto usamos ```docker run --rm``` el cual hara lo que
tenga que hacer y se eliminara el contenedor de forma automatica.

```r
docker run --rm centos bash
```

En este caso en tanto nos salgamos "exit" se auto-destruira

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 50. Cambiar el Document Root de Docker

<!--ruta del docker root-->
**¿Que es el document Root default /var/lib/docker?** Si buscamos esta info en docker ```docker info | grep -i root```
nos dara el document root ```Docker Root Dir: /var/lib/docker``` que es la ruta donde
docker guarda las imagenes, redes, etc. donde cada vez que hacemos algo con
docker se vasa en este directorio.

1, Editamos **/lib/systemd/system/docker.service** y podemos
hacerlo a traves de **ExecStart** ejemplo, le agregamos el --data-root

```r
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock --data-root=alguna/ruta/
```

2, Indicamos que leea los cambios que agregamos: `systemctl deamon-reload`

3, Reiniciamos para que se apliquen los cambios: `systemctl restart docker`

psar los cambios antiguos

PENDIENTE

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 5: Docker Volumes

- Fuente: [docu](https://docs.docker.com/storage/volumes/)

Estructura: `docker run ... -v /ruta/host:ruta/contenedor`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 52. Introduccion - Volumenes

Los volumenes permiten almacenar data persistente del contenedor, ya
que sin los volumenes toda la informacion contenida en el al ser
eliminado tambien se borraran los datos.

Los volumenes nos ayudan a mantener la informacion de forma
persistente, independientemente de que se elimine el contenendor

Por ejemplo, si creamos una base dedatos en un contenedor sin
volumenetes y eliminamos el contenedor todos los registros y
DDL sera eliminado igual, en este problema es donde entran para
solucionar los volumes.

Tenemos 3 yipos de volumentes

- **Host**: Son aquellos que se almacenan en el Docker Host por ejemplo
en una carpeta que nosostros definimos.

- **Anonymus**: No definimos una carpeta pero docker defina una random
y ahi aleatoriamente persiste la informacion.

- **Named Volumenes**: Son carpetas que nombramos pero estas son administradas
por docker, lo mismo que los Anonymus

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 54. Volúmenes de host - Caso práctico MySQL

**Objetivo:** En la documentacion de Docker hub nos dice que toda la info
se guarda en /var/lib/mysql asi que vamos a poner esta carpeta dentro de
un volumen.

<!-- Crear volumen dentro de nuestro host, asignar volumen de contenedor en host, guardar carpeta del contenedor en carpeta del host-->

1, Lo asignamos cuando creamos el contenedor con

```r
docker run -d --name my-mysql -p 3306:3306 -e "MYSQL_ROOT_PASSWORD=root" -v /home/mario/docker/volumentes/datos1:/var/lib/mysql mysql

#Entramos y creamos algunos datos
mysql -u root -p -h 192.168.1.68 --port 3306
create database test; use test; create table usuarios (nombre varchar(50));  insert into usuarios VALUES("holadd");
```

2, Si vemos nustra carpeta */home/mario/docker/volumentes/datos1* veremos
algunos archivos

**Resultado:** A pesar de que eliminamos el contenedor a la hora de crear
un nuevo contenedor apuntando su volumen a la misma ruta del host que el
contenedor anterior aun tenemos todos los datos.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 55. Volúmenes anónimos - Caso práctico MySQL

Docker de forma automatica va a crear un volumen el cual podemos ver en "/var/lib/docker/volumes"
esta ruta tendra varias carpetas con el nombre del id que doker le asigno.

Por ejemplo  

1, Creamos un contennedor sin asignarle un volumen

```r
docker run -d --name my-mysql -p 3306:3306 -e "MYSQL_ROOT_PASSWORD=root" mysql
186d6522cb3bb2ea0d45c32e3c44223a668f02bf75feb8f3674d1accb6dec5fc
```

2, Como vemos el id que nos dio al crear lo podemos ver con un inspect

<!-- ver volumen de contenedor, ver cual es el volumen de contenedor, ver ruta del volumen del contenedor, ruta volumen contenedor-->

```r
sudo docker inspect my-mysql | grep "Source"
"Source": "/var/lib/docker/volumes/ed546d52ff8b89b60691a1929d5ddf0c251cbcbe7eb5c2f876d6e77c5e53d64c/_data",
```

**Importante:** Al eliminar un contenedor tenemos la opcion de eliminar
su volumen anonimo o no, esto es lo que hace el argumento ```-v```

```r
sudo docker rm -fv contenedor
```

3, SI queremos reutilizar este volumen anonimo primero es evitar eliminarlo
con el argumento ```-v``` y segundo es pasarlo como volumen del otro

```r
docker run -d --name my-mysql -p 3306:3306 -e "MYSQL_ROOT_PASSWORD=root" -v /var/lib/docker/volumes/ed546d52ff8b89b60691a1929d5ddf0c251cbcbe7eb5c2f876d6e77c5e53d64c/_data/:/var/lib/mysql mysql
```

**Resultado:** Vimos que podemos acceder al volumen anonimo y reutilizarlo
pero **no se recomienda** ya que da nombres raros lo cual puede confundir
su gestion.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 57. Volúmenes nombrados - Caso práctico MySQL

Estos son como la union de un volumen de Host y uno anonimo

<!-- crear volumen -->

1, Creamos un volumen desde docker con: `docker volume create mi-volumen`

<!-- mostrar los volumenes creados, listar volumentes de docker, ver los volumes-->

2, Ahora podemos ver los volumenes creados: `docker volume ls`

3, Ahora para usarlo lo podemos hacer de la siguiente manera.

```r
docker run -d --name my-mysql -p 3306:3306 -e "MYSQL_ROOT_PASSWORD=root" -v mi-volumen:/var/lib/mysql mysql
```

**Resultado:** Tenemos el volumen dontro de las carpetas de
docker pero le asignamos un nombre, y de igual forma si elliminamos el
contennedor y lo creamos con el mismo la data estara guardada en esta y
podremos recuperarla.

<!-- volumenes en windows volumenes, uso de volumenes en windows -->

Para usar volumenes en windows, debemos de poner un doble slash `//`
para indicar la raiz tanto de nuestro windows como del windows, por
ejemplo

- `-v` indicamos que queremos trabajar en la ruta `//e/cursos/0activ/sistema/docker/node/dos/app`
  de nuestro equipo, y que su homologa en el contenedor sea `/app/`
- `-w`: que cuando entremos al contenedor sea en la ruta `/app`

```r
docker run -ti -v //e/cursos/0activ/sistema/docker/node/dos/app:/app/ -w //app maven:3-openjdk-11-slim bash
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 57. Volúmenes nombrados: Creacion y uso de respldos con .tar

**Objetivo:** Vamos a crear un volumen y vamos a crear un respaldo de
este en un .tar, luego este .tar vamos a recuperarlo tanto en un volumen
nuevo, como sustituir el respaldo por lo que hay en el .tar

1, Creamos contenedor que descomprima el tar en la raiz del volumen

- `-v vol_original:/workdir` No es necesario crear el volumen ya que se
  creara solo

```r
# CREASMOS CONTENEDOR PARA GENERAR CONTENIDO
docker run --rm \
  -v vol_original:/workdir \
  -w //workdir \
  -ti nginx:1.18-alpine sh \
  -c 'mkdir -p test/subdir && echo "raiz" > raiz.txt && echo "subDir" > test/algo1.txt && echo "SubSubDir1" > test/subdir/sub.log && ls -R .'

# CARPETAS Y ARCHIVOS CREADOS
# ./raiz.txt
# ./subDir/sub.txt
# ./subDir/subSubDir/subSub.log
```

3, Crear respaldo guardandolo en un .tar.

NOTA: Al entrar al .tar en windows se vera una carpeta llamada "." (sin comillas)
pero al descomprimir esta no se creara

- `-v /$(pwd):/backup \`: en la carpeta que corremos el comando se creara el .tar
- `-v vol_original:/to_backup \`: # monstamos el columen a respaldar
- `-w //to_backup \`: # para que el contenido del .tar quede igual al del volumen
- `alpine:3.13.3`: # Cualquier imagen con tar nos valdra
- `tar cvf //backup/X.tar .`: # creamos .tar en la carpeta de nuestro host que se monto en el contenedor

```r
docker run --rm \
  -v /$(pwd):/backup \
  -v vol_original:/to_backup \
  -w //to_backup \
  alpine:3.13.3 \
  tar cvf //backup/vol_recuperado.backup.tar .
```

5,A, Crear volumen a partir de .tar, en este caso el .tar es nuestro respaldo
del volumen anterior.

Windows

```r
docker volume create vol_recuperado && \
  docker run --rm \
    -v vol_recuperado:/restored \
    -v /$(pwd):/to_restore \
    -w //to_restore \
    alpine:3.13.3 \
    tar -x -f vol_recuperado.backup.tar -C //restored
```

Linux

```r
docker volume create vol_recuperado && \
  docker run --rm \
    -v vol_recuperado:/restored \
    -v /$(pwd):/to_restore \
    -w //to_restore \
    alpine:3.13.3 \
    tar -x -f vol_recuperado.backup.tar -C //restored
```

5,B, Comprobar que el volumen tenga la info del respaldo.

```r
docker run --rm \
  -v vol_recuperado:/some_path \
  -w //some_path \
  nginx:1.18-alpine \
  ls -la
```

6, Sustituir contenido de volumen por el de un .tar

G,1, Cambiar el contenido como querramos nosotros

<!-- ver contenido en un volumen, ver archivos de un volumen, ver carpetas de un volumen, ver volumen
explorar volumen, entrar a volumen
 -->

NOTA: como tal no se entra a un volumen, mas bien creamos un contenedor
con el volumen, y usando el contenedor entramos al volumen. **CUIDADO**
si nos equivocamos de nombre, se creara un nuevo volumen con el nombre
que indicamos.

- `volume inspect` es para que si nos quivocamos de nombre del volumen
  no ejecute el segundo comando y asi no creara otro volumen.

```r
docker volume inspect vol_original > /dev/null && docker run --rm \
  -v vol_original:/some_path \
  -w //some_path \
  -ti nginx:1.18-alpine sh
```

6,2, Sustitur contenido de volumen por lo que hay en un .tar

- IMPORTANTE: Recomendable primero hacer un backup de la info actual ya que
  esta se borrara

- CUIDADO: Puede que pase que por alguna razon falle la eliminacion y
  no empieze la descomprecion, o que solo tengamos un `&` lo que hara
  que descomprima y elimina al mismo tiempo lo que hara que elimine lo
  que se le puso dejando incompleto el respaldo.

- CUIDADO: Evitar intentar hacerlo e un solo comando, lo itente y me estubo
  fallando

- Nota: Puede que marque un error `No such file or directory` pero puede
  ser por que la carpeta estaba vacia o no habia un folder/archivo que
  iniciara con "." (punto).

Partes del comando

- `-v vol_original:/volume_content`: Montamos el volumen para poderlo
  manipular dentro del contenedor que creamos para este proposito
- `-v /${PWD}/xxx.backup.tar:/path_of_backup/xxx.backup.tar`: Exportamos
  respaldo dentro del contenedor para descomprimirlo en la carpeta del
  volumen
- `nginx:1.18-alpine sh`: Nos vale cualquier imagen que tenga tar
- `-c 'rm -rf .... & tar -x -f vol_original.backup.tar -C //volume_content'`: Es
  **importante** notar que tenemos un doble andpersant **&&**, y que en
  tar tenemos `-C` que apunta a donde montamos el volumen

```r
# solo sustituimos el nombre del volumen y del archivo .tar
docker run --rm \
  -v vol_original:/volume_content \
  -v /${PWD}/vol_recuperado.backup.tar:/path_of_backup/vol_recuperado.backup.tar \
  -w //path_of_backup \
  nginx:1.18-alpine sh \
  -c 'rm -fr //volume_content/* //volume_content/.*'

docker run --rm \
  -v vol_original:/volume_content \
  -v /${PWD}/vol_recuperado.backup.tar:/path_of_backup/vol_recuperado.backup.tar \
  -w //path_of_backup \
  nginx:1.18-alpine \
  tar -x -f vol_recuperado.backup.tar -C //volume_content
```

6,3, Comprobamos metiendonos de nuevo al contenedor y deberiamos de ver
el mismo contenido que tenimos el el .tar y lo que estaba antes ya no
deberia de estar.

**Resultado:** Aprendimos a crear un respaldo d un volumen en un .tar,
de de este .tar crear un nuevo volumen y a sustituir el contenido que
habia por lo que estaba dentro del .tar.

- **trabajos futuros:**
  - Ver como combinar dos respaldos
  - creacion de respaldos progresivos

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 58. Dangling volumes

Al igual que las imagenes huerfanas es el mismo problema, el cual
tenemos volumenes que no usa nngun contennedor y si no los nomnramos
no sabremos de que trataba este volumen, **se recomenda eliminarlos**.

- saber que volumenes no esta referenciados, <!-- listar volumenes huerfanos, listar volumenes sin referencia -->

```r
docker volume ls -f dangling=true
```

- Eliminar volumen: `docker volume rm id_nombre_volumen`

- Eliminar todos los volumenes: `sudo docker volume rm $(sudo docker volume ls -qf dangling=true)`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 59. Persistiendo data en MongoDB

Lo mismo pero mongo guarda sus datos en **data/db**

```r
docker run -d -p 27017:27017 --name mi-mongo -v /ruta/host/:/data/db mongo
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 60. Persistiendo data en Jenkins

Segun la docu dice /var/jenkins_home

```r
docker run -d -p 8080:8080 --name mi-jenkins -v /ruta/host/:/var/jenkins_home jenkins
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 61. Persistiendo logs en Nginx

Solo identificamos donde se guardan los log y ya

<!-- levantar servidor nginx-->

```r
docker run -d -p 80:80 --name mi-nginx -v /ruta/host/:/var/log/nginx/ nginx
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 62. Comparte volúmenes entre uno o más contenedores

**Objetivo:** Tenemos dos contenedores uno que ejecuta un proceso y
lo registra en un html y otro que expone este html en un servidor web,
por lo que requeririamos que se compartan archivo ambos contenedores.

1, Creamos un contenedor que siempre imprimira la hora en un html con
el script **script_tiempo.sh**

```r
#!/bin/bash

while true; do
  echo $(date +%H:%M:%S) >> /opt/index.html && \
  sleep 10
done
```

2, Definimos el dockerfile

```r
FROM centos

COPY script_tiempo.sh /script_tiempo.sh

RUN chmod +x /script_tiempo.sh

CMD /start.sh
```

3, Construimos imagen y corremos pero le definimos su volumen

```r
docker build -t cada_5seg .
sudo docker run -v /home/mario/docker/volumentes/vol_compartido/common/:/opt -d --name cada5 cada_5seg
```

4, Creamos contenedor nginx, pero ahora el contenido sera en donde ya esta generando
cada5 el otro contenedor apuntando a donde estan sus html

```r
sudo docker run -p 80:80 -v /home/mario/docker/volumentes/vol_compartido/common/:/usr/share/nginx/html -d --name mi-nginx nginx
```

**Resultado:** Estamos compartiendo un volumen en dos contenedores
en el cual en un contenedor es la ruta donde sirve el serv. web y el otro
solo guarda contenido en esa ruta.,

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 6: Docker Network

Veremos como crear eliminar redes ysus tipos que son, como conectar
los contenedores.

- **Bridge:**

- **Host:**

- **None:**

- **Overlay:**

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 65. ¿Cuál es la red por defecto?_

<!-- inspeccionar contenedor, ver ip de contenedor, ver detalles de contenedor, ver detalles contenedor-->

Por defecto Docker crea y asigna la red **docker0** Podemos ver la ip de docker con
`ip a | grep docker` en mi caso podemos es el rango 172.17.0.1/16 en el cual es
el Getway, y el rango si inspeccionamos un contenedor con
`sudo docker inspect mi-centos | grep "IPAddress"` y veremos que la IP esta en este rango.

<!-- ver las redes de docker, listar redes -->

1, Listar las redes de docker `docker network ls` la que dice bridge
en la colunma *NAME* veremos que esa es la que usa.

2, Inspeccionamos la red de docker: `docker network inspect bridge`

<!-- hacer ping de un contenedor a otro, ping entre contenedores, comunicacion entre dos contenedores, comunicacion entre 2 contenedores, comunicacion entre  contenedores -->

**Actividad:** Desde un contenedor centos vamos a hacerle ping a otro
contenedor usando las IPs que docker les asigno.

3, Levantamos ambos contenedores con `sudo docker run -dti --name host_uno centos bash`

4, Aberiguamos la ip de ambos con  `sudo docker inspect host_dos | grep "IPAddress"`

5, Hacemos ping `sudo docker exec host_uno bash -c "ping ip_host_dos"`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 66. Crear una red definida por el usuario

Como vimos arriba por defecto es bridge0 pero podemos agregar resdes
en las cuales nosotros les agregamos contenedores, el cual es como una
mini red virtual en la cual estaran los contenedores.

<!-- crear una red en docker, crear red, crear network estructura-->

```r
docker network create -d {dirver} --subnet {xxx.xxx.xxx.xxx/xx} --gateway {xxx.xxx.xxx.1} {nombre_red}
```

ejemplo:

```r
docker network create -d bridge --subnet 173.100.1.0/24 --gateway 173.100.1.1 mi-red
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 68. Agregar contenedores a una red distinta a la por defecto

<!-- definir red a contenedor, asignar red a contenedor, agregar red a contenedor-->

Lo podemos hacer a travez del el argumento ```--network``` de run

```r
docker run --network nombre-red -d --name nombre -ti nom_imagen
```

ejemplo

```r
docker run --network mi-red -d --name centos_red_p -ti centos

# si inspeccionamos el contenedor
"Networks": {
   "mi-red": {...
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 69. Conectar contenedores en la misma red

Una de las ventajas de crear los contenedores en una red personalizada es
que podemos hacer referencia entre ellos por su nombre, como una especie de
DNS por ejemplo. en el ejemplo de comunicacion entre dos contenedores

```r
docker run --network nombre-red -d --name nombre -ti nom_imagen
```

**Ejemplo**: Abajo podemos ver como creamos dos contenedores host1 y host2
con la red de "mi-red" y luego no usamos la IP usamos el nombre del contenedor
para hacer referencia en el ping

![7_custom_networking_ping](./img/7_custom_networking_ping.gif)

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 70. Conectar contenedores en distintas redes

**Pre-Condiciones:** Tenemos 2 redes creadas por nosotros (mi-red, mi-red2) y
tenemos 3 contenedores creados uno con mi-red, otro con mi-red2 y el tercero
con la red por defecto docker0.
**Objetivo:** Queremos comunicar estos contenedores apesar de que estos estan
en diferentes redes.

1, Cremos las redes: `docker network create mi-red2`

2, Creamos host1, host2 y host3 este ultimo sin el --network: `docker run -dti --network mi-red1 --name host1 centos`
  
  2.1. Vemos que tengamos los contennedores con las redes diferentes.
  `sudo docker inspect hostX | grep -A 1 "Networks"`

3, Agregamos conexion a host1 a mi-red2 para que se pueda conectar a host2 y lo mismo
para los demas contenedores

`sudo docker network connect mi-red2 host1`

**Resultado:** Si hacemos un `docker inspect host1` veremos que este
se le agrego la red "mi-red2" y aun conserva la "mi-red" por lo que este
se podra comunicar con los contenedores que esten en ambas redes

4, Si quremos desconectar el contenedor, o quitar la red del contenedor que
recien agregamos podemos hacerlo con

```r
sudo docker network disconnect mi-red2 host1
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 71. Eliminar redes

Nos puede mandar el error **has active endpoints** lo que significa que
tenemos algun contenedor usando la red que queremos eliminar, lo cual podemos
desconectar o eliminar el contenedor para poder hacerlo.

```r
sudo docker network rm mi-red2
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 71. Asignar IP a un contenedor

Lo hacemos a travez del argumento ip el cual antes debemos de haber creado una red con
su gateway y la subnet y que la ip que le damos este dentro del rango.

```r
sudo docker run --network [nombre_red] --ip [ip_enRango] --name [nom_cont] -ti [nom_img]
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 73. La red de Host

Esta es la red de nuestra misma maquina si hacemos un `docker network ls`
veremos que tenemos una red llamada **host**, lo unico que debemos de hacer
es agregar esta red al contenedor.

```r
docker run --network host ...
```

Lo que hara es heredar todad las configuraciones del Docker host incluso
el hostname.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 74. La red None

Lo mismo que host si vemos en ```docker network ls``` veremos una red
llamada none, y significa que no tendra red si la asignamos a nuestro
contenedor.

```r
docker run --network none ...
```

El contenedor no tendra red

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 75. CORS en Docker

SI creamos un contenedor y exponemos un servicio por un puerto veremos
algo como esto ```0.0.0.0:8080->80/tcp``` en PORT lo que significa que
podremos acceder a esta maquina a ese puerto desde cualquier IP, pero
si queremos liminar a que solo sea accesible a algunas IP especificas
podremos hacerlo, **por ejemlo** si queremos que solo sea accesible nuestro
host desde el local host hariamos algo como.

```r
docker run -d -p 127.0.0.1:8081:80 nginx
```

Si lo vemos veremos un `127.0.0.1:8081->80/tcp` lo que significa
que el contenedor solo sera accesible por el localhost.

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 7: Docker Compose

Nos ayuda a definir aplicaciones multi-contenedor que podria
ser una aplicacion que necesita de apache y mysql, cada uno de
estas erramientas estarian definidas en diferentes contenedores
podriamos hacer de forma manual como hemos aprendido pero seria
tardado.

En esto entra Compose donde desde un archivo podemos definir los
diferentes contenedores, redes, imagenes, volumentes etc. lo creara
todo lo necesario a travez de un paso todo lo necesario para nuestra
app multicontenedor.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 78. Instalación

Fuente: [Link](https://docs.docker.com/compose/install/)

instalar en linux

```r
sudo curl -L "https://github.com/docker/compose/releases/download/X.X.X/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

En caso de tener problemas de permisos: `sudo chmod +x /usr/local/bin/docker-compose`

Comprobamos instalacion: `docker-compose --version`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 79. Primeros pasos en Compose

1, Creamos un archivo llamado **docker-compose.yml** con los
capos obligatorios de *version* y *services*

```yml
version: '3'
services:
    nombre_servicio:
        container_name: nginx_container
        ports:
            - "8080:80"
        image: nginx
```

2, Construimos con docker-compose: `sudo docker-compose up -d`

Si observamos vemos que se creo una red y se la asigno a nuestro
contenedor

3, Para detener lo que iniciamos agregamos un: `sudo docker-compose down`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 80. Variables de entorno en Compose

fuente: `https://docs.docker.com/compose/environment-variables/`
Tenemos dos formas de crearlos, definindolos en el mismo yml o

```yml
version: '3'
services:
  db:
    image: mysql
    container_name: mi-mysql
    ports:
      - "3306:3306"
    environment:
      - "MYSQL_ROOT_PASSWORD=root"
```

<!--

-->

Desde archivo docker-compose.yml

```yml
version: '3'
services:
  db:
    env_file:
      - ./commons.env
    image: mysql
    container_name: mi-mysql
    ports:
      - "3306:3306"
```

commons.env

```yml
MYSQL_ROOT_PASSWORD=root
MYSQL_USER=mario
MYSQL_PASSWORD=mario
```

- Levantamos: `sudo docker-compose up -d`

Podemos comprobar que se crearon correctamente las
varibles entrando al contenedor y ejecutar env para
ver las varibles de linux.

```r
sudo docker exec -ti mi-mysql bash

env | grep MYSQL
 MYSQL_PASSWORD=mario
 MYSQL_ROOT_PASSWORD=root
 MYSQL_MAJOR=8.0
 MYSQL_USER=mario
 MYSQL_VERSION=8.0.17-1debian9
```

**Recomendacion:** Por seguridad es recomendable usar el
archivo y que este tenga los permisos contados.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 81. Volúmenes en Compose

Para los **volumenes nombrados** que son aquellos que se crean
en las carpetas de docker, hacemos lo siguiente.

```yml
version: '3'
services:
  db:
    image: nginx
    container_name: mi-nginx
    ports:
      - "80:80"
    volumes:
      - "mi-volimen-1:/usr/share/nginx/html"

volumes:
  mi-volimen-1:
```

La informacion se guardara en este caso en /var/lib/docker/volumes/nginx_mi-volimen-1/_data/
por lo que mientras no eliminemos el volumen la info sera persistente.

<!--

 -->

Para los **volumentes de host** lo hariamos declarando la ruta host dos puntos y
la ruta del contenedor.

```yml
version: '3'
services:
  web:
    image: nginx
    container_name: mi-nginx
    ports:
      - "80:80"
    volumes:
      - "/home/mario/docker/compose/nginx/html:/usr/share/nginx/html"
```

La informacion estara en la ruta de nuestro Docker Host que definimos de forma
persistente aun que se borre el contenedor.

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 82. Redes en Compose

Solo hacemos el uso de networks

```yml
version: '3'
services:
  web:
    image: nginx
    container_name: mi-nginx
    ports:
      - "80:80"
    networks:
      - mi_network_1

networks:
  mi_network_1:
```

**Cuidado:** Respetar la sintaxis el networks de abajo va sin "- " pero
el de arriba si lo lleva, al parecer el "- " indica un conjunto/arreglo

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 83. Construye imágenes en Compose

Podemos crear imagenes con docker build en la cual definimos una ruta
en donde esta el Dockerfile del cual se construira.

1, docker-compose.yml

```yml
version: '3'
services:
  custom:
    container_name: mi-custom-image
    image: custom-image

    build: .
```

1,1, En caso de que querramos definir un Dockerfile con
un nombre distinto que por el default hariamos

```yml
version: '3'
services:
  custom:
    container_name: mi-custom-image
    image: custom-image

    build:
      context: .
      dockerfile: mi_dockerfile
```

3, En la misma ruta agregamos el Dockerfile

```dockerfile
FROM centos

RUN mkdir /opt/test
```

4, Construimos desde docker-compose: `docker-compose build`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 84. Sobreescribe el CMD de un contenedor con Compose

Creamos un servicio web desde un comando de python del
cual necesitamos hacerlo desde el CMD

```yml
version: '3'
services:
  web_python:
    image: centos
    command: python -m SimpleHTTPServer 80
    ports:
      - "80:80"
```

<!-- usar otro nombre que el docker-compose.yml, docker-compose usar otro archivo, docker-compose archivo distinto -->

1. Levantamos: `docker-compose -f docker-compose-cmd.yml up -d`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 85. Limitar recursos en contenedores (Compose v2)

Fuente: [Link](https://docs.docker.com/compose/compose-file/) buscar "RESOURCES"

Lo hacemos con los mismos valores que como lo haciamos antes

```yml
version: '3'
services:
  mi-host:
    image: nginx
    container_name: host_limit
    resources:
      deploy:
        limits:
        cpus: '0.50'
        memory: 50M
```

PENDIENTES

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 86. Politica de reinicio de contenedores

Si nosotros desde docker detenemos un contenedor este se quedara
de esta manera, pero si por X motivos queremos que estos al detenerse
y cumplir algunas condiciones inicien de nuevo por si solos, entonces
podemos hacer uso de la politica de reinicio.

**Objetivo:** Vamos a crear un contenedor que en su CMD ejecutara un
script que durara X tiempo y morira, y por consiguiente igual morira
el contenedor, pero con las politicas de reinicio este se volvera a
iniciar segun cuando se lo indiquemos

1, start.sh

```sh
#!/bin/bash
echo "inicio..."
sleep 5
echo "Termino..."
```

2, Dockerfile

```dockerfile
FROM centos
COPY start.sh /start.sh
RUN chmod +x /start.sh
CMD /start.sh
```

3, docker-compose.yml

<!-- iniciar contenedor junto con systema, iniciar junto el sistema, in
iniciar contenedor junto con el sistema, iniciar contenedor con el sistema,
crear contenedor que inicie junto con el sistema
-->

```yml
version: '3'
services:
  mi-host-2:
    container_name: mi-host-2
    image: restart-image
    build: .
# siempre que se detenga se va a reiniciar
#   restart: always
# siempre que se detenga se va a reiniciar, almenos que nosotros le demos un stop
    restart: unless-stopped
# Si falla no se reiniciara
    restart: on-failure
```

4, levantamos: `docker-compose up -d`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 87. Personaliza el nombre de tu proyecto en Compos

Podemos cambiar el nombre del prefijo: `docker-compose -p mi_prefijo up -d`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 90. Instalando WordPress + MySQL

- Lo hacemos con el siguiente yml

  - [8_docker-compose-wordpress.yml](./img/8_docker-compose-wordpress.yml)

  - [docker-compose-wordpress.yml](./img/8_docker-compose-wordpress.yml)

- levantar: `docker-compose -f docker-compose-wordpress.yml up -d`

- eliminar: `docker-compose -f docker-compose-wordpress.yml down`

- Observar que si volvemos a levantar los cambios no se eliminan ya que se guardan en el docker host

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 91. Instalando Drupal + PostgreSQL

- Lo hacemos con el siguiente yml  
  - file://E:\cursos\_2019\img\dev_ops\docker\9_docker-compose-drupal.yml  
  - [9_docker-compose-drupal.yml](./img/9_docker-compose-drupal.yml)  

- Igual que wordpress

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 92. Instalando PrestaShop + MySQL

- Lo hacemos con el siguiente yml  
  - file://E:\cursos\_2019\img\dev_ops\docker\10_docker-compose-prestashop.yml  
  - [10_docker-compose-prestashop.yml](./img/10_docker-compose-prestashop.yml)  

- Igual que wordpress

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 93. Instalando Joomla + MySQL

- Lo hacemos con el siguiente yml  
  - file://E:\cursos\_2019\img\dev_ops\docker\11_docker-compose-joomla.yml  
  - [11_docker-compose-joomla.yml](./img/11_docker-compose-joomla.yml)  

- Igual que wordpress

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 94. Instalando Reaction Ecommerce - NodeJS + MongoDB

- Lo hacemos con el siguiente yml  
  - file://E:\cursos\_2019\img\dev_ops\docker\12_docker-compose-react.yml  
  - [12_docker-compose-react.yml](./img/12_docker-compose-react.yml)  

- Igual que wordpress

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 95. Instalando Guacamole

- Sirve de para control remoto de equipos a travez de la web

  - file://E:\cursos\_2019\img\dev_ops\docker\13_guacamole.zip  

  - [13_guacamole.zip](./img/13_guacamole.zip)  

- Igual que wordpress

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 97. Instala Zabbix con Compose

- Es para monitoreo de servidores linux  
  - file://E:\cursos\_2019\img\dev_ops\docker\14_zabbix.zip  
  - [14_zabbix.zip](14_zabbix.zip/img/dev_ops/docker/14_zabbix.zip)  

- Igual que wordpress

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## Sección 8: Docker Registry

### 99. Crea tu propio Docker Registry

Fuente: [Link](https://docs.docker.com/registry/)

No es mas que la forma de tener una especie de Docker Hub pero
en nuestro local, el cual este mismo lo podemos levantar en forma
de contenedor.

- Pa Levantar: `docker run -d -p 5000:5000 --name mi-registry -v $PWD/data/:/var/lib/registry/ registry:2`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 100. Sube tus imágenes!_

Vamos a aprender como subir y bajar nuestras imagenes de forma local  

**Objetivo:** Vamos a descargar una imagen del docker hub
la tageamos y la subimos a nuestro docker hub local

1, Descargamos de Docker Hub oficial: `docker pull hello-world`

2, Tageamos: `docker tag hello-world:mia localhost:5000/mi-hello-world`

3, Si nos fijamos en `sudo docker images` veremos que tenemos

```r
localhost:5000/hello-world   mia                 fce289e99eb9        9 months ago        1.84kB
```

4, Subimos nustra imagen: `docker push localhost:5000/hello-world`

5, Si nos fijamos en data/docker/registry/v2/repositories/ de nuestro host
veremos que esta registrado nuestra imagen que subimos

6, bajar la imagen de nustro local: `docker pull localhost:5000/hello-world:mia`

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### 101. Comparte las imágenes en tu red

Pendiente

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## CONFIGURACION DE PROXY, CONFIGURAR ACCESSO A LA INTRANET, AGREGAR UN PROXY, USO DE ARCHIVO .PAC

1. ir a Chrome -> Configuracion -> buscar: "proxy" -> Abrir configuracion la de proxy -> petaña Conexiones -> Configuracion de LAN

2. activar casillas "Detectar la conf. auto." "Usar scripts de configuracion automatica"  

3. El archivo `file:///./Otros/http-proxy.pac` copiarlo a la ruta `c:/WINDOWS/system32/drivers/etc/`

4. Poner en el campo  direccion la siguiente direccion smb://c:/path/file.pac mejor usar por `http://i.com/bcproxy.pac`

5. Reiniciar (Recomendado)  
N.NOTA: En mi caso no puedo acceder a internet y tener el cable al mismo tiempo, asi que manejarlos por separado, el internet y la intranet  
N.NOTA: Antes tenia la url, por si las moscas

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## Errores

### dockercompose TypeError You must specify a directory to build in path

**Error:** "dockercompose TypeError You must specify a directory to build in path"
**Descripcion:** Al ejecutar "docker-compose -f docker-compose-jenkiswdocker.yml up -d"
me da este error, con el siguinte contenido

```yml
version: '3.7'
services:
    jenkins:
      container_name: jenkinswdocker
      build:
        context: ./dockerfile-jenkinswdocker
      privileged: true
      restart: unless-stopped
      ports:
        - "8082:8080"
      volumes:
        - ./jenkins_home:/var/jenkins_home
        - /var/run/docker.sock:/var/run/docker.sock
```

**Causa:** El `context` solo sirve para indicar la ruda donde se encuentra
nuestro dockerfile, por lo que nos falta de definir el nombre del archivo
ya que en este caso es diferente.
**Solucion:** Definir el nombre de nuestro docker file

```yml
build:
  context: .
  dockerfile: dockerfile-jenkinswdocker
```

### /bin/sh: apt-get: not found

<!-- no puedo instalar, error al instalar, error en instalacion error -->

**Descripcion:** Al querer instalar un paquete en un contenedor alpine
da este error

```r
RUN apt-get install <PAQUETE>
```

**Causa:** Los contenedores alpine no cuentan con el apt-get
**Solucion:** Usar la sig. alternativa para instalar un paquete: `RUN apk add --update <PAQUETE>`

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

-->

## Por el camino

### Construir Jar

<!-- generar jar, construir jar,  -->

**Introduccion:** Tenemos dos maneras comunes de crear un Jar, uno donde
se usa un contenedor java/maven para crarlo, y que al finalizar quede
el jar. Y la otra es crear una imagen que esta contenga el jar el cual
primero se construlle y luego se ejecuta en el mismo dockerfile

A, Crear Jar con contenedor

A,1, Generamos el Jar en una sola linea

```r
sudo docker run --rm -v /root/.m2:/root/.m2 -v /home/mario/docker/pipeline/my-maven-app:/app -w /app maven:3-alpine mvn -B -DskipTests clean package

--rm                     # al final destrulle el contenedor
-v /root/.m2:/root/.m2   # aqui guarda las dependencias asi que las guardamos en un volumen pa no descargarlas cada vez
-v /home/...my-maven-app...:/app  # aqui guarda el .jar lo guardamos en el docker host
-w /app                  # indicamos que queremos estar el el directorio /app
maven:3-alpine           # indicamos que usaremos una imagen de maven version 3-alpine
mvn -B -DskipTests clean package  # ejecutamos el comando maven que contruira el .jar
```

A,2, Lo mismo pero con windows

```r
docker run --rm -v //c/Users/subject51/.m2://root/.m2 -v //e/cursos/0activ/sistema/docker/node/dos/app/://app -w //app maven:3-openjdk-11-slim mvn -B -DskipTests clean package
```

B, Crear jar y ejecutarlo con dockerfile

```dockerfile
FROM maven:3-openjdk-11-slim
ENV HOME=/home/usr/app
RUN mkdir -p $HOME
WORKDIR $HOME
ADD pom.xml $HOME
# downloading dependencies
RUN ["/usr/local/bin/mvn-entrypoint.sh", "mvn", "verify", "clean", "--fail-never"]
# 3. add all source code and start compiling
ADD . $HOME
RUN mvn package -DskipTests
EXPOSE 8080
RUN cp ./target/*.jar ./target/app.jar
CMD ["java", "-jar", "./target/app.jar"]
```

<!--

====================================================== divicion titulo

-->

----

<!--

====================================================== divicion titulo

-->

### Construir gatsby con docker NO RECOMENDADO

**problema:** Se tarda mucho en hacer la imagen y luego toma del cache
cuando no queremos que esto pase.

**Objetivo:** Crear un dockerfile para que nos sirva para crear una imagen
con los estaticos producidos por gatsby.

A, Agregamos `.gitignore` en la raiz del proyecto, para que no se copie
principalmente el node_modules

```r
node_modules
public
.dockerignore
Dockerfile
.cache
```

B, Creamos el Dockerfile en la raiz de nuestro proyecto

```dockerfile
FROM node:14.15.5-slim as node
ENV HOME=/usr/src/app
ENV CODE=./gatsby/code
WORKDIR $HOME
RUN yarn add global gatsby-cli
ENV PATH="/node_modules/.bin:${PATH}"
COPY $CODE/package*.json $HOME
COPY $CODE/yarn.lock $HOME
RUN yarn install
COPY $CODE $HOME
RUN yarn clean && yarn build

FROM nginx:1.18-alpine as server
EXPOSE 80
COPY --from=node /usr/src/app/public /usr/share/nginx/html
```

C, Crear imagen: `docker build --tag gatsby:1 .`

D, Levantar contenedor: `docker run --name cdmt_gatsby -p 80:80 gatsby:1`

**Resultado:** Tenemos una imagen que si ejecutamos tendra los estaticos
de nuestro sitio gatsby.

### Construir gatsby con docker RECOMENDADO

Podemos usar el siguiente Script el que da el mismo resultado que arriba,
solo que aqui tomara las dependencias del host, tal como si estubieramos
en desarrollo, lo cual lo hace mas rapido y facil.

```bash
#!/bin/bash
# NOTAS
# Este script se ha probado de la siguente manera "bash ./build_and_start.sh"
set -e

BUILD_GATSBY_CODE='cdmt_gatsby_prod_code_vol';
BUILDER_HOME='/usr/src/app/';
BUILDER_IMAGE_NAME='node:14.15.5-slim';
BUILD_COMAND='yarn build';

PORTS='80:80';
RUNNER_IMAGE_NAME='nginx:1.18-alpine';
RUNNER_CONTAINER_NAME='cdmt_prod_gatsby';
RUNNER_CONTAINER_HOME='/usr/share/nginx/html/';

# si esta corriendo el contenedor detenlo
if [ "$(docker container inspect -f '{{.State.Status}}' $RUNNER_CONTAINER_NAME)" == "running" ];
  then
    docker stop $RUNNER_CONTAINER_NAME
    echo "#########: $RUNNER_CONTAINER_NAME Stopped"
  else
    echo "#########: There is no container $RUNNER_CONTAINER_NAME running to stop"
fi

# Construir app
echo "#########: Build...";
docker run --name ${RUNNER_CONTAINER_NAME}_builder \
  --rm \
  -v $BUILD_GATSBY_CODE:$BUILDER_HOME \
  -w /$BUILDER_HOME/code \
  $BUILDER_IMAGE_NAME \
  $BUILD_COMAND
echo "#########: Gatsby Builded \n"

# Levantar App
echo "#########: Starting..."
docker run --name $RUNNER_CONTAINER_NAME \
  -d --rm \
  -p $PORTS \
  -v ${BUILD_GATSBY_CODE}:/etc/nginx/conf.d:ro \
  -v ${BUILD_GATSBY_CODE}:$RUNNER_CONTAINER_HOME:ro \
  $RUNNER_IMAGE_NAME
echo "#########: $RUNNER_CONTAINER_NAME instantiated"
```
