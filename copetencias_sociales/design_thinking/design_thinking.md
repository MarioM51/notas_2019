# Design Thinking

**Que es,** Una forma de inovar centrada en las personnas.  
**Problema**, cuando tenemos que crear una solucion orientada al cliente,
muchas veces no sabemos como empezar y/o abordar el problema, para generar
una solucion.  
**Solucion**, podemos implementar la estrategia de Design Thinking la
cual tiene diversos roles, etapas, tecnicas, y la cual esta orienda a
personas.  
**Ejemplo:**, Una empreza tenia probleas ya que vajaron sus ventas, y
al querer solucionarlo no podian definir su problema en un parrafo.

## Introduccion

### Problemas perversos

Se denominan asi a los problemas que son

- Imposibles de predecir y dificiles de cuantificar: Por ejemplo, crear
  un plan de evacuacion de emergencia, siempre tendremos problemas a
  la hora de su ejecucion.
- Diversidad de actores: aveces algunas soluciones que venefician a algunos
  pergidican a otros.
- Son Cambiantes y varialbes devido al entorno: ej. una solucion para
  un problema de trafico funcionara en mexico pero no en eu.

Es aqui donde Design Thinking aporta estructura y metodo, a las personas
que lidian con problemas perversos.

### Otras metodologias para problemas perversos

#### Inovacion abierta

Es una forma agil de incorporar el potencial de ideas externas en una
compania, para crear un proceso de inovacion mas ricos, donde se tiene
el problema que el conocimiento esta cada vez mas disperso, y las
oportunidades deben de generarse con prontitud para que no se pierdan.  

Permitiendo combinar el proceso de inovacion de una StartUp a una gran
empresa, es decir, poder adoptar conocimiento externo en conjuncion con
el conocimiento especializado.

#### User Driven Inovation

"Inovacion dirigida por el usuario", Contradice la idea de que las
grandes inovaciones salen de las empresas, diciendo que estas sale de
los usuarios. En la cual se requieren `early adopters` que pueden tener
acceso s prototipos para que ellos perfilar sus cualidades y atributos
al producto final.

### Otras aportaciones del Design Thinking

La filosofia del Design Thinking a llegado a otras metodologias como lo
son SCRUM, y lean los cuales trabajan juntos, las cuales una de las ideas
centrales son los prototipos y testeo rapido.

`Una forma de saber si una empresa a implementado el Design Thinking`
podremos reconocer los distintos niveles.

- Nivel 1: `Productores`: Es el mas basico, donde se mejoran aspectos
  visuales y de eficiencia, los mismos roles que crean el producto.
- Nivel 2: `Conectores`: Participan diferentes roles de la empresa, para
  mejorar el producto.
- Nivel 2: `Arquitectos`: Aqui se tienen diferentes etapas donde en cada
  una de estas se prueba el producto/prototipo.
- Nivel 2: `Cientificos`: Aqui ya una metodologia de diseño, que incluye
  la experimentacion con estapas de hipotesis, validacion e iteraciones.
- Nivel 2: `Visionarios`: Aqui el diseño y el negocio estan completamente
  fucionados, donde las deciciones de negocio estgan intimamente vinculadas
  con los resultados de diseño, que trabajaron con usuarios e iteraciones.

### El equipo Design Thinking un grupo multidisciplinario

Entre mas ojos entrenados de diferentes formas mejores ideas tendremos,
lo ideal es combinar personas de los siguientes ambitos.

- Area de negocio
- Profecionales del diseño
- Personas de conocimiento tecnico

El numero de personas en un equipo no es estricto, pero se recomienda
no mas de 8 personas, para no tener problemas de comunicacion.

`Etapas y equipos`: es recomendable que primero se tengan las personas
de investigacion, diseño y negocio para la `etapa de definicion`, y luego
se tengan personas de perfil tecnico y especializado para la `etapa de`
`prototipado`.

Tambien es interesante tener personas que vayan saliendo y entrando del
proyecto.

Es necesario que el equipo tenga autonomia y recursos suficientes como
tambien limites de tiempo y requerimientos de calidad.

### Sin facilitador no hay Design Thinking

Este rol es muy importante, ya que es el que debe de entender bien los
objetivos, velando para que el equipo siempre sepa que es lo que hay que
hacer y como debe de hacerlo.

Entre sus actividades estan.

- `Crear el route map`: Este documento debe de especificar que se va
  ha hacer en cada una de las fases.
- `Calendario`: Debe de velar para ver que cada etapa se termine en el
  tiempo establecido.
- `Organizar la agenda`: Definir reuniones, seciones de trabajo,
  apartar las los espacios, etc.
- `Preparar los materiales`: ej, cartulinas, proyectores, computo,
  guines de dinamicas.
- `Preparar las dinamicas y guiones de trabajo`: Las reuniones y sessiones
  de trabajo deben de tener un resultado, y si no lo tienen un plan de
  contingencia.
- `Garantizar que el objeivo se cumple`:

### Briefing: Alcance y objetivo

Antes de empezar debemos de saber que tenemos lo necesario para iniciar
el proyecto, entre lo cual esta lo siguiente.

- Origen del proyecto y motivaciones: Es interesante investigar si ya
  hubo proyectos similares para apoyarse en estos.
- Desidores del Proyecto: A quienes se reportaran los avanses
- Recursos a disposicion del equipo.
- Output que se espera de cada quien y de cada etapa, tambien es valioso
  contar con metricas de exito.

### Reglas de comunicacion y herramientas

Podemos caer en el que parece que hacemos design thinking pero si no
cumplimos con las disiplinas, no tendremos los veneficios de esta. una
de estas disiplinas es la comunicacion.

- `Separar momentos de divergencia y Convergencia`: Entiendase divergencia
  como las personas o las etapas de generar ideas, y la Convergencia como
  las que evaluan y seleccionan ideas.
- `No enamorarse de una idea`: No hay que cerrarse a cambios, o a descartar
  una idea por otra, y debemos de hacer sentir comodos a los demas a
  hacer aportaciones a nuestras ideas.
- `Frases matadoras`: tales como; no se puede, ya lo intentamos, esa idea
  no la vamos a hacer, ya que intimidan la coperacion y al proceso divergente.
- `Escucha activa`: Saber que estamos entiendiendo a todos, y no caer
  en una conversacion repetitiva.
- `Construye en equipo`: Tener en cuenta que alguien mas puede construir
  sus ideas a partir de la d alguien mas.
- `Post-it`: Estos deben ser auto explicativos, lo cual podemos usar;
  frases completas (sujeto, verbo y predicado), ejemplos.
- `Documentar`: Podemos crear resumenes de una sesiones y fotos.
- `Limitar el tiempo`: Establecer limiter bien hechos y comunicados, da
  una precion positiva sobre el equipo.

### Gestion de espectativas

Debemos entender bien que es lo que esperan los usuarios o stakeholders,
y que estos sean bien comunicados en el equipo.

### Pasos a dar en el Design Thinking

- ¿Que queremos aprender?
- ¿Cuales van a ser las fuentes de informacion?
- ¿En que contexto obtendremos dicha informacion?
- ¿Que tecnicas usaremos para obtener la informacion?
