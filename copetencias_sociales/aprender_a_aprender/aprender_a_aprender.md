# Aprendiendo a aprender

- Link: https://www.coursera.org/learn/aprendiendo-a-aprender  

## Indice

- [Aprendiendo a aprender](#aprendiendo-a-aprender)
  - [Indice](#indice)
  - [Introduccion](#introduccion)
  - [Uso de los modos Enfocado y Difuso... O bien, algo de Dalí te vendrá bien](#uso-de-los-modos-enfocado-y-difuso-o-bien-algo-de-dalí-te-vendrá-bien)
  - [¿Qué es el aprendizaje?](#qué-es-el-aprendizaje)
  - [Sinopsis de la procrastinación](#sinopsis-de-la-procrastinación)
  - [La práctica lo hace permanente](#la-práctica-lo-hace-permanente)
  - [Introducción a la memoria](#introducción-a-la-memoria)
  - [La importancia del sueño en el aprendizaje](#la-importancia-del-sueño-en-el-aprendizaje)
  - [Entrevista al Dr. Terrence Sejnowski](#entrevista-al-dr-terrence-sejnowski)
  - [fragmentación](#fragmentación)
    - [Que es un fragmento](#que-es-un-fragmento)
    - [Cómo formar un fragmento](#cómo-formar-un-fragmento)
  - [Ilusiones de competencia](#ilusiones-de-competencia)
  - [¿Qué te motiva?](#qué-te-motiva)
    - [Como tener motivacion](#como-tener-motivacion)
  - [El valor de una biblioteca de fragmentos](#el-valor-de-una-biblioteca-de-fragmentos)
  - [Sobreaprendizaje, ahogo, Einstellung e intercalado](#sobreaprendizaje-ahogo-einstellung-e-intercalado)
  - [Lecturas recomendadas](#lecturas-recomendadas)
    - [Habito de los 30seg](#habito-de-los-30seg)
    - [Como ser mas afortunado](#como-ser-mas-afortunado)
    - [El poder de hacer garabatos](#el-poder-de-hacer-garabatos)
  - [Semana 3 - Procrastinacion](#semana-3---procrastinacion)
    - [Introducción a la procrastinación y la memoria](#introducción-a-la-procrastinación-y-la-memoria)
    - [Ocuparse de la procrastinación: Es más fácil y valioso de lo que parece](#ocuparse-de-la-procrastinación-es-más-fácil-y-valioso-de-lo-que-parece)
    - [Zombis en todas partes](#zombis-en-todas-partes)
    - [Surf: Proceso versus producto](#surf-proceso-versus-producto)
    - [Aprovechar a tus zombis para que te ayuden](#aprovechar-a-tus-zombis-para-que-te-ayuden)
    - [Malabares con la vida y el aprendizaje](#malabares-con-la-vida-y-el-aprendizaje)
  - [Memoria](#memoria)
    - [Profundizar en la memoria](#profundizar-en-la-memoria)
    - [¿Qué es la memoria de largo plazo?](#qué-es-la-memoria-de-largo-plazo)
    - [Creación de grupos significativos y la técnica del palacio de la memoria](#creación-de-grupos-significativos-y-la-técnica-del-palacio-de-la-memoria)
  - [Semana 4 - Renacimiento del aprendizaje](#semana-4---renacimiento-del-aprendizaje)
    - [Cómo ser un mejor estudiante](#cómo-ser-un-mejor-estudiante)
    - [Introducción al Renacimiento del Aprendizaje y cómo liberar tu potencial](#introducción-al-renacimiento-del-aprendizaje-y-cómo-liberar-tu-potencial)
    - [Crear una metáfora o analogía visual vívida](#crear-una-metáfora-o-analogía-visual-vívida)
    - [No se necesita la envidia del genio](#no-se-necesita-la-envidia-del-genio)
    - [Cambia tus pensamientos, cambia tu vida](#cambia-tus-pensamientos-cambia-tu-vida)
  - [Semana 4 - Como Liberar tu potencial](#semana-4---como-liberar-tu-potencial)
    - [El valor del trabajo en equipo](#el-valor-del-trabajo-en-equipo)
    - [Lista de comprobación de las pruebas (ChekLists)](#lista-de-comprobación-de-las-pruebas-cheklists)
    - [Comienzo difícil: Un salto a una continuación fácil](#comienzo-difícil-un-salto-a-una-continuación-fácil)
    - [Instrucciones útiles finales para pruebas](#instrucciones-útiles-finales-para-pruebas)
    - [Resumen](#resumen)
  - [Por el camino](#por-el-camino)
    - [Accesos rapidos](#accesos-rapidos)
    - [Aprender idiomas](#aprender-idiomas)
      - [Consejos de Polyglot Grace](#consejos-de-polyglot-grace)
      - [Mejora tu pronunciacion con el metodo de SHADOWING](#mejora-tu-pronunciacion-con-el-metodo-de-shadowing)

<div class="page" />

## Introduccion

Cuando no entendemos algo los zombies siguien azotando la cabeza la pared
una y otra vez asta que logran algo.  

Hay 2 formas de aprendisaje el **Enfocado y difuso** estamos mas
familirizados con el enfocado en el cual le dedicamos tiempo solamente
a algo que queremos entender, y el difuzo es funciona como el juego de
pinball, en el cual una pelotita choca contra varios cojines y entre
cojines pegue la pelotita mas puntos ganaremos.  

![](./img/1_pensamiento_difuso_y_enfocado.PNG)

Como podemos ver en la imagen cuando estamos concentrados en un tema
nuestra linea de pensamiento esta concentrada en una zona en concreto
y cuando estamos en la forma difusa la linea de pensamiento recorre mas
zonas.  
Hasta donde entedemos no podemos estar en un modo difuso y enfocado al mismo
tiempo.

**Nota** Las analogias y metaforas son una poderoza herramienta para aprender,
en este caso usamos reprecentamos el cerebro como una maquina de pinball, y
nuestra linea de pensamientos como la pelotita que rebota, y los diferentes
cojines como las ideas.  

Ya que esto ayuda a simplificar o transportar la idea a algo con lo que estamos
familiarizados.  

![](./img/15_usar_metaforas.PNG)

<div class="page" />

## Uso de los modos Enfocado y Difuso... O bien, algo de Dalí te vendrá bien

**Ejemplo de uso de pensamiento difuso** lo que hacia dali era sentarse comodo
con *una llave en la mano* pensando libremente, y cuando estaba a punto de
quedarse dormido, esto probocaba que tirara la llave y al caer esta hacia ruido,
lo cual lo despertaba y este empezaba a trabajar en lo que habia estado
concentrado anteriormente, Segun leyendas *Edison hacia lo mismo* con un rodamiento.  

**En resumen** lo recomendable es pensar en modo difuso para nuevas ideas,
y luego en modo concentrado para realizar las ideas, para que **adoptemos**
nosotros esto es poco a poco.

## ¿Qué es el aprendizaje?

Escaneos del cerebro muestran que cuando estamos en forma de repozo o en alguna
actividad, se activan y desactivamn zonas concretas del cerebro segun lo que
estemos haciendo, lo que significa que nuestros cerebros estan constantemente
activando y desactivando diferentes areas.  

Tambien se observo una neurona viva cuando un animal no habia aprendido nada,
cuando aprendio algo, y despues de dormir, y en las 3 etapas hubo cambios en la
neurona.  

Esto ilustra que cuando nos dormimos, al despertar nuestro cerebro modfico su
estructura. Este tipo de datos estan en **brainfacts.org**

## Sinopsis de la procrastinación

Cuando queremos hacer algo que comunmente procrastinamos, lo que sucede es que
ocurre se activan areas de nuestro cerebro relacionadas con el dolor, no
provocando dolor pero si un neuromalestar, y naturalmente nuestro cerebro evita
estas tareas, dirigiendo la atencion a otro lugar.  

Las investigaciones indican que cuando comenzamos la tarea que procrastinamos
al poco tiempo de hacerla este neuromalestar desaparece

![](./img/02_como_funciona_la_procrastinacion.PNG)

Aqui podemos ver como nos viene la idea de "que fastidio" antes de hacer una
tarea, dirigimos nuestra atencion a algo mas placentero, y obtenemos placer
de forma facil/rapida.  

**Consejo** Usar la estrategia de **pomodoro** esto significa quitar todas las 
distracciones, poner un cronometro para concentrarnos durante 25min y cuando 
termine darnos algo de placer, un chocolate, camita etc.

## La práctica lo hace permanente

Comunmente odiamos las metematicas por su naturaleza abstracta de sus ideas o 
conceptos, por ejemplo, al aprender que es una buo podemos asociarlo con una 
imagen un sonido, una imagen, incluso el sonido de la palabra con el sonido 
que este hace, incluso otros temas igual de abstractos como lo es el amor, 
esperanza, entusiamo podemos relacionarlo, por que aun que no las podamos ver 
o tocar las podemos sentir.  

Pero con las matematicas esto comunmente no sucede, no hay nada analogo a lo 
que se pueda relacionar, es por eso que en estos casos sea importante la 
practica para que estas ideas las podamos interpretar, y en este punto aun que 
la idea sea totalmente abstracto, el patron de pensamiento para poderlo aplicar 
se combierte en algo concreto.

![](./img/03_practica_permanencia.PNG)

Por ejemplo, cuando empezamos a lidiar con un nuevo concepto este patron de 
pensamiento se graba debilmente, como podemos ver la linea mas tenue, lo cual 
implica que aun no tenemos la idea completa, luego mejoramos obtenemos la idea 
completa, y **al final interiorizamos** la misma idea con descanzo y pensamiento 
difuso y practica tenemos la idea mas firme.   
Es **importante** interiorizar una idea descanzando o pensando en ella de forma 
difuza, ya que esto de alguna manera hace la idea se concrete.   

## Introducción a la memoria
Tenemos dos tipos de **memoria la de largo plazo y memoria de trabajo** en el 
cual para pensar en algo traemos recurdor de memoria a largo plazo, investigaciones 
indican que podemos trabajar hasta con cuatro fragmentos de memoria, lo cual no 
es mucho. **por ejemplo** para anotar un numero de telefono, nos lo dicen y 
lo repetimos para que no se nos olvide asta que lo podamos anotar.   

**Repeticion espaciada**. Para que se nos quede algo de en la memoria de 
largo plazo necesitaremos repasarlo varias veces en un periodo de x dias, ya 
que si lo repetimos varias veces el mismo dia sera menos probable que se nos 
quede, por ejemplo recordarlo/practicarlo 1 vez al dia al principio, y de ahi 
una vez a la semana y asi sucesivamente.


## La importancia del sueño en el aprendizaje
Cuando estamos despiertos se generan toxinas, las cuales empeoran nuestro 
rendimiento, y cuando dormimos estas toxinas desaparecen, como podemos ver 
abajo, asi que no dormir o dormir poco por donde le veamos es una mala idea.   

![](./img/04_suenio_toxinas.gif)

Tambien el sueño ayuda con el proceso de comprencion, incluso al dormir el 
cerebro relaciona lo aprendido con otras cosas y busca la solucion del problema.

## Entrevista al Dr. Terrence Sejnowski
Este doctor es un neurocientifico computacional muy cabron.

- No solo se trata de leer un monton si no de practicar lo leido.   

- Aprendes mas en una tarea activa que escuchando pasivamente.   

- Se entra en un estado difuso de una mejor manera cuando uno hace algo, 
como lo es el correr, bañarse, mas si se esta afuera.   

- Muchas veces no se puede evitar caer en tareas multiples, ya que es 
importante antender a las personas por voz, mail, msj de texto, anotar y 
escucha, etc. pero es **importante** tener en cuenta que no es hacer cosas 
al mismo tiempo si no ir intercambiando las actividades.    
El doctor cree que con la calma de la noche es cuando es mejor en su trabajo

- Ha descubierto que tatones que tienen un **ambiente comodo y social positivo** 
esta aprendera mucho mas rapido que unaque esta en un ambiente empobresido.   

- El exito no se alcanza siendo inteligente, muchas personas inteligentes no 
tienen exito, el doctor conoce personas exitosas que su factor comun es que son 
**apacionadas y persistentes** de lo que hacen.   

- Francis Crick premio novel por estudiar el cerebro creyo por muchos años 
que el cerebro de plastico que tenia en su oficina estaba a escala (que no 
era asi), hasta que alguien le dijo lo contrario, **metafora** descubres 
muchas cosas saliendo de tu rutina y/o aceptando la perpectiva de los demas.   

## fragmentación
Con fragmentos nos referimos a paquetes de informacion que nuestro cerebro 
puede acceder facilmente 

### Que es un fragmento
Los fragmentos son como rompecabesas en los cuales es dificil recordar un 
solo fragmento, pero cuando estos estan ordenas e interconectados es mas 
facil recordar un fragmento, ya que este conjunto forma una imagen general.

Y estas imagenes generales tambien pueden ser fragmentos de informacion los 
cuales evocan a todos estos pequeños fragmentos, por ejemplo, **verstirse** z
(imagen general) uno ya no piensa en los pasos que con lleva, como el ponerse 
calcetines pantalon, zapatos, playera etc. 


### Cómo formar un fragmento
**PRIMER PASO CONCETRACION:** Como mensionamos tenemos solo unas cuatro 
ranuras de informacion para nuestra memoria de trabajo, y como formaremos 
un nuevo fragmento si una de estas ranuras esta ocupada chateando, viendo 
publicaciones, 

**Por ejmplo** Antes de querer aprender futbol uno va a la cancha de futbol, 
con el instructor para ponerle atencion, y no lo haremos almenos optimamente, 
si estamos fuera en un lugar que no se puede jugar futbol, no prestarle 
atencion al intructor, etc.


**SEGUNDO PASO COMPRENCION DE IDEA BASICA** Aqui es donde sentamos las bases, 
de lo que queremos hacer, por ejemplo, viendo como se hace, por medio de de 
ejemplos ya resueltos o indicarnos cuales son los pasos.   

La formacion de fragmentos es un poco diferente en cada area, no es lo mismo 
generar un fragmento, para un problema matematico, que para una leccion de 
futbol.   

En ambos casos vemos y/o nos explican como se hace, ya sea viendo como se 
hace el movimiento de *futbol* con alguien mas o con nosotros mientras nos explica 
los pasos, o el problema *matematico*, nos sientas las bases de cuales son las 
pautas para que podamos iniciar el problema y cual es el resultado esperado, mas 
los pasos que conlleba resolverlo.   

**Por ejemplo**, al aprender futbol uno aprende pequeñas cosas como.   
- pater un balon, para un pase o un gol.   
- correr con el balon.   
- quitarle el balon a alguien   
- evitar que te quiten el balon   
- reglas como cuando es un saque de esquina o fuera de areas   
- etc   

Al aprender todas estas cosas se concidera que ya sabes futbol, y que hay 
diferentes rangos de saber, donde.   

- sabes como es pero aun te falla.   
- sabes pero aun lo sobre-piensas.   
- sin pensar reaccionas de una forma correcta.   

**Ejemplo 2** Al querer aprender una ecuacion el profesor resolvera una 
ecuacion en el pisarron explicando que esta aciendo por emdio de los pasos 
como.   
- La exprecion debe deter dos partes la exprecion con una incognita y 
  una igualdad.   
- Buscamos limpiar la incognita pasando los numeros con su operacion 
  inversa.   
- Realizamos la operacion y la incognita debe quedar resuelta.   

**TERCER PASO CONTEXTO** es entender el contexto para que no solo veas como usar 
el fragmento, tambien **cuando usarlo** que comunmente se obtiene practicando.   

No ahi que confundir el **proceso de comprencion con la experiencia solida**, 
osea no es lo mismo entender por que un problema en especifico se resuelve 
explicado por tu profesor y repetido por ti, a saber como se solucionan ese 
tipo de problemas, 

> Solo haciendolo tu mismo en repetidas ocaciones asegurara que 
> realmente aprendiste

**Ejemplo** En el caso de aplicar la ecuacion tenemos ecuaciones con 
diferentes operaciones, en este caso debemos ir mas alla del problema que 
nos explico el maestro que fue de suma y paso a restar, a nosotros hacer 
una ecuacion con una operacion de multiplicar y que la aplicquemos su 
inversa de dividir.   

**Ejemplo 2**, arriba trabajamos con un sub-fragmento de las ecuaciones que 
es operaciones y aplicar su inversa, pero igual podemos aprender a como pasar 
una frase en lenguaje natural a una ecuacion, por ejemplo, "nos dieron 20 
manzanas y nos cobraron 10 pesos" ser capasas a transformar la frase a esto 
"20m = 10p" -> "m = 10p/20" -> "m = 0.5"

> Si sabemos como aplicar algo pero no sabemos en que casoa y/o cuando 
> vamos a aplicarlo entonces, es menos probable que lo recordemos.   

En otras palabras puede que tengamos una nueva herramienta en nuestra caja de 
herramientas, pero si no sabes cuando vas a usarla entoces no te va a sevir de 
mucho.   

**Recordar** que un fragmento comunmente tiene 2 caminos uno mas general y 
otro mas espesifico, o imagen general y parte de la imagen, y tener la imagen 
general puede ser mas importante por que nos ayuda a acceder a varios fragmentos 
como vemos abajo.   

![](./img/05_imagen_general_evoca_varias_partes_de_imagen.PNG)


Tambien en el caso de que nos esten dando la informacion de forma de pequeños 
fragmentos podemos alejarnas un poco ya sea viendo cuales son las partes 
importantes de cada fragmento o pensar de una forma difusa p para deducir su 
contexto.

![](./img/06_imagen_general_vs_partes_de_imagen.PNG)

como vemos en la imagen, primero vemos en los fragmentos un hombre dos 
ruedas, un espejo, una defensa etc, lo cual al pensarlo deducimos que 
es la imagen general de alguien en un carro.   

**RESUMEN**
- Hay un proceso de fragmentación que va de abajo hacia arriba, en el cual 
  practicar y repetir te puede ayudar a construir y fortalecer cada fragmento, 
  de modo que puedas acceder a él siempre que lo necesites.   

- Hay un proceso de “panorama general” que va de arriba hacia abajo y te 
  permite ver dónde encaja lo que estás aprendiendo. 

- Tanto los procesos de fragmentación que van de abajo hacia arriba como los 
  procesos de "panorama general" que van de arriba hacia abajo son vitales para 
  dominar el material. 

- La fragmentación puede implicar que aprendas cómo usar una cierta técnica 
  para la solución de problemas. El contexto significa aprender cuándo usar 
  esa técnica y no alguna otra.


## Ilusiones de competencia
Hay partes escenciales para organizar el aprendizage, el cual usaremos tecnicas 
como
- Recordar.   
- Ilucion de copetencia.   
- Mini-examenes.   
- Valor de cometer errores.   

**Recordar** Habeses uno lo que hace al leer un libro e intentar que esto se nos 
quede, es re-leer el capitulo, pero es mas efectivo **leer un parrafo mirar a otro lado** 
y ver que tanto recordamos. Otra forma efectiva de recordar es **practicar** lo 
que dice, y otra es la creacion de **diagramas al finañl** ya que entendiste los 
diferentes conceptos del tema

**Ilucion de copetencia** Es comun que solo al entender como se hace algo, 
creamos que nosotros podemos hacerlo, lo unico que corrabora que nosotros 
podemos es hacer algo, es hacerlo nosotros mismos, tambien ocurre que como 
**creemos que tenemos la informacion a la mano (google)** significa que ya no lo 
tenemos que repasar, ya que al ver la informacion hay y recordarla, no es 
lo mismo que podamos hacerlo.   


![](./img/14_teoria_practica.PNG)

**Valor de cometer errores**: Como comentabamos arriba puede que aprendimos 
algo y encontramos la forma de no recordarla pero si ubicarla, pero con el 
tiempo lo olvidamos y tenemos la ilucion de que lo sabemos, aqui entran los 
**mini-examenes** que nos permiten re-evaluar si en realidad sabemos algo.   

## ¿Qué te motiva?
**Acetilcolina** (molecula de concentracion) Al poner atencion en algun tema 
empieza a crearse, y ayuda a crear nuevas interconecciones en nuestro cerebro 
osea ayuda directamnte al aprendisaje y a la memoria.   

**Dopamina** (molecula del placer) es la forma de recompenza del cerebro 
indicando que la tarea que se esta haciendo se motive a repetirse ya que 
esta nos dara placer ya sea en el momento o despues, pero se puede causar 
resistencia a esta y tambien o por ende adiccion.   

**Serotonina** (molecula de la feicidad) entre mas deprimidos estemos tendremos 
menor cantidad de serotonina, y un comportamiento de mayor riesgo, por ejemplo 
los criminales precentan una cantidad baja de serotonina, tambien se a decubierto 
que tenerla baja afecta nuestro aprendisaje y memoria

**Concluciones** Estudia lo que te gusta por que existe evidencia cientifica que 
te costara mas trabajo aprender, y se busca ser feliz con aspectos como sociales 
motivantes, buscar un objetivo que quieras, etc ya que igual al no tenerlos 
afectara tu rendimiento academico.   

### Como tener motivacion

**Objetivos S.M.A.R.T.**: Es una forma de motivarnos, si tienen estas caracteristicas 
nuestros avances seran mas claros, y nuestras recompenzas mas seguidas.   

![](./img/16_objetivos_smart.PNG)

- **Specific**: Es imprescindible que los objetivos fijados sean **concretos**. 
  Por ejemplo: Aumentar el número de followers de nuestra cuenta de Facebook.   

- **Measurable**: Si no somos capaces de medir el impacto del objetivo fijado, difícilmente 
  podremos saber si nuestra acciónes han tenido éxito una vez finalizada. Es decir, que 
  debemos hacer de nuestro objetivo una meta cuantificable. Por ejemplo: **Aumentar 10%** 
  el número de followers de nuestra cuenta de Facebook.   

- **Attainable=Alcanzable**: Hemos de fijar objetivos atendiendo a las probabilidades, 
  estadísticas y referencias a nuestra disposición. Sin dejar de ser ambiciosos, 
  nuestras metas no deben ser **desproporcionadas**, ya que ello puede llevarnos a la 
  frustración con facilidad. Por ejemplo: Si nuestro número de followers ha aumentado 
  entre un 2% y un 3% durante las mismas fechas en los últimos cinco años, no podemos 
  fijarnos como objetivo que durante esta campaña crezca un 10% pero si un 4%.   

- **Realist**: Aunque es muy similar a la característica anterior, el matiz que ésta 
  aporta es que debemos atender a los recursos de los que dispone nuestra empresa y 
  que puede poner al servicio de un objetivo en particular. Por ejemplo: Si nuestro 
  número de followers ha aumentado entre un 2% y un 3% durante las mismas fechas en 
  los últimos cinco años, y durante la presente campaña tenemos menos presupuesto que 
  la temporada anterior, no podemos fijarnos como objetivo que nuestro crecimiento 
  sea de un 10%.

- **Timely**: Esta característica hace referencia a los **plazos concretos** dentro 
  de los cuales deberemos cumplir el objetivo fijado. Por ejemplo: Aumentar el número 
  de followers de nuestra cuenta de Facebook en un 10% antes de la primera semana de enero.

**Scott Young**, Un master del estudio dedica 4hr de estudio algo que puede llegar 
a ser contraintuitivo, pero esto le permite concentrarse en lo importante y 
descanzar para poder concretar el conocimiento, pero estas horas las hace intensas


## El valor de una biblioteca de fragmentos
Como mensionamos un fragmento que no sabemos interconectarlo o en otras palabras
no sabemos cuando usarlo, o para que nos sirve, entonces lo mas probable es que 
olvidemos este fragmento. 

**Transferencia** Es cuando los fragmentos pueden ayudar a comprender nuevos 
conceptos el cual es equivalente a apartir de fregmentos de diferentes temas 
crear conexiones por nuestra cuenta y darnos cuenta de algo, por ejemplo, cuando 
relaciono **Steve Jobs** los celulares con la tecnologia touch y las computadoras.    

![](./img/08_transferencia.PNG)

Sin envargo cuando tenemos una gran cantidad de estos y empezamos a aprender 
algo nuevo, nosotros solos podemos empezar a encontrerle interconexiones, pero 
esto pasa cuando ya hemos practicado 

Tenemos 2 formas de resolver un problema **secuencial e holistico**   
- Secuencial: es el una serie de pasos uno tras otro que nos dirijen a una 
  solucion.
  
- Holistico: Este usamos una serie de fragmentos relacionados y por medio 
  de nuestra experiencia los interconectamos para dar una solucion.   

Comunmente (no siempre) usamos el modo holistico para solucionar problemas 
mas complejos y el modo secuencial para problemas mas simples.   

**Conclucion:** El conocimiento puede tener sinercia osea que entre mas sabemos 
podemos aprender mas rapido, pero es importante tener los fragmentos con su 
respectivo contexto, para saber interconectar fragmentos cada vez mas rapido y 
en mayor cantidad y en una de esas descubrir algo nuevo, o por nuestra popia 
cuenta.   



## Sobreaprendizaje, ahogo, Einstellung e intercalado
**Para que sirve** puede ser util para el perfeccionismo como una precentacion, 
y/o enfrentarce a situaciones de nerviosismo un concierto de piano, por ejemplo 
los precentadores de TED llegan a practicar hasta 70hr para una charla tipica 
de 20min.   

**Desventajas** estudios demuestran que asta cierto punto es un desperdicio de 
tiempo, ya que esta informacion ya paso a tu memoria de largo plazo y no pasara 
de ahi, tambien dara la **ilucion** de expertismo, mientras que solo estas 
dominando una fraccion del tema, y lo que se **sobre-estudia comunmente es lo facil** 

**Ejemplo desventaja** es similar a aprender carpintería aprendiendo solo como 
martillar, y después de un tiempo, **pensarás que todo se arregla a martillazos**.   

![](./img/12_aprendido_util_sobreaprendido_estorbo.PNG)

**Practica Deliberada** Esto significa estudiar lo que mas se te dificulta de un 
tema, lo que hace la diferencia entre un buen estudiante y un exelente estudiante

**Einstellung** Este concepto es que con un simple pensamiento inicial, idea, etc 
que evita que encuentres la mejor solucion, es como un bache o una desviacion que 
uno toma sin querer, que hace que uno piense en lo mismo cada vez, como se puede 
ver en la imagen, la solucion esta abajo, pero nuestro **Einstellung** hace que 
vayamos a la solucion que no corresponde.   

![](./img/09_Einstellung.PNG)

**Saltar al agua sin saber nadar** es un error comun empezar a trabajar un tema 
sin tener bien los fundamentos de este, por ejemplo, al aprender a ir en 
bicicleta, primero usa las ruedas de ayuda.   

![](./img/11_primero_fundamentos.PNG)

**Intercalar** Significa tener saber de varias disiplinas y poder usar estas 
enm diferentes campos, por ejemplo, premios noveles se inspiraron de lugares 
que no tenian que ver con su campo.   

![](./img/10_intercalar.PNG)


## Lecturas recomendadas

### Habito de los 30seg 
Al final de una secion de lectura, estudio, reunion 
etc, tomate 30seg (no mas, no menos) para anotar lo mas relevante, no es 
tomar nota es *Es un acto de interpretación, priorización y toma de decisiones.*   
El uso de 30seg uno evitara los detalles y evitara anotar de mas 


### Como ser mas afortunado
Comunmente los desafortunados son mas tensos que los afortunados.   

Investigaciones muestran que la **anciedad** interrumpe la habilidad de 
descubrir lo **inesperado**.   

**Experimentaron** con 2 grupos de personas, donde debian de encontrar 
puntos al centro de una pantalla que aparecian sin previo aviso, 
al grupo A se les daba dinero si no los encontraba crendo mas 
ancidad, y el B no ocurria nada, pero tambien aparecian puntos en 
los bordes de la pantalla, lo que vieron que el grupo A no se daban 
cuenta de estos puntos.   

![](./img/13_los_afortunados_son_positivos.PNG)


La investigación reveló que las personas afortunadas generan buena 
fortuna a través de cuatro principios básicos. 
 1. Son hábiles para crear y notar oportunidades.
 2. tome decisiones afortunadas escuchando su intuición. 
 3. Crea profecías autocumplidas a través de expectativas positivas. 
 4. adoptar una actitud resistente que transforme la mala suerte en buena. 

Las personas afortunadas hacen caso a como se sienten ante varios aspectos 
incluido el racional, mientras que los desafortunados, llevan nada mas los 
racional.   

Desafortunados suelen ser rutinarios, hablan con las mismas personas toman 
el mismo camino, los afortunados al contrario llevan mayor variedad de cosas 
a sus vidas.   

Ven el lado positivo de algo, por ejemplo, un afortunado se rompio la pierna 
al caer de las escaleras, y aun decia que tuvo suerte por que no se rompio el 
cuello.   

### El poder de hacer garabatos
Se a mostrado que hacer garabatos puede mejorar la capasidad de aprendizaje, 
ya que al garabatear el cerebro le es mas facil procesar informacion compleja, 
y de estabilizar emociones complejas (equivalente a quitar el estres) dejando 
espacio para la atencion a x temas que incluso mejora la capasidad de recordar.   



## Semana 3 - Procrastinacion 

### Introducción a la procrastinación y la memoria
La procrastinacion y la memoria a largo plazo estan relacionados, esto por que 
crear fragmentos solidos de largo plazo, facilmente memoria de trabajo toma tiempo.   

Como explicamos la procrastinacion tiene el siguiente proceso, 1) Al pensar en la 
responsabilidad se produce un neuro-malestar 2) Tomamos el habito de pro-crastinar
que la tenemos como **respuesta automatica**, 3) dirigimos nuestra atencion a algo 
mas placenteto a corto plazo.   

![](./img/02_como_funciona_la_procrastinacion.PNG)

Estas caracteristicas **son similares a las de una adiccion** por lo que entre mas 
le hagamos caso a dirigirnos a la tarea mas facil, se nos hara mas dificil evitarla.   
 
### Ocuparse de la procrastinación: Es más fácil y valioso de lo que parece

La procrastinacion a pezar de ser util para la memoria a largo plazo, facilmente 
podemos pasar esto a perdida de tiempo, aprenderemos sobre nuestros zombis 
internos (respuestas automaticas) para engañarlos. 
**La procrastinacion es un habito adictivo**.   
 
- La procrastinacion es un **gran mal habito** el cual lleva tiempo evitarla.   

- Evitarla nos puedre traer muchos beneficios.   

- Entramos en ella con **auto-convenciendonos** con cosas que no son ciertas.   

**Analogia** Hace unos 150 años en una conferencia de ciencias y artes, todos 
sus integrantes **tomaron a proposito, una cantidad de arsenico** equivalente 
al doble de una dosis mortal, pero no les sucedio nada.   
Esto fue por que se puede crear **inmunidad al arsenico** tomando pequeñas dosis 
de este y aumentando esta a lo largo del tiempo, hasta el punto que uno es 
inmune incluso a la dosis mortal.   

![](./img/18_como_evitar_procastinar.PNG)

**Moraleja** Poco a poco podremos ser mas inmunes a la procrastinacion.   


### Zombis en todas partes
Si pensamos la primera vez que cualquiera esta manejando por primera vez uno 
esta en un estado de **sobre-alerta** en el que nos sentimos abrumados por la 
gran cantidad de informacion, y por lo tanto solemos equivocarnos.   
Pero alguien que ya sabe manejar este ya esta en un estado **automatico~zombie** 
donde ya no piensa tanto, solo hace las cosas.

![](./img/19_conduciendo_primera_vez_vs_experimentado.PNG)

Hablando Neuro-cientificamente un habito (uso de fragmento constante) es un 
ahorrador de energia que nos permite concentrarnos en otras actividades, estos 
habitos pueden ser cortos como cepillarse los dientes o largos como una caminata, 
y comunmente tienen 4 partes.

**1. Estimulo**, ej. cuando decimos "hora de hacer tarea" o "ver que recibi", 
lo importante de este es **como respondemos a este**.   

**2. Rutina**, Es el la respuesta~accion que comunmente da el cerebro cuando 
recibimos el estimulo, estos estimulos pueden ser utiles, inofencibas o dañinas.   

**3. Recompensa**, Todo habito se desarrolla debido a que nos recompenza, 

**4. Creer**, Los habitos tienen poder por que crees en ellos, por ejemplo, 
tienes el habito de estudiar, por que crees que te hara mejor persona, o 
tienes el habito de procrastinar, por que crees que no podras dejar de hacerlo.   


### Surf: Proceso versus producto
Tener pensamientos negativos es normal incluso con algo que nos gusta lo 
imprtante es como los gestionamos, investigaciones ven que personas que evitan 
la procrastinacion, dicen cosas como *de de perder el tiempo y ponte en ello* , 
*una vez enpiezes te sentiraas mejor*.   

Otra forma util es **pensar en el proceso no en el producto**, por ejemplo, 
de lugar de pensar, "debo de terminar la tarea" pensar "voy a dedocarme a la 
tarea por 20min" 


### Aprovechar a tus zombis para que te ayuden
Comunmente uno no se da cuenta que esta procrastinando o que empezo a hacerlo, 
por ejemplo, nos llego un mensaje, y segun solo hibamos a leer el mensaje, pero 
sin darnos cuenta ya estamos leyendo publicaciones. Un ejemplo seria que 
**mientras haces un pomodoro, apagar toda distraccion**.   

**Crear un plan**, por ejemplo estudiantes dejan el celular en su carro, o 
desconectarse de internet, talvez al principio no funcione bien el plan, pero 
poco a poco lo hara, incluso haciendo ajustes a este.   

**Recompenza** para hacer el habito mas facilmente, hay que asegurarnos 
que recibimos una, cuando terminamos o hacemos nuestra rutina, . 


### Malabares con la vida y el aprendizaje
Es util cada noche escribir una lista de que hecer al dia siguiente, es importante 
tener una **hora para terminar**, lo que puede que no nos de tiempo, y asi 
concentrarnos en lo que es importante, para que la proxima vez que nos planemos 
una actividad, tengamos una mejor nocion y seamos mas realisatas del tiempo que 
nos toma realizar una accion




## Memoria

### Profundizar en la memoria
Tenemos una **gran capacidad de memoria espacial**, por ejemplo, al entrar 
a una casa, donde esta la sala, cocina, baño, las proporciones de la mesa los 
sillosnes, etc.   

Esto se da ya que las **necesidades antepasadas** eran al salir y luego regresar 
a casa, lo cual tenian que recordar cosas como, donde esta el rio, manzano, etc.   

Es la razon por la que nos cuesta trabajo recordar numeros, nombres, patrones, ya 
que no teniamos la necesidad de esto.   

Por lo dicho antes concluimos que **Las imagenes son importantes para memorisar** 
ya que estas se conectan a nuestra memoria espacial. Por ejemplo, para recordar la 
formula f=ma podemos usar la siguiente imagen, la cual nos ayudara a recordar mucho 
mas facil, por que invoca la frase "flying mule ass".   

![](./img/21_flying_mule_ass.PNG)

**Usar targetas**, Son utiles para recordar, en donde, escribimos lo que queremos 
recordar de un lado, y la respuesta del otro, y usarla de forma periodica.

![](./img/22_recordar_con_targetas.gif)


### ¿Qué es la memoria de largo plazo?
El hipocampo es la parte del cerebro que nos ayuda a recordar a largo plazo, y 
estos pueden tener cambios cada vez que los recordamos, este proceso se llama 
**reconsolidacion**, el cual puede incluso crear recuerdos falsos, 

### Creación de grupos significativos y la técnica del palacio de la memoria

Para poder recordar numeros podemos hacer **asociacion** por ejemplo con nuestra 
edad y un evento que paso a dicha edad, o referencias, por ejemplo, para recordar 
11 usamos "11 segundos es un buen tiempo para correr los 100 metros".   

![](./img/25_memorizar_por_asociacion.PNG)

tambien aplica con los **acronimos** por ejemplo, A PIE (un pastel) para 
"Abstraccion, Polimorfismo Iherence=herencia, Encampsulacion", igual no 
**pueden ser imperfectos y personalisados**, el proceso que podemos llevar 
es el siguiente.    

![](./img/23_como_crear_acronimo.PNG)

**Palacio de memoria** esta tecnica comprobada es usar un entorno familiar y/o 
en el que solemos estar, para recordar cosas, por ejemplo, usar el color y 
forma de los muebles para imaginarnos-relacionar los acronimos o conceptos 
que queremos memorizar, incluso es un ejercicio que nos hara mas creativos.   

![](./img/24_memory_palace.PNG)

## Semana 4 - Renacimiento del aprendizaje

### Cómo ser un mejor estudiante

**mas vale Ejercicio que farmacos** Hoy en dia sabemos que en ciertas areas
del cerebro como el hipocampo, las neuronas nacen y mueren todos los dias,
y lo que las salva de morir es usar estos nuevos patrones periodicamente
como otras actividades, que sin saber usar estos patrones al hacer distintas
y nuevas actividades, como el ejercicio que es mas poderoso que cualquier
farmaco para aprender mejor disponible en el mercado.

Hasta la pubertad nuestros el area prefrontal del cerebro por fin es capas de
manejar la percepcion, por ejemplo, ver la profundidad de una imagen escalonada
lo que demuestra que madura y con eso tambien maduran otras cosas como el
razonamiento, la cual tambien es manejada por esta area del cerebro.

### Introducción al Renacimiento del Aprendizaje y cómo liberar tu potencial

El conocimiento no lleva un crecimiento en orden, incluso es normal que
despues de un tiempo un tema que creias dominabas luego paresca que te
lleva mas trabajo, esto puede suceder por que hay una re-estructuracion,
**por ejemplo**, en los que estan aprendiendo un nuevo lenguaje, estos
tienen periodos que sienten que se vuelven incomprencibles.

### Crear una metáfora o analogía visual vívida

Una analogia es darse cuenta que un cierto tema o patron es similar a
otra cosa, **entre mas visuales mejor**, igual pudes modificarlas o
sustituirlas con el tiempo.

Por ejemplo, en 1886 se creo una imagen de unos monos reprecentando la
un anillo de benceno, en la que si nos fijamos, cada mono reprecenta
una molecula de hidrojeno, y sus colas entrlazadaz reprecentan un enlace
doble.  

![27_analogias_visuales](./img/27_analogias_visuales.PNG)

Y haci ahi otras metaforas, que usaban cosas simples para explicar conceptos
avanzados como el de.

- onion = negativo, (cebollas) por que hacen llorar
- paws = pawsitivo (patas de gato)

### No se necesita la envidia del genio

**Repeticion**, Aprender cualquier cosa tiene un periodo donde lo mejor es la
repeticion, y despues de un tiempo ya no sera necesaria ya que lo hemos
aprendido de memoria, sin olvidar que debemos de encontrarle sentido a los
que estamos haciedo

**Sindrome del impostor**, este sindrome comprobado y comun en las personas,
es sentir que un tema que dominas, tienes la sensacion que en realidad no lo
haces.

**Defecto de los genios**, si ahi personas que tienen una capacidad mayor a
la del promedio, pero igual no viene una bendicion sin una maldicion, por
ejemplo, las personas capaces de procesar mas informacion son menos creativas
y las personas que tienen una mayor capacidad de memorizar, pueden caer mas
facil en el problema de Einstellung.

### Cambia tus pensamientos, cambia tu vida

Santiago Ramon y Crajal, padre de la neorociencia moderna fue encarcelado
a los 11 años ya que era un revelde, es comun que antes de los 20 años, no
se desarrolla por completo las areas de control y pensamiento, haciendo que
tengan un comportamiento mas impulsivo.

Fue por esto que hasta los 20 años Santiago empezo a mostrar señales de
copetencia, y tambien se dio por que cambio su manera de pensar, ya que convivia
comunmente con personas estudiosas.

Lo que el mensiono en su auto-biografia, fue que la clave del axito de las
personas que lo rodeaban era por su perseverancia, y la flexibilidad de
cambiar sus ideas y adminir sus errores.

## Semana 4 - Como Liberar tu potencial

### El valor del trabajo en equipo

**Transtorno de aplia perspectiva**, un golpe fuerte en la cabeza puede
causar daño el emisferio derecho, los que padecen este raro sindrome pueden
funcionan casi normalmente, incluso si tenian la capacidad de hacer calculos
complejos, lo seguiran podiendo hacer, pero no tienen una capacidad de
perpectiva o de informacion incoherente, **por ejemplo**, si sacan un
calculo el que un puesto ambulante de hot-dogs muestra perdidas de 1 millon,
estos no mostraran sorpresa y/o no tendran la capacidad de decir "hey esto
esta raro o no concuerda esta informacion con puesto de hot-dogs"

Investigaciones indican que el emisferio derecho, nos ayuda a dar un paso
atras para poner nuestro trabajo en perspectiba, tambien nos puede pasar a
nosotros, por ejemplo, al no querer revizar un examen que recien hizimos,
esto puede significar no querer usar diferentes partes de nuestro cerebro.

**El hemisferio izquierdo** se centra en hacer e interpretar la realidad,
aferrandose a hacerlas como siempre como esta es y funciona comunmente, lo
cual conlleba un gran trabajo.

**Usamos mas al hemisferio izquierdo cuando estamos en focus** lo cual
implica que podamos equivocarnos con cosas obias, por lo que se recomienda
que aunque estes seguro de lo que hicistes lo revices ya que puedes estar
callendo en una perpectiva confiada del hemisferio izquierdo.

**No te engañes a ti mismo** la persona que mas nos engaña somos nosotros
mismos, esto lo dijo Richard Feynman premio nobel de fisica, y la mejor
manera de luchar contra esto es **trabajar en equipo**, por ejemplo, en
reuniones de ideas, grupos de estudio, publicar tu trabajo.

### Lista de comprobación de las pruebas (ChekLists)

Para estudios que terminan en un examen, es mucho mas util hacer
repetidas veces un examen, que estudiar un texto

**Richard Felder** fue de los primeros en oficializar el uso de
Cheklists, originalmente dirijido a ingenieros, y un Checklist
para prepararse para un examen podria ser la siguiete.

- [ ] Hiciste un un esfuerzo serio para entender el texto?, buscar
      por ejemplos ya resuelto no cuenta
- [ ] Trabajaste con compañeros de clase o almenos verificaste
      las soluciones, de preferencia con otros?
- [ ] Intentaste resumir los temas con analogias, metaforas
      cuadros mentales, etc?
- [ ] Participaste activamente en el problema y discuciones con otros?
- [ ] Consultaste con alguien cuando tuviste problemas con algo?
- [ ] Entendistes todos los problemas incluso cuando los resolvistes?
- [ ] Te dieron una guia de estudio?
- [ ] Repasaron la guia en grupo y se preguntaron mutuamente
- [ ] Importante!. Dormiste lo sificiente antes de tu prueba?

### Comienzo difícil: Un salto a una continuación fácil

Un modo comun al contestar un examen es empezar con lo facil, pero esto
puede ser contra producente, lo mas recomendable es empezar con el
problema dificil, y si te atoras ir a otro problema mas facil, esto
produce una **intercalacion entre los modos difuso y concentrado**

**Analogia** es como un chef cuando al pone a freir un bistec, mientras
este se frie este corta tomates o cebollas, mientras se termina de freir
el bistec.

### Instrucciones útiles finales para pruebas

**Cuando te estresas** el cuerpo segrega quimicos como el cortisona, lo que
causa sudor en las palmas, un nudo en el estomago, latido rapido, investigaciones demuestran
que puedes aumentar tu rendimiento pensando **positivamente**, por ejemplo,
"este examen me da miedo" a "quiero hacer este examen para hacer mi mejor
intento" o "quiero saber que tanto se del examen" etc.  

**relajarse** vijilan que tengas una respiracion relajada, almenos 1min.

**Tapa las respuestas de opcion multiple** al intentar responder sin ver
las respuestas, esto te ayuda a no enrredar informacion.  

**Un Plan B** tener un plan incluso para el peor escenario, hara que no
te estreses tanto, haciendo que aumente tu rendimiento, por ejemplo,
una carrera alternativa

**Buena y Mala preocupacion**, una hace que te prepares, la otra hace que
te estreses y pierdas tiempo

**Chequeo leve noche anterior** si estudiamos correctamente, es decir, haber
estudiado con periodicidad el tema, un estudio ligero ayuda a recordar,
y si tu examen es de logica, entonces trata de verlo con **diferente perspectiva**
lo cual hara que puedas notar errores que puedas cometer.

### Resumen

¿Cuál de las siguientes opciones crees que es más importante para ti a
fin de mejorar tu capacidad de aprender? No hay una respuesta correcta,
las seleccionadas fueron las mas seleccionadas por la comunidad.

- [X] Crear una metáfora o analogía para lo que intentas aprender.
- [X] El ejemplo del ganador del Premio Nobel Santiago Ramón y de Cajal
      con respecto al hecho de que si cambias tus pensamientos, puedes
      cambiar tu vida.
- [ ] Tener orgullo en aspirar a tener éxito a causa de las mismas razones
      por las que otras personas dicen que no podrás lograrlo.
- [ ] Tener en cuenta que cuando pasas rápidamente por una pregunta de una
      tarea o de una evaluación y no vuelves a controlar tu trabajo, estás
      actuando un poco como una persona que se niega a utilizar parte de
      su cerebro.
- [X] Consultar una lista de verificación de prueba.
- [ ] Utiliza la técnica "comienzo difícil, ve a lo más sencillo".
- [ ] ¡Cambia tu postura desde "Esta prueba me ha dado miedo" a "Esta
      prueba me ha emocionado para hacer mi mejor esfuerzo"!
- [ ] Aprender a utilizar técnicas de respiración para ayudarte a
      controlar los sentimientos de estrés y pánico al realizar pruebas.
- [ ] Obligarte a parpadear, cambiar tu atención y, luego, volver a
      comprobar tus respuestas usando una perspectiva general y
      preguntándote, "¿Esto realmente tiene sentido?"
- [X] Cambiar tus hábitos para asegurarte de tratar de conseguir un buen
      sueño la noche antes de una prueba.

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Por el camino

### Accesos rapidos

- [itaki](https://www.italki.com/): Plataforma para encontrar maestros
  de ideomas y tener videollamadas con ellos

### Aprender idiomas

#### Consejos de Polyglot Grace

- Fuente. [YouTube](https://www.youtube.com/watch?v=Lvae_CiMa5o)

1, **todo lo que aprendas que tenga un contexto**, no memorises palabras
o verbos, como dog es perro, o learn = aprender, mejor memoriza frases
que incluyan estas palabras. Por ejemplo.

- No aprendas `dog`, aprende:
  - That dog barks at bikes
  - I like dogs because they are loyalty

- No aprendas `travel`, aprende.
  - I like to travel but it is expensive
  - I usually travel on sundays to restaurants with my family

2, Aprende los **basicos de verdad**, los cuales suelen ser como hacer preguntas
como, when, how, who, what, etc, por que los primeros contactos suelen
ser preguntar cosas a otras personas.

- Where are you from?
- How are you?
- What kind of music do you like?

3, Aprende **palabras que extiendan las frases**, como lo son but, becuse,
if, even though(aunque), etc, y con esto empesar a armar oraciones
plantillas como son

| I like | ______________ | because it's | _________ |
| ------ | -------------- | ------------ | --------- |
|        | Traveling      | but it is    | expensive |
|        | learn japonese |              | diferent  |
|        | music          |              | relaxing  |

4, **Escribe tu propia historia**, escribe mas de 1 hoja sobre ti o algo que
te guste, donde uses diferentes tipos de tiempos, estructuras, y palabras.
Debes de memorizarlo, poder decir el mismo contexto sin leerlo, esto
por que en tus primeras conversaciones **te pondras nervioso**, por lo que
memorizar sentencias/frases, no tendras que hacerlas mientras estas
nervioso.

5, **Encuentra un tutor**, Puede ser online con herramientas como
[itaki](https://www.italki.com/), que lo importante es que corrijan desde
su punto de vista experto.

6, **Primer encuentro real**, encuentra/conoce un compañero de lenguaje, sal a la
calle al pais, por que una interaccion mas real, que a diferencia del
tutor, las conversaciones ya no seran controladas.

7, **Empieza aprender de material autentico**, busca herramientas de
aprendizaje de calidad, donde tengas un buen input y output, que tengan
cosas como transcripciones (ej. video en ingles con subs en ingles), unos
ejemplos de estas herramientas son

- [babbel.com](https://www.babbel.com/)
- [readlang.com](https://readlang.com/)

8, **Aprende a travez del lenguaje que quieres aprender**, esto quiere
decir que si te gusta la musica, ajedres, etc, aprende estos temas leyendo,
escuchando en ingles o idioma que quieras aprender. Por ejemplo, la poliglota
estro a clases de piano precenciales con un maestro que hablaba ingles,
mientras que todabia no dominaba el ingles, pero esto le permitio
interactuar en ingles y eso le ayudo mucho.

#### Mejora tu pronunciacion con el metodo de SHADOWING

- Fuente: [YouTube](https://www.youtube.com/watch?v=DP00q0euk7Y)

1, (entry level) Pronuncia bien las vocales y consonantes, lo cual son
las bases que si no se tienen costara mucho sonar como nativo.

2, Entender una cosa es como suenan las vocales y consonantes por si
solas, y como funcionan en las palabras y frases, ya que en muchos
lenguajes existen cambios, que provocan que no se escuche como se lee,
hay cambios segun las diferentes combinanciones, contextos, intenciones
que cambian el segun como deberia de sonar, siguiendo las vocales/consonantes

- Ejemplo, una cosa es 'what, are, you, doing?' y otra 'what are you doing'
  que suena como /wadayou doin/, por que uno se dice lento y otro rapido.

3, **¿Que es shadowing?**, es imitar al que esta hablando como el menor
resazo posible, por ejemplo, econtrar un video o audio y empezar decir
lo que el dice, tratando que sea al mismo tiempo, imagina que eres
esa persona, si el rie, grita, murmulla tu haces lo mismo.
**No es importante entender que dice** lo importante es imitar como dice
las oraciones, flojos, y sonar como el.

4, **Diferentes niveles de shadowing**, El shadowing puede ser deafiante,
por lo que lo ideal seria ir por niveles.

1. Con una trascripcion/subtitulos a una velocidad recida con alguien
   que hable formalmente.
2. Luego hazlo sin los subtitulos
3. hazlo a velocidad normal.
4. (avanzado) Hazlo con personas que hablen rapido y/o usen informalidades
   modismos o parecidos.
