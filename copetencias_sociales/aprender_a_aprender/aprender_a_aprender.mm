<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1584168228642" ID="ID_1637993715" MODIFIED="1584874422389" TEXT="Aprender a Aprender">
<edge WIDTH="2"/>
<font BOLD="true" NAME="Eras Bold ITC" SIZE="20"/>
<node CREATED="1584168930903" HGAP="74" ID="ID_1155905475" MODIFIED="1585871633557" POSITION="left" TEXT="Limitantes" VSHIFT="-6">
<edge COLOR="#ff3333" WIDTH="2"/>
<cloud/>
<font BOLD="true" NAME="SansSerif" SIZE="16"/>
<node CREATED="1584168965070" HGAP="35" ID="ID_593998005" MODIFIED="1584875551502" TEXT="Procrastinacion" VSHIFT="-17">
<node CREATED="1584170480242" ID="ID_571413746" MODIFIED="1584875092856">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/02_como_funciona_la_procrastinacion.PNG" width="250" height="100" />
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tener pensamientos negativos al querer iniciar una actividad es normal incluso con algo que nos guste
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584873488969" ID="ID_372496272" MODIFIED="1585385803720" TEXT="Habito adictivo = Respuesta automatica"/>
<node CREATED="1584874483540" ID="ID_510394334" MODIFIED="1584874663269">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/18_como_evitar_procastinar.PNG" width="200" height="100" />
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Hace unos 150 a&#241;os en una conferencia de ciencias y artes, todos sus <b>integrantes tomaron a proposito, una cantidad de arsenico</b>&#160;equivalente&#160;&#160;al doble de una dosis mortal, pero no les sucedio nada.&#160;&#160;&#160;&#160;Esto fue por que se puede crear <b>inmunidad al arsenico</b>&#160;tomando peque&#241;as dosis&#160;&#160;de este y aumentando esta a lo largo del tiempo, hasta el punto que uno es&#160;&#160;inmune incluso a la dosis mortal.&#160;&#160;</font>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584875539183" ID="ID_1760874398" MODIFIED="1584875840308" TEXT="Como evitarla">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <font size="4">Estrategia de Pomodoro, empezando con 25min y al terminar darnos una recompensa, lo importante es cumplir con el tiempo, no que lograremos en ese tiempo.</font>
      </li>
      <li>
        <font size="4">Pensar en el proceso no en el producto, por ejemplo,&#160;de lugar de pensar, &quot;debo de terminar la tarea&quot; pensar &quot;voy a dedocarme a la&#160; tarea por 20min&quot;</font>
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1584169515287" HGAP="56" ID="ID_1083891707" MODIFIED="1584666372235" TEXT="Poca practica" VSHIFT="-9">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">A veces creemos que por que entendimos algo creemos </font>
    </p>
    <p>
      <font size="4">que lo aprendimos, pero esto es si no lo practicamos lo mas </font>
    </p>
    <p>
      <font size="4">seguro es que no lo sabemos hacer, por lo que se recomienda </font>
    </p>
    <p>
      <font size="4">practicar hacerlo uno mismo ya</font>
    </p>
  </body>
</html></richcontent>
<node CREATED="1584169768250" HGAP="77" ID="ID_453228629" MODIFIED="1585390217184" VSHIFT="-5">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="img/26_repazo_espaciado.PNG" />
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Es Recomendable repasar el tema en lapzos aplazados para que esta mas facilmente se quede en la memoria a largo plazo</font>.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1584171075889" HGAP="65" ID="ID_861749842" MODIFIED="1584604880158" TEXT="Poco Sue&#xf1;o" VSHIFT="6">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estar despierto se crea naturalmente &quot;toxinas&quot; que impiden un pensamiento optimo, y esta se desechan solo cuando dormimos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584171162783" HGAP="54" ID="ID_1655362986" MODIFIED="1585871691828" TEXT="Ambiente empobrecido" VSHIFT="47">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Se ha visto en roedores que en un ambiente empobresido este puede aprender menos, en cambio si esta ficicamente y socialmente bien aprendera mas optimamente</font>
    </p>
  </body>
</html></richcontent>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1655362986" ENDARROW="Default" ENDINCLINATION="1171;67;" ID="Arrow_ID_1001209759" SOURCE="ID_1520563930" STARTARROW="Default" STARTINCLINATION="524;-354;"/>
</node>
<node CREATED="1584172269467" HGAP="61" ID="ID_571700969" MODIFIED="1585875621381" TEXT="ilucion de copetencia">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>a veces creemos que por que comprendimos algo ya lo aprendimos, y la verdad solo podemos asegurarnos de que lo aprendimos por medio de la practica o bien por medio de mini-examenes</b>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1584172288552" HGAP="13" ID="ID_1749285727" MODIFIED="1584665750147" TEXT="Mini-examenes" VSHIFT="26">
<node CREATED="1584604517541" HGAP="23" ID="ID_1448398840" MODIFIED="1584666372235" VSHIFT="-3">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="img/14_teoria_practica.PNG" />
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1584173033726" HGAP="64" ID="ID_773561411" MODIFIED="1585875648179" TEXT="Miedo a los errores" VSHIFT="-1">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Los errores son parte fundamental del proceso de aprendisaje siempre y cuando aprendamos de ellos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584600986302" HGAP="40" ID="ID_501866053" MODIFIED="1585875651842" TEXT="Sobre aprendizaje" VSHIFT="12">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Para que sirve</b>&#160;puede ser util para el perfeccionismo como una precentacion, y/o enfrentarce a situaciones de nerviosismo un concierto de piano, por ejemplo los precentadores de TED llegan a practicar hasta 70hr para una charla tipica de 20min.&#160;&#160;
    </p>
    <p>
      
    </p>
    <p>
      <b>Ejemplo desventaja</b>&#160;es similar a aprender carpinter&#237;a practicando solo con un martillo. Despu&#233;s de un tiempo, pensar&#225;s que puedes arreglar cualquier cosa a golpes.&#160;&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1584608756706" ID="ID_275485187" MODIFIED="1584871612310" TEXT="descanzo  ~ Lapzos de tiempo">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><font size="4">Scott Young</font></b><font size="4">, Un master del estudio dedica 4hr de estudio algo que puede llegar a ser contraintuitivo, pero esto le permite concentrarse en lo importante y descanzar para poder concretar el conocimiento, pero estas horas las hace intensas y con un nivel alto de concentracion y </font>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584601066299" ID="ID_1553109252" MODIFIED="1584871934659">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/12_aprendido_util_sobreaprendido_estorbo.PNG" height="100" width="230" />
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">El sobre-aprendisaje puede causar Einstellung ya que si siempre estudiamos como martillar, luego pensaremos que todo se arregla a martillazos</font>
    </p>
  </body>
</html></richcontent>
<arrowlink COLOR="#000000" DESTINATION="ID_1958464440" ENDARROW="None" ENDINCLINATION="69;0;" ID="Arrow_ID_45633868" STARTARROW="Default" STARTINCLINATION="-70;45;"/>
</node>
</node>
<node CREATED="1584601964037" HGAP="-1" ID="ID_1958464440" MODIFIED="1585875639022" TEXT="Einstellung" VSHIFT="6">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Einstellung: Este concepto es que con un simple pensamiento inicial, idea, etc que evita que encuentres la mejor solucion, es como un bache o una desviacion que uno toma sin querer, que hace que uno piense en lo mismo cada vez</font>
    </p>
  </body>
</html>
</richcontent>
<linktarget COLOR="#000000" DESTINATION="ID_1958464440" ENDARROW="None" ENDINCLINATION="69;0;" ID="Arrow_ID_45633868" SOURCE="ID_1553109252" STARTARROW="Default" STARTINCLINATION="-70;45;"/>
</node>
<node CREATED="1584602087644" HGAP="-37" ID="ID_752209002" MODIFIED="1585875634020" TEXT="Saltar al agua sin saber nadar" VSHIFT="45">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">es un error comun empezar a trabajar un tema sin tener bien los fundamentos de este, por ejemplo, al aprender a ir en bicicleta, primero usa las ruedas de ayuda.</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1584171352655" HGAP="32" ID="ID_123344427" MODIFIED="1585871513710" POSITION="right" VSHIFT="3">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Herramientas
    </p>
  </body>
</html></richcontent>
<edge COLOR="#00ff00" WIDTH="2"/>
<cloud/>
<font BOLD="true" NAME="SansSerif" SIZE="16"/>
<node CREATED="1584605348068" HGAP="-411" ID="ID_467510175" MODIFIED="1585871481532" TEXT="Fragmentacion" VSHIFT="-29">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Los fragmentos son como rompecabezas en los cuales es dificil recordar un solo fragmento, pero cuando estos estan ordenas e interconectados es mas facil recordar un fragmento, ya que este conjunto forma una imagen genera</b>
    </p>
  </body>
</html></richcontent>
<node CREATED="1584172833731" HGAP="189" ID="ID_776390687" MODIFIED="1584608686939" TEXT="General a especifoco" VSHIFT="18"/>
<node CREATED="1584171613541" HGAP="201" ID="ID_577560336" MODIFIED="1584608687231" VSHIFT="7">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/06_imagen_general_vs_partes_de_imagen.PNG" width="120" height="180" />
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584600656764" ID="ID_13518950" MODIFIED="1584608687539" TEXT="&#xbf;Como crear fragmentos?">
<node CREATED="1584600720426" ID="ID_435676739" MODIFIED="1584600892834" TEXT="1. Concentrarse">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Como mensionamos tenemos solo unas cuatro ranuras de informacion para nuestra memoria de trabajo, y como formaremos un nuevo fragmento si una de estas ranuras esta ocupada chateando, viendo publicaciones</font>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584600768895" ID="ID_607082442" MODIFIED="1584600887367" TEXT="2. Comprencion idea basica">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">COMPRENCION DE IDEA BASICA Aqui es donde sentamos las bases,&#160;&#160;de lo que queremos hacer, por ejemplo, viendo como se hace, por medio de de&#160; ejemplos ya resueltos o indicarnos cuales son los pasos</font>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584600841262" ID="ID_707393273" MODIFIED="1584600880781" TEXT="Contexto">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">es entender el contexto para que no solo veas como usar el fragmento, tambien <b>cuando usarlo</b>&#160;que comunmente se obtiene practicando.</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1584605418037" HGAP="-208" ID="ID_1540343781" MODIFIED="1585871489169" TEXT="Metaforas y analogias">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Las analogias y metaforas son una poderoza herramienta para aprender, en este caso usamos reprecentamos el cerebro como una maquina de pinball, y nuestra linea de pensamientos como la pelotita que rebota, y los diferentes cojines como las ideas.&#160;&#160; </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">Ya que esto ayuda a simplificar o transportar la idea a algo con lo que estamos familiarizados.&#160;&#160;&#160;</font>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="16"/>
<node CREATED="1584606246964" HGAP="39" ID="ID_1473988842" MODIFIED="1584606267188" VSHIFT="11">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="img/15_usar_metaforas.PNG" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1584606888017" HGAP="50" ID="ID_1470519094" MODIFIED="1585875905322" TEXT="Objetivos~Motivacion" VSHIFT="119">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><font size="4">**Specific**</font></b><font size="4">: Es imprescindible que los objetivos fijados sean **concretos**. </font>
    </p>
    <p>
      <font size="4">Por ejemplo: Aumentar el n&#250;mero de followers de nuestra cuenta de Facebook.&#160;&#160; </font>
    </p>
    <p>
      
    </p>
    <p>
      <b><font size="4">**Measurable**</font></b><font size="4">: Si no somos capaces de medir el impacto del objetivo fijado, dif&#237;cilmente </font>
    </p>
    <p>
      <font size="4">podremos saber si nuestra acci&#243;nes han tenido &#233;xito una vez finalizada. Es decir, que </font>
    </p>
    <p>
      <font size="4">debemos hacer de nuestro objetivo una meta cuantificable. Por ejemplo: **Aumentar 10%** </font>
    </p>
    <p>
      <font size="4">el n&#250;mero de followers de nuestra cuenta de Facebook.&#160;&#160; </font>
    </p>
    <p>
      
    </p>
    <p>
      <b><font size="4">**Attainable=Alcanzable**</font></b><font size="4">: Hemos de fijar objetivos atendiendo a las probabilidades, </font>
    </p>
    <p>
      <font size="4">estad&#237;sticas y referencias a nuestra disposici&#243;n. Sin dejar de ser ambiciosos, </font>
    </p>
    <p>
      <font size="4">nuestras metas no deben ser **desproporcionadas**, ya que ello puede llevarnos a la </font>
    </p>
    <p>
      <font size="4">frustraci&#243;n con facilidad. Por ejemplo: Si nuestro n&#250;mero de followers ha aumentado </font>
    </p>
    <p>
      <font size="4">entre un 2% y un 3% durante las mismas fechas en los &#250;ltimos cinco a&#241;os, no podemos </font>
    </p>
    <p>
      <font size="4">fijarnos como objetivo que durante esta campa&#241;a crezca un 10% pero si un 4%.&#160;&#160; </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">**Realist**: Aunque es muy similar a la caracter&#237;stica anterior, el matiz que &#233;sta </font>
    </p>
    <p>
      <font size="4">aporta es que debemos atender a los recursos de los que dispone nuestra empresa y </font>
    </p>
    <p>
      <font size="4">que puede poner al servicio de un objetivo en particular. Por ejemplo: Si nuestro </font>
    </p>
    <p>
      <font size="4">n&#250;mero de followers ha aumentado entre un 2% y un 3% durante las mismas fechas en </font>
    </p>
    <p>
      <font size="4">los &#250;ltimos cinco a&#241;os, y durante la presente campa&#241;a tenemos menos presupuesto que </font>
    </p>
    <p>
      <font size="4">la temporada anterior, no podemos fijarnos como objetivo que nuestro crecimiento </font>
    </p>
    <p>
      <font size="4">sea de un 10%. </font>
    </p>
    <p>
      
    </p>
    <p>
      <b><font size="4">**Timely**</font></b><font size="4">: Esta caracter&#237;stica hace referencia a los **plazos concretos** dentro </font>
    </p>
    <p>
      <font size="4">de los cuales deberemos cumplir el objetivo fijado. Por ejemplo: Aumentar el n&#250;mero </font>
    </p>
    <p>
      <font size="4">de followers de nuestra cuenta de Facebook en un 10% antes de la primera semana de enero.</font>
    </p>
  </body>
</html></richcontent>
<node CREATED="1584607974599" ID="ID_1828065731" MODIFIED="1584608046677">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/16_objetivos_smart.PNG" height="170" width="390" />
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584878802363" ID="ID_125095740" MODIFIED="1584878971084" TEXT="Listas To-Do con tiempo de termino">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">es importante&#160;&#160;tener una hora para terminar, lo que puede que no nos de tiempo, y asi&#160;&#160;concentrarnos en lo que es importante, para que la proxima vez que nos planemos&#160;&#160;una actividad, tengamos una mejor nocion y seamos mas realisatas del tiempo que&#160;&#160;nos toma realizar una accion</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#000000" CREATED="1584665019442" HGAP="42" ID="ID_585795479" MODIFIED="1585871751725" STYLE="fork" TEXT="Mapas mentales" VSHIFT="139">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ayudan al pensamiento difuso, y son utiles ya que conecta los diferentes fragmentos que tenemos, ya que con solo ver una palabra o imagen ya podemos traer un fragmento y poder trabajar de forma enfocada sobre dicho fragmento
    </p>
  </body>
</html></richcontent>
<node CREATED="1584666102802" ID="ID_691258140" MODIFIED="1584666116963">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="img/17_mapas_mentales.PNG" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1584874466294" HGAP="69" ID="ID_1479973742" MODIFIED="1585875783567" TEXT="Habitos" VSHIFT="31">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Hay habitos malos y buenos </font>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1584874816269" HGAP="30" ID="ID_1747029350" MODIFIED="1585386523150" TEXT="1.Estimulo, 2.Rutina, 3.Recompensa, 4.Creer" VSHIFT="9">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      **1. Estimulo**, ej. cuando decimos &quot;hora de hacer tarea&quot; o &quot;ver que recibi&quot;, lo importante de este es **como respondemos a este**.
    </p>
    <p>
      
    </p>
    <p>
      **2. Rutina**, Es el la respuesta~accion que comunmente da el cerebro cuando recibimos el estimulo, estos estimulos pueden ser utiles, inofencibas o da&#241;inas.
    </p>
    <p>
      
    </p>
    <p>
      **3. Recompensa**, Todo habito se desarrolla debido a que nos recompenza,
    </p>
    <p>
      
    </p>
    <p>
      **4. Creer**, Los habitos tienen poder por que crees en ellos, por ejemplo, tienes el habito de estudiar, por que crees que te hara mejor persona, o tienes el habito de procrastinar, por que crees que no podras dejar de hacerlo.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584874705460" ID="ID_989429119" MODIFIED="1584874801101">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/20_habitos.PNG" width="150" height="150" />
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584875841875" ID="ID_162804196" MODIFIED="1584878795277" TEXT="Pensar en procesos no en productos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Es por esto que es importante pomodoro ya que pensamos en cumplir el tiempo y no con un objetivo </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">por ejemplo, de lugar de pensar, &quot;debo de terminar la tarea&quot; pensar &quot;voy a dedocarme a la tarea por 20min&quot; </font>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584878979765" ID="ID_472473334" MODIFIED="1584879049341">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    Mas vale poco a poco que de vez en cuando trabajo duro
  </body>
</html></richcontent>
</node>
<node CREATED="1585386142102" ID="ID_1112380034" MODIFIED="1585386193433" TEXT="experiencia = automatico">
<node CREATED="1585386195227" ID="ID_827852008" MODIFIED="1585386488084">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/19_conduciendo_primera_vez_vs_experimentado.PNG" height="100" width="210" />
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1585386665919" HGAP="-23" ID="ID_415322152" MODIFIED="1585875891355" TEXT="Memoria" VSHIFT="-444">
<node CREATED="1585386831207" ID="ID_443850481" MODIFIED="1585875876569">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/21_flying_mule_ass.PNG" height="100" width="100" />
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Por lo dicho antes concluimos que **Las imagenes son importantes para memorisar** ya que estas se conectan a nuestra memoria espacial. Por ejemplo, para recordar la formula f=ma podemos usar la siguiente imagen, la cual nos ayudara a recordar mucho mas facil, por que invoca la frase <b>&quot;flying mule ass&quot;</b>.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1585386690099" HGAP="36" ID="ID_599550992" MODIFIED="1585389096037" TEXT="Gran memoria grafica-espacial" VSHIFT="8">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4"><b>Tenemos una **gran capacidad de memoria espacial**, por ejemplo, al entrar a una casa, donde esta la sala, cocina, ba&#241;o, las proporciones de la mesa los sillosnes, etc. </b></font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4"><b>Esto se da ya que las **necesidades antepasadas** eran al salir y luego regresar a casa, lo cual tenian que recordar cosas como, donde esta el rio, manzano, etc.</b></font>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1585387291153" HGAP="42" ID="ID_1172168066" MODIFIED="1585389090777" VSHIFT="16">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/22_recordar_con_targetas.gif" height="100" width="140" />
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1585387437422" HGAP="121" ID="ID_1577789497" MODIFIED="1585871715963" TEXT="Reconsolidacion" VSHIFT="29">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cada vez que recordamos algo puede tener cambios este recuerdo ya sea para bien o para mal, ya que la memoria es algo que tiene vida propia y contante mente se mueve
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1585387545763" ID="ID_1259249155" MODIFIED="1585389070417">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Asociacion<br /><img src="img/25_memorizar_por_asociacion.PNG" height="100" width="210" />
    </p>
  </body>
</html></richcontent>
<node CREATED="1585387549066" ID="ID_1386808131" MODIFIED="1585387552085" TEXT="Acronimos">
<node CREATED="1585387554261" ID="ID_179912402" MODIFIED="1585388139792">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="img/23_como_crear_acronimo.PNG" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1585387586169" ID="ID_1717900626" MODIFIED="1585388626657">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/24_memory_palace.PNG" height="100" width="210" />
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      **Palacio de memoria** esta tecnica comprobada es usar un entorno familiar y/o en el que solemos estar, para recordar cosas, por ejemplo, usar el color y forma de los muebles para imaginarnos-relacionar los acronimos o conceptos que queremos memorizar, incluso es un ejercicio que nos hara mas creativos.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1584665604485" HGAP="107" ID="ID_261509380" MODIFIED="1585871622090" POSITION="left" TEXT="Conceptos" VSHIFT="-232">
<edge WIDTH="2"/>
<cloud/>
<font BOLD="true" NAME="SansSerif" SIZE="16"/>
<node CREATED="1584600431907" HGAP="61" ID="ID_1520563930" MODIFIED="1585871691828" TEXT="Quimica" VSHIFT="-5">
<arrowlink DESTINATION="ID_1655362986" ENDARROW="Default" ENDINCLINATION="1171;67;" ID="Arrow_ID_1001209759" STARTARROW="Default" STARTINCLINATION="524;-354;"/>
<node CREATED="1584600455603" ID="ID_280409844" MODIFIED="1584600548234" TEXT="Dopamina = Placer">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      la forma de recompenza del cerebro indicando que la tarea que se esta haciendo se motive a repetirse ya que&#160; esta nos dara placer ya sea en el momento o despues, pero se puede causar&#160; resistencia a esta y tambien o por ende adiccion
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584600550001" ID="ID_854483313" MODIFIED="1584606870201" TEXT="Serotonina=Feicidad">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4"><b>entre mas deprimidos estemos tendremos menor cantidad de serotonina, y un comportamiento de mayor riesgo, por ejemplo los criminales presentan una cantidad baja de serotonina, tambien se a decubierto que tener baja autestima afecta nuestro aprendisaje y memoria</b></font>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584600632856" ID="ID_1230610664" MODIFIED="1584600650171" TEXT="Acetilcolina=Concentracion">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Al poner atencion en algun tema
    </p>
    <p>
      empieza a crearse, y ayuda a crear nuevas interconecciones en nuestro cerebro
    </p>
    <p>
      osea ayuda directamnte al aprendisaje y a la memoria
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1584602889333" ID="ID_364649123" MODIFIED="1584604108553" TEXT="Los afortunados son positivos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Investigaciones muestran que la <b>anciedad</b>&#160;interrumpe la habilidad de descubrir lo <b>inesperado</b>.
    </p>
    <p>
      
    </p>
    <p>
      <b>Experimentaron</b>&#160;con 2 grupos de personas, donde debian de encontrar puntos al centro de una pantalla que aparecian sin previo aviso,al grupo A se les daba dinero si no los encontraba crendo mas ancidad, y el B no ocurria nada, pero tambien aparecian puntos en los bordes de la pantalla, lo que vieron que el grupo A no se daban cuenta de estos puntos.&#160;&#160;
    </p>
    <p>
      
    </p>
    <p>
      La investigaci&#243;n revel&#243; que las personas afortunadas generan buena fortuna a trav&#233;s de cuatro principios b&#225;sicos.
    </p>
    <p>
      &#160;1. Son h&#225;biles para crear y notar oportunidades.
    </p>
    <p>
      &#160;2. tome decisiones afortunadas escuchando su intuici&#243;n.
    </p>
    <p>
      &#160;3. Crea profec&#237;as autocumplidas a trav&#233;s de expectativas positivas.
    </p>
    <p>
      &#160;4. adoptar una actitud resistente que transforme la mala suerte en buena.
    </p>
  </body>
</html></richcontent>
<node CREATED="1584602970801" ID="ID_1777345579" MODIFIED="1584665773356">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/13_los_afortunados_son_positivos.PNG" height="100" width="230" />
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1584168783951" HGAP="69" ID="ID_622740649" MODIFIED="1584874437185" TEXT="Estados de pensamiento" VSHIFT="30">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">No Podemos estar al mismo tiempo en los dos estados y ambos son utiles segun lo que queramos lograr</font>
    </p>
  </body>
</html></richcontent>
<edge WIDTH="2"/>
<node CREATED="1584168841252" ID="ID_799002122" MODIFIED="1584168877406" TEXT="Difuso"/>
<node CREATED="1584168886100" ID="ID_16236355" MODIFIED="1584172529029" TEXT="Enfocado" VSHIFT="6"/>
<node CREATED="1584172568188" ID="ID_962327104" MODIFIED="1584172630928">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/1_pensamiento_difuso_y_enfocado.PNG" height="100" width="230" />
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
