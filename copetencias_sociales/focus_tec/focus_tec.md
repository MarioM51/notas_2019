# Focus Tecs

1 --------------------------------------- IDS, Sueldo: 31, Desarrollador java
  Java
  Obligatorios:
  • Java 7 o superior
  • Spring
  • Maven,
  • Hibernate
  • GIT, TFS,
  • Webservices (REST),
  • Pruebas Unitarias (jUnit)
  • BD MSSQL server 2014 (Stored procedures, triggers, indices, etc.)

  Deseable:
  • Conocimientos en C# (Windows Forms y nUnit)
  • JMS
  • Microservicios

2 ----------------------------------------- Salinas, Sueldo: 32, Desarrollador - Java
Java Avanzado
• HTML CSS JavaScript
• JSP
• Spring
• MyBatis
• Maven
• Jboss
• Servicios Rest / Soap
• Git
• React (no estaba pero en otras ofertas de salinas si)

3 ------------------------------------ STEFANINI México/Brasil, Sueldo: 28, Desarrollador Web Java

EXPERIENCIA, CONOCIMIENTOS Y COMPETENCIAS:
Mínimo 3 años en puesto similar como Desarrollador Wen Java
Conocimientos y Experiencia Avanzada en:
• Programación en Java J2EE, SDK 1.8 +
• Spring Boot
• Microservices
• Maven
• Git & GitHub
• JUnit
• JPA
• SQL
• HTML 5.0
• CSS
• JS

Conocimientos y Experiencia Deseable
• Code Scan con Checkmarx
• Looper (Jenkins)
• React JS
• Mockito UT
• PowerMock
• Azure Cloud Ecosystem
• Big Data Basics
• Linux CLI

• Ingles

4 ------------------------------------- Empresa: KODE, Salario: ?, Puesto: Desarrollador JAVA EE Jr

Conocimientos Técnicos REQUERIDOS:
• Dominio del lenguaje Java.
• Conocimientos sólidos en Java (Java SE, Java EE, Java ME).
• Manejo INTERMEDIO de algún framework orientado a desarrollo en Java como:
  • Spring MVC
  • JSF
  • Struts
  • GWT (Google Web Toolkit) o
  • Vaadin
• Manejo INTERMEDIO de algún framework de persistencia en Java como:
  • Hibernate
  • Enterprise Java Beans (EJB) o
  • Java Persistence API (JPA)
• Manejo INTERMEDIO de algún administrador de base de datos como:
  • MySql
  • Oracle
  • SQL Server
  • SQLite o
  • PostgreSQL
• Manejo INTERMEDIO de JavaScript.

Conocimientos Técnicos OPCIONALES:
• Implementación y consumo de WebServices con Java (Axis 2 o JAX-WS).
• AJAX (Asynchronous JavaScript and XML).
• JAX-WS (Java API for XML Web Services).
• JMS (Java Message Service).
• SOA.
