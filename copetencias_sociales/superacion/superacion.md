# Superacion

## Tips rapidos

### El raton el queso y el gato

No pongas el miedo delante, ponlo atras, para que este miedo te empuje
a actuar y evitar que te enfrene/conjele, por ejemplo.

- Si queremos que al amarrar un raton de la cola para que jale algo.
  - No le pongas un queso al frente.
  - Ponle un gato atras

- Si le da miedo invitar a una chica.
  - Su miedo deberia ser que se va a arrepentir de no haberlo intentado
  - NO deberia de tener miedo de que lo rechase.

- Si su miedo es ir a una estrevista de trabajo.
  - Su miedo deberia ser que esta pasando el tiempo, se lo van a dar
    a alguien menos calificado, y se arrepentira mas de no intentarlo.
  - No que lo van a rechazar.
