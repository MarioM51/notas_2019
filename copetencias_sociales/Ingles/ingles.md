# Curso de Ingles desde cero con Pacho8a

- Fuente: [youtube](https://www.youtube.com/playlist?list=PLgrNDDl9MxYmUmf19zPiljdg8FKIRmP78)
- Nota: Para busquedas se inicia la frace con
  - uso de
  - ejemplos de
  - estructura de
- Extraer los bloques de tarjetas ankis `sed -n '/```QUITARr/,/```/p' francisco_ochoa.md > ingles8a.ankis`

## Indice

- [Curso de Ingles desde cero con Pacho8a](#curso-de-ingles-desde-cero-con-pacho8a)
  - [Indice](#indice)
  - [Accesos](#accesos)
    - [ToDo](#todo)
  - [A1. Basico](#a1-basico)
    - [4 - Presente continuo, progresivo o gerundio afirmativo - Reglas ING](#4---presente-continuo-progresivo-o-gerundio-afirmativo---reglas-ing)
    - [7. Uso de Who, Why y Because con el verbo to be en presente](#7-uso-de-who-why-y-because-con-el-verbo-to-be-en-presente)
    - [8.1 A vs An](#81-a-vs-an)
    - [8.2 There is - There are (Afirmativa, negativa, interrogativa)](#82-there-is---there-are-afirmativa-negativa-interrogativa)
    - [9. Adjetivos calificativos](#9-adjetivos-calificativos)
    - [10. Uso de How Many para preguntar cantidades contables](#10-uso-de-how-many-para-preguntar-cantidades-contables)
    - [11. How much con sustantivos no contables + verbo TO BE](#11-how-much-con-sustantivos-no-contables--verbo-to-be)
    - [12, 13: Uso de This, That, These, Those](#12-13-uso-de-this-that-these-those)
    - [14. El presente simple y sus reglas (S-ES-IES)](#14-el-presente-simple-y-sus-reglas-s-es-ies)
    - [15. El presente simple y sus auxiliares DO y DOES (Interrogativo y negativo)](#15-el-presente-simple-y-sus-auxiliares-do-y-does-interrogativo-y-negativo)
    - [16. Ejercicio de escucha en presente simple - Listening Exercise](#16-ejercicio-de-escucha-en-presente-simple---listening-exercise)
    - [17. Cómo usar adverbios de frecuencia en el presente simple](#17-cómo-usar-adverbios-de-frecuencia-en-el-presente-simple)
    - [18. presente simple Uso de How Often y Auxiliares Do y Does con adverbios de frecuencia](#18-presente-simple-uso-de-how-often-y-auxiliares-do-y-does-con-adverbios-de-frecuencia)
    - [21. Adjetivos VS Adverbios y 6 reglas para crearlos](#21-adjetivos-vs-adverbios-y-6-reglas-para-crearlos)
    - [22. How well + Intensificadores Pretty- Quite - Very - Really (Intensifiers)](#22-how-well--intensificadores-pretty--quite---very---really-intensifiers)
    - [23. Ejercicio de escucha - Diferencia entre LOOK, SEE y WATCH](#23-ejercicio-de-escucha---diferencia-entre-look-see-y-watch)
    - [VERBOS MODALES](#verbos-modales)
      - [24. ¿Qué son los verbos modales?, Verbo Modal CAN](#24-qué-son-los-verbos-modales-verbo-modal-can)
      - [Aprende a diferenciar el verbo Need del Verbo Modal Need](#aprende-a-diferenciar-el-verbo-need-del-verbo-modal-need)
      - [37. Verbo Modal Could en Presente forma Afirmativa, negativa, interrogativa](#37-verbo-modal-could-en-presente-forma-afirmativa-negativa-interrogativa)
      - [Cómo usar Can - Could - Be able to en Presente - Pasado - Futuro](#cómo-usar-can---could---be-able-to-en-presente---pasado---futuro)
      - [Verbo Modal Should y Shouldn't](#verbo-modal-should-y-shouldnt)
      - [25. Cómo utilizar Wh Questions con el verbo modal CAN](#25-cómo-utilizar-wh-questions-con-el-verbo-modal-can)
      - [26. Modal Can + Conjunciones y Adverbios (too, also, and, or, also, only)](#26-modal-can--conjunciones-y-adverbios-too-also-and-or-also-only)
      - [27. Verbo Modal Should en todas sus formas + Wh Questions](#27-verbo-modal-should-en-todas-sus-formas--wh-questions)
      - [28. Can y Should + Pronombres Objeto. Cuándo y cómo usarlos](#28-can-y-should--pronombres-objeto-cuándo-y-cómo-usarlos)
      - [29. Verbo Let + Pronombres Objeto y modales Can y Should](#29-verbo-let--pronombres-objeto-y-modales-can-y-should)
      - [30. Modal WOULD en todas sus formas - Diferencia Can y Should](#30-modal-would-en-todas-sus-formas---diferencia-can-y-should)
      - [32. Desafío de escritura con Would like and Would love](#32-desafío-de-escritura-con-would-like-and-would-love)
      - [WOULD YOU MIND IF PARTE 1](#would-you-mind-if-parte-1)
      - [Would you mind + ING– Polite questions – Estructura y Ejemplos – PARTE 2](#would-you-mind--ing-polite-questions--estructura-y-ejemplos--parte-2)
      - [Must - usar el verbo modal MUST](#must---usar-el-verbo-modal-must)
      - [33. WH Questions con el verbo modal WOULD en presente - Estructura](#33-wh-questions-con-el-verbo-modal-would-en-presente---estructura)
      - [Todo acerca del HAVE TO y HAS TO en PRESENTE, PASADO y FUTURO](#todo-acerca-del-have-to-y-has-to-en-presente-pasado-y-futuro)
    - [MODALES PERFECTOS](#modales-perfectos)
      - [Uso de could have y can have](#uso-de-could-have-y-can-have)
      - [Como usar el modal Would y el modal perfecto Would Have](#como-usar-el-modal-would-y-el-modal-perfecto-would-have)
      - [Modales May have - Might have](#modales-may-have---might-have)
      - [uso de Must Have](#uso-de-must-have)
    - [34. Adjetivos posesivos + Dictado en inglés nivel básico](#34-adjetivos-posesivos--dictado-en-inglés-nivel-básico)
    - [35. Diferencia de preguntas con WHAT y WHICH](#35-diferencia-de-preguntas-con-what-y-which)
    - [36. Preguntas y respuestas con WHICH - Uso de One y Ones](#36-preguntas-y-respuestas-con-which---uso-de-one-y-ones)
    - [38. Cómo usar EITHER y NEITHER para decir TAMPOCO](#38-cómo-usar-either-y-neither-para-decir-tampoco)
    - [39. Técnica de enlace y ejercicio de escucha con preguntas cortas](#39-técnica-de-enlace-y-ejercicio-de-escucha-con-preguntas-cortas)
    - [40 Examen Basico](#40-examen-basico)
    - [41. To be Was y Were - Pasado progresivo o continuo / Uso de ING](#41-to-be-was-y-were---pasado-progresivo-o-continuo--uso-de-ing)
    - [42. Adverbios de tiempo en PASADO y PRESENTE que debes aprender](#42-adverbios-de-tiempo-en-pasado-y-presente-que-debes-aprender)
    - [42.1. Uso de THERE WAS y THERE WERE con HOW MANY y HOW MUCH](#421-uso-de-there-was-y-there-were-con-how-many-y-how-much)
    - [Uso de There con los diferentes tiempos](#uso-de-there-con-los-diferentes-tiempos)
    - [43. Usos de WHILE en el pasado TO BE en oraciones afirmativas](#43-usos-de-while-en-el-pasado-to-be-en-oraciones-afirmativas)
    - [44. Cuando y como usar FOR y AGO con cuantificadores en pasado](#44-cuando-y-como-usar-for-y-ago-con-cuantificadores-en-pasado)
    - [45. How-long/How-long-ago/How-much-time/Since, en pasado TO BE](#45-how-longhow-long-agohow-much-timesince-en-pasado-to-be)
    - [48. Uso de So/Too/So-much/So-many/Too much/Too-many en pasado](#48-uso-de-sotooso-muchso-manytoo-muchtoo-many-en-pasado)
    - [49. Cómo decir y preguntar la hora en inglés](#49-cómo-decir-y-preguntar-la-hora-en-inglés)
    - [50. cinco preposiciones para decir la hora en inglés que debes conocer](#50-cinco-preposiciones-para-decir-la-hora-en-inglés-que-debes-conocer)
    - [53-A. Pasado simple](#53-a-pasado-simple)
      - [52. Ejercicio de escritura en PASADO SIMPLE con omisión de Pronombres Sujeto](#52-ejercicio-de-escritura-en-pasado-simple-con-omisión-de-pronombres-sujeto)
      - [53. Conectores de contraste en PASADO SIMPLE](#53-conectores-de-contraste-en-pasado-simple)
      - [55. Cómo usar las Wh Questions en Pasado Simple](#55-cómo-usar-las-wh-questions-en-pasado-simple)
      - [56. Diferencia entre Preguntas Sujeto y Objeto en Pasado Simple](#56-diferencia-entre-preguntas-sujeto-y-objeto-en-pasado-simple)
    - [57. Futuro Will y Going to - Cómo diferenciarlos fácilmente en todas sus formas](#57-futuro-will-y-going-to---cómo-diferenciarlos-fácilmente-en-todas-sus-formas)
    - [59. REFORZAR: 5 formas diferentes a WILL y GOING TO para hablar en futuro](#59-reforzar-5-formas-diferentes-a-will-y-going-to-para-hablar-en-futuro)
    - [60. Preposiciones de tiempo IN - ON - AT - Cómo y cuándo usarlas](#60-preposiciones-de-tiempo-in---on---at---cómo-y-cuándo-usarlas)
    - [Estas son las 5 formas de usar el ING o el GERUNDIO](#estas-son-las-5-formas-de-usar-el-ing-o-el-gerundio)
    - [Gerundio VS Infintivo – ¿Cómo usarlos? Diferencias y ejemplos detallados](#gerundio-vs-infintivo--cómo-usarlos-diferencias-y-ejemplos-detallados)
    - [Cómo utilizar preposiciones en preguntas](#cómo-utilizar-preposiciones-en-preguntas)
    - [SUBJECT VERB AGREEMENT](#subject-verb-agreement)
    - [EXAMEN Ingles basico](#examen-ingles-basico)
  - [B1. Pre-Intermedio](#b1-pre-intermedio)
    - [1. Tiempo Presente Perfecto, uso de HAVE y HAS](#1-tiempo-presente-perfecto-uso-de-have-y-has)
    - [2. Presente Perfecto, Uso de auxiliares HAVE y HAS, Diferencia entre SINCE y FOR](#2-presente-perfecto-uso-de-auxiliares-have-y-has-diferencia-entre-since-y-for)
    - [3. Use of BECAUSE, AS, SINCE y FOR to give arguments](#3-use-of-because-as-since-y-for-to-give-arguments)
    - [3.A. Stative Verbs, Definición y Ejemplos, Incluye listado](#3a-stative-verbs-definición-y-ejemplos-incluye-listado)
    - [4. Relative Clauses and Relative Pronouns](#4-relative-clauses-and-relative-pronouns)
    - [4. Presente Perfecto vs Pasado Simple, aprende a diferenciarlos](#4-presente-perfecto-vs-pasado-simple-aprende-a-diferenciarlos)
    - [5. Uso de BE ABLE TO en el Presente Perfecto, CAN y COULD](#5-uso-de-be-able-to-en-el-presente-perfecto-can-y-could)
    - [6. Dictado en Presente Perfecto](#6-dictado-en-presente-perfecto)
    - [present perfect vs present perfect continuos/progresive](#present-perfect-vs-present-perfect-continuosprogresive)
    - [8. Uso de HOW LONG en Presente Perfecto Continuo o Progresivo](#8-uso-de-how-long-en-presente-perfecto-continuo-o-progresivo)
    - [Uso de Wh-Questions en Presente Perfecto Continuo o Progresivo](#uso-de-wh-questions-en-presente-perfecto-continuo-o-progresivo)
    - [Uso de There was con tiempos](#uso-de-there-was-con-tiempos)
    - [10. Diferencia y uso de THERE HAS BEEN y THERE HAVE BEEN](#10-diferencia-y-uso-de-there-has-been-y-there-have-been)
    - [PRUEBA 1 - PRE-INTERMEDIO](#prueba-1---pre-intermedio)
    - [11. Diferencia entre Presente Perfecto y Pasado Perfecto](#11-diferencia-entre-presente-perfecto-y-pasado-perfecto)
    - [12. Pasado Simple/tiempo preterito y Pasado Perfecto - Conjunciones / Conectores](#12-pasado-simpletiempo-preterito-y-pasado-perfecto---conjunciones--conectores)
    - [13. Pasado Perfecto Progresivo o Continuo con Phrasal Verbs](#13-pasado-perfecto-progresivo-o-continuo-con-phrasal-verbs)
    - [14. Ejercicio de escritura en Pasado Perfecto y Pasado Perfecto Continuo](#14-ejercicio-de-escritura-en-pasado-perfecto-y-pasado-perfecto-continuo)
    - [15. Dictado en Pasado Perfecto y Pasado Perfecto Continuo](#15-dictado-en-pasado-perfecto-y-pasado-perfecto-continuo)
    - [16. Futuro Perfecto y Presente Simple usando BY, BEFORE, ALREADY y YET](#16-futuro-perfecto-y-presente-simple-usando-by-before-already-y-yet)
    - [17. Futuro Perfecto Progresivo o Continuo con Adverbios y Estructuras](#17-futuro-perfecto-progresivo-o-continuo-con-adverbios-y-estructuras)
    - [Cómo preguntar y responder con WH QUESTIONS en Futuro Perfecto Progresivo](#cómo-preguntar-y-responder-con-wh-questions-en-futuro-perfecto-progresivo)
    - [19. Aprende a diferenciar el Futuro Perfecto del Futuro Perfecto Progresivo](#19-aprende-a-diferenciar-el-futuro-perfecto-del-futuro-perfecto-progresivo)
    - [20. Una hora de clase con todos los tiempos verbales y auxiliares en todas sus formas](#20-una-hora-de-clase-con-todos-los-tiempos-verbales-y-auxiliares-en-todas-sus-formas)
    - [PRUEBA 2 - PRE-INTERMEDIO](#prueba-2---pre-intermedio)
  - [B2. INGLÉS INTERMEDIO](#b2-inglés-intermedio)
    - [2. Affirmative Statements with Professions. Listen and classify the ques / Comprehension](#2-affirmative-statements-with-professions-listen-and-classify-the-ques--comprehension)
    - [N. Cuando usar DON’T HAVE y HAVEN’T, DOESN’T HAVE y HASN’T – Diferencias](#n-cuando-usar-dont-have-y-havent-doesnt-have-y-hasnt--diferencias)
    - [Pronombres indefinidos](#pronombres-indefinidos)
      - [Pronombres indefinidos para LUGARES, Cuando y como usar SOMEWHERE- ANYWHERE - EVERYWHERE - NOWHERE](#pronombres-indefinidos-para-lugares-cuando-y-como-usar-somewhere--anywhere---everywhere---nowhere)
      - [Pronombres indefinidos para PERSONAS: Uso de SOMEONE, ANYONE, EVERYONE, NO ONE](#pronombres-indefinidos-para-personas-uso-de-someone-anyone-everyone-no-one)
      - [Pronombres Indefinidos para Cosas - Uso de Something, Anything, Everything y Nothing](#pronombres-indefinidos-para-cosas---uso-de-something-anything-everything-y-nothing)
    - [Uso de Whatever - Wherever - Whenever - However - Whichever - Whoever](#uso-de-whatever---wherever---whenever---however---whichever---whoever)
    - [6. Old habits with USED TO – Affirmative, Negative, Interrogative, and Answers](#6-old-habits-with-used-to--affirmative-negative-interrogative-and-answers)
    - [7. How to use USED TO with WH QUESTIONS – What, Where, Who, How, Why, etc](#7-how-to-use-used-to-with-wh-questions--what-where-who-how-why-etc)
    - [8. There used to be in affirmative, negative, interrogative, and answers](#8-there-used-to-be-in-affirmative-negative-interrogative-and-answers)
    - [9. Difference between There used to be, There was, and there were](#9-difference-between-there-used-to-be-there-was-and-there-were)
    - [10. Writing exercise by translating from Spanish to English](#10-writing-exercise-by-translating-from-spanish-to-english)
    - [TEST 1 - INTERMEDIATE](#test-1---intermediate)
  - [Extras](#extras)
    - [Cuando usar TO antes de un verbo y cuando NO usarlo y diferencias con el FOR](#cuando-usar-to-antes-de-un-verbo-y-cuando-no-usarlo-y-diferencias-con-el-for)
    - [Aprende a diferenciar OF y FROM con estos 8 ejemplos](#aprende-a-diferenciar-of-y-from-con-estos-8-ejemplos)
    - [Así de fácil es usar ALREADY, STILL y YET](#así-de-fácil-es-usar-already-still-y-yet)
    - [Aprende a usar el verbo GET en inglés con estas 3 bases](#aprende-a-usar-el-verbo-get-en-inglés-con-estas-3-bases)
    - [Explicación detallada de la VOZ ACTIVA y PASIVA en inglés - Passive Voice](#explicación-detallada-de-la-voz-activa-y-pasiva-en-inglés---passive-voice)
    - [Uso del artículo THE en inglés - Cuando NO usarlo](#uso-del-artículo-the-en-inglés---cuando-no-usarlo)
    - [Cuándo usar y diferenciar WAS – WERE – DID – Pasado to be VS Pasado Simple](#cuándo-usar-y-diferenciar-was--were--did--pasado-to-be-vs-pasado-simple)
    - [Uso de It](#uso-de-it)
    - [Condicionales](#condicionales)
      - [Condicional 0](#condicional-0)
      - [Condicional 1](#condicional-1)
      - [Condicional 2](#condicional-2)
      - [Condicional 3](#condicional-3)
    - [Stative Verbs – Definición, Ejemplos y Lista](#stative-verbs--definición-ejemplos-y-lista)
    - [¿Qué debo usar? ALONE - ONLY - JUST](#qué-debo-usar-alone---only---just)
    - [10 preposiciones de movimiento que debes aprender - Diferencia IN - INTO y más](#10-preposiciones-de-movimiento-que-debes-aprender---diferencia-in---into-y-más)
    - [Cuándo y cómo usar EVEN - NOT EVEN - EVEN SO - EVEN IF - EVEN THOUGH](#cuándo-y-cómo-usar-even---not-even---even-so---even-if---even-though)
    - [REPORTED SPEECH = INDIRECT SPEECH](#reported-speech--indirect-speech)
      - [Cómo usarlo](#cómo-usarlo)
      - [Practica Reported Speech](#practica-reported-speech)
    - [Instead of vs Instead](#instead-of-vs-instead)
    - [Que significa GONNA, LEMME, GIMME – Contracciones informales](#que-significa-gonna-lemme-gimme--contracciones-informales)
    - [Uso de Wanna, Wansta, Hafta y Hasta - cómo contracciones informales](#uso-de-wanna-wansta-hafta-y-hasta---cómo-contracciones-informales)
    - [Cuándo y cómo usar HAD BETTER en todas sus formas](#cuándo-y-cómo-usar-had-better-en-todas-sus-formas)
  - [Avanzado](#avanzado)
    - [Verbos Causativos - Cómo usarlos en voz activa y pasiva](#verbos-causativos---cómo-usarlos-en-voz-activa-y-pasiva)

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

## Accesos

a, www.pacho8a.com

### ToDo

- Diferencias entre borrow y lend
- Agregar actividad de escuchar y escribir
- TODO
  - https://www.pacho8a.com/ingl%C3%A9s/be-left-have-left/
  - https://www.pacho8a.com/ingl%C3%A9s/herramientas-de-pr%C3%A1ctica/

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

## A1. Basico

### 4 - Presente continuo, progresivo o gerundio afirmativo - Reglas ING

- Fuente: [pacho8a](https://www.pacho8a.com/ingl%C3%A9s/curso-ingl%C3%A9s-desde-cero/lecci%C3%B3n-4/)
- Resumen: [Imagen](./img/01_gerundio.jpg)

```r
Como se distribuye is y are, y ejemplos;"
Is, es para singular y no contables.

Are, es para plural y contables.


1. Donde estan ellos? Where ARE they?

2. Donde esta el? Where IS he?

3. Milk IS produced by some animals
"

Presente progresivo: Como se identifica, equivalente en español, Ejemplo;"
Como se identifica: Cuando al verbo se le agrega un ing.

Equivalente en español: Los verbos que terminan en *-ando o -endo*

Esp: Corre, Corriendo, escribe, escribiendo
Ing: Run  , Running  , write  , Writting

Ejemplo: I am cooking = estoy cocinando
"

Cuales son las 3 reglas de convertir a verbo en jerundio;"

1. Cuando termina en consonante, solo agregar ing
  - Buy = Buiying
  - Go = Going
  - Read = Reading

2. Cuando termina en ´e´ sustituirla
  - Beliv´e´ = Beliv´ing´
  - Practic´e´ = Practic´ing´

3. Cuando termina en cvc (consonante-vocal-consonante) se duplica la
ultima y agregamos
  - trotar = Jog = Jogging
  - Picar = chop = Chopping
  - Comprar = Shop = Shopping
  - Exepciones cuando terminan en `w x, y, z`
    - Draw = Drawing
    - Play = Playing
"

Estoy caminando;"
i am walking"

Mi hermano Carlos esta en la cocina, el esta cocinando con su esposa;"
My brother Carlos is in the kitchen, he is coocking with his wife
"

Jenny esta con `su` padre `en` el sofa. Ellos estan `viendo` TV;"
Jenny is with `her` father `on` the sofa, they are `whatching` TV
"

Write, Study;"
Writting, Studying
"

Luis y Lady están en la casa, en el comedor, en la silla, en la mesa. Ellos están comiendo pizza;"
Luis and Lady are at/in the house, in the kitchen/diningroom, on the chair, at the table, they are eating pizza.
"

Nosotros estamos en la escuela, en el salón de clase y el profesor está escribiendo en el tablero;"
We are `at-in` the school, in the classroom and the teacher is writing on the board.
"

Jenny está con su padre en la sala en el sofá. Jenny está viendo televisión y su padre está leyendo;"
Jenny is with her father in the `restroom-living room` on the sofa, Jenny is watching tv and her father is reading
"

Gloria está en el parque jugando con su perro y Ana está en el jardín sembrando flores;"
Gloria is `at-in` the park playing with her dog and Ana is at the garden planting flowers
"

John and Marcos are at the garage. They are fixing the car;"
John y Marco estan en la cochera, ellos estan arreglando el carro.
"

Rita is with her friend Steve in the bedroom, on the bed, they are studying English and reading a book;"
Rita esta con su amigo Steve en el dormitorio, `en-sobre` la cama, ellos estan estudiando ingles y leyendo un libro.
"

The cat is eating fish and the dog is eating meat. The dog is in the garden and the cat is on the bed;"
El gato esta comiendo pescado y el perro esta comiendo carne. El perro esta en el jardin y el gato esta en la cama
"

Cesar, Steve, and John are in a restaurant. They are on a chair at a big table eating pizza;"
Cesar, Steve and John estan en un restaurante. Ellos estan en una silla en la mesa grande comiendo pizza
"

I am in my house with my friend Gloria. We are watching TV and eating pizza;"
Estoy en mi casa con mi amiga Gloria. Estamos mirando TV y comiendo pizza
"

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 7. Uso de Who, Why y Because con el verbo to be en presente

```r
-Por que estas estudiando ingles -Por que quiero viajar;"
-Why are you Studing english? -because i want to travel
"

-¿La pizzarra es negra? -No, no lo es -¿Porque no? -Por que la pizzara es blanca;"
-Is the board black? -No, it isn´t -Why not? -Because the board is white"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 8.1 A vs An

```r
Que significa ´a´ y ´an´, y en que se difencian?;"

Significa un o una, y la diferencia es como en español cuando usamos
un ´y´ o ´e´, `Maria y Isabela` se escucha feo, en cambio si samos
`Maria e Isabela` se escucha mejor.

Se usa ´a´ cuando termina en consonante ej. ´A Car, A Woman´

Se usa ´an´ cuando termina en consonante ej. ´An Apple, An orange´

"

(usa a o an) _ Car, _ Woman, _ elevator;"
A Car, A Woman, An elevator
"

(usa a o an) _ hour, _ university, _ hospital;"
- An hour (tiene sonido de vocal)
- A university (Suena la letra `y`)
- An hospital. (Suenal la letra `j`)
"

```

### 8.2 There is - There are (Afirmativa, negativa, interrogativa)

```r
Diferencia entre ´there is´, ´there are´;"
´there is´ es para singlular, y ´there are´ para plural, ej.
There is a dog / There are two dogs
"

No hay dos perros;"
There´s not a dog
"

No hay un perro;"
There are not 2 dogs  
"

-¿Hay dos perros?, -Si, los hay. No, no los hay;"
-Are there two dogs?. -Yes, there are. -No, there aren´t.
"

En mi casa, sobre la mesa, hay dos manzanas;"
In my house, on the table, there are two apples
"

En el parque hay dos gatos, y hay un perro, ellos estan corriendo;"
In the park there are two cats, and there´s a dog, they are running
"

¿hay 4 baños en tu casa?. No, no hay cuatro baños;"
Are there four bathrooms in your house?. No, there aren´t
"

En mi casa, en la sala hay muchas cosas;"
In my house, in the living room there are many things.
"

Hay un reloj en la pared. Hay una silla al lado de la mesa;"
There is a clock on the wall. there´s a chair at the table,
"

sobre la mesa hay un libro, hay una lámpara y hay un florero;"
over the table there is a book, there is a lamp and there´s a vase.
"

En el florero hay 5 flores. En la sala hay un sofá;"
in the vase there are five flowers. In the living room there´s a sofa,
"

sobre el sofá hay 2 cojines y al lado del sofá hay un gato;"
on the sofa there are two cushions and *(Mmm-NO|next to it of)at* the sofa there´s a cat.
"

En el piso hay una pelota y hay una alfombra;"
*(NO|In)On* the floor there´s a ball and a carper
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 9. Adjetivos calificativos

```r
Que es un adjetivo 2;"
Son los que describen a alguien o algo, por ej. pueden responden a dos
preguntas ¿Como es? y ¿como estan?

El perro cafe: ¿como es el perro? =cafe es nuestro adjetivo
  -The brown dog

Un dia nublado ¿como esta el clima? = nublado es nuestro adjetivo
  -A cloudy day
"

Que sifnifica el acronimo O.T.E.F.C.O.M.P y a que hace referencia;"
Hace referencia a identificar los adjetivos

O. Opinion
T. Tamañp
E. Edad
F. forma
C. Color
O. Origen
M. Material
P. Proposito
"


En la frase frase `en el gareje hay un carro rojo y dos carros azules` ¿cuales son los adjetivos?;"
los colores son los adjetivos.
"

Como se usan los adgetivos en español y en ingles;"
En el espa;ol primero es el sujeto y luego el adjetivo, por ej.
`El carro rojo` donde carro es el sujeto y rojo el adjetivo.

En ingles es al revez (`adjetive + subject`), por ej. `the red car`
"

Los adjetivos pueden in en plural, y da un ej. de por que no;"
No, Decir que los `Los carros son azules/the blues cars` seria incorrecto
"

En la sala hay una mesa empolvada;"
In the living room there is a *dusty* table
"

Mi padre es flaco y mi madre es flaca tambien;"
My father is thin and my mother too.
"

En el comedor hay una silla plateada;"
In the dining room there´s a silvery chair.
"

Hay una mesa vieja. Sobre la mesahay 3 objetos;"
There´s *an* old table.  Over the table there are three objects
"

Hay un libro azul, hay una lámpara amarilla y hay un florero nuevo;"
There´s a blue book, there´s a yellow lamp and a new vase.
"

Dentro del florero hay 3 hermosas flores;"
in the vase there are three beautiful flowers.
"

En el piso hay una pelota roja y sobre la pared hay un reloj blanco;"
On the floor there´s a red ball and on the wall there´s a white clock
"

En la sala hay un sofá viejo;"
In the living room there is *an* old sofa
"

Sobre el sofá hay dos cojines;"
On the sofa there are two cushions
"

hay un cojín amarillo y hay un cojín blanco;"
there´s a yellow cushion and there is a white cushion.
"

Mi gato Tom está al lado del sofá. Mi gato es negro;"
My cat Tom is at the sofa. My cat is black.
"

En el piso hay una alfombra empolvada;"
On the floor there´s a dusty carpet.
"

Pacho tiene una calculadora vieja, cuadrada, negra y plastica;"
Pacho has an old, square, black and plastic calculator
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 10. Uso de How Many para preguntar cantidades contables

```r
(traduccion) how many / how much;"
how many: cuantos/cuantas
how much: cuanto/cuanta
"

#uso de there is y there are, #uso de there are y there is
Diferencia entre there is y there are;"
There is: singular
There are: plural
"

¿Cuantas sillas azules hay en la cocina?;"
How many blue chairs are there in the kitchen.
"

-¿Cuantos libros viejos hay sobre la mesa? -Hay dos libros sobre la mesa;"
-How many old books are there on the table?. -There are two books on the table
"

-¿Cuantas flores hay en el jardin? -Hay tres flores;"
-How many flowers are there in the garden?. -There are 3 flowers in the garden
"

¿Cuantas pelotas rojas hay en la caja? -Hay una pelota roja;"
How many red balls are there in the box?. -There is a red ball in the box
"

¿Cuantos sofas viejos hay en la sala?. -hay cuatro sofas viejos en la sala;"
How many old sofas are there in the living room?. -There are four old sofas in the living room
"

¿Cuantos relojes blancos hay en la pared?. -hay un reloj blanco en la pared;"
-How many white clocks are there on the wall?. -There is one white clock on the wall
"

-¿Cuantos gatos pequeños hay en la cama?. -Hay cuatro.;"
How many small cats are there *on* the bed?. -there are four
"

-¿Cuantos cojines viejos en el sofa?. -hay uno;"
How many new cushions are there on the sofa?. -there is one
"

-¿Cuantas lamparas caras hay en la sala?. -Hay seis;"
How many expensive lamps are there in the living room?. -there are six
"

-¿Cuantos carros rojos hay en el garaje? -Hay dos ;"
-How many red cars are there in the garage?. -there are two
"

-¿Cuantos melones deliciosos hay en la tabla?. -Hay uno.;"
How many delicious mellons on the table?. -there is one
"

-¿Cuantas aves hay volando en el cielo?. -Hay dos aves volando en el cielo.;"
How many birds are there flying in the sky?. -there are two birds flying in the sky
"

-¿Cuantas mesas hay en el salon?. -Hay una;"
how many round tables are there in the *classroom*. -there is one
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 11. How much con sustantivos no contables + verbo TO BE

```r
-Cuanto dinero (no se puede contar) hay en la mesa?. -Hay algo de dinero en la mesa;"
-How much money is there on the table?. -There´s some money on the table
"

Cuanta harina hay sobre la mesa?. -Hay un poco;"
-How much flour is there on the table? -There´s a little
"

-Cuanta arena hay en el desierto? -Hay un monton de arena;"
-How much sand is there in the desert? -There´s a lot of sand in the desert.
"

-Cuanto aceite hay en el sarten?. -Hay un poco de aceite;"
-How Much oil is there in the frying pan? -There´s a little bit of oil
"

-¿Cuanto Jugo de naranja ahi en el vaso?. -No hay nada de jugo de naranja;"
How much orange juice is there in the glass?. -There´s nothing of orange juice.
"

-Cuanta sal hay en el huevo? -hay una pizca de sal;"
-How much salt is there in the egg? -There´s a pinch of salt in the egg?
"

Hola Alex ¿Cuántas manzanas verdes hay en la nevera?;"
Hi alex, How many green apples are there in the fridge?
"

Hola Erika. En la nevera hay dos manzanas verdes y hay una roja;"
Hi Erika, In the fridge there are 2 green apples and one red apple
"

¿Cuánta carne hay en el congelador?;"
How much meat is there in the freezer?
"

En el congelador hay 1 kg de carne y hay 1 libra de pollo;"
There is 1Kg of meat and one pound of chiken in the freezer.
"

¿Cuánto jugo hay en la jarra?;"
How much juice is there in the jar?
"

Hay un poquito de jugo de naranja, pero hay 1 litro de leche;"
There´s a little bit of orange juice, but there is 1 liter of milk.
"

Con gusto Erika;"
You´re welcome Erika
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 12, 13: Uso de This, That, These, Those

```r
this: ¿traduccion?, ¿cerca o lejos?, ¿plural o singular?, ¿ejemplo?;"
¿Traduccion?
. -esto, esta, este
_
¿cerca o lejos?
. -cerca
_
¿plural o singular?
. -Singular
_
¿Ejemplo?
. -this is a cellphone, in my hand
"

that: ¿traduccion?, ¿cerca o lejos?, ¿plural o singular?, ¿ejemplo?;"
¿Traduccion?
. -eso, esa, ese
_
¿cerca o lejos?
. -Lejos
_
¿plural o singular?
. -Singular
_
¿Ejemplo?
. -That in the sky is a airplane
"

There: ¿traduccion?, ¿ejemplo?;"
¿Traduccion?
. -estas, estos
_
¿Ejemplo?
. -There are two/many cars.
. -There is one/a car
"

Como se pronuncia this y como there;"
Se pronuncian igual los diferenciamos por el uso de `is` o `are` en la
oracion.
"

Those: ¿traduccion?, ¿cerca o lejos?, ¿plural o singular?, ¿ejemplo?;"
¿Traduccion?
. -esos, esas
_
¿cerca o lejos?
. -Lejos
_
¿plural o singular?
. -Plural
_
¿Ejemplo?
. -Those are two cars.
"

¿Esos(lejos) son sus aretes grandes?. -No, esos son sus aretes pequeños;"
Are those her big earrings? -No, those are her small earrings.
"

-¿Esos(lejos) son sus vecinos ruidosos? -No, esos son vecinos callados;"
-Are those your noisy neighbors? -No, those are your quiet neighbors
"

-¿Es este su reloj caro. -No, este es su reloj barato;"
-Is this his expensive watch?. -No, this is his cheap watch
"

-¿Son estas preguntas faciles?. -No, esas son respuestas faciles;"
-Are these the easy questions?. -No, these are the easy questions.
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 14. El presente simple y sus reglas (S-ES-IES)

```r
¿Para que se usa el presente simple, y ejemplo?;"
para actividades que se hacen de forma rutinaria o diaria, por ejemplo,
`yo estudio`, `el trabaja`, `ellos comunmente cocinan en`
"

En presente simple cuando se agrega la terminaciones (S-ES-IES) y que excepciones tiene;"
Cuando hablamos en `tercera persona en singular` es decir `el, ella, y eso`.

Hacemos excepcion cuando se usa modales como **can, must, should**,
ej. no se pueden poner cans, musts, etc.
"

(P.S. 3per Singular) cuando se agrega la terminacion `s` a un verbo;"
Cuando termina en consonante o vocal.

- He drink**s** water
- she drink**s** water
- Maria --plays-- with the dog.
"

En P.S. 3per Singular cuando se agrega el *es*;"
Cuando terminan en o-x-z-s-ss-sh-ch

- He watch**es** the TV
- She search**es** the TV
- Peter studies in the livingroom
"

En P.S. 3per Singular cuando se le agrega *ies*;"
(Cuando termina con consonante y luego *y*, ej. study, cry, fly

- He cries when cut onions.
- The bird eats and flies
"

Cuando se usa el *to* ej. el quiere jugar?;"
Cuando tenemos dos verbos comunes juntos, ej. *he wants to play*, con la
excepcion de los verbos modales, ej. *he can to play* es incorrecto, por
que can es un verbo modal"

# PRESENTE SIMPLE
Mis hijas juegan con sus juguetes / Mi hija juega con sus juguetes;"
My daughters play with their toys / My daughter plays with her toys"

Miguel pesca en el rio / Nosotros pescamos en el rio;"
Miguel fishes in the river / we fish in the river"

El chef mezcla los ingredientes / Mi madre y yo mesclamos los ingredientes;"
The chef mixes the ingredients / My mother and i mix the ingredients"

Peter tiene clases de ingles hoy / Ustedes tienen clases de ingles hoy;"
You have english class today / Peter has english class today"

(P.S. 3per Singular) Change, Brush, Speak;"
Changes, Brushes, Speaks
"

(P.S. 3per Singular) Sing, Dance, Watch;"
Sings, Dances, Watches
"

(P.S. 3per Singular) Have, Like, Cry;"
Has, Likes, Cries
"

(P.S. 3per Singular) Sleep, Do, Fly;"
Sleeps, Does, Flies
"

(P.S. 3per Singular) Prepare, Fix, Know, Feel;"
Prepares, Fixes, Knows, Feels
"

Mi hermana Alejandra habla inglés, pero mis hermanos Andrés y David hablan francés;"
My sister Alejandra speaks english, but my brothers Andres and David speak french"

A él le gusta bailar salsa y a su esposa le gusta bailar tango;"
He likes *to* dance salsa and his wife likes *to* dance tango"

Yo cepillo mis dientes todos los días;"
I brush my teeth every day"

Nosotros vemos televisión en la sala, en el sofá. Mi mamá ve televisión en su habitación sobre le cama;"
We watch TV in the living room, on the sofa. Mi Mom watches TV in *her* room on the bed"

Edna tiene un bebé. El bebé llora todo el día. Edna canta canciones y su bebé duerme;"
Edna has a baby. He cries all day. Edna sings songs and *her* baby sleeps"

Nosotros hacemos 5 ejercicios, pero el profesor hace 10;"
We do 5 exercises, but *the* profesor does 10"

Mi abuelo y mi padre están en el garaje. Ellos arreglan el carro y mi mamá prepara el almuerzo;"
My grandfather and my father are in the garaje, they fix the car and my mom prepares the lunch"

Yo me siento bien. Ella se siente bien. Él se siente bien y nosotros nos sentimos bien;"
I feel well. She feels well. He feels well and we feel well"

En mi casa, en el jardín hay un pájaro. El pájaro come y vuela muy feliz;"
In my house, in the garden there is a bird. The bird eats and flies very happy"

Ella sabe las respuestas porque ella practica inglés todos los días en su casa;"
She knows the answers because she practices every day in her house"

Quiero decirte algo sobre un amigo mio;"
I want to tell you something about a friend of mine"

Que significa noon y afternoon;"
noon=medio dia, afternoon=despues de medio dia"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 15. El presente simple y sus auxiliares DO y DOES (Interrogativo y negativo)

Cuando queremos usar el presente simple en forma interrogativo o negativo,
nos ayudara el uso del Do y Does, siendo este ultimo el de tercera
persona en singular. Es decir.

- Do: I, We, You, They
- Does: She, he, it

Algunas veces se utilizan el do y does en la forma afirmativa para
enfatizar la oracion.

**Cuidado:** Cuando usamos does ya no modificamos el verbo a su forma
presente simple 3ra persona singular.

Ejemplos Negativos

- He does not play soccer / He doesn´t play soccer
- They don´t eat meat / I doesn´t eat meat

Ejemplos Interrogativos

- Do i play soccer / Does he play soccer? R.- Yes, he does / No, he doesn´t
- Do they eat meat? / Does he eat meat? R.- Yes, he does / No, he doesn´t

Ejemplos Interronegativas

- Don´t i play soccer / Doesn´t he play soccer?
- Don´t they eat meat? / Doesn´t he eat meat?

Ejercicios:

```r
#- PRESENTE SIMPLE Interrogativo y negativo
Como se distribuyen las pronombres el `Do` y `Does`;"
Do: I, We, You, They / Does: She, he, it"

En el presente simple cuando para que se usa el Do y Does;"
1) Crear oraciones en negativo o interrogativo, 2) enfatizar oraciones"

(P.S. Version interrogativa y negativa) They like to eat pizza;"
<?>Do they like to eat pizza / <->They don´t like to eat pizza"

(P.S. Version interrogativa y negativa) My friend Joseph lives in japan;"
<?>Does my friend Joseph live in japan? / <->My friend joseph doesn´t live japan"

(P.S. Version positiva y negativa) Does she study english today?;"
<+>She studies english today / <->She doesn´t study english today"

(P.S. Version positiva y negativa) Do you have pets in your house?;"
<+>You have pets in your house / <->You don´t have pets in your house"

(P.S. Version interrogativa y negativa) John sleeps on his bed;"
<?>Does John sleep on his bed? / <->John doesn´t sleep on his bed"

(P.S. Version interrogativa y negativa) They speak English every day;"
<?> Do They speak english every day? / <->They don´t speak english every day"

(P.S. Version interrogativa y negativa) Her son does the homework with his friend;"
<?>Does Her son do the homework with his friend? / <->Her son doesn´t do the homework with his friend"

(P.S. Version interrogativa y negativa) They watch a movie in the living room;"
<?>Do they watch a movie in the living room? / <->They don´t watch a movie in the living room "

(P.S. Version interrogativa y negativa) The plane flies very high;"
<?> Does the plane fly very high? / <->The plane doesn´t fly very high"

(P.S.) -Peter study at school- Does Peter work in the supermarket?;"
No, because Peter studies at the school"

(P.S.) -In the morning my mother prepares the breakfast- Does the mother prepare the lunch in the morning?;"
No, because the mother prepares the breakfast in the morning"

(P.S.) -Peter eat the breakfast with my brother- Do Peter and his brother eat breakfast together?;"
Yes, because they eat the breakfast together"

(P.S.) -Peter`s brother studies at the university- Do Peter and his brother study at the university?;"
No, because Peter Studies at the school, and peter´s brother studies at the university"

(P.S.) -Peter`s brother studies International Business- Does the brother study English?;"
No, because he studies international business"

(P.S.) -Peter`s mother doesn`t work in the supermarket but she works in the house- Does the mother work in the supermarket?;"
No, because she works in the house"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 16. Ejercicio de escucha en presente simple - Listening Exercise

En la fuente del cuarso (esta hasta arriba) buscar el audio.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 17. Cómo usar adverbios de frecuencia en el presente simple

```r
#- dias, meses, estaciones
Lunes, Martes Miercoles;"
Monday, Tuesday, Wednesday"

Jueves, Viernes;"
Thursday, Friday"

Sabado, Domingo;"
Saturday, Sunday"

enero, febrero, marzo;"
January, February, March"

abril, mayo, junio;"
April, May, June"

julio, agosto, septiembre;"
July, August, September"

octubre, noviembre, diciembre;"
October, November, December"

primavera, verano, otoño, invierno;"
spring, summer, fall, winter"

#- adverbios, presente simple, frecuencia
Que es un verbo (adverb=adverbio) y Cual es su orden;"
ad (agregar), verb(verbo), es decir, le agrega informacion al verbo,
Orden: Subject + Adverb + Verb
ej, She never swims on Mondays, donde never es el adverb."

Cuales son los adverbs of frequency  = adverbios de frecuencia;"
Always
Usually
Frequently
Never
Sometimes
Seldom (pocas veces)
Hardle ever (casi nunca)
"

Cuales son los periods of time = periodos de tiempo;"
for ... = ej. for 2 days, for one year
for more than = Por mas de ...
for over ... = por ensima de ...
during ...  = durante ...
"

Cuales son los past adverbs of time in past = adverbios de frecuencia en el pasado;"
long time ago = long ago = hace mucho tiempo
A few XXX ago = hace unos XXX = hace un par de XXX
In the past = en el pasado
"

I always work __(in/on/at) mondays;"
I always work on monday
"
Ej. ´, los dias de la semana siempre usan in, on, at;"On"

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 18. presente simple Uso de How Often y Auxiliares Do y Does con adverbios de frecuencia

```r
#- presente simple, Do/Does
estructura de preguntas con adverbs;"Auxiliar(Do) + Subject + Adverb + verb + Complement?
ej. Does she always go to the gym on Fridays?."

-¿Tu siempre comes pizza los sabados? -No, yo no como siempre pizza los Sabados;"
-Do you always eat pizza on Saturdays?, -No, I Don´t always eat pizza on saturdays."

# que son las
Cuales son las Wh-Questios ó open questions;"
Las que no son preguntas cerradas tipo ´-¿tu comes pizza? R-Si/No´, estas
piden informacion, ej. ´¿Cada cuanto comes pizza? R-Cada semana´,
"

# lista de wh-questions
Enlista 8 wh-questions y su traduccion;"
why=¿Por que? because=porque,
where=donde,
when=cuando,
what=que,
which=cual,
how=como,
how often=cada cuanto
Who=quien
"

Posibles respuestas de ´why´ y ejemplo;"
=Because
=To/For + [Reason]

¿Por que estudias ingles?
Because: Por que quiero viajar
  because I want to travel
To: Estudio ingles *PARA* eseñarlo
  I study englisg *TO* teach it
  I study english *FOR* teaching it

TODO: como funcionan los verbos y el for
"

Posibles respuestas de ´where´ y ejemplo;"
=Place
=Preposition of place

¿A donde vas?
Place: Al gimnacio=To the gym

¿Donde es la fiesta?
Prep Of Place: En mi casa=At my house
"

¿Cada cuanto comes pizza?;"
How often do you eat pizza
"

-¿Va ella a trabajar los fines de semana?;"
Does she go to work *on* weekends?"

-¿Cada cuanto ellos viajan a miami? -Ellos viajan cada año;"
How often *do* they travel to miami? -They travel every year"

¿Siempre desayunas en tu casa?;"
Do you always *have* breakfast in your house?"

¿Cada cuánto desayunas en tu casa?;"
How often do you have breakfast in your house?"

¿Él toma leche a menudo?;"
Does he *often drinks*  milk?"

¿Qué tan a menudo toma él leche?;"
how often does he drink milk?"

¿Van ellos frecuentemente al gimnasio?;"
How frequently do they go to the gym? != Do they go frequently to the gym?"

¿Trabajan ellos normalmente los fines de semana?;"
Do they usually  work *on* weekends?"

¿Cada cuánto trabajan ellos los fines de semana?;"
How often do they work on weekends?"

¿Estudias inglés muy a menudo?;"
Do you almost always study english?"

¿Cada cuánto estudias inglés?;"
how often do you study english?"

(corrige) I always goes to the supermarket with my mother;"
I always *go*..."

(corrige) She eat always apples and bananas;"
She *always eats*..."

(corrige) They doesn´t go usually to the movies;"
They don´t usually go..."

(corrige) Rachael and Mark frequently plays tennis the Sundays;"
Rachael and mark frequently play tennis on Saturdays"

(corrige) How often does you practice English?;"
How often *do* you..."

#---------------------------------
"Selecciona las que describen la siguiente oracion
[Una sala con una mesa que tiene tres objetos, 1 libro, 1 lamparara
amarilla y 1 florero. En el piso de la sala hay una pelota roja, 1
alfombra y 1 gato. Tambien hay un sofa con dos cojines.]
1. There`s a yellow lamp on the table.
2. There`s a box at the table.
3. There are 3 different objects on the table.
4. There`s a red ball on the floor.
5. There are two cushions on the sofa.
6. There are 1 carpet on the floor.
7. There`s a chair and there`s a table.
8. There are 5 beautiful flowers on the vase.";"1,3,4,5,7"
#---------------------------------

"¿Cuál de las siguientes preguntas es correcta?
1. How many money is there on the table?
2. How much money are there on the table?
3. How many money are there on the table?
4. How much money is there on the table?";"4"

"Respuesta de [How much water is there in the pool?] donde [hay una picina con mucha agua]
1. There`s many water in the pool
2. There`s a lot of water in the pool
3. There are a lot of water in the pool
4. There are many water in the pool";"2"

Esas manzanas estan en el refrigerador;"
These apples are in the fridge"

Este es mi perro y esa es una pelota roja. Mi perro esta jugando con la pelota;"
This is my dog and that is a red ball. My dog is playing with the ball.
"

"¿Cuál de los siguientes verbos no cumple la regla de la tercera persona del singular?
1. Likes
2. Brushs
3. Has
4. Flies";"2"

"Complete según corresponda: She (despierta) _______ at 5:00 am.
1.cries
2.wakes up
3.get up
4.go to sleep
";"2"

"¿Cual de los siguientes no es un adverbio de frecuencia?
1. Frequently
2. Generally
3. Alwaysly
4. Sometimes
5. Never
";"3"

 ¿Cada cuánto lava ella el carro?;"
How often does she wash the car?"

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 21. Adjetivos VS Adverbios y 6 reglas para crearlos

Definiciones.

- Adjetivo: Son las caracteristicas de un sujeto (Le agrega informacion
  a los sujetos).
- Adverbios: Hablan de las acciones, como se hacen las acciones (Le
  agrega informacion a los verbos.).

Al conocer un adjetivo tambien podemos conocer su adverbio, Y lo hacemos
por medio de 6 reglas que veremos.

Es el equivalente en español a agregar la terminacion mente, ej. simple a
simplemente, lento a lentamente, etc.

1, Agregar `ly` al final
| Adjetivo | Adverbios  |
| :------- | :--------- |
| Bad      | Bad`ly`    |
| Slow     | Slow`ly`   |
| Normal   | Normal`ly` |

2, Agregar terminacion `ally` cuando termine en `ic`.
| Adjetivo   | Adverbios      |
| :--------- | :------------- |
| Specif`ic` | Specific`ally` |
| Dramat`ic` | Dramatic`ally` |

3, Cambiar terminacion `le` por `ly`
| Adjetivo | Adverbios |
| :------- | :-------- |
| Simp`le` | Simp`ly`  |
| Humb`le` | Humb`ly`  |

4, Cambiar terminacion `y` por `ily`
| Adjetivo | Adverbios |
| :------- | :-------- |
| Eas`y`   | Eas`ily`  |
| Happ`y`  | Happ`ily` |

5, Cambiar terminacion `ue` por `uly`
| Adjetivo | Adverbios |
| :------- | :-------- |
| D`ue`    | D`uly`    |
| Tr`ue`   | Tr`uly`   |

6, Excepciones o adverbios irregulares
| Adjetivo | Adverbios |
| :------- | :-------- |
| Good     | Well      |
| Fast     | Fast      |
| Hard     | Hard      |

Usa correctamente el advervio o adjetivo
| Adjetivo | Adverbios | Oracion                                                 |
| :------- | :-------- | :------------------------------------------------------ |
| Good     | Well      | They are sining `well`                                  |
| Quick    | Quickly   | John walks `quickly`                                    |
| Slow     | Slowly    | Turtles are `slow`                                      |
| bad      | badly     | I fell `bad(Animo)/badly(Salud)` (depende del contexto) |
| Soft     | Softly    | She speak english `Softly`                              |

```r
#- reglas, uso de adverbios y adjetivos
Cuales son las 6 reglas para pasar de Adjetivo VS Adverbio;"
1, Agregar `ly` al final
2, Agregar terminacion `ally` cuando termine en `ic`.
3, Cambiar terminacion `le` por `ly`
4, Cambiar terminacion `y` por `ily`
5, Cambiar terminacion `ue` por `uly`
6, Excepciones o adverbios irregulares
"

"Cuales son los Adverbios de los siguientes Adjetivos
-Dramatic -Good -Normal";"
Dramat`ic`: Dramatic`ally`
Good: Well
Normal: Normal`ly`
"

"Cuales son los Adverbios de los siguientes Adjetivos
-Specific -True -Slow";"
Specif`ic`: Specific`ally`
Tr`ue`: Tr`uly`
Slow: Slowly`
"

"Cuales son los Adverbios de los siguientes Adjetivos
-Happy -Bad -Humble ";"
Happ`y`: Happ`ily`
Bad: Badly
Humb`le`: Humb`ly`
"

"Cuales son los Adverbios de los siguientes Adjetivos
-Easy -Fast -Simple";"
Eas`y`: Eas`ily`
Fast: Fast
Simp`le`: Simp`ly`
"

"Cuales son los Adverbios de los siguientes Adjetivos
-Hard -Due -Quiet";"
Hard: Hard
D`ue`: D`uly
Quiet: Quietly
"

#/////

Cuales son los Adverbios de los siguientes Adjetivos BEAUTIFUL, HAPPY Quiet;"
*Beautifully*, happily, quietly
"

 Michael está en la biblioteca leyendo un libro silenciosamente;"
Michael is in the library reading a book quietly
"

 Ella toca la guitarra hermosamente;"
She plays the guitar Beautifully"

 Jorge y Ana están felizmente casados;"
Jorge and ana are happily married"

(acompleta) My friend Luisa doesn`t drive her car very__, she drives__;"
well - badly"

"(Selecciona) In my exam all the answers are __
1. Correctly
2. Correct
3. Incorrectly
4. Incorrec";"
2"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 22. How well + Intensificadores Pretty- Quite - Very - Really (Intensifiers)

Podremos tener la oracion "-¿como cantan ellas? -ellas cantan bonito",
pero podemos ser un poco mas detallistas y terner la siguiente oracion.

> "-¿que tan bien cantan ellas? -ellas cantan muy bien".

Arriba se puede decir la parte del `muy bien` es un intencificador, lo
cual permite expresar una caracteristica cualificadora, entre los cuales
tenemos las siguientes opciones.

Adverbs Intensifiers
|            |              |      |           |                |
| :--------- | :----------- | :--- | :-------- | :------------- |
| Adverbio   | Bastante     | Muy  | Realmente | Extremadamente |
| Adverb     | Pretty/Quite | Very | Really    | EXTREMELY      |
| Relevancia | +1           | +2   | +3        | +4             |

Estructura para pregunta con adverbio intencificador

> **HOW + ADVERB + DO/DOES + SUBJECT + VERB + COMPLEMENT**

Podriamos tener la siguiente oracion en ingles.

> "-How well do they sing? -They sing `Pretty(informal)/Quite` well".  

Ejemplos

```r
#- Ejemplos de Pregunta con adverbio intencificador
¿Que tan bien cantan ellas?;"
How well do they sing?"

¿Que tan mal cantan ellas?;"
How badly do they sing?"

Que tan rapido camina Pedro;"
How fast does Pedro walks?"

Que tan dificil es el examen;"
How hard does the exam is?"

Que tan felizes estaban ellos;"
How happily do they was?"

"-¿Que tan fluido habla Obama ingles?
.  -El habla bastante fluido
.  -El habla muy fluido
.  -El habla realmente fluido
";"
-How *fluently* does obama speak English? 
.  -He speaks quite well
.  -He speaks very well
.  -He speaks really well
"

-¿Que tan bien conduces? -yo condusco muy bien;"
-how well do you drive? -I drive very well
"

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 23. Ejercicio de escucha - Diferencia entre LOOK, SEE y WATCH

Diferencias ver

- Look = mirar = de reojo, accion corta, solo voltear y mirar
- See = ver = habilidad, accion media, ej. el no puede ver.
- watch = Observar = cuidadosamente, accion prolongada, notar detalles.

Diferencias

- Hear = oir = sin detalle
- Listen = escuchar = con detalle

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### VERBOS MODALES

Fuente: [pacho8a.com](https://www.pacho8a.com/ingl%C3%A9s/verbos-modales/)

#### 24. ¿Qué son los verbos modales?, Verbo Modal CAN

```r
#- verbos-modales
¿Que es un verbo infinitivo?;"
En español son los que terminan en ar-er-ir ej. correr, caminar, jugar
trabajar, etc. Lo cual se *TO* ej. to run, to walk, to work, etc."

¿Cuales son los verbos modales?, lista de verbos modales, lista de modales;"
Can = Poder (Habilidad, Permiso).
Should = Deberia (consejo, recomendacion)
Must = debe (obligacion, orden)
Could = Podria.
May
Might
Will,
Would,
Shall,
Ought to,
"

¿7 Caracteristicas de los verbos modales?;"
-Son 10 (creo hay mas)

-No tienen infinitivo:
  =Usar 'to' antes del modal, seria incorrecto usar `to can`.

-No se coloca el To en el siguiente verbo:
  =Incorrecto: 'he can to play'

-No tienen gerundio
  =(uso de -ing), seria incorrecto: canning

-No llevan s-es-ies para sujetos singular 3ra persona
  
-No se usan auxiliares
  =Seria incorrecto usar `do you can?`, correcto: `can you?`

-Se usan con otro verbo
"

Estructura de pregunta con verbo modal;"
wh-question + modal verb + subject + verb + complement
"

#- verbo-modal, can
¿Cuando se usa *can* y sus ejemplos?;"
Se usan en
  -Habilidades, Falta de habilidad, Permisos, Proibicion
_
Ejemplos:
-Habilidad:
. -You can/caen/ swim

-Falta de habilidad:
. -You can not speak japonese.

-Permiso
. -You can drive my car.

-Prohibicion
. -You can´t come in
"

(corrige y explica) do you can drive my car?;"
correccion: can you drive my car?.
explicacion: can es un verbo modal y estos no necesitan auxiliar
"

(-, + y ?) Ellos no pueden trabajar los fines de semana;"
(-) They can´t work on weekends
(+) They can work on weekends
(?) can they work on weekends?
"

(-, + y ?) Beto puede hablar inglés con sus amigos;"
(-) Beto can´t speak english with his friends
(+) Beto can speak english with his friends
(?) Can Beto speak english with his friends?
"

(-, + y ?)  Ella puede pintar bastante bien;"
(+) she can paint quite well
(-) she can´t paint quite well
(?) can she paint quite well?
"

(-, + y ?) Oscar no puede estudiar inglés muy a menudo;"
(+) Oscar can study english *very often*
(-) Oscar can´t study english *very often*
(?) can Oscar study english *very often*?
"

(-, + y ?) ¿Podemos ir al parque mañana?;"
(+) We can go to the park tomorrow
(-) we cant`n go to the park tomorrow
(?) can we go to the park tomorrow?
"

(-, + y ?) Yo puedo abrir el horno;"
(+) I can open the oven
(-) i can´t open the oven
(?) Can i open the oven?
"

(-, + y ?) ¿Pueden Ruth y Dario ver televisión en el sofá?;"
(-) Ruth and Dario can´t watch tv on the sofa.
(+) Ruth and Dario can watch tv on the sofa.
(?) can Ruth and Dario watch tv on the sofa?.
"
```

#### Aprende a diferenciar el verbo Need del Verbo Modal Need

```r
modal need: ¿Que es?, ¿Estructura?, ¿ejemplo?, ¿Diferencia con el verbo comun?;"
¿Que es?
.  -Usado en el ingles britanico, y entendible en el americano.
_
¿Estructura?
.  -Presente
.    -Suj + needn't + V.Base + Complemento
.  -Pasado
.    -Suj + needn't + Have + V.Base + Complemento
_
Ejemplo
.  -El no necesita reservar el hotel
.    -He needn't book the hotel
_
¿Diferencia con el verbo comun?
.  -We don´t need to book the hotel
.    -Es lo mismo solo una es la version britanica y la otra americana
_
Notas
.  -Despues de los verbos modales no va el `to`, ni agregar `s` cuando
.   usamos 3raPerSin
"
```

#### 37. Verbo Modal Could en Presente forma Afirmativa, negativa, interrogativa

```r
Could: ¿Traduccion?, ¿Uso?, ¿Pronuniacion?, ¿Estructura?, ¿Diferencia con can?;"
Traduccion
.  -Pudo(Pasado)
.  -Podria(en condicionales, y si el contexto lo requiere)
_
Uso de could:
.  -Para Habilidad, Cortesia, Posibilidad
_
Pronunciacion:
.  -could/Kud/
.  -couldn´t/couldna/
_
Estructura
.  -Suj + could + V.Base + Compl.
_
Can vs could vs be able to
.  -Pasado   = Pudo = Could.
.  -Presente = Puede = Can
.  -Futuro   = Podra = will be able to 
_
Podria vs pudo
.  -Ella pudo ayudarnos          = She could help us
.  -Ella podria ayudarnos si ... = She could help us if ...
"

(+,-,?,=) Ella podria ayudarnos;"
(+)She could help us
(-)She couldn´t help us
(?)Could she help us?
(=)Yes she could / No, she couldn´t
"

1.She could help us / 2.She could help us if ... ;"
1. Ella pudo ayudarnos     
2. Ella podria ayudarnos si
"

1.Yo no podria vivir sin ti / 2.Yo no era capas de vivir sin ti / 3.Yo no pude vivir sin ti;"
1.I couldn´t live without you
2.I wasn´t able to live without you
3.I did not able to live without you
"

Yo no podria vivir sin ti;"
I couldn´t live without you
"

¿Pudo venir con nosotros?;"
could they come with us?
"

¿Podrias por favor darme algo de tomar?;"
could you please give me something to drink?
"

Nosotros podriamos ir a españa juntos;"
We could go to spaint together
"

¿Podiras practiar mas despues de esta clase?;"
could you practice more after this class?
"

1. ¿Podrías ayudarme con mi tarea?;"
Could you help me with my homework?
"

2. Yo podría trabajar mañana pero no podría descansar el domingo;"
I could work tomorrow but i couldn´t rest on sunday
"

3. ¿Podrían ellos aprender español en ese país?;"
could they learn spanish in that country?
"

4. Carlos no podría comenzar a trabajar hoy;"
Carlos couldn´t start to work today
"

5. ¿Podrías perdonarme? Sí, yo podría;"
Could you forgive me? yes, I could.
"

6. Tu hijo podría llegar a ser un excelente cantante;"
Your son could *(poco-preciso=be)become* an excellent singer
"

7. ¿Podríamos hacer algo para comer?;"
Could we make something to eat?
"
```

#### Cómo usar Can - Could - Be able to en Presente - Pasado - Futuro

```r
I will can speak english (corrige y explica);"
I will be able to speak english.

Porque:`can` es un modal y no se puede usar junto a otro modal, por lo
que no hay de otra mas que usar la alternativa `will be able to`
"

(pre,pas,fut)Soy puedo hablar ingles (con `can` y `be able to`);"
(pas) I could speak english
(pas) I was able to speak english

(pre)I can speak english
(pre)I am able to speak english

(fut)I will be able to speak english
(fut)I going to be able to speak english
"

Ella puede tocar la guitarra pero no puede cantar;"
She can play the guitar but she can´t sing
"

Nosotros no [pudimos|fuimos capaces de] terminar la tarea;"
We [couldn´t|weren´t able to] finish the homework
"

Juan no podra ir al cine esta noche;"
Juan won't be able to go to the cinema=movies tonight
"

Yo no pude practicar ingles ayer, pero podre practicar mañana;"
I couldn´t practice English yesterday, but I will be able to practice tomorrow

Incorrecto:`but I will can`, porque 'will' y 'can' son modales y no se combinan
"

1. Andrés pudo comprar muchas cosas para navidad;"
Andres could buy many things for Christmas
"

4. Ella será capaz de enseñar francés, porque ella pudo vivir en Paris el año pasado;"
She will be able to teach french, because she could live in Paris last year
"

5. Luis no es capaz de manejar su carro, pero su amigo José podrá enseñarle;"
Luis isn´t able to drive his car, but his friend Jose will be able to teach him
"
7. La policía pudo arrestar al ladrón. Él no podrá robar otra vez;"
The police could arrest the thief. He won't be able to steal again
"

8. Yo no podré estar contigo en la fiesta mañana, pero voy a poder estar contigo hoy;"
I won't be able to be with you at the party tomorrow, but I'm going to be *able to be* with you today.
"

9. Mis amigos y yo no pudimos entrenar ayer, así que no seremos capaces de ganar mañana;"
My friends and I couldn´t train yesterday, So we won´t be able to win tomorrow
"

10. Camila y Mari no pueden trabajar juntas, ellas hablan todo el día y sus amigos no son capaces de trabajar bien;"
Camila and Mari can´t work together, they speak all day and their friends aren't ** able to work well
"
```

#### Verbo Modal Should y Shouldn't

```r

```

#### 25. Cómo utilizar Wh Questions con el verbo modal CAN

```r
#- verbo-modal, can, wh-questions
¿Quien puede trabajar el viernes?;"
Who can work on friday?"

¿Donde puedo encontrar un banco?;"
Where can i find a bank?"

¿Cuando ellos pueden venir a mi casa?;"
when can they come to my house"

¿Como podemos comer con palos chinos?;"
how can we eat with chopsticks?"

¿Cada cuanto puedes estudiar ingles?;"
How often can you study english"

¿Que tan bien puedes hablar ingles?;"
How well can you speak english?"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

#### 26. Modal Can + Conjunciones y Adverbios (too, also, and, or, also, only)

```r
#- conjunciones
Para que sirven y cuales son las Conjunciones?;"
Que son: Sirven para crear conjuntos y/o crear conexiones
- Too (tambien, se usa al final)
- Also (Tambien, ademas, se usa coando no esta al final)
- and (y, e)
- or (o, ni)
- but (pero, sino, sino que)
- Only (solo, solamente)
- Not only (NO solo, no solamente)
- But also (pero tambien, sino que tambien)
"

(T)Yo puedo cantar tambien / Yo tambien puedo cantar;"
I can sing too / i *can also* sing"

(T)Yo puedo bailar y tambien cantar / yo puedo bailar y cantar tambien;"
I can dance and also sing / I can dance and sing too."

(T)Yo puedo cantar pero tambien bailar;"
I can sing but also sing"

(T)Yo no puedo cantar ni bailar;"
I can´t sing or dance"

(T)Yo solo puedo cantar o bailar;"
I *can only* sing or dance"

(T)Yo no puedo cantar pero puedo bailar;"
I can´t sing but i can dance"

(T)Yo no solo puedo bailar sino que tambien cantar;"
I can´t only dance but also sing"

(T)Tu no solamente puedes ver el video si no que tambien practicar;"
You can´t only watch the video but also practice"

1. Yo puedo ver una película en mi casa y además comer pizza;"
I can watch a movie in my house and also eat pizza"

2. Juan no puede jugar ni escuchar música cuando está estudiando;"
Juan can´t play or *listen to music* when he is *studying*"

3. Mi mamá no solamente puede cocinar, sino que también puede arreglar su carro;"
My mom can´t only cook, but also fix her car"

4. ¿Puedes hablar dos idiomas? Si, yo puedo hablar español y también inglés;"
can you speak two languages? yes, i can speak spanish and also english"

5. Nosotros no solamente podemos leer, sino que también podemos escribir;"
We can´t only read, but also write"

6. El lunes, yo puedo ir al gimnasio, pero no puedo ir el martes;"
On monday, i can go to the gym, but i can`t go on tuesday"

7. Gerardo puede saltar la cuerda y algunas veces su esposa puede saltar la cuerda también;"
Gerardo can jump the rope and sometimes his wife can jump the rope too"

8. ¿Puede ella también beber agua? Si, ella no solamente puede beber agua, sino que también tomar leche;"
can *she also* drink water? Yes, she can`t only drink water but also drink milk."

9. Nosotros también podemos hablar y escribir en inglés bastante bien;"
We can also speak and write in english quite well"

10. Mi amiga Thali puede bailar, correr o nadar realmente bien, pero yo sólo puedo bailar;"
My friend Thali can dance, run or swim really well, but i can only dance."
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

#### 27. Verbo Modal Should en todas sus formas + Wh Questions

```r
#- verbo-modal, should, consejos, recomendaciones, wh-question
should/shouldn´t ¿pronunciacion?, ¿Significado?, ¿Estructura?, ¿Ejemplo?;"
Pronunciacion
.  -should/shud/
.  -shouldn´t/shurent/
_
¿Significado?
.  -Deberia (recomendacion, consejo)
_
¿Estructura?
.  -Suj + should + vBa + Comp
_
¿Ejemplo?
.  -I + should + wash + my card
.    -Yo deberia lavar el carro
"

(+,-,?,?-,=) Ella deberia de ayudar a sus amigos;"
(+) She should help her friends
(-) She should not help her friends / She shouldn´t help her friends
(?) Should she help her friends: R. Yes, she should / No, she shouldn´t
(?-) Shouldn´t she help her friends.
(=) Yes, she should / No, she shouldn´t
"

¿Donde deberiamos ir esta noche?;"
where should we go tonight?
"

¿Cuando deberia Jhon pagar la cuenta?;"
When should Jhon pay the bill?
"

Nosotros no deberiamos fumar;"
We shouldn´t smoke
"

¿Que deberiamos de hacer ahora?;"
What should we do now?
"

¿Helen deberia comer algo saludable?;"
Helen should eat something healthy
"

¿Que tan a menudo yo deberia de practicar ingles?;"
How often should I practice english?
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

#### 28. Can y Should + Pronombres Objeto. Cuándo y cómo usarlos

```r
Que son los Pronombres Objeto y como se relacionan con los Pronombres personales;"
-Personales: I , you, he , she, it, we, they
-Objeto:     me, you, him, her, it, us, them
"

Yo puedo ir con ella;"
I can go with her
"

Tu deberias de practicarlo;"
You should practice it
"

Yo puedo explicarte;"
I can explain to you
"

Tu no deberia de comprarlo/comprarlos;"
You shuldn´t buy it/them
"

Juan no puede estar con nosotros;"
Juan can´t *be* with us.
"

Ana deberia de venir con ellos;"
Ana should came with them
"

Pudes ayudarme;"
Can you help me?
"

1. Patricia no debería estar jugando con el perro. Ella debería estar bañándolo;"
Patricia shouldn´t be playing with the dog. She should be Bathing it
"

2. Estos ejercicios están realmente fáciles. Nosotros podemos hacerlos;"
*These* exercises are really easy. we can do them
"

3. Esa mujer necesita ayuda. ¿Puedes ayudarla?;"
That woman *needs* help. can you help her?
"

4. Nosotros siempre vamos contigo al gimnasio, pero hoy no podemos llevarte;"
We always *go with you* to the gym, but today we can´t take you
"

5. Yo no tengo zapatos nuevos y estos zapatos están muy bonitos ¿Debería comprarlos?;"
I don´t have new shoes and these shoes are very pretty, Should I buy them?
"

6. Luis debería estar lavando su carro hoy, pero él lo está pintando. (está pintándolo);"
Luis should be washing his car today, but he is painting it
"

7. Carmen debería ir con nosotros, pero ella no puede;"
Carmen should go with us, but she can´t
"

8. Tu no deberías estar hablando con él, tu deberías estar escuchándome;"
You shouldn´t *be* talking with him, you should be *listening* to me
"

9. Esa es una caja fuerte y nosotros no podemos abrirla. ¿Puedes ayudarnos?;"
That is a safe and we can´t open it. Can you help us?
"

10. ¿Puedo ir con ustedes? Yo no quiero ir con ellos;"
Can I go with you? I don´t want *to* go with them
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

#### 29. Verbo Let + Pronombres Objeto y modales Can y Should

```r
#- let
let/let´s;"
Dejar, Dejar que, Permitir, Permitir que / vamos, vamos a
"

Estructura frases Presente simple afirmativo con let y ejemplo;"
Estructura: let + pronombre-objeto + verbo base + complemento
Ejemplo: He lets us work CUIDADO: no es un verbo modal, por lo que si
le aplican las reglas del presente simpre tercera persona
"

Dejame conducir hoy;"
Let me drive today
"

Dejame ayudarte;"
let me help you
"

Dejanos hacerlo;"
let us do it
"

Dejalo que ella *se* vaya;"
Let her go (si se tiene problemas con traducir el *se* podemos mover
la oracion a dejala ir la cual seria mas facil para nosotros traducirla)
"

Ellos deberian dejarnos ir;"
They let us go
"

Yo no puedo permitir que ella se vaya;"
I can´t let her go
"

Puedes dejarme hacerlo otra vez;"
can you let me do it again
"

El profesor nos deja trabajar juntos;"
The teacher *lets* us work together
"

Deberia yo dejarte practicar;"
*Should I* let you practice
"

Vamos a practicar;"
Let´s practice != Let´s to practice
"

1. Mi amigo John está pintando su carro y él me deja ayudarlo;"
My friend John is painting his car and he *lets* me help him
"

2. Necesito tu martillo. ¿Puedes dejarme usarlo?;"
I need your hammer. can you let me use it?
"

3. Tu hijo está jugando con su carro y está destruyéndolo. Tú no deberías permitir que lo haga;"
Your son is playing with his car and he is destroying it. You shouldn´t let him do it
"

4. Déjame llamar a Mark. Él está en el parque con sus hijos. Él está jugando con ellos;"
Let me call*Mark. he is in the park with his *children*. he is playing with them
"

5. Gina quiere ir al cine con sus amigas. ¿Puedes dejarla ir con ellas?;"
Gina wants to go to the cinema with her friends. can you let her go with them?
"

6. ¡Vamos a comer! Yo quiero comer hamburguesa y la quiero con queso ¿y tú?;"
Let´s eat I want to eat burger and i want it with cheese, and you?
"

7. Yo no puedo dejar que vengas conmigo;"
I can´t let you come with me
"

8. No podemos dejar que el perro se coma el sofá. Lo está destruyendo;"
We can´t let the dog eat the sofa. It´s destroying it
"

9. ¿Deberían ellos dejar que vayas conmigo?;"
should they let you go with me?
"

10. Vamos a hacer estos ejercicios una y otra vez;"
Let´s do these exercises once and again
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

#### 30. Modal WOULD en todas sus formas - Diferencia Can y Should

```r
#- verbo-modal, WOULD, can~could, let~Should
would: significado, Utilidad, equivalente en español, y pronunciacion de WOULD;"
Significa:
. -No significa nada funciona como la terminacion ia, ias, ian.
. -ej: iria, trabajarias, comerian, etc.
_
Pronunciacion:
. would=/wuod/, wouldn´t=/wurent/
_
Para:
. Peticiones y ofrecimientos formales.
_
Ejemplos:
. Tu caminarias
.  = you would walk
.  = you'd walk
. Ellos saldrian
-  = they would go out
.  = they'd go out
. Yo haria
.  = I would do
.  = I'd do
. etc.
_
Notas:
.  He would: No significa nada, ya que le agrega la terminacion ia a
.  un verbo, y en este caso no tenemos un verbo.
"

(+,-?)Tu Caminarias conmigo;"
(+)You would walk with me = You´d walk with me.
(-)You would not walk with me = You wouldn´t not walk with me.
(?)Would you walk with me?, R.- Yes, I would, No, I wouldn´t
"

(Explica que esta mal)He would goes with me | He woulds go with me;"
Se tratan de aplicar reglas del terera persona singular, al usar un verbo
modal en este caso would, y como sabemos estas reglas no aplican para
los verbos modales
"

Nosotros no comeriamos pizza juntos;"
We wouldn´t eat pizza togther
"

¿Comenzarian ellos a trabajar mañana?;"
Would they start *to* work tomorrow?
"

Usted deberia dejarme ir;"
You should let me go
"

El perro correria con ustedes en el parque;"
The dog would run with *them* in the park
"

1.él quería, 2.el querría;"
1.He wanted

2.We would want
"

#-could
Yo podria cocinar esta noche;"
I could/kud/ cook tonight"

1. Ella escogería el vestido para su boda;"
She´d choose the dress for her wedding
"

2. Juan no estudiaría con ellos en la universidad;"
Juan wouldn´t study with them at the university
"

3. Nosotros podríamos ayudarte, pero tú deberías practicar la lección;"
We could help you, but you should practice the lesson
"

4. ¿Compraría Patrick esa casa nueva para su familia?;"
would patrick buy that new house for his family?
"

5. Mis padres vendrían a visitarme esta noche y yo iría a visitarlos este fin de semana;"
My parents would come to visit me tonight and I´d go to visit them this weekend
"

6. ¿Vendrías conmigo a la fiesta esta noche?;"
Would you come with me to the party tonight
"

7. John y sus amigos no conseguirían los boletos en el aeropuerto;"
John and his friends wouldn´t get the *tickets* in the airport
"

8. Marta se quedaría en su casa con su esposo y dormirían todo el día;"
Marta´d stay in her house with her husband and *they* would sleep all day
"

9. Nosotros hablaríamos inglés fluidamente, pero tendríamos que practicar todos los días;"
we´d speak english fluently, but we *(NO)should|(Si)would* have to practice every day
"

10. ¿Podrían ellos enviar me el correo electrónico después de clase?;"
Could they send me the e-mail after class?
"

1. El señor Smith ayudaria a su esposa a cocinar y ella le ayudaria a el tambien;"
Mr. Smith would help his wife to cook and she would help him too
"

2.-Tu manejarias mi carro hoy? -Yo no puedo hacerlo ;"
Would you drive my car today? I can´t do it
"

3. ¿Me harias un favor?*me*gustaria comprar algunas cosas en el supermercado;"
Would you do me a favor? I´d like to buy some things at the supermarket
"

4. Ellos no pagarian con efectivo, ellos pagarian con targeta de credito;"
They wouldn´t pay in cash, they would pay with credit card
"

5. Me gustaria viajar *alrededor* del mundo, y me gustaria llevar a toda mi familia conmigo;"
I´d like to travel around the world, and I´d like to take all my family with me
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

#### 32. Desafío de escritura con Would like and Would love

```r
#- Would like, Would love
Estructura de would like/love (gustaria/encantaria);"
Pron-Perso + would + like/love + Pronombre-Objeto/Sujeto + verbo-infinitivo + Complemento
Ej. I´d like Peter would to come with me
"

Me gustaria que Peter viniera conmigo;"
I´d like *(no)that* Peter would *(si)to* come with me
"

A mis padres les gustaria que yo trabajara;"
My parents would like *(No)I|(Si)me* *(no)would* to work = 
My parents would like me to work
"

Nos encantaria que tu nos ayudaras;"
We´d love you to help us
"

No me gustaria que te vayas hoy;"
I wouldn´t like you *to* go today
"

Me gustaria que tu no estuvieras triste;"
I´d like you not to be sad
"

Te gustaria que yo te ayudara;"
would you like *me to* help you?
"

A el le encantaria que Mike fuera con el;"
He´d love Mike to go with him
"

¿Te gustaria que fueramos contigo?;"
Would *you like* us to go with you?
"

Me gustaria que tu no lo hicieras;"
I would like you not to do *it=that*
"

¿Te gustaria que practicaramos mas?;"
Would you like us to practice more?
"

1. Me gustaría que mi hija Ana visitara a su madre en el hospital;"
I´d like My *daughter* Ana to visit her mother at the hospital
"

2. A ellos no les gustaría que nosotros llegáramos el viernes;"
They wouldn´t *like* us to come on Friday
"

3. Me encantaría que tu escogieras el carro nuevo;"
I´d love you *to* choose the new car
"

4. ¿Les encantaría a ellos que nosotros pagáramos la cuenta?;"
Would they love *(no)to them* us to pay the bill?
"

5. ¿Te gustaría que me quedara contigo hoy?;"
would you like me to stay with you today?
"

6. No me gustaría que vinieras tarde esta noche;"
I wouldn´t *like* you to come late tonight
"

7. A mi esposa le gustaría que yo no viera televisión todo el día;"
My wife would like me not to watch TV all day
"

8. A nosotros no nos gustaría que ellos viajaran solos a Paris;"
We wouldn´t like them *to* travel alone to paris
"

9. A mi padre le encantaría que mi madre no fumara;"
My father would love my mother not to smoke
"

10. ¿Le gustaría a ella que nosotros hagamos su tarea?;"
Would she *like* us to do her homework?
"

1. My wife wouldn´t like me to buy that old house, she´d like me to buy a new house;"
A mi esposa no le gustaria que yo comprara esa casa vieja, a ella le gustaria que comprara una casa nueva
"

2. Would you like us to come to help you with the homework?;"
¿Te gustaria que nosotros vinieramos a ayudarte con la tarea?
"

3. Would you love your kids not to jump on the bed?;"
¿Te encantaría que tus niños no saltaran sobre la cama?
"

4. Daniel would like us not to take his belongings;"
A Daniel le gustaria que no tomaramos sus pertenencias
"

5. Pacho would like me to practice this topic with many other exercises;"
A Pacho le gustaria que yo practicara este tema con muchos otros ejercicios
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### WOULD YOU MIND IF PARTE 1

```r
Would you mind ...: ¿Uso? ¿Traduccion?, ¿Pronuniacion?, ¿Estructura?, ¿Ejemplos?, ¿Respuestas? ;"
¿Uso?
  -Para preguntas corteses o amables.
  -preguntar por permiso

¿Traduccion?
  -Te importaria [si/que/...]

¿Pronuniacion?
  -/gud/

¿Estructura?
  -Would + Suj1 + mind if + Suj2 + [Pasado|Presente] simple + Compl.?

¿Ejemplos?
  -¿Te importaria si tomara/tomo tu libro por un rato?
    -Would you mind if I took/take your book for a while

¿Respuestas?
  -Not at all: No en lo absoluto
  -I`d be glad to = estaria feliz de
  -That would be fine = Estaria bien
  -Of course [not]
  -Sure, Ok, No, It`s Ok
"

¿Te importaria si ella tomara/toma tu libro por un rato?;"
Would you mind if she took/takeS your book for a while
"

¿Le importaria a el si contempláramos/contemplamos su boceto?;"
Would he mind if we beheld/behold his sketch?
"

¿Te importaria si fuera/voy contigo? -Eso estaria bien;"
Would you mind if I went/go with you? -That would be fine
"

¿Te importaria si no hiciera/hago esto? -Si por supuesto;"
Would you mind if I [didn´t|don´t] do this? -yes of course
"

¿Le importaria a Pacho si practicaramos/practicamos un poco mas? -No en lo absoluto;"
-Would Pacho mind if we practiced/practice a little more?
-Not at all
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Would you mind + ING– Polite questions – Estructura y Ejemplos – PARTE 2

```r
Would you mind + Verb+ing ...: ¿Uso? ¿Traduccion?, ¿Pronuniacion?, ¿Estructura?, ¿Ejemplos?;"
Uso
  Peticion, pedirle que alguien haga algo de forma formar

Traduccion
  Te importaria [hacer una accion]

¿Pronuniacion?
  -Would/gud/

¿Estructura?
  -Would + Subject + mind + Verb_ing + Compl + ?

¿Ejemplos?
  ¿Te importaria traerme ese cojin?
    -Would you mind bringing me that cushion?

  ¿Le importaria a tu esposa darme un vaso de agua?
    -Would your wife mind giving me a glass of water?

  ¿Te importaria recojer a mis hijos a las 3 en punto?
    -Would you mind picking up my children at 3 o`clock
"

¿Le importaria a tu novio irse mas temprano esta noche?;"
Would your boyfriend mind leaving earlier tonight?
"

¿No te importaria quedarte con nosotros esta noche?;"
Wouldn't you mind staying with us tonight?

Would not: Es poco comun pero no hay pex
"

¿Le importaria a ellos no fumar en este lugar?;"
Would they mind not smoking in this place?
"

¿Te importaria traerme ese cojin?;"
Would you mind bringing me that cushion?
"
    
¿Le importaria a tu esposa darme un vaso de agua?;"
Would your wife mind giving me a glass of water?
"

¿Te importaria recojer a mis hijos a las 3 en punto?"
Would you mind picking up my children at 3 o`clock
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

#### Must - usar el verbo modal MUST

```r
Must: ¿Traduccion? ¿Estructura?, ¿Traducion?, ¿Ejemplo?, ¿Diferencia con should?;"
¿Traduccion?
.  -Deber (obligacion de hacer algo)
_
¿Estructura?
.  -Suj + must + verb + compl
_
¿Traducion?
.  -You must study englisg everyday
.    -Tu debes estudiar ingles todos los dias
_
¿Ejemplo?
.  -
_
¿Diferencia con should?
.  -Should = deberias = consejo, recomendacion
.  -Must = debes = obligacion, orden
"

(+,-,?,=)El debe de fumar en la casa;"
(+)He must smoke in the house
(-)He [must not|mustn´t/masent/] smoke/smouk/ in the house
(?)Must he smoke in the house?
(=)Yes, he must / No, he must not
"

1. Yo debo disfutar mi vida;"
I must enjoy my life
"

2. Nosotros no debemos pelear;"
We must not fight
"

3 ¿Debe Andres bailar Salsa?;"
Must Andres dance salsa?
"

¿Debo comprar frutas?;"
Must I buy fruits?
"

El no debe hablar fuerte en clase;"
He mustn´t talk laud in class
"

Si queremos estar en forma, debemos seguir la dieta que sugirió el entrenador.;"
If we want to be in form, We must follow the diet that the coach suggested
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

#### 33. WH Questions con el verbo modal WOULD en presente - Estructura

```r
#- wh-questions, would
Estrutura de un pregunta would con wh-question;"
wh-questions + would + Pronombre + verbo-base + Complemento
"

(?,+,-) ¿Por que te gustaria viajar a paris?;"
(?)Why would you like *to* travel to paris?,
(+)You would like *to* travel to paris,
(-)You would not like *to* travel to paris,
"

¿Donde irian ellos este invierno?;"
Where would they go this winter?
"

¿Cuando empezariamos las clases?;"
When would we begin *the* clases?
"

¿Que tan bien tocarias la guitarra?;"
how well would you play the guitar?
"

¿Que tan a menudo ellos estudiarian ingles?;"
How often would they study english?
"

¿Que harias esta noche?;"
What would you do tonigh?
"

¿Quien te gustaria ser?;"
Who would you like *to* be?
"

¿Donde te gustaria trabajar?;"
Where would you like to work?
"
```

#### Todo acerca del HAVE TO y HAS TO en PRESENTE, PASADO y FUTURO

```r
Has to/Have to: ¿Que es? ¿Traduccion?, ¿Estructura?, ¿Ejemplos?;"
¿Que es?
.  -Es un verbo semi-modal, lo cual podemos trabajarlo como modal y como verbo,
.  -El `have/has` es un modal, y `have/has to` es un verbo
_
¿Traduccion?
.  -Tener que ... (obligacion -> consecuencias)
_
¿Estructura? (solo nocion)
.  -Pre: Have/has to <-> do/does.
.  -pas: Had to <-> did
.  -Fut: Have to <-> will
_
¿Ejemplos? 
.  -Pre: 
.    -(+)[I/He] [have/has] to work now
.    -(-)[I/he] [don´t/doesn´t] have to to work now
.    -(?)[Do/Does] [I/he have] to work now?
.  -pas: 
.    -(+)[I/He] had to work yesterday
.    -(-)[I/He] didn't have to work yesterday
.    -(?)Did [I/he] have to work yesterday?
.  -Fut: 
.    -(+)[I/He] will have to practice tomorrow
.    -(-)[I/He] won´t have to practice tomorrow
.    -(?)will [I/he] have to practice tomorrow?
"

#presente
(+,-,?,=)Yo tiengo que trabajar hoy;"
(+)I have to work today
(-)I don´t have to to work today
(?)Do I have to work today
(=)Yes, I do / No, I don't
"

(+,-,?,=)El tiene que trabajar hoy;"
(+)He has to work today
(-)He doesn´t have to to work today
(?)Does he have to work today
(=)Yes, he does / No, I doesn't
"

He have to work today (corrige y explica);"
He *has* to work today

Porque: Cuando tenemos 3raPerSing se usa has
"

Do he has to work today (corrige y explica);"
*Does* he *have* to work today

Porque: Cuando tenemos 3raPerSing se usa Does y cuando usamos el does
ya es necesario pasar el have a su forma 3raPerSing
"

#Pasado
(+,-,?,=)Yo tuve que estudiar ayer;"
(+)I had to work yesterday
(-)I didn't have to work yesterday
(?)Did I have to work yesterday?
(=)Yes I did, No I didn´t
"

Yo no tuve que trabar ayer = I hadn´t to work yesterday (corrige y explica);"
I didn't have to work yesterday

Porque: el `have to` se maneja como verbo base, no como modal.
"

(+,-,?,=)El tuvo que estudiar ayer;"
(+)He had to work yesterday
(-)He didn't have to work yesterday
(?)Did he have to work yesterday?
(=)Yes he did, No he didn´t
"

#Futuro
(+,-,?,=)Yo tendre que practicar este tema mañana;"
(+)I will have to practice this topic tomorrow
(-)I won´t have to practice this topic tommorrow
(?)will I have to practice this topic tommorrow
(=)Yes I will, No I won´t
"

# ejercicios
(+,-,?)Nosotros tenemos que leer ese libro para mañana;"
(+)We have to read that book for tomorrow
(-)We don´t have to read ...
(?)Do we have to read ...?
"

(+,-,?)Mi esposa tuvo que cocinar pollo para la cena;"
(+)My wife had to cook chicken for dinner
(-)My wife didn´t have to cook ...
(?)*Did* My wife have to cook ...?
"

(+,-,?)Tu tendrás que perder peso pronto;"
(+)You will have to lose weight soon
(-)You won´t have to lose ...
(?)Will you have to lose ...?
"

(+Pre,-Pas,?Fut)John tiene que hacer muchas cosas;"
(+Pre)John *has* to do many things
(-Pas)John didn´t have to do many things
(?Fut)Will John have to do many things?
"

(+Fut,-Pre,?Pas)Ustedes tienen que usar uniforme todos los días;"
(+Fut)You will have to wear uniform every day
(-Pre)You don´t have to wear uniform every day
(?Pas)Did you have to wear uniform every day?
"

(Pre,Pas,Fut)Todo el mundo tiene que mostrar el pasaporte en el aeropuerto;"
(Pre) Everybody has to show the Passport in the airport.
(Pas) Everybody had to show the Passport n the airport.
(Fut) Everybody will have to show the Passport at the airport.
"

(Pas,Fut,Pre)7. Yo tengo que trabajar 8 horas y ella tiene que trabajar 6 horas;"
(Pas) I didn’t have to work 8 hours and she didn’t’ have to work 6 hours.
(Fut) I won’t have to work 8 hours and she won’t have to work 6 hours.
(Pre) I have to work 8 hours and she has to work 6 hours.
"
```

### MODALES PERFECTOS

```r
Diferencia etre modal y modal perfecto;"
El modal perfecto usa el have, para decir en español haber, ha, he, hubo,
habra
_
ej:
.  -`Podriamos(could/can have) haberlo tenido todo`
.  -`el habria(would have) hecho lo mismo`
ss
"
```

#### Uso de could have y can have

```r
Podriamos haberlo tenido todo;"
We (could have)/coura/ had it all
_
https://pacho8a.blogspot.com/2019/05/adele-rolling-in-deep.html
"

Pronunciacion de ´could have´;"
coura
"

Ale no puede haber comprado nada las tiendas estan cerradas;"
Ale couldn´t not have bought nothing the stores are close
"

Pudiste haber llamado antes de salir ¿Puesdes esperarme?;"
You could have call me before *leaving*, can you wait for me?

*X(leave)O(leaving)*: Despues de una prepocicion debemos de usar el
verbo con ing
"

¿Podrias ayudarme?, ¿Podrias haberlo ayudado?;"
could you help me?, could you have help him?
"
```

#### Como usar el modal Would y el modal perfecto Would Have

```r
Would have: ¿Traduccion?, ¿Uso?, ¿Pronuniacion?, ;"
¿Traduccion?
.  -Habria, hubiese
_
Pronunciacion
.  -wouldn´t = Wouldna
.  -woul have = Woulda
_
¿Uso?
.  -Algo habria/hubiese sucedido en el pasado
_
¿Diferencia con would?
.  -Terminacion `ría` en verbos
_
¿Estructura?
.  -Suj + would + have + V.P.P + Compl.
_
Ejemplo?
.  -El habria hecho lo mismo
.    -He´d have done the same
.    -He wouldn´t have done the same
.    -Would he have done the same?
"

(+,-,?,=)El habria hecho lo mismo;"
(+)He´d have done the same
(-)He wouldn´t have done the same
(?)Would he have done the same?
(=)Yes he would, No he wouldn´t
"

Mari estaria durmiendo en su casa por este clima frio;"
Mari would be sleeping in her house because of this cold weather
"

Si ellos my hubiesen preguntado, les habria dicho la verdad;"
If they had asked me, i would have told them the truth
"

Me habria desmayado si ...;"
I would have faint if
"

¿Vestirías a tus hijos así?;"
would you dress your children like that?
"

¿No habría Miriam perdonado a su esposo después de tantos años?;"
Wouldn´t *Miriam have* forgiven her husband after so many years?
"

No habríamos pasado el examen si no hubiera sido por tu ayuda. (Conditional 3);"
We wouldn´t have passed the exam if *it* hadn’t been for your help
"

¿Te gustaría jugar cartas conmigo? Por supuesto que me gustaría. / Claro que sí.;"
Would you like to play cards with me? Of course, I would
"

Si no hubiese olvidado tu número, te habría llamado;"
If i *hadn’t* forgotten your number, I would have phoned you
"

Si ganara la lotería, ayudaría a los habitantes de calle.;"
If i *won* the lottery, I´d help the homeless
"

Mi secretaría presentaría su renuncia esta noche.;"
My secretary would present her resignation tonight
"

Ellos no venderían su casa este año, tal vez lo harían el próximo año.;"
They wouldn´t sell their house this year, Maybe they´d *do it* next year
"

Me gustaría agradecerte por todo lo que haces por mí.;"
I´d like to thank you for everything you do for me
"
```

#### Modales May have - Might have

```r
May: Might: Traduccion, Uso, Estructura, Ejemplo(+,-,?,=);"
Traduccion
.  -May: Puede (Presente)
.  -Might: Pudo, Podria (Pasado)
_
Notas
.  -Podrian incluir el 'que'
.  -pudo o podria segun el contexto
__
Uso
.  -Posibilidad, Autorizacion, Permiso(Cortes)
.  -Can-Permiso es mas informal
__
Estructura
.  -Suj+ May/Might + V.Base + Compl
__
Ejemplo(+,-,?,=)
.  -Mario puede/Podria estar mintiendo
.    -(+)Mario may/might be lying
.    -(-)Mario may/might not be lying
.    -(?)may Mario/might be lying
.    -(=)Yes he might, No he may not
.  -Mario pudo estar mintiendo ayer
.    -Mario might be lying yesterday
__
"

May have: Might have: Traduccion, Uso, Estructura, Ejemplo(+,-,?,=);"
Traduccion
.  -May have: Pudo haber
.  -Might have: pudo/podria haber
_
Estructura
.  -Suj + May/Might + have + V.P.P. + Comp.
__
Ejemplo(+,-,?,=)
.  -Mario puede/podria haber salpicado
.    -(+)Mario may/might have splashed
.    -(-)Mario may/might not have splashed
.    -(?)may/migh Mario have splashed
.    -(=)Yes he may, no he might not
.  -Mario pudo haber salpicado ayer
.    -(+)Mario might have splashed yesterday
__
"

Podria haber ganado la loteria, pero no compre el boleto;"
I might have won the lottery buy, I didn´t buy the ticket
"

Puede que llueva mas tarde, tomare una sombrilla;"
It may rain later, I will take an umbrella.
"

Lo intentaste, pero puede que no haigas sido constante;"
You tried *it*, but you may not have been constant
"

#
Si no estudiaste bien, puede que tengas que memorizar todo;"
If you didn´t study well, you may have to memorize everything
"

Hellen pudo haber superado sus problemas pero decidió no hacerlo;"
Hellen might have overcome her problems but she decided not *to* do it
"

El cazador puede disparar en cualquier momento;"
The hunter may shoot at any time
"

Los estudiantes pueden no haber subrayado muchas palabras desconocidas, solo unas cuentas;"
The Students may not have *underlined* many unknown words, just a few
"

¿Pudieron haber tocado otra canción en la fiesta?;"
Might they have played another song at the party?
"

No estoy segura, pero puede que yo no lo perdone;"
I´m not sure, but I may not forgive him
"

Rick no pudo haber prometido eso, es una mentira;"
Rick might not have promised that, it's a lie
"

Este computador no está funcionando bien, puede no haber sido reparado correctamente;"
This computer isn't *working* well, it may not have been fixed correctly
"

Despertaste el bebé. Puede que el esté llorando;"
You woke up the baby. He may be crying
"

Ella no pudo haber cancelado la orden. Debió haber sido mi hijo;"
She might not have cancelled the order. It *must have been* my son

*It should h...*: Deberia haber
"

```

<!-- Aprende el uso del modal Must y el modal perfecto Must Have - Diferencias -->
#### uso de Must Have

```r
Must have: Traduccion, uso, Estructura;"
Traduccion
.  -[debe/debio/debia] haber
_______________

uso
.  -ASUMIR ALGO, Deber, obligacion
____________

Diferencias
. -he Must have avoided
.  -Debe haber evitado (obligacion)

. -he Must avoid
.  -Debe evitar(No hay cone. con el pasado)

. -he should have avoided
.  -Debería haber evitado (recomendacion)
____________

Estructura
.  -Suj + musthave + v.p.p + Compl.
____________

Ejemplo(+,-,?,=)
.  -Beto [debe/debio/debia] haber traido la comida
.    +Beto must have brought the food
.    -Beto must not have brought the food
.    ?Must Beto have brought the food?
_______________

Nota
.  -En español es mas extenso, pero en ingles es mas simple, es por eso que equivale a varias formas.
.  -Lo diferenciamos segun el contexto.
"

No encuentro mis llaves, debi [dejarlas/haberlas dejado] en la oficina;"
I don´t find my keys, I must have left *them* in the office
"

Ustedes debieron [mostrar/haber mostrado] su pasaporte ayer;"
You must have shown your passport yesterday
"

¿Debio Alex haber estudiado antes de su examen?;"
Must Alext have studied before his exam?
"

Andrew debió firmar los documentos antes de viajar;"
Andrew must have signed the documents before travelling
"

Despues de la cirugía, tu madre no debió haber disfrutado su cumpleaños;"
After the *surgery*, your mother mustn't have enjoyed her birthday
"

¿Debieron aprender a hablar inglés para venir aquí?;"
Must they have learned to speak english to come here?
"

Laura no debió decirle eso, él debe estar muy triste.;"
Laura mustn't have *told him* that, He *must be* very sad

*must have been* = debe haber estado
"

No puedo ponerme esta blusa, debe haberse encogido.;"
I can´t put on this blouse, it must have shrunk
"

Debiste haber seguido la dieta, si querias conseguir buenos resultados;"
You must have followed the diet, if you wanted *to* get good results
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 34. Adjetivos posesivos + Dictado en inglés nivel básico

```r
Mi perro Spike esta en su casa;"
My dog Spike is in *its* house
"

A nosotros nos gustaria cambiar nuestro carro;"
We would like to change *our* car
"

A ellos les gustaria comer en su casa hoy;"
They would like to eat in their house today
"

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 35. Diferencia de preguntas con WHAT y WHICH

```r
#- WHAT-vs-WHICH
Diferencia entre WHAT y WHICH?;"
El what es para algo desconocido y which es cuando tenemos opcines,
Ej. What/Which dress would she like?, con *What* es que no hay una
lista de opciones mientras que con *Which* si las hay
"

1. In ___ country would you like to live, Peru, Argentina, or Brazil?;"
Which
"

2. __ would they like to drink?;"
What
"

3. I can offer you coffee or tea. ___ would you like to drink?;"
Which
"

4. Mike likes sport cars. __ cars do you like?;"
What
"

5. __ breed are your dogs?;"
What
"

6. __ kind of music does your mother like, Pop, Rock, or Classical music?;"
Which
"

7. I want to eat a different kind of food. __ food do you recommend me?;"
What
"

8. We should practice sports. __ sports should we practice?;"
What
"

9. Those girls are running really fast. __ is your girlfriend?;"
Which
"

10. Those children are playing at the playground. __ are your children?;"
Which
"

1. ¿Cuál idioma habla tu amigo Rony, inglés o español?;"
Which language does *(orden)your friend rony speak*, english or spanish?
"

2. ¿Cuál programa de televisión le gusta a ella?;"
What tv program does she like?
"

3. ¿A qué hora te despiertas en la mañana?;"
What *time* do you wake up in the morning?
"

4. ¿Cuál es tu deporte favorito, natación o fútbol?;"
Which is your favorite sport, *(escritura)swimming* or soccer?
"

5. ¿Qué película te gustaría ver esta noche?;"
what movie *(NO do)would* you like to watch tonight?
"

6. ¿Qué te gustaría hacer mañana?;"
What would you like to do tomorrow?
"

7. ¿Qué jugo prefieres, jugo de naranja o jugo de piña?;"
Which juice do you prefer, orange juice or pineapple juice?
"

8. ¿Qué bus debería tomar hoy, el bus rojo o el bus verde?;"
Which bus *(NO do)should* i *(NO have)take* today, the red bus or the green bus?
"

9. ¿Cuál es tu apellido, Ochoa o Grisales?;"
Which is your last name, Ochoa or Grisales?
"

10. ¿Cuál de tus hijas practica patinaje?;"
Which of your daughters *(evitar poner do)* practices skating?
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 36. Preguntas y respuestas con WHICH - Uso de One y Ones

```r
Para que sirve el one/ones y como se diferencian;"
sustituye el obteto del cual estamos hablando, 
ej. A:Look those shoes. B:Which ones
En este caso ones sirve para haer referencia a los zapatos
"

¿Cuales son los libros de ella (entre dos pilas de libros)? =Los de ella son los nuevos;"
A: Which are her books?
B: Her books are the new ones
"

(Que esta mal)A:Which is your house, B:The blue;"
B:The blue *one*, no es como en español que podemos decir *la azul*,
en ingles debemos de indicar de que hablamos, lo podrimos decir
B:The blue house o B:The blue one "

"A:¿Cuál reloj te gustaría comprar, el oscuro o el claro?
B:Me gusta ese.
A:¿Cuál?
B:El claro.";"
A:Which watch would you like to buy, the dark one or the light one.
B:I like that one.
A:Which one?
B:The light one"

"A:Mira esos perros jugando.
B:El de la izquierda es el mío
A:Me gusta, pero el de la derecha es mi favorito";"
A:Look *at* those dogs playing.
B:The left one is mine.
A:I like it, but the right one is my favorite.
"

"A.Esos son los carros de John.
B. ¿Cuáles?.
A. El rojo y el gris.
B. Me gustaría tener el rojo.
A. El rojo es su favorito";"
A:Those are *(sobra)the* John´s cars.
B:Which ones?
A:The red one and gray one.
B:I would like to have the red one.
A:the red one is his favorite"

"A.A mi mamá le gustaría tener dos de esos bolsos.
B. ¿Cuáles les gustaría?
A. Le gustaría tener el azul y el verde.
B. No me gusta el verde. Prefiero el rojo.";"
A:My mom would like to have two of those purses
B:Which ones *(falto)would she like*?
A:She would like to have the blue one and green one.
B:I don´t like *the* green one, i prefer the red one."

"A.¿Es este el celular de ella?
B. No, ese no es el suyo.
A. ¿Cuál es el de ella?
B. El de ella está en la mesa. Ese es el mío. Es el nuevo.
A. ok. Gracias";"
A:*(orden)Is this* her cellphone?
B:No. that one is not *(nose)hers*.
A:Which one is of *(nose)hers*?.
B:*Hers* is on the table, that one is mine, the new one.
A:Ok. Thanks"

"A.Tengo 7 bolígrafos en mis manos. ¿Cuál te gustaría?
B. Me gustaría ese.
A. ¿Cuál?
B. El verde. ¿Puedes darme los rojos también?
A. Por supuesto.";"
A:I have 7 pens in my hands, which one *(NO do)Would* you like?
B:I would like that one
A:Which one?
B:the green one, can you give me the red ones too?
A:Of course"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 38. Cómo usar EITHER y NEITHER para decir TAMPOCO

```r
EITHER y NEITHER: Significado, Pronunciacion, Estructura;"
Significado:Tampoco,
Pronunciacion: EITHER=/ider/ NEITHER=/nider/
Estrutura EITHER: Pronombre+Auxiliar(-)/to be(-)/v.modal(-)+EITHER
Estrutura NEITHER: neither+Auxiliar(+)/to be(+)/v.modal(+)+Pronombre"

I wouldn´t go with them (contestar EITHER y NEITHER);"
I wouldn´t either / Neither would I / Me neither"

They shouldn´t drink alcohol (contestar EITHER y NEITHER *we*);"
We shouldn´t either / neither should we / Us neither"

Griselda isn`t in her house (contestar EITHER y NEITHER *her mother*);"
Her mother isn´t either / neither is her mother / Her mother neither"

I don´t like onion (contestar EITHER y NEITHER *My wife*);"
My wife doesn´t either / neither does My wife / My wife neither"

Jhon is not saving money (we/neither);"
neither are we / Us neither"

She couldn´t help me(I/either);"
I couldn´t either / Me neither"

I can`t believe it (they/neither);"
neither can they / Them neither"

Claudia doesn´t love you (Juan/either);"
Juan doesn`t either / Juan neither"

1. I am not afraid of heights. (I/either);"
I am not either / Me neither"

2. They don´t have a big house. (we/neither);"
neither do we / Us neither"

3. Martha doesn´t like maths. (you/either);"
You don´t either / You neither"

4. She shouldn´t smoke cigarette. (they/either);"
They shouldn´t either / them neither"

5. John couldn´t help me yesterday. (I/neither);"
neither could i / Me neither"

6. I wouldn´t go to that place alone. (she/either);"
She wouldn`t either / Her neither"

7. We don´t like those cars. (I/neither);"
Neither do I / Me neither"

8. My sister is not here. (my parents/either);"
My parents *aren`t* either / My parents neither"

9. My boss wouldn´t let me go home now. (mine/neither);"
Neither would mine / Mine Neither"

10. Griselda doesn´t want us to go to the shopping mall (I/neither);"
Neither do I / Me neither"

11. Luis can´t open his new present. (we/either);"
We can´t either / Us neither"

12. Tony and Mari can´t play the piano. (You/neither);"
Neither can you / You neither"

13. Angela wouldn´t like you to watch that movie alone. (Her husband/neither);"
Neither would her husband / Her husband neither"

14. My cousin Tony isn´t behaving well. (My son/either);"
My son isn`t either / My son neither"

15. They aren´t studying English every day. (You/either);"
You aren`t either / You neither"

16. Michael doesn´t practice English very often. (Gina/neither);"
Neither does Gina / Gina Neither"

17. There aren´t flowers in my house. (Her house/either);"
*(falto)There* aren`t in her house either / Her house neither"

18. We are not buying food today. (We/neither);"
Neither are we / Us Neither"

19. He doesn´t want to go with them. (I/either);"
I *don´t* either / Me neither"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 39. Técnica de enlace y ejercicio de escucha con preguntas cortas

Consejos de escucha

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--  divicion titulo, separacion titulo, 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 40 Examen Basico

Ir a fuente para re-hacer examen.

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 41. To be Was y Were - Pasado progresivo o continuo / Uso de ING

```r
#- pasado-progresivo, was, were, was vs were vs was
was y were: Reparticion tobe, Traduccion;"
Uso/reparticion
  Was(singular y no contables): I, he, she it
  Were(plural y contables): We, you, they

Traduccion: ej: 
  *I was = yo era/estaba*, 
  *we were = nosotros eramos/estabamos*

"

Ella fue mi novia;"
she was my girlfriend"

Ana y Bety estuvieron hablando ingles;"
Ana y Bety were speaking english"

Luz o Marta estuvo roncando anoche;"
Luz o Marta *was* snoring last night.
Nota: Se uso was ya que estamos indicando 'o'
por lo que se manejacomo singular"

Yo estaba por el banco pero mis padres estaban dentro del banco;"
I was at the bank but my parents were in the bank"

Charlie estaba barriendo el piso;"
Charlie was sweeping the floor"

(Corrige Y explica)I was learning a new topic now;"
Correccion: I *am* learning a new topic now
Explicacion: Estamos usando el advervio *now* por lo cual no podemos
mesclar pasado y presente"

1. John estaba en su casa;"
John was in his house
"

2. Ellos eran compañeros de clase;"
they were classmates
"

3. Pilar o Carmen estuvo en el cine con Camilo;"
Pilar or Carmen was in the cinema with Camilo
"

4. Mary estuvo en el hospital esta mañana;"
Mary was in the hospital this morning
"

5. Yo estuve comiendo pizza el lunes y mi amiga estuvo viendo una película;"
I was eating pizza on monday and my famale friend was watching a movie
"

6. Mis vecinos estuvieron llamando la policía anoche. La policía estuvo en mi casa;"
My neighbors were calling *(sobra)to* the police *(NO tonight)last night*. The police were in my house
"

7. Nosotros estábamos en mi casa. Estuvimos cantando y bailando con los niños;"
We were in my house. We were singing and dancing with the children
"

8. Ella y él estuvieron en un restaurante chino. Ellos fueron amigos en la escuela;"
She and he *(NO was)were* in *(NO the)a* chinese restaurant. they were friends in the school
"

9. Yo estaba con él y él estaba conmigo. Ellos estaban con ella y ella estaba con ellos;"
I was with him and he was with me. They were with her and she was with *(NO their)them*
"

10. Claire era la madre de él. Juan era el padre de ellos. Ellos estaban jugando en el parque ayer;"
Claire was his mother. Juan was their *father*. they were playing in the park yesterday
"

11. Smith __ in the street this morning;"
was
"

12. Paul or Mark __ opening the store;"
was
"

13. I __ with my boy friend drinking some beers;"
was
"

14. We __ having fun;"
were
"

15. The weather __ really cold but we __ swimming in the pool;"
was, were
"

16. She __ walking quite fast yesterday;"
was
"

17. (convertir a pasado)Jane and Michael are doctors;"
Jane and Michael were doctors
"

18. (convertir a pasado)She´s in the museum;"
She was in the museum
"

19. (convertir a pasado)It´s a big dog;"
It was a big dog
"

20. (convertir a pasado)You´re my best friend;"
you were my best friend
"

21. (convertir a pasado)We are studying French;"
We were studying french
"

22. (convertir a pasado)Paula or Karla is sleeping on my bed;"
Paula or Karla was sleeping on my bed
"

23. (convertir a pasado)I´m really busy;"
I was really busy
"

24. (convertir a pasado)He´s chatting with his girlfriend;"
He was chatting with his girlfriend
"

25. (convertir a pasado)Lukas and I are tired. We are working hard;"
Lukas and I were tired. We *were* working hard
"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 42. Adverbios de tiempo en PASADO y PRESENTE que debes aprender

```r
morning, afternoon, evening;"
Mañana, tarde, noche"

ayer en la noche;"
yesterday in the evening"

Last year she __ 22, so she __ 23 now;"
was, is"

Today the weather __ nice, but yesterday it __ cold;"
is, was"

Pablo __ hungry the other day, but at the moment he __ full;"
was, is"

My parents __ currently living in spain, but they __ here last month;"
are, were"

I __ sleeping yesterday morning, now I __ practicing sports;"
was, am"

"Hello, I _1_ Victor. Yesterday, I _2_ in my house with all my family
We _3_ doing different activities. I _4_ with my friend Eduardo; we _5_
in my bedroom studying English and eating candies Right now, my sister
Alejandra _6_ in the living room with her friends; they _7_ watching TV.
Last night, they _8_ watching horror movies all night The day before
yesterday, my mother and my grandmother _9_ in the garage. My father _10_
painting the dog´s house, and my uncle _11_ fixing his car They _12_
currently on the sofa watching a soccer game. The other day my
grandfather _13_ in the garden with my grandmother, they _14_ singing
and dancing. Now, my grandpa _15_ planting some flowers";"
1=am, 2=was, 3=were, 4=was, 5=were, 6=is, 7=are, 8=were, 9=were, 10=was,
11=was, 12=are, 13=was, 14=were, 15=is"

a. En este momento ella está limpiando su oficina;"
*At* this moment she´s cleaning *her* office"

b. Carol y Jack estuvieron en Bahamas el verano pasado;"
Carol and jack were in bahamas *(sobra)on* last summer"

c. Actualmente estoy estudiando inglés, trabajando y practicando deportes;"
Currently i´m studying english, working and practicing sports"

d. El viernes pasado Juan estuvo con nosotros, pero hoy está en Perú;"
*(sobra)on* last friday Juan was with us, but today he is in Peru"

e. El año pasado yo era un conductor, hoy en día soy un ingeniero;"
last year i was a driver, *(NO today)nowadays* i´m an engineer"

f. Mi padre tenía 70 años. Mi madre tiene 60 y mi hija tiene 4;"
my father was 70 years *(falto)old*, my mother is 60 and my daughter is 4"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 42.1. Uso de THERE WAS y THERE WERE con HOW MANY y HOW MUCH

```r
#cuando se usa there was, como se usa there was
(traduccion, cuando se usa): there was/there were - how much/how many;"
there was/there were
  traduccion
    habia / hubo
    (Cuidado there was no signifca habia o hubo y tambien para there were,
    se traducen igual)

  cuando se usa
    there was: singular
    there were: Plural

how much/how many
  traduccion
    how much: ¿Cuanto(a)?
    how many: ¿Cuantos(as)?

  cuando se usa
    igual que español, ej.
      cuanta agua = how much water
      cuantas botellas  = how many bottles
"

¿Cuanta sal habia en la boslsa?;"
¿How much salt *was there* in the bag?

*there was*: Esta mal por que debemos respetar la estructura
"

(+,-,?,=)Habia poca sal en la bolsa;"
(+)there was little salt in the bag
(-)there wasn't little salt in the bag
(?)Was there little salt in the bag?
(=)Yes, there was / No there wasn't
"

(+,-,?,=)Habia pocas naranjas en la mesa;"
(+)There were few oranges on the table
(-)There weren't few oranges on the table
(?)Were there few oranges on the table
(=)Yes, there were / No there weren't
"

¿Cuantos perros habia en el carro? =habia dos perros;"
How many dogs were there in the car? =There were two dogs
"

¿Habia gatos en la imagen? =No, no habia;"
Were there cats in the image? =No, there weren't
"

¿Cuanta agua habia en la picina? =Habia mucha agua (no usar a lot);"
How much water was there in the pool? =There was much water
"

¿Cuantas platanos habia en la imagen? =Habia dos platanos;"
How many babanas were there in the picture? =There were two bananas
"

1. Había una pera en el refrigerador;"
There was a pear in the refrigerator
"

2. Habia dos manzanas sobre la mesa;"
There were two apples on the table
"

3. Habia una mesa en la cosina;"
There was a chair in the kitchen
"

4. Habia diez niños jugando futbol;"
There were 10 children playing soccer
"

5. ¿Habia zorros en el zoologico?;"
Were there foxes in the zoo?
"

6. ¿Habia un carro en el estacionamiento?;"
Was there a car at the parking lot?
"

7. ¿Habia dos tomates en el suelo?;"
Were there two tomatoes on the ground?
"

8. ¿Habia muchos relojes en la tienda?;"
Were there many watches in the store?
"

9. No habia un cuchillo en el cajon;"
There was not a knife in the drawer
"

10. No habia un percado en el lago;"
There were not fish in the lake
"

1. ¿Cuanta sal hay? =Hay medio salero;"
How much salt is there? =there is half salt shaker
"

2. ¿Cuantos carros habia? =habia 6 carros;"
How many cars were there? =There were six cars
"

3. ¿Cuantos perros habia? =Habia un perro;"
How many dogs were there? =There was one dog
"

4. ¿Cuanta leche hay? =Hay una jarra llena;"
How much milk is there? =There is a full jar
"

5. ¿Cuantas pelotas habia? =habia 10 pelotas;"
How many balls were there? =There where ten balls
"

7. ¿Cuanto polvo habia? =habia algo de polvo;"
How many dust was there? =There was some dust
"

```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### Uso de There con los diferentes tiempos

```r
(con tiempos) There: ¿pasado, presente, futuro, modales, perfecto pasado?;"
-Pasado
. -There was a meeting
.   -Hubo una reunion
.
. -There were 2 meetings
.   -Habia 2 reuniones


-Presente
.  -There is 1 meeting
.    -hay una reunion
.
.  -There are 2 meetings
.    -hay 2 reuniones


-Futuro
.  -There will be 1 meeting
.    -Habra una reunion

.  -There will be 2 meetings
.    -Habra dos reuniones

.  -There is going to be 1 meeting
.    -Va ha haber una reunion

.  -There are going to be 2 meetings
.    -Va ha haber dos reunion


-Futuro en pasado
.  -There was going to be 1 meeting yesterday but not anymore
.    -Iba a haber 1 reunion ayer pero ya no

.  -There waere going to be 2 ...
.    -Iba a haber 2 ...


-Modales
.  -There can be (1|2) meeting(s)
.    -Puede que haiga ...

.  -There could be (1|2) meeting(s)
.    -Podria haber ...

.  -There would be a meeting if ...
.    -Habria una reunion si ...

.  -There should be (1|2) meeting(s)
.    -Deberia haber ...

.  -There has to be 1 meeting
.    -Tiene que haber una...

.  -There have to be 2 meeting
.    -Tiene que haber dos...

.  -There must be (1|2) meeting(s)
.    -Debe haber ...
.    -Es posible que haiga

.  -There may be (1|2) meeting(s)
.    -Puede que haya ...

.  -There might be (1|2) meeting(s)
.    -Puede haber ...


-Pasado Perfecto 
.  -There has been (1|2)
.    -Habia habido (1|2)


-Presente perfecto
.  -There has been 1
.    -Ha habido 1

.  -There have been 12
.    -Ha habido 2


-Futuro perfecto
.  -There will have been
.    -Habra habido 1
"


Hubo una reunion;"
There was a meeting
"

Habia 2 reuniones;"
. -There were 2 meetings
"

Hay una reunion;"
There is 1 meeting
"

Hay 2 reuniones;"
There are 2 meetings
"

Habra una reunion;"
There will be 1 meeting
"

Habra dos reuniones;"
There will be 2 meetings
"

There is going to be 1 meeting;"
Va ha haber una reunion
"

There are going to be 2 meetings;"
Va ha haber dos reunion
"

There was going to be 1 meeting yesterday but not anymore;"
Iba a haber 1 reunion ayer pero ya no
"

Iba a haber 2 reuniones;"
There waere going to be 2 ...
"

Puede que haiga 1(2) reunion(es);"
There can be (1|2) meeting(s)
"

Podria haber 1(2) reunion(es);"
-There could be (1|2) meeting(s)
"

Habria una reunion si 1(2) reunion(es);"
There would be a meeting if ...
"

Deberia haber 1(2) reunion(es);"
There should be (1|2) meeting(s)
"

Tiene que haber una reunion;"
There has to be 1 meeting
"

Tiene que haber dos reuniones;"
There have to be 2 meeting
"

(Solo una en ingles) Debe haber ... = Es posible que haiga;"
There must be (1|2) meeting(s)
"

Puede que haya 1(2) reunion(es);"
There may be (1|2) meeting(s)
"

Puede haber 1(2) reunion(es);"
There might be (1|2) meeting(s)
"

Habia habido 1(2) reunion(es);"
There has been (1|2)
"

Ha habido 1(2) reunion(es);"
There has been 1
"

Ha habido 2;"
There have been 12
"

Habra habido 1;"
There will have been
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 43. Usos de WHILE en el pasado TO BE en oraciones afirmativas

```r
while, a while, a little while, once in a while;"
Mientras/aunque, un rato, un ratito, de vez en cuando"

equivalente de little en español;"
al ito, ej. perrito, zapatito, celularcito, aunque igual funciona
como pequeño"

little cat, little persons, little mause;"
gatito, personitas, ratoncito"

A noche mientras tu estabas manejando yo estaba durmiendo;"
last night while you *were* driving I was slepping"

Yo estaba estudiando ingles un rato mientras estaba escuchando musica;"
I was studying English a while, while I was listening/lisening/ *to* music
I was studying English a while, while listening to music, (podemos hacerlo
por que estamos hablando de la misma persona)"

Ana estuvo trabajando un ratito mientras el estuvo cocinando;"
Ana was working a little while while he was cooking"

Luis y kelly estuvieron esperandote por un rato;"
Luis and kelly were waiting *for* you for a while"

Mi hijo estaba comiendo pizza mientras jugaba;"
my son was eating pizza while eating"

Nosotros estuvimos corriendo de vez en cuado;"
We were running once in a while"

Despues de un rato balidando estuvimos bebiendo agua;"
after a while dancing we were drinking water"

1. El domingo pasado, ellos estaban cantando mientras miraban la pantalla;"
Last sunday, they were *(escritura)singing* while watching *(falto)at* the screen"
2. Ayer, mi esposa estaba preparando el desayuno mientras yo estaba tomando una ducha;"
yesterday, my wife was preparing *(sobra)the* breakfast while I was taking a *(NO shower)shawer*"
3. El chef estuvo pelando las papas por un rato mientras hervía el agua;"
The chef was peeling the potatoes for a while, while boling the water"
4. Usted estaba caminando mientras nosotros estábamos corriendo;"
You were walking while *(NO us)we* were running"
5. El año pasado estuve comiendo comida chatarra de vez en cuando;"
last year I was eating junk food once in a while"
6. Mientras Elizabeth estuvo en el sofá su madre estuvo leyendo un rato;"
While elizabeth was on the sofa, her mother was reading a while"
7. Te veo en un rato;"
I see you in a while"
8. La clase pasada, Pacho estaba explicándome mientras yo estaba escribiendo;"
last class, Pacho was *explaining* to me while I was writing"
9. Tú estabas hablando mientras comías y ella estaba fumando mientras hablaba;"
You were speaking while eating and she was smoking while speaking"
10. Mientras estuve durmiendo, el teléfono estuvo sonando por un rato;"
While i was sleeping, the phone was *ringing* for a while"
11. Diego estuvo roncando mientras dormía y yo estuve escuchándolo toda la noche;"
Diego was snoring while sleeping and I was listening to him all night"
```

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

----

<!--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-->

### 44. Cuando y como usar FOR y AGO con cuantificadores en pasado

```r
for;"
por, para, durante, desde hace"

ago;"
hace /agou/"

Nombre del grupo y trad: many, much, some, any, none, a lot of ,little, few;"
Nombre del grupo:
  = Quantifiers
Traduccion
  = many (muchos)
  = much (mucho)
  = some (algunos)
  = any (alguno)
  = none (ninguno)
  = a lot of / lots of (muchos/as)
  = little / a little (poco/un poco)
  = few / a few (pocos/unos pocos)
  = a couple/capol/ (un par)"

La mujer estuvo cargando los bebes desde hace varias horas;"
The woman was *carrying* the babies..
Mal : ..since several hours
Bien: ..for several hours
Tip: *desde hace varias x* convertir a *por varias*"
La mujer estuvo cargando los bebes hace varias horas;"
The woman was *carrying* the babies..
Mal:  ..ago several hours
Bien: ..several hours ago"

Gabi y Lane fueron amigas desde hace un par de años;"
Gabi and Lane were friends for a couple of years"
Gabi y Lane fueron amigas hace un par de años;"
Gabi and Lane were friends a couple of years ago"

La mujer estuvo paseando a la mujer por casi 30 minutos;"
the nurse was walking the woman for almost 30 minutes"
La mujer estuvo paseando a la mujer durante casi 30 minutos;"
the nurse was walking the woman almost 30 minutes ago"

Ellos estuvieron grabando la pelicula hace unos cuantos años;"
They were filming the movie *a few* years ago"
Ellos estuvieron grabando la pelicula desde hace unos cuantos años;"
They were filming the movie for *a few* years"

Estuvimos asando la carne alrededor de 10 minutes;"
we were grilling the meat for around 10 minutes"
Estuvimos asando la carne durante 10 minutes;"
we were grilling the meat around 10 min ago"

1. Ana was reading a book for a couple of hours while her husband was watching TV;"
Ana estuvo leyendo un libro por un par de horas mientras su esposo esta viendo TV"
2. A few days ago, we were on vacation. Now, we are studying for two hours every day;"
hace unos *cuantos* dias, estabimos de vacaciones, ahora, estamos estudiando *por* 2 horas cada dia"
3. Something was happening in my house four days ago;"
Algo *(No habia)estuvo* pasado in mi casa hace cuatro dias"
4. My dog was barking for 30 minutes while looking at the door;"
Mi perro estaba ladrando por 30min mientras miraba a la puerta"
4. Mike was drinking some beers while driving last night. He was driving for 1 hour;"
Mike estuvo bebiendo algunas cervezas mientras manejaba anoche, El estuvo manejando por 1hr"
5. You were saving money for almost 10 years. Now, you´re enjoying your new car;"
Tu estabas ahorrando dinero por casi 1o años. Ahora, estas disfrutando tu nuevo carro"

1. (corrige)Now, we were riding our bikes, but yesterday we were jogging for a while;"
now, *we´re* riding our bikes, but yesterday we were jogging for a while"
2. (corrige)I´m currently studying while working but I was at the beach with my friends a week ago;"
Todo esta bien"
3. (corrige)The day before yesterday, Maggie and Peggy are walking their dogs for several hours;"
The day before yesterday, Maggie and Peggy *were* walking their dogs for several hours"
4. The dogs were playing in the park while Maggie and Peggy was chatting for a while;"
The dogs were playing in the park while Maggie and Peggy *were* chatting for a while"
5. (corrige)Some minutes ago, I am trying to open the door of my house;"
Some minutes ago, I was trying to open the door of my house"
6. (corrige)Now I was on the sofa singing song while eating with my wife;"
now I´m on the sofa singing *a* song while eating with my wife"
7. (corrige)We are singing songs for many hours´ yesterday;"
We *were* singing songs for many hours´ yesterday"
```

### 45. How-long/How-long-ago/How-much-time/Since, en pasado TO BE

```r
How long? / How much time? / How long ago?;"
Cuanto tiempo?(mas comun)/Cuanto tiempo?(mas especifico)/hace cuanto tiempo?"

¿Hace cuanto tiempo estabas tu manejando?;"
How long ago were you driving?"

"¿Cuánto tiempo estuvo durmiendo Michael?
- How much time was sleeping Michael?
- How much time was Michael sleeping?
- How long was sleeping Michael?
- How long were Michael sleeping?";"
How much time was Michael sleeping?"

-Hace cuanto tiempo estovo ella en londres -ella estuvo ahi hace 6 meses;"
- How long ago was she in london? -She was there 6 months ago."

"- Cuanto tiempo estuvieron ellos cocinando mientras pesaban?
 - Ellos estuvieron cocinando desde esta mañana";"
-How long *were* they cooking while fishing?
-They were cooking since this morning"

¿Cuanto tiempo estas estudiando ingles? -Yo estoy estudiando 2hr al dia;"
How long are you studing english? -I´m *studying* *(falto?)for* 2 hours every day"

1. ¿Cuánto tiempo estuviste viviendo en Canadá? -por dos años;"
How long were you living in Canada? -I was living there for 2 years"
2. ¿Hace cuánto tiempo estuvo Mike en el hospital? -hace dos semanas;"
how long ago was Mike in the hospital? -since 2 weeks / Mike was in the hospital 2 weeks ago"
3. ¿Hace cuánto tiempo fuiste un taxista? -hace un par de semanas;"
-How long ago *(orden)were you* a taxi driver? -I was *(falto)a* taxi driver for a couple of weeks *(falto)ago*"
4. ¿Hace cuánto tiempo estuvo Ana cambiando las llantas del carro? -desde esta tarde;"
How long ago was ana changing the car´s tires? -she was changing it since this *(NO morning)afternoon*"
5. ¿Cuánto tiempo estuvimos cantando en la fiesta? -por muchas horas;"
How long were we singing at the party -we were singing for many hours"
6. ¿Cuánto tiempo estuvieron ellos visitando a su madre? -por tres días;"
How long were they visiting *their* mother? they were visiting their mother for three days"
7.¿Cuánto tiempo estuvieron Omar y Luis hablando mientras corrían? -durante 30 min;"
How long were Omar and Luis talking while running? -they were talking for thirty minutes"
8. ¿Hace cuánto tiempo está Erika haciendo ejercicio? -desde ayer en la mañana;"
How long *ago* is Erika doing exercise? -she is doing exercise since yersterday in the morning"

(Trad, past/present?) So far;"
hasta ahora/Hasta el momento - presente"

hasta ahora/Hasta el momento;"
So far"

(Trad, past/present?) Right now;"
Justo ahora - present"

(Trad, past/present?) Currently;"
Actualmente - present"

Actualmente;"
Currently"

(Trad, past/present?) A week ago;"
Hace una semana - past"

(Trad, past/present?) A month ago;"
hace un mes - past"

hace un mes;"
A month ago"

(Trad, past/present?) Last night;"
La noche pasada / Anoche - past"

(Trad, past/present?) Today;"
Hoy -  past/present"

(Trad, past/present?) The day before yesterday;"
Antier - past"

Antier;"
The day before yesterday"

(Trad, past/present?) Last week;"
La semana pasada - past"

(Trad, past/present?) By now;"
Por ahora - present"

(Trad, past/present?) At this time;"
En este momente - present"

(Trad, past/present?) Now;"
Ahora/actualmente - presente"

(Trad, past/present?) Since;"
Desde/Desde entonces - past/present"

(Trad, past/present?) Last year;"
el año pasado - past"

(Trad, past/present?) Yesterday;"
ayer - past"

(Trad, past/present?) Yesterday afternoon;"
ayer en la tarde - past"

(Trad, past/present?) Last class;"
La clase pasada - past"

(-,?)She WAS happy yesterday;"
(-)She wasn´t happy yesterday
(?)was she happy yesterday?"

(-,?)You were in Santa Marta last week;"
(-)You weren´t in Santa Marta last week
(?)Were you in Santa marta last week?"

(-,?)Carlos and Marcos were at the disco last weekend;"
(-)Carlos and Marcos weren´t at the disco last weekend
(?)Were Carlos and Marcos at the disco last weekend?"

(-,?)Jorge and Karla were in Paris a month ago;"
(-) Jorge and Karla weren´t in Paris a month ago.
(?) were Jorge and Karla in Paris a month ago?"

"Hi! My name´s Frank. Today I __ in my office with my friends, but
yesterday I __ at the park with my wife. We __ very happy together.
Now, my wife __ in her job. She __ working in a big company, but last year,
she __ working in a pet shop";"
Hi! My name´s Frank. Today I _am_ in my office with my friends, but
yesterday I _was_ at the park with my wife. We _were_ very happy together.
Now, my wife _is_ in her job. She _is_ working in a big company, but
last year, she _was_ working in a pet shop"

"The day before yesterday, my sister and I __ at the cinema, we __
watching a very good movie about robots in the space, but now my sister
__ with her boyfriend in the stadium, they __ watching a soccer game.
Last weekend, they __ in the stadium too";"
The day before yesterday, my sister and I _were_ at the cinema, we
_were_ watching a very good movie about robots in the space, but now my
sister _is_ with her boyfriend in the stadium, they _are_ watching a
soccer game. Last weekend, they _were_ in the stadium too"

1. Is Frank in his house today? (No lo esta) (resp larga o because);"
No, because he´s in his office with his friends"

2. Was Frank at the park with his son yesterday (no porque estaba con su esposa)?;"
No, because he was at the park with his wife"

4. Is Frank´s wife working in a pet shop now? (no, ella trabaja en una gran compania);"
No, because *now* she is working in a big company"

5. Were Frank and his sister watching a bad movie? (la peli era buena);"
No, they weren´t"

"Él es Jackson. Ayer, él y su hermana Luisa estuvieron visitando sus amigos Mike y Peter. Mike
estuvo trabajando con Jackson en una gran compañía por 7 años y Luisa estuvo trabajando con
Peter desde el año pasado. Hace un año, ellos estuvieron de vacaciones en Costa Rica.
Anoche, estuvieron tomando vino mientras hablaban de sus vacaciones por casi 8 horas. Luisa
les estaba mostrando las fotos mientras recordaban esos momentos en ese país. Todo mundo
estaba feliz";"
He´s Jackson. yesterday, he and his sister Luisa were visiting their friends Mike and Peter. Mike 
was working with Jackson in a big company for seven years, and Luisa was working with
Peter since last year. A year ago, they were on *(NO vacation)vacation* *(No in)to* Costa Rica.
Last night, they were drinking wine while talking *about* their *vacation* for almost 8 hours.
Luisa was showing *them the* photos while *they* remembered those moments in that country.
everybody *was* happy"
```

### 48. Uso de So/Too/So-much/So-many/Too much/Too-many en pasado

```r
So + Adj/Adv, so much, so many;"
tan x       , tanto/a, tantos/as"

Too + Adj/Adv, too much   , to many;"
demasiado x  , demasiado/a, demasiados/as"

Yo estaba tan/demaciado feliz que ...;"
I was so/too happy that"

Ellos estaban bailando tan bien que ...;"
they was dancing so/too well that"

Nosotros estabamos tomando tanta/demasiada agua;"
We were drinking [so much/too much] water"

You were eating [so many/too many] apples that ...;"
Tu estabas comiendo {tantas/demasiadas} manzanas que ..."

I was working [too much/soo much];"
Yo estaba trabajando demasiado/tanto"

Yo estaba tan cansado que estube durmiendo por casi 15 horas;"
I was so tired that I was sleeping for almost 15 hours"

Tu no deberias perder tanto tiempo;"
You shouldn´t *lose* so *much* time"

Estuviste en el telefono por demasiadas horas;"
You were *(in dentro)on* the phone for too many hours"

Mi amigo estuvo tan borracho que no pudo manejar;"
My friend was *too*so drunk that *he* could not drive"

1. Yo no estuve trabajando tanto ayer;"
I wasn´t working so much yesterday"

2. Ese juego estuvo tan divertido que estuvimos demasiado emocionados todo el tiempo;"
That game was so fun that we were too excited all *the* time"

3. Anoche, Juan estuvo estudiando tantos temas que estuve ayudándolo por varias horas;"
last night, Juan was studying so many topics that I was helping him for *several hours*"

4. La clase estuvo tan fácil que estuve haciendo los ejercicios demasiado bien;"
The class was so easy that I was doing the *exercises* too well."

5. El mes pasado, estuvimos descansando demasiado;"
Last month, we were resting too much"

6. ¿Estuviste de vacaciones por tantos días?;"
were you on vacation for so many days?"

7. Mis vecinos estuvieron haciendo demasiado ruido esta mañana;"
My neighbors were *making* too much noise this morning"

8. Karla estuvo gastando tanto dinero en Paris que ahora no puede gastar tanto;"
Karla was spending so much money in Paris that now she can´t *(NO spending)spend* so much"

9. No quiero que comas tanto. Ayer, estuviste comiendo demasiadas hamburguesas;"
I don´t want *(sobra that)* you *(falto)to* eat so much. yesterday, you were eating too many *burgers*"

10. Antier, Juan estuvo cocinando tanta comida que no pudo descansar;"
The day before yesterday, Juan was cooking so much food that he *(NO can´t)couldn´t* rest"

11. Estuvimos viendo tantos programas de televisión ayer que no pudimos hacer la tarea;"
We were watching so many TV programs yesterday that we *(NO can´t) couldn´t* do the homework"
```

### 49. Cómo decir y preguntar la hora en inglés

```r
#-time
Cuando usar *past/after* y *to* cuando se dice la hora y ej. de c/u;"
- *past* cuando esta dentro de los primeros 30min
- *to*: Cuando esta dentro de los ultimos 30min
- 2:20 twenty past two
"

Como decir la hora;"
- Formal: It´s + MINUTES + past/to + HOUR
  - 5:15: ej: It´s + a quarter past + five
- Informal: It´s HOUR + MINUTES
  - 2:13: It´s It´s two thirteen
- En Punto: It´s + HOUR + O´Clock
  - 1:00 = One O´Clock"

Son las 4:15;"
Formal: It´s a quarter past four
Informal: It´s four fifteen"

Son las 8:30;"
Formal: It´s half past eight
Informal: It´s thirty past eight"

Son las 10:55;"
F: It´s five to eleven
I: It´s ten fifty five"

-What time is it? -Son las 7:05 (informal/formal);"
-Que hora es?
-F:It´s five past seven
-I:It´s seven o five (se usa el *o* cuando los min son menor que 10)"

Excuse me, can you tell me the time? please -Son las 9:40;"
-Perdone, puede deceirme la hora? porfavor -It´s twenty to ten"

Do you know what time is it? -Son las 3:00;"
-Sabes que hora es? -It´s three O´Clock"

Do you know the time? -Son las 6:35;"
-Sabes la hora? -It´s twenty five to seven"

What is the time? please -Son las 1:28;"
-Que hora es? por favor -It´s twenty eight past one"

Have you got the time? -Son las 4:45;"
-Tiene la hora? -It´s a quarter to five"
```

### 50. cinco preposiciones para decir la hora en inglés que debes conocer

```r
#- time
(explica diferencias) You should be here [by/at/at-about] 6 O´Clock;"
- By: Puede ser antes, o en punto.
- At: Debe ser en punto.
- At: about: Puede ser antes, en punto, o poco despues
"

(explica diferencias )You should be here [on/in] time;"
On: Debe ser en punto
In: Debe ser antes de esa hora"

A que hora estaba ella en la parada de autobus - ella estaba [por/alrededor de] las 8:30 en la parada de autobus;"
-What time was she at the bus stop?
-She was at the bus stop *at about*  half past eight"

-A que hora tu empiezas a trabajar? -Yo empiezo a trabajar a las 7:31;"
-What time do you start to work? -I start to work at seven thirty one"

-A que hora nos podemos encontrar esta noche? -Nos podemos reunir a las 9:45 (tantito antes);"
-What time can we meet tonight
We can meet by a quarter to ten"

-A que hora sale el tren? -El tren sale en punto, 7:00 (preciso);"
-What time does the train leave?
-The train *leaves* on time, at seven o´Clock"

-A que hora estabas en el aeropuerto -Estaba a tiempo (con tiempo);"
-What time were you at the airport? -I was at the airport in time"
```

### 53-A. Pasado simple

```r
Como se convierte un verbo a pasado simple;"
Agregando la terminacion 'es' ej. Avoid->Avoided, Sneeze->Sneezed
"

Cual es el verbo auxiliar del pasado simple;"
did
"

Estructura del pasado simple (+, -, ?) y ejemplo;"
(+)Sujeto + verbo simpPast + complemento
  - We bought the dinner
(-)Sujeto + Auxiliar + Not + verbo forma simple + complemento
  - We didn`t buy the dinner
(?)Auxiliar + Sujeto + verbo forma simple + complemento
  - Didn't we buy the dinner?
"

(+,-, ?) nosotros rebanamos la cometa;"
(+)We sliced the kite
(-)We didn`t slice the kite
(?)Did we slice the kite
"
```

#### 52. Ejercicio de escritura en PASADO SIMPLE con omisión de Pronombres Sujeto

<!-- uso de did -->

```r
AX1. Ayer, mi amigo Mike volo de Madrid a Paris;"
Yesterday, my friend Mike *flew* from Madrid to Paris.
"

AX2.El se desperto a las 5:30 de la mañana;"
He woke up at half past five of the morning.
"

AX3.Tomo una ducha rapida, bebio algo de cafe y salio de su casa;"
He took a quick shower, drank some coffee and left his house.
"

AX4.despues de eso, Mike condujo al aeropuerto por casi 1 hora;"
after that, Mike drove to the airport for almost one hour.
"

AX5. Cuando el llego alla, estaciono el carro y se registro;"
When he arrived/got there, *(No se puede omitir por que suena raro)he* parked the car and (si se puede omitir he) checked in.
"

AX6. Luego, el desayuno y espero su vuelo;"
Then, he had breakfast and waited *for* his *flight*.
"

AX7. El avion salio a tiempo y llego dos horas despues;"
The *plane departed* on time and arrived two hours *(NO after)later*
"

AX8. Finalmente, el tomo un taxi del aeropuerto al hotel;"
Finally, he took a taxi from the airport to the hotel
"

He took a quick shower, *he* drankcofee, (Explica por que se puede evitar ese he);"
Por que la frase hahla de la misma persona y esta en el mismo tiempo.
"

-¿A que hora despertó Mike ayer? -el desperto a las 5:30 am;"
-*(sobra At)* What time *(FALTO)did* Mike wake up yesterday
-He wake up at five thirty am
"

-¿Tomó Mike algo de jugo después de la ducha? -No, no lo hizo;"
-Did Mike *(NO take)drink* some juice after *(FALTO)the* shower?
-No, he didn`t
"

-¿Estaba Mike en un hotel cuándo despertó? -No, por que el estaba en su casa;"
-Was Mike in a hotel when he wake up?
-No, because he was in his house
"

-¿Tomo Mike un taxi al aeropuerto? -No, el manejo al aeropuerto en su carro;"
-Did Mike take a taxi to the airport? -No, he drove to the airport in his car
"

-¿Cuánto tiempo condujo Mike al aeropuerto? -El condujo por casi una hora;"
-How long *did* Mike drive to the airport?
-He drove for almost 1 hour
"

-¿Qué hizo Mike cuándo llegó al aeropuerto? -El estaciono su carro y se registro;"
-What did Mike *(FALTO)do* when he arrived to the airport?
-He parked his car and checked in
"

-¿Qué hizo Mike después del desayuno? -El espero su vuelo;"
-What did Mike *(FALTO)do* after *(NO have)the* breakfast?
-He waited his *(FALTO)for* flight
"

-¿Llegó el avión tres horas después? -No, el avion tardo 2 horas;"
-Did the *plane* arrive three hours later?
-No, the plane arrived two hours later
"
```

#### 53. Conectores de contraste en PASADO SIMPLE

```r
Cuales son los conectores de contraste y ejemplo en español;"
Pero, Sin enbargo, aunque, Aunque/pero
Ej. El niño rompio la ventanapero no se lastimo"

But, However, although/ouldo/, though/dou/, even though;"
Pero,
Sin enbargo,
aunque,
Aunque/pero,
Aunque(enfacis)"

El niño rompio la ventana, pero no se lastimo;"
The boy broke the windows, but he *didn`t* [hurt himself]=[get hurt]"

Sabiamos el tema, sin enbargo, no pasamos el examen;"
We knew the topic, However, we didn`t *pass* the exam"

Aunque ellos limpiaron la casa, no lavaron el carro;"
Although they *cleaned* the house, they didn`t wash the car"

Aunque no fui a clase, practique en casa;"
though I didn`t go to class, I practiced at home"

Explica como se puede remplazar el but de **I didn`t go to class, *but* I practiced**;"
Podemos quitar el *but* y poner al final un though"

I didn`t go to class, I practiced at home though;"
Yo no fui a clase, pero practique en casa."

The boy broke the windows, he didn`t get hurt though;"
El niño rompio la ventana, pero no se lastimo"

Aunque ella vino temprano, no comenzo a trabajar;"
Even though she came early, she didn`t start to work*ing*"

Ellos no estuvieron de acuerdo, pero todavia son amigos;"
They didn`t agree, but they are *still* friends"

No te asigne esa tarea, aunque la hiciste genial;"
I didn`t assign you that task, Although you did it great"

Andres no me creyo, aunque yo le conte la verdad;"
Andres didn´t believe me, even although I said/told him the truth"

1. Leiste ese libro sin embargo, no respondiste las preguntas;"
You read that book *however*, you didn`t answer the questions"

2. A ellos no les gustó la fiesta, pero a mi me gustó;"
They didn`t like the party, but I *liked* it"

3. Mark corrió durante una hora, sin embargo, él no está cansado;"
Mark ran for one hour, however, he is not tired"

4. Decidimos viajar a New York con la familia aunque sólo fue dos días;"
We decided *to* travel to New York with the family although *it was only* two days"

5. Nosotros fuimos a celebrar aunque no ganamos el campeonato;"
We went to celebrate *even though* we didn`t win the championship"

6. Maria no pudo ayudarme, pero ella lo intentó;"
Maria *couldn´t* help me, but she tried"

7. John visitó París, pero no gastó mucho dinero;"
John visited Paris, he didn`t spend a lot of money though"

8. Ellos se sentían enfermos, pero querían visitar los lugares turísticos de la ciudad;"
They felt sick, but they wanted *to* visit the *touristic* places of the city"

9. Ellos no tenían cinco dólares, pero me prestaron dos;"
They didn`t have five dollars, but they *lent* two"

10. Aunque Karla necesitaba algo de ayuda, ella pudo hacerlo ella misma;"
Although Karla needed some help, she *could do it* herself"
```

#### 55. Cómo usar las Wh Questions en Pasado Simple

<!-- uso de did -->

```r
Estructura de pasado simple con wh questions y ejemplo;"
Estructura: wh-question + did + sujeto + verbo-base + complemento
Ejemplo:    When        + did  +  he   +    go      + to the zoo?
"

-¿Cuando fue el al zoologico? -El fue al zologico ayer;"
-When did he go to the zoo? -He went to the zoo yesterday"

-¿Que hisiste ayer? -Vi una pelicula en mi casa;"
-¿What did you do yesterday? -I see a movie in my house"

-¿Como llegaron ellos aca? -ellos vinieron en taxi;"
-How did they come here? -They came here by taxi"

-¿Cuanto tiempo se quedaron en italia? -Nos quedamos en italia por 3 meses;"
-How long did you stay in Italy? -We stay in Italy for three months"

-¿Cuanto dinero gastaste el mes pasado? -Gaste mucho dinero el mes pasado;"
-How much money did you spend last mont? -I spent a lot of money last month"

-¿Por que ella no fue a trabajar ayer? -Ella fue al doctor por que se sintio mal;"
-Why didn`t she go to work yesterday? -She went to the doctor because she felt sick"

-Que tan bien ellos leyeron la historia ayer -Ellos leyeron la historia muy bien;"
-How well did they read the story yesterday? -They read the story very well"

# //////////////////////////

-¿Cuando murio Maria? -Ella murio cuando tenia 64 años;"
When did Maria die? -She died when she was 64 years old"

-¿Donde vivia Maria? -Ella vivia en un pueblo muy lejano muy cerca de la playa;"
-Where did Maria live? -She lived in a faraway town pretty close to the beach"

-¿Por que Victoria recuerda a Maria? -Porque ella era muy especial para ella;"
-Why did Victoria remember Maria? -Because she was really special to her"

¿Cuantas horas Victoria tenia que manejar para visitarla? -Ella tenia que manejar por cinco horas;"
-How many hours did Victoria have to drive to visit Her? -She had to drive for 5 hours"

-¿Donde se sentaban ellas en la playa? -Ellas se sentaban en la arena;"
-Where did they sit at the beach? -They sat on the sand"

¿Que hablaron ellas en la playa? -Ellas hablaron sobre los buenos tiempos;"
-What did they talk at the beach? -They talked about the good old times"

-¿Que hicieron en su juventud? -Ellas hicieron muchas cosas;"
-What did they do during their youth? -They did many things"

-¿Donde estudiaron ellas? -Ellas estudiaron en la misma escuela;"
-Where did they study? -They studied at the same school"

-¿Que tan inteligente era maria? -Ella era realmente inteligente;"
-How smart was Maria? -She was really smart"

-¿Que le enseñaba Victoria a Maria? -Ella le enseñaba ingles;"
-What did Victoria teach Maria? -She taught her English"

-¿Que tan a menudo ellas estudiaban metematicas? -Ellas estudiaban cada fin de semana;"
-How often did they study mathematics? -They studied every weekend"
```

#### 56. Diferencia entre Preguntas Sujeto y Objeto en Pasado Simple

```r
Cuales son las preguntas objeto y preguntas sujeto, diferencias y estructuras;"
Cuales son
- Preguntas objeto (P.O.): Tratan de averiguar por un objeto, es decir,
  por la parte derecha de la oracion, ej. ¿que es ESO?.
- Preguntas sujeto (P.S.): Tratan de averiguar por un sujeto, es decir,
  por la parte izquiera de la oracion, ej. ¿ESO que color tiene?.

Estructura
- P.O.: Wh + Aux + pron + verb + complement
- P.S.: Wh + forma(+/-) y tiempo verbo + complement.

Ejemplos
- P.O.: What did Mike buy?, What did they do?
- P.S.: Who bought a new car?, Who didn`t do the homework, en este caso
  el *didn`t* se agrega por que es la forma, en este caso negativa"

¿Quien te llamo?;"
Who called you?, Decir *Who did call you?* es incorreto, por que es
una pregunta sujeto"

¿Que paso?;"
What happened?"

¿Cuando paso?;"
When did it happen?"

¿Quien no comio pizza?;"
Who didn´t eat pezza?"

¿Cual puerta no abrio?;"
*which* door didn´t open?"

¿Quien pago la cuenta?;"
Who paid the bill?"

1. ¿Quién los besó?;"
Who kissed you?"

2. ¿Qué horneó el panadero?;"
What did the baker bake?"

3. ¿Cuál de estos hombres robó tu billetera?;"
Which *did* of *these* men stole your wallet?"

4. ¿Dónde conseguiste esa camisa nueva?;"
Where did you get that new shirt?"

5. ¿Quíen desempolvó mi radio viejo?;"
Who dusted my old radio?"

6. ¿Qué decidió Laura?;"
What did *(Orden)Laura decide*?"

7. ¿Quienes no terminaron la tarea?;"
Who didn´t finish the homework?"

8. ¿Quién no escuchó lo que dije?;"
Who *(falto)didn´t* listen *(falto)to* what I said?"

9. ¿Cuál casa compraste, la nueva o la vieja?;"
*(NO What)Which* house did you buy, the new one or the old one?, (NO
confundir con pregunta objeto)"

10. ¿Qué le pasó a esta silla?;"
What happened to this chair?"

(Crea Pregunta objeto y sujeto)Gina didn´t remember her son´s birthday;"
P.S.: Who didn`t remember her son´s birhday?
P.O.: What didn`t remember Gina?"

(Crea Pregunta objeto [When] y sujeto [Who])My friends laughed a lot yesterday;"
P.S.: Who laughed a lot yesterday?
P.O.: When did my friends laugh a lot?"

(Crea Pregunta objeto[why] y sujeto[who])My mother lit a candle in memory of her father;"
P.S.: Who lit a candle ...?
P.O.: Why did my mother light a candle?
"

(Crea Pregunta objeto[Where] y sujeto[Who])The children licked their popsicles at the park;"
P.S.: Who licked their popsicles at the park?
P.O.: What did the children do at the park?
"

(Crea Pregunta objeto y sujeto)We didn´t remember where we parked the car;"
P.S.: who didn´t remember where we...?
P.O.: What didn´t *we* remember?"
```

### 57. Futuro Will y Going to - Cómo diferenciarlos fácilmente en todas sus formas

```r
¿Diferencia entre ´will´ y ´going to´?;"
Will
  - Menos problable
  - Convierte el verbo a futuro
  - ej. i will learn = yo aprendere

Going to
  - Mas probable
  - Agrega un ´vamos´
  - ej. I going learn = yo vy a prender
"

Estructura del will;"
Sujeto + will + verboBase + Complemento
"

Estructura del going to;"
Sujeto + to be + going to + verbo base + complemento
"

(+/-/?)Nosotros aprenderemos ingles (will);"
(+)We will learn english = we´ll learn ...
(-)We will not learn english = we won´t learn ...
(?)Will we learn english?
"

(+/-/?)Nosotros vamos a aprender ingles (going to);"
(+)We are going to learn english
(-)We aren`t going to learn chinise
(?)Are we going to learn chainise?
"

Ella tendra un bebe muy pronto;"
She will have a baby very soon"

Mike no va a creerme cuando se lo cuente?;"
Mike isn`t going to believe me when I tell him"

Yo no conducire todo el camino a casa;"
I won´t drive all the way home"

¿Se van a encontrar ellos esta noche?;"
are they going to meet tonight?"

¿Realizaras los ejercicios para practicar?;"
will you do the exercises to practice?"

(+/-/?)Yo voy a tocar la puerta;"
+, I´m going to knock the door
-, I´m not going to knock the door?
?, Am I going to knock the door?
"

(+/-/?)Mi mamá comprará algunas frutas en el supermercado;"
+ My mom will buy some fruits in the supermarket
- My mom won´t buy some...
? will my mom buy some...
"

(+/-/?)Carlos y Maria comerán afuera esta noche;"
+, Carlos and Maria will eat *out* tonight
-, Carlos and Maria won´t eat...
?, will Carlos and Maria eat..?
"

(+/-/?)Esto va a dolerte;"
+, This is going to hurt you
-, This isn´t going to hurt you
?, is this going to hurt you?
"

(+/-/?)Tu vas a ayudarme a cargar las maletas;"
+ You are going to help me to *carry* the bags
- You aren´t going to help...
? are you going to help   ...?
"

(+/-/?)El profesor te explicará el ejercicio;"
+ The teacher will explain to you the exercise=[the exercise to you]
- The teacher won´t explain...
? will the teacher explain....?"

My name is Smith. My friends and I are not going to go to school tomorrow;"
Mi nombre es Smith. Mis amigos y yo no vamos a ir a la escuela mañana."

We won´t have classes because the principal will give the teachers a day off;"
No tendremos clases porque el director le dara a los profesores un dia libre"

We´re planning to do many things. In the morning;"
Nosotros planeamos muchas cosas"

we´re going to be in the gym for 1 hour;"
Vamos a estar en el gimnasio por una hora"

After training, we´re going to be in the swimming pool for a couple of hours;"
Despues de entrenar, vamos a a nadar en la picina por un par de horas"

I think, we´ll have lunch together in a very nice restaurant near my house;"
Creo, vamos a desayunar juntos en un restaurante muy lindo cerca de mi casa"

We´re going to order some of the specials;"
Ordenaremos algunos especiales"

I think, they´ll prepare pizza or pasta tomorrow, I´m not sure. I prefer pizza;"
Creo, ellos preparan pizza o pasta mañana, no estoy seguro. Yo prefiero la pizza"

so I´m going to order a big pizza with a lot of cheese and a soda;"
entonces yo voy a ordenar una pizza grande con un monton de queso y una soda"

My friends are going to order pasta because they like it very much;"
Mis amigos van a ordenar pasta porque a ellos les gusta mucho"

As you can see, we´re going to have a great day tomorrow;"
Como puedes ver, vamos a tener un gran dia mañana"

And you, What will you do tomorrow?;"
Y yu, ¿Que haras mañana?"

¿Van a ir los estudiantes a la escuela mañana? -No, ellos no;"
are the students going to *(falto go to)* school tomorrow? -No, they aren´t"

-¿ellos almorzarán juntos mañana? -Si, ellos lo haran;"
will they *(falto)have* lunch together tomorrow? -Yes, they will"

¿Comerá Smith pasta en el restaurante? -No, porque el comera pizza;"
Will Smith eat pasta in the restaurant? -No, because he will eat pizza"

¿Van los amigos de Smith a ordenar pizza? -No, ellos no;"
Are Smith´s friends going to order pizza? -No, they aren´t."
```

### 59. REFORZAR: 5 formas diferentes a WILL y GOING TO para hablar en futuro

```r
Cuales son las 5 formar de hablar en futuro (SIN usar will/going to);"
1.Usado adverbios con o sin modales
2.Futuro con [presente continuo]=[presente progresivo]
3.Futuro en pasado
4.Futuro Proximo (estar a punto de/estar por)=(BE about)
5.Futuro proximo Be + Infinitivo
"

uso de futuro sin will/goingTo usando adverbios sin o con modales;"
A.1.Yo trabajo mañana
  =I work tomorrow.
B.2.Usando advervio y verbos modales
  =I can work tomorrow.
"

uso de futuro sin will/goingTo con Futuro [presente continuo]=[presente progresivo];"
A.1.We are going to aat tonight
  = [Cenaremos esta noche/vamos a cenar esta noche]
B.2.[Vengo mas tarde/vendre mas tarde]
  = I´m caming later
"

uso de futuro sin will/goingTo, con Futuro en pasado;"
A.1. Ella iba a invitarnos un helado ayer
  = She *was going to* invite us to eat ice cream yesterday
B.2. Ellos no le iban a dar a ella una oportunidad la proxima clase
    = They *weren`t* to give her a chance *next class*
C.3. Ibas a dejarme sola el domingo?
    =*were* you *going to* leave me alone on Sunday?
"

uso de futuro sin will/goingTo, con Futuro Proximo (estar a punto de/estar por)=(BE about);"
donde BE podemos usar; is/are si es presente, o was/were si es pasado
A.Tu estas por comenzar a practicar
  =You are about [to start|starting] to practice
B.Estamos a punto de hablar ingles
  =We are about to speak in english
"

uso de futuro sin will/goingTo, con Futuro proximo Be + Infinitivo;"
Debes practicar ahora mismo
  =You are to practice right now
Debemos de hablar ingles en clase
  =We are to speak english in class
"

(sin will/goingTo) El concierto comienza a las 9:00 pm mañana;"
The concert starts at 9:00pm tomorrow =
The concert is starting at 9:00pm tomorrow"

(sin will/goingTo) Ellos iban a conocerte al final de la reunión;"
They were going to meet you at the end of the meeting"

(sin will/goingTo) Mi madré horneará galletas para mi cumpleaños;"
My mother *[NO|will bake] is baking* cookies for my birthday"

(sin will/goingTo) Luis está a punto de hacer algunas preguntas a su maestra;"
Luis is about to *(NO|make) ask* some questions to his teacher"

(sin will/goingTo) Mis niños deben dormir en 30 minutos;"
My children are to sleep in 30 minutes"

(sin will/goingTo) Yo iba a comprar algunas cosas en el supermercado esta noche, pero las compraré mañana;"
I was going to buy some things at the supermarket tonight, but I *(Falto)am* *(NO)will* buying them tomorrow"

(sin will/goingTo) Debes dibujar un círculo rojo pero no debes colorearlo;"
You are *to draw* a red circle but you aren`t to color it"

(sin will/goingTo) Esos hombres no iban a robarme, ellos iban a ayudarme;"
Those men weren`t going to steal me, they were going to help me"

(sin will/goingTo) Ustedes deben entregar el examen en 1 hora. No recibo después;"
You are to hand-in the exam in 1 hour. I´m not receiving later"

(sin will/goingTo) ¿Van a volar a Londres este fin de semana que viene?;"
Are you flying to London this coming weekend?"

(sin will/goingTo) Frank no va a llevar a mi amiga al aeropuerto porque ella irá en taxi;"
Frank is not taking my friend to the airport because she is going by taxi"

(sin will/goingTo) ¿Estabas por terminar la comida?;"
were you about to finish your meal?"

(sin will/goingTo) Nosotros no nos vamos aún. Íbamos a escucharte un rato más;"
We aren`t leaving yet, we were going to listen to you a while more"

(sin will/goingTo) Milena debería entrenar mañana. Yo iba a ir con ella al gimnasio, pero vamos a ir juntas la próxima semana;"
Milena should train tomorrow, i was going to go with her to the gym, but we are going together next week"

(sin will/goingTo) Mi hija no canta esta noche pero ella cantará mañana en la tarde. Voy con mi esposa y algunos amigos;"
My daughter doesn´t sing tonight, but she´s singing tomorrow afternoon. I´m going with my wife and some friends"

(sin will/goingTo) Ibamos a enviarte el dinero el próximo lunes, pero vamos a enviártelo mañana en la mañana;"
We were going to send you the money next monday, but we are sending it to you tomorrow morning"
```

### 60. Preposiciones de tiempo IN - ON - AT - Cómo y cuándo usarlas

```r
Cuando hablamos de tiempo cuando usamos *IN*;"
Dentro de un periodo: de tiempo, Meses, años, estciones.
Partes del dia: morning, afternoon, evening"

Cuando hablamos de tiempo cuando usamos *ON*;"
Dias especificos, fechas largas, Dias, Dias especiales"

Cuando hablamos de tiempo cuando usamos *AT*;"
Momentos exactos, horas/eventos especificos, at night, noon, midday, midnight"

¿cuales son las prepociones de lugares?;"
Es cuando usamos 'en'=in, 'sobre'=on, 'por'=at para referirnos a un lugar
"

-we will be with you __ your birthday party -we will be with you __ your birthday;"
On (se esta hablando de un dia), At (se esta hablando de la fista (mas preciso))"

I´m going to do it __ the afternoon;"
in"

She will meet her husband __ 6:30pm;"
At"

the meeting will be __ June 4th;"
on"

I´m leaving my house __ two hours;"
in"

-See you tomorrow __ night -See you tomorrow __ the evening;"
at(creo por uso comun/tradicion), in(es parte del dia)"

the pub  is closing __ midnight __ Mondays;"
at(es preciso), on"

Natalia and I are going to  the beach __ summer;"
in"

__ Sunday __ 9 o´clock, I will be __ my house;"
On, at, in"
```

### Estas son las 5 formas de usar el ING o el GERUNDIO

```r
#Que son las preposiciones, define preposiciones
Que es una preposicion, uso de preposiciones, lista de proposiciones;"
Que es una preposicion
.  -Es una palabra que introduce una intecion o proposito, que puede ser
.   un lugar, tiempo, momento, proposito, etc. O tambien

uso de preposiciones, ejemplos
.  -lugar: voy *a* Madrid.
.  -tiempo: Trabaje *por* 5 horas.
.  -momento: El evento se celebró *en* el mes de diciembre.
.  -proposito: Esas lágrimas son *de* felicidad.

lista de proposiciones
.  -from = de, desde, (lugar, recorrido)
.  -of,= de (propiedad)
.  -For = Para, por (razon, modo)
.  -look for = Buscar
.  -look at = mirar
.  -About = sobre, acerca de
.  -after = despues, despues de
.  -before = antes, antes de
.  -in = dentro de
.  -on = encima de
.  -at = cerca de
.  -to = a (se;alamiento, direccion)
.  -by = por (identificador)
.  -with = con
.  -without = sin
.  -used to = solia,
.  -object to = oponerse a
.  -Etc.
"

Mensona las 5 formas de usar el ing y un ejemplo de cada uno;"
1.Progresivo continuo
  -The man was digging with his shovel
  -Para la terminacion ando/endo

2.Sujeto u objeto
  -Cocinar es su pasion = Cooking is his passion
  -Su pasion es cocinar = His passion is cooking/to cook
  -Algo asi como abjetivisar un verbo
  -[to cook]: Igual se puede pero no siempre se puede, por lo que se
   recomiendael cooking

3.Despues de preposicion
.  -Esta llave es para abrir la puerta = This key is for opening the door
.  -Tambien se puede con `This key is TO OPEN the door`


4.Adjetivo (como es algo / como estan las cosas)
.  -Fue un juego desafiante=It was a challenging game
.  -¿como es el juego? =defafiante, por lo tanto ese es nuestro adjetivo

5.Despues de algunos verbos.
.  -Despues de determinados verbos debemos usr el to o el gerundio.
.  -Verbos como: Go, like, start, love, avoid, keep, finish, stop.
.  -Me gusta comer pizza=I like to eat pizza
"

#---
Viajar es lo que amo;"
Traveling is what I love
"

Lo que amo es viajar;"
What I love is traveling
"

#---
ire despues de revizar mi email;"
I'll go after *X(check) O(hecking)* my email

X(check): Esta mal porque despues de una prepocicion en este caso after,
debemos de usar el gerundio.
"

Estoy acostumbrado a ir alla;"
I'm used to *going* there

usar ´go´ estaria mal porque al usar una prepocicion luego va un gerundio.
"

Ella siempre lee antes de dormir;"
She always read before sleeping

Usar ´sleep estaria mal porque al usar una prepocicion luego va un gerundio.
"

#---
Ella esta en un lugar interesante;"
She is in an interesting place

*interest place*: Estaria mal por que interest es un adjetivo
"

Esta no es una cuerda de saltar;"
This isn't a jumping rope

*jump rope*: Estaria mal por que jump es un Adjetivo
"

#---

Nos encanta ir al cine;"
I love going to the cinema / I love to go to the cinema

Las dos son corretas
"

No pares de practicar;"
Don't stop practicing = Don't stop to practice

Lsa 2 son correctas
"

Despues de que verbos hay que usar el gerundio = lista de verbos que despues usan gerundio;"
Consider   , Suggest  , Enjoy
Keep       , Mantener , Quit
Appreciate , Discuss  , Mind
Mention    , Finish   , Stop
Go         , Like     , Dislike
Love       , Start    , Begin
Postpone   , Delay    , Hate
Admit      , Avoid
"
```

### Gerundio VS Infintivo – ¿Cómo usarlos? Diferencias y ejemplos detallados

<!--  Gerundios VS Infintivos VS Gerundios, cuando usar Gerundios o Infintivos
cuando usar Infintivos o Gerundios.
 -->

[Fuente](https://www.youtube.com/watch?v=l0sK51yvaGM)

```r

```

### Cómo utilizar preposiciones en preguntas

```r
from where are you?=¿de donde eres? (corrige y explica);"
where are you from?

Porque: aqui se trato de traducir el el mismo orden que en el español,
ya que from=de, pero el orden en español e ingles cambia, y en este caso
las preposiciones suelen usarse al final.
"

¿Con quien esta viviendo Maria?;"
Who is Maria living with? = Who's Maria living with?
_
Incorrecto: With who is Maria living?: porque en las preguntas las
prepociones van al final
"

¿Sobre que esta hablando usted?;"
What are you talking about?
_
Incorrecto: About what are you talking?:
Porque: en las preguntas las prepociones van al final
"

¿De que estas tu asustado?;"
What are you afraid of?

Incorrecto: Of what are you scaring?:
Porque: En las preguntas las prepociones van al final
"

¿Para qué es esto?;"
What is this for?

Incorrecto: For what is this?
Porque: En las preguntas las prepociones van al final
"

¿Con quien vas?;"
Who do you go with?

Incorrecto: With who are you going?
Porque: En las preguntas las prepociones van al final
"

¿En que semestre estas?;"
What semester are you in?

Incorrecto: In which semester are you?
Porque: En las preguntas las prepociones van al final
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### SUBJECT VERB AGREEMENT

Fuente: [pacho8a.com](https://www.pacho8a.com/ingl%C3%A9s/subject-verb-agreement/)

```r
Milk (is/are) produced by some animals;"
Milk IS produced by some animals

(leche es un no contable)
Igual que en español
"

Milk and water (is/are) liquid;"
Milk and water ARE liquid

Igual que en español
"

Rice and chiken (is/are) my favorite plate;"
Rice and chiken IS my favorite plate

Estamos hablando de UN todo, que es el plato, podriamos
usar ARE si hablaramos de platos.

Igual que en español
"

Ten miles (is/are) too far for me to walk;"
IS

Cuando hablamos de unidades como distancia, tiempo, dinero, peso,
debemos de usar IS.

En este caso estamos hablando de '10 millas' que corresponde
a UNA distancia.
"

Thirty pounds of sugar (were/was) enough;"
WAS

Porque: Estamos hablando de libras como UNA medida.

Igual que en español
"

Many of my friends (seem/seems) to enjoy the class;"
SEEM
"

So of the money I had (is/are) lost;"
IS

Dinero es un no contable, si hablaramos de dos o mas, billetes o monedas
entonces si seria ARE porque eso si es contable/plural
"

Somo of the animals (do/does) not like fruits;"
DO

podemos decir 'los animales', pero no 'lo animales'
"

Geo, as well as his friends, (studies/study) english;"
Studies,

Geo, al igual que sus amigos, (estudia/estudian) ingles.

Estamos agregando informacion, no conformando un grupo, por lo
que quien estudia va referente solo a Geo
"

Geo and Mike, together with his friend Ale, (was/were) practicing english;"
WERE

Ale solo es info adicional, estamos hablando de Geo y Mike
"


----

A pair of shoes (is/are) what he wants;"
IS, WANTS,

IS porque hablamos de UN par
"

Not only you, but also your wife (watch/waches) TV;"
WATCHES

En este caso es por que wife es el sujeto mas sercano, por
lo que es 3raPerSingu
"

Everyone (deserve/deserves) a second chance;"
DESERVES

Estamos hablando de UN conjunto
ES: 'Todo el mundo merece'
NO: 'Todo el mundo merecen'
"

Monday or fridays (is/are) the same for me;"
ARE

En este caso el sujeto mas servcano (viernes) es plural
"

Three quartes of a potto (is/are) water;"
IIS

Igual que en español: '3/4 de papa ES agua', no refiere
a los 3/4, refiere a la papa
"

Two plus four (is/are) six;"
IS,

Igual que en español: '2+4 ES 6'
"

```

<!--

#########################################

-->

----

<!--

#########################################

-->

### EXAMEN Ingles basico

TODO: PENDIENTE: hacer examen final de ingles basico

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

#############################################################################################################
#############################################################################################################

-->

## B1. Pre-Intermedio

### 1. Tiempo Presente Perfecto, uso de HAVE y HAS

```r
Que es el presente perfecto;"
Es una actividad que empezo en el pasado, y aun no ha terminado en el
presente
"

Have;"
Tener, haber
"

Diferencia entre have, has y had;"
- Have es para los queno NO son singular tercera persona  (Yo he vaciado la sal = I have emptied the salt)
- Has es en presente perfecto (El ha soltado el lazo = He has released the noose)
- had es en pasado perfecto (Ellos habian peleado cuando ocurrio = They had fought when it happened)
"

Que es el pasado participio de los verbos;"
En español es el equivalente a la terminacion ´-ado, -ido, -to, -so, -cho

por ej. caminado, desaparecido, tenido, soñado. escrito, etc.
"

Yo he ayudado, Ella ha ocultado, Eso ha desaparecido;"
I have helped, She has hidden, It has faded,
"

Nosotros hemos comido, Ellos han corrido;"
We have eaten, they have run
"

El ha solicitado, Tu has enviado;"
He has sought, You have sent
"

#reparticion de have, distrubucion de have, have y have
Have/Has, Como se distribuyen los Pronombres Personales;"
Se separan igual que el presente simple, es decir, se usa has para
he, she, it y para los demas have

- Have: I, You, We, They
- Has: He, She, It
"

Estructura de presente perfecto con have y ejemplos (+,-,?);"
Subj + Have/Has + Verb Pasado Participio + Comp.

- Hemos llamado a la policia
- (+) We have called the police
- (-) We haven`t called the police
- (?) Have we called the police?
"

Diferencia entre presente simple y presente perfecto;"
Diferencias contextuales
.  Presente simple: No tiene un inicio ni un fin ej 'Yo canto'
.  Precente perfecto: Tiene un fin definido y cercano o que apenas ocurrio
.    ej. 'yo he cantado'

Diferencias Estruturales
.  Presente simple: usa el auxiliar do/does, ej. 
.    (+)I prefer juice
.    (-)I don't want juice
.    (?)Do you want juice?
.  Presente perfecto: usa el auxiliar have/has
.    (+)I have run
.    (-)I haven't run
.    (?)Have I run?
"

(+,-,?) Frank ha tenido un gran dia;"
(+) Frank has had a great day
(-) Frank hasn´t had a great day
(?) has Frank had a great day?
"

Cual es la contraccion de have, por ej. `I have`;"
I´ve, We´ve
"

Erick y Kelly se han enamorado;"
Erick and Kelly have fallen in love
"

El señor Smith no ha estado aqui desde esta mañana;"
Mr. Smith hasn´t been here since this morning
"

¿Has entrenado durante 4 horas hoy?;"
have you trained for four hours today?
"

You have never had an outlet;"
nunca has tenido una salida
"

¿No ha comido ella chocolate desde la semana pasada?;"
Hasn´t she eaten chocolate since last week?
"

(+,-,?)Ted y Sally han vivido en los Ángeles desde 1995;"
(+)Ted and Sally Have lived in Los Angeles since 1995
(-)Ted and Sally haven´t lived...
(?)Have Ted and Sally lived...?
"

(+,-,?)El señor y la señora Perez han decidido mudarse a su nueva casa en Boston;"
(+)Mr. and Mrs. Perez have decided to move to their new house in Boston
(-)Mr. and Mrs. Perez haven´t decided ...?
(?)Have Mr. and Mrs. Perez decided...?
"

(+,-,?)El teléfono ha sonado muchas veces esta mañana;"
(+)The phone has *sounded* many times this morning
(-)The phone hasn´t *sounded*...
(?)has The phone sound...?
"

(+,-,?)Karol le ha escrito a su madre un par de veces este mes;"
(+)Karol has written to her mother a couple of times this month
(-)Karol hasn´t written...
(?)has Karol written...?
"

(+,-,?)Mis empleados han trabajado duro toda la semana;"
(+)My *(Escritura)employees* have *(NO work)worked* hard all week
(-)My employees haven´t worked...
(?)have My employees worked...?
"

(+,-,?)Emily ha estado en su habitación durante cinco horas;"
(+)Emily has been in her room for five hours
(-)Emily hasn´t been...
(?)has Emily been..?
"

(+,-,?)Los niños han terminado su desayuno;"
(+)The *(NO childrens)children* have finished their breakfast
(-)The children haven´t finished...
(?)have The children finished...?
"

(+,-,?)Los clientes se han quejado acerca del mal servicio de la tienda;"
(+)The clients have complained about the bad service of the store
(-)The clients haven´t complained..
(?)Have The customers complained...?
"

9. Ted and Sally have lived in Los Angeles since 1995;"
Ted y sally han vivido en los angeles desde 1995
"

10. Our boss has sent us an e-mail, it reads that he has cancelled today´s meeting;"
Nuestro jefe *nos* ha enviado un e-mail, dice que el ha cancelado la reunion de hoy
"

11. I haven´t washed my clothes because I haven´t had time;"
Yo no he lavado mi ropa porque yo no he tenido tiempo
"

12. Have you decided where to go?, You haven´t told me anything;"
¿Tu has decidido a donde ir?, No me has dicho nada
"

13. My mother has not enjoyed her party because my father has drunk too much;"
Mi mama no ha disfrutado su fiesta porque mi papa ha bebido mucho
"

14. John has fallen in love with his new neighbor, but he hasn´t talked to her;"
John se ha enamorado de su nuevo vesina, pero el no ha hablado con ella
"

15. Have you learned this topic easily?;"
¿Tu has aprendido este tema facilmente?
"
```

### 2. Presente Perfecto, Uso de auxiliares HAVE y HAS, Diferencia entre SINCE y FOR

```r
¿Por cuanto tiempo Michael y Jenny han sido veterinarios?;"
-How long have Michael and Jenny been vets?
"

-Michael y Jenny han sido veterinarios por muchos años;"
-Michael y Jenny has been vets for many years.
"

Michael or Jenny no han vivido en las aferas de la ciudad desde marzo;"
Michael or Jenny hasn´t lived in the city outskirts since March
"

¿Michael o Jenny desde cuando no han vivido en las afueras de la ciudad?;"
Since when hasn´t Michael or Jenny lived in the city outskirts?
"

¿Hace cuanto que uno de los jugadores no ha ganado una medalla?;"
How long hasn´t one of the racers won a medal?
"

Uno de los corredores no ha ganado aun una medalla desde que el esta compitiendo;"
One of the racers hasn´t won a medal yet since he is racing
"

¿Hace cuanto tiempo tu has cantado esa cancion?;"
How long have you sung that song?
"

Tu ya has cantado esa cancion desde hace mucho tiempo;"
You have already sung that song *(NO since)for* a long time ago
"

Gerson nunca a estado en la capital desde que el vive en Guatemala;"
Gerson never has been *(NO in por que se esta hablando de la visita)to* the capital since he live in Guatemala
"

Nosotros nunca hemos estado en Nueva York desde hace un mes;"
We have never been *(NO to porque se esta hablando de permanencia/periodo)in* New York dince one month
"

# DUDA, PENDIENTE, TODO
(Q y R en Presente perfecto)Her mother - be on a diet - 6 months;"
- How long has her mother been on a diet?
- She *(NO has|¿¿¿???)is* been on a diet for 6 month
"

(Q y R en Presente perfecto) You and your family - live here - 1996;"
Q.Since when have you and your family lived here?
R.We have lived her since 1996
"

(Q y R en Presente perfecto)Your parents - be married - almost thirty years;"
Q.How long have your parents been married?
R.They have married for almost thirty years
"

(Q y R en Presente perfecto)You and George - Know each other - we were in college;"
Q.Since when have You and George known each other?
R.We have know us since collage
"

(Q y R en Presente perfecto)Bryan or Mike - Play chess today - about three hours;"
Q.Has Bryan or Mike played chess today?
R.Yes, they have played for about three hours
"

(Q y R en Presente perfecto)Kate - be to that school - she was 5 years old;"
Q.How long have Kate been to that school?
R.She has been there for five years old
"

(Q y R en Presente perfecto)Pacho - Teach English - approximately 10 years;"
Q.How long Pacho has been taught english?
R.He has taught english approximately 10 years
"

Their friends - Stay in Washington D.C - their father died;"
Q.Since when have their friends stayed in Washington D.C.?
R.They have stayed there since their father died
"

You - Be in London - the last 6 years;"
Q.How long have you been in London?
R.I have been in London for the last 6 years
"

#--------
"(Presente perfecto) Amy and Randy _1_ lived in New York _2_ 1998. Amy
works at a university in New York. She´s _3_ there _4_ three years. She´s
_5_ too busy and tired _6_ about five months. Randy is a construction worker.
He _7_ been in the construction business _8_ he graduated from high school.";"
1.Have, 2.since, 3.lived, 4.for, 5.been, 6.for, 7.has, 8.since,
"
#--------

#--------
"(Presente perfecto)It´s spring break at the university, so Amy _1_ not
_2_ to work _3_ a week now. She´d like to spend more time with Randy,
but he _4_ not _5_ a vacation _6_ their honeymoon. Amy and Randy have
_7_ about this _8_ the past few months. Amy has _9_ of traveling _10_
she was very young, but Randy isn´t interested in visiting new places";"
1.has, 2.gone, 3.since, 4.has, 5.on, 6.since, 7.taught, 8.for, 9.wanted, 10.since
"
#--------
```

### 3. Use of BECAUSE, AS, SINCE y FOR to give arguments

```r
#-------------
"Cuales son las 3 conjunciones para conectar dos ideas de un argumento,
por ej. ´El sol no gira al rededor de la tierra `porque` la tierra
gira al rededor del sol´";"

- because (porque),

- as (ya que, lo que, aunque, como lo(a)

- since (pues, comoquiera que),

- for (debido a).

NOTA: Se pueden usar indiscriminadamente para conjugar argumentos.
"
#-------------

China no tiene una poblacion pequeña `ya que` tiene una poblacion grande;"
China doesn´t have a small population `as` it has a big population
"

Los ratones no corren tras los gatos, `debido a que` los gatos comen ratones;"
Mice don´t run after cats, `for` cats does eats mice
"

Los elefantes no tienen orejas pequeñas, `pues` las tienen grandes;"
Elephants don´t have small ears, `since` they have big ears
"

El agua no hierve a los 90 grados, `porque` hierve a los 100 grados;"
Water doesn`t boil at 90 celsius, `because` it boils at one hundred celsius
"

El son no sale del oeste ´porque` sale en el oeste;"
The sun doesn't *rise* in the west ´bacause´ it *rises* in the east
"

El agua no se conjela a los 5°C _debido a_ que se conjela a los 0°C;"
Water _doesn´t_ freeze at 5°C _for it freezes at_ 0°C
"
```

### 3.A. Stative Verbs, Definición y Ejemplos, Incluye listado

```r
Que es un stative verbs y ejemplos;"
Son verbos que no involucran una accion en si, mejor dicho, involucran
un estado o situacion es/existe pero no se esta haciendo o sucediendo.

Ejemplos
- Yo te amo (seria raro/incorrecto decir yo te estoy amando)
  - I love you (it would be weird/incorrect to say, ´i´m loving you´)
- Correct: Your friend hate me // Incorrect: Your friends are hating me.
"

Cuales son los diferentes estados que maneja los Stative Verbs;"
Emocionales, Mentales, Percepcion, Posecion
"

ejemplos Emotional Stative Verb y frases incorrecta correcta;"
- Love, Like, Dislike, Care, hate, envy

- Yo te amo
  - Incorrecto: I'm loving you
  - Correcto: I love you
  - Alternativa: I´m starting to love you
- Tus amigos me odia
  - Inco: Your friends are hating me
  - Corr: Your friends hate me
  - Alte: Your friends starting to hate me
"

Ejemplos de Mental Stative Verb y frases incorrecta correcta;"
- Know, suppose, Realize, Believe, Understand, Think

- Nosotros lo conocemos
  - Incorrecto: we are knowing him
  - Correcto: We know him
  - Alternativa: We are getting to know him
- Eduardo piensa que el esta mal
  - Inco: Eduardo is thinking that he is wrong
  - Corr: Eduardo thinks that he is wrong
  - Alte: Eduardo is getting think that he is wrong
"

Ejemplos de Perception Stative Verb y frases incorrecta correcta;"
- taste, hear, see, smell, feel

- La sopa sabe deliciosa
  - Inco: The soup is tasting delicious
  - Corr: The soup tastes delicious
  - Alte: The soup is becoming delicius
- Te veo en la puerta
  - Inco: I´m seeing you at the door
  - Corr: I see you at the door
  - Alte: I´m seeing you at the door at the door tomorrow
"

Ejemplos de Possession Stative Verb y frases incorrecta correcta;"
- Possess, have, own, belong

- Monica tiene mucho dinero
  - Inco: Monica is having a lot of money
  - Corr: Monica has a lot of money
  - Alte: Monica is getting a lot of meney
- Tu adquiriste un nuevo carro
  - Inco: You are owning a new car
  - Corr: You own a new car
  - Alte: You are getting a new car
"
```

### 4. Relative Clauses and Relative Pronouns

Fuente: [pacho8a.com](https://www.pacho8a.com/ingl%C3%A9s/relative-clauses-1/)

```r
Relative Clauses: Relative Pronouns: ¿ejemplo?, ¿Para que son? ¿Cuales son?;"
¿Ejemplo?, 
  - Repeticion: Ana es una mestra, ella nacio en argentina.
  - Acortada: Ana es una maestra que nacio en argentina.

¿Para que son?, Como su nombre lo dice relacionan clausulas y oraciones,
y tambien para no hacer tantas pausas y repeticiones.

¿Cuales son?: Who, Whose, Whom, Where, Where
"

Partes de una sentencia(sentence) y/o una oracion(clause);"
Tienen un sujeto, un verbo y un complemento,

ej. Pacho es un maestro
"

(Relative clauses) who: ¿se usa en?, ¿significado?, ejemplo;"
se usa en: personas,

Segnifica: Quien/Quienes

Ejemplo: She is my dauther 'who' is 4 years old
"

Mi hija, `quien` tiene cuatro años de edad, esta en la escuela;"
My dauther, who is four years old, is in the school.
"

(Relative clauses) which: se usa en, significado, ejemplo;"
Se usa en: Lugares, animales, cosas

Segnifica: el/la cual, las/los cuales

Ejemplo: This is my dog spike `which` jumps very high
"

Este es mi perro spike, el cual esta en el veterinario, salta muy alto;"
This is my dog spike, which is at the vet, jumps very high
"

(Relative clauses) whom: se usa en, significado, ejemplo;"
Se usa en: Personas

Segnifica: a quien, a quienes

Ejemplo: They are our new friends, whom we met in Paris.

Dif con who: 
  who=Quien/Quienes
"

Nuestros amigos a quienes conocimos en paris, estan en inglaterra;"
Our friends whom we met in Paris, are in England
"

(Relative clauses) whose: se usa en, significado, ejemplo;"
Se usa en: Personas

Segnifica: de quien, cuyo, cuyos

Ejemplo: He is Michael whose(cuyo) son is Jhon
"

Paco, cuyo hijo es Juan, esta aqui;"
Paco, whose son is Juan, is here
"

(Relative clauses) that: se usa en, significado, ejemplo;"
Se usa en: Personas, animales, cosas

Segnifica: que

Ejemplo: Luis is at the hotel that is near his house.
"

1. El pez es un animal que vive en el agua;"
The fish is an animal that/which *lives* in the *(NO wather)water*
"

2. El hombre que(no usar that) vino ayer es mi esposo;"
The man who *(NO come)came* yesterday is my husband
"

3. Mi hermana, quien estaba leyendo el libro, es muy inteligente;"
My sister, who(NO whom por que no es a quien) was reading the book, is very *(NO inteligen)intelligent*
"

4. Jany es la chica quien canta la canción;"
Jany is the girl *(NO whose=cuya)who* sings the song
"

5. Los niños, a quien viste ayer, son mis primos;"
The *(No childrens)children*, whom you saw yesterday, are my *(NO cousinGs)cousins*
"

#===============

(Usa Relative.Clauses/Pronouns)Fran is an english teacher, he was born in medellin in 1983;"
Fran is an english teacher `who` nacio en medellin.
"

(Usa Relative.Clauses/Pronouns) He has two daugthers, Isabella and Emily. Isabella is 5 years old and Emily is 3 years old;"
He has two daugthers, Isabella who is 5 years old, and Emily who is 3 years old.
"

(Usa Relative.Clauses/Pronouns)He lives with a special woman, her name is Pamela;"
He lives with a special woman whose name is pamela
"

(Usa Relative.Clauses/Pronouns)Fran has a youtube channel, Fran is also called Pacho, he shares english classes;"
Fran has a youtube chanel where is called Pacho, and shares english classes.
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 4. Presente Perfecto vs Pasado Simple, aprende a diferenciarlos

```r
Diferencia entre pasado simple y pasado perfecto;"
El pasado simple es que algo ocurrio y termino, Se le asocia con 'when'

mientras que el pasado perfecto algo ocurrio pero aun no termina, se
asocia con 'since'
"

Nancy [went/has gone] to a party last week;"
went"

Peter [was/has been] here since this morning;"
has been"

We [called/have called] you 5 times so far;"
have called"

Ana [didn´t write/haven`t written] that letter;"
both"

We [lived/have lived] here almost all our lives;"
have lived, (podria quedar 'lived' si estuvieran de visita en el lugar
donde estan, pero creo se entiende mejor que 'here' nos evoca al
presente perfecto)"

We [moved/have moded] when we [got/have got] married;"
moved, got"

I [missed/have missed] you when you were there;"
missed"

[Did/Has] your father enjoy the movie;"
Did"

Christian [arrived/has arrived] three days ago;"
arrived"

I only [slept/have slept] for two hours last night;"
sleep"

She [didn´t fly/hasn´t flown] yet, because she [didn´t find/hasn´t found] her passport;"
hasn`t flown, hasn´t found"

[Did/Has] your father enjoyed the movie;"
Has"

# ------

So far this week, I [have]__ two tests and a quiz (Present Perfect vs Simple Past);"
have had"

John is an artist. He [draw]__ many beautiful pictures in his lifetime (Present Perfect vs Simple Past);"
has draw
"

Last week he [draw]__ a beautiful mountain scene (Present Perfect vs Simple Past);"
drew"

Clark and Mike [arrive]__ here three days ago (Present Perfect vs Simple Past);"
arrived"

Robert and Karla [live]__ here since May 1st (Present Perfect vs Simple Past);"
have lived"

Peter´s flat;"
Piso de Peter"

Emily [not, go]__ to study English at Peter´s flat last week (Present Perfect vs Simple Past);"
didn`t not"

Please, try not to be absent from class again for the rest of the term. You (already, miss)__ too many classes (Present Perfect vs Simple Past);"
have alrady missed"

You (miss) two classes just last week (Present Perfect vs Simple Past);"
missed"

Since this morning, he (call)__ here four times trying to reach you (Present Perfect vs Simple Past);"
has called"

He (call)__ at 10:20, 10:40, 11:00, and 11:30 (Present Perfect vs Simple Past);"
Called"

__ you already filled my tank? I (wait)__ for almost 15 minutes(Present Perfect vs Simple Past);"
have, have waited"

Elizabeth (never, see)__ snow in her entire lifetime(Present Perfect vs Simple Past);"
has never seen"

I (see)__ snow for the first time in my life last winter(Present Perfect vs Simple Past);"
saw"

I (start)__ trying to reach him three days ago(Present Perfect vs Simple Past);"
stared"

Since then, I (send)__ him 5 e-mails and I (phone)__ him four times(Present Perfect vs Simple Past);"
have sent, have phoned"

I also (call)__ him at least 3 times yesterday(Present Perfect vs Simple Past);"
called"
```

### 5. Uso de BE ABLE TO en el Presente Perfecto, CAN y COULD

```r
(Corrige) I will can play soccer;"
I will able to play soccer
"

Cual es la diferencia entre usar puedo en pasado/presente y pas.part/futuro y ejemplo;"
Para pasado/presente podemos hacer uso de 'could/can', que es puedo, pero
para pas.part/futuro solo podemos usar 'be able to', que es ser capas de

Yo pude: I could play soccer
Yo puedo: I can...
Yo podre: I will able to... / I´m going to be able to...
Yo he podido: I been able to...
"

(Corrije) They have could climb the Everest;"
They have been able too...
"

Ellos han podido escalar el Everest;"
They have been able to climb the Everest
"

Ella a podido ahorrar dinero;"
She has been able to save money
"

Ella no ha podido encontrar sus amigos;"
She hasn´t been able to find her friends
"

Ha podido Mau vender su carro;"
Has Mau been able to sell his house
"

Ellos han podido mantener el equilibro;"
Thay have been able to keep the balance
"

Ese hombre no ha sido capaz de barrer las escaleras por el viento fuerte;"
That man hasn´t been able to sweep the stairs for the strong wind
"

1. El carnicero no ha podido afilar el cuchillo;"
The butcher hasn´t been able to sharpen the knife
"

2. ¿Has podido solucionar el problema?;"
have you been able to solve the problem?
"

3. Los estudiantes han podido resolver los ejercicios fácilmente;"
The students have been able to solve the exercises easily
"

4. Quiero editar algunas fotos con mi celular pero no he podido descargar una buena aplicación para hacerlo;"
I *(NO have wanted)want to* edit some photos with my cell phone. but I *(NO couldn´t)haven´t been able to* download a good app to do it
"

5. Mi hermana no ha sido capaz de cambiar la contraseña de su correo electrónico;"
My sister hasn´t been able to change the password of her e-mail
"

6. ¿No han podido ellos memorizar todos los verbos irregulares? No, ellos no han podido;"
Haven´t they been able to memorize all the irregular verbs?, No, they haven't
"

7. Hasta ahora, no he podido pagar todas mis cuentas pero se que lo haré pronto;"
So far, I *(Falto)haven´t* been *(Sobra)not* able to pay all my bills, but I know I´ll do it soon
"

8. Nuestra hija no ha sido capaz de cepillar sus dientes muy bien, ella apenas tiene 3 años;"
Our *(W dauther)daughter* hasn´t been able to brush her teeths very well, she is just 3 years old
"

9. ¿Ha podido George conseguir comida enlatada? No, no ha podido. El ha estado en 3 supermercados hasta ahora;"
Has George been able to *get canned* food? No, He *hasn´t. He`s been* to 3 supermarkets so far
"

10. ¿Cuántas hojas has podido imprimir?, He podido imprimir solamente 5 hojas;"
How many pages have you been able to print?, I *(Falto)have* been able to print only 5 pages
"

1. How much money have you been able to save throughout your life?;"
¿Cuánto dinero has podido ahorrar en toda tu vida?
"

2. I haven`t been able to read the newspaper because I´ve been really busy;"
No he sido capas de leer el periodico, porque he estado realmente ocupado
"

3. Have you been able to break the safety glass? No, I haven´t: but I´ll be able to break it with this hammer;"
¿Has podido romper el vidrio de seguridad? No, No he podido, pero podré romperlo con este martillo
"
```

### 6. Dictado en Presente Perfecto

```r
# TODO: YA LO TOME 1 VEZ, FALTAN OTRAS DOS
Dictado en Presente Perfecto;"
https://www.youtube.com/watch?v=1PZcOUYl5M4
"
```

### present perfect vs present perfect continuos/progresive

```r
# uso de presente perfecto
Para que sirve el presente perfecto y ej. en español;"
Algo que a ocurrido en el momento, y no se sabe si se va a continuar.
En otras palabras, Acaba de ocurrir y no creo que siga ocurriendo.
Ej: Hemos estudiado ingles.
"

Para que sirve el presente perfecto continuo/progresivo y ej. en español;"
Igual que el presente perfecto, pero es algo que ha estado ocurriendo.
y tiene una alta probabilidad de continuar. En otras palabras, esta
ocurriendo y lo mas seguro seguira ocurriendo
Ej: Hemos estado estudiando ingles.
"

Estructura de presente perfecto con have y ej.;"
Estructura:  Subj + Has/Have + V.Pasado.Participio + Complement
Ejemplo   :  We   +   have   +        studied      + english with ...
"

(+,-,?)Yo he asado la carne;"
(+)I have broiled the meat.
(-)I haven`t boiled the meat.
(?)have I boiled the meat.
(?-)haven`t I boiled the meat.
"

(+,-,?)Ellos han jurado que acabaran el proyecto;"
(+)They've sworn that they will finish the project
(-)They haven't sworn ...
(?)Haven they sworn ...
"

-¿has tostado el pan? -No, no lo he tostado;"
Have you browned the bread? - No, I haven`t
"

-El ha calmado la situacion?. -Si, el los ha calmado;"
Has he calmed the situation?. -Yes, he has calmed them.
"

Estructura de presente perfecto y ej.;"
Estructura:  Subj + Has/Have + been +  V.ing   + Complement
Ej.:         We   +   have   + been + studying + english with Pacho
"

(+,-,?)Hemos estado estudiando ingles;"
(+)We've been studying english
(-)We haven't studying english
(?)Have we been studying?
"

(+,-,?)Ella ha estado llenando la picina;"
(+)She has been stuffing the pool
(-)She hasn`t been stuffing the pool
(?)Has she been stuffing the pool?
"

-El ha estado confiando en ellos? -No, el no lo ha confiado, -Si, el lo ha confiado;"
-He has been trusting on they? -No, he hasn´t trust, -Yes, he has been trust
"

# diferencia entre el pasado participio y presente perfecto continuo
El ha sido estudiante desde el año pasado, y ha estado estudiando quimica con David;"
(P.P.)he has been student since last year, and (P.P.C.) he has been studying chemistry with David.
Al parcer el P.P. despues del been sigue sujeto y en P.P.C. sigue el verbo en gerundio.
"

Mi hermano ha estado durmiendo desde que llegue;"
My brother has been sleeping since I *(Va en pasado|arrive)arrived*
"

Hasta ahora, he aprendido muchos verbos y he estado mejorando mi nivel de ingles;"
So far, I've learned many verbs and I've been *(improve)improved* my english level
"

¿Has podido ver los partidos de futbol?;"
Have you *been* able to watch the soccer games?
"

Si, he estado disfrutandolos con mi familia;"
Yes, I've been enjoing them with my family
"

1. Ellos se han mudado a un nuevo apartamento en Orlando;"
They have moved to a new apartment in Orlando"

2. Peter y Mike han estado cantando Karaoke durante casi 2 horas;"
Peter and Mike Have been singing Karaoke for almost two hours"

3. Hemos tenido mucho trabajo estos días, incluso hoy hemos estado trabajando desde las 4 am.;"
We´ve *had* a lot of work *(this es para singular|this)these* days, even today we´ve been working since 4am"

4. ¿Has estado cocinando para tu familia desde la semana pasada? No. He estado cocinando desde ayer;"
have you been cooking for all *(you)your* family since *(el since pude incluir el/la|the)* last week? No, I`ve been cooking since yesterday"

5. Sally ha estado trabajando para esa compañía desde 1995 y ha visitado muchos paises en reuniones de negocios;"
Sally has been working for that company since 1995 and she has visited many countries at bussiness meeting"

6. ¿Le has prestado el celular a tu sobrino? Él ha estado jugando con él toda la mañana;"
have you *(borrowed)lent* the cell phone to your *nephew*? He has been playing with it all morning"

7. Le hemos estado diciendo a todos los clientes que los artículos de promoción se han agotado;"
we've been saying to all the clients that the *promo* articles have *been* sold out"

8. El estudiante ha estado respondiendo el examen durante más de 2 horas y no ha podido terminar aún;"
The student has been answering the exam for more *than* 2 hours and he hasn't *been* able to *finish* yet"

9. He estado comprando la lotería desde hace dos semanas pero no he tenido nada de suerte;"
I´ve been buying the lottery *for* 2 weeks but I haven't had any luck
"

10. Ellos han perdido mucho dinero, sin embargo, desde el año pasado han estado viviendo mucho mejor;"
They've lost a lot of money; *however*, since last year they've been living much better"

I have liked horror movies since I was a child. Today, I´ve been watching my favorite movies;"
Me han gustado las peliculas de terror desde que era un niño. Hoy, He estado mirando mis peliculas favoritas"

My friend George has been working on his new book for months, but finally today, he has released it;"
Mi amigo Jorge ha estado trabajando  en su nuevo libro por meses, pero finalmente hoy, lo ha publicado."

Have you studied French? No, I haven´t. I´ve been studying English since I´m at the university;"
¿Has estudiado frances? No, He estado estudiando ingles desde que estoy en la universidad"
```

### 8. Uso de HOW LONG en Presente Perfecto Continuo o Progresivo

```r
Estructura de preguntas en Presente Perfecto Continuo o Progresivo con how long, y ejemplo;"
Estrucuta: How long + Has/have + Subject + Been + Verbo-ing + Complemento      ?
Ejemplo  : How long +   have   +   you   + been +  working  + for that company ?
Traduccio: ¿Cuanto tiempo has trabajado para esa compania?
Respuesta: He estado trabajando para esa compania por casi 10 años
Respuesta: I´ve been working for that company for almost 10 years
"

-¿Cuanto tiempo Edy ha estado hablando con su novia? -El ha estado hablando con su novia desde que ella lo llamo;"
-How long has Edy been talking with his girlfriend? -[He´s/He has] been talking with his gf since she called him
"

-¿Cuanto tiempo ha estado tu madre llendo al gimnasio? -No estoy seguro, tal vez 3 meses mas o menos;"
-How long has your mom been going to the gym? -*I`m* not sure, maybe 3 *or so*
"

-¿Cuanto tiempo has estado buscando un nuevo empleo? -No lo se, quizas hace solo un par de semanas;"
How log have you been loking for a new job? -I don´t know, maybe just *a couple* of weeks *ago*
"

Toma dictado del audio https://co.ivoox.com/es/player_ek_27091350_2_1.html;"
it´s been raining since last night, the ground is still wet, John has been sitting on the same park
bench for *hours*, his clothes are *soaked* and he´s been *coughing* all morning, a while ago someone
gave him a *bowl* of hot soup, it´s no *longer* warm, but Jonh has been *eating it* anyway.
how *did* he end *up* like this?, John a former *building superintendent* lost his job and his apartment
when he hurt his back, he has been living on the street since then, and he is not alone. John is just
one of the *possible* 7 million americans who have been making their homes in the streets, parks
and subway stations of our cities. the number of homeless men, women, and children has been
climbing *steadily* since *1980* and will continue to rise until the *government* takes action
"

Toma dictado del audio https://www.ivoox.com/lesson-8-questions-intermediate-level-audios-mp3_rf_27091407_1.html y responde segun el audio anterior;"
How long has it been raining?
It has been raining since last night

How long has John been sitting on the same park bench?
he has been sitting for hours

How long has John been coughing?
he has been coughing for all morning

how long has John been eating his soup?
He has been eating his soup *since someone gave it to him*

how long has John been living on the street?
He has been living on the street since he hurt his back.

how long has the number of homeless people been *increasing*?
It has been *increasing steadily* since 1980.
"
```

### Uso de Wh-Questions en Presente Perfecto Continuo o Progresivo

```r
(Infiere la pregunta, wh-question+PrePerCon o PPProg) She has been visiting this city every month;"
*How often* has she been visiting this city?
"

(Infiere la pregunta, wh-question+PrePerCon o PPProg) Something strage has been happening in my life;"
What *has* been happening *in* your life? (what es una subject questions asi que cuidado con *have you*)
"

(Infiere la pregunta, wh-question+PrePerCon o PPProg) Gabirel´s parents have been to venezuela 5 times;"
How many times have Gabirel´s parents been to venezuela?
"

(Infiere la pregunta, wh-question+PrePerCon o PPProg) Is Mike who has a presentation tomorrow and he´s been rehearsing all day long (who);"
why has mike been rehearding all day?
who has been rehearding for the presentation?
"

Redacta https://www.ivoox.com/lesson-9-granddad-intermediate-level-audios-mp3_rf_27214193_1.html;"
We’ve been hearing about Granddad since we arrived here in Amboseli Park. He is one of the last
“tuskers.” Two days ago, we finally saw him. His tusks are more than seven feet long. I have never
seen anything like them.

Granddad has been living here for more than 60 years. He’s experienced everything, and he’s
survived countless threats from human beings. Young men have been testing their courage against
him, and poachers have been hunting him for his ivory. His experience and courage has saved him
so far.

For the last two days, Granddad has been moving slowly through the tall grass. He has been eating
and resting. Luckily, it has been raining a lot this year, and even the biggest elephants have been
finding enough food and water.

Watching this elephant has been an incredible experience. I hope he is still here when we come
back next year!
"

-¿Hace cuánto que escuchas sobre el abuelo? -Hemos estado escuchado sobre el abuelo desde que llegamos aquí en Parque Amboseli;"
-How long have you been hearing about Granddad? -We’ve been hearing about Granddad since we arrived here in Amboseli Park
"

-¿Cuánto tiempo lleva viviendo allí el abuelo? -El abuelo ha estado viviendo aquí desde hace más de 60 años.;"
-How long has Granddad been living there? -Granddad has been living here for more than 60 years.
"

¿Cómo se ha estado moviendo el abuelo? -El abuelo se ha estado moviendo lentamente;"
How has Granddad been moving? -Granddad has been moving slowly.
"

Where has Granddad been moving slowly? -through the tall grass;"
¿Dónde se ha movido el abuelo lentamente? -a través de la hierba alta
"

¿Hace cuántos días el abuelo se ha estado moviendo lentamente? -Durante los últimos dos días;"
How many days ago has Granddad been moving slowly? -For the last two days
"

¿Quiénes han estado encontrado suficiente comida y agua? -los elefantes más grandes.;"
Who have been finding enough food and water? -the biggest elephants.
"

¿Qué ha sido una experiencia increíble? -Ver a este elefante.;"
What has been an incredible experience? -Watching this elephant.
"

¿Quién lo ha estado cazando? -los cazadores furtivos lo han estado buscando por su marfil;"
Who has been hunting him? -poachers have been hunting him for his ivory
"

¿Quiénes han estado probando su valentía? -Los jóvenes han estado probando su coraje contra él.;"
Who have been testing their courage? -The Young men have been testing their courage against him
"

¿Cuánto ha estado lloviendo este año? -ha estado lloviendo mucho este año;"
How much has it been raining this year? -it has been raining a lot this year
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Uso de There was con tiempos

### 10. Diferencia y uso de THERE HAS BEEN y THERE HAVE BEEN

```r
there has been / there have been, traduce y explica diferenca;"
Trad: ha habido

has: para singular o no contables

have: para plural
"

estrutura para ha habido;"
there has/have been + [cuantitative (opcional)] + sustantivo
"

version extendida de `there´s some` y `there´s been some` y traducion;"
#There´s some
  =There is some = hay algo de

#There´s been some
  =There has been some = ha habido algo de
"

(+,-,?,?-)ha habido mucho ruido en clase (=)Si lo ha habido;"
(+)There has been a lot of noise in class = there´s been a lot ...
(-)There hasn´t been a lot ...
(?)Has there been a lot ...?
(-?)hasn´t there been a lot...?
(=)Yes, there has | No, there hasn´t
"

(+,-,?,?-,=)Ha habido un par de ordenes ultimantente;"
(+)There`ve been a couple of orders lately
(-)There haven`t been a c...
(?)Have there been a c...?
(-?)Haven`t there been a c...?
(=)Yes, there have | No, There haven´t
"

Ha habido demaciada lluvia y la gente ha estado usando impermiables;"
There has been too *(NO many)much* rain and *(NO)the* people have been *wearing* waterproof
"

Por las indundaciones, ha habido unas cuantas casas afecadas;"
*Because of* the floods, there *have* been a couple of hauses affected
"

Ha habido cientos de carros atrapados en la autopista;"
There´ve been hundreds of cars trapped *on* the highway
"

¿Cuantas personas heridas ha habido?;"
How many injured people have there been?

-Otra
- How many people have there been hurt?
- ¿Cuántas personas han resultado heridas?
"

Ha habido menos nieve esta semana, sin embargo, hemos estado prendiendo la calefacción;"
There's been less snow this week; however, We've been turning on the heating.
"

Recientemente ha habido poca agua en la región, tal vez es por la ola de calor;"
Recently, there's been little water in the region, maybe it is because of the heat wave.
"

Este año ha habido muchos más turistas que el año pasado, es por eso que hemos estado vendiendo mucho más;"
This year there have been much more tourists than last year, that's why we've been selling much more
"

No ha habido nadie en esta casa durante años. Parece una casa fantasma;"
There hasn't been anyone in this house for years. It looks like a ghost house.
"

¿Ha habido muchas quejas sobre el servicio al cliente?;"
Have there been many complaints about the customer service?
"

Con esta, ha habido tres clases de inglés el día de hoy. Hemos estado estudiando demasiado;"
With this one, there have been three English classes today. We've been studying too much.
"

En este pueblo, ha habido algunos casos de robo, sin embargo, el alguacil ha estado buscando los ladrones;"
In this town, there have been some theft cases, however, the Sheriff has been looking for the thieves.
"

He estado leyendo tu ensayo pero ha habido un par de errores que pude identificar;"
I've been reading your essay but there have been a couple of mistakes that I could identify.
"

¿No ha habido mucho polvo en garaje? Si, nuestro abuelo ha estado arreglando su silla favorita;"
Hasn't there been a lot of dust in the garage? Yes, our grandfather has been fixing his favorite chair.
"

No ha habido nadie que pueda ayudarme. He buscado ayuda por todas partes;"
There hasn't been anyone who can help me. I've looked for help everywhere.
"

Is there any reason why there hasn´t been a review for this hotel since December 2016?;"
¿Hay alguna rason porque no ha habido una reseña para este hotel desde diciembre del 2016?
"

There haven´t been any new episodes of my favorite series, so I´ve been watching some TV soaps;"
No ha habido ningun nuevo episodio de mi serie favorita, asi que he estado viendo algunas telenovelas
"

Why hasn´t there been a World War III?;"
¿Por que no ha habido una 3* guerra mundial?
"
```

### PRUEBA 1 - PRE-INTERMEDIO

[Examen](https://www.pacho8a.com/ingl%C3%A9s/curso-ingl%C3%A9s-pre-intermedio/prueba-1-pre-intermedio/)

### 11. Diferencia entre Presente Perfecto y Pasado Perfecto

```r
# Que es presente perfecto, que es present perfect, definicion de presente perfecto
Que significa que un evento esta en presente perfecto, y ejmeplo en esp/ing;"
Que el evento inicio en el pasado y aun tiene vigencia en el presente

Ejemlo: Ella ha trabajado duro hoy = She has worked hard today
"

Diferencia entre el presente perfecto y el presente perfecto continuo/progresivo;"
Son paresidos, solo que el P.P. se tiene insertidumbre si continuara
el evento o accion. Y el P.P.C./P. se tiene mayor seguridad que el evento
o accion continue hacia el futuro.

Ej. P.P. : Ella ha asado la carne y yo he cortado las verduras
Ej. P.P.P: Ella ha estado cortando la carne mientras yo cocino
"

# Que es pasado perfecto, que es past perfect, definicion de pasado perfecto
Que es el pasado perfecto, y ejemplo esp/ing;"
Un evento que inicio en el pasado y termino en el pasado, y opcional-
mente antes de que ocurriera un segundo evento.

|------pasado---------------|-----futuro-----|
  Ev1.---------Ev1  Ev2--- 


Ej: Ella habia trabajado duro hoy = she had worked hard today
"


Diferencia entre Presente Perfecto y Pasado Perfecto;"
un evento presente perfecto ocurrio y aun sigue en el presente, mientras
que el pasado perfecto, el evento inicio en el pasado y termino en el
pasado.
Es decir el presente perfecto llega ha tocar el presente, mientras que
el pasado perfecto nunca lo toca.
"

Estructura del pasado perfecto;"
Subject + had + verb Pasado Participio + complemento
 She    + had +         worked         +   today
"

Version extendida de 1_`she´d work` y 2_`she´d worked`, y traducion;"
1. She would work = Ella trabajaria
2. She had worked = Ella habia trabajado
"

(+,-,?,=)El habia ido al parque;"
(+) He had gone to the park     = He´d gone ...
(-) He had not gone to the park = He hadn´t gone ...
(?) Had he gone to the park?
(=)Yes, he had, No, he had not.
"

El mesero me habia recomendado esa comida / La mesera habia recomendado esa comida, a mi;"
=The waiter had recommended *me* that food
=The waitress had recommended that food to me
"

Hemos encontrado un buen hotel para pasar la noche;"
We *(NO had=habiamos)have* found a good hotel to *spend* the night.
"

El la habia conocido a ella en N.Y.;"
had he *(known=saber)met* her in New York?
"

Nicole no habia escrito la carta de renuncia;"
Nicole hadn´t written the resignation letter
"

1. Ellos no habían decidido comenzar a estudiar;"
They hadn't decided to start studying.
"

2. Juan no había dicho nada de su nuevo trabajo;"
Juan hadn't said anything about his new job.
"

3. ¿Había Tiffany escogido una carrera para cuando tenía 10 años?;"
Had Tiffany chosen a career by when she was 10 years old?
"

4. Joseph ha tomado una decisión muy dificil en su vida;"
Joseph has taken a very difficult decision in his life
"

5. El profesor había hecho muchos ejercicios interesantes en el tablero;"
The teacher had made many interesting exercises on the board.
"

6. ¿No habían ellos hablado en publico en muchas ocasiones?;"
Hadn't they spoken in public in many ocassions?"

7. ¿Habías leído el capítulo 19 con Michael y John?;"
Had you read chapter 19 with Michael and John?
"

8. Ese actor famoso ha actuado en más de 50 películas hasta ahora;"
That famous actor has acted in more than 50 movies so far
"

9. David le había dado a su hermano buenas noticias sobre la universidad;"
David had given his brother good news about the university.
"

10. ¿Se habían conocido ellos en Australia?;"
Had they met in Australia?
"

Had he read the newspaper yet?;"
¿Él ya había leído el periódico?
"

Amanda had turned off the TV because she was way too sleepy;"
Amanda había apagado el televisor porque estaba demasiado adormilada
"

Hadn´t you been to the Central Park with your dog and friends?;"
¿No habías estado en el parque central con tu perro y amigos?
"
```

### 12. Pasado Simple/tiempo preterito y Pasado Perfecto - Conjunciones / Conectores

```r
Da unos ejemplos de conjunciones entre pasado simple y perfecto;"
when (cuando), as soon as (en tanto), by the time(para cuando),
[1day] before ([idia antes que]), until(hasta)
"

traduce y para que se suelen usar: when, as soon as, by the time, before, until;"
Suelen usarse como conjunciones entre pasado simple y pasado perfecto.

- when (cuando),
- as soon as (en tanto),
- by the time (para cuando),
- [1day] before ([idia antes que]),
- until (hasta)
"

Diferencia entre Pasado Simple y Pasado Perfecto;"
Ambos indican acciones que ya han sido completadas, pero los tiempos
difieren en estructura y significado.

PASADO PERFECTO: Suele tener la palabra had en y viene seguido de otro
verbo. Ejemplo.  
When I got there, he had left the house=Cuando llegué, ya había salido de casa

PASADO SIMPLE: Suele tener una acción única que empezó y terminó en el pasado.  
Ejemplo I went to the store yesterday=Ayer fui a la tienda). Esto es tiempo.

PASADO PERFECTO: Suelen tener las palabras before y after.  
Ejemplo: Before I got there, they had started eating=Antes de que llegara,
habían empezado a comer.

PASADO PERFECTO: Suele tener un evento pasado que ocurrio antes de otro
evento pasado.  
Ejemplo: We had been stranded for two days before he arrived
        =Estuvimos varados durante dos días antes de que llegara.
"

¿Para conectar dos eventos en pasado simpre debemos escribimos Ev1 y luego Ev2?, ej. ´(Ev1)Ella se habia ido cuando, (Ev2)el concierto termino´;"
No por que podemos decir `(Ev2)cuando eso termino, (Ev1)ella se fue`, lo mismo aplica
en ingles: `she had left when the concert ended` o `when the concert ended she had left`
"

Traduce y Señala pasado simpre y pasado perfecto: |ella se habia ido| cuando |el concierto termino|;"
|she had left| when |the concert ended|
    P.P.                   P.S.
"

Traduce y Señala pasado simpre y pasado perfecto: en tanto |Nos bajamosdel carro| |Ellos ya nos habian visto|;"
as soon as |we get off the car| |they had already seen us|
                    P.S.                P.P.
"

Traduce y Señala pasado simpre y pasado perfecto: |Habiamos ido al eropuerto| para cuando |nos llamaste|;"
|We'd gone to the airport| by the time |you called us|
          P.P.                               P.S.
"

Diferencia de pronunciacion entre 1. Weed y 2. we´d;"
1. /guid/
2. /guidt/
"

Yo habia estado en madrid| 1 año antes que |nos casaramos|;"
|I´d been to madrid| one year before |ge got married|
"

Traduce y Señala pasado simpre y pasado perfecto: hasta |que la secretaria me lo pidio| |Yo no habia firmado el contrato|;"
until |the secretary asked me| |I hadn´t signed the contract|
                P.S.                         P.P.
"

Traduce y Señala pasado simpre y pasado perfecto: |Yo no habia firmado el contrato| hasta |que la secretaria me lo pidio|;"
|I hadn´t signed the contract| until |the secretary asked me|.
          P.P.                       P.S.
"

1 Yo nunca había comido pizza antes de mudarme a esta ciudad;"
I *had never eaten* pizza before *I* *moved* to this city
"

2 Cuándo ellos llegaron, nosotros ya nos habíamos ido a casa;"
When they *arrived*, we had already gone *(NO)to* home.
"

3 Juan no había visto a su madre después de que él se casó;"
Juan *(NO hasn´t)hadn´t* seen his mother after he got married
"

4 Tan pronto llegué al cine, la película ya había comenzado;"
As soon as I *arrived* at the cinema, the movie had already started
"

5 Ella no se había dado cuenta del temblor de tierra hasta que oyó a la gente gritando;"
She hadn't noticed  the earthquake until she heard people screaming
"

6 Mi hermana me dijo que el había estudiado mucho para el examen;"
My sister told me that he had studied a lot for the exam.
"

7 El bosque se quemó fácilmente porque no había llovido durante meses;"
The forest burned easily because it hadn´t rained for months.
"

8 ¿Había viajado Sergio a Hong Kong antes de que perdiera su trabajo?;"
had sergio traveled to Hong Kong before he lost his job?
"

9. El portero no nos abrió la puerta porque habíamos olvidado nuestra tarjeta de identificación;"
The doorman *didn't open* the door *for us* because we had forgotten our id card
"

10 Yo no sabía que habías venido con toda tu familia al evento;"
I didn`t know that you had come with all your family to the event
"

When the police arrived, the thieves had stolen everything;"
Cuando la pilicia llego, los ladrones habian robado todo.
"

2 Alice hadn´t been so happy until she bought that old car;"
Alice no habia estado/sido tan feliz hasta que ella compro ese carro viejo.
"

3 By the time you left, I had already had everything ready;"
Para cuando te fueste, yo ya había tenido todo listo.
"
```

### 13. Pasado Perfecto Progresivo o Continuo con Phrasal Verbs

```r
Estructura del pasado perfecto progresivo;"
Subject + had + been + verb-ing + complement
"

(+,-,?,=)El habia estado ensayando para su presentasion;"
(+)He had been rehearsing for his presentation
(-)He hadn´t been rehearsing for his presentation
(?)Had he been rehearsing for his presentation?
(=) Yes, he had / No, he hadn´t
"

Defierencias entre el pasado perfecto y pasado perfecto progresivo;"
Pasado perfecto se usa para acciones que se han completado en el pasado.
mientras el pasado perfecto continuo puede ser usado para una acción
continua en el pasado.

- PP : El habia leido el libro [cuando invento eso].
- la accion inicio y termino en el pasado, y opcionalmente continuara otra.
- Suele tener la palabra `habia|tenido`

- PPP: El habia estado leyendo el libro [durante dos semanas].
- La accion inicio continuo un tiempo y termino en el pasado, y suele
  indicar que duro un determinado tiempo.
- Suele tener la palabra `habia tenido`
"

Que es un verbo frasal=phrasal verbs y ejemplos;"
Son verbos que estan conformados por mas de una palabra.
- Ejemplos
  - Get over
  - try out
  - look ever
  - write down
  - do over
"

Cheer up;"
Animar - Alegrar
"

Get ahead;"
Salir adelante - progresar
"

Pick out;"
Elegir - Escoger
"

Take back;"
Regresar - Devolver
"

Turn up/down;"
Subir/Bajar
"
 
Let out;"
Dejar salir
"

Get together;"
Reunirse/Juntarse
"

Set up;"
Preparar, poner, montar.
"
 
Go out;"
Salir
"

Throw away;"
Tirar-a-la-basura/Desechar
"

Ella habia estado llorando mucho hasta que lo (supero)-(verbo frasal);"
She had been crying a lot until she *got over* him
"

Ellos habian estado (probando)-(verbo frasal) esa maquina por casi media hora;"
They had been trying out that machine for almost half *an* hour
"

Mi jefe no habia estado (revisado)-(verbo frasal) los pagos y perdio demasiado dinero;"
My boss hadn`t been look over the payments and he lost too much money
"

¿No habias tu estado (anotando)-(verbo frasal) lo que yo te dije?;"
Hadn´t you been writing down what I *told* you?
"

Yo no habia estado ahi cuado lo (hicieron de nuevo)-(verbo frasal);"
I hadn´t been there when they did it over
"

1 Juan había estado (animando)-(verbo frasal) a sus compañeros de trabajo cuándo su jefe llegó;"
Juan had been cheering up his coworkers when his boss arrived.
"

2 Esa gente había estado teniendo muchos problemas desde el año pasado, pero (salieron adelante)-(verbo frasal);"
*Those* people had been *(falto)having* many problems since *(falto)the* last year, but they got ahead
"

3 ¿Habías estado (eligiendo)-(verbo frasal) tu compañero de clase antes que el profesor llegara?;"
had you been picking out your classmate before the teacher arrived?
"

4 Yo había estado conduciendo el carro de mi padre pero ya lo (regresé)-(verbo frasal);"
I had been driving my father´s car, but I *took it back already* 
"

5 Esa música estaba muy fuerte porque mi hijo había estado (subiendo)-(verbo frasal) el volumen del radio;"
That music *was* really loud because my son had been turning up the volume of the radio.
"

6 Ken no había estado en el aeropuerto a esa hora, él llegó tarde porque su jefe no lo dejó (salir)-(verbo frasal);"
Ken hadn´t been *to* the airport at that *time*, he arrived later because his boss didn`t (let him out)
"

7 Mis amigos y yo nos habíamos estado (reuniendo)-(verbo frasal) los fines de semana para ir al centro comercial;"
My friends and I had been (*getting* together) *on* weekends to go to the *shopping mall*
"

8 ¿El mesero no había (puesto)-(verbo frasal) la mesa para toda la familia para cuando llegaron?;"
hadn´t the waiter ** (set up) the table for all the family by the time they arrived?
"

9 Yo no me había (puesto)-(verbo frasal) mi ropa nueva porque tuve que preparar la cena;"
I hadn´t ** (put on) my new clothes because I had to prepare *dinner*
"

10 Mi hermana Alexandra había estado (saliendo)-(verbo frasal) con alguién varias veces hasta que ella se mudó a Paris;"
My sister Alexandra had been (going out) with someone several times until she moved to Paris
"

1 My mom had been using the dental floss for about 15 minutes and then, she threw it away;"
Mi mama habia estado usando la seda dental por unos 15 minutos y luego, ella lo tiro.
"

2 Nobody hadn´t been picking up my children at school, I had to go after work;"
Nadie habia estado *recogiendo* a mis hijos de la escuela, Yo tuve que ir despues del trabajo
"

3 Hadn´t you been calling me back? My phone rang several times;"
*¿No me habías estado llamando?*, Mi telefono sono varias veces
"
```

### 14. Ejercicio de escritura en Pasado Perfecto y Pasado Perfecto Continuo

```r
La madre de Tim habia viajado a Las Vegas para una entrevista de trabajo;"
Tim´s mother had traveled to Las Vegas for a job intrview
"

Estaba cansada porque habia estado conduciendo durante 8 horas seguidas;"
She was tired because she had been driving for 8 hours *in a row*
"

8 horas seguidas;"
in a row
"

Para mantenerse despierta, ella habia preparado un litro de cafe y solia beberlo mientras conducia;"
To keep herself awake, she´d prepared a liter of coffe and she used to drink *it* while driving
"

La radio estaba muy fuerte, ya que ella habia estado cantando sus canciones favoritas;"
The radio was very loud, *since* she´d been singing her favorite songs
"

Minutos mas tarde, el locutor habia dicho los numeros ganadores de la loteria;"
minutes later, the announcer had said the *winning* numbers of the lottery
"

asi que ella detuvo el carro y los anoto;"
So she stoped the car and *wrote them* down
"

ella habia estado buscando su boleto por mas de 10 minutos, pero no pudo encontrarlo;"
She had been *looking* for her ticket for more *than* 10 minutes, but she *could`t* found it
"

ella estaba tan triste, cuando regreso a casa, recordo que habia dejado el boleto sobre la mesa;"
she was soo sad, when she *came* back home, she remembered that she had left the ticket on the table
"

Entonces ella reviso los numero ¿y adivina que?, no habia ganado la loteria;"
then she checked the numbers, and guess what?, she hadn´t won the lottery
"

#####

Antes de morir, mi abuelo Jerry había estado viajando alrededor del mundo con mi abuela Elsa;"
Before *dying*, my grandpa Jerry had been traveling around the world with my grandma Elsa
"

Ellos habían comprado una casa pequeña en California y vivieron allí por más de 15 años;"
They had bought a small house in california and *they* lived there for more *than* 15 years
"

Mi abuela Elsa nunca había visitado otros países;"
My grandma Elsa had never visited other countries
"

incluso ella no había estado fuera de California;"
even she hadn`t *ever been outside* California
"

Mi abuelo fue un soldado americano y había visitado algunos países con el ejército, pero nunca con mi abuela;"
My grandpa was *an* American soldier and he had visited some countries with the army, but never with my grandma
"

Un día, decidí hacer algo especial por mis abuelos;"
one day, I decided to do something special for my grandparents
"

Yo no los había visitado por casi 5 años ya que había estado trabajando para una gran compañía a tiempo completo;"
I hadn´t visited them for almost 5 years because I *have* been working for a big company ** full time
"

así que les envié un sobre con 10 boletos diferentes dentro;"
So I *sent* them *an* *envelope* with 10 different tickets inside
"

Era casi medio día cuándo recibí una llamada telefónica;"
It was almost *midday* when I got a call
"

Era Jerry, mi abuelo. Había recibido el sobre;"
It was Jerry, my grandpa, He had received the envelope
"

y estaba bastante emocionado junto con mi abuela;"
And he was pretty excited *along* with my grandma
"

Habían estado ahorrando algo de dinero en una alcancía durante años con el fin de viajar;"
they´d been saving some ** money in a *piggy bank* for years in order to travel
"

pero nunca habían imaginado que su nieto iba a hacer sus sueños realidad de viajar juntos y visitar nuevos lugares;"
but they *hadn’t ever* imagined that *their* grandson was *going to make their dreams come true to travel together and to visit new places.*
"
```

### 15. Dictado en Pasado Perfecto y Pasado Perfecto Continuo

[Tomar dictado](https://www.youtube.com/watch?v=FU4Vvv2ttOc)

### 16. Futuro Perfecto y Presente Simple usando BY, BEFORE, ALREADY y YET

```r
Uso del Futuro Perfecto y ejemplo es/ing;"
Indicar que un evento futuro ocurrira o no, en un determinado momento,
es decir, que a partir de determinado momento ocurrira o no ocurrira
algo en el futuro. Y tiene enfasis en la accion terminada.
Para describir el momento comunmente se utiliza el presente simple.

Ej.
  Es:Para cuando esta clase termine, habras aprendido muchas cosas
  In:by the time this class *finishes*, you will have learned many things

Nota: Es *finishes* por que *this classs* es tercera persona singular.
Nota2: El orden del {momento} y {lo que ocurrira} puede variar, primero
uno y luego el otro, o viseversa.
"

Estructura del Futuro Perfecto (con have);"
Subject + will have + verbo P.P. + Complement
"

(+,-,?,=) habras aprendido muchas cosas;"
(+)You´ll have learned many things
(-)You won´t have learned many things
(?)Will you have learned many things?
(=)Yes, I´ll / No, I won´t
"

Para cuando el semaforo este en rojo, el carro se habra detenido;"
By the time the traffic light is red, the car will have [stoped | pulled over]
"

Antes de que esta maquina se descomponga, ellos ya la habran arreglado;"
Before ** this machine ** breaks down, they´ll have *already* fixed *it*.
"

¿Habra el conductor atropellado al venado para el momento en que cruce la calle?;"
Will the driver have *run over/down* the deer by the time it *crossed* the street
"

Para mañana en la mañana, no habremos entregado el proyecto todavia;"
*By* tomorrow ** morning, we won´t *have handed in* the project yet
"

Antes de que regresemos a casa, habremos conocido gran parte de esta ciudad;"
Before we come back ** home, We´ll have known great part of this city.
"

Yo ya habré organizado todo para la fiesta para cuando estés aquí;"
I'll *(orden)have already*  organized everything for the party by the time you're here.
"
```

### 17. Futuro Perfecto Progresivo o Continuo con Adverbios y Estructuras

```r
uso del Futuro Perfecto Progresivo, y ej, esp/ing;"
Indicara cuando y durante cuanto tiempo se habra estado realizado una accion,
a diferencia del futuro perfecto, el futuro perfecto progresivo se concentra
en la duracion de ese momento futuro.
Ej.
  Esp: A las 3 en punto, habran estado viendo la pelicula durante una hora
  Ing: at 3 o'clock, they'll have been whatching the movie for one hour
"

Estructura del Futuro Perfecto Progresivo;"
Subject + will + have + been + verb-ing + complement
"

(+,-,?,=)A las 3 en punto, habran estado viendo la pelicula durante una hora;"
(+)At 3 o'clock, they'll have been whatching the movie for one hour.
(-)At 3 o'clock, they won't have been whatching the movie for one hour.
(?)will they have been whatching the movie for one hour at 3 o'clock?
(=) Yes, they will / No, they won't

Nota: Las preguntas en FPP deben de tener el momento al final.
"

El proximo mes, ella habra estado ahorrando durante 9 meses;"
Next month, she'll have been saving for 9 month
"

¿habras estado viviendo en Francia por mas de 8 años cuando tu hija cumpla 10?;"
will you have been living in france for more *than* eight years when *your* daugther *turns* 10 years?
"

No te habre estado esperando por mas 10 minutos para cuando llegues;"
I won't *have* been waiting for you for over 10 minutes by the time you ** arrive
"

###

El próximo viernes cuándo yo llegue a la casa de mi madre, mi familia ya habrá estado asando la carne durante más de 2 horas;"
Next Friday when I *arrive at* my mother's house, my family will have already been grilling the meat for more than 2 hours.
"

Para cuándo terminemos este nivel pre intermedio, ya habremos estado hablando inglés con nuestros amigos;"
By the time we finish this pre-intermediate level, we will have already been speaking english with our friends.
"

Mr. Smith no habrá estado invirtiendo su dinero por más de 6 meses porque la economía está muy inestable;"
Mr. Smith *won’t have* been investing his money for ** over 6 months because the economy *is* very unstable.
"

¿El chef habrá estado cocinando algo especial para ella para cuándo llegue al restaurante?;"
will the chef have been cooking something special for *her* by the time she *arrives* at the restaurant?
"

Habremos estado disfrutando nuestro aprendizaje por casi 10 años para el año 2028;"
We will have been enjoying our *learning* for almost 10 years by the year 2028
"

Cuándo ella despierte a su hija, ella habrá estado durmiendo por casi 3 horas;"
When she *wakes* up her daughter, she´ll have been sleeping for almost 3 hours.
"

¿Habrán ellos estado digitando esos documentos por más de 1 semana para cuándo el jefe pida el reporte?;"
will they have been typing *those* documents for more than 1 week by the time the boss *asks* the report?
"

Para el próximo diciembre, habré estado esforzándome para aprender inglés por casi un año;"
*By* next December, I´ll have been striving to learn english for almost 1 year
"

Nosotros no habremos estado subrayando las palabras desconocidas para cuándo la clase termine;"
We won´t have been *highlighting* the *unknown* words by the time the class finishes.
"

Esos políticos habrán estado discutiendo el mismo tema por casi 2 meses este próximo viernes;"
those politicians will have been *discussing* the same topic for over 2 months this next friday
Those politicians will have been discussing the same topic for almost 2 months this coming Friday
"

She´ll have been working here for five years by the end of this year;"
Ella habra estado trabajando aqui durante 5 años para final de este año
"

I won´t have been running for over an hour by the end of the afternoon;"
No habre estado corriendo por mas de una hora para *al final de la tarde*
"

Will you have been driving from Miami to Orlando for more than 3 hours tomorrow?;"
¿Habras estado conduciendo de Miami a Orlando por mas de 3 horas mañana?
"
```

### Cómo preguntar y responder con WH QUESTIONS en Futuro Perfecto Progresivo

```r
-Por que has estado estudiando ingles por 1 año -Por que yo habre estado trabando para una gran compania para ese entonces;"
-Why will you haven been studying for one year
-because I´ll have been working for a bing company by that time
"

Habre estado estudiandolo para enseñarlo;"
I will have been studying it TO teach it
"

-¿Donde habran ellos estado entrenando para el campeonato? -Ellos habran estado entrenando en Budapest;"
-Where will they have been training for the championship?
-They´ll have been training IN Budapest
"

-¿Cuando habra estado Gus comprando su nueva casa? -El habra estado comprando su nueva casa para diciembre -De hecho para diciembre 24;"
-When will have been Gus buying his new house?
-He´ll have been buying his new house by December
-Actually at December 24th
"

-¿Que habras estado haciendo para el final de este año? -Yo habre estado viajando por todo Ecuador con mis amigos;"
-What you have been doing by yhe end of the year?
-I will have been traveling *all over* Ecuador with my friends
"

-¿QUe pelicula habremos estado viendo esta noche? -Habremos estado viendo esta o esa;"
-Which movie will have been watching tonight?
-We´ll have been watching this or that one
"

-¿Como habran estado pagando tus amigos el viaje? -Ellos habran estado pagandolo mensualmente;"
-How will your firends been paying the trip
-They´ll have been paying it monthly
"

-¿Cada cuanto habra ella estado ensayando el discurso? -Ella habra estado ensayandolo todos los dias;"
-How often will she have been rehearsing the speech
-She´ll have been rehearsing it every day
"

# TODO: FALTAN EJERCICIOS
1. ¿Cuánto tiempo habremos estado trabajando para tu padre?;"
How long will we have been working for *your* father?
"

2. ¿Cuándo habrás estado terminando el proyecto?;"
when will you have been finishing the Project?
"

3. ¿Por qué Jaime habrá estado invirtiendo su dinero durante 10 años?;"
Why will Jaime have been investing his money for 10 years?
"

4. ¿Qué tan a menudo habrán estado leyendo el libro la próxima semana?;"
How often will they have been reading the book *for* next week?
"

5. ¿Cuáles países habrás estado visitando en los próximos dos años?;"
which countries will you have been visiting by the next two years?
"

6. ¿Qué habrá estado pintando Juan en septiembre 20?;"
What will Juan have been painting at september 20th?
"

7. ¿Habrán estado ustedes viviendo juntos antes de casarse?;"
will you have been living together before *getting married*?
"

8. ¿Con qué frecuencia habrá estado corriendo Michael antes de la maratón?;"
how often will Michael have been running before the marathon?
"

How will you have been living the next five years? I’ll have been living them peacefully;"
- ¿Como habras estado viviendo los proximos 5 años?
- *Los habré* estado viviendo en paz.
"

Who will have been with supporting by the time need it? All my family will have been supporting me as they always do it;"
- ¿Quien habra estando apoyando para cuando lo necesite?
- Toda mi familia habra estado apoyandome como siempre lo hacen
"
```

### 19. Aprende a diferenciar el Futuro Perfecto del Futuro Perfecto Progresivo

```r
diferencia entre futuro perfecto y futuro perfecto progresivo y ejemplo en esp;"
FP: Ago habra estado ocurriendo en un momento (Tiene enfasis en la accion terminada)
FPP: Algo abra estado ocurriendo en un momento (Tiene enfasis en la duracion). 

Ejemplos
- Tengo que salir a las 7pm, ¿Ya [1.habremos cenado|2.habremos estado cenando] para entonces?
  - Si nuestra intencion es decir que si para las 7 ya terminamos de cenar,
    entonces sera 1 (futuro perfecto).
  - Pero si nuestra intencion es decir que para las 7 estaremos cenando
    entonces sera 2 (futuro perfecto continuo)

-Para el siguiente verano, Yo [1.habre vivido|2.habre estado viviendo] en este cuarto por 10 años
  - (1)=FP, no quedaria ya que se da a entender que esta viviendo y seguira
    viviendo en ese cuarto.
  - (2)=FPP, Quedaria ebien ya que estamos hanblando de la duracion para
     ese momento en el futuro.
"

(F.P. o F.P.P.) By the next summer, I __(live) in this flat for 10 years;"
By the next summer, I´ll have been living in this flat for 1o years
"

(F.P. o F.P.P.) She __(travel) for nearly 2 days by the time I see her;"
She _will have been traveling_ for nearly 2 days by the time I see her
"

(F.P. o F.P.P.) When they turn up, I __(withdraw) my money;"
When they turn up, I _will not have withdrawn_ my money
"

(F.P. o F.P.P.) By this time next month, you __(save) enough money to buy a car;"
By this time next month, you _will have saved_ enough money to buy a car
"

(F.P. o F.P.P.) In december George and his wife __(pay) their house for two years;"
In december George and his wife _will have been paying_ their house for two years.
"

(F.P. o F.P.P.) In december George and his wife __(pay) their house;"
In december George and his wife _will have paid_ their house.
"

(F.P. o F.P.P.) We´re so late that everyone __(leave) by the time we get there;"
We´re so late that everyone _will have left_  by the time we get there
"

(F.P. o F.P.P.) By the time she wants to help me I __(clean) the kitchen for over 3 hours;"
By the time she wants to help me I _will have been cleaning_ the kitchen for over 3 hours
"
```

### 20. Una hora de clase con todos los tiempos verbales y auxiliares en todas sus formas

resumen uso de tiempos, uso general de tiempos, resument tiempos,

```r
uso de be, tabla de be, reparticion del be segun tiempos;"
I:    am  / was  / will be
you:  are / were / will be
he:   is  / was  / will be
she:  is  / was  / will be
it:   is  / was  / will be
we:   are / were / will be
they: are / were / will be
"

# presente
(+,-,?,=)Yo estoy en clase;"
(+)I´m in class
(-)I´m not in class
(?)Am I in class?
(=)Yes, I am / No, I´m not
"

(+,-,?,=)Tu estas en clase;"
(+)you're in class
(-)you aren't in class
(?)Are you in class?
(=)Yes, you are / No, you aren´t
"

(+,-,?,=)Ella esta en clase;"
(+)She´s in class
(-)She isn´t in class
(?)Is she in class?
(=)Yes, She is / No, She isn´t
"

# pasado

# was para singular, were para plural
(+,-,?,=)Yo estaba en clase;"
(+)I was in class
(-)I wasn´t in class
(?)Was I in class?
(=)Yes, I was / No, I wasn´t
"

(+,-,?,=)Ellos estaban en clase;"
(+)They were in class
(-)They weren't in class
(?)Were they in class?
(=)Yes, they were / No, they weren´t
"

# futuro

(+,-,?,=)Yo estare en clase;"
(+)I´ll be in class
(-)I won't be in class
(?)Will I be in class?
(=)Yes I will / No I won´t
"

(+,-,?,=)Tu estaras en clase;"
(+)You´ll be in class
(-)You won´t be in class
(?)Will you be in class?
(=)Yes you will / No Yo won´t
"

(+,-,?,=)El estara en clase;"
(+)He´ll be in class
(-)He won´t be in class
(?)Will he be in class?
(=)Yes he´ll / No he won´t
"

# Tiempo continuo o progresivo

#pasado continuo

(+,-,?,=)Yo estaba comiendo;"
(+)I was eating
(-)I wasn´t eating
(?)Was I eating?
(=)Yes I was / No I wasn´t
"

(+,-,?,=)El estaba comiendo;"
(+)He was eating
(-)He wasn´t eating
(?)Was he eating?
(=)Yes he was / No he wasn´t
"

(+,-,?,=)Tu estabas comiendo;"
(+)You were eating
(-)You weren´t eating
(?)Were you eating?
(=)Yes you were / No you weren´t
"

#Presente continuo
(+,-,?,=)Yo estoy comiendo;"
(+)I'm eating
(-)I'm not eating
(?)Am I eating?
(=)Yes I am / No I´m not
"

(+,-,?,=)ustedes estan comiendo;"
(+)You´re eating
(-)You aren´t eating
(?)Are you eating?
(=)Yes you are / No you aren´t
"

(+,-,?,=)El esta comiendo;"
(+)He´s eating
(-)He isn´t eating
(?)Is he eating?
(=)Yes he is / No he isn´t
"

# Futuro continuo
(+,-,?,=)Ustedes estaran comiendo;"
(+)You´ll be eating
(-)You won´t be eating
(?)Will you be eating?
(=)Yes you will / No you won´t
"

(+,-,?,=)Ella estara comiendo;"
(+)She'll be eating
(-)She won't be eating
(?)Will she be eating?
(=)Yes she will / No she wont't
"

(+,-,?,=)Nosotros estaremos comiendo;"
(+)We'll be eating
(-)We won't be eating
(?)Will we be eating?
(=)Yes we wiil / No we won´t
"

# Pasado continuo con going to
(+,-,?,=)Ella iba a comer;"
(+)She was going to eat
(-)She wasn't going to eat
(?)Was she going to eat?
(=)Yes she was / No she wasn´t
"

(+,-,?,=)Ellos iban a comer;"
(+)They were going to eat
(-)They weren´t going to eat
(?)Were they going to eat?
(=)Yes they were / No they weren´t
"

(+,-,?,=)Yo iba a comer;"
(+)I was going to eat?
(-)I wasn´t going to eat
(?)Was I going to eat?
(=)Yes I was / No I wasn´t
"

# presente continuo con going to
(+,-,?,=)Tu vas a comer;"
(+)You´re going to eat?
(-)You aren´t going to eat
(?)Are you going to eat?
(=)Yes you´re / No you aren´t
"

(+,-,?,=)El va a comer;"
(+)He is going to eat
(-)He isn't going to eat
(?)Is he going to eat?
(=)Yes he is / No he isn´t
"

(+,-,?,=)Yo voy a comer;"
(+)I´m going to eat
(-)I´m not going to eat
(?)Am I going to eat?
(=)Yes I´m going to / No I´m not going to
"

#tiempos simples - simple tenses: Do/Does, did, will

#simple present, presente simple
(+,-,?,=)Ellos cocinan;"
(+)They cook
(-)They don't cook
(?)Do they cook?
(=)Yes, they do / No, they don´t
"

# does para 3ra persona singular "she, he, it", do para "i, you, they, we"

(+,-,?,=)Ella cocina;"
(+)she cooks
(-)She doesn't *cook*
(?)Does she cook?
(=)Yes, she does / No, she doesn´t
"

#simple past, pasado simple

(+,-,?,=)Tu Cocinaste;"
(+)You cooked
(-)You didn't cook
(?)Did you cook?
(=)Yes, you did / No, you didn´t
"

(+,-,?,=)El comio;"
(+)She ate
(-)She didn't eat
(?)Did she eat
(=)Yes, she did / No, she didn´t
"

#simple future / futuro simple

(+,-,?,=)Ellos cocinaran;"
(+)They will cook
(-)They won´t cook
(?)Will they cook?
(=)yes they will
"


# TIEMPOS PERFECTOS, PERFECT TENSES > have/has, had, will have
# se usan los verbos en pasado participio para los 3 tiempos perfectos

#Presente perfecto, present perfect
# has para 3ra persona singular (she/he/it) have para (I/you/they/we)
(+,-,?,=)Hemos disfrutado;"
(+)We've enjoyed
(-)We haven´t enjoyed
(?)Have we enjoyed?
(=)Yes, we have / No, we haven´t
"

(+,-,?,=)Ha disfrutado;"
(+)He's enjoyed
(-)He hasn´t enjoyed
(?)Has he enjoyed?
(=)Yes, he has / No, he hasn't
"

Diferencia entre 1.(he´s run) y 2.(he´s running) y traduccion;"
Diferencia: La forma corta no refiere a lo mismo (1) es para `have` y
el (2) es para `is`, y nos damos cuenta por la palabra siguiente y su
forma, (1) es un verbo en pasado participio, y el (2) es un

he´s run     = he had run    = El ha corrido.
he´s running = he is running = El esta corriendo
"

(version extendida y traduccion) that´s never been true;"
- That has never been true
- Eso nunca ha sido verdad
"

#pasado perfecto, past perfect
# Se usa had como para 3ra persona singular y los demas
(+,-,?,=)Liz habia disfrutado;"
(+)Liz had enjoyed
(-)Liz hadn't enjoyed
(?)Had Liz enjoyed?
(=)Yes, Liz had / No, Liz hadn´t
"

(+,-,?,=)habian disfrutado;"
(+)They had enjoyed
(-)They hadn't enjoyed
(?)Had they enjoyed?
(=)Yes, they had / No, they hadn´t
"

#futuro perfecto, future perfect
(+,-,?,=)habra disfrutado;"
(+)He'll have enjoyed.
(-)he won't have enjoyed.
(?)Will he have enjoyed?
(=)Yes, he will / No, he won´t
"

(+,-,?,=)habre vendido;"
(+)I'll have sold.
(-)I won't have sold.
(?)Will I have sold?
(=)Yes, I will / No, I won´t
"

# TIEMPOS PERFECTOS PROGRESIVOS > have/has, had, will have
# lo mismo que tiempos perfectos pero acompañado del been

#Presente perfecto progresivo, present perfect progresive
(+,-,?,=)Ella ha estado viendo tv;"
(+)She's been watching TV
(-)She hasn't been watching TV
(?)Has she been whatching TV?
(=)Yes, she has / No, she´s not
"

(+,-,?,=)Ellos han estado viendo TV;"
(+)They've been watching TV
(-)They haven't been watching TV
(?)Have they been watching TV?
(=)Yes they have / No, They´ve not
"

¿Ambas son correctas? (1.They´ve not ...) (2. They haven´t ...);"
Si
"

#Pasado perfecto progresivo, past perfect progresive
(+,-,?,=)Isma habia estado corriendo;"
(+)Isma had been running
(-)Isma hadn´t been running
(?)Had Isma been running?
(=)Yes, he had / No he hadn´t
"

(+,-,?,=)habiamos estado corriendo;"
(+)We had been running
(-)We hadn´t been running
(?)Had we been running?
(=)Yes, we had / No, we hadn´t
"


#Futuro perfecto progresivo, future perfect progresive
(+,-,?,=)Gina habra estado firmado;"
(+)Gina will have been signing
(-)Gina won´t have been signing
(?)Will Gina have been signing?
(=)Yes, she will / No, she won´t
"

(+,-,?,=)Habre estado firmando;"
(+)I will have been signing
(-)I won´t have been signing
(?)Will I have been signing?
(=)Yes, I will / No, I won´t
"

```

### PRUEBA 2 - PRE-INTERMEDIO

[prueba-2-pre-intermedio](https://www.pacho8a.com/ingl%C3%A9s/curso-ingl%C3%A9s-pre-intermedio/prueba-2-pre-intermedio/)

Errores

- Mayor cuidado al leer las preguntas, ya que traduci de rapido, "¿cuanto
  cuesta un lapiz?" en lugar de "¿cuando cuentas dos lapises?" que hera
  la correcta
- Diferencia entre incorrecta: "they has been in Spain many times." y
  correcta: "they have been to Spain many times."
- Uso del "who" y del "Whose" incorrecta:"Who's dog is this?" correcta:"Whose dog is this?"
- Aprender bien la estructura del "Futuro perfecto progresivo" ya que
  use "he will has been" y era "he will have been", y segun la estructura
  se usa "will has been" tanto para 3ra persona singular y los demas.

```r

"
In April 2002, Malaysian police arrested a woman for selling “magic pencils” for $225 each. The woman claimed that the pencils could produce correct answers in university entrance exams. 

How much would two magic pencils cost?
.  +)$22.800
.  +)$225
.  +)$450
.  +)$2002";"
$450
"

"Michael and Sharick...
+)has been to Spain many times.
+)have been to Spain many times.
+)has been in Spain many times.
+)have been in Spain many times.
";"
Michael and Sharick have been *to* Spain many times.
"

__ dog is this? || +)Who´s | +)Whose | +)Whom;"
Whose dog is this?
"

"Edgar will ...
=)has been living in Europe for three years by the time my son be born
=)have been living in Europe since three years.
=)have been living in Europe for three years by the time my son be born.
=)has been living in Europe for three years.

";"
Edgar will have been living in Europe for three years by the time my son be born.
"
```

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

#############################################################################################################
#############################################################################################################

-->

## B2. INGLÉS INTERMEDIO

- [Fuente - Lista](https://www.pacho8a.com/ingl%C3%A9s/curso-ingl%C3%A9s-intermedio/)

### 2. Affirmative Statements with Professions. Listen and classify the ques / Comprehension

Escuchar [audios](https://www.pacho8a.com/ingl%C3%A9s/curso-ingl%C3%A9s-intermedio/lesson-2/)

### N. Cuando usar DON’T HAVE y HAVEN’T, DOESN’T HAVE y HASN’T – Diferencias

- [Fuente](https://www.youtube.com/watch?v=xRwh2B5TDz8)

```r
Yo no tengo un carro nuevo;"
Mal: `I haven´t a new car`
+ Porque no cumple con la estructura del presente simple, que es el
. tiempo en que esta la frase que queremos traducir, el have con esa
. estructura se pareciera a la del presente perfecto que se traduciria
. algo asi como 'yo no he nuevo carro'

Bien: `I don´t have a new car`
+ Porque si cumple con la estructura del PRESENTE SIMPLE, que es la actual
. que queremos traducir.

O bien: `I haven't had a new car`='Yo no he tenido un carro'
+ para usar el have de esa forma estamos usando la estrucura del
. PRESENTE PERFECTO, pero no es lo que queremos traducir ahora.

Tip: Hay que saber cuando usamos have como verbo 'tener', o como
auxiliar 'he'
"

I haven´t a new car;"
La estructura esta mal.

Si se quiere decir 'Yo no tengo un carro nuevo'
entonces seria 'I don´t have a new car'

o para decir 'Yo no he tenido un carro nuevo'
seria 'I haven´t had a new car'
"

I haven´t had;"
No he tenido
"

I don´t do;"
Yo no hago
"

Yo no he tenido un carro nuevo;"
I haven't had a new car
"

I haven´t had a new car;"
Yo no he tenido un carro nuevo
"

1. Melani no tiene mascotas / 2. Melani no ha tenido mascotas;"
1. Melani doesn't have pets
2. Melani *has't* has pets
"

Melani has´t pets;"
Melani no ha mascotas

Estructura incorrecta. las correcciones serian.
1. Melani doesn't have pets: Melani no tiene mascotas
2. Melani *has't* has pets: Melani no ha tenido mascotas
"

1.No tenemos que trabajar hoy / 2.No hemos tenido que trabajar hoy / 3.No hemos trabajado hoy;"
1.We don´t have to work today
2.We haven´t had to work today
3.We haven´t worked today
"

We haven´t to work today;"
No hemos trabajar hoy

Estructura incorrecta. las correcciones serian.
1. We don´t have to work today: No tenemos que trabajar hoy
2. We haven´t worked today: No hemos trabajado hoy
"

El no ha tenido un buen trabajo;"
He hasn´t had a good job.
"

El no tiene un buen trabajo;"
He doesn´t have a good job
"

He hasn´t a good job;"
El no ha un buen trabajo

Estructura incorrecta. las correcciones serian.
1. El no ha tenido un buen trabajo: He hasn´t had a good job
2. El no tiene un buen trabajo: He doesn´t have a good job
"

No tengo nada que perder;"
I don´t have *X(nothing) O(anything)* to lose
"

No he tenido nada que perder;"
I haven´t had *anything* to lose
"

I haven´t nothing to lose;"
Yo no he nada que perder.

Estructura incorrecta. las correcciones serian.
1. Yo no tengo nada que perder: I don´t have nothing to lose
2. No he tenido nada que perder: I haven´t had *anything* to lose
"

1. Ellos no tienen que hacerlo;"
They don’t have to do it.
"

2. Mi amigo no ha tenido nunca un perro;"
My friend hasn’t ever had a dog.
"

3. No tienes que irte todavía, te puedes quedar hasta mañana;"
You don’t have to go yet; you can stay until tomorrow.
"

4. Carmen no tiene animales en su casa, pero ella tiene algunos en su casa de campo;"
Carmen doesn’t have animals in her house, but she has some in her country house.
"

5. Me gustaría ayudarte, pero no tengo suficiente dinero;"
I’d like to help you, but I don’t have enough money
"

6. Peter and Mike no han tenido una oportunidad de reunirse hoy;"
Peter and Mike haven’t had a chance to meet today
"
```

### Pronombres indefinidos

- [Fuente](https://www.pacho8a.com/ingl%C3%A9s/pronombres-indefinidos/)

#### Pronombres indefinidos para LUGARES, Cuando y como usar SOMEWHERE- ANYWHERE - EVERYWHERE - NOWHERE

```r
Mensiona 5 posibles traducciones de where;"
¿Donde?
lugar(es)
lado(s)
parte(s)
sitio(s)
"

¿Que es el acronimo S.A.E.N. para pronombres indefinidos?;"
Some, Any, Every, No
"

Somewhere, Anywhere, Everywhere, Nowhere;"
Somewhere: [Algun|alguna]-[lugar-lado-parte-sitio]
Anywhere: [ningun|cualquier]-[lugar-lado-parte-sitio]
Everywhere: [todos|todas]-[lados-partes]
Nowhere> Ningun(a)[lugar-lado-parte-sitio]
"

3 traducciones de some;"
algo, algun, alguna
"

ellos quieren ir a algun lugar;"
They what to go somewhere
"

¿Quieren ir ellos a algun lugar?;"
*Do* they what to go somewhere?
"


3 traducciones de any;"
algo, ningun, cualquier
"

Ellos no quieren ir a cualquier/ningun lugar;"
They don´t want to go anywhere

cualquier/ningun: se diferencian por el contexto.
"

Ellos quieren ir a [todos lados|todas partes];"
They whant to go everywhere
"

No quieren ir a ningun lado;"
They want to go nowhere

Mal: They don´t want to go nowhere: tenemos doble negacion, En español
no hay problema pero en ingles si
"

Mis llaves deben estar en alguna parte;"
My keys must ** be somewhere
"

¿Estuvieron ustedes en algun lugar ayer?;"
*were* you [anywhere|somewhere] yesterday?

somewhere: Uso este si estoy casi o seguro que estuvieron en algun lugar.
anywhere: Uso este si no estoy seguro.
"

En mi casa hay polvo por todas partes;"
In my house there is dust everywhere
"

Yo no fui a ningun lado anoche;"
I went nowhere last night
I didn´t go *X(somewhere) O(anywhere)* last night
"

Llevame donde sea;"
take me somewhere=algun lugar conocido
take me anywhere=donde sea incluso desconocido
"

1. Te encontraré en algún lugar de este mundo;"
I will find you somewhere *X(of) O(in)* this world
"

2. Michael perdió su billetera en alguna parte.;"
Michael lost his wallet somewhere
"

3. ¿Salió ella a algún lugar?;"
did she go out anywhere/somewhere??
"

4. Tenemos que respetar a las personas aquí y en todas partes;"
We have to respect people here and everywhere
"

5. No había ningún lugar para estacionar el carro;"
There was nowhere to park the car
"

6. Hoy es viernes y me gustaría ir contigo a cualquier parte.;"
Today is friday and I would like *to* go with you anywhere
"

7. Carlos y Fatima no pudieron encontrar ningún lugar para bailar;"
Carlos and Fatima *X(can found) O(could find)* nowhere to dance
"

8. Ese edificio es tan alto que puedo verlo desde cualquier parte de la ciudad;"
That building is so high that i can see it *X(since) from* anywhere in the city
"

9. ¿Esos animales tienen que encontrar algún lugar para pasar el invierno?;"
Do those animals have to find somewhere to spend the winter?
"

10. Yo no puedo ir a ningún lado pero ustedes pueden ir a cualquier parte;"
I can't go anywhere, but you can go anywhere
"

1. ¿Su celular esta en algun lado de este carto? ;"
Is her cell phone anywhere in this room?
"

3. Ella no fue a ningun lugar con el ayer.;"
She didn´t go anywhere with him yesterday
"

4. La solucion a la probreza es difundir la educancion a todas partes.;"
The solution to poverty is to spread education everywhere
"

5. You can leave early tonight, but I don´t want to see dust anywhere;"
puedes salir temprano esta noche, pero no quiero ver polvo en ningun sitio
"

6. No he visto a Juan en ningun lugar. el debe estar escondido en algun lugar;"
I haven´t seen Juan anywhere. He must be hidden somewhere.
"

7. The kids can´t be running everywhere. They´re destroying everything.;"
Los niños no pueden estar corriendo por todas partes, Estan destruyendo todo
"

2. I looked for a hotel everywhere in this city, but I had nowhere to spend the night.;"
Busqué un hotel por todas partes en esta ciudad, pero *no tuve* ningun lugar donde pasar la noche.
"
```

#### Pronombres indefinidos para PERSONAS: Uso de SOMEONE, ANYONE, EVERYONE, NO ONE

la fuente esta en titulo de este sub-titulo

```r
(uso y traduccion) Some[one|body], Any[one|body], Every[one|body], No[One|body];"
El uso de `XXXone` es mas comun y formal, el uso de `body` es mas informal.

-Traducion
. Some[one|body]: Alguien/Alguno/Alguna persona.
. Any[one|body]: Cualquier(a)/Nadie/Alguien
. Every[one|body]: Todo(s)/Todo mundo/Todas las personas
. No[One|body]: Ninguno

-Uso
. Some[one|body]: Medio o conocemos la afirmacion o respuesta
. Any[one|body]: Desconocemos la afirmacion o respuesta, o para negaciones
. Every[one|body]: Todo(s)
. No[One|body]: Ninguno/Nadie/Ninguna persona
"

(+,-,?)Ella bailo con alguien;"
(+)She dances with someone/anyone
(-)She doesn´t dance with anyone? (no se puede someone)
(?)Does she dance with someone/anyone?

.  anyone: Cualquiera (baila con quien sea incluso desconocido)
.  someone: Alguien (baila con alguien conocido)
"

Todo mundo baila con ella;"
Everyone *dances* with her

*dances* Siempre que hablamos con pronombre indefinidos estamos hablando
de forma singular.
"

Nadie baila con ella;"
No one *dances* with her
"

El puede ir con alguien/cualquira;"
He can go with someone/anyone
"

Yo no conozco a nadie aqui;"
I don´t know anyone here
"

No hay nadie en casa;"
There isn´t anyone at home
There is no one at home
"

Todo mundo deberia de aprender ingles;"
Every-one|body should learn english
"

Nadie puede ayudarme;"
No one can help me
there isn't anyone that can help me
"
```

#### Pronombres Indefinidos para Cosas - Uso de Something, Anything, Everything y Nothing

```r
Something, Anything, Everything y Nothing;"
Something: Algo, alguna cosa
Anything: Cualquier cosa, Nada, Alguna cosa, Algo
Everything: Todo, todo lo que,
Nothing: Nada
"

Yo vi algo en el jardin;"
(+)I saw something/ in the garden
"

(corrige)I didn´t see something in the garden;"
'Yo no vi algo en el jardin', suena raro, mas bien seria 
'Yo no vi nada en el jardin' el cual seria
'I didn´t see anything in the garden'
"

¿Necesitas algo mas? (usar anything y something, y explicar diferencia);"
Do you need something else?
.  Creemos que la respuesta sera positiva

Do you need anything else?
.  No sabemos si la respuesta sera negativa o positiva.
"

Ellos no querian comer nada;"
They didn´t want to eat anything

Tambien podria ser `Ellos no querian comer cualquier cosa` ya depende
del contexto
"

A ella de gutaria hacer cualquier cosa/algo;"
She would like to do anything/something
"

El perdio todo lo que tenia;"
He lost everything *(sobra)what* he had

*what*: Sobra porque el everything se puede usar como 'todo lo que'
asi que ya tenemos el `que` incluido
"

¿Vas a comprar todo lo que necesitas hoy?;"
*Are you going to* buy everithing you need today?

*Will you buy*=compraras
"

No hay nada para comer;"
There is nothing to eat
There isn´t anything to eat
"

¿No hay nada para tomar?;"
Is there nothing to drink?
Is not there anything to drink ?
"

Deberíamos ordenar algo para cenar. No hay nada en la nevera;"
We should order something *for* dinner, there is nothing in the fridge

... dinner, There isn't anything in the fridge
"

¿Qué quieres que cante? Puedes cantar cualquier cosa;"
What do you want *me to sing*? you can sing anything/ You can sing whatever.
"

Yo no presioné nada pero eso comenzó a funcionar solo;"
I didn´t press anything but it started to work by itself.
"

Estaba pensando en algo pero lo olvidé. No recuerdo nada;"
I was thinking *X(in)about* something but I forgot it. I don´t remember anything
... I remember nothing
"

Me encantó ese lugar. Todo estába en paz y armonía. Ahora, todo en mi vida está mucho mejor;"
I *loved* *that* place, everything was in peace and harmony. Now, everything in my life is much better
"

Mike no me contó nada sobre lo que le dijiste. ¿Puedes contarme algo?;"
Mike didn´t tell me *anything* about what you told to him. can you tell me something/anything?
"

No había nada en la habitación pero encontramos algo en el baño;"
There wasn´t anything in the room but we found something in the bathroom
There was nothing in ...
"

Mis compañeros de clase no estaban haciendo nada pero yo estuve haciendo algo productivo;"
My classmates weren´t doing anything, but I was doing something productive
"

Tu siempre dejas todo para el final. Por favor, intenta hacer algo antes de la fecha límite;"
You always *X(let)leave* everything for the end. please, try *to* do something before the deadline

let: permitir-dejar
leave: abandonar-dejar
"

Su madre le dio al perro algo para comer pero el perro no quiso comer nada;"
Her mother gave the dog something to eat but the dog *didn't* want to eat anything. / wanted to eat nothing.
"

My father did¨t give me anything to read during the weekend;"
Mi papa no me dio nada que leer durante el fin de semana
"

Living in New York, where everything is fast, turned me into an impatient person;"
*Vivir* en Nueva York, donde todo es rapido, me volvio en una persona impaciente
"

You didn’t say anything about the new car I bought. Say something next time;"
Tu no digiste nada del nuevo carro que compre. Di algo la proxima vez.
"
```

### Uso de Whatever - Wherever - Whenever - However - Whichever - Whoever

- [Fuente](https://youtu.be/1zr4lfdXyD0)

```r
De que hablan what, who, when, where, which, how;"
What: cosas
Who: personas
When: tiempo/momento
Where: lugar
Which: cosas de una coleccion
How: Manera(s)
"

Que es un sufijo y un prefijo, y ejemplos;"
Sufijo: Palabra que se añade al final de una palabra.
Prefijo: Lo mismo pero se añade al inicio.

Sufijo: ing: load-ing, runn-ing, cooking...
Prefijo: un: un-do, un-fortunately, un-conscious

Tambien los tenemos en español
"

Mensiona 4 posibles traducciones what;"
Que, Cual, Lo(a) que.
"

Wherever, Whenever, However, Whichever, Whoever: taduccion / equivalente formal;"
Whatever: Lo que sea / anything
Whoever: Quien sea / anyone
Whenever: [Cuando sea|Cuando quiera que] / anytime
Wherever: Donde sea / anywhere
Whichever: [Cual sea|Cualquiera que] / n/a
However: [como sea|sin embargo]
"

Lo que sea que hagas, haslo bien;"
Whatever you do, do it well
"

Tu puedes ir con quien sea que tu quieras;"
You can go with whoever you want
"

Llamame cuando sea [que quieras]/[me necesites];"
call me whenever you [want]/[need me]
"

Yo quiero vivir donde sea;"
I want to live where ever=I wanna live where ever
"

Tama cualquiera que tu quieras;"
Take whichever you want
"

Vive como sea que tu lo desees;"
Live however you wish
"

1. ¡Por favor! siéntate dónde sea / dónde quieras;"
Please! sit wherever you want
"

2. Puedes hacerlo de la forma que quieras, pero lo necesito para hoy;"
You can do it however you want, but I need it for today
"

3. Estamos listos para la prueba. Podemos empezar cuando gustes;"
We are ready for the test, we can begin whenever you like.
"

4. Mi perro me sigue donde quiera que yo vaya;"
My dog follows me wherever I go
"

5. Esas no son las respuestas. Puedes preguntarle a quien sea;"
*Those* are not the answers. You can ask whoever.
"

(subfijo ever) 6. My home is your home. You can do __ and you can come __;"
whatever, whenever
"

(subfijo ever) 7. We can play here or __ you can choose __;"
wherever, whichever
"

(subfijo ever) 9. Go __ you want and read a book. You can read  __ you prefer;"
wherever, whichever
"

(subfijo ever) 10. Tomorrow, we will have a party. You are invited to go  __ between 3pm and 7pm and you can invite __ you want;"
whenever, whoever
"
```

### 6. Old habits with USED TO – Affirmative, Negative, Interrogative, and Answers

- [Fuente](https://www.pacho8a.com/ingl%C3%A9s/curso-ingl%C3%A9s-intermedio/lesson-6/)

```r
(+,-,?,=)Las faldas de las mujeres [solian | acostumbraban] ser largas y formales;"
(+)The women's skirts used to be long and formal.
(-)The women's skirts didn't use to be long and formal.
(?)Did women's skirts use to be long and formal?
(=)Yes, they did | No, they didn´t / that`s right | that's wrong
"

(+,-,?,=)Los hombres solian tener el pelo largo;"
(+)Men used to have long hair.
(-)Men didn't use to have long hair.
(?)Did men use to have long hair?
(=)Yes, they did | No, they didn´t / that`s right | that's wrong
"

(+,-,?,=)Los niños solian vestirse como adultos;"
(+)Children used to dress like adults.
(-)Children didn't use to dress like adults.
(?)Did children use to dress like adults?
(=)Yes, they did | No, they didn´t / that`s right | that's wrong
"

Hombres y mujeres solian bailar en eventos formales;"
Men and women used to dance at formal events
Men and women didn't use to dance at formal events
Did men and women use to dance at formal events?
"

Las mujeres solian usar enaguas bajo sus faldas;"
Women used to wear petticoats under *their* skirts
Women didn't use to wear petticoats under their skirts
Did Women use to wear petticoats under their skirts?
"

Los hombres solian llevar bastones;"
Men used to carry walking sticks
Men didn't use to carry walking sticks
Did men use to carry walking sticks?
"
```

### 7. How to use USED TO with WH QUESTIONS – What, Where, Who, How, Why, etc

- [Fuente](https://www.youtube.com/watch?v=YZ4kO7uKjzo)

```r
Estructura para ´used to´ con WH-questions;"
WH-QUESTIONS + DID + SUBJECT + USE TO + VERB(BASE) + COMPLEMENT?
"

¿Que solia usar ella cuando vivia en canada?;"
What did she use to wear when she lived in canada?
"

(corrige)she used to wear a jacket, a scarf and a mittens when she lives in canada?;"
she used to wear a jacket, a scarf and [] gloves when she live[d] in canada[?]
------
a gloves: a=un osea 'un guantes'

she lives: solia vestir ... cuando ella vive en canada, estamos hablando
en pasado, osea cuando ella vivia

canada?: No es una pregunta
"

-¿Cuando solian arrancar las frutas de los arboles? -Soliamos arrancar las frutas de los arboles en la tarde alas 5.;"
-When did you use to pluck the fruits from the trees?
-We used to to pluck the fruits from the trees in afternoon at 5 O'Clock

Nota: sta mal escrito `alas 5` se escribe ´a las 5´
"

-¿Por que solias gastar tanto dinero? -Solia gastar mucho dinero porque(no usar because) vivia en una ciudad muy cara;"
-Why did you use to spend a lot of money?
-I used to spend a lot of money since=porque I lived in a very expensive city
"

I used to spend a lot of money since I lived in a very expensive city;"
Solia gastar mucho dinero porque vivia en una ciudad muy cara

Mal: Solia gastar mucho dinero *desde que* vivia en una ciudad muy cara
"

¿Que tan bien mike solia tocar el piano? =Mike solia tocar el piano bastante bien;"
-How well did Mike use to play the piano?
=Mike used to play the piano quite well
"

¿Quienes se solian quitar los zapatos en la puerta? =Esos niños solian quitarse los zapatos en la puerta;"
-Who did used to take off the shoes at the door?
-Who used to take off their shoes at the door?
-Those kids used to take off their shoes at the door
"

They use to meet on a coffee shop near their house when they started to date (Corrige y crea pregunta);"
(?)Where did they use to meet?
(=)They [used] to meet at a coffee shop near their house when they started to date
"

He used to taught almost eighty students when he were a teacher (Corrige y crea pregunta);"
(?)How many students did he use to teach when he was a teacher?
(=)He used to [teach] almost [eight] students when he [was] a teacher
"

My grandpa used pour in his coffee a lot of sugar (Corrige y crea pregunta);"
(?)How much sugar did your grandpa use to pour in his coffe?
(=)My grandpa used [to] pour in his coffee a lot of sugar
"

Those model used to train every day to keep fit (Corrige y crea pregunta);"
(?)How often did those models use to train to keep fit?
(=)Those [models] used to train every day to keep fit
"

I used to eating pancakes with powdered milk since my grandma makes them delicious (Corrige y crea pregunta);"
(?)Why did you use to eat pancakes with powdered milk?
(=)I used to [eat] pancakes with powdered milk since my grandma [made] them delicious
"

My husband use to pick up the childrens in the afternoon? (Corrige y crea pregunta con who);"
(?)Who used to pick up the children in the afternoon?
(=)My husband [used] to pick up the [children] in the afternoon[]
"

Amanda used to works really hard (Corrige y crea pregunta[How hard]);"
(?)How hard did Amanda use to work?
(=)Amanda used to [work] really hard
"

My parents use to watch TV in the night (Corrige y crea pregunta);"
(?)When did your parents use to watch TV?
(=)My parents [used] to watch TV [at] the night
"

She used to visited the Eiffel Tower when she was in Paris (Corrige y crea pregunta);"
(?)What did she use to visit when she was in Paris?
(=)She used to [visit] the Eiffel Tower when she was in Paris
"

I used to buys those shirts at the central shopping mall (Corrige y crea pregunta);"
(?)Where did you use to buy those shirts?
(=)I used to [buy] those shirts at the central shopping mall
"

#--------

¿Ellos donde se solian reunir? =Solian reunirse en la cafeteria cerca de su casa;"
Where did they use to meet? =They used to meet at a coffee shop near their house
"

¿A cuantos estudiantes el le solia enseñar? =El solia enseñarle a ochenta estudiantes cuando era maestro;"
How many students did he use to teach? =He used to teach almost eight students when he was a teacher
"

¿Cuanta azucar tu abuelo le solia vertir en su cafe? =El solia vertir mucha azucar en el;"
How much sugar did your grandpa use to pour in his coffe? =He used to pour lot of sugar into it
"

¿Esos modelos cada cuanto solian entrenar para mantenerse en forma? =Ellos solian entrenar todos los dias;"
How often did those models use to train to keep fit? =They used to train every day.
"

¿Por que solias comer panqueques? =Yo solia comerlos porque mi abuela los hacia muy deliciosos;"
Why did you use to eat pancakes? =I used to eat them because my grandma made them delicious
"

¿Quien solia recojer los niños por la tarde? =Mi esposo solia recojerlos;"
Who used to pick up the children in the afternoon? =My husband used to pick up them
"

¿Que tan duro solia trabajar Amanda? =Amanda solia trabajar muy duro;"
How hard did Amanda use to work? =Amanda used to work really hard
"

¿Tus padres cuando solian trotar? =Ellos solian trotar en la noche;"
When did your parents use to jog? =they used to jog at the night
"

¿Donde solias comprar esas camisetas? =Solia comprarlas en el centro comercial;"
Where did you use to buy those shirts? =I used to buy them at the mall
"
```

### 8. There used to be in affirmative, negative, interrogative, and answers

- [Fuente](https://www.pacho8a.com/ingl%C3%A9s/curso-ingl%C3%A9s-intermedio/lesson-8/)

```r
Solia haber;"
there used to be
"

(+,-,?,=)Solia haber muchos carros en este condominio;"
(+)There used to be many cars in this condo
(-)There didn`t use to be many cars in this condo
(?)Did there use to be many cars in this condo?
(=)Yes there did / No, there didn´t
"

(corrige) in the past did there use to be many cars in this condo?;"
Did there use to be many cars in this condo [in the past]?

Es importante seguir la estructura de las preguntas, ya que deben de
empiezar por el auxiliar.
"

¿Cuanta comida solia haber en estos estantes? =Solia haber mucha comida;"
How much food did there use to be on these shelves?
=There used to be many food
"

¿Cuantos niños solia haber en este parque? =Solia haber cientos de niños jugando en este parque de juegos;"
How many children did there use to be in this playground?
=There used to be hundreds of children playing in this playground
"

Hace mucho tiempo, solía haber más gente en esta ciudad;"
Long time ago, there used to be more people in this town
"

¿Cuántos de estos coches había hace cincuenta años?;"
How many of these cars did there use to be fifty years ago?
"

En el pasado, no solía haber patos en este lago, ahora hay cientos;"
In the past, there didn’t use to be ducks in this lake, now there are hundreds
"

Antras el tiempo, no solía haber gente en el techo de la estación;"
Back in time, there didn’t use to be people on the roof of the station
"

Hace mucho tiempo, solía haber varios trenes, ahora solo hay uno;"
Long ago, there used to be several trains, now there’s only one
"

Hace mucho tiempo, no solía haber tantos edificios en la ciudad de Nueva York;"
Long time ago, there didn’t use to be so many buildings in New York City
"
```

### 9. Difference between There used to be, There was, and there were

[Fuente](https://www.youtube.com/watch?v=mdiQh4s9xCg)

```r
Diferencias entre ´there used to´ y ´there was/were´;"
-There used to be
  -Algo que existio en el pasado (no en el presente)
  -Enfasis en la duracion de tiempo
  -Solo suele indicar un aprox. o rutina de cuanto ocurrio(a)

-There was(singular)/were(plural)
  -Algo que paso y podria pasar de nuevo
  -No tiene enfasis en la duracion del tiempo
  -Comunmente indica cuando ocurrio, pero igual no.

-Solia haber muchas personas en este pueblo hace algunos años
  -There used to be many people in this town some years ago.
  -Estamos dando a entender que no habra personas de nuevo en este pueblo

-Habia muchas personas en este pueblo hace algunos años
  -There were many people in this town some years ago.
  -Estamos dando entender que podria volver a haber personas

-Solia haber un gato en el techo cada mañana
  -There used to be a cat on the roof every morning
  -Damos a entender que el gato ya no estara porque murio o algo

-Habia un gato en el techo O(esta mañana) X(cada mañana) 
  -there was a cat on the roof X(every morning) O(this morning)
  -Damos a entender que el gato podria volver a aparecer
  -X(cada mañana): Suena mal, no esta claro si, si pasara de nuevo o no.
"

1.¿Solia haber dinosaurios en este bosque? 2.¿Habia dinosaurios en este bosque? (indica cual podria estar mal);"
1. Did there use to be dinosaurs in the forest?
2. Were there dinosaurs in the forest?

Podria estar mal la 2, porque estamos dando entender que podria haber
dinosaurios de nuevo.
"

"
1.Hace unos pocos años antes no solia haber tantos profesores en esta universidad. 
2.Hace unos pocos años antes no habia tantos profesores...
(traduccion y explica diferencia de contexto)";"
1.A few years ago there didn´t use to be so many teachers in this university
2.A few years ago There weren't so many teachers ...

Son muy parecidas, pero podria desirse que, la 1 indica que no volvera
a haber pocos profesores, y la 2 indica que podria haber otra vez pocos
profesores, pero creo buscarle diferencias en estos casos seria complicarse
o en contextos muy espesificos/precisos
"

```

### 10. Writing exercise by translating from Spanish to English

[Fuente](https://www.pacho8a.com/ingl%C3%A9s/curso-ingl%C3%A9s-intermedio/lesson-10/)

```r
Hace unos años, no solia haber tantas profeciones como lo hay hoy en dia;"
Some years ago, there didn`t *use* to be so many prefessions *as* there are nowadays
"

Mi abuelo, quien trabajo para el gobierno por mas de tres decadas;"
My granpa, who worked for the goverment for more *than* three decades
"

El solia aconsejarme que estudiara algo en lo cual pudiera tener estabiliad;"
He used to *advise me to* study something *in which* I could have stability
"

Y formar una familia de la misma manera que el lo hizo;"
And *raise* a family *in* the same way he did *it*
"

Como mi abuelo, todavia hay muchas personas que piensan lo mismo;"
Like my Granpa, there are *still* many people *who* think the same

there are *still*: Tambien se puede `*still* there are`, pero es mas
correcto/comun husar el still al final.
"

Porque fueron criados/educados bajo las mismas creencias;"
Because they *were* raised under the same beliefs
"

Estudiar es importante, pero no especialmente para conseguir un trabajo especifico;"
*Studing* is important, but not specially to get a spesific job

*Study*: es un verbo, *Studing* es un sustantivo
"

Un trabajo en el que puedas pasar el resto de tu vida, a menos que asi lo quieras;"
A job in *X(that) O(which)* you can *spend* the rest of your life, *X(at least) O(unless)* you want it [so|in that way]

*X(that): Creo igual se puede pero `that` es usado para algo muy abierto
y en este caso estamos hablando de un trabajo, es decir, una seleccion
de una coleccion, el cual queda mejor ´which´.

X(at least): por lo menos, al menos, siquiera
"

Estudiar le permite a la gente abrir sus mentes a un nuevo mundo;"
Studying *allow* to people *to* open their *minds* to a new world

let/allow: creo da igual pero es mas comun usar allow con un gerundio
mints=mentas
"

Un mundo que requiere personas capases, que compartan sus conocimientos con otros;"
A world that requires capable people, *X(which) O(who)* share their knowledge with others.

*X(which)*: Estamos hablando de personas, no se cosas.
"

Y luchen por alcanzar sus sueños;"
And struggle to reach their dreams
"

El mundo y la sociedad han cambiado notablemente;"
The world and the society have *changed* notably
"

y es por eso que la gente ha comenzado a buscar diferentes profesiones u ocupaciones;"
And that's why people *have started* to look for different professions *or* occupations
"

Ya que hay un montón de necesidades que no fueron cubiertas antes;"
Since there are *a lot of* needs that *weren’t* covered before
"

Aprecio los consejos de mi abuelo, pero ahora muchos de ellos están desactualizados;"
I appreciate my grandfather's pieces of advice, but now many of them are outdated
"
```

### TEST 1 - INTERMEDIATE

[Fuente](https://www.pacho8a.com/ingl%C3%A9s/curso-ingl%C3%A9s-intermedio/test-1-intermediate/)

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 1, seccion separador seccion, separar seccion

.

#############################################################################################################
#############################################################################################################

-->

----

----

----

<!-- 

.

.

#############################################################################################################
#############################################################################################################

.

divicion seccion, divicion de seccion 2

.

#############################################################################################################
#############################################################################################################

-->

## Extras

### Cuando usar TO antes de un verbo y cuando NO usarlo y diferencias con el FOR

```r
# uso de to, como usar to, cuando usar to, cuando no usar to
I want TO eat, we can swim ¿por que si tenemos dos verbos en infinitivo (comer y nadar), uno usa el TO y el tro no?;"
Porque can es un verbo modal, y los verbos modales no deben llevar delante el to
"

Traducciones de TO, y uso con ejemplos, sin contar para el infinitivo;"
Trad: a, para

Uso: señalar transferencia o movimiento
.  -to + lugar
.  -to + verbo infinitivo
.  -to + alguien/algo

Ejemplos:
.  -I go TO my place (lugar)
.  -You are in the university TO learn (v. infinitivo)
.  -He bought a present to you (alguien)
"

He bought a present __(to/for) you, explica el porque;"
He bought a present to you

Porque: se usa to para hacer referencia a alguien y for para razon/intencion o beneficio
"

I came __(to/for) see you, explica el porque;"
I came to see you

Porque: Se usamos un verbo en infinitivo (verte)
"

Traducciones de FOR, y uso con ejemplos;"
Trad: por, para

Uso: señalar Razon, Intencion o Beneficio (R.I.B)
.  -for + (R.I.B)
.  -for + Verbo gerundio (ing)
.  -for + alguien/algo

Ejemplos:
.  -I go to the gym FOR my healt (R.I.B.)
.  -You are in the university FOR learning, (Verbo gerundio)
.  -This is for you (Esto es X(PARA) O(POR) ti)(alguien)
.  -I stopped the car FOR some coffe (R.I.B.)
"

1.You are in the university __(for/to) learn / 2.You are in the university __(for/to) learning, explica el porque;"
1. You are in the university TO learn
2. You are in the university FOR learning

Porque: 1.Usamos un verbo en infinitivo (aprender). 2.Estamos pasando
un verbo a una cosa.
"

Trad. y Diferencias entre 1.(He bought a present TO you) y 2.(He bought a present FOR you);"
Trad
.  1.El compro un regalo para ti
.  2.El compro un regalo por ti

Diferencias
.  1.El regalo lo compro para dartelo
.  2.El compro el regalo por ti, (lo tenias que comprar tu pero lo compro el)
"

Trad. y Diferencias entre 1.(This is FOR you) y 2.(This is TO you);"
Trad
.  1.Esto es para ti (dar, trasferencia)
.  2.Esto es por ti (intencion, beneficio, razon)
"

#-----

1.Went to Canada __(for/to) see my Sister (explica y que cambiaramos para que fuera el for);"
to

Porque: Uso de verbo infinitivo

Cambiariamos para poder usar for, el verbo `see` por `seeing`.
"

2. This lesson was made __(for/to) you (explica);"
for

Porque: `por ti` señalamos (Reazon.I.B.)
"

3. Susan travel __(for/to) london __(for/to) a conference (explica);"
To, for

Porque: To, para señalar un lugar/movimiento, for, la razon
"

4. This is impartant __(for/to) me (explica);"
for

Porque: Señalamos beneficio
"

S. Juan cooks dinner __(for/to) his wife (explica);"
for y to

Porque: Señalamos razon
"

6. Read a story __(for/to) the children (explica);"
to

Porque: (al niño) señalamos a quien leer la historia
"
```

### Aprende a diferenciar OF y FROM con estos 8 ejemplos

```r
Traducciones de `of` y ´from´ y sus usos;"
of - de - pertenencia
from - de/desde - pertenencia, lugar, tiempo
"

1. The laptop __(of/from) my father;"
of
"

2. She is __(of/from) Australia;"
from
"

3.The President __(of/from) France;"
of
"

El presidente de Francia=France`s president (corrige y por que esta mal);"
The President of France.

El XXX`s solo aplica para seres vivos, en este caso Fracia no es un ser vivo.
"

4. We Came __(of/from) the Paler´s house;"
from
"

5.The friend __(of/from) Reina comes __ the gym;"
of, from
"

6 Ana woked __(of/from) her house;"
from
"

7.The toys __(of/from) my pets are __ China;"
of, from
"

8.I study English __(of/from) 8am to llam;"
from
"
```

### Así de fácil es usar ALREADY, STILL y YET

```r
Diferencias entre ALREADY, STILL y YET;"
ALREADY = ya, (+,?), (inicio de, conclucion de).

Yet = Aun, todavia, ya, (-,?),

Still =Todavia, aun (pregreso) (+,-,?)
"

# uso del already y diferencias con yet
Already, traduccion, posicion, ¿usado en oraciones ?s, +s y -negativas?;"
Traduccion: Ya

Posicion
.  -Despues de `to be`
.  -Al final de oraciones
.  -Entre aux. y verbo principal, modales o tradicionales

¿usado en oraciones?: Positivas e interrogativas
"

We already are in class (corrige y explica);"
We are already in class = We are in class already

El aready no puede ir antes del verbo to be, el already va despues del
verbo to be o al final, en este caso
"

We are in class yet (corrige y explica);"
We are in class already

El yet solo se usa en oraciones interrogativas
"

Are we yet in class? (corrige y explica);"
Are we already in class? = Are we in class yet?

El `yet` solo puede ir al final de la oracion
"

(+,?)Ellos ya han visto este tema;"
(+)They have already seen this topic
(?)Have they already seen this topic?
"

# uso de yet
yet, traducciones, posiciones, ejemplos, proninciacion, ¿se usa en oraciones (+,-,?)?;"
proninciacion
.  -/yiet/

posiciones / traducciones
.  -Al final de la oracion / Aun, todavia, ya
.  -Entre oraciones(conjuncion) / pero, sin embargo

¿se usa en oraciones?: 
.  -Negativas, interrogativas

Ejemplos
.  -Ella no ha entendido todavia
.    -She hasn´t understood YET
"

1.Has she undertood yet?/2.Hasn`t she undertood yet? (explica diferencias de traduccion);"
1.Ella ya ha entendido?
2.Ella todavia no ha entendido?

En oraciones ?s yet se traduce como `ya` y en negativas como `todavia/aun`
"

Ella no ha entendido todavia, pero(usar yet) ella todavia esta practicando;"
She hasn`t undertood yet, yet she is still practicing
"

# uso de still
still, traduccion, posicion, ¿usado en oraciones +,-,??, ejemplos;"
posiciones / traducciones
.  -Despues del to be / todavia
.  -Antes del auxiliar o modal / aun, todavia (algo aun no realizado)
.  -Entre oraciones(conjuncion) / aun asi

¿se usa en oraciones?: 
.  -(+s, -s y ?s)

Ejemplos
.  -We are still watching this video
"

Todavia no puedo comprarlo (con still y yet);"
I still can´t buy it
I can´t buy it yet
"

¿Estas todavia alla?;"
Are you still there?
"

1. Ella ya nos había llamado cuando tu llegaste;"
She had already called us when you arrived
"

2. Tú no me has dado el dinero aún, sin embargo todavía tienes tiempo;"
You haven´t *given* me the money yet, yet/however you *still* have time
"

3. Yo tengo que salir pero todavía está lloviendo;"
I have to leave but it's still raining
"

4. Mi amigo Walter ya ha visitado 20 paises, pero él todavía quiere visitar muchos más;"
My friend Walter has already visited 20 countries, yet he still wants *to* visit many more
"

5. ¿Ya encontraste tus llaves o todavía las estás buscando?;"
*Did* you already found your keys or are you still searching them?
"

6. ¿Ya tomaste un taxi? (Utiliza Yet);"
*Did* you take a taxi yet?
"

7. Yo todavía estoy enamorado de ti, pero yo no quiero casarme contigo todavía;"
I'm still in love *X(of) O(with)* you, yet I don´t want to marry you yet
"

8. No podemos viajar aún, nuestro avión no ha llegado todavía;"
We can´t travel yet, our plane *hasn't* arrived yet
"

9. Mi hija ya ha terminado su tarea. Mi hijo no la ha terminado todavía. Él aún sigue leyendo;"
My daughter has already finished her homework. My son hasn`t finished yet. He is still reading
"

10. Mi esposa y yo todavía vamos al cine los fines de semana. Ya hemos visto muchas películas juntos;"
My wife and I still go to the cinema on weekends, We have already watched/seen many movies together
"

11. ¿Todavía estás trabajando en esa empresa? ¿No has renunciado todavía?;"
Are you still working in that company? *haven't* you quit yet?
"
```

### Aprende a usar el verbo GET en inglés con estas 3 bases

```r
#uso de get
Tres formas de trabajar el verbo get;"
1.Por definicion, es decir cuando encontramos un verbo que puede ser
sustituido por el get.

2.Adjetivo, Cuando queremos indicar un cambio de estado, ej. mi papa
se esta envejeciendo=Mi dad is getting old, podria indentificarse por
el uso de `se` en español.

3.Phrasal verb: (verbos conformados por dos palabras), get+preposicion,
ó get+adverbio
"

Lista de verbos que pueden traducirse como get;"
-Son mas, pero podemos inferirlos por sus sinonimos
-Obtener
-Conseguir
-Tener
-Entender
-Comprar
-LLegar
-Buscar
-Recibir
-Tener
-Coger/tomar
"

Lista de adjetivos con los que puede ir antes el get por cambio de estado;"
-old
-drunk
-wet
-fat
-dark
-dizzy
-Y otros
"

lista de preposiciones y adverbios que se puede usar antes el get;"
-prepositions
. -get by = arreglarselas, salir adelante, conseguir
. -get out = salir, escaparse
. -get into = meterse en
. -get across = atravesar, pasar, hacer entencer
. -get along = llevarse bien

-Adverb
. -by
. -together
. -along
. -out
. -across

-Y otros
"

#Por definicion
Ella esta tomando un taxi (usar get, por definicion);"
She is taking/getting a taxi
"

Tengo un problema (usar get, por definicion);"
I get a problem
"

Tengo un problema (usar get, por definicion);"
I get a problem
"

¿Recibiste las manzanas?(usar get, por definicion);"
Did you get the apples?
"

Yo no entiendo este verbo(usar get, por definicion);"
I don´t get this verb
"

Ellos quieren llegar al parque(usar get, por definicion);"
They want to get to the park
"

#Adjetivo
Ellos se emborracharon (usar get, por adjetivo);"
They got drunk
"

Mis amigos se mojaran en el concierto (usar get, por adjetivo);"
My friends will get wet in the concert
"

Patricia engordo (usar get, por adjetivo);"
Patricia got fat

Patricia get fat = entricia engordar
"

El dia se esta oscureciendo(usar get, por adjetivo);"
The day is getting dark
"

Me estoy mareando (usar get, por adjetivo);"
I`m getting dizzy
"

#Phrasal verb
ellas se las arreglaron y ganaron la carrera (usar get, por Phrasal verb);"
They GOT BY and win the racing
"

Yo me inscribi/matricule/meti a clases de ingles (usar get, por Phrasal verb);"
I GOT INTO English class
"

Nosotros nos llevamos bien (usar get, por Phrasal verb);"
We GET ALONG well
"

Ellos se junstaran/reuniran a trabajar (usar get, por Phrasal verb);"
They will GET TOGETHER to work
"

Por favor sal de aca (usar get, por Phrasal verb);"
Please, GET OUT of here
"

1. Yo conseguí mi pasaporte la semana pasada (usar get);"
I got my passport last week
"

2. Ella recibió mi mensaje cuando yo la llamé (usar get);"
She got my message when I called her
"

3. Nosotros nos estamos quedando calvos (usar get);"
We are getting bald
"

4. Ellos no se llevan bien, pero trabajan juntos (usar get);"
They don’t get along well, but they work together.

Se puedeomitir el well
"

5. ¿Puedo llegar a Londres en tren? (usar get);"
Can I get to london by train?
"

6. El chocolate se derritió bajo el sol (usar get);"
The chocolate got *melted* under the sun
"

7. Yo no entendí el chiste (usar get);"
I didn´t get the joke
"

8. Usted necesitará recuperar su dinero pronto (usar get);"
You will need *to* get back your money soon
"

9. Mi esposa obtuvo un aumento en su trabajo (usar get);"
My wife got a raise in her job
"

10. Él recorre muchos lugares todos los días (usar get);"
He *gets* around many places every day
"

11. El perro se asustó con los fuegos artificiales (usar get);"
My dog got scared with the fireworks.
"

12. Ella necesita salir de esa situación (usar get);"
She needs to get out of this situation
"

13. ¿Puedes traer mis zapatos por favor? (usar get);"
Can you get my shoes please
"

14. Charlie no pudo alcanzar las cajas, pero consiguió una silla (usar get);"
Charlie can`t got at the boxes, but he got a chair
"

15. Mi hermana Glen era gorda, pero ahora se está poniendo delgada (usar get);"
My sister Glen was fat, but now she is getting thin
"

16. Yo sé que perdiste a tu madre, pero tienes que superarlo (usar get);"
I Know you *lost* your mother, but you have to get it over 

I know that you lost...
"
```

### Explicación detallada de la VOZ ACTIVA y PASIVA en inglés - Passive Voice

```r
voz activa, ¿Que es?, estructura, ejemplos;"
¿Que es?
.  -Resalta el sujeto (quien realiza la accion)

Estructura
.  -Quien Hace + Accion(tiempo) + Quien recibe

Ejemplos
.  -Pacho graba el video
"

Que es la voz pasiva;"
¿Que es?
.  -Resalta el objeto (quien resive la accion)

Estructura
.  -Quien Recive + to be(tiempo) + Ve.Part + By + Quien hace/Pron. Obj.

Ejemplos
.  -El video es grabado por Pacho
"

1.Pacho graba el video 2.El video es grabado por Pacho, (traduce (presente,pasado,futuro), señala cual es vos activa y cual pasiva);"
1.Voz activa
.  -(present)Pacho records the video
.  -(past)Pacho recorded the video
.  -(future)Pacho will record the video

2.Voz pasiva
.  -(present)The video is recorded by Pacho
.  -(past)The video was recorded by Pacho
.  -NOTA: No olvidar que estamos usando (recorded) el pasado participio.
.  -(futuro)The video will be(sera) recorded by Pacho
"

Michael Jackson canta Billy Jean / Billy Jean es cantada por Michael Jackson, (voz activa vs pasiva);"
Michael Jackson sings Billy Jean / Billy Jean is sung by Michael Jackson
"

La casa esta siendo pintada por ellos / Ellos estan pintando la casa;"
The house is being painted by them / They are painting the hause
"

Mi madre ha preparado la cena / La cena ha sido preparada por mi mama;"
My mom has prepared the dinner / The dinner has been prepared by my mom
"

1.me roboron la billetera (traduce a ingles en voz pasiva);"
My wallwt was stolen by them
"

4. La pelota sera encontrada por el perro (traduce a ingles en voz activa);"
The dog will find the ball
"

5.El profesor ha calificado los examenes (traduce a ingles en voz pasiva);"
The exams *have been graded* by the teacher
"

7.Jhon estuvo escribiendo el libro (traduce a ingles en voz pasiva);"
The book *was being* written by John

El libro esta siendo escrito por Juan
"

Our cat killed the bird (traduce a ingles en voz pasiva);"
The bird was killed by our cat
"

The teacher will not correct these exams(traduce a ingles en voz pasiva);"
These exams won´t be corrected by the teacher.
"

The company might not hire my brother(traduce a ingles en voz pasiva);"
My brother might not be hired by the company
"

Underage kids should not watch that film(traduce a ingles en voz pasiva);"
That film shouldn´t be watched by underage kids.
"

A man called Francesco invented one of the first typewriters in 1575(traduce a ingles en voz pasiva);"
One of the first typewriters was invented by a man called Francesco in 1575

El orden puede variar
"

The Puritan government banned Christmas in Massachusetts in 1659(traduce a ingles en voz pasiva);"
Christmas was banned by the Puritan government in Massachusetts in 1659
=
Christmas was banned in Massachusetts in 1659 by the Puritan government
"
```

### Uso del artículo THE en inglés - Cuando NO usarlo

```r
the, pronunciacion, Traduccion, uso;"
Pronunciacion
.  -/de/ ó /da/

Traduccion
.  -el, la, los, las

Uso de the
.  -Para hablar de algo que se conoce o se conocera pronto, es decir ya
.  se definio en la oracion o en el contexto ej. He is the teacher
"

the, cuando no usar;"
Titulos de personas
.  -Mr. /mister/
.  -Ms. /miss/ = no sabemos si esta casada
.  -Mrs. /mises/ = casada
.  -Miss = señorita
.  -Dr.
.  -Etc.
.  -Ejemplo: O(Mr. Ochoa is here) X(The Mr. Ochoa is here)

Cosas en general
.  -She likes X(the) chocolate pies
.  -I don´t like X(the) vegatables (vegetales en general)
.    -I don't like THE vegetables that are on that table (unos vegetales especificos)

Dias de la semana
.  -Monday, etc
.  -We rest on fridays
.    -Incorrecto: We rest X(the) fridays

Idiomas
.  -Incorrecto I like the English.
.  -Correcto I like English.

Posesivo Sajon
.  -Incorrecto: The Juan´s house
.    -Correcto: Juan´s house

Momentos del año
.  -Yo fui a francia el mes pasado
.    -Incorrecto:I went to France THE last month
.    -Correcto  :I went to France last month
"

El señor Ochoa esta aca;"
Mr. Ochoa is here

Incorrecto: The Mr. 
"

La señorina Colombia es linda;"
Miss. Colombia is pretty
"

A ella le gustan los pasteles de chocolate;"
She likes chocolate pies

Incorrecto: the choco...
"

No me gustan los vegetales / No me gustan los vegetales que hay en la mesa;"
-I don´t like vegatables/betchtabols/
.  -Incorrecto: the vegatables, porque hablamos en general

- I don´t like THE vegetables that are on that table
.  -podemos usar the porque hablamos en especifico.
"

Nosotros descansamos el viernes;"
We rest on fridays

Incorrecto: We rest THE fridays
"

Yo quiero aprender el Ingles;"
I want to learn English

Incorrecto: I want to learn THE English
"

The Spanish is difficult (Corrige y explica);"
Correccion: Spanish is difficult

Porque: El `the` no se usa en idiomas y/o cosas en general, titulos
personales, o dias de la semana
"

The mother`s day (Corrige y explica);"
Mother`s day

Porque: El posesivo sajon ya incluye el `el` si dejamos el `the`
entonces de traduciria como `El el dia de las madres`
"

The exam is on the next week (Corrige y explica);"
The exam is on next week

El `THE` no se puede usar para partes del año.
"

We will go in the summer (Corrige y explica);"
We will go in summer

El `THE` no se puede usar para partes del año.
"

Nosotros iremos en el veramos;"
We will go in summer

Incorrecto: We will go in THE summer
"
```

### Cuándo usar y diferenciar WAS – WERE – DID – Pasado to be VS Pasado Simple

```r
Diferencias entre el was/were y did;"
Pasado to be
.  -was/were = Era, estaba

Pasado Simple
.  -Did = Auxiliar

Si la oracion en pasado tiene 'era' o 'estaba' entonces lleva was/were,
si no entonces lleva did
"

¿Estabas en el supermercado?;"
Were you in the super?

Incorrecto: did you were in the super?
"

Erika didn´t undertand this topic / Erica wasn´t undertanding this topic;"
Erika no entendia este tema / Erica no estaba entendiendo este tema
"

¿Donde estaban ellos anoche?;"
Where were they last night?
"
```

### Uso de It

```r
1.It´s a pleasure / 2.It´s been a pleasure (traduce y explica diferencia);"
1.Es un placer
2.Ha sido un placer

En la 1 significa `It is` y en la 2 significa `It has`
"

1.Es un placer / 2.Ha sido un placer;"
1.It´s a pleasure
2.It´s been a pleasure
"

Me gusta;"
I like it

No olvidar el `it`
"

The restaurant open his doors tommorrow (Corrige y explica);"
The restaurant open its doors tommorrow

Porque: ´his´ es para masculinos no para cosas como restaurantes.
"

El restaurante abre sus puertas mañana;"
The restaurant open *its* doors tommorrow
"

"
+)The restaurant open his doors tommorrow
+)The restaurant open their doors tommorrow
+)The restaurant open its doors tommorrow";"
its
"
```

### Condicionales

#### Condicional 0

```r
Que es el codicional cero en ingles, y ejemplo;"
Condicionales para hechos reales, cientificos o probables.

Ej.
.  -Si abres la ventana, se metera el aire. (real)
.  -Si tu estudias, dominaras x tema. (probable)
.  -Si enfiras mucho el agua, se conjela. (cientifico)

Solo cabe en los afirmativos y negativos, no para afirmativos
"

Si pagas por adelantado, te damos un descuento especial;"
If pay in advance, we give you a special discount
"

Si ella viaja en septiembre, el tiquet cuesta menos;"
If she travels in september, the ticket costs less
"

Si Liz no practica, no aprende;"
If liz doesn´t practice, she doesn´t learn
"
```

#### Condicional 1

```r
Que es el codicional uno en ingles, ejemplo y estructura;"
¿Que es?
.  -Condicionales de probabilidades futuras

Estructura
.  Condicional(PS) + resultado(FS) ó VM

Ejemplos
.  If you study you will learn
.  If he doesn't study, he won't learn

PS=presente simple
FS=Futuro simple
VM)Verbo modal
"

Si ella va yo debo ir;"
If she goes, I must to go
"

Si ella no va, yo no deberia de ir;"
If she goes, I shouldn´t go
"
```

#### Condicional 2

```r
Para que es el codicional 2 en ingles, estructura y ejemplo;"
Para que
.  -Para condicionales himprobables o hipoteticos

Estructura
.  -Condicion(PS) + would ó modal + verboBase 
.  -PS=Pasado simple

Ejemplo
.  -Si tuviera dinero, compraria un helado
.    -If I had money, I would buy an ice cream
.  -Si Pacho supiera Frances, el te enseñaria
.    -If Pacho Knew French, he would teach you
.    -If Pacho Knew French, he could teach you

Nota Importante:
.  -En condicionales siempre se usa el were, aunque el sujeto le vaya el was.
.  -Ejemplos:
.    -If I was|were in Paris, I would take many photos
.    -Notar que en otras estructuras iria a fuerza el was, pero cuando
.     usamos condicionales se usa el were
"

Si yo estubiera en Paris, Tomaria muchas fotos;"
If I was|were in Paris, I would take many photos

was|were: Creo formalmente va el were, pero tambien es muy usado el was
"

Si ellos no entrenaran, no ganarian el campeonato;"
If they didn`t train, they wouldn´t win the champion
"

Yo no estaria viendo este video, si no estuviera aprendiendo ingles;"
I wouldn´t be watching this video, If I wasn´t|weren´t learning English

wasn´t|weren´t: Creo formalmente va el were, pero tambien es muy usado el was
"

Nosotros no podriamos ayudarte, si estuvieramos ocupados;"
We couldn´t help you, if we were busy
"

1. Si yo fuera millonario, viajaría alrededor del mundo;"
If I was millionaire, I would travel around the world
"

2. Si Johnny viajara a España, Yo iría con él;"
If Johnny traveled to Spain, I would go with him
"

3. Nosotros no estaríamos trabajando hoy si fuera día feriado;"
We wouldn´t be working today if it was holiday
"

4. Lucía visitaría a su amiga Miriam si ella no estuviera trabajando hoy;"
Lucia would visit her friend Miriam if she weren´t working today.
"

5. Jorge no podría jugar futbol si no tuviera un balón;"
Jorge couldn´t play soccer if he didn´t have a ball.
"

6. Si yo fuera tú, compraría ese carro nuevo;"
If i were you, I would buy that new car

Tambien se puede `If I was you,`
"

7. Ustedes no harían eso si estuvieran en mi lugar;"
You wouldn´t do that if you were in my place
"

8. Si no tuviéramos que trabajar hoy, iríamos a cine en la tarde;"
If we X(weren´t) O(didn´t) have to work today, We would go to the cinema in the afternoon
"

9. Si ellos ganaran la carrera, recibirían un premio;"
If they won the race, they would get a prize
"

10. Si él estuviera en el supermercado, compraría muchas cosas;"
If he were in the supermarket, he would buy many things

Tambien se puede> 'If he was in the ...'
"
```

#### Condicional 3

```r
Para que es el codicional 3 en ingles, estructura y ejemplo;"
Para que
.  -Para condicionales imposibles o que nunca ocurrieron

Estructura
.  -             Condicion               +             Resultado
.  -[If + Pronombre + Had + VerboPasPar] + [Pronom + modal + have + verbPas]
.
.  - La condicion debe estar en pasado perfecto
.  - El resultado debe estar en presente perfecto
.    -Modales: would, could, should

Ejemplo
.  -If I had studied, I would have passed the exam
.    -Si yo hubiese estudiado, yo habria pasado la prueba
.  -They could have fixed the car, if they had had the tools
.    -Podrian haber arreglar el carro, si ellos hubiesen tenido las herramientas

Nota:
.  -Podemos relacionar la estructura con {PomoHadVerbPP} para el condicional,
.   y {PromoHaveErp} para el resultado. Y asi memorizarlo
"

Si ellos hubiesen pagado mas, yo habria trabajado mas horas;"
If they had paid more, I would have worked more hours
"

Tu no podrias haber hecho el ejercicio si no hubieses practicado;"
You couldn´t have done the exercise if you hadn´t practiced
"

1. Si tu no hubieses creído en mí, yo no lo hubiera hecho;"
If you hadn´t believed in my, I wouldn´t have done it
"

2. Si yo no hubiese gastado mi dinero, podría haber comprado ese helado;"
If I hadn´t spent my money, I could have bought that ice cream
"

3. El atleta podría haber ganado la carrera si hubiese corrido más rápido;"
The athlete could have won the race if he had run faster.
"

4. Si Any hubiese comido carne, ella no habría comido pollo;"
If Any had eaten meat, she wouldn´t have eaten chicken
"

5. Nosotros hubiéramos ido caminando si el perro no hubiese perseguido el gato;"
We would have gone walking if the dog hadn´t chased the cat
"
```

### Stative Verbs – Definición, Ejemplos y Lista

```r
Que es un stative Verb;"
Es un verbo que NO hace referencia a una accion, mas bien hace referencia
a un estado o situacion, que es o existe.
"

Que categorias manejan los stative verbs;"
(No es algo que suceda o haga, es un estado o situacion)

-Emotional State
.  -Love: I love you
.  -Dislike
.  -Like
.  -Care
.  -Have: Your friends hate me
.  -Envy

-Mental State
.  -Know: We Know him
.  -Realize
.  -Understand
.  -Suuppose
.  -Believe
.  -Think

-Perception state
.  -Taste
.  -Hear
.  -See: I see you at the door
.  -Smell
.  -Feel

-Possession(pertenencia) state
.  -Possess
.  -Have
.  -Own: You own a car
.  -Belong
"

I am loving you (corrige, explica, alternativa);"
I love you

Explica
.  -Love es un stative verb, lo cual suena raro decir 'yo te estoy amando',
.   no es algo que se haga si no un estado o situacion.

Alternativa:
.  -`I'm starting to love you`: aqui si se podria usar como algo que esta
.   sucediendo, y no como un edstado o situacion
"

We are knowing him=Lo conocemos (corrige, explica, alternativa);"
We know him

Explica
.  -Queremos decir que lo conocemos, que es un estado/situacion no una accion,
.   lo cual se escucharia raro `We are knowing him`, podriamos darnos
.   a enternernos mejor con la alternativa, para indicar que apenas lo
.   estamos conociendo

Alternativa/correccion:
.  -We are getting to know him
"

Eduardo is thinking that he is wrong (corrige, explica, alternativa);"
Eduardo thinks that he is wrong

Explica
.  -`Eduardo esta pensando que el esta mal` suena raro ya que pensar no
.   es una accion si no un estado/situacion.


Alternativa/correccion:
.  -Eduardo is starting to think that he is wrong
"

La sopa sabe delicioso=The soup is tasting delicious (corrige, explica, alternativa);"
The soup tastes delicious

Explica
.  -`La sopa esta sabiendo delicioso` suena raro


Alternativa/correccion:
.  -The soup is getting/become delicious: Para indicar que se esta
.   poniendo sabrosa, talvez por que aun se esta preparando.
"

Monica is having a lot of money=Monica tiene mucho dinero(corrige, explica, alternativa);"
Monica has a lot of money

Explica
.  -'Monica esta teniendo mucho dinero' suena raro, si queremos decir
.   que un momento dado/corto esta obteniendo mucho dinero usar alternativa


Alternativa/correccion:
.  -Monica is getting a lot of money
"

You are owning a car=Tu tienes un carro(corrige, explica, alternativa);"
You own a car

Explica
.  -`Tu estas [teniendo/siendo dueño de] un carro`: suena raro, si
.    queremos decir que esta en proceso de obtener un carro, podemos
.    usar la alternativa

Alternativa/correccion:
.  -You are getting a car
"
```

### ¿Qué debo usar? ALONE - ONLY - JUST

Fuente: [youtube](https://www.youtube.com/watch?v=qgItiOmYRzU&t=1s)

```r
Diferencia entre Alone, Only, y Just;"
Alone(Estado/Adjetivo, para personas) = Solo
.
Only = Solo, Solamente, Unicamente
.
Just = Sólo, Solamente, unicamente, Recien, acabado, Justo, Justamente
"

Yo sólo quiero estar solo;"
I only=just what to be alone
"

Ellos acabaron=recien de llegar;"
They just arrived
"

This is [only/just] what i want (Diferencias);"
Only: Es lo unico que necesito: Una cosa en particular
.
Just: Es justo lo que necesito
"

They train just/alone/only train in the gym(Diferencias);"
Just=only: Entrenan solo en el gym.
Alone: Entrenan solos en el gym
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### 10 preposiciones de movimiento que debes aprender - Diferencia IN - INTO y más

```r
Lista de prepociones de movimiento;"
To        : a ..., a la ...
Towards   : Hacia
Up/Down   : Hacia arriba, hacia abajo
Across    : hacia el otro lado de
Into/onto : Hacia adentro, hacia encima
Around    : Al rededor de
Through   : A travez de
Along     : A lo largo de
"


To (Trad y ej, como preposicion de movimiento);"
.  -a ..., a la ... (señalar, direccion, reccorrido)
.  -I'm going TO the downtown
.   -Voy al centro de la ciudad
"

Towards (Trad y ej, como preposicion de movimiento);"
Pronunciacion:
.  -/toger/
_
Traducciones:
.  -Hacia
_
Ejemplo
.  -She goes TOWARD his house
.    -Ella va hacia su casa

Notas:
.  -Toward(americano), Towards(ingles)
.  -Nota: Es sinonimo de `to`
"

Up/Down (Trads y ej, como preposicion de movimiento);"
Traducciones
.  -hacia arriba, hacia abajo
_
Ejemplo
.  -They go down/up
.    -Ellos van hacia abajo/arriba
"

Across (Trad y ej, como preposicion de movimiento);"
Traducciones
.  -hacia el otro lado de
_
Ejemplo
.  -He goes ACROSS the street
.    -El va hacia el otro lado de la calle
"

Into/onto (Trads y ej, como preposicion de movimiento);"
Traducciones
.  -Hacia adentro, hacia encima
_
Ejemplo
.  -She walked INTO the house
.    -Ella camino hacia adentro de la casa
.  -The cat jumped onto the sofa
.    -El gato salto hacia encima del sofa
"

Around (Trad y ej, como preposicion de movimiento);"
Traducciones
.  -Al rededor de
_
Ejemplo
.  -The dog is running around the three
"

Through (Trad y ej, como preposicion de movimiento);"
Traduccion
.  -A travez de
_
Ejemplo
.  -They went through the jungle
.    -Ellos fueron a travez de la junga
"

Along (Trad y ej, como preposicion de movimiento);"
Traducciones
.  -A lo largo de
_
Ejemplos
.  -We´re walking along the beach
.    -Estamos caminando a lo largo de la playa
"

Ellos van hacia arriba;"
They go toward up

Mal: 'They go toward up', el up ya incluye el hacia, por lo que no es
necesario poner el toward=hacia
"

He goes across of the street (corrige y explica);"
He goes across ** the street
_
El `Of` no es necesario ya que across significa `hacia el otro lado de`
"

She walked [in|into] the house (traduce ambas opciones);"
She walked in the house
.  -Ella camino dentro de la casa

She walked into the house
.  -Ella camino hacia adentro de la casa
"

The cat jumped [onto/on] the sofa (traduce ambas);"
The cat jumped onto the sofa
.  -El gato salto hacia encima del sofa
The cat jumped on the sofa
.  -El gato salto sobre el sofa (ya estaba en el sofa y sin salir de este salto)
"

Ellos fueron a travez de la junga;"
They went through the jungle
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Cuándo y cómo usar EVEN - NOT EVEN - EVEN SO - EVEN IF - EVEN THOUGH

```r
Even: ¿Traducciones?, ¿Uso?, ¿Pronuniacion?, ¿Posiciones?, ¿Uso como conjuncion?, ¿Ejemplos?;"
¿Traduccion?
.  -Incluso, hasta aun, hasta aun,
.  -Not even: ni siquiera
_
¿Uso?
.  -Adicionar, enfatizar, comparar, situaciones inesperadas
_
¿Pronuniacion?
.  -Even/iven/
_
¿Posiciones?
.  -Despues del to be
.  -Entre sujeto y verbo
.  -Entre Entre aux|modal y verbo
.  -Para enfasis, al inicio
_
¿Uso como conjuncion?
.  -Even though = Aunque
.  -Even if = Incluso si
.  -Even so = aun asi (contraste)
_
¿Ejemplos?
.  -Eres grosero incluso con la policia
.    -You are EVEN rude to the police
.  -Ella ni si quiera fue a trabajar hoy
.    -She didn´t EVEN go to work today
.  -Incluso mis amigos se han olvidando de mi
.    -EVEN my friends have forgotten me
.  -Seras mi mejor amigo aunque no estemos de acuerdo
.    -You´ll be my best friend EVEN THOUGH we disagree
_
¿Notas?
.  Until = hasta, pero hablando de limite
"

Eres grosero incluso con la policia=You are even rude to the police(corrige y explica);"
You are rude even with the police
_
Until=hasta, pero hablando de limites
"

1.You are even rude [to|with] the police(traduce y explica);"
1.Eres grosero incluso [con|junto con] la policia
.  -To: es para señalar, indicar
.  -with: es para hacer un grupo
"

```

<!--

#########################################

-->

----

<!--

#########################################

-->

### REPORTED SPEECH = INDIRECT SPEECH

[Fuente](https://www.pacho8a.com/ingl%C3%A9s/reported-speech/)

#### Cómo usarlo

```r
Reported Speech=Indirect Speech vs Direct Speech;"
Direct Speech
.  -Es como tal como se dijo una frase

Reported Speech
.  -Es cuando alguien dice una frase por alguen mas

______
Ejemplo
.  -En el DS fue como lo dijo Tom. en el IS alguien mas lo esta contando
.   por Tom
.    -DS: `I was in my house with my wife`, say Tom
.    -IS: He was in his house with his wife

"

He [was|has been] in his house with his wife (Traduce y diferencias);"
was: El estaba en su casa con su esposa
.  -No ha pasado tanto tiempo


has been: El habia estado en su casa con su esposa
.  -Ya ha pasado algo de tiempo


La idea es la misma solo cambia la persepcion de cuando paso el evento
"

`Amanda [has|had] to work tomorrow`, I said (convierte a discurso indirecto, traduce y explica diferencias);"
I said Amanda [has|had] to work tomorrow


Yo dije que Amanda [tiene|tenia] que trabajr hoy


Diferencias: Igual que en español
"

`You [will/would] go with me to the doctor`, Said my mom (convierte a discurso indirecto, traduce y explica diferencias entre will/would);"
DI: My mom say that I [will/would] go with her to the doctor

Trad: Mi mama dijo que yo [ire|iria] con ella al doctor

Diferencia: Igual que en español
"

1.Ella [ha|habia] cantado|  2.Ella [tiene|tuvo] que cantar, 3.ella [cantara|cantaria];"
1.She [has|had] sung

2.She [has to|had to] sing

Nota: Para el tenia se maneja tambien el `had to`, creo por el contexto se diferencian

3. She [will|would] sing
"

1,Iremos, 2,Iriamos;"
1.We will go

2.We would go
"

```

#### Practica Reported Speech

[Fuente](https://youtu.be/RIJcAvSaZGE)

TODO: Hacer varias cartas de todos los tiempos posibles juntos    

```r
"
1.El [era|fue|estaba] feliz
2.El solia ser feliz
3.el habia sido feliz
4.El seria feliz

5.El [esta|es] feliz
-
7.El ha sido feliz
8.El ha estado siendo feliz

9.El sera feliz
10.El va a ser feliz
11.El habrá sido feliz
12.El habrá estado siendo feliz
";"
1.He was happy
2.He used to be happy
3.He had been happy
4.He would be haapy

5.He is happy
-
7.he has been happy
8.he has been being happy

9.He will be happy
10.He is going to be happy
11.He will have been happy
12.He will have been being happy
"

"
1. Yo estaba trabajando el viernes pasado
2. Yo solia trabajar en el gobierno
3. Yo trabaje de policia
4. Yo habia trabajado ese jueves
5. Yo habia estado trabajando mientras paso

6. yo habria trabajado el sabado si el ubiera estado ahi
7. Yo habria estado trabajando con Mike
8. Yo trabajaria si tuviera las herramientas

9. Yo estoy trabajando ahora
10. Yo trabajo en el banco
11. Yo he trabajado de mesero
12. Yo he estado trabajando todo este tiempo

13. Yo trabajare mañana
14. Yo voy a trabajar pasado mañana
15. Yo habre trabajado 18 horas para las 4am
16. Yo habre estado trabajando 18 horas para las 4am
";"
1. I was working last friday
2. I used to work in the government
3. I worked as a policeman
4. I had worked that Thursday
5. I had been working while that happened

6. I would have worked on saturday if he *had been* there
7. I would have been working with Mike
8. I would work if I had the tools

9. I am working now
10. I work at the bank
11. I have worked as a waiter
12. I have been working all this time

13. i will work tomorrow
14. I am going to work the day after tomorrow
15. I will have worked 18 hours by 4am
16. I will have been working 18 hours by 4am
"

"
0. Ustedes eran estudiantes
1. A Ustedes les gustaba la clase del maestro despedido
2. A ustedes les solia gustar la visita del abuelo
3. A Ustedes les habia gustado ese helado
4. A ustedes les gustaria ir a acampar con el telefono
5. A ustedes les estaba empezando a gustar escribir aqui

A. Ustedes son estudiantes
6. A ustedes les esta gustando este masaje
7. A ustedes les gusta correr temprano
8. A ustedes les ha gustado el postre
9. A ustedes les ha estado gustando esta actividad

10. A ustedes les gustara la pelicula
11. A ustedes les va a gustar y punto
12. A ustedes les habra gustado cuando lo intenten
13. A ustedes les habra estado gustando cuando vayan a la mitad
";"
0. You were students
1. You liked the class of the fired teacher
2. You used to like the gradpa's visit
3. You had liked that ice cream
4. You would like to go to camp with the phone
5. You were starting to like to write here

A. You are students
6. You are liking this message
7. You like to run early
8. You have liked the dessert
9. You have been liking this activity

10. You will like the movie
11. You are going to like it and that is all
12. You will have liked *it* when you *try it*
13. You will have been liking it when you go to halfway
"

"
1. Ahi habia perros ayer
2. Ahi solia haber perros cuando vivia el vecino
3. Ahi ha habido perros porque hay pelos
4. Ahi habria un perro si lo hubieras comprado uno

5. Ahí esta un perro comiendo
6. Ahí ha estado habiendo perros en estos dias
7. Ahí hay perros peleandose

8. Ahí habra perros si no cierras la puerta
9. Ahí estaran los perros para cuando lo huelan
10. Ahí va a haber perros por el festival
";"
1. There *were* dogs yesterday
2. There used to be dogs when the neighbor lived
3. there have been dogs because there is hair
4. there would be a dog if you *had bough* one

5. There is a dog eating
6. there have been dogs in this days
7. There are dogs fighting

8. There will have dogs if you don´t close the door
9. there will be the dogs by then they sniff it
10. There is going to be dogs for the festival
"
```

### Instead of vs Instead

[Fuente](https://www.pacho8a.com/ingl%C3%A9s/clases-de-ingl%C3%A9s-en-general/instead-of-vs-instead/)

```r
Instead of: vs Instead; Traduccion, Que son, Estructura, Ejemplos;"
Traduccion
. -Instead of:
.  -En vez de|En lugar de
. -Instead:
.  -En su lugar|a cambio

Que son
. -Instead of:
.  -Preposicion
. -Instead:
.  -Advervio

_
Estructura
. -Instead of:
.  -Instead of + [Sustantivo|ver-ing]
. -Instead:
.  -Inicio o final de la oracion

_
Ejemplos
. -Instead of:
.  -Tomare te en vez de cafe
.    -I´ll drink tea INSTEAD OF coffe
.  -Ellos visitaron Peru en lugar de quedarse en casa
.    -They visited Peru instead of stayING at home
. -Instead:
-  -No viajaran, compraran en su lugar
-   -They won´t travel, [INSTEAD] they´ll buy [INSTEAD]

_
Notas
.  -Despues de una preposicion va el verbo con ing
"

1. Mañana es día feriado, así que trabajaré hoy en lugar de trabajar mañana;"
Tomorrow is holiday, so I´ll work today INSTEAD OF workING tomorrow
"

2. En vez de limpiar su cuarto, Jorge limpió la casa;"
Instead of cleanING his room, Jorge cleaned the house

Jorge cleaned the house instead of cleaning his room.
"

3. Queria llegar temprano al trabajo, en su lugar, llegué tarde;"
I wanted *to* arrive early at work, instead, I arrived late
"

4. Martha no irá a la casa de campo, en su lugar, le gustaría compartir algo de tiempo con sus amigos;"
Martha won´t go to the *country house*, instead, she would like *to* share some * * time with her friends

Martha won’t go to the country house, she’d like to share some time with her friends instead.
"

5. Nuestro bebe nunca duerme una hora, a cambio, él duerme tres horas;"
Our baby never sleep one hour, instead, he sleeps 3 hours

Our baby doesn’t sleep one hour, he sleeps three hours instead.
"

6. En lugar de ir a jugar fútbol, deberías hacer tu tarea;"
Instead of go*ing* to play soccer, you should do your homework

You should to your homework instead of going to play soccer.
"

7. Con esta nueva lavadora, yo solo presiono un botón en lugar de presionar tres;"
With this new *washing* machine, I only press one button INSTEAD OF pressING three

With this new washing machine, INSTEAD OF pressing three buttons, I only press one.
"

8. Juan y Luis no estuvieron en la conferencia, en su lugar, ellos fueron a su bar favorito;"
Juan and Luis *weren't* at the conference, instead, they went to their favorite bar

Juan and Luis weren’t at the conference, they went to their favorite bar instead.
"

9. Preferimos nadar en vez de jugar tenis, pero ellos prefieren ir al mercado en lugar de quedarse con nosotros;"
We prefer *swimming* instead of playing tennis, but they prefer *going* to the market instead of staying with us
"

10. Ella pudo haber venido con nosotros, en su lugar, decidió estar sola;"
she could *have* come with us, instead, she decided *to* be alone

She could have come with us, she decided to be alone instead.
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Que significa GONNA, LEMME, GIMME – Contracciones informales

```r
Gonna, Lemme, Gimme: ¿Que son? ¿Traduccion?, ¿Ejemplos?;"
¿Que son?
  -Contracciones informales

Equivalente en español
  -Voy para el parque
    -Voy pal parque

¿Traduccion?
  -Gonna = Going to = vamos a (futuro)
  -Lemme = Let me = Dejame (Permiso)
  -Gimme = Give me = Dame

¿Ejemplos?
  -I´m not goint to do that
    -I´m not gonna do that

  -Please let me try
    -Please lemme try it

  -Are you give me that please?
    -Are you gimme that please?
"

1.(Usa contracciones informales y traduce) I am going to go to the birth of my son;"
I´m gonna go to the birth of my son.

Voy a ir al nacimiento de mi hijo
"

2.(Usa contracciones informales y traduce) He didn’t let me go, so it is not going to befall;"
He didn’t lemme go, so it's not gonna befall

Él no me dejó ir, así que no va a suceder
"

3.(Usa contracciones informales y traduce) She doesn’t give me the car keys;"
She doesn’t gimme the car keys

Ella no medio las lleves del carro
"

4.(Usa contracciones informales y traduce) Are you going to meet Mary?;"
Are you gonna visit Mary?

¿Vas a visitar a Mary?
"

5.(Usa contracciones informales y traduce) They are going to play the guitar next Saturday;"
They’re gonna play the guitar next Saturday

Ellos van a tocar la guitarra el siguiente sabado
"

6.(Usa contracciones informales y traduce) Let me think about it!;"
Lemme think about it!

Dejame pensar sobre eso!
"

7.(Usa contracciones informales y traduce) Give me a second please. I am going to listen to this audio;"
Gimme a second please. I’m gonna listen to this audio

Dame un segundo por favor. Voy a escuchar este audio
"

8.(Usa contracciones informales y traduce) Is she going to call me back?;"
Is she gonna call me back?

¿Ella me me a llamar de vuelta?
"

9.(Usa contracciones informales y traduce) Did you let me do it?;"
Did you lemme do it?

¿Me dejaste hacerlo?
"

10.(Usa contracciones informales y traduce) I was not going to send that e-mail;"
I wasn’t gonna send that e-mail

Yo no iba a mandarte ese correo
"

11.(Usa contracciones informales y traduce) What are you going to do tonight?;"
What are you gonna do tonight?


¿Que vas a hacer esta noche?
"

12.(Usa contracciones informales y traduce) Are you going to talk to Paul?;"
Are you gonna talk to Paul?

¿Vas a hablar con Paul?
"

13.(Usa contracciones informales y traduce) Let me just grab my handbag;"
Lemme just grab my handbag

Deja me solo tomar mi bolsa de mano
"

14.(Usa contracciones informales y traduce) You are not going to give me anything;"
You are not gonna gimme anything

Tu no me vas a dar nada
"

15.(Usa contracciones informales y traduce) He does not let me try it, but I am going to do it. So, give me that!;"
He does not lemme try it, but I am gonna do it. So, gimme that!

El no me dejara intentarlo, pero voy a hacerlo, entonces dame eso.
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Uso de Wanna, Wansta, Hafta y Hasta - cómo contracciones informales

```r
Wanna, Wansta, Hafta y Hasta: ¿Que son? ¿Traduccion?, ¿Ejemplos?;"
¿Que son?
  -Contracciones informales

Equivalente en español
  -Voy para el parque
    -Voy pal parque

¿Traduccion?
  -Wanna: Want to
  -Wansta: Wants to
  -Hafta: Have to
  -Hasta: Has to

¿Ejemplos?
  -I want to go home
  -I wanna go home

  -She wants to eat pizza
  -She wansta eat pizza

  -We have to practie every day
  -We hafta to practice every day

  -It has to turn on
  -It hasta turn on
"

Ella no quiere hacer nada, pero tiene que hacerlo (usa contracciones infor. y traduce);"
She doesn´t [wanna] do anything, but she [hasta] do it

She doesn’t want to do anything, but she has to do it
"

Michael quiere salir con sus amigos, pero ellos no quieren ir(usa contracciones infor. y traduce);"
Michael [wansta] go out with his friends but they don´t [wanna].

Michael wants to go out with his friends but they don’t want to go
"

Tengo que hacer muchas cosas este fin de semana proximo, pero no quiero trabajar solo(a)(usa contracciones infor. y traduce);"
I [hafta] to do many things this coming weekend, but I dont´t [wanna] work

I have to do many things this coming weekend, but I don’t want to work alone
"

¿Tuviste que despertar tenprano ayer? Fue un dia nevado y helado (usa contracciones infor. y traduce);"
Did you [hafta] to wake up early yesterday? It was a freezing snowy day

Did you have to wake up early yesterday? It was a freezing snowy day
"

Gina quiere conseguir una nueva casa pero tiene que pedir un credito al banco(usa contracciones infor. y traduce);"
Gina [wansta] get a new house but she [hasta] ask for a credit at a bank

Gina wants to get a new house but she has to ask for a credit at a bank
"

El director quiere prohibir a los estudiantes usar chaquetas deportivas(usa contracciones infor. y traduce);"
The principal wansta forbid students to use sports jackets

The principal wants to forbid students to use sports jackets
"

Joel y maria no tienen que hornear el pastel si no quieren hacerlo(usa contracciones infor. y traduce);"
Joel and Maria don’t hafta bake the cake if they don’t wanna do it

Joel and Maria don’t [have to] bake the cake if they don’t [want to] do it
"

No tienes que estar de acuerdo si no compartes sus pensamientos, ella quiere expresar sus sentimientos solo eso(usa contracciones infor. y traduce);"
You don’t [have to] agree if you don’t share her thoughts. she [wants to] express her feelings only that

You don’t hafta agree if you don’t share her thoughts. she wansta express her feelings only that
"

Uno tiene que disfrutar todo en la vida porque solo tenemos una.(usa contracciones infor. y traduce);"
One [hasta] enjoy everything in life because we only have one

One [has to] enjoy everything in life because we only have one
"

I don’t [wanna] miss you but I [hafta] go now. Do you [wanna] go with me?(quita contracciones infor. y traduce);"
I don’t [want to] miss you but I [have to] go now. Do you [want to] go with me?

No quiero extrañarte pero tengo que irme ahora, ¿Quieres ir conmigo?
"

She hasta pick up his children at school by 1 pm because her husband doesn’t wanna do it(quita contracciones infor. y traduce);"
She [has to] pick up his children at school by 1 pm because her husband doesn’t [want to] do it

Ella tiene que recoger sus hijos en la escuela a eso de la 1pm porque su esposo no quiere hacerlo
"

I know that I hafta be patient, however, I don’t wanna wait anymore(quita contracciones infor. y traduce);"
I know that I [have to] be patient, however, I don’t [want to] wait anymore

Yo se que tengo que ser paciente, sin embargo, no quiero esperar mas.
"
```

<!--

#########################################

-->

----

<!--

#########################################

-->

### Cuándo y cómo usar HAD BETTER en todas sus formas

```r
had better: Significados, Estructura, Usos, Ejemplos;"
Significados
  -Sera mejor que
  -Es mejor que
  -Mas te vale que

Estructura
  -Suj + HAD BETTER + VerboBase + Compl.

Usos
  -Advertencias
  -Recomendacion
  -Consejo fuerte
  -Preguntar por un consejo/opinion

Ejemplos
  -[We had|We'd] better sleep now
    -Sera mejor que durmamos ya
  
  -We'd better not sleep now
    -Sera mejor que no durmamos ya
  
  -Had we better sleep now?

  -had nor we better sleep now?
    -¿No sera mejor que durmamos ya?
  -
"

Sera mejor que el este seguro antes de responder;"
He'd better be sure before aswering

he had b...

Se usa el -ing porque tenemos un verbo despues de una prepocion, en
este caso before
"

sera mejor que funcione cuando lo pruebe;"
It'd better work when I test it.

It had b...
"

¿Sera mejor que ella vaya mañana?;"
Had she better go tomorrow?
"

mas te vale que no hagas demasiada sopa;"
You'd better not make too much soup

You had b...
"

Sera mejor que ellos lo inviten a la fiesta;"
They'd better invite him to the party

They had b...
"

Mas vale que no dejes que mi perro se haga pipi aqui;"
You'd better not let my dog pee here

You had b...
"

¿Sera mejor que Juan lo intente otra vez?;"
had Juan better try it again?
"

¿No sera mejor que practiques un poco mas?;"
Hadn't you better practice a little more?

Hadn't=/hadrent/
"

```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

## Avanzado

### Verbos Causativos - Cómo usarlos en voz activa y pasiva

```r
Que es un verbo causativo y ejemplos;"
Son los verbos que causan que un sujeto u objeto cause una accion sobre
otro sujeto u objeto.
.

Ejemplos
.  -She asks to the plumber fix her sink
.  -Ella esta causando que el plomero le haga algo al fregadero.
"

4 verbos que se pueden trabajar de forma causativa;"
Have: Pedir, mandar, solicitar, mandar, hacer que.
-
Get: Conseguir, lograr que, convencer a:
-
Make: Obligar, hacer que (forzar)
-
Want: Querer que (peticion - deseo)
"

Estructura de los verbos causativos con voz activa + ejemplos;"
Sujeto + [Verbo causativo(con tiempo y forma)] + Objeto Indirecto + V.base + Objeto Directo
_
Ejemplos
. She + has + the plumber + fix + her sink
. The profesor + make + me + begin + again=over
. I + got + the mechanic + to + check + the brakes
.  -(En caso del get se le agrega el to)
_
Vebos causativos:
.  Have, Get, Make, Want, etc.
_
Tiempo: pas. pres, fut.
Forma: Neg, Interr, Pos.
"

Traducciones de ´Have´ como verbo causativo;"
Hacer que alguien haga: Pedir, mandar, solicitar, mandar, hacer que alguien/algo.
"

She has the plumber fix her sink (traduce y explica);"
Ella le pidio al plomero que erreglara su fregadero

Porque: En ente caso estamos usando have como verbo causativo, lo cual
de esa forma significa: pedir, mandar, solicitar, encargar a...
"

Estructura de los verbos causativos con voz pasiva + ejemplo;"
Sujeto + [VerboCausativo(con tiempo y forma)] + ObjDire + V.Pas.Par + ObjIndi
_
Ejemplos
.  -She has her sink fixed by the plumber.
.  -I got the brakes checked by the mechanic
_
Vebos causativos:
.  Have, Get, Make, Want, etc.
_
Tiempo: pas. pres, fut.
Forma: Neg, Interr, Pos.
"

She has her sink fixed (traduce y explica);"
Ella [pide|manda|solicita] a arreglar su fregadero.

Porque: En ente caso estamos usando have como verbo causativo, lo cual
de esa forma significa: pedir, mandar, solicitar, encargar a...
"

(+,-,?)Ella mando a arreglar su fregadero (con have=verbo causativo);"
(+)She had her sink fixed 
(-)She didn´t have her sink fixed
(?)Did she have her sink fixed
"

Estructura de los verbos causativos con voz activa y get + ejemplo;"
Sujeto + [get](con tiempo y forma) + Objeto Indirecto + to + V.base + Objeto Directo
-
I got the mechanic to check the brakes
Consegui=logre que el mecanico checara los frenos
-
Tiempo: pas. pres, fut.
Forma: Neg, Interr, Pos.
-
Nota: El mecanico tenia una recistencia a revisarlos, pero se le convencio
"

I got the mechanic to check the brakes (traduce y explica);"
Logre que el mecanico checara los frenos
_
Porque: podemos usar get como verbo causativo el cual significa
Conseguir, lograr que, convencer a
"

El lograra/convencera que el barber cortge su cabello (con get = verbo causativo);"
He will get the barber to cut his hair
"

Estructura de los verbos causativos con voz pasiva y get + ejemplo;"
Sujeto + [get](con tiempo y forma) + Objeto directo + V.Pas.Par + Objeto indirecto
.
I got the brakes checked by the mechanic
Me checaron los frenos = Yo solicite que los frenos fuesen checados
.
Tiempo: pas. pres, fut.
Forma: Neg, Interr, Pos.
"

El solicitara que su cabello sea cortado;"
He will get his hair cut.
"

Estructura de los verbos causativos;"
Sujeto + [get](con tiempo y forma) + Objeto Indirecto + to + V.base + Objeto Directo
-
I got the mechanic to check the brakes
Consegui=logre que el mecanico checara los frenos
-
Tiempo: pas. pres, fut.
Forma: Neg, Interr, Pos.
-
Nota: El mecanico tenia una recistencia a revisarlos, pero se le convencio
"

El profesor me hizo comenzar de nuevo (con make = verbo causativo);"
The profesor make me begin again|over
"

Nuestra madre nos ha hecho cocinar algo especial para ti;"
Our mother *has* make us cook something special for you
"

Susan queria que ellos fueran a la playa;"
Susan *wanted them* to go to the beach
"

¿Quieres que vaya contigo?;"
Do you want me to go with you?
"

1.She had her house painled;"
Ella pidio que su casa fuera pintada
"

2.I made the children tidy up the room;"
Yo hice que los niños ordenaran el cuarto
"

3.You got me to change your broken window;"
Tu lograste que yo cambiara tu ventana rota
"

4 Do you want us to pracice?;"
¿Quieres que practiquemos?
"

5.We got the carpets cleaned;"
Mandamos a limpiar las alfombras

Es voz pasiva porque el `to` no esta presente
"

You should have made your brother call the police;"
Deberias hacer que tu hermano llame a la policia
"

1. Mi madre me pidió ayudarla con los quehaceres porque quiere que yo haga algo;"
My mother had me X(to) help her with the chores because she wants me *to* do something
"

2. Logré que arreglaran mi computadora. Pensé que no funcionaba pero ellos la hicieron funcionar;"
I got *them to* fix my computer. I *thought* it didn´t work but they *made it* work.
"

3. Tu no me obligaste a montar a caballo. Yo mismo quería hacerlo;"
You didn´t make me *ride* a horse. I wanted to do it *myself*
"

4. ¿Quieres que te hagamos trabajar mañana?;"
Do you want us *to make you* work tomorrow?
"

5. Ella no logró que yo trajera su carro. Yo no quería hacerlo. Ella no puede obligarme a hacerlo;"
She didn't get me to bring her car. I didn't want *to* do it. She can't *make* me do it
"

6. Ellos no conseguirán que el profesor les traduzca el documento;"
They won't *get* the teacher to translate them the document
"

7. I didn`t have my brother drive my car;"
I didn't have my car driven
"

8. Did you get your mother to sell her house?;"
Did you get her house sold?
"

9. The students have the teacher explain to them the grammar structures;"
The students have the grammar structures explained to them
"

10. The manager got his secretary to write a new report for tomorrow;"
The manager got a new report written for tomorrow
"

11. He should get him to change his decision;"
He should get his decision changed
"

12. Your boss should have everyone clean all the shelves;"
Your boss should have all the shelves cleaned
"
```

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->

----

<!--

#######################################################################
#######################################################################
#######################################################################

-->
