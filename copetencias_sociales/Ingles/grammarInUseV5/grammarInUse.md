# English Grammar in use

- [English Grammar in use](#english-grammar-in-use)
  - [Present and past](#present-and-past)
    - [2 Present simple (I do)](#2-present-simple-i-do)
    - [3 Present continuous and present simple 1 (I am doing and I do)](#3-present-continuous-and-present-simple-1-i-am-doing-and-i-do)
    - [4 Present continuous and present simple 2 (I am doing and I do)](#4-present-continuous-and-present-simple-2-i-am-doing-and-i-do)
    - [5 Past simple (I did)](#5-past-simple-i-did)
    - [6 Past continuous (I was doing)](#6-past-continuous-i-was-doing)
  - [Present perfect and past](#present-perfect-and-past)
    - [7 Present perfect 1 (I have done)](#7-present-perfect-1-i-have-done)
    - [8 Present perfect 2 (I have done)](#8-present-perfect-2-i-have-done)
    - [9 Present perfect continuous (I have been doing)](#9-present-perfect-continuous-i-have-been-doing)
    - [10 Present perfect continuous and simple (I have been doing and I have done)](#10-present-perfect-continuous-and-simple-i-have-been-doing-and-i-have-done)
    - [11 how long have you (been) ... ?](#11-how-long-have-you-been--)
    - [12 for and since when ... ? and how long ... ?](#12-for-and-since-when---and-how-long--)
    - [13 Present perfect and past 1 (I have done and I did)](#13-present-perfect-and-past-1-i-have-done-and-i-did)
    - [14 Present perfect and past 2 (I have done and I did)](#14-present-perfect-and-past-2-i-have-done-and-i-did)
    - [15 Past perfect (I had done)](#15-past-perfect-i-had-done)
    - [16 Past perfect continuous (I had been doing)](#16-past-perfect-continuous-i-had-been-doing)
    - [17 have and have got](#17-have-and-have-got)
    - [18 used to (do)](#18-used-to-do)

## Present and past

### 2 Present simple (I do)

```yml
id: 2.1
  description: Complete the sentences using the following verbs
  Spanish:
    - 1 Tanya habla Alemán muy bien.
    - 2 Ben y Jack van a la misma escuela.
    - 3 Manejar mal causa muchos accidentes
    - 4 El museo cierra a las 4 los domingos
    - 5 Mis padres viven en un apartamento muy pequeño
    - 6 Los juegos olímpicos se celebran cada cuatro años
    - 7 El canal de panama conecta el océanos atlántico y pacifico
  verbs: cause(s), close(s), connect(s), go(es), live(s), speak(s), take(s)
  toFill:
    - 1 Tanya speaks ___ German very well.
    - 2 Ben and Jack ___ to the same school.
    - 3 Bad driving ___ many accidents
    - 4 The museum ___ at 4 o’clock on Sundays.
    - 5 My parents ___ in a very small flat
    - 6 The Olympic Games ___ place every four years.
    - 7 The Panama Canal ___ the Atlantic and Pacific oceans.
  filled:
    - 1 Tanya { speaks } German very well.
    - 2 Ben and Jack { go } to the same school.
    - 3 Bad driving { causes } many accidents
    - 4 The museum { closes } at 4 o’clock on Sundays.
    - 5 My parents { live } in a very small flat
    - 6 The Olympic Games { take } place every four years.
    - 7 The Panama Canal { connects } the Atlantic and Pacific oceans.

id: 2.2.1
  Spanish: Julia no bebe té muy a menudo
  engInc: Julia doesn’t drink ___ (not / drink) tea very often.
  caution: ========================
  english: Julia doesn't drink tea very often

id: 2.2.2
  Spanish: ¿A que hora los bancos cierran aquí?
  engInc:  What time ___ (the banks / close) here?
  caution: ========================
  english: What time { does the banks } close here?

id: 2.2.3
  Spanish: Tengo un carro, pero no lo uso mucho
  engInc:  I have a car, but I ___ (not / use) it much.
  caution: ========================
  english: I have a car, but I { don't use } it much

id: 2.2.4
  Spanish: ¿De donde es Maria?. ¿es ella Española?
  engInc: Where ___ (Maria / come) from? Is she Spanish?
  caution: ========================
  english: Where { does come } Maria comes from?, Is she Spanish?

id: 2.2.5
  Spanish: A-¿A que te dedicas? B-Yo soy electricista.
  engInc:  A-What ___ (you / do)? B-I’m an electrician.
  caution: ========================
  english: A-What do you do? B-I'm an electrician.

id: 2.2.6
  Spanish: Mira esta sentencia, Que significa esta palabra.
  engInc:  Look at this sentence. What ___ (this word / mean)?
  caution: ========================
  english: Look at this sentence, What does this word mean?

id: 2.2.7
  Spanish: David no esta muy en forma. El no hace ningún deporte
  engInc: David isn't very fit. He ___ (not/do) any sport
  caution: ========================
  english: David isn't very fit. He doesn't do any sports.

id: 2.2.8
  Spanish: Me toma una hora llegar al trabajo en la mañana
  engInc:  It ___ (take) me an hour to get to work in the morning. How Long ___ (it / take) you?
  caution: ========================
  english: It { takes } me an hour to get to work in the morning.

id: 2.2.8.1
  Spanish: ¿Cuanto tiempo te lleva?
  engInc: How Long ___ (it / take) you?
  caution: ========================
  english: How Long does it take you?



id: 2.3
  description: Complete the sentences using these verbs, Sometimes you need the negative.
  SpanishFrases:
    - La tierra gira al rededor del sol
    - El arroz no creese en climas fríos
    - El sol sale por el este
    - Las abejas hacen miel
    - Los vegetarianos no comen carne
    - Un ateo no cree en dios
    - Un interprete traduce de un lenguaje a otro.
    - Los mentirosos son personas que no dicen la verdad
    - El rio Amazonas fluye al océano atlántico 
  words_for_complete: believe, eat, flow, go, grow, make, rise, tell, translate
  incomplete_frases:
    - The earth { goes } round the sun
    - Rice { doesn't grow } in cold climates.
    - The sun ___ in the east.
    - Bees ___ honey.
    - Vegetarians ___ meat.
    - [wrong] An atheist ___ in God.
    - An interpreter ___ from one language into another.
    - Liars are people who ___ the truth.
    - The River Amazon ___ into the Atlantic Ocean.
  caution: ========================
  response: 
    complete_frases:
      - The earth { goes } round the sun
      - Rice { doesn't grow } in cold climates.
      - The sun { rise } in the east.
      - Bees { make } honey.
      - Vegetarians { don't eat } meat.
      - An atheist { doesn't believe } in God.
      - An interpreter { translates } from one language into another.
      - Liars are people who { don't tell } the truth.
      - The River Amazon { flows } into the Atlantic Ocean.

id: 2.4.1
  description: Write the questions
  youWhatToKnow: You know that Lisa plays tennis. You want to know how often.
  incomplete: How often ___
  Spanish: ¿Que tan seguido juegas tenis?
  response: 
    question: How often do you play tennis ?

id: 2.4.2
  description: Write the questions
  youWhatToKnow: Perhaps Lisa's sister plays tennis too. You want to know. Ask Lisa.
  incomplete: ___ your sister ___
  Spanish: ¿Tu hermana juega tenis?
  response: 
    question: Does your sister play tennis?

id: 2.4.3
  description: Write the questions
  youWhatToKnow: You know that Lisa goes to the cinema a lot. You want to know how often. Ask her.
  Spanish: ¿Que tan seguido vas al cine?
  response: 
    question: how often do you go to the cinema?

id: 2.4.4
  description: Write the questions
  youWhatToKnow: You know that Lisa's brother works. You want to know what he does. Ask Lisa.
  Spanish: ¿A que se dedica tu hermano?
  response: 
    question: What does your brother do?

id: 2.4.5
  description: Write the questions
  youWhatToKnow: You're not sure whether Lisa speaks Spanish. You want to know. Ask her.
  Spanish: ¿Hablas español?
  response: 
    question: Do you speak Spanish?

id: 2.4.6
  description: Write the questions
  youWhatToKnow: You don't know where Lisa's grandparents live. You want to know. Ask Lisa.
  Spanish: ¿Donde viven tus abuelos?
  response: 
    question: Where do your grandparents live?

id: 17
  description: Complete using the following:
  Spanish:
    - El señor Evans no esta en la oficina hoy. Yo le sugiero tratar de llamarlo mañana.
    - No le dire a nadie lo que dijiste lo prometo.
    - Debes dejarme pagar la comida insisto
    - Me disculpo por lo que he dicho, No debí haberlo dicho
    - El nuevo restaurante en la calle Baker es muy bueno, yo lo recomiendo
    - Creo tu estas absolutamente en lo correcto, Concuerdo contigo
  incomplete_frases:
    - Mr. Evans is not in the office today. { I suggest } you try calling him tomorrow
    - I won't tell anybody what you said ___
    - You must let me pay for the meal ___
    - ___ for what I said. I shouldn't have said it.
    - The new restaurant in Baker Street is very good ___ it
    - I think you're absolutely right ___ with you.
  to_complete: I agree, I apologies, I insist, I promise, I recommend, I suggest

  response: 
    complete_frases:
      - Mr. Evans is not in the office today. { I suggest } you try calling him tomorrow
      - I won't tell anybody what you said { I promise }
      - You must let me pay for the meal { I insist }
      - { I apologize } for what I said. I shouldn't have said it
      - The new restaurant in Baker Street is very good { I recommend } it
      - I think you're absolutely right { I agree } with you
```

### 3 Present continuous and present simple 1 (I am doing and I do)

```r
"
Description:
  Check if need corrections on {xxx} or put Ok if not requieres any.

Spanish:


Question:
  1 Water {boils} at 100 degrees Celsius.
";
"
Audio:
  [sound:./img/myaudio.mp3]

Solution

"

"2 How often {are you going} to the cinema?";Do you go

"3 Ben {tries} to find a job, but he hasn’t had any luck yet.";
Ok

"4 Martina {is phoning} her mother every day.";
Phones

"5 The moon {goes} round the earth in about 27 days.";
Ok

"6 Can you hear those people? What {do they talk} about?";
{talking}

"7 What {do you do} in your spare time?";
Ok

"8 Sarah is a vegetarian. She {doesn’t eat} meat.";
Ok

"9 I must go now. It {gets} late.";
Getting

"10 ‘Come on! It’s time to leave.’ ‘OK, {I come}.’";
I am coming

"11 Paul is never late. He’s {always starting} work on time.";
start

"12 They don’t get on well. They {are always arguing}.";
Ok
```

```yml
id: 3.2.1
  front: "
    Put the verb into the correct form, present continuous or present simple
    ___ (I / usually / get) hungry in the afternoon.
  "
  back: "
    I usually get hungry in the afternoon.
  "

   I’m getting (I / get) hungry. Let’s go and eat something.
```


### 4 Present continuous and present simple 2 (I am doing and I do)

### 5 Past simple (I did)

### 6 Past continuous (I was doing)

## Present perfect and past

### 7 Present perfect 1 (I have done)

### 8 Present perfect 2 (I have done)

### 9 Present perfect continuous (I have been doing)

### 10 Present perfect continuous and simple (I have been doing and I have done)

### 11 how long have you (been) ... ?

### 12 for and since when ... ? and how long ... ?

### 13 Present perfect and past 1 (I have done and I did)

### 14 Present perfect and past 2 (I have done and I did)

### 15 Past perfect (I had done)

### 16 Past perfect continuous (I had been doing)

### 17 have and have got

### 18 used to (do)
