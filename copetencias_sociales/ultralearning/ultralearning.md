# UltraLearning

## FOREWORD

- A core point is not only soaking up knowledge, is also putting it in use.
- Passive learning create knowledge, active practice create skill
- In theory, there is no difference between theory and practice. But in practice, there is
