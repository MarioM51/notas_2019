# Cuarso de casos reales

## Parte 1

### Escenario Pagina basica: LandingPage/Catalogo

El cliente no esta satisfecho con las plataformas populares como lo son
facebook, twitter, amazon, etc. para dar a conocer su negocio/producto.
Asi que busca una pagina basica para mostrarlo hacerlo de forma optima,
para esto quiere caracteristicas como.

- Mostrar la informacion de una forma personalizada
- Mostrar un catalogo de sus productos o servicios
- Mostrar publicaciones para darse a conocer en la red
- Para caracteristicas mas avanzadas quiere conservar plataformas
  populares para por ejemplo, manejar pedidos, primer contacto, resolver
  consultas, seguira haciendo esto por medio de estas.

### Requerimientos

- Analisis: Casos de uso, Scrum-Backlog
- Diseño UI/UX
- git basico
- html/css/js
- servidores: ssh/ftp/dns

### Casos post-produccion

- maximo 1 vez a la semana quedra algun cambio, lo que significa que
  tendra que contactarnos para hacer un cambio pequeño al html lo cual
  no parece un problema.
- Tener mas o menos 10 visitas por dia.

### ejemplos

- Una carpinteria
  - (A1) Quere mostrar informacion para que posibles clientes los vayan
    a ver fisicamente, o contactar por medio de alguna plataforma, ya
    sean posibles clientes u organizaciones como espresas privadas, gobierno,
    ferias etc, para que luego los contraten o incluyan en alguna actividad.
  - (A2) Mostrar un catalogo de productos que tiene listos para vender, o
    informar si lo tiene en existencia ya sea para que el cliente haga
    un pedido y luego vaya por el al establecimiento.
  - (A3) Darse a conocer por medio de publicaciones de interes, como: sus
    mejores trabajos que han realizado, articulos sobre juguetes (trompos,
    valeros, juegos de mesa), atracciones turisticas, etc.

----

## Paginas intermedias, catalogo interactivo

### Escenario Carrito de pedidos

La pagina basica ya lleva un tiempo funcionando y ya tienen un trafico
mayor, provocando que sea un trabajo significativo manejar los pedidos,
peticiones, e identifican los siguientes problemas.

- Cada rato quieren cambios sobre lo que se muestra en el catalogo, y
  para esto la mejor solucion seria.
  - Editar los productos (cambiar existencia) por un formulario.
  - Mostrar/ocultar un producto
  - Agregar productos

- Cada rato preguntan si tienen existencia de algun producto y la lista
  de productos que quieren de estos, por lo que quieren mostrar las
  unidades disponibles y que seleccionen los productos que quieren
  dentro de su pedido.
  - Lo que va a requerir de un **carrito de pedidos**, para no meterse
    con cosas de mercado pago, paypal y temas legales, NO sera un carrito
    de compras. Cuando realicen el pedido les dara una **clave-pedido** para que
    la puedan mandar una plataforma como whatsapp y ya seguir con el
    pedido de forma manual

- Quieren **manejar los pedidos** de una manera mas sencilla por lo que
  tendran un back-office para resolver lo siguiente de los pedidos.
  - ¿Se creo el pedido pero nunca llego un mensaje para darle continuacion?
  - ¿En que parte del pedido ya no se dio continuidad?
  - ¿Se completo el pedido?
  - ¿Ahi alguna observacion/comentario sobre el pedido?

- Quieren que los clientes sepan que piensan otros clientes, por
  lo que se agrega un enlace a la publicacion del producto en una
  plataforma que acepte comentarios como facebook.

#### Requerimientos



### 